﻿using System;
using System.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using Serilog;

namespace TVSI.Bond.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Configure);
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            AreaRegistration.RegisterAllAreas();

            // Config log4net
            //log4net.LogicalThreadContext.Properties["AccountNo"] = "Other";
            //log4net.LogicalThreadContext.Properties["ApiName"] = "Other";
            //log4net.Config.XmlConfigurator.Configure();

            var logPath = ConfigurationManager.AppSettings["LOG_PATH"] + "";
            if (string.IsNullOrEmpty(logPath))
                logPath = System.Web.Hosting.HostingEnvironment.MapPath("~") + "/Logs/";

            // Config Serilog
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Map("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + "Other", (name, wt) 
                    => wt.File(logPath + name + "-log" + ".log"))
                .CreateLogger();
        }
        //protected void Application_PostAuthorizeRequest() 
        //{
        //    System.Web.HttpContext.Current.SetSessionStateBehavior(System.Web.SessionState.SessionStateBehavior.Required);
        //}
    }
}
