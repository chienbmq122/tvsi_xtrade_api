﻿using System;
using System.Data.SqlTypes;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.Mm;
using TVSI.Bond.WebAPI.Lib.Models.Order;
using TVSI.Bond.WebAPI.Lib.Services;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// Order API
    /// </summary>
    public class OrderController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(OrderController));

        private IOrderService _orderService;
        private IMmService _mmService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        public OrderController(IOrderService orderService, IMmService mmService)
        {
            _orderService = orderService;
            _mmService = mmService;
        }

        #region "Buy/Sell Bond"
        /// <summary>
        /// Lấy danh sách HĐ đang có của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OM_CL_ContractList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<ContractListResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Danh sách HĐ")]
        public async Task<IHttpActionResult> OM_CL_ContractList(ContractListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OM_CL_ContractList");
                SetLanguage(lang);

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var data = await _orderService.GetContractList(model, lang);

                var retModel = new SuccessListModel<ContractListResult> {RetData = data};
                LogResult(log, retModel, model.UserId, "OM_CL_ContractList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OM_CL_ContractList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy chi tiết hợp đồng của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OM_CL_ContractDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ContractDetailResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Danh sách HĐ")]
        public async Task<IHttpActionResult> OM_CL_ContractDetail(ContractDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OM_CL_ContractDetail");
                SetLanguage(lang);

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var data = await _orderService.GetContractDetail(model.ContractNo, model.AccountNo);
                
                var retModel = new SuccessModel<ContractDetailResult> {RetData = data};
                LogResult(log, retModel, model.UserId, "OM_CL_ContractDetail");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OM_CL_ContractDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Danh sách lệnh đặt của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OM_OB_OrderBook")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<OrderBookResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Sổ lệnh")]
        public async Task<IHttpActionResult> OM_OB_OrderBook(OrderBookRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OM_OB_OrderBook");
                SetLanguage(lang);
                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var data = await _orderService.GetOrderBook(DateTime.Today.Date, model.AccountNo);

                var retModel = new SuccessListModel<OrderBookResult> {RetData = data};
                LogResult(log, retModel, model.UserId, "OM_OB_OrderBook");
                return Ok(retModel);

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OM_OB_OrderBook");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lây thông tin chi tiết lệnh đặt của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OM_OB_OrderDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<OrderDetailResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Sổ lệnh")]
        public async Task<IHttpActionResult> OM_OB_OrderDetail(OrderDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OM_OB_OrderDetail");
                SetLanguage(lang);

                var retData = await _orderService.GetOrderDetail(model.ContractNo, model.AccountNo);
                
                var retModel = new SuccessModel<OrderDetailResult> {RetData = retData};
                LogResult(log, retModel, model.UserId, "OM_OB_OrderDetail");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OM_OB_OrderDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Hủy lệnh đặt của Khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OM_OB_OrderCancel")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Sổ lệnh")]
        public async Task<IHttpActionResult> OM_OB_OrderCancel(OrderCancelRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OM_OB_OrderCancel");
                SetLanguage(lang);

                var retInt = await _orderService.CancelOrder(model.ContractNo, model.AccountNo);
                if (retInt == 1)
                {
                    return Ok(new ErrorModel
                              {
                                  RetCode = RetCodeConst.MOMOB_E001_NOT_FOUND,
                                  RetMsg = ResourceFile.OrderModule.MOMOB_E001
                              });
                }

                if (retInt == 2)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = RetCodeConst.MOMOB_E002_STATUS_HAS_CHANGED,
                        RetMsg = ResourceFile.OrderModule.MOMOB_E002
                    });
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OM_OB_OrderCancel");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách lịch sử giao dịch của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OM_OH_OrderHistList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<OrderHistResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Lịch sử giao dịch")]
        public async Task<IHttpActionResult> OM_OH_OrderHistList(OrderHistRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OM_OH_OrderHistList");
                SetLanguage(lang);

                // Validate empty data of request model
                var errModel = ValidateRequestModel(model, lang);
                if (errModel != null)
                    return Ok(errModel);

                var dtxFromDate = SqlDateTime.MinValue;
                var dtxToDate = SqlDateTime.MaxValue;

                if (!string.IsNullOrEmpty(model.FromDate))
                    dtxFromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(model.ToDate))
                    dtxToDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _orderService.GetOrderHistList((DateTime)dtxFromDate, (DateTime)dtxToDate, model.AccountNo);

                return Ok(new SuccessListModel<OrderHistResult> { RetData = data });

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OM_OH_OrderHistList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// lấy danh sách lệnh đặt cần KH xác nhận
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OM_OC_OrderConfirmList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<OrderConfirmListResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Xác nhận lệnh đặt")]
        public async Task<IHttpActionResult> OM_OC_OrderConfirmList(OrderConfirmListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            
            try
            {
                LogRequest(log, model, "OM_OC_OrderConfirmList");
                SetLanguage(lang);
                // Validate empty data of request model
                var errModel = ValidateRequestModel(model, lang);
                if (errModel != null)
                    return Ok(errModel);

                var dtxFromDate = SqlDateTime.MinValue;
                var dtxToDate = SqlDateTime.MaxValue;

                if (!string.IsNullOrEmpty(model.FromDate))
                    dtxFromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(model.ToDate))
                    dtxToDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _orderService.GetOrderConfirmList((DateTime)dtxFromDate, (DateTime)dtxToDate, model.AccountNo);

                return Ok(new SuccessListModel<OrderConfirmListResult> { RetData = data });

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OM_OC_OrderConfirmList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Xác nhận đã nhận hồ sơ hợp đồng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OM_OC_OrderConfirm")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Xác nhận hợp đồng")]
        public async Task<IHttpActionResult> OM_OC_OrderConfirm(OrderConfirmRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OM_OC_OrderConfirm");
                SetLanguage(lang);

                await _orderService.ConfirmOrder(model.RequestOrderID, model.AccountNo);
                
                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OM_OC_OrderConfirm");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        #endregion

        #region "Buy/Sell MM"
        /// <summary>
        /// Lấy danh sách HĐ MM đang có của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_CL_ContractList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<MmContractListResult>))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Danh sách HĐ")]
        public async Task<IHttpActionResult> MM_CL_ContractList(MmContractListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "MM_CL_ContractList");
                SetLanguage(lang);

                var data = await _mmService.GetMmContractList(model.AccountNo);

                var retModel = new SuccessListModel<MmContractListResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "MM_CL_ContractList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_CL_ContractList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin tóm tắt của HĐ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_CL_ShortContractDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<MmContractShortDetailResult>))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Danh sách HĐ")]
        public async Task<IHttpActionResult> MM_CL_ShortContractDetail(MmContractShortDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "MM_CL_ShortContractDetail");
                SetLanguage(lang);

                var data = await _mmService.GetMmShortContractDetail(model.AccountNo, model.ContractNo);

                var retModel = new SuccessModel<MmContractShortDetailResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "MM_CL_ShortContractDetail");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_CL_ShortContractDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy chi tiết hợp đồng MM của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_CL_ContractDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<MmContractDetailResult>))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Danh sách HĐ")]
        public async Task<IHttpActionResult> MM_CL_ContractDetail(MmContractDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "MM_CL_ContractDetail");
                SetLanguage(lang);

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var data = await _mmService.GetMmContractDetail(model.ContractNo, model.AccountNo);

                var retModel = new SuccessModel<MmContractDetailResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "MM_CL_ContractDetail");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_CL_ContractDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách lệnh đặt MM trong ngày của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_OB_OrderBook")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<MmOrderBookResult>))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Sổ lệnh")]
        public async Task<IHttpActionResult> MM_OB_OrderBook(MmOrderBookRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "MM_OB_OrderBook");
                SetLanguage(lang);
                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var data = await _mmService.GetMmOrderBook(DateTime.Today.Date, model.AccountNo);

                var retModel = new SuccessListModel<MmOrderBookResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "MM_OB_OrderBook");
                return Ok(retModel);

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_OB_OrderBook");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lây thông tin chi tiết lệnh đặt MM của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_OB_OrderDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<MmOrderDetailResult>))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Sổ lệnh")]
        public async Task<IHttpActionResult> MM_OB_OrderDetail(MmOrderDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "MM_OB_OrderDetail");
                SetLanguage(lang);

                var retData = await _mmService.GetMmOrderDetail(model.ContractNo, model.AccountNo);

                var retModel = new SuccessModel<MmOrderDetailResult> { RetData = retData };
                LogResult(log, retModel, model.UserId, "MM_OB_OrderDetail");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_OB_OrderDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Hủy lệnh đặt MM của Khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_OB_OrderCancel")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Sổ lệnh")]
        public async Task<IHttpActionResult> MM_OB_OrderCancel(MmOrderCancelRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "MM_OB_OrderCancel");
                SetLanguage(lang);

                var retInt = await _mmService.CancelMmOrder(model.ContractNo, model.AccountNo);
                if (retInt == 1)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = RetCodeConst.MOMOB_E001_NOT_FOUND,
                        RetMsg = ResourceFile.OrderModule.MOMOB_E001
                    });
                }

                if (retInt == 2)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = RetCodeConst.MOMOB_E002_STATUS_HAS_CHANGED,
                        RetMsg = ResourceFile.OrderModule.MOMOB_E002
                    });
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_OB_OrderCancel");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách lịch sử giao dịch HDMG của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_OH_OrderHistList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<MmOrderHistResult>))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Lịch sử giao dịch")]
        public async Task<IHttpActionResult> MM_OH_OrderHistList(MmOrderHistRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "MM_OH_OrderHistList");
                SetLanguage(lang);

                // Validate empty data of request model
                var errModel = ValidateRequestModel(model, lang);
                if (errModel != null)
                    return Ok(errModel);

                var dtxFromDate = SqlDateTime.MinValue;
                var dtxToDate = SqlDateTime.MaxValue;

                if (!string.IsNullOrEmpty(model.FromDate))
                    dtxFromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(model.ToDate))
                    dtxToDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _mmService.GetMmOrderHistList((DateTime)dtxFromDate, (DateTime)dtxToDate, model.AccountNo);

                return Ok(new SuccessListModel<MmOrderHistResult> { RetData = data });

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_OH_OrderHistList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách lệnh đặt MM cần KH xác nhận
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_OC_OrderConfirmList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<MMOrderConfirmListResult>))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Xác nhận lệnh đặt")]
        public async Task<IHttpActionResult> MM_OC_OrderConfirmList(MmOrderConfirmListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {

            try
            {
                LogRequest(log, model, "MM_OC_OrderConfirmList");
                SetLanguage(lang);

                var dtxFromDate = SqlDateTime.MinValue;
                var dtxToDate = SqlDateTime.MaxValue;

                if (!string.IsNullOrEmpty(model.FromDate))
                    dtxFromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(model.ToDate))
                    dtxToDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _mmService.GetMmOrderConfirmList((DateTime)dtxFromDate, (DateTime)dtxToDate, model.AccountNo);

                var successModel = new SuccessListModel<MMOrderConfirmListResult> {RetData = data};
                LogResult(log, successModel, model.UserId, "MM_OC_OrderConfirmList");

                return Ok(successModel);

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_OC_OrderConfirmList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách dòng tiền của lệnh bán cần xác nhận
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_OC_GetContractPaymentInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<MMOrderPaymentInfoResult>))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Xác nhận lệnh đặt")]
        public async Task<IHttpActionResult> MM_OC_GetContractPaymentInfo(MMOrderPaymentInfoRequet model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {

            try
            {
                LogRequest(log, model, "MM_OC_GetContractPaymentInfo");
                SetLanguage(lang);

               
                var data = await _mmService.MM_OC_GetContractPaymentInfo(model.ContractNo);

                return Ok(new SuccessListModel<MMOrderPaymentInfoResult> { RetData = data });

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_OC_GetContractPaymentInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Xác nhận đã nhận hồ sơ hợp đồng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_OC_OrderConfirm")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Xác nhận hợp đồng")]
        public async Task<IHttpActionResult> MM_OC_OrderConfirm(MmOrderConfirmRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "MM_OC_OrderConfirm");
                SetLanguage(lang);

                await _mmService.ConfirmMmOrder(model.AccountNo, model.RequestOrderID);

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_OC_OrderConfirm");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        #endregion

        #region Xác nhận thông tin đặt lệnh
        /// <summary>
        /// Lây thông tin chi tiết xác nhận lệnh đặt của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OM_OB_OrderConfirmDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<OrderConfirmDetailResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Xác nhận lệnh đặt")]
        public async Task<IHttpActionResult> OM_OB_OrderConfirmDetail(OrderConfirmDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OM_OB_OrderConfirmDetail");
                SetLanguage(lang);

                var retData = await _orderService.GetOrderConfirmDetail(model.RequestOrderID, model.AccountNo);

                var retModel = new SuccessModel<OrderConfirmDetailResult> { RetData = retData };
                LogResult(log, retModel, model.UserId, "OM_OB_OrderConfirmDetail");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OM_OB_OrderConfirmDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lây thông tin chi tiết xác nhận lệnh đặt của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_OB_OrderConfirmDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<MMOrderConfirmDetailResult>))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Xác nhận lệnh đặt")]
        public async Task<IHttpActionResult> MM_OB_OrderConfirmDetail(MMOrderConfirmDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "MM_OB_OrderConfirmDetail");
                SetLanguage(lang);

                var retData = await _mmService.GetMMOrderConfirmDetail(model.RequestOrderID, model.AccountNo);

                var retModel = new SuccessModel<MMOrderConfirmDetailResult> { RetData = retData };
                LogResult(log, retModel, model.UserId, "MM_OB_OrderConfirmDetail");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_OB_OrderConfirmDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Từ chối nhận hồ sơ hợp đồng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OM_OC_OrderRefuse")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Từ chối hợp đồng")]
        public async Task<IHttpActionResult> OM_OC_RefuseConfirm(OrderRefuseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OM_OC_RefuseConfirm");
                SetLanguage(lang);

                var retInt = await _orderService.RefuseOrder(model.RequestOrderID, model.AccountNo, model.Note);

                if (retInt == 1)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = RetCodeConst.MOMOB_E003_CAN_NOT_REFUSE,
                        RetMsg = ResourceFile.OrderModule.MOMOB_E003
                    });
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OM_OC_RefuseConfirm");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Từ chối nhận hồ sơ hợp đồng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MM_OC_OrderRefuse")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình SPTC > Tab HĐMG > Tab TTGD > Từ chối hợp đồng")]
        public async Task<IHttpActionResult> MM_OC_OrderRefuse(MmOrderRefuseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "MM_OC_OrderRefuse");
                SetLanguage(lang);

                var retInt = await _mmService.RefuseMmOrder(model.AccountNo, model.RequestOrderID, model.Note);

                if (retInt == 1)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = RetCodeConst.MOMOB_E003_CAN_NOT_REFUSE,
                        RetMsg = ResourceFile.OrderModule.MOMOB_E003
                    });
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MM_OC_OrderRefuse");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        #endregion
    }
}
