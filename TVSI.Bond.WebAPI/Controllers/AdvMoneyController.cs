﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Lib.Models.AdvMoney;
using TVSI.Bond.WebAPI.Lib.Services;

namespace TVSI.Bond.WebAPI.Controllers
{
    public class AdvMoneyController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(AdvMoneyController));
        private IAdvMoneyService _advMoneyService;
        private ICashTransferService _cashTransferService;
        private ISystemService _systemService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="AdvMoneyController"></param>
        public AdvMoneyController(IAdvMoneyService advMoneyService, ICashTransferService cashTransferService, ISystemService systemService)
        {
            _advMoneyService = advMoneyService;
            _cashTransferService = cashTransferService;
            _systemService = systemService;
        }

        #region API ứng tiền

        /// <summary>
        /// Lấy thông tin có thể ứng của tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("ADV_CI_GetAvaiableAdvanceInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AvaiableAdvanceInfoResult>))]
        [Note("Màn hình GD tiền > Ứng tiền", true)]
        public async Task<IHttpActionResult> ADV_CI_GetAvaiableAdvanceInfo(AvaiableAdvanceInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "ADV_CI_GetAvaiableAdvanceInfo");
                
                var data = await _advMoneyService.GetAvaiableAdvanceInfo(model.AccountNo);

                var dataRet = new SuccessModel<AvaiableAdvanceInfoResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "ADV_CI_GetAvaiableAdvanceInfo");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "ADV_CI_GetAvaiableAdvanceInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Lấy phí ứng tiền theo số tiền ứng hoặc số tiền thực nhận
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("ADV_CI_GetFeeAdvanceInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<FeeAdvanceInfoResult>))]
        [Note("Màn hình GD tiền > Ứng tiền", true)]
        public async Task<IHttpActionResult> ADV_CI_GetFeeAdvanceInfo(FeeAdvanceInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "ADV_CI_GetFeeAdvanceInfo");
                var data = new FeeAdvanceInfoResult
                {
                    FeeAmt = 0
                };
                var systemDate = _systemService.GetSystemDate();
                if (model.Type == 1)
                    data.FeeAmt = _cashTransferService.GetAdvanceFee(model.AccountNo, systemDate.ToString("yyyy-MM-dd"), TVSI.Utility.Functions.GetDouble(model.Amount), model.NumDay);
                else
                {
                    data.FeeAmt = _cashTransferService.GetAdvanceFee(model.AccountNo, systemDate.ToString("yyyy-MM-dd"), TVSI.Utility.Functions.GetDouble(model.Amount), model.NumDay);
                    if (data.FeeAmt > 0)
                    {
                        var tien_thuc_nhan = model.Amount - data.FeeAmt;
                        var bieu_phi = Math.Round(data.FeeAmt / (model.NumDay * tien_thuc_nhan), 5);
                        data.FeeAmt = Math.Ceiling(model.Amount * bieu_phi * model.NumDay);
                    }
                }

                var dataRet = new SuccessModel<FeeAdvanceInfoResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "ADV_CI_GetFeeAdvanceInfo");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "ADV_CI_GetFeeAdvanceInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Thêm mới lệnh ứng tiền
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("ADV_CI_AddCustAdvance")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AddAdvanceInfoResult>))]
        [Note("Màn hình GD tiền > Ứng tiền", true)]
        public async Task<IHttpActionResult> ADV_CI_AddCustAdvance(AddAdvanceInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "ADV_CI_AddCustAdvance");

                var data = await _advMoneyService.AddCustAdvance(model);

                var dataRet = new SuccessModel<AddAdvanceInfoResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "ADV_CI_AddCustAdvance");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "ADV_CI_AddCustAdvance");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách lệnh ứng tiền trong ngày
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("ADV_CI_CustAdvanceToday")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<CustAdvanceInfoResult>))]
        [Note("Màn hình GD tiền > Ứng tiền", true)]
        public async Task<IHttpActionResult> ADV_CI_CustAdvanceToday(CustAdvanceTodayRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "ADV_CI_CustAdvanceToday");

                var data = await _advMoneyService.GetCustAdvanceToday(model.AccountNo, lang);
                var dataRet = new SuccessListModel<CustAdvanceInfoResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "ADV_CI_CustAdvanceToday");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "ADV_CI_CustAdvanceToday");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách lệnh ứng tiền lịch sử
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("ADV_CI_CustAdvanceHist")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<CustAdvanceInfoResult>))]
        [Note("Màn hình GD tiền > Ứng tiền", true)]
        public async Task<IHttpActionResult> ADV_CI_CustAdvanceHist(CustAdvanceHistRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "ADV_CI_CustAdvanceHist");

                var data = await _advMoneyService.GetCustAdvanceHist(model.AccountNo, model.FromDate, model.ToDate, lang);
                var dataRet = new SuccessListModel<CustAdvanceInfoResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "ADV_CI_CustAdvanceHist");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "ADV_CI_CustAdvanceHist");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        #endregion
    }
}