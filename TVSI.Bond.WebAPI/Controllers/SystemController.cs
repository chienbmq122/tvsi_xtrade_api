﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Services;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models.System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Models.QRCode;
using Serilog;
using TVSI.Bond.WebAPI.Lib.Models.Notications;

//using Nest;

namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// System API
    /// </summary>
    public class SystemController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(SystemController));
        private ISystemService _systemService;
        private IQRService _qrService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="systemService"></param>
        public SystemController(ISystemService systemService, IQRService qrService)
        {
            _systemService = systemService;
            _qrService = qrService;
        }

        /// <summary>
        /// Check thông tin đăng nhập
        /// </summary>
        /// <param name="model">Thông tin đăng nhập</param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /System/SM_LO_SystemLogin
        ///     {
        ///         "UserId": "044007",
        ///         "Password": "62-BF-09-B1-9A-D3-B7-FD-40-BF-DD-70-A9-79-87-71"
        ///     }
        ///
        /// </remarks>
        [ActionName("SM_LO_SystemLogin")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<UserLoginResult>))]
        [Note("MH Đăng nhập")]
        public async Task<IHttpActionResult> SM_LO_SystemLogin(UserLoginRequest model
            , string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                // Log request Info
                var requestJson = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //log4net.LogicalThreadContext.Properties["AccountNo"] = "user_login";
                ////log4net.Config.XmlConfigurator.Configure();

                //log.Info("UserID=" + model.UserId + "\t Api=SM_LO_SystemLogin \t Request=" + requestJson);

                Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId).Information("UserID = " +
                    model.UserId
                    + "\t Api = SM_LO_SystemLogin \t Request = " + requestJson);

                // Test elastic search
                //var uri = new Uri("http://10.0.67.14:9200");
                //var settings = new ConnectionSettings(uri);
                //ElasticClient client = new ElasticClient(settings);
                //settings.SetDefaultIndex("bondmobileapi");
                //client.Index<UserLoginRequest>(model, null);


                SetLanguage(lang);

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                if (string.IsNullOrEmpty(model.UserId))
                    return Ok(new ErrorModel
                    {
                        RetCode = RetCodeConst.MSMLO_E001_PASSWORD_INCORRECT,
                        RetMsg = ResourceFile.SystemModule.MSMLO_001
                    });

                //if (model.UserId.Length < 6)
                //    return Ok(new ErrorModel
                //    {
                //        RetCode = RetCodeConst.MSMLO_E001_PASSWORD_INCORRECT,
                //        RetMsg = ResourceFile.SystemModule.MSMLO_001
                //    });

                var retInt = await _systemService.GetUserStatus(model.UserId, model.Password);

                switch (retInt)
                {
                    case -2:
                        //log.Info("UserID=" + model.UserId + "\t Api=SM_LO_SystemLogin \t Login Failed. Status=" + retInt);
                        Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId).Information(
                            "UserID=" + model.UserId
                                      + "\t Api=SM_LO_SystemLogin \t Login Failed. Status=" + retInt);
                        return Ok(new ErrorModel
                        {
                            RetCode = RetCodeConst.MSMLO_E003_NOT_PERMISSION,
                            RetMsg = ResourceFile.SystemModule.MSMLO_003
                        });

                    case -1:
                        //log.Info("UserID=" + model.UserId + "\t Api=SM_LO_SystemLogin \t Login Failed. Status=" + retInt);
                        Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId).Information(
                            "UserID=" + model.UserId
                                      + "\t Api=SM_LO_SystemLogin \t Login Failed. Status=" + retInt);
                        return Ok(new ErrorModel
                        {
                            RetCode = RetCodeConst.MSMLO_E001_PASSWORD_INCORRECT,
                            RetMsg = ResourceFile.SystemModule.MSMLO_001
                        });

                    case 0:
                        //log.Info("UserID=" + model.UserId + "\t Api=SM_LO_SystemLogin \t Login Failed. Status=" + retInt);
                        Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId).Information(
                            "UserID=" + model.UserId
                                      + "\t Api=SM_LO_SystemLogin \t Login Failed. Status=" + retInt);
                        return Ok(new ErrorModel
                        {
                            RetCode = RetCodeConst.MSMLO_E002_CUSTOMER_NOT_ACTIVE,
                            RetMsg = ResourceFile.SystemModule.MSMLO_002
                        });
                }

                //log4net.LogicalThreadContext.Properties["AccountNo"] = "user_login";
                //log4net.LogicalThreadContext.Properties["ApiName"] = "SM_LO_SystemLogin";
                //log4net.Config.XmlConfigurator.Configure();

                // Xử lý login, get danh sách tài khoản trả về
                var accountNoList = await _systemService.GetAccountNoList(model.UserId);
                var dataRet = new SuccessListModel<UserLoginResult> { RetData = accountNoList };
                //LogResult(log, dataRet, "SM_LO_SystemLogin");
                var resultJson = JsonConvert.SerializeObject(dataRet, Formatting.None, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //log.Debug("Api=SM_LO_SystemLogin" + "\t Response=" + resultJson);
                Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId)
                    .Information("Api=SM_LO_SystemLogin" + "\t Response=" + resultJson);
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                //log4net.LogicalThreadContext.Properties["AccountNo"] = "user_login";
                //log4net.LogicalThreadContext.Properties["ApiName"] = "SM_LO_SystemLogin";
                //log4net.Config.XmlConfigurator.Configure();
                //log.Error("SM_LO_SystemLogin:" + ex.Message);
                Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId).Error(
                    "Api=SM_LO_SystemLogin" + "\t ErrMsg=" + ex.Message
                    + "\t InnerException=" + ex.InnerException);
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        /// Check thông tin đăng nhập
        /// </summary>
        /// <param name="model">Thông tin đăng nhập</param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /System/SM_LO_ApplicationLogin
        ///     {
        ///         "UserId": "cuonglm",
        ///         "Password": "Password xịn"
        ///     }
        ///
        /// </remarks>
        [ActionName("SM_LO_ApplicationLogin")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<UserLoginResult>))]
        [Note("MH Đăng nhập")]
        public async Task<IHttpActionResult> SM_LO_ApplicationLogin(UserLoginRequest model
            , string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                // Log request Info
                var requestJson = JsonConvert.SerializeObject(new UserLoginRequest
                {
                    UserId = model.UserId,
                    Password = "****"
                }, Formatting.None, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //log4net.LogicalThreadContext.Properties["AccountNo"] = "user_login";
                ////log4net.Config.XmlConfigurator.Configure();

                //log.Info("UserID=" + model.UserId + "\t Api=SM_LO_SystemLogin \t Request=" + requestJson);

                Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId).Information("UserID = " +
                    model.UserId
                    + "\t Api = SM_LO_ApplicationLogin \t Request = " + requestJson);

                // Test elastic search
                //var uri = new Uri("http://10.0.67.14:9200");
                //var settings = new ConnectionSettings(uri);
                //ElasticClient client = new ElasticClient(settings);
                //settings.SetDefaultIndex("bondmobileapi");
                //client.Index<UserLoginRequest>(model, null);


                SetLanguage(lang);

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                if (string.IsNullOrEmpty(model.UserId))
                    return Ok(new ErrorModel
                    {
                        RetCode = RetCodeConst.MSMLO_E001_PASSWORD_INCORRECT,
                        RetMsg = ResourceFile.SystemModule.MSMLO_001
                    });

                var retInt = await _systemService.GetUserStatus(model.UserId, model.Password);

                // 20220808-Xử lý Case Super Account
                // BEGIN
                if (retInt == -1)
                {
                    var retLoginLdap = await _systemService.LoginLdap(model.UserId, model.Password);
                    if (retLoginLdap == -1)
                    {
                        Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId).Information(
                            "UserID=" + model.UserId
                                      + "\t Api=SM_LO_ApplicationLogin \t Login Failed. LDAP CHECKED FAILED");
                        return Ok(new ErrorModel
                        {
                            RetCode = RetCodeConst.MSMLO_E001_PASSWORD_INCORRECT,
                            RetMsg = ResourceFile.SystemModule.MSMLO_001
                        });
                    }

                    if (retLoginLdap == -2)
                    {
                        Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId).Information(
                            "UserID=" + model.UserId
                                      + "\t Api=SM_LO_ApplicationLogin \t Login Failed. LDAP NOT PERMISSION");
                        return Ok(new ErrorModel
                        {
                            RetCode = RetCodeConst.MSMLO_E003_NOT_PERMISSION,
                            RetMsg = ResourceFile.SystemModule.MSMLO_003
                        });
                    }

                    if (retLoginLdap > 0)
                    {
                        return Ok(new SuccessListModel<UserLoginResult>
                            {
                                RetData = new List<UserLoginResult>
                                {
                                    new UserLoginResult
                                    {
                                        UserId = model.UserId,
                                        AccountNo = model.UserId,
                                        AccountName = _systemService.GetLdapUserName(model.UserId),
                                        UserType = retLoginLdap
                                    }
                                }
                            }
                        );
                    }
                }
                // END

                switch (retInt)
                {
                    case -1:
                        //log.Info("UserID=" + model.UserId + "\t Api=SM_LO_SystemLogin \t Login Failed. Status=" + retInt);
                        Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId).Information(
                            "UserID=" + model.UserId
                                      + "\t Api=SM_LO_ApplicationLogin \t Login Failed. Status=" + retInt);
                        return Ok(new ErrorModel
                        {
                            RetCode = RetCodeConst.MSMLO_E001_PASSWORD_INCORRECT,
                            RetMsg = ResourceFile.SystemModule.MSMLO_001
                        });

                    case 0:
                        //log.Info("UserID=" + model.UserId + "\t Api=SM_LO_SystemLogin \t Login Failed. Status=" + retInt);
                        Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId).Information(
                            "UserID=" + model.UserId
                                      + "\t Api=SM_LO_ApplicationLogin \t Login Failed. Status=" + retInt);
                        return Ok(new ErrorModel
                        {
                            RetCode = RetCodeConst.MSMLO_E002_CUSTOMER_NOT_ACTIVE,
                            RetMsg = ResourceFile.SystemModule.MSMLO_002
                        });
                }

                //log4net.LogicalThreadContext.Properties["AccountNo"] = "user_login";
                //log4net.LogicalThreadContext.Properties["ApiName"] = "SM_LO_SystemLogin";
                //log4net.Config.XmlConfigurator.Configure();

                // Xử lý login, get danh sách tài khoản trả về
                var accountNoList = await _systemService.GetAccountNoList(model.UserId);
                var dataRet = new SuccessListModel<UserLoginResult> { RetData = accountNoList };
                //LogResult(log, dataRet, "SM_LO_SystemLogin");
                var resultJson = JsonConvert.SerializeObject(dataRet, Formatting.None, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //log.Debug("Api=SM_LO_SystemLogin" + "\t Response=" + resultJson);
                Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId)
                    .Information("Api=SM_LO_ApplicationLogin" + "\t Response=" + resultJson);
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                //log4net.LogicalThreadContext.Properties["AccountNo"] = "user_login";
                //log4net.LogicalThreadContext.Properties["ApiName"] = "SM_LO_SystemLogin";
                //log4net.Config.XmlConfigurator.Configure();
                //log.Error("SM_LO_SystemLogin:" + ex.Message);
                Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + model.UserId).Error(
                    "Api=SM_LO_ApplicationLogin" + "\t ErrMsg=" + ex.Message
                    + "\t InnerException=" + ex.InnerException);
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        /// Kiểm tra password
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /System/SM_CP_CheckPassword
        ///     {
        ///         "UserId": "044007",
        ///         "AccountNo": "0440071",
        ///         "Password": "62-BF-09-B1-9A-D3-B7-FD-40-BF-DD-70-A9-79-87-71"
        ///     }
        ///
        /// </remarks>
        [ActionName("SM_CP_CheckPassword")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình kiểm tra Password")]
        public async Task<IHttpActionResult> SM_CP_CheckPassword(CheckPasswordRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_CP_CheckPassword");
                SetLanguage(lang);


                // FO_AM_Change_Password
                var isValid = await _systemService.CheckPassword(model.UserId, model.Password);

                if (!isValid)
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = "MSMCP_001",
                        RetMsg = ResourceFile.SystemModule.MSMCP_001
                    };
                    LogResult(log, errorModel, model.UserId, "SM_CP_CheckPassword");
                    return Ok(errorModel);
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_CP_CheckPassword");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        /// Kiểm tra Pin
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_CP_CheckPin")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình kiểm tra Password")]
        public async Task<IHttpActionResult> SM_CP_CheckPin(CheckPinRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_CP_CheckPin");
                SetLanguage(lang);


                // FO_AM_Change_Password
                var isValid = await _systemService.CheckPin(model.UserId, model.Pin);

                if (!isValid)
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = "MSMCI_002",
                        RetMsg = ResourceFile.SystemModule.MSMCI_002
                    };
                    LogResult(log, errorModel, model.UserId, "SM_CP_CheckPin");
                    return Ok(errorModel);
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_CP_CheckPin");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        /// Đổi password
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_CA_ChangePassword")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("MH thông tin tài khoản > Thông tin đăng nhập > Thay đổi mật khẩu")]
        public async Task<IHttpActionResult> SM_CA_ChangePassword(ChangePasswordRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_CA_ChangePassword");
                SetLanguage(lang);

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                // FO_AM_Change_Password
                var retInt = await _systemService.ChangePassword(model.UserId, model.NewPassword, model.OldPassword);

                if (retInt > 0)
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = RetCodeConst.MSMCA_E001_OLDPASSWORD_INCORRECT,
                        RetMsg = ResourceFile.SystemModule.MSMCA_001
                    };
                    LogResult(log, errorModel, model.UserId, "SM_CA_ChangePassword");
                    return Ok(errorModel);
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_CA_ChangePassword");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        /// Đổi mã Pin
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_CI_ChangePin")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("MH thông tin tài khoản > Thông tin đăng nhập > Thay đổi mã Pin")]
        public async Task<IHttpActionResult> SM_CI_ChangePin(ChangePinRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_CI_ChangePin");
                SetLanguage(lang);

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                // FO_AM_Change_Pin
                var retInt = await _systemService.ChangePin(model.UserId, model.NewPin, model.OldPin);

                if (retInt > 0)
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = RetCodeConst.MSMCI_E001_OLDPIN_INCORRECT,
                        RetMsg = ResourceFile.SystemModule.MSMCI_001
                    };
                    LogResult(log, errorModel, model.UserId, "SM_CI_ChangePin");
                    return Ok(errorModel);
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_CI_ChangePin");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        /// Lấy danh sách thông báo
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("SM_NO_NotificationList")]
        [ResponseType(typeof(SuccessModel<NotificationResult>))]
        [Note("All Màn hình > Icon thông báo > Màn hình thông báo  ")]
        public async Task<IHttpActionResult> SM_NO_NotificationList(NotificationRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                //LogRequest(log, model, "SM_NO_NotificationList");
                SetLanguage(lang);

                //// Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var notificationList =
                    await _systemService.GetNotificationList(model.PageIndex, model.PageSize, model.AccountNo, lang);
                var dataRet = new SuccessModel<NotificationResult> { RetData = notificationList };

                //LogResult(log, dataRet, "SM_NO_NotificationList");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_NO_NotificationList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Đồng bộ những thông báo đã đọc về TVSI
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("SM_NO_SyncReadNotification")]
        [ResponseType(typeof(SuccessModel))]
        [Note("All Màn hình > Icon thông báo > Màn hình thông báo  ")]
        public async Task<IHttpActionResult> SM_NO_SyncReadNotification(SyncNotificationRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_NO_SyncReadNotification");

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                await _systemService.SyncNotificationRead(model.MessageIdList, model.AccountNo);
                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_NO_SyncReadNotification");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách filter cho lịch sự kiện
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("SM_EV_SearchFilterList")]
        [ResponseType(typeof(SuccessModel<SearchEventFilterResult>))]
        [Note("Màn hình Tin tức > Lịch sự kiện")]
        public async Task<IHttpActionResult> SM_EV_SearchFilterList(SearchEventFilterRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_EV_SearchFilterList");
                SetLanguage(lang);


                var data = await _systemService.GetEventFilterData(model.AccountNo);

                var dataRet = new SuccessModel<SearchEventFilterResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "SM_EV_SearchFilterList");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_EV_SearchFilterList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách lịch sự kiện
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("SM_EV_EventList")]
        [ResponseType(typeof(SuccessListModel<EventListResult>))]
        [Note("Màn hình Tin tức > Lịch sự kiện")]
        public async Task<IHttpActionResult> SM_EV_EventList(EventListRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_EV_EventList");
                SetLanguage(lang);

                var data = await _systemService.GetEventList(model.PageIndex, model.PageSize, model.BondCode,
                    model.EventType, model.AccountNo);

                var dataRet = new SuccessListModel<EventListResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "SM_EV_EventList");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_EV_EventList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy chi tiết lịch sự kiện
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("SM_EV_EventDetail")]
        [ResponseType(typeof(SuccessModel<EventDetailResult>))]
        [Note("Màn hình Tin tức > Lịch sự kiện")]
        public async Task<IHttpActionResult> SM_EV_EventDetail(EventDetailRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_EV_EventDetail");
                SetLanguage(lang);

                var data = await _systemService.GetEventDetail(model.EventID);
                var dataRet = new SuccessModel<EventDetailResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "SM_EV_EventDetail");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_EV_EventDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Gửi SMS
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_ME_SendSms")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("MH Đăng ký Soft OTP")]
        public async Task<IHttpActionResult> SM_ME_SendSms(SendSmsRequest model, string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_ME_SendSms");
                SetLanguage(lang);

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var retInt = await _systemService.SendSms(model.UserId, model.Message, model.ServiceCode);

                if (retInt > 0)
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = RetCodeConst.MSMME_E001_MOBILE_PHONE_NOT_EXIST,
                        RetMsg = ResourceFile.SystemModule.MSMME_001
                    };
                    LogResult(log, errorModel, model.UserId, "SM_ME_SendSms");
                    return Ok(errorModel);
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_ME_SendSms");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        /// Gửi SMS
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_ME_SendSms_02")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("MH Đăng ký Soft OTP")]
        public async Task<IHttpActionResult> SM_ME_SendSms_02(SendSmsRequest_02 model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                log4net.LogicalThreadContext.Properties["AccountNo"] = "SMS";
                log4net.Config.XmlConfigurator.Configure();
                log.Info("Api=SM_ME_SendSms_02 \t Request=" + model.Mobile + " " + model.Message);

                var retInt = await _systemService.SendSms_02(model.Mobile, model.Message);

                if (retInt > 0)
                {
                    return Ok(new SuccessModel());
                }

                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
            catch (Exception ex)
            {
                log.Error("Api=SM_ME_SendSms_02 \t" + ex.Message);
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }


        /// <summary>
        /// Gửi OTP qua email
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_ME_SendOTPEmail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("MH Đăng ký Soft OTP")]
        public async Task<IHttpActionResult> SM_ME_SendOTPEmail(SendOTPEmailRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "SM_ME_SendOTPEmail");

                var ret = await _systemService.SendOTPEmail(model.Otp, model.CustCode);

                if (ret)
                {
                    var retModel = new SuccessModel<bool>()
                        { RetCode = RetCodeConst.SUCCESS_CODE, RetData = true };
                    LogAnonymousResult(log, retModel, "SM_ME_SendOTPEmail");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "SM_ME_SendOTPEmail");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }


        /// <summary>
        ///  Quên mật khẩu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_FP_ForgotPassword")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("MH đăng nhập > Quên mật khẩu")]
        public async Task<IHttpActionResult> SM_FP_ForgotPassword(ForgotPasswordRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_FP_ForgotPassword");
                SetLanguage(lang);

                var retInt = await _systemService.ForgotPassword(model.UserId, model.CardId);

                if (retInt == 1)
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = RetCodeConst.MSMCA_E001_FORGOTPASSWORD_INCORRECT,
                        RetMsg = ResourceFile.SystemModule.MSMVA_002
                    };
                    LogResult(log, errorModel, model.UserId, "SM_FP_ForgotPassword");
                    return Ok(errorModel);
                }

                if (retInt == 2)
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = RetCodeConst.MSMMP_001_ACCOUNT_NOT_FOUND,
                        RetMsg = ResourceFile.SystemModule.MSMVA_002
                    };
                    LogResult(log, errorModel, model.UserId, "SM_FP_ForgotPassword");
                    return Ok(errorModel);
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_FP_ForgotPassword");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }


        /// <summary>
        ///  Quên mã PIN đặt lệnh
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_FP_ForgotPin")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Check Email > Nhận mã PIN")]
        public async Task<IHttpActionResult> SM_FP_ForgotPin(ForgotPINRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_FP_ForgotPin");
                SetLanguage(lang);

                var retInt = await _systemService.ForgotPIN(model.UserId, model.CardId);

                if (retInt > 0)
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = RetCodeConst.MSMCA_E001_RESETPIN_INCORRECT,
                        RetMsg = ResourceFile.SystemModule.MSMCA_001
                    };
                    LogResult(log, errorModel, model.UserId, "SM_FP_ForgotPin");
                    return Ok(errorModel);
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_FP_ForgotPin");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }


        /// <summary>
        ///  Kích hoạt tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_VA_VerifyAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Check Email > Nhận Email kích hoạt")]
        public async Task<IHttpActionResult> SM_VA_VerifyAccount(VerifyAccountRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_VA_VerifyAccount");
                SetLanguage(lang);

                var retInt = await _systemService.VerifyAccount(model.UserId, model.Email);

                if (retInt > 0)
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = RetCodeConst.MSVA_E001_VERIFYACCOUNT_INCORRECT,
                        RetMsg = ResourceFile.SystemModule.MSMVA_001
                    };
                    LogResult(log, errorModel, model.UserId, "SM_VA_VerifyAccount");
                    return Ok(errorModel);
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_VA_VerifyAccount");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        ///  Kích hoạt nhận mật khẩu/PIN
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_CC_ConfirmCode")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Check Email > Kích hoạt nhận mật khẩu/PIN")]
        public async Task<IHttpActionResult> SM_CC_ConfirmCode(ConfirmCodeRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_CC_ConfirmCode");
                SetLanguage(lang);

                var retInt = await _systemService.SendConfirmCode(model.ConfirmCode);

                if (retInt > 0)
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = RetCodeConst.MSMCA_E001_CONFIRMCODE_INCORRECT,
                        RetMsg = ResourceFile.SystemModule.MSMCA_001
                    };
                    LogResult(log, errorModel, "confirm-code", "SM_CC_ConfirmCode");
                    return Ok(errorModel);
                }

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                model.UserId = "confirm-code";
                LogException(log, model, ex, "SM_CC_ConfirmCode");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        /// Hàm lấy ngày giao dịch hiện tại của hệ thống
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_TD_NextTradeDate")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CurrentTradeDateResult>))]
        [Note("Hàm lấy ngày giao dịch tiếp theo của hệ thống", true)]
        public async Task<IHttpActionResult> SM_TD_NextTradeDate(CurrentTradeDateRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_TD_NextTradeDate  ");

                var nextDate = await _systemService.GetNextTradeDateByDate(DateTime.Today);

                return Ok(new SuccessModel<CurrentTradeDateResult>
                    {
                        RetData = new CurrentTradeDateResult
                        {
                            CurTradeDate = nextDate.ToString("dd/MM/yyyy")
                        }
                    }
                );
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_TD_NextTradeDate");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        ///  Inactive tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_IA_InactiveAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Chức năng Inactive Account")]
        public async Task<IHttpActionResult> SM_IA_InactiveAccount(BaseRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_IA_InactiveAccount");
                SetLanguage(lang);

                var retData = await _systemService.InactiveAccount(model.UserId);
                if (retData > 0)
                    return Ok(new SuccessModel());

                var errorModel = new ErrorModel
                {
                    RetCode = RetCodeConst.MCTCI_E001_CUSTINFO_NOT_FOUND,
                    RetMsg = ResourceFile.CustomerModule.MCTCI_001
                };
                return Ok(errorModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_IA_InactiveAccount");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        /// Get QR Zalo
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GE_CC_GetQRZalo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<QRCodeResult>))]
        [Note("Chức năng Inactive Account")]
        public async Task<IHttpActionResult> GE_CC_GetQRZalo(QRCodeRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "GE_CC_GetQRZalo");
                SetLanguage(lang);

                var retData = await _qrService.GetQRCode(model);
                var retModel = new SuccessModel<QRCodeResult>()
                    { RetCode = RetCodeConst.SUCCESS_CODE, RetData = retData };
                LogAnonymousResult(log, retModel, "GetQRCode");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GE_CC_GetQRZalo");
                return Ok(new ErrorModel(RetCodeConst.ERR999_SYSTEM_ERROR));
            }
        }

        /// <summary>
        /// Show Banner Tvsi
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("IM_GE_GetBannerTvsi")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<string>))]
        [Note("Đánh dấu noti đã đọc")]
        public async Task<IHttpActionResult> IM_GE_GetBannerTvsi(GetBannerTvsiRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "IM_GE_GetBannerTvsi");
                SetLanguage(lang);

                var data = await _systemService.GetBannerTvsiInfo();

                if (data != null && data.Count() > 0)
                {
                    LogAnonymousRequest(log, model, "IM_GE_GetBannerTvsi");
                    return Ok(new SuccessListModel<string>()
                    {
                        RetCode = RetCodeConst.SUCCESS_CODE,
                        RetData = data
                    });
                }

                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,
                    RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "IM_GE_GetBannerTvsi");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}