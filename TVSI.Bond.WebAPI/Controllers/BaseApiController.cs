﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using log4net;
using Newtonsoft.Json;
using Serilog;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.System;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// Base API Controller
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public abstract class BaseApiController : ApiController
    {
        /// <summary>
        /// Validate dữ liệu request có hợp lệ hay không
        /// </summary>
        /// <param name="requestModel">request model</param>
        /// <param name="lang">Ngôn ngữ đang sử dụng</param>
        /// <returns></returns>
        protected ErrorModel ValidateRequestModel(object requestModel, string lang)
        {
            if (requestModel.GetType() == typeof(UserLoginRequest))
            {
               return ValidateUserLoginRequest((UserLoginRequest) requestModel, lang);
            }

            if (requestModel.GetType() == typeof (ChangePasswordRequest))
            {
                return ValidateChangePasswordRequest((ChangePasswordRequest) requestModel, lang);
            }

            if (requestModel.GetType() == typeof (ChangePinRequest))
            {
                return ValidateChangePinRequest((ChangePinRequest) requestModel, lang);
            }

            if (requestModel.GetType() == typeof (NotificationRequest))
            {
                return ValidateNotificationRequest((NotificationRequest)requestModel, lang);
            }

            if (requestModel.GetType() == typeof (SyncNotificationRequest))
            {
                return ValidateSyncNotificationRequest((SyncNotificationRequest) requestModel, lang);
            }
            return null;
        }

        /// <summary>
        /// Set ngôn ngữ trả về của API
        /// </summary>
        /// <param name="lang"></param>
        protected void SetLanguage(string lang)
        {
            if (CommonConst.LANGUAGE_EN.Equals(lang))
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-GB");
            }
        }

        #region "Log Request/Result Data"

        /// <summary>
        /// Log Request
        /// </summary>
        /// <param name="log"></param>
        /// <param name="requestModel"></param>
        /// <param name="apiName"></param>
        protected void LogRequest(ILog log, BaseRequest requestModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            
            var requestJson = JsonConvert.SerializeObject(requestModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log4net.LogicalThreadContext.Properties["AccountNo"] = requestModel.UserId;
            //log4net.LogicalThreadContext.Properties["ApiName"] = apiName;
            //log4net.Config.XmlConfigurator.Configure();
            //log.Info("CustCode=" + requestModel.UserId + "\tApi=" + apiName + "\t Request=" + requestJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + requestModel.UserId).Information("API=" + apiName + "\t Request=" + requestJson);
        }
        
        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <param name="log"></param>
        /// <param name="resultModel"></param>
        /// <param name="apiName"></param>
        protected void LogResult(ILog log, SuccessModel resultModel, string accountNo, string apiName)
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(resultModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log4net.LogicalThreadContext.Properties["AccountNo"] = accountNo;
            //log4net.Config.XmlConfigurator.Configure();
            //log.Info("CustCode=" + accountNo + "\tApi=" + apiName + "\t Response=" + resultJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + accountNo).Information("API=" + apiName + "\t Response=" + resultJson);
        }

        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="log"></param>
        /// <param name="resultModel"></param>
        /// <param name="apiName"></param>
        protected void LogResult<T>(ILog log, SuccessModel<T> resultModel, string accountNo, string apiName)
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(resultModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log4net.LogicalThreadContext.Properties["AccountNo"] = accountNo;
            //log4net.Config.XmlConfigurator.Configure();
            //log.Info("CustCode=" + accountNo + "\tApi=" + apiName + "\t Response=" + resultJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + accountNo).Information("API=" + apiName + "\t Response=" + resultJson);
        }

        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="log"></param>
        /// <param name="resultModel"></param>
        /// <param name="apiName"></param>
        protected void LogResult<T>(ILog log, SuccessListModel<T> resultModel, string accountNo, string apiName)
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(resultModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log4net.LogicalThreadContext.Properties["AccountNo"] = accountNo;
            //log4net.Config.XmlConfigurator.Configure();
            //log.Info("CustCode=" + accountNo + "\tApi=" + apiName + "\t Response=" + resultJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + accountNo).Information("API=" + apiName + "\t Response=" + resultJson);
        }

        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <param name="log"></param>
        /// <param name="errorModel"></param>
        /// <param name="apiName"></param>
        protected void LogResult(ILog log, ErrorModel errorModel, string accountNo, string apiName)
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(errorModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log4net.LogicalThreadContext.Properties["AccountNo"] = accountNo;
            //log4net.Config.XmlConfigurator.Configure();
            //log.Info("CustCode=" + accountNo + "\tApi=" + apiName + "\t Response=" + resultJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + accountNo).Information("API=" + apiName + "\t Response=" + resultJson);
        }
        
        
        /// <summary>
        /// Log lỗi exception
        /// </summary>
        /// <param name="log"></param>
        /// <param name="requestModel"></param>
        /// <param name="ex"></param>
        /// <param name="apiName"></param>
        protected void LogException(ILog log, BaseRequest requestModel, Exception ex, string apiName)
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var errorJson = JsonConvert.SerializeObject(requestModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log4net.LogicalThreadContext.Properties["AccountNo"] = requestModel.UserId;
            //log4net.Config.XmlConfigurator.Configure();
            //log.Error("CustCode=" + requestModel.UserId + "\tApiName=" + apiName + ":" + ex.Message + "\t ErrorData=" + errorJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + requestModel.UserId).Error("API=" + apiName 
                                    + "\t Message:" + ex.Message + "\t InnerMessage:" 
                                    + ex.InnerException + "\t RequestData=" + errorJson);
        }
        #endregion
        

        #region "Anonymous Log"
        /// <summary>
        /// Log Request gửi lên
        /// </summary>
        /// <param name="log"></param>
        /// <param name="requestModel"></param>
        /// <param name="apiName"></param>
        protected void LogAnonymousRequest(ILog log, BaseEkycRequest requestModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            //log4net.LogicalThreadContext.Properties["AccountNo"] = "Anonymous";
            //log4net.LogicalThreadContext.Properties["ApiName"] = apiName;

            //log4net.Config.XmlConfigurator.Configure();
            var requestJson = JsonConvert.SerializeObject(requestModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log.Info("Api=" + apiName + "\t Request=" + requestJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + "Anonymous").Information("API=" + apiName + "\t Request=" + requestJson);
        }
        
        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <param name="log"></param>
        /// <param name="resultModel"></param>
        /// <param name="apiName"></param>
        protected void LogAnonymousResult(ILog log, SuccessModel resultModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(resultModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log.Debug("Api=" + apiName + "\t Response=" + resultJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + "Anonymous").Information("API=" + apiName 
                                + "\t Response=" + resultJson);
        }

        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="log"></param>
        /// <param name="resultModel"></param>
        /// <param name="apiName"></param>
        protected void LogAnonymousResult<T>(ILog log, SuccessModel<T> resultModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(resultModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log.Debug("Api=" + apiName + "\t Response=" + resultJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + "Anonymous").Information("API=" + apiName
                                                                   + "\t Response=" + resultJson);
        }

        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="log"></param>
        /// <param name="resultModel"></param>
        /// <param name="apiName"></param>
        protected void LogAnonymousResult<T>(ILog log, SuccessListModel<T> resultModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(resultModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log.Debug("Api=" + apiName + "\t Response=" + resultJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + "Anonymous").Information("API=" + apiName
                                                                   + "\t Response=" + resultJson);
        }
        
        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <param name="log"></param>
        /// <param name="errorModel"></param>
        /// <param name="apiName"></param>
        protected void LogAnonymousResult(ILog log, ErrorModel errorModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(errorModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log.Debug("Api=" + apiName + "\t Response=" + resultJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + "Anonymous").Information("API=" + apiName
                                                                   + "\t Response=" + resultJson);
        }

        /// <summary>
        /// Log lỗi exception
        /// </summary>
        /// <param name="log"></param>
        /// <param name="requestModel"></param>
        /// <param name="ex"></param>
        /// <param name="apiName"></param>
        protected void LogAnonymousException(ILog log, BaseEkycRequest requestModel, Exception ex, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var errorJson = JsonConvert.SerializeObject(requestModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            //log.Error(apiName + ":" + ex.Message + "\t ErrorData=" + errorJson);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + "Anonymous").Error("API=" + apiName
                          + "\t Message:" + ex.Message + "\t InnerMessage:"
                          + ex.InnerException + "\t RequestData=" + errorJson);
        }
        #endregion
        
        #region "Validate All Model"

        /// <summary>
        /// Validate dữ liệu Login
        /// </summary>
        /// <param name="model"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        private ErrorModel ValidateUserLoginRequest(UserLoginRequest model, string lang)
        {
            if (string.IsNullOrEmpty(model.UserId))
                return new ErrorModel(RetCodeConst.ERR900_ISNULLOREMPTY, string.Empty, 
                    lang == CommonConst.LANGUAGE_VI ?  "Tên đăng nhập" : "UserName");

            if (string.IsNullOrEmpty(model.Password))
                return new ErrorModel(RetCodeConst.ERR900_ISNULLOREMPTY, string.Empty, 
                    lang == CommonConst.LANGUAGE_VI ? "Mật khẩu" : "Password");

            return null;
        }

        /// <summary>
        /// Validate dữ liệu yêu cầu thay đổi Password
        /// </summary>
        /// <param name="model"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        private ErrorModel ValidateChangePasswordRequest(ChangePasswordRequest model, string lang)
        {
            var err = ValidateBaseRequset(model, lang);

            if (err != null)
                return err;

            if (string.IsNullOrEmpty(model.OldPassword))
                return new ErrorModel(RetCodeConst.ERR900_ISNULLOREMPTY, string.Empty,
                    lang == CommonConst.LANGUAGE_VI ? "Mật khẩu cũ" : "Old Password");

            if (string.IsNullOrEmpty(model.NewPassword))
                return new ErrorModel(RetCodeConst.ERR900_ISNULLOREMPTY, string.Empty,
                    lang == CommonConst.LANGUAGE_VI ? "Mật khẩu mới" : "New Password");

            return null;
        }


        /// <summary>
        /// Validate dữ liệu yêu cầu thay đổi Pin
        /// </summary>
        /// <param name="model"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        private ErrorModel ValidateChangePinRequest(ChangePinRequest model, string lang)
        {
            var err = ValidateBaseRequset(model, lang);

            if (err != null)
                return err;

            if (string.IsNullOrEmpty(model.OldPin))
                return new ErrorModel(RetCodeConst.ERR900_ISNULLOREMPTY, string.Empty,
                    lang == CommonConst.LANGUAGE_VI ? "Mã Pin cũ" : "Old Pin");

            if (string.IsNullOrEmpty(model.NewPin))
                return new ErrorModel(RetCodeConst.ERR900_ISNULLOREMPTY, string.Empty,
                    lang == CommonConst.LANGUAGE_VI ? "Mã Pin mới" : "New Pin");

            return null;
        }

        private ErrorModel ValidateNotificationRequest(NotificationRequest model, string lang)
        {
            var err = ValidateBaseRequset(model, lang);

            return err;
        }

        private ErrorModel ValidateSyncNotificationRequest(SyncNotificationRequest model, string lang)
        {
            var err = ValidateBaseRequset(model, lang);

            if (err != null)
                return err;

            if (model.MessageIdList == null || model.MessageIdList.Count == 0)
                return new ErrorModel(RetCodeConst.ERR900_ISNULLOREMPTY, string.Empty,
                    lang == CommonConst.LANGUAGE_VI ? "Danh sách ID" : "ID List");

            return null;
        }

        #region "Validate BaseRequest"
        private ErrorModel ValidateBaseRequset(BaseRequest model, string lang)
        {
            if (string.IsNullOrEmpty(model.UserId))
                return new ErrorModel(RetCodeConst.ERR900_ISNULLOREMPTY, string.Empty,
                    lang == CommonConst.LANGUAGE_VI ? "Tên đăng nhập" : "UserName");

            if (string.IsNullOrEmpty(model.AccountNo))
                return new ErrorModel(RetCodeConst.ERR900_ISNULLOREMPTY, string.Empty,
                    lang == CommonConst.LANGUAGE_VI ? "Số tài khoản" : "AccountNo");

            return null;
        }
        #endregion
        

        #endregion

    }
}
