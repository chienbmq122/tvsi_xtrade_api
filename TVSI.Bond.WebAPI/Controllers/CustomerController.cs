﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.Customer;
using TVSI.Bond.WebAPI.Lib.Services;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.ResourceFile;

namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// Customer API
    /// </summary>
    public class CustomerController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(CustomerController));
        private ICustomerService _customerService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="customerService"></param>
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Lấy số dư tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CM_CB_CustCashBalance")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CustBalanceResult>))]
        [Note("Màn hình SPTC > Tab Tài sản")]
        public async Task<IHttpActionResult> CM_CB_CustCashBalance(CustBalanceRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_CB_CustCashBalance");
                SetLanguage(lang);

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var data = await _customerService.GetCustBalanceInfo(model.UserId);

                if (data != null)
                {
                    var dataRet = new SuccessModel<CustBalanceResult> {RetData = data};
                    LogResult(log, dataRet, model.UserId, "CM_CB_CustCashBalance");
                    return Ok(dataRet);
                }

                // Khong tim thay thong tin
                return Ok(new ErrorModel
                          {
                              RetCode = RetCodeConst.MCMCB_E001_CUSTBALANCE_NOT_FOUND, 
                              RetMsg = ResourceFile.CustomerModule.MCMCB_001
                          });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_CB_CustCashBalance");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin số dư theo loại tài sản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CM_CB_CustCashBalanceByAssetType")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CustAssetTypeResult>))]
        [Note(" Màn hình SPTC > Tab Tài sản > Biểu đồ > Chọn loại tài sản")]
        public async Task<IHttpActionResult> CM_CB_CustCashBalanceByAssetType(CustAssetTypeRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_CB_CustCashBalanceByAssetType");

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);


                var dataRet = await _customerService.GetCustAssetType(model.UserId, model.AssetType);

                var retModel = new SuccessModel<CustAssetTypeResult> {RetData = dataRet};
                LogResult(log, retModel, model.UserId, "CM_CB_CustCashBalanceByAssetType");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_CB_CustCashBalanceByAssetType");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin liên lạc của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CM_CI_CustInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CustInfoResult>))]
        [Note("Màn hình Thông tin tài khoản")]
        public async Task<IHttpActionResult> CM_CI_CustInfo(CustInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_CI_CustInfo");
                SetLanguage(lang);
                // Validate empty data of request model
                var errModel = ValidateRequestModel(model, lang);
                if (errModel != null)
                    return Ok(errModel);

                var data = await _customerService.GetCustInfo(model.UserId, model.AccountNo);
                if (data != null)
                {

                    var retModel = new SuccessModel<CustInfoResult> {RetData = data};
                    LogResult(log, retModel, model.UserId, "CM_CI_CustInfo");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel {
                    RetCode = RetCodeConst.MCTCI_E001_CUSTINFO_NOT_FOUND, 
                    RetMsg = ResourceFile.CustomerModule.MCTCI_001});
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_CI_CustInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin Email của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CM_CI_CustEmailInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CustEmailInfoResult>))]
        [Note("Màn hình Thông tin tài khoản > Email")]
        public async Task<IHttpActionResult> CM_CI_CustEmailInfo(CustEmailInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_CI_CustEmailInfo");
                SetLanguage(lang);

                var data = await _customerService.GetCustEmailInfo(model.UserId);

                var retModel = new SuccessModel<CustEmailInfoResult> {RetData = data};
                LogResult(log, retModel, model.UserId, "CM_CI_CustEmailInfo");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_CI_CustEmailInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin Điện thoại Contact Center của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CM_CI_CustPhoneInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CustPhoneInfoResult>))]
        [Note("Màn hình Thông tin tài khoản > Điện thoại Contact Center")]
        public async Task<IHttpActionResult> CM_CI_CustPhoneInfo(CustPhoneInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_CI_CustPhoneInfo");
                SetLanguage(lang);

                var data = await _customerService.GetCustPhoneInfo(model.UserId);
                var retModel = new SuccessModel<CustPhoneInfoResult> {RetData = data};
                LogResult(log, retModel, model.UserId, "CM_CI_CustPhoneInfo");
                return Ok(retModel);

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_CI_CustPhoneInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin các ngân hàng đang Active của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CM_CI_CustBankInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<CustBankInfoResult>))]
        [Note("Màn hình Thông tin tài khoản > Thông tin ngân hàng")]
        public async Task<IHttpActionResult> CM_CI_CustBankInfo(CustBankInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_CI_CustBankInfo");
                SetLanguage(lang);

                var data = await _customerService.GetCustBankInfo(model.UserId);

                var retModel = new SuccessListModel<CustBankInfoResult> {RetData = data};
                LogResult(log, retModel, model.UserId, "CM_CI_CustBankInfo");
                return Ok(retModel);

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_CI_CustBankInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin tất cả các ngân hàng của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CM_CI_FullCustBankInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<FullCustBankInfoResult>))]
        [Note("Màn hình Thông tin tài khoản > Thông tin ngân hàng")]
        public async Task<IHttpActionResult> CM_CI_FullCustBankInfo(FullCustBankInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_CI_FullCustBankInfo");
                SetLanguage(lang);

                var data = await _customerService.GetFullCustBankInfo(model.UserId);

                var retModel = new SuccessListModel<FullCustBankInfoResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "CM_CI_FullCustBankInfo");
                return Ok(retModel);

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_CI_FullCustBankInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Cập nhật trạng thái Tài khoản ngân hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CM_CI_UpdateCustBankStatus")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình Thông tin tài khoản > Thông tin ngân hàng")]
        public async Task<IHttpActionResult> CM_CI_UpdateCustBankStatus(UpdateCustBankRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_CI_UpdateCustBankStatus");
                SetLanguage(lang);

                var data = await _customerService.UpdateCustBankStatus(model);

                if (data > 0)
                {
                    var retModel = new SuccessModel();
                    LogResult(log, retModel, model.UserId, "CM_CI_UpdateCustBankStatus");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.MCTCI_E001_CUSTINFO_NOT_FOUND,
                    RetMsg = CustomerModule.MCTCI_001
                });

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_CI_UpdateCustBankStatus");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách số tài khoản theo khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CM_AI_AccountNoList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<AccountNoListResult>))]
        [Note("Màn hình Thông tin tài khoản > Danh sách tài khoản")]
        public async Task<IHttpActionResult> CM_AI_AccountNoList(AccountNoListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_AI_AccountNoList");
                SetLanguage(lang);

                var data = await _customerService.GetAccountNoList(model.UserId);

                var retModel = new SuccessListModel<AccountNoListResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "CM_AI_AccountNoList");
                return Ok(retModel);

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_AI_AccountNoList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách số tài khoản theo khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CM_AI_SetDefaultAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình Thông tin tài khoản > Danh sách tài khoản")]
        public async Task<IHttpActionResult> CM_AI_SetDefaultAccount(DefaultAccountRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_AI_SetDefaultAccount");
                SetLanguage(lang);

                var data = await _customerService.SetDefaultAccount(model);

                if (data > 0)
                {
                    var retModel = new SuccessModel();
                    LogResult(log, retModel, model.UserId, "CM_AI_SetDefaultAccount");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.MCTCI_E001_CUSTINFO_NOT_FOUND,
                    RetMsg = CustomerModule.MCTCI_001
                });

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_AI_SetDefaultAccount");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Check tài khoản @
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CM_CK_CheckAlphaAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AlphaAccountResult>))]
        [Note("Màn hình Thông tin tài khoản > Danh sách tài khoản")]
        public async Task<IHttpActionResult> CM_CK_CheckAlphaAccount(AlphaAccountRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_CK_CheckAlphaAccount");
                SetLanguage(lang);

                var data = await _customerService.GetAlphaAccountInfo(model.AccountNo);

                var retModel = new SuccessModel<AlphaAccountResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "CM_CK_CheckAlphaAccount");
                return Ok(retModel);

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_CK_CheckAlphaAccount");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}
