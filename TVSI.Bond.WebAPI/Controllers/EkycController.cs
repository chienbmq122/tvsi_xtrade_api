﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.Ekyc;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Lib.Services;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Utility.EkycParams;
using TVSI.Bond.WebAPI.ResourceFile;


namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// Bond API
    /// </summary>
    public class EkycController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(EkycController));
        private IEkycService _ekycService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="ekycService"></param>
        public EkycController(IEkycService ekycService)
        {
            _ekycService = ekycService;
        }


        /// <summary>
        /// Phương thức này dùng để Check CMND/CCCD có tồn tại trong hệ thông
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CK_CheckCardIdExisted")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Sử dụng khi ấn xác nhận tại màn hình mở tài khoản trực tuyến bằng EKYC")]
        public async Task<IHttpActionResult> EK_CK_CheckCardIdExisted([FromBody]CardIdExsitedRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_CK_CheckCardIdExisted");

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel
                              {
                                  RetCode = EkycRetCodeConst.EK999_MISSING_DATA,
                                  RetMsg = EkycRetMsgConst.EKRetMsg_EK999
                              });
                }
                
                var isExistCardId = await _ekycService.CardIDExsited(model.CardId);
                if (isExistCardId)
                {
                    return Ok(new SuccessModel {RetCode = RetCodeConst.SUCCESS_CODE});
                }
                return Ok(new ErrorModel
                {
                    RetCode = EkycRetCodeConst.EK904_CARDID_EXISTED,
                    RetMsg = EkycRetMsgConst.EKRetMsg_EK904
                });
                
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_CK_CheckCardIdExisted");
                return Ok(new ErrorModel
                          {
                              RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,
                              RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR
                          });
            }
        }


        /// <summary>
        /// Phương thức này dùng để Check CMND/CCCD có tồn tại trong hệ thông
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CK_CheckCardIdExisted_v2")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Sử dụng khi ấn xác nhận tại màn hình mở tài khoản trực tuyến bằng EKYC")]
        public async Task<IHttpActionResult> EK_CK_CheckCardIdExisted_v2([FromBody] CardIdExsitedRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_CK_CheckCardIdExisted_v2");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = EkycRetCodeConst.EK999_MISSING_DATA,
                        RetMsg = EkycRetMsgConst.EKRetMsg_EK999
                    });
                }

                var cardId = await _ekycService.CardIDExsited_v2(model.CardId);
                if (cardId == 0)
                    return Ok(new SuccessModel { RetCode = RetCodeConst.SUCCESS_CODE });
                if (cardId == 1)
                {
                    var custcode = await _ekycService.GetCustCodeByCardId(model.CardId);
                    if (!string.IsNullOrEmpty(custcode))
                    {
                        return Ok(new ErrorModel
                        {
                            RetCode = "EK_965",
                            RetMsg = EkycRetMsgConst.EKRetMsg_EK_OpenInfo_AccountExst.Replace("{0}",custcode)
                        });
                    }
                }
                return Ok(new ErrorModel
                {
                    RetCode = EkycRetCodeConst.EK904_CARDID_EXISTED,
                    RetMsg = EkycRetMsgConst.EKRetMsg_EK904
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_CK_CheckCardIdExisted");
                return Ok(new ErrorModel
                          {
                              RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,
                              RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR
                          });
            }
        }

        /// <summary>
        /// Phương thức này dùng để mở TK khách hàng bằng EKYC
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CA_CreateCustomer")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Sử dụng khi ấn xác nhận tại màn hình mở tài khoản trực tuyến bằng EKYC")]
        public async Task<IHttpActionResult> EK_CA_CreateCustomer([FromBody]EkycSaveUserInfoRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_CA_CreateCustomer");
                var validate = new List<EkycRegisterErrors>
                               {
                                   new EkycRegisterErrors
                                   {
                                       RetErrorCode = EkycRetCodeConst.EK999_MISSING_DATA,
                                       RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK999
                                   }
                               };

                if (!ModelState.IsValid)
                {
                    return Ok(new EkycErrorListModel<EkycRegisterErrors>
                        {
                            RetCode = EkycRetCodeConst.EK999_MISSING_DATA,
                            RetErrorList = validate
                        });
                }

                var validation = await _ekycService.ValidationFormRegisterUser(model);
                var retMsgList = validation.ToList();
                if (retMsgList.Count > 0)
                {
                    return Ok(new EkycErrorListModel<EkycRegisterErrors>
                        {RetCode = EkycRetCodeConst.EK900_VALIDATION_DATA, RetErrorList = retMsgList});
                }
                var dataRet = await _ekycService.Tvsi_sDangKyTaiKhoanEkyc(model);
                if (dataRet)
                {
                    var retModel = new EkycSuccessModel
                                   {
                                       RetCode = RetCodeConst.SUCCESS_CODE,
                                       RetMsg = EkycRetMsgConst.EKRetMsg_EK000
                                   };
                    LogAnonymousResult(log, retModel, "EK_CA_CreateCustomer");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK1000_DATA_SAVEFAIL, RetMsg = EkycRetMsgConst.EKRetMsg_EK1000});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_CA_CreateCustomer");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }    
        
        
        /// <summary>
        /// Phương thức này dùng để mở TK khách hàng bằng  version 2
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CA_CreateCustomer_v2")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Sử dụng khi ấn xác nhận tại màn hình mở tài khoản trực tuyến bằng EKYC")]
        public async Task<IHttpActionResult> EK_CA_CreateCustomer_v2([FromBody]EkycSaveUserInfoRequest_v2 model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_CA_CreateCustomer_v2");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        RetMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var validation = await _ekycService.ValidationFormRegisterUser_v2(model);
                var retMsgList = validation.ToList();
                if (retMsgList.Count > 0)
                {
                    return Ok(new EkycErrorListModel<EkycRegisterErrors>
                        {RetCode = EkycRetCodeConst.EK900_VALIDATION_DATA, RetErrorList = retMsgList});
                }
                var dataRet = await _ekycService.Tvsi_sDangKyTaiKhoanEkyc_v2(model);
                if (dataRet)
                {
                    var retModel = new EkycSuccessModel
                                   {
                                       RetCode = RetCodeConst.SUCCESS_CODE,
                                       RetMsg = EkycRetMsgConst.EKRetMsg_EK000
                                   };
                    LogAnonymousResult(log, retModel, "EK_CA_CreateCustomer_v2");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK1000_DATA_SAVEFAIL, RetMsg = EkycRetMsgConst.EKRetMsg_EK1000});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_CA_CreateCustomer_v2");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để kiểm tra update video xác thực chữ ký
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_UP_UpdateStatusVideo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Sử dụng khi ấn xác nhận tại màn hình mở tài khoản trực tuyến bằng EKYC")]
        public async Task<IHttpActionResult> EK_UP_UpdateStatusVideo([FromBody]UpdateVideoSignatureRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_UP_UpdateStatusVideo");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var saveUpdate = await _ekycService.UpdateStatusVideo(model);
                if (saveUpdate == 99)
                {
                    var data = await _ekycService.CheckUpdateStatusVideo(model.UserID);
                    if (data)
                    {
                        var retModel = new SuccessModel<bool>() {RetCode = RetCodeConst.SUCCESS_CODE, RetData = true};
                        LogAnonymousResult(log, retModel, "EK_UP_UpdateStatusVideo");
                        return Ok(retModel);
                    }
                }

                if (saveUpdate == -1)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = EkycRetCodeConst.EK_917_UPDATE_VIDEO, RetMsg = EkycRetMsgConst.EKRetMsg_EK917_ACCNUM
                    });
                }

                if (saveUpdate == -2)
                    return Ok(new ErrorModel()
                    {
                        RetCode = EkycRetCodeConst.EK_917_UPDATE_VIDEO, RetMsg = EkycRetMsgConst.EKRetMsg_EK917_SACEFAIL
                    });

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK_917_UPDATE_VIDEO, RetMsg = EkycRetMsgConst.EKRetMsg_EK917});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_UP_UpdateStatusVideo");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }


        /// <summary>
        /// Phương thức này dùng để lấy danh sách nơi cấp cmnd/cccd
        /// </summary>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_PI_GetListPlaceOfIssue")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetListPlaceOfIssueResult>))]
        [Note("Sử dụng khi mở tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> EK_PI_GetListPlaceOfIssue(string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {         
                LogAnonymousRequest(log, null, "EK_PI_GetListPlaceOfIssue");

                var data = await _ekycService.GetListPlaceOfIssue();
                if (data.Count() > 0)
                {
                    var retModel = new SuccessListModel<GetListPlaceOfIssueResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = data};
                    LogAnonymousResult(log, retModel, "EK_PI_GetListPlaceOfIssue");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, null, ex, "EK_PI_GetListPlaceOfIssue");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        

        /// <summary>
        /// Phương thức này dùng để Confirm OTP Ekyc Mobile trên Website
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CF_ConFirmOPTEkycM")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Sử dụng khi mở tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> EK_CF_ConFirmOPTEkycM(EkycConfirmModelRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, null, "EK_CF_ConFirmOPTEkycM");
                if (model.codem.Length < 30)
                {
                    return Ok(new ErrorModel()
                    {
                        RetMsg = EkycRetMsgConst.EKRetMsg_EK_CONTENT_LENGTH,
                        RetCode = EkycRetCodeConst.EK_930_ADDRESS_LENGTH32
                    });
                }
                var data = await _ekycService.ConfirmEkycMobile(model.codem);
                if (data)
                {
                    var retModel = new SuccessModel<bool>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = true};
                    LogAnonymousResult(log, retModel, "EK_CF_ConFirmOPTEkycM");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = EkycRetCodeConst.EK_999_CONFIRM_EMAIL, RetMsg = EkycRetMsgConst.EKRetMsg_EK_CONFIRM_999
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, null, ex, "EK_CF_ConFirmOPTEkycM");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để check sale ID
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_PI_CheckSaleID")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<string>))]
        [Note("Sử dụng khi mở tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> EK_PI_CheckSaleID(OtherReRequest model, string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, null, "EK_PI_CheckSaleID");

                var data = await _ekycService.GetSaleID(model.SaleId.Substring(0, 4));
                if (!string.IsNullOrEmpty(data))
                {
                    var retModel = new SuccessModel<string>()
                        {RetCode = EkycRetCodeConst.EK_SALEID_TRUE, RetData = data.Substring(0, 4)};
                    LogAnonymousResult(log, retModel, "EK_PI_CheckSaleID");
                    return Ok(retModel);
                }

                return Ok(new EkycClientError()
                    {RetCode = EkycRetCodeConst.EK906_SALEID_NOT_EXISTED, RetMsg = EkycRetMsgConst.EKRetMsg_EK906});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, null, ex, "EK_PI_CheckSaleID");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }


        /// <summary>
        /// Phương thức này dùng để lấy danh sách các chi nhánh TVSI
        /// </summary>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_BR_GetNearestBranch")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<TvsiBranch>))]
        [Note("Sử dụng khi mở tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> EK_BR_GetNearestBranch(string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, null, "EK_BR_GetNearestBranch");

                var data = await Task.FromResult(EkycUtils.BranchListTvsi());
                if (data.Count > 0)
                {
                    var retModel = new SuccessListModel<TvsiBranch>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = data};
                    LogAnonymousResult(log, retModel, "EK_BR_GetNearestBranch");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, null, ex, "EK_BR_GetNearestBranch");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy danh sách các ngân hàng
        /// </summary>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_BA_GetBankList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<BankModel>))]
        [Note("Sử dụng khi mở tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> EK_BA_GetBankList(string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, null, "EK_BA_GetBankList");
                var data = await _ekycService.GetBankList();
                if (data.Count() > 0)
                {
                    var retModel = new SuccessListModel<BankModel> {RetData = data};
                    LogAnonymousResult(log, retModel, "EK_BA_GetBankList");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, null, ex, "EK_BA_GetBankList");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để compare thông tin ekyc khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CE_CompareEkycCust")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Sử dụng khi thay đổi thông tin tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> EK_CE_CompareEkycCust(CompareInfoCustRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_CE_CompareEkycCust");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }
                var result = await _ekycService.CompareEkycCust(model);
                if (result != null)
                {
                    if (result.Count() > 0)
                    {
                        return Ok(new EkycErrorListModel<EkycCompareNotSameResult>()
                        {
                            RetCode = EkycRetCodeConst.EK_918_INFONOTSAME,
                            RetErrorList = result
                        });
                    }
                    var retModel = new SuccessModel<bool>
                    {
                        RetCode = EkycRetCodeConst.SUCCESS_CODE,
                        RetData = true
                    };
                    LogAnonymousResult(log, retModel, "EK_CE_CompareEkycCust");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = EkycRetCodeConst.EK_919_ACCNOTFOUND,
                    RetMsg = EkycRetMsgConst.EKRetMsg_EK919_ACCNOTFOUND
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, null, ex, "EK_CE_CompareEkycCust");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        /// <summary>
        /// Phương thức này dùng để thay đổi thông tin khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CI_ChangeInfoCust")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Sử dụng khi thay đổi thông tin tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> EK_CI_ChangeInfoCust(ChangeUserInfoRequest model,string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_CI_ChangeInfoCust");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }
                if (model.FieldChange != null)
                {
                    var result = await _ekycService.ChangeDataCustInfo(model);
                    if (result)
                    {
                        var retModel = new SuccessModel<bool>()
                            {RetCode = EkycRetCodeConst.SUCCESS_CODE, RetData = true};
                        LogAnonymousResult(log, retModel, "EK_CI_ChangeInfoCust");
                        return Ok(retModel);
                    }
                }
                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK_998_CHANGEINFOCUST, RetMsg = EkycRetMsgConst.EKRetMsg_EK_CHANGEINFOCUST_998});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, null, ex, "EK_CI_ChangeInfoCust");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }


        /// <summary>
        /// Phương thức này dùng để thay đổi thông tin khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_CC_CheckChangeInfoCust")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Sử dụng khi thay đổi thông tin tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> CI_CC_CheckChangeInfoCust(FieldChangeRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_CC_CheckChangeInfoCust");

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }
                
                var result = await _ekycService.CheckChangeInfoCust(model);
                if (!result)
                {
                    var retModel = new SuccessModel<bool>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = true};
                    LogAnonymousResult(log, retModel, "CI_CC_CheckChangeInfoCust");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = EkycRetCodeConst.EK_999_CHANGEINFOPENDING,
                    RetMsg = EkycRetMsgConst.EKRetMsg_EK_CHANGEPENDING_999
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, null, ex, "CI_CC_CheckChangeInfoCust");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }    
        
        /// <summary>
        /// Phương thức này dùng để thay đổi thông tin DỊCH VỤ GDTT
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CI_GetPhoneCust")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetPhoneCustResult>))]
        [Note("Sử dụng khi thay đổi thông tin tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> EK_CI_GetPhoneCust(GetUserInfoRequest model,string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_CI_GetPhoneCust");

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }
                var data = await _ekycService.GetPhoneCust(model.UserID);
                if (data != null)
                {
                    var retModel = new SuccessModel<GetPhoneCustResult>(){ RetCode = EkycRetCodeConst.SUCCESS_CODE, RetData = data};
                    LogAnonymousResult(log, retModel, "EK_CI_GetPhoneCust");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK_998_CHANGEINFOCUST, RetMsg = EkycRetMsgConst.EKRetMsg_EK_CHANGEINFOCUST_998});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, null, ex, "EK_CI_GetPhoneCust");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }


        /// <summary>
        /// Phương thức này dùng để thay đổi thông tin SMS
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CI_ChangePhoneCust")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Sử dụng khi thay đổi thông tin tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> EK_CI_ChangePhoneCust(ChangePhoneCust model,string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_CI_ChangePhoneCust");

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                /*if (!string.IsNullOrEmpty(model.SmsPhoneReceive))
                {
                    var validPhone = await _ekycService.GetPhoneExisted(model.UserID,model.SmsPhoneReceive);
                    if (validPhone == 1)
                    {
                        return Ok(new EkycSuccessModel<bool> { RetCode =EkycRetCodeConst.EK_001_DATAEXISTED, RetData  = false,
                            RetMsg = EkycRetMsgConst.EKRetMsg_EK_PHONE01.Replace("{Phone}",model.SmsPhoneReceive)});

                    }
                }

                if (!string.IsNullOrEmpty(model.SmsPhoneCancel))
                {
                    var validPhone = await _ekycService.GetPhoneExisted(model.UserID,model.SmsPhoneReceive);
                    if (validPhone == 1)
                    {
                        return Ok(new EkycSuccessModel<bool> { RetCode =EkycRetCodeConst.EK_001_DATAEXISTED, RetData  = false,
                            RetMsg = EkycRetMsgConst.EKRetMsg_EK_PHONE01.Replace("{Phone}",model.SmsPhoneCancel)});

                    }
                }*/
                var data = await _ekycService.ChangePhoneCust(model);
                if (data)
                {
                    var retModel = new EkycSuccessModel<bool> { RetCode = EkycRetCodeConst.SUCCESS_CODE,RetMsg = EkycRetMsgConst.EKRetMsg_EK_SUCCESS,RetData = true};
                    LogAnonymousResult(log, retModel, "EK_CI_ChangePhoneCust");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK_998_CHANGEINFOCUST, RetMsg = EkycRetMsgConst.EKRetMsg_EK_CHANGEINFOCUST_998});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, null, ex, "EK_CI_ChangePhoneCust");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy danh sách các Tỉnh Thành
        /// </summary>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_BP_GetProvinceList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<ProvinceModelResult>))]
        [Note("Sử dụng khi mở tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> EK_BP_GetProvinceList(string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, null, "EK_BP_GetProvinceList");

                var data = await _ekycService.GetProvinceList();
                if (data.Count() > 0)
                {
                    var retModel = new SuccessListModel<ProvinceModelResult>() {RetData = data};
                    LogAnonymousResult(log, retModel, "EK_BP_GetProvinceList");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, null, ex, "EK_BP_GetProvinceList");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }
        

        /// <summary>
        /// Phương thức này dùng để lấy danh sách chi nhánh ngân hàng theo mã ngân hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_BA_GetBankBranchList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<BranchInfoResult>))]
        [Note("Sử dụng khi mở tài khoản bằng EKYC")]
        public async Task<IHttpActionResult> EK_BA_GetBankBranchList(GetBankBranchListRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_BA_GetBankBranchList");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.GetSubBranchList(model.BankNo);
                if (dataRet.Count() > 0)
                {
                    var retModel = new SuccessListModel<BranchInfoResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "EK_BA_GetBankBranchList");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_BA_GetBankBranchList");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy thông tin hiện hữu của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CM_GetUserInformation")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<UserInfoResult>))]
        [Note("Dành cho màn hình bổ sung thông tin Ekyc")]
        public async Task<IHttpActionResult> EK_CM_GetUserInformation(GetUserInfoRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_CM_GetUserInformation");
                var dataRet = await _ekycService.GetInfoCustomer(model);
                if (dataRet != null)
                {
                    var retModel = new SuccessModel<UserInfoResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "EK_CM_GetUserInformation");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                {
                    RetCode = EkycRetCodeConst.EK905_ACCOUNT_NOREGISTER, RetMsg = EkycRetMsgConst.EKRetMsg_EK905
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_CM_GetUserInformation");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy lịch sử Video Call của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_VC_GetLogVideoCallHist")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<VideoCallHistInfoRequest>))]
        [Note("Dành cho màn hình lịch Video Call Ekyc")]
        public async Task<IHttpActionResult> EK_VC_GetLogVideoCallHist(VideoCallHistRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_VC_GetLogVideoCallHist");
                var dataRet = await _ekycService.GetVideoCallHist(model);
                if (dataRet.Count() > 0)
                {
                    var retModel = new SuccessListModel<VideoCallHistInfoRequest>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "EK_VC_GetLogVideoCallHist");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_VC_GetLogVideoCallHist");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để đặt lịch Video Call cho khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_VC_VideoCallSchedule")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Dành cho màn hình đặt lịch Video Call Ekyc")]
        public async Task<IHttpActionResult> EK_VC_VideoCallSchedule(VideoCallScheduleRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_VC_VideoCallSchedule");
                var dataRet = await _ekycService.sDatLichVideoCallEkyc(model);
                if (dataRet)
                {
                    var retModel = new SuccessModel() {RetCode = RetCodeConst.SUCCESS_CODE};
                    LogAnonymousResult(log, retModel, "EK_VC_VideoCallSchedule");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK1000_DATA_SAVEFAIL, RetMsg = EkycRetMsgConst.EKRetMsg_EK1000});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_VC_VideoCallSchedule");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy lịch sử đặt lịch Video Call của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_VC_VideoCallScheduleHist")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<VideoCallScheduleHistInfoResult>))]
        [Note("Dành cho màn hình đặt lịch Video Call Ekyc")]
        public async Task<IHttpActionResult> EK_VC_VideoCallScheduleHist(VideoCallHistRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_VC_VideoCallScheduleHist");
                var dataRet = await _ekycService.GetVideoCallScheduleHist(model);
                if (dataRet.Count() > 0)
                {
                    var retModel = new SuccessListModel<VideoCallScheduleHistInfoResult>()
                        {RetCode = EkycRetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "EK_VC_VideoCallScheduleHist");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_VC_VideoCallScheduleHist");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để  Update thông tin khi KH Video Call cho TVSI xong  
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_UD_UpdateLogHistVideoCall")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Dành cho màn hình > Video Call, Update thông tin khi KH Video Call cho TVSI xong")]
        public async Task<IHttpActionResult> EK_UD_UpdateLogHistVideoCall(UpdateStatusVideoCallRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_UD_UpdateLogHistVideoCall");
                var dataRet = await _ekycService.SUpdateStatusVideoCall(model);
                if (dataRet)
                {
                    var retModel = new SuccessModel() {RetCode = EkycRetCodeConst.SUCCESS_CODE};
                    LogAnonymousResult(log, retModel, "EK_UD_UpdateLogHistVideoCall");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK1000_DATA_SAVEFAIL, RetMsg = EkycRetMsgConst.EKRetMsg_EK1000});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_UD_UpdateLogHistVideoCall");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy lịch sử đặt lịch Video Call của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_VC_CancelVideoCallSchedule")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Phương thức này dùng để Hủy lệnh đặt video call từ KH")]
        public async Task<IHttpActionResult> EK_VC_CancelVideoCallSchedule(CancelVideoCallScheduleRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_VC_CancelVideoCallSchedule");
                var dataRet = await _ekycService.SCancelDatLichVideoCall(model);
                if (dataRet)
                {
                    var retModel = new SuccessModel() {RetCode = RetCodeConst.SUCCESS_CODE};
                    LogAnonymousResult(log, retModel, "EK_VC_CancelVideoCallSchedule");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK1000_DATA_SAVEFAIL, RetMsg = EkycRetMsgConst.EKRetMsg_EK1000});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_VC_CancelVideoCallSchedule");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy danh sách ngân hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_BM_BankAccountList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<BankInfoListResult>))]
        [Note("Dành cho màn hình > Thay đổi thông tin chuyển tiền > thông tin tài khoản ngân hàng")]
        public async Task<IHttpActionResult> CI_BM_BankAccountList(BankAccountRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_BM_BankAccountList");
                var dataRet = await _ekycService.GetBankAccountList(model.UserID);
                if (dataRet.Count() > 0)
                {
                    var retModel = new SuccessListModel<BankInfoListResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "CI_BM_BankAccountList");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CI_BM_BankAccountList");
                
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy chi tiết thông tin tài khoản Bank
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_BM_BankAccountDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BankDetailInfoResult>))]
        [Note("Dành cho màn hình > Thay đổi thông tin chuyển tiền > Thông tin tài khoản ngân hàng > Chi tiết")]
        public async Task<IHttpActionResult> CI_BM_BankAccountDetail(AccNoDetailRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_BM_BankAccountDetail");
                var dataRet = await _ekycService.GetBankAccountDetail(model);
                if (dataRet != null)
                {
                    var retModel = new SuccessModel<BankDetailInfoResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "CI_BM_BankAccountDetail");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CI_BM_BankAccountDetail");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để đăng ký tài khoản Bank
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_BM_RegistBankAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Dành cho màn hình >  Thay đổi thông tin chuyển tiền > Thông tin tài khoản ngân hàng > Thêm mới")]
        public async Task<IHttpActionResult> CI_BM_RegistBankAccount(RegisterBankRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_BM_RegistBankAccount");
                var statusContent = EkycStatusConts.EK_REG_MONEY_TRANSFER;
                var contentChange = EkycContentChangeConts.EK_DANGKYBANK;
                var dataRet =
                    await _ekycService.SLogThayDoiThongTinKhEkycRegisterBank(model, statusContent, contentChange);
                if (dataRet)
                {
                    var retModel = new SuccessModel() {RetCode = RetCodeConst.SUCCESS_CODE};
                    LogAnonymousResult(log, retModel, "CI_BM_RegistBankAccount");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK1000_DATA_SAVEFAIL, RetMsg = EkycRetMsgConst.EKRetMsg_EK1000});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CI_BM_RegistBankAccount");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy Hủy đăng ký tài khoản Bank
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_BM_DeleteBankAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Dành cho màn hình > Thay đổi thông tin chuyển tiền > Hủy đăng ký")]
        public async Task<IHttpActionResult> CI_BM_DeleteBankAccount(DeleteBankRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_BM_DeleteBankAccount");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var statusContent = EkycStatusConts.EK_REG_MONEY_TRANSFER;
                var contentChange = EkycContentChangeConts.EK_HUYBANK;
                var dataRet = await _ekycService.SLogThayDoiThongTinKhEkycDelBank(model, statusContent, contentChange);
                if (dataRet)
                {
                    var retModel = new SuccessModel() {RetCode = RetCodeConst.SUCCESS_CODE};
                    LogAnonymousResult(log, retModel, "CI_BM_DeleteBankAccount");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK1000_DATA_SAVEFAIL, RetMsg = EkycRetCodeConst.EK1000_DATA_SAVEFAIL});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CI_BM_DeleteBankAccount");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy danh sách chuyển tiền tài khoản đối ứng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_AI_InternalAccountList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<InternalAccountInfoResult>))]
        [Note("Dành cho màn hình > Thay đổi thông tin chuyển tiền  > Thông tin tài khoản đối ứng")]
        public async Task<IHttpActionResult> CI_AI_InternalAccountList(BankAccountRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_AI_InternalAccountList");
                var dataRet = await _ekycService.GetInternalAccountInfo(model);
                if (dataRet.Count() > 0)
                {
                    var retModel = new SuccessListModel<InternalAccountInfoResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "CI_AI_InternalAccountList");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CI_AI_InternalAccountList");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy chi tiết chuyển tiền tài khoản đối ứng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_AI_InternalAccountDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<InternalAccountDetailInfoResult>))]
        [Note("Dành cho màn hình > Thay đổi thông tin chuyển tiền > Thông tin tài khoản đối ứng > Chi tiết")]
        public async Task<IHttpActionResult> CI_AI_InternalAccountDetail(InternalAccountDetailRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_AI_InternalAccountDetail");

                var dataRet = await _ekycService.GetInternalAccountDetail(model);
                if (dataRet != null)
                {
                    var retModel = new SuccessModel<InternalAccountDetailInfoResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "CI_AI_InternalAccountDetail");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CI_AI_InternalAccountDetail");
                return Ok(new ErrorModel {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR , RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy hủy chuyển tiền tài khoản đối ứng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_AI_DeleteInternalAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Dành cho màn hình > Thay đổi thông tin chuyển tiền > Thông tin tài khoản đối ứng > Hủy đăng ký")]
        public async Task<IHttpActionResult> CI_AI_DeleteInternalAccount(InternalAccountDetailRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_AI_DeleteInternalAccount");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var statusContent = EkycStatusConts.EK_REG_MONEY_TRANSFER;
                var contentChange = EkycContentChangeConts.EK_HUYDANGKYTKDOIUNG;
                var dataRet =
                    await _ekycService.SLogThayDoiThongTinKhEkycDelInterAcc(model, statusContent, contentChange);
                if (dataRet)
                {
                    var retModel = new SuccessModel() {RetCode = RetCodeConst.SUCCESS_CODE};
                    LogAnonymousResult(log, retModel, "CI_AI_DeleteInternalAccount");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK1000_DATA_SAVEFAIL, RetMsg = EkycRetMsgConst.EKRetMsg_EK1000});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CI_AI_DeleteInternalAccount");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để đăng ký các dịch vụ của TVSI
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_RS_RegistCustomerService")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Dành cho màn hình > Thay đổi đăng ký dịch vụ GDTT")]
        public async Task<IHttpActionResult> CI_RS_RegistCustomerService(RegistCustomerServiceResult model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_RS_RegistCustomerService");
                var statusCT = EkycStatusConts.EK_REG_SERVICE_STATUS;
                var contentChange = EkycContentChangeConts.EK_DANGKYDICHVUGDTT;
                var dataRet = await _ekycService.SLogThayDoiThongTinKhEkyc(model, statusCT, contentChange);
                if (dataRet)
                {
                    var retModel = new SuccessModel() {RetCode = RetCodeConst.SUCCESS_CODE};
                    LogAnonymousResult(log, retModel, "CI_RS_RegistCustomerService");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK1000_DATA_SAVEFAIL, RetMsg = EkycRetMsgConst.EKRetMsg_EK1000});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CI_RS_RegistCustomerService");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Phương thức này dùng để lấy lịch sử Thay đổi thông tin
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_RS_ChangeInfoHist")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<RegistServiceHistInfoResult>))]
        [Note("Dành cho màn hình > Quản lý thay đổi thông tin")]
        public async Task<IHttpActionResult> CI_RS_ChangeInfoHist(ChangeInfoHistRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_RS_ChangeInfoHist");

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.GetChangeInfoHist(model);
                if (dataRet.Count() > 0)
                {
                    var retModel = new SuccessListModel<RegistServiceHistInfoResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "CI_RS_ChangeInfoHist");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CI_RS_ChangeInfoHist");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        /// <summary>
        /// Phương thức này dùng để lấy lịch sử Thay đổi thông tin
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_CA_GetImageByCardID")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetImageByCardIdResult>))]
        [Note("Dành cho màn hình > Quản lý thay đổi thông tin")]
        public async Task<IHttpActionResult> CI_CA_GetImageByCardID(GetImageByCardIdRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_CA_GetImageByCardID");

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.GetImageByCardID(model);
                if (dataRet != null)
                {
                    var retModel = new SuccessModel<GetImageByCardIdResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, new SuccessModel(), "CI_CA_GetImageByCardID");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = "Không tìm thấy thông tin"});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CI_CA_GetImageByCardID");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        } 
        
        
        
        /*/// <summary>
        /// Phương thức này dùng update card id (Chỉ dùng để test LƯU ý)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CI_UD_UpdateCardIdeKYC")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<RegistServiceHistInfoResult>))]
        [Note("Dành cho màn hình > Quản lý thay đổi thông tin")]
        public async Task<IHttpActionResult> CI_UD_UpdateCardIdeKYC(CardIdRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CI_UD_UpdateCardIdeKYC");

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.UpdateCardIdeKYC(model);
                if (dataRet)
                {
                    var retModel = new SuccessModel<bool>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = true};
                    LogAnonymousResult(log, retModel, "CI_UD_UpdateCardIdeKYC");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, RetMsg = EkycRetMsgConst.EKRetMsg_EK903});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CI_UD_UpdateCardIdeKYC");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }*/
        
        /// <summary>
        /// Phương thức này dùng để đăng ký tài khoản margin
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CI_CreateAccountMargin")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Dành cho màn hình > Quản lý thay đổi thông tin")]
        public async Task<IHttpActionResult> EK_CI_CreateAccountMargin(CreateAccountMarginRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "EK_CI_CreateAccountMargin");

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }
                var dataRet = await _ekycService.CreateAccountMargin(model);
                if (dataRet)
                {
                    var retModel = new SuccessModel<bool>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = true};
                    LogAnonymousResult(log, retModel, "EK_CI_CreateAccountMargin");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK1000_DATA_SAVEFAIL, RetMsg = EkycRetMsgConst.EKRetMsg_EK1000});
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "EK_CI_CreateAccountMargin");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        /// <summary>
        /// Phương thức dùng check tài khoản margin có được phép đăng ký
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CI_CheckAccountMargin")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CheckAccountMarginResult>))]
        [Note("Dành cho màn hình > Quản lý thay đổi thông tin")]
        public async Task<IHttpActionResult> EK_CI_CheckAccountMargin(BaseRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "EK_CI_CheckAccountMargin");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.CheckAccountMargin(model.UserId);
                if (dataRet != null)
                {
                    var retModel = new SuccessModel<CheckAccountMarginResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "EK_CI_CheckAccountMargin");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK_333_ACCOUNTMARGINEEXISTED, RetMsg = EkycRetMsgConst.EKRetMsg_EK_MARGIN});
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "EK_CI_CheckAccountMargin");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }    
             
        /// <summary>
        /// Phương thức dùng để hủy yêu cầu TĐTT eKYC
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CI_CancellChangeInfoCust")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Dành cho màn hình > Quản lý thay đổi thông tin")]
        public async Task<IHttpActionResult> EK_CI_CancellChangeInfoCust(CancellChangeInfoCustRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "EK_CI_CancellChangeInfoCust");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.CancellChangeInfoCust(model);
                if (dataRet != null)
                {
                    var retModel = new SuccessModel<bool>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = true};
                    LogAnonymousResult(log, retModel, "EK_CI_CancellChangeInfoCust");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                    {RetCode = EkycRetCodeConst.EK1000_DATA_SAVEFAIL, RetMsg = EkycRetMsgConst.EKRetMsg_EK1000});
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "EK_CI_CancellChangeInfoCust");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }  
        
        
        /// <summary>
        /// Phương thức dùng check trạng thái bổ sung hồ sơ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_AP_CheckAddProfile")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Dành cho màn hình thông tin tài khoản > Bổ sung video chữ ký")]
        public async Task<IHttpActionResult> EK_AP_CheckAddProfile(BaseRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "EK_AP_CheckAddProfile");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.CheckAddProfile(model);
                if (dataRet)
                {
                    var retModel = new SuccessModel<bool>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = true};
                    LogAnonymousResult(log, retModel, "EK_AP_CheckAddProfile");
                    return Ok(retModel);
                }
                return Ok(new EkycSuccessModel<bool>()
                    {
                        RetCode = EkycRetCodeConst.EK_555_CHECKADDPROFILE, 
                        RetMsg = EkycRetMsgConst.EKRetMsg_EK_SMS,
                        RetData = false});
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "EK_AP_CheckAddProfile");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }       
        
        
        /// <summary>
        /// Lấy thông tin của sale
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_GE_GetInfoSale")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetInfoUserIdResult>))]
        [Note("Dành cho màn hình thông tin tài khoản > Bổ sung video chữ ký")]
        public async Task<IHttpActionResult> EK_GE_GetInfoSale(GetInfoUserIdRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "EK_GE_GetInfoSale");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.GetInfoSale(model);
                if (dataRet != null)
                {
                    var retModel = new SuccessModel<GetInfoUserIdResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "EK_GE_GetInfoSale");
                    return Ok(retModel);
                }
                return Ok(new EkycSuccessModel<bool>()
                    {
                        RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, 
                        RetMsg = EkycRetMsgConst.EKRetMsg_EK903,
                        RetData = false});
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "EK_GE_GetInfoSale");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }      
        
        
        /// <summary>
        /// Lấy ảnh thay đổi tt eKYC
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// {
        ///    "CardID": "sample string 1"
        ///  }
        ///
        /// </remarks>
        [ActionName("EK_GE_GetImageChangeInfoCust")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetImageChangeInfoResult>))]
        [Note("Dành cho màn hình thông tin tài khoản > Bổ sung video chữ ký")]
        public async Task<IHttpActionResult> EK_GE_GetImageChangeInfoCust(GetImageChangeInfoCustRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_GE_GetImageChangeInfoCust");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.GetImageChangeInfoCust(model);
                if (dataRet != null)
                {
                    var retModel = new SuccessModel<GetImageChangeInfoResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, new SuccessModel(), "EK_GE_GetImageChangeInfoCust");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, 
                    RetMsg = EkycRetMsgConst.EKRetMsg_EK903
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_GE_GetImageChangeInfoCust");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }   
               
               
               
        /// <summary>
        /// Phương thức dùng để thay đổi số đt sms
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_CK_ChangeSmsInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetImageChangeInfoResult>))]
        [Note("Dành cho màn hình thông tin tài khoản > Bổ sung video chữ ký")]
        public async Task<IHttpActionResult> EK_CK_ChangeSmsInfo(GetImageChangeInfoCustRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_CK_ChangeSmsInfo");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = /*await _ekycService.ImageChangeInfoCust(model);*/ string.Empty;
                if (dataRet != null)
                {
                    var retModel = new SuccessModel<GetImageChangeInfoResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = null};
                    LogAnonymousResult(log, retModel, "EK_CK_ChangeSmsInfo");
                    return Ok(retModel);
                }
                return Ok(new EkycSuccessModel<bool>()
                {
                    RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, 
                    RetMsg = EkycRetMsgConst.EKRetMsg_EK903,
                    RetData = false});
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_CK_ChangeSmsInfo");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        } 
        
        
        /// <summary>
        /// Phương thức dùng để lấy ảnh chữ ký hợp đồng bổ sung
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        ///  POST Ekyc/EK_IM_GetImageAddProfile
        ///
        ///   {
        ///    "CardID": "073491053"
        ///   }
        ///
        ///
        /// </remarks>
        [ActionName("EK_IM_GetImageAddProfile")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetImageSignatureResult>))]
        [Note("Dành cho màn hình thông tin tài khoản > Bổ sung video chữ ký")]
        public async Task<IHttpActionResult> EK_IM_GetImageAddProfile(GetImageChangeInfoCustRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_IM_GetImageAddProfile");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.GetImageAddProfile(model);
                if (dataRet != null)
                {
                    var retModel = new SuccessModel<GetImageSignatureResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, new SuccessModel(), "EK_IM_GetImageAddProfile");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, 
                    RetMsg = EkycRetMsgConst.EKRetMsg_EK903,
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_IM_GetImageAddProfile");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        } 
        
        /// <summary>
        /// Phương thức dùng stringee
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample Request
        ///
        /// POST Ekyc/AT_SA_StringeeAccessToken
        /// 
        ///  {
        ///    "userId": "100145",
        ///     "phone": "0355488383"
        /// }
        ///
        /// 
        /// </remarks>
        [ActionName("AT_SA_StringeeAccessToken")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<StringeeResult>))]
        [Note("Dành cho màn hình thông tin tài khoản > Bổ sung video chữ ký")]
        public async Task<IHttpActionResult> SA_TK_StringeeAccessToken(StringeeRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "SA_TK_StringeeAccessToken");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.GetAccessTokenStringee(model);
                if (dataRet != null)
                {
                    var retModel = new SuccessModel<StringeeResult>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = dataRet};
                    LogAnonymousResult(log, retModel, "SA_TK_StringeeAccessToken");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, 
                    RetMsg = EkycRetMsgConst.EKRetMsg_EK903,
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "SA_TK_StringeeAccessToken");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        } 
        
        
        
        /// <summary>
        /// Phương thức dùng để lấy tỉnh thành (cho mở tài khoản)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// </remarks>
        [ActionName("CC_GC_GetProvinceList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<ProvinceResult>))]
        [Note("Dùng cho mở tài khoản")]
        public async Task<IHttpActionResult> CC_GC_GetProvinceList(BaseEkycRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CC_GC_GetProvinceList");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var results = await _ekycService.GetProvince();
                if (results.Any())
                {
                    var retModel = new SuccessListModel<ProvinceResult>()
                    {RetCode = RetCodeConst.SUCCESS_CODE, RetData = results};
                    LogAnonymousResult(log, retModel, "CC_GC_GetProvince");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, 
                    RetMsg = EkycRetMsgConst.EKRetMsg_EK903,
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CC_GC_GetProvinceList");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        /// <summary>
        /// Phương thức dùng để lấy quận huyện
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// </remarks>
        [ActionName("CC_GC_GetDistrictList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<DistrictResult>))]
        [Note("Dùng cho mở tài khoản")]
        public async Task<IHttpActionResult> CC_GC_GetDistrictList(DistrictRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CC_GC_GetDistrictList");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var results = await _ekycService.GetDistrict(model);
                if (results.Any())
                {
                    var retModel = new SuccessListModel<DistrictResult>()
                    {RetCode = RetCodeConst.SUCCESS_CODE, RetData = results};
                    LogAnonymousResult(log, retModel, "CC_GC_GetDistrictList");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, 
                    RetMsg = EkycRetMsgConst.EKRetMsg_EK903,
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CC_GC_GetDistrictList");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }
       
        /// <summary>
        /// Phương thức dùng để lấy xã phường
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// </remarks>
        [ActionName("CC_GC_GetWardList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<WardResult>))]
        [Note("Dùng cho mở tài khoản")]
        public async Task<IHttpActionResult> CC_GC_GetWardList(WardRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CC_GC_GetWardList");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var results = await _ekycService.GetWardList(model);
                if (results.Any())
                {
                    var retModel = new SuccessListModel<WardResult>()
                    {RetCode = RetCodeConst.SUCCESS_CODE, RetData = results};
                    LogAnonymousResult(log, retModel, "CC_GC_GetWardList");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, 
                    RetMsg = EkycRetMsgConst.EKRetMsg_EK903,
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CC_GC_GetWardList");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        } 
        
        /// <summary>
        /// Phương thức dùng để lấy tên nhân viên môi giới
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// </remarks>
        [ActionName("CC_GC_GetSaleName")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<SaleIDResult>))]
        [Note("Dùng cho mở tài khoản")]
        public async Task<IHttpActionResult> CC_GC_GetSaleName(SaleIDRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "CC_GC_GetSaleName");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                       
                }


                if (model.saleID.Length >= 4 && model.saleID != null)
                {
                    var result = await _ekycService.GetSaleName(model.saleID);
                    if (result != null)
                    {
                        var retModel = new SuccessModel<SaleIDResult>(){RetCode = RetCodeConst.SUCCESS_CODE, RetData = result};
                        LogAnonymousResult(log, retModel, "CC_GC_GetWardList");
                        return Ok(retModel);
                    }
                    return Ok(new ErrorModel()
                    {
                        RetCode = EkycRetCodeConst.EK903_DATA_EMPTYORNULL, 
                        RetMsg = EkycRetMsgConst.EKRetMsg_EK903,
                    });   
                }
                return Ok(new ErrorModel()
                {
                    RetCode = EkycRetCodeConst.EK900_VALIDATION_DATA, 
                    RetMsg = "Mã nhân viên phải đủ 4 ký tự",
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "CC_GC_GetWardList");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }  
        
        
        /// <summary>
        /// Dùng để update số CMND
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_UPDATE_CARDID")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Dành cho màn hình thông tin tài khoản > Bổ sung video chữ ký")]
        public async Task<IHttpActionResult> EK_UPDATE_CARDID(GetImageChangeInfoCustRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_UPDATE_CARDID");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRet = await _ekycService.UpdateCardID(model.CardID);
                if (dataRet)
                {
                    var retModel = new SuccessModel<bool>()
                        {RetCode = RetCodeConst.SUCCESS_CODE, RetData = true};
                    LogAnonymousResult(log, retModel, "EK_UPDATE_CARDID");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = "888", 
                    RetMsg = "Không thành công.",
                });
            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_UPDATE_CARDID");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }  
        
        /// <summary>
        /// Dùng để check thông tin mở tài khoản KH
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("EK_OP_CheckOpenAccountInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Dành cho màn hình thông tin tài khoản > Bổ sung video chữ ký")]
        public async Task<IHttpActionResult> EK_OP_CheckOpenAccountInfo(CheckOpenAccountInfoRequest model,
            string src = CommonConst.SOURCE_TYPE_M,
            string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogAnonymousRequest(log, model, "EK_OP_CheckOpenAccountInfo");
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                        {RetCode = EkycRetCodeConst.EK999_MISSING_DATA, RetMsg = EkycRetMsgConst.EKRetMsg_EK999});
                }

                var dataRetForm = await _ekycService.CheckCustomerFormInfomation(model.Email,model.Phone);
                if (dataRetForm != 0)
                {
                    if (dataRetForm == 2)
                    {
                        return Ok(new ErrorModel()
                        {
                            RetCode = "EK_966", 
                            RetMsg = EkycRetMsgConst.EKRetMsg_EK_OpenInfo_Form
                        });
                    }
                }

                var dataRetEkyc = await _ekycService.CheckCustomerEkycInfomation(model.Email, model.Phone);
                if (dataRetEkyc != 0)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = "EK_966", 
                        RetMsg = EkycRetMsgConst.EKRetMsg_EK_OpenInfo_CheckEmail
                    });   
                }
                var retModel = new SuccessModel<bool>()
                    {RetCode = RetCodeConst.SUCCESS_CODE, RetData = true};
                LogAnonymousResult(log, retModel, "EK_OP_CheckOpenAccountInfo");
                return Ok(retModel);

            }
            catch (Exception ex)
            {
                LogAnonymousException(log, model, ex, "EK_OP_CheckOpenAccountInfo");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }  
        
        
        
        
    }
}