﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.Ekyc;
using TVSI.Bond.WebAPI.Lib.Models.Notications;
using TVSI.Bond.WebAPI.Lib.Services;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// Bond API
    /// </summary>
    public class NoticationController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(NoticationController));
        private INoticationService _noticationService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="noticationService"></param>
        public NoticationController(INoticationService noticationService)
        {
            _noticationService = noticationService;
        }

        /// <summary>
        /// Lấy thong bao trong ngay
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GE_NO_GetNoticationOfDay")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<NoticationResponseModel>))]
        [Note("Màn hình thong bao")]
        public async Task<IHttpActionResult> GE_NO_GetNoticationOfDay(NoticationRequestModel model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "GE_NO_GetNoticationOfDay");
                SetLanguage(lang);

                var data = await _noticationService.GetNoticationOfDay(model);

                var dataRet = new SuccessModel<NoticationResponseModel> { RetData = data };
                LogResult(log, dataRet, model.UserId, "GE_NO_GetNoticationOfDay");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GE_NO_GetNoticationOfDay");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy lich su thong bao
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GE_NO_GetHistoryNotication")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<NoticationResponseModel>))]
        [Note("Màn hình thong bao")]
        public async Task<IHttpActionResult> GE_NO_GetHistoryNotication(NoticationRequestModel model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "GE_NO_GetHistoryNotication");
                SetLanguage(lang);

                var data = await _noticationService.GetHistoryNotication(model);

                var dataRet = new SuccessModel<NoticationResponseModel> { RetData = data };
                LogResult(log, dataRet, model.UserId, "GE_NO_GetHistoryNotication");
                return Ok(dataRet);
                
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GE_NO_GetHistoryNotication");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Dang ky thong bao cho user
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("PO_NO_PostRegisterNotication")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Dang ky thong bao cho user")]
        public async Task<IHttpActionResult> PO_NO_PostRegisterNotication(NoticationContractModel model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "PO_NO_PostRegisterNotication");
                SetLanguage(lang);

                var data = await _noticationService.PostRegisterNotication(model);

                var dataRet = new SuccessModel();
                LogResult(log, dataRet, model.UserId, "PO_NO_PostRegisterNotication");
                return Ok(dataRet);
                
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "PO_NO_PostRegisterNotication");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lay thong tin dang ky thong bao khach hang
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GE_NO_GetRegisterNotication")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<NoticationContractResponseModel>))]
        [Note("Màn hình thong bao")]
        public async Task<IHttpActionResult> GE_NO_GetRegisterNotication(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "GE_NO_GetRegisterNotication");
                SetLanguage(lang);

                var data = await _noticationService.GetRegisterNotication(model);

                var dataRet = new SuccessModel<NoticationContractResponseModel> { RetData = data };
                LogResult(log, dataRet, model.UserId, "GE_NO_GetRegisterNotication");
                return Ok(dataRet);
 
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GE_NO_GetRegisterNotication");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lay thong tin loại thông báo
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GE_NO_GetNoticationType")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<NoticationTypeModel>>))]
        [Note("Lay thong tin loại thông báo")]
        public async Task<IHttpActionResult> GE_NO_GetNoticationType(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "GE_NO_GetNoticationType");
                SetLanguage(lang);

                var data = await _noticationService.GetListTypeNotication(model);

                if (data != null)
                {
                    var dataRet = new SuccessModel<List<NoticationTypeModel>> { RetData = data };
                    LogResult(log, dataRet, model.UserId, "GE_NO_GetNoticationType");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,
                    RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GE_NO_GetNoticationType");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Đánh dấu noti đã đọc
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("PO_NO_ReadNotication")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Đánh dấu noti đã đọc")]
        public async Task<IHttpActionResult> PO_NO_ReadNotication(ReadNoticationContact model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "PO_NO_ReadNotication");
                SetLanguage(lang);

                var data = await _noticationService.ReadNotication(model);

                if (data)
                {
                    var dataRet = new SuccessModel();
                    LogResult(log, dataRet, model.UserId, "PO_NO_ReadNotication");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,
                    RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "PO_NO_ReadNotication");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tạo mới notication warning
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("PO_NO_CreateNotication")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Tạo mới notication warning")]
        public async Task<IHttpActionResult> PO_NO_CreateNotication(CreateNoticationRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "PO_NO_CreateNotication");
                SetLanguage(lang);

                var data = await _noticationService.CreateNotication(model);

                if (data)
                {
                    var dataRet = new SuccessModel();
                    LogResult(log, dataRet, model.UserId, "PO_NO_CreateNotication");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,
                    RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR
                });
               
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "PO_NO_CreateNotication");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy cảnh báo trong ngay
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GE_NO_GetWarningOfDay")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<NoticationResponseModel>))]
        [Note("Lấy cảnh báo trong ngay")]
        public async Task<IHttpActionResult> GE_NO_GetWarningOfDay(NoticationRequestModel model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "GE_NO_GetWarningOfDay");
                SetLanguage(lang);

                var data = await _noticationService.GetWarningOfDay(model);

                var dataRet = new SuccessModel<NoticationResponseModel> { RetData = data };
                LogResult(log, dataRet, model.UserId, "GE_NO_GetWarningOfDay");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GE_NO_GetWarningOfDay");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy lich su canh bao
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GE_NO_GetHistoryWarning")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<NoticationResponseModel>))]
        [Note("Lấy lich su canh bao")]
        public async Task<IHttpActionResult> GE_NO_GetHistoryWarning(NoticationRequestModel model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "GE_NO_GetHistoryWarning");
                SetLanguage(lang);

                var data = await _noticationService.GetHistoryWarning(model);

                var dataRet = new SuccessModel<NoticationResponseModel> { RetData = data };
                LogResult(log, dataRet, model.UserId, "GE_NO_GetHistoryWarning");
                return Ok(dataRet);

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GE_NO_GetHistoryWarning");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Cập nhật trạng thái cho noti
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("PO_NO_UpdateStatusNotication")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Cập nhật trạng thái cho noti")]
        public async Task<IHttpActionResult> PO_NO_UpdateStatusNotication(UpdateStatusNoticationContact model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "PO_NO_UpdateStatusNotication");
                SetLanguage(lang);

                var data = await _noticationService.UpdateStatusNotication(model);

                if (data)
                {
                    var dataRet = new SuccessModel();
                    LogResult(log, dataRet, model.UserId, "PO_NO_UpdateStatusNotication");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,
                    RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "PO_NO_UpdateStatusNotication");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Đánh dấu all noti đã đọc
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("PO_NO_ReadAllNotication")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Đánh dấu noti đã đọc")]
        public async Task<IHttpActionResult> PO_NO_ReadAllNotication(ReadAllNoticationRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "PO_NO_ReadAllNotication");
                SetLanguage(lang);

                var data = await _noticationService.ReadAllNotication(model);

                if (data)
                {
                    var dataRet = new SuccessModel();
                    LogResult(log, dataRet, model.UserId, "PO_NO_ReadAllNotication");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,
                    RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "PO_NO_ReadAllNotication");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }   
        
    }
}
