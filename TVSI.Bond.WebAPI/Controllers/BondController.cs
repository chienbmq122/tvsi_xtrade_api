﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.Bond;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Lib.Services;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Hosting;
using System.Text;
using TVSI.Utility;

namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// Bond API
    /// </summary>
    public class BondController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(BondController));
        private IBondService _bondService;
        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="bondService"></param>
        public BondController(IBondService bondService)
        {
            _bondService = bondService;
        }

        #region Trái phiếu
        /// <summary>
        /// Lấy thông tin các sản phẩm trái phiếu của TVSI
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_PR_BondProductInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ProductInfoResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Sản phẩm")]
        public async Task<IHttpActionResult> BM_PR_BondProductInfo(ProductInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_PR_BondProductInfo");

                // Validate empty data of request model
                var errModel = ValidateRequestModel(model, lang);
                if (errModel != null)
                    return Ok(errModel);

                var retProductInfo = await _bondService.TVSI_sAPI_BM_PR_BondProductInfo(lang);
                var dataRet = new SuccessListModel<ProductInfoResult> { RetData = retProductInfo };

                LogResult(log, dataRet, model.UserId, "BM_PR_BondProductInfo");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_PR_BondProductInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết từng sản phẩm trái phiếu của TVSI
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_PR_BondProductInfoByType")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ProductTypeResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Sản phẩm", true)]
        public async Task<IHttpActionResult> BM_PR_BondProductInfoByType(ProductTypeRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_PR_BondProductInfoByType");

                var responseData = await _bondService.TVSI_sAPI_BM_PR_BondProductInfoByType(lang);
                var dataRet = new SuccessListModel<ProductTypeResult> { RetData = responseData };

                LogResult(log, dataRet, model.UserId, "BM_PR_BondProductInfoByType");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_PR_BondProductInfoByType");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách bảng giá Outright
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OR_BondOutRightBoard")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<OutrightBoardResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Bảng giá Outright", true)]
        public async Task<IHttpActionResult> BM_OR_BondOutRightBoard(OutrightBoardRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_OR_BondOutRightBoard");

                var responseData = await _bondService.TVSI_sAPI_BM_OR_BondOutRightBoard(model.AccountNo, "", lang);
                var dataRet = new SuccessListModel<OutrightBoardResult> { RetData = responseData };

                LogResult(log, dataRet, model.UserId, "BM_OR_BondOutRightBoard");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_OR_BondOutRightBoard");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách bảng giá Outright (Tìm kiếm theo BondCode)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OR_BondOutRightCode")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<OutrightBoardResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Bảng giá Outright", true)]
        public async Task<IHttpActionResult> BM_OR_BondOutRightCode(OutRightBoardCodeRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_OR_BondOutRightCode");

                var responseData = await _bondService.TVSI_sAPI_BM_OR_BondOutRightBoard(model.AccountNo, model.SearchBondCode, lang);
                var dataRet = new SuccessListModel<OutrightBoardResult> { RetData = responseData };

                LogResult(log, dataRet, model.UserId, "BM_OR_BondOutRightCode");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_OR_BondOutRightCode");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Lấy danh sách mã trái phiếu Outright
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        //[ActionName("BM_OR_BondOutRightList")]
        //[HttpPost]
        //[ResponseType(typeof(SuccessListModel<OutRightListResult>))]
        //[Note("Màn hình SPTC > Tab Trái phiếu > Tab Bảng giá Outright", false)]
        //public IHttpActionResult BM_OR_BondOutRightList(OutRightListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        //{
        //    try
        //    {
        //        return Ok(new ErrorModel { RetCode = RetCodeConst.ERR998_NOT_IMPLEMENT });
        //    }
        //    catch (Exception ex)
        //    {
        //        LogException(log, model, ex, "");
        //        return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
        //    }
        //}


        /// <summary>
        /// Lấy thông tin chi tiết trái phiếu 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_BI_BondInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BondInfoDetailResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Bảng giá Outright, Đầu tư... > Thông tin trái phiếu", true)]
        public async Task<IHttpActionResult> BM_BI_BondInfo(BondInfoDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_BI_BondInfo");

                var retBondInfo = await _bondService.TVSI_sAPI_BM_BI_BondInfo(model.BondCode, lang);
                var dataRet = new SuccessListModel<BondInfoDetailResult> { RetData = retBondInfo };

                LogResult(log, dataRet, model.UserId, "BM_BI_BondInfo");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_BI_BondInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách trái phiếu 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_BI_IssueGroupInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<IssuerGroupInfoResult>))]
        [Note("All > Danh sách trái phiếu", true)]
        public async Task<IHttpActionResult> BM_BI_IssueGroupInfo(IssuerGroupInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_BI_IssueGroupInfo");

                var data = await _bondService.TVSI_sAPI_BM_BI_IssueGroupInfo();

                var dataRet = new SuccessListModel<IssuerGroupInfoResult> { RetData = data };

                LogResult(log, dataRet, model.UserId, "BM_BI_IssueGroupInfo");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_BI_BondInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin trái phiếu khi đặt lệnh mua
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_BondBuyInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BuyInfoResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_BondBuyInfo(BuyInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_IV_BondBuyInfo");
                var ipgws = new TVSIWS_IPG.IPGWS();
                var withdraw = 0m;
                if (model.AccountNo.EndsWith("1"))
                {
                    try
                    {
                        var dataInfo_01 = ipgws.IPGWS530(model.AccountNo, "BOND_API");
                        var ws530 = TVSI.Utility.WebServiceEntitySerializer.Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS530>(dataInfo_01);
                        if (ws530?.so_tien > 0)
                            withdraw = ws530.so_tien;
                    }
                    catch (Exception ex)
                    {
                        LogException(log, model, ex, "");
                    }
                }

                var getAwaiterData = await _bondService.TVSI_sAPI_BM_PR_BondProductInfoByType(lang);
                var productTypeList = getAwaiterData.ToList();
                var productInfoList = new List<BuyProductInfo>();
                foreach (var item in productTypeList)
                {
                    productInfoList.Add(new BuyProductInfo
                    {
                        ProductType = item.BondType,
                        ProductName = item.BondTypeName,
                        ProductDescription = item.Description
                    });
                }

                var data = new BuyInfoResult
                {
                    Withdrawal = withdraw,
                    ProductInfoList = productInfoList
                };
                var dataRet = new SuccessModel<BuyInfoResult> { RetData = data };

                LogResult(log, dataRet, model.UserId, "BM_IV_BondBuyInfo");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_BondBuyInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách kỳ hạn trái phiếu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_BondProductTermList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<ProductTermResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_BondProductTermList(ProductTermRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_IV_BondProductTermList");

                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondProductTermList(model.AccountNo, model.ProductType, model.Amount, tradeDate, lang);


                var dataRet = new SuccessListModel<ProductTermResult> { RetData = getAwaiterData };
                LogResult(log, dataRet, model.UserId, "BM_IV_BondProductTermList");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_BondProductTermList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách kỳ trả lãi
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_BondProductPayTermList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<ProductPayTermResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_BondProductPayTermList(ProductPayTermRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_IV_BondProductPayTermList");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondProductPayTermList(model.AccountNo, model.ProductType, model.Amount, tradeDate, model.Term, lang);

                var dataRet = new SuccessListModel<ProductPayTermResult> { RetData = getAwaiterData };

                LogResult(log, dataRet, model.UserId, "BM_IV_BondProductPayTermList");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_BondProductPayTermList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tìm kiếm mã trái phiếu thỏa mãn điều kiện
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_FindBondRequest")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<FindBondRequestResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_FindBondRequestcc(FindBondRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_IV_FindBondRequest");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_FindBondRequest(model.AccountNo, model.ProductType, model.Amount, tradeDate,
                    model.Term, model.IsPreSale, model.PayTerm, lang);

                var dataRet = new SuccessListModel<FindBondRequestResult> { RetData = getAwaiterData };
                LogResult(log, dataRet, model.UserId, "BM_IV_FindBondRequest");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_FindBondRequest");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tra cứu tỷ suất lợi nhuận mà NĐT nhận được khi chọn các kỳ hạn trái phiếu tương ứng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_BondBuyCalBondRate")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BuyCalRateResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_BondBuyCalBondRate(BuyCalRateRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_IV_BondBuyCalBondRate");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var sellDate = !string.IsNullOrEmpty(model.SellDate) ? DateTime.ParseExact(model.SellDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                        : tradeDate;
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondBuyCalBondRate(model.AccountNo, model.ProductType, model.Amount, tradeDate,
                    model.Term, model.IsPreSale, model.PayTerm, model.SubBondCode, sellDate, lang);

                var custBankWait = await _bondService.TVSI_sAPI_BM_IV_GetCustBankInfo(model.UserId);

                var calBondInfo = getAwaiterData.FirstOrDefault();
                if (calBondInfo.RetCode == RetCodeConst.SUCCESS_CODE)
                {
                    var custBanInfo = custBankWait.ToList();

                    var buyCalRateResult = new BuyCalRateResult
                    {
                        CalBondInfo = calBondInfo,
                        CalCustBankInfos = custBanInfo
                    };

                    var dataRet = new SuccessModel<BuyCalRateResult> { RetData = buyCalRateResult };
                    LogResult(log, dataRet, model.UserId, "BM_IV_BondBuyCalBondRate");
                    return Ok(dataRet);
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + calBondInfo.RetCode);
                    var dataRet = new ErrorModel { RetCode = calBondInfo.RetCode, RetMsg = retMsg };                    
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_BondBuyCalBondRate");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin mã coupon
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_CheckRedeemCode")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<RedeemCodeInfo>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_CheckRedeemCode(CheckRedeemCodeRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_CheckRedeemCode");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_CheckRedeemCode(model.AccountNo, model.SubBondCode, tradeDate, model.Volume, model.RedeemCode, model.RedeemType);
                var data = getAwaiterData.FirstOrDefault();
                if (data == null)
                {
                    SetLanguage(lang);
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("NoRedeemCode");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };

                    LogResult(log, dataRet, model.UserId, "BM_IV_CheckRedeemCode");
                    return Ok(dataRet);
                }
                else
                {
                    if (data.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<RedeemCodeInfo> { RetData = data };
                        LogResult(log, dataRet, model.UserId, "BM_IV_CheckRedeemCode");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + data.RetCode);
                        var dataRet = new ErrorModel { RetCode = data.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_CheckRedeemCode");
                        return Ok(dataRet);
                    }

                    //var dataRet = new SuccessModel<RedeemCodeInfo> { RetData = data };
                    //LogResult(log, dataRet, "BM_IV_CheckRedeemCode");
                    //return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_CheckRedeemCode");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        ///// <summary>
        ///// Lấy thông tin lệnh mua từ những giá trị nhập vào của khách hàng
        ///// </summary>
        ///// <param name="model"></param>
        ///// <param name="src">Nguồn yêu cầu</param>
        ///// <param name="lang">Ngôn ngữ</param>
        ///// <returns></returns>
        //[ActionName("BM_IV_BondFindBuyInfo")]
        //[HttpPost]
        //[ResponseType(typeof(SuccessModel<FindBuyInfoResult>))]
        //[Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua", false)]
        //public IHttpActionResult BM_IV_BondFindBuyInfo(FindBuyInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        //{
        //    try
        //    {
        //        return Ok(new ErrorModel { RetCode = RetCodeConst.ERR998_NOT_IMPLEMENT });
        //    }
        //    catch (Exception ex)
        //    {
        //        LogException(log, model, ex, "");
        //        return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
        //    }
        //}

        /// <summary>
        /// Validate thông tin lệnh mua của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_BondValidateBuyInfo")]
        [HttpPost]
        [ResponseType(typeof(BuyDataResult))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_BondValidateBuyInfo(ValidateBuyInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_IV_BondValidateBuyInfo");
                var tradeDate = DateTime.ParseExact(model.BuyInfo.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var sellDate = !string.IsNullOrEmpty(model.BuyInfo.SellDate)
                    ? DateTime.ParseExact(model.BuyInfo.SellDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    : DateTime.Today;
                // Kiểm tra tiền
                var cashAmt = 0M;
                var ipgws = new TVSIWS_IPG.IPGWS();
                var dataInfo_01 = ipgws.IPGWS530(model.AccountNo, "BOND_API");
                var ws530 = TVSI.Utility.WebServiceEntitySerializer.Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS530>(dataInfo_01);
                if (ws530?.so_tien > 0)
                    cashAmt = ws530.so_tien;
                                
                // Kiểm tra tiền
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondValidateBuyInfo(model.AccountNo, model.BuyInfo.BondCode, model.BuyInfo.SubBondCode,
                    model.BuyInfo.Volume, tradeDate, model.BuyInfo.Rate, model.BuyInfo.RedeemCode,
                    sellDate, model.BuyInfo.CustBankID, cashAmt, lang);
                var data = getAwaiterData.FirstOrDefault();
                SetLanguage(lang);
                if (data != null)
                {
                    if (data.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<BuyDataResult> { RetData = data };
                        LogResult(log, dataRet, model.UserId, "BM_IV_BondValidateBuyInfo");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + data.RetCode);
                        var dataRet = new ErrorModel { RetCode = data.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_BondValidateBuyInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_BondValidateBuyInfo");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Xác nhận thông tin lệnh mua của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_BondConfirmBuyInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ConfirmBuyInfoResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_BondConfirmBuyInfo(ConfirmBuyInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_IV_BondConfirmBuyInfo");
                var tradeDate = DateTime.ParseExact(model.BuyInfo.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var sellDate = !string.IsNullOrEmpty(model.BuyInfo.SellDate)
                    ? DateTime.ParseExact(model.BuyInfo.SellDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    : DateTime.Today;
                
                // Kiểm tra tiền
                var cashAmt = 0M;
                var ipgws = new TVSIWS_IPG.IPGWS();
                var dataInfo_01 = ipgws.IPGWS530(model.AccountNo, "BOND_API");
                var ws530 = TVSI.Utility.WebServiceEntitySerializer.Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS530>(dataInfo_01);
                if (ws530?.so_tien > 0)
                    cashAmt = ws530.so_tien;

                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondConfirmBuyInfo(model.AccountNo, model.BuyInfo.BondCode, model.BuyInfo.SubBondCode,
                    model.BuyInfo.Volume, tradeDate, model.BuyInfo.Rate, model.BuyInfo.RedeemCode,
                    sellDate, model.BuyInfo.CustBankID, model.BuyInfo.IsHardCopy, cashAmt, lang);
                var data = getAwaiterData.FirstOrDefault();
                SetLanguage(lang);
                if (data != null)
                {
                    if (data.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<ConfirmBuyInfoResult> { RetData = data };
                        LogResult(log, dataRet, model.UserId, "BM_IV_BondConfirmBuyInfo");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + data.RetCode);
                        var dataRet = new ErrorModel { RetCode = data.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_BondConfirmBuyInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_BondConfirmBuyInfo");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_BondConfirmBuyInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Validate thông tin lệnh bán của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_BondValidateSellInfo")]
        [HttpPost]
        [ResponseType(typeof(ValidateSellInfoResult))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua > Xác nhận lệnh bán", true)]
        public async Task<IHttpActionResult> BM_IV_BondValidateSellInfo(ValidateSellInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "BM_IV_BondValidateSellInfo");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondValidateSellInfo(model.AccountNo, model.ContractNo, tradeDate, model.Volume, model.RedeemCode, model.CustBankID, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<ValidateSellInfoResult> { RetData = returnData };
                        LogResult(log, dataRet, model.UserId, "BM_IV_BondValidateSellInfo");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_BondValidateSellInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_BondValidateSellInfo");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_BondValidateSellInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin lệnh bán
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_FindSellContractInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<FindSellContractInfoResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua > Lấy thông tin lệnh bán", true)]
        public async Task<IHttpActionResult> BM_IV_FindSellContractInfo(FindSellContractInfo model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_FindSellContractInfo");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_FindSellContractInfo(model.AccountNo, model.ContractNo, tradeDate);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var custBankWait = await _bondService.TVSI_sAPI_BM_IV_GetCustBankInfo(model.UserId);
                        var custBanInfo = custBankWait.ToList();
                        returnData.CalCustBankInfos = custBanInfo;

                        var dataRet = new SuccessModel<FindSellContractInfoResult> { RetData = returnData };
                        LogResult(log, dataRet, model.UserId, "BM_IV_FindSellContractInfo");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_FindSellContractInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_FindSellContractInfo");
                    return Ok(dataRet);
                }

                //returnData.CalCustBankInfos = custBanInfo;

                //var dataRet = new SuccessModel<FindSellContractInfoResult> { RetData = returnData };
                //return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_FindSellContractInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin lệnh bán theo thông tin nhập
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_CalcSellContractInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CalcSellContractInfoResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua > Lấy thông tin lệnh bán", true)]
        public async Task<IHttpActionResult> BM_IV_CalcSellContractInfo(CalcSellContractInfo model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_CalcSellContractInfo");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_CalcSellContractInfo(model.AccountNo, model.ContractNo, tradeDate, model.Volume, model.RedeemCode);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<CalcSellContractInfoResult> { RetData = returnData };
                        LogResult(log, dataRet, model.UserId, "BM_IV_CalcSellContractInfo");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_CalcSellContractInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_CalcSellContractInfo");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_CalcSellContractInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Xác nhận thông tin lệnh bán của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_BondConfirmSellInfo")]
        [HttpPost]
        [ResponseType(typeof(ConfirmSellInfoResult))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua > Xác nhận lệnh bán", true)]
        public async Task<IHttpActionResult> BM_IV_BondConfirmSellInfo(ConfirmSellInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_BondConfirmSellInfo");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondConfirmSellInfo(model.AccountNo, model.ContractNo, tradeDate,
                    model.Volume, model.RedeemCode, model.CustBankID, model.IsHardCopy, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<ConfirmSellInfoResult> { RetData = returnData };
                        LogResult(log, dataRet, model.UserId, "BM_IV_BondConfirmSellInfo");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_BondConfirmSellInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_BondConfirmSellInfo");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_BondConfirmSellInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Lấy thông tin lệnh bán
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_FindRollContractInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<FindRollContractInfoResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua > Lấy thông tin lệnh roll", true)]
        public async Task<IHttpActionResult> BM_IV_FindRollContractInfo(FindRollContractInfo model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_FindRollContractInfo");
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_FindRollContractInfo(model.AccountNo, model.ContractNo);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var custBankWait = await _bondService.TVSI_sAPI_BM_IV_GetCustBankInfo(model.UserId);
                        var custBanInfo = custBankWait.ToList();
                        returnData.CalCustBankInfos = custBanInfo;

                        var dataRet = new SuccessModel<FindRollContractInfoResult> { RetData = returnData };
                        LogResult(log, dataRet, model.UserId, "BM_IV_FindRollContractInfo");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_FindRollContractInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_FindRollContractInfo");
                    return Ok(dataRet);
                }

                //returnData.CalCustBankInfos = custBanInfo;

                //var dataRet = new SuccessModel<FindSellContractInfoResult> { RetData = returnData };
                //return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_FindRollContractInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Lấy thông tin lệnh bán theo thông tin nhập
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_CalcRollContractInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CalcRollContractInfoResult>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua > Lấy thông tin lệnh bán", true)]
        public async Task<IHttpActionResult> BM_IV_CalcRollContractInfo(CalcRollContractInfo model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_CalcRollContractInfo");
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_CalcRollContractInfo(model.AccountNo, model.ContractNo, model.RedeemCode);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<CalcRollContractInfoResult> { RetData = returnData };
                        LogResult(log, dataRet, model.UserId, "BM_IV_CalcRollContractInfo");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_CalcRollContractInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_CalcRollContractInfo");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_CalcRollContractInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        
        /// <summary>
        /// Validate gia hạn hợp đồng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_BondValidateRollBack")]
        [HttpPost]
        [ResponseType(typeof(ValidateRollInfoResult))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Button Gia hạn", false)]
        public async Task<IHttpActionResult> BM_IV_BondValidateRollBack(ValidateRollInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_BondValidateRollBack");
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondValidateRollInfo(model.AccountNo, model.ContractNo, model.RedeemCode, model.CustBankID, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<ValidateRollInfoResult> { RetData = returnData };
                        LogResult(log, dataRet, model.UserId, "BM_IV_BondValidateRollBack");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_BondValidateRollBack");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_BondValidateRollBack");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_BondValidateRollBack");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Xác nhận gia hạn hợp đồng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_BondConfirmRollBack")]
        [HttpPost]
        [ResponseType(typeof(ConfirmRollInfoResult))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Button Gia hạn", false)]
        public async Task<IHttpActionResult> BM_IV_BondConfirmRollBack(ConfirmRollInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_BondConfirmRollBack");
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondConfirmRollInfo(model.AccountNo, model.ContractNo,
                    model.RedeemCode, model.CustBankID, model.IsHardCopy, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<ConfirmRollInfoResult> { RetData = returnData };
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_BondConfirmRollBack");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Danh sách bản in HĐ mua
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_ViewBuyContractDataPrintInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<PrintContractDataInfo>>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > View HĐ mua", true)]
        public async Task<IHttpActionResult> BM_IV_ViewBuyContractDataPrintInfo(ConfirmBuyInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_ViewBuyContractDataPrintInfo");
                var data = new List<PrintContractDataInfo>();

                var dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };
                var tradeDate = DateTime.ParseExact(model.BuyInfo.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var sellDate = !string.IsNullOrEmpty(model.BuyInfo.SellDate)
                    ? DateTime.ParseExact(model.BuyInfo.SellDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    : DateTime.Today;

                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_ViewContractBuyPrintInfo(model.AccountNo, model.BuyInfo.BondCode, model.BuyInfo.SubBondCode,
                    model.BuyInfo.Volume, tradeDate, model.BuyInfo.Rate, model.BuyInfo.RedeemCode,
                    sellDate, model.BuyInfo.CustBankID, lang);
                var dataPrint = getAwaiterData.ToList();
                SetLanguage(lang);
                if (dataPrint != null)
                {
                    var productTypeInfo = dataPrint.FirstOrDefault(x => x.Code == "ProductType");
                    var presaleInfo = dataPrint.FirstOrDefault(x => x.Code == "IsPreSale");
                    var mapPath = HostingEnvironment.MapPath("~");
                    if (productTypeInfo != null)
                    {
                        var templateName = "";
                        var title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_RecommendBuy");
                        if (productTypeInfo.CodeValue == "HDMG")
                        {
                            title = ResourceFile.BondModule.ResourceManager.GetString("MM_TitlePrint_RecommendBuy");
                            templateName = @"Buy_DeNghiHopDongMoiGioi.html";
                            if (presaleInfo.CodeValue == "1")
                                templateName = @"Buy_DeNghiHopDongMoiGioi_CoBanTruocHan.html";

                            var content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\" + templateName, Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);

                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });
                        }
                        else
                        {
                            templateName = @"\Buy_DeNghiMua.html";

                            var content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\" + templateName, Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);

                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });


                            if (productTypeInfo.CodeValue != "OUTRIGHT")
                            {
                                if (presaleInfo != null && presaleInfo.CodeValue == "1" && productTypeInfo.CodeValue == "FIX-FLEX")
                                    content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Buy_DeNghiBan_CoBanTruocHan.html", Encoding.UTF8);
                                else
                                    content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Buy_DeNghiBan.html", Encoding.UTF8);

                                content = ReplacePrintData(dataPrint, content, lang);

                                title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_RecommendSell");
                                data.Add(new PrintContractDataInfo
                                {
                                    Title = title,
                                    Content = content
                                });
                            }

                            title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_Confirm");
                            content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\XacNhanGiaoDich.html", Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });
                        }
                    }
                }
                dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };
                LogResult(log, dataRet, model.UserId, "BM_IV_ViewBuyContractDataPrintInfo");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_ViewBuyContractDataPrintInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        private string ReplacePrintData(List<PrintCodeData> dataPrint, string content, string lang)
        {
            foreach (var item in dataPrint)
            {
                if (item.DataType == "Number")
                {
                    double value = Functions.GetDouble(item.CodeValue);
                    if (value != 0)
                        content = content.Replace("{{" + item.Code + "}}", value.ToString("#,#"));
                    else
                        content = content.Replace("{{" + item.Code + "}}", "0");

                    if (item.IsReadByWord == "1")
                    {
                        content = content.Replace("{{" + item.Code + "ByWord}}", DICHSO.XL_DOC_SO.DOC_SO_CHUOI(value, "", "", 0).Replace(" chẵn", ""));
                        content = content.Replace("{{" + item.Code + "ByWord_EN}}", CommonConst.ToWords(value, "en"));
                    }
                }
                else if (item.DataType == "String")
                {
                    content = content.Replace("{{" + item.Code + "}}", item.CodeValue);
                    content = content.Replace("{{" + item.Code + "_EN}}", CommonConst.ConvertWordToEN(item.CodeValue));
                }
                else if (item.DataType == "Date")
                {
                    string value = "";
                    if (!string.IsNullOrWhiteSpace(item.CodeValue))
                    {
                        value = Functions.GetDateTime(item.CodeValue).ToString("dd/MM/yyyy");
                        content = content.Replace("{{" + item.Code + "}}", value);
                        value = CommonConst.FormatDateTimeEN(Functions.GetDateTime(item.CodeValue));
                        content = content.Replace("{{" + item.Code + "_EN}}", value);
                    }
                    else
                    {
                        content = content.Replace("{{" + item.Code + "}}", "");
                        content = content.Replace("{{" + item.Code + "_EN}}", "");
                    }
                }
            }
            return content;
        }

        [ActionName("BM_IV_ViewRollContractDataPrintInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<PrintContractDataInfo>>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > View HĐ Gia hạn", false)]
        public async Task<IHttpActionResult> BM_IV_ViewRollContractDataPrintInfo(ConfirmRollInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_ViewRollContractDataPrintInfo");
                var data = new List<PrintContractDataInfo>();
                var dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };

                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_ViewContractRollPrintInfo(model.AccountNo, model.ContractNo,
                    model.RedeemCode, model.CustBankID, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                var dataPrint = getAwaiterData.ToList();

                SetLanguage(lang);
                if (dataPrint != null)
                {
                    var productTypeInfo = dataPrint.FirstOrDefault(x => x.Code == "ProductType");
                    var mapPath = HostingEnvironment.MapPath("~");
                    if (productTypeInfo != null)
                    {
                        if (productTypeInfo.CodeValue == "HDMG")
                        {
                            var content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Roll_DeNghiGiaHan.html", Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);
                            var title = ResourceFile.BondModule.ResourceManager.GetString("MM_TitlePrint_RecommendRoll");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });
                        }
                        else
                        {
                            var content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Roll_DeNghiGiaHan.html", Encoding.UTF8);

                            content = ReplacePrintData(dataPrint, content, lang);
                            var title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_RecommendRoll");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });

                            content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\XacNhanGiaoDich.html", Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);
                            title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_Confirm");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });
                        }
                    }
                }
                dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };
                LogResult(log, dataRet, model.UserId, "BM_IV_ViewRollContractDataPrintInfo");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_ViewRollContractDataPrintInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        [ActionName("BM_IV_ViewSellContractDataPrintInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<PrintContractDataInfo>>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > View HĐ bán", false)]
        public async Task<IHttpActionResult> BM_IV_ViewSellContractDataPrintInfo(ConfirmSellInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_ViewSellContractDataPrintInfo");
                var data = new List<PrintContractDataInfo>();
                var dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };

                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_ViewContractSellPrintInfo(model.AccountNo, model.ContractNo, tradeDate,
                    model.Volume, model.RedeemCode, model.CustBankID, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                var dataPrint = getAwaiterData.ToList();

                SetLanguage(lang);
                if (dataPrint != null)
                {
                    var productTypeInfo = dataPrint.FirstOrDefault(x => x.Code == "ProductType");
                    var sellAll = dataPrint.FirstOrDefault(x => x.Code == "IsSellAll");
                    var isAdvance = dataPrint.FirstOrDefault(x => x.Code == "IsAdvance");
                    var mapPath = HostingEnvironment.MapPath("~");
                    if (productTypeInfo != null)
                    {
                        var content = "";
                        if (productTypeInfo.CodeValue == "HDMG")
                        {
                            if (sellAll != null && sellAll.CodeValue == "1")
                                content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanToanBo.html", Encoding.UTF8);
                            else
                                content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanMotPhan.html", Encoding.UTF8);

                            content = ReplacePrintData(dataPrint, content, lang);
                            var title = ResourceFile.BondModule.ResourceManager.GetString("MM_TitlePrint_RecommendSell");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });
                        }
                        else
                        {
                            if (sellAll != null && sellAll.CodeValue == "1")
                                content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanToanBo.html", Encoding.UTF8);
                            else
                                content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanMotPhan.html", Encoding.UTF8);

                            content = ReplacePrintData(dataPrint, content, lang);
                            var title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_RecommendSell");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });

                            if (isAdvance != null && isAdvance.CodeValue == "1")
                            {
                                content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_BanCoUngTruoc.html", Encoding.UTF8);
                                content = ReplacePrintData(dataPrint, content, lang);
                                title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_Advance");
                                data.Add(new PrintContractDataInfo
                                {
                                    Title = "Hoàn trả ứng",
                                    Content = content
                                });
                            }

                            content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\XacNhanGiaoDich.html", Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);
                            title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_Confirm");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = "Xác nhận GD",
                                Content = content
                            });
                        }
                    }
                }
                dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };
                LogResult(log, dataRet, model.UserId, "BM_IV_ViewSellContractDataPrintInfo");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_ViewSellContractDataPrintInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        #endregion

        [ActionName("BM_IV_GetNextTradeDate")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<DateTime>))]
        [Note("Lấy ngày giao dịch tiếp theo")]
        public async Task<IHttpActionResult> BM_IV_GetNextTradeDate(string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                var nextTradeDate = await _bondService.TVSI_sAPI_BM_IV_GetNextTradeDate();

                var dataRet = new SuccessModel<DateTime> { RetData = nextTradeDate };

                LogResult(log, dataRet, "masterdata", "BM_IV_GetNextTradeDate");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {                
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        #region HDMG

        [ActionName("BM_PR_MMProductInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ProductInfoResult>))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Sản phẩm")]
        public async Task<IHttpActionResult> BM_PR_MMProductInfo(ProductInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_PR_MMProductInfo");

                // Validate empty data of request model
                var errModel = ValidateRequestModel(model, lang);
                if (errModel != null)
                    return Ok(errModel);

                var retProductInfo = await _bondService.TVSI_sAPI_BM_PR_MMProductInfo(lang);
                var dataRet = new SuccessListModel<ProductInfoResult> { RetData = retProductInfo };

                LogResult(log, dataRet, model.UserId, "BM_PR_MMProductInfo");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_PR_MMProductInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        [ActionName("BM_PR_MMProductInfoByType")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ProductTypeResult>))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Sản phẩm", true)]
        public async Task<IHttpActionResult> BM_PR_MMProductInfoByType(ProductTypeRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_PR_MMProductInfoByType");

                var responseData = await _bondService.TVSI_sAPI_BM_PR_MMProductInfoByType(lang);
                var dataRet = new SuccessListModel<ProductTypeResult> { RetData = responseData };

                LogResult(log, dataRet, model.UserId, "BM_PR_MMProductInfoByType");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_PR_MMProductInfoByType");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách kỳ hạn HDMG
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_MMProductTermList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<ProductTermResult>))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_MMProductTermList(ProductTermRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_MMProductTermList");

                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondProductTermList(model.AccountNo, (int)CommonConst.ProductType.MM, model.Amount, tradeDate, lang);


                var dataRet = new SuccessListModel<ProductTermResult> { RetData = getAwaiterData };
                LogResult(log, dataRet, model.UserId, "BM_IV_MMProductTermList");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_MMProductTermList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Lấy danh sách kỳ trả lãi HDMG
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_MMProductPayTermList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<ProductPayTermResult>))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_MMProductPayTermList(ProductPayTermRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_MMProductPayTermList");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondProductPayTermList(model.AccountNo, (int)CommonConst.ProductType.MM, model.Amount, tradeDate, model.Term, lang);

                var dataRet = new SuccessListModel<ProductPayTermResult> { RetData = getAwaiterData };

                LogResult(log, dataRet, model.UserId, "BM_IV_MMProductPayTermList");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_MMProductPayTermList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tìm kiếm HDMG thỏa mãn điều kiện
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_FindMMRequest")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<FindBondRequestResult>))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_FindMMRequest(FindBondRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_FindMMRequest");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_FindBondRequest(model.AccountNo, (int)CommonConst.ProductType.MM, model.Amount, tradeDate,
                    model.Term, model.IsPreSale, model.PayTerm, lang);

                var dataRet = new SuccessListModel<FindBondRequestResult> { RetData = getAwaiterData };
                LogResult(log, dataRet, model.UserId, "BM_IV_FindMMRequest");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_FindMMRequest");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tra cứu tỷ suất lợi nhuận mà NĐT nhận được khi chọn các kỳ hạn trái phiếu tương ứng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_MMBuyCalBondRate")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BuyCalRateResult>))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_MMBuyCalBondRate(BuyCalRateRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_MMBuyCalBondRate");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var sellDate = !string.IsNullOrEmpty(model.SellDate) ? DateTime.ParseExact(model.SellDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                        : tradeDate;
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondBuyCalBondRate(model.AccountNo, (int)CommonConst.ProductType.MM, model.Amount, tradeDate,
                    model.Term, model.IsPreSale, model.PayTerm, model.SubBondCode, sellDate, lang);

                var custBankWait = await _bondService.TVSI_sAPI_BM_IV_GetCustBankInfo(model.UserId);

                var calBondInfo = getAwaiterData.FirstOrDefault();
                if (calBondInfo.RetCode == RetCodeConst.SUCCESS_CODE)
                {
                    var custBanInfo = custBankWait.ToList();

                    var buyCalRateResult = new BuyCalRateResult
                    {
                        CalBondInfo = calBondInfo,
                        CalCustBankInfos = custBanInfo
                    };

                    var dataRet = new SuccessModel<BuyCalRateResult> { RetData = buyCalRateResult };
                    LogResult(log, dataRet, model.UserId, "BM_IV_MMBuyCalBondRate");
                    return Ok(dataRet);
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_MM_" + calBondInfo.RetCode);
                    var dataRet = new ErrorModel { RetCode = calBondInfo.RetCode, RetMsg = retMsg };
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_MMBuyCalBondRate");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        [ActionName("BM_IV_MMSellBeforeRateInfos")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<SellBeforeRateInfo>))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_MMSellBeforeRateInfos(SellBeforeRateRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_MMBuyCalBondRate");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_GetMMSellBeforeRateInfos(model.AccountNo, model.SubBondCode, tradeDate, model.RedeemCode, lang);

                var dataRet = new SuccessListModel<SellBeforeRateInfo> { RetData = getAwaiterData };
                LogResult(log, dataRet, model.UserId, "BM_IV_MMSellBeforeRateInfos");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_MMSellBeforeRateInfos");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Validate thông tin lệnh mua của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_MMValidateBuyInfo")]
        [HttpPost]
        [ResponseType(typeof(BuyDataResult))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_MMValidateBuyInfo(ValidateBuyInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_MMValidateBuyInfo");
                var tradeDate = DateTime.ParseExact(model.BuyInfo.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var sellDate = !string.IsNullOrEmpty(model.BuyInfo.SellDate)
                    ? DateTime.ParseExact(model.BuyInfo.SellDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    : DateTime.Today;
                // Kiểm tra tiền
                var cashAmt = 0M;
                var ipgws = new TVSIWS_IPG.IPGWS();
                var dataInfo_01 = ipgws.IPGWS530(model.AccountNo, "BOND_API");
                var ws530 = TVSI.Utility.WebServiceEntitySerializer.Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS530>(dataInfo_01);
                if (ws530?.so_tien > 0)
                    cashAmt = ws530.so_tien;

                // Kiểm tra tiền
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondValidateBuyInfo(model.AccountNo, model.BuyInfo.BondCode, model.BuyInfo.SubBondCode,
                    model.BuyInfo.Volume, tradeDate, model.BuyInfo.Rate, model.BuyInfo.RedeemCode,
                    sellDate, model.BuyInfo.CustBankID, cashAmt, lang);
                var data = getAwaiterData.FirstOrDefault();
                SetLanguage(lang);
                if (data != null)
                {
                    if (data.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<BuyDataResult> { RetData = data };
                        LogResult(log, dataRet, model.UserId, "BM_IV_MMValidateBuyInfo");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_MM_" + data.RetCode);
                        var dataRet = new ErrorModel { RetCode = data.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_MMValidateBuyInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_MMValidateBuyInfo");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Xác nhận thông tin lệnh mua của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_MMConfirmBuyInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ConfirmBuyInfoResult>))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Đầu tư > Màn hình mua", true)]
        public async Task<IHttpActionResult> BM_IV_MMConfirmBuyInfo(ConfirmBuyInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_MMConfirmBuyInfo");
                var tradeDate = DateTime.ParseExact(model.BuyInfo.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var sellDate = !string.IsNullOrEmpty(model.BuyInfo.SellDate)
                    ? DateTime.ParseExact(model.BuyInfo.SellDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    : DateTime.Today;

                // Kiểm tra tiền
                var cashAmt = 0M;
                var ipgws = new TVSIWS_IPG.IPGWS();
                var dataInfo_01 = ipgws.IPGWS530(model.AccountNo, "BOND_API");
                var ws530 = TVSI.Utility.WebServiceEntitySerializer.Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS530>(dataInfo_01);
                if (ws530?.so_tien > 0)
                    cashAmt = ws530.so_tien;

                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondConfirmBuyInfo(model.AccountNo, model.BuyInfo.BondCode, model.BuyInfo.SubBondCode,
                    model.BuyInfo.Volume, tradeDate, model.BuyInfo.Rate, model.BuyInfo.RedeemCode,
                    sellDate, model.BuyInfo.CustBankID, model.BuyInfo.IsHardCopy, cashAmt, lang);
                var data = getAwaiterData.FirstOrDefault();
                SetLanguage(lang);
                if (data != null)
                {
                    if (data.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<ConfirmBuyInfoResult> { RetData = data };
                        LogResult(log, dataRet, model.UserId, "BM_IV_MMConfirmBuyInfo");
                        try
                        {
                            var dic = new Dictionary<string, string>
                            {
                                {"{CustName}", ""},
                                {"{AccountNo}",model.AccountNo},
                                {"{TradeDate}", tradeDate.ToString("dd/MM/yyyy")},
                                {"{DateNow}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")},
                                {"{CouponDate}", tradeDate.ToString("dd/MM/yyyy")},
                                {"{SellDate}", dataRet.RetData.SellDate.ToString("dd/MM/yyyy")},
                                {"{BondCode}", dataRet.RetData.BondCode },
                                {"{BondName}", dataRet.RetData.Issuer },
                                {"{Term}", "" },
                                {"{Volume}", TVSI.Utility.Functions.FormatVolume(dataRet.RetData.Volume) },
                                {"{OpenPrice}", TVSI.Utility.Functions.FormatVolume(dataRet.RetData.Price) },
                                {"{Amount}", TVSI.Utility.Functions.FormatVolume(dataRet.RetData.Amount) },
                                {"{Rate}", dataRet.RetData.CashDataInfo.FirstOrDefault(x=>x.RowCode =="Yield").Amount },
                                {"{PIT}", TVSI.Utility.Functions.FormatVolume(dataRet.RetData.PIT)},
                                {"{SaleID}", ""},
                                {"{RealAmount}", TVSI.Utility.Functions.FormatVolume(dataRet.RetData.Amount)},
                                {"{Status}", "Chờ xử lý" }
                            };
                        }
                        catch (Exception ex)
                        {

                        }
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_MM_" + data.RetCode);
                        var dataRet = new ErrorModel { RetCode = data.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_MMConfirmBuyInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_MMConfirmBuyInfo");
                    
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_MMConfirmBuyInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Validate thông tin lệnh bán của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_MMValidateSellInfo")]
        [HttpPost]
        [ResponseType(typeof(ValidateSellInfoResult))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Đầu tư > Màn hình mua > Xác nhận lệnh bán", true)]
        public async Task<IHttpActionResult> BM_IV_MMValidateSellInfo(ValidateSellInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_MMValidateSellInfo");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondValidateSellInfo(model.AccountNo, model.ContractNo, tradeDate, model.Volume, model.RedeemCode, model.CustBankID, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<ValidateSellInfoResult> { RetData = returnData };
                        LogResult(log, dataRet, model.UserId, "BM_IV_MMValidateSellInfo");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_MM_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_MMValidateSellInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_MMValidateSellInfo");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_BondValidateSellInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Xác nhận thông tin lệnh bán của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_MMConfirmSellInfo")]
        [HttpPost]
        [ResponseType(typeof(ConfirmSellInfoResult))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Đầu tư > Màn hình mua > Xác nhận lệnh bán", true)]
        public async Task<IHttpActionResult> BM_IV_MMConfirmSellInfo(ConfirmSellInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_MMConfirmSellInfo");
                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondConfirmSellInfo(model.AccountNo, model.ContractNo, tradeDate,
                    model.Volume, model.RedeemCode, model.CustBankID, model.IsHardCopy, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<ConfirmSellInfoResult> { RetData = returnData };
                        LogResult(log, dataRet, model.UserId, "BM_IV_MMConfirmSellInfo");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_MM_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_MMConfirmSellInfo");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_MMConfirmSellInfo");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_MMConfirmSellInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        
        /// <summary>
        /// Validate gia hạn hợp đồng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_MMValidateRollBack")]
        [HttpPost]
        [ResponseType(typeof(ValidateRollInfoResult))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Đầu tư > Button Gia hạn", false)]
        public async Task<IHttpActionResult> BM_IV_MMValidateRollBack(ValidateRollInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_MMValidateRollBack");
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondValidateRollInfo(model.AccountNo, model.ContractNo, model.RedeemCode, model.CustBankID, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<ValidateRollInfoResult> { RetData = returnData };
                        LogResult(log, dataRet, model.UserId, "BM_IV_MMValidateRollBack");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_MM_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_MMValidateRollBack");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_MMValidateRollBack");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_MMValidateRollBack");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Xác nhận gia hạn hợp đồng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_MMConfirmRollBack")]
        [HttpPost]
        [ResponseType(typeof(ConfirmRollInfoResult))]
        [Note("Màn hình SPTC > Tab HDMG > Tab Đầu tư > Button Gia hạn", true)]
        public async Task<IHttpActionResult> BM_IV_MMConfirmRollBack(ConfirmRollInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_MMConfirmRollBack");
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_BondConfirmRollInfo(model.AccountNo, model.ContractNo,
                    model.RedeemCode, model.CustBankID, model.IsHardCopy, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<ConfirmRollInfoResult> { RetData = returnData };
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_MM_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_MMConfirmRollBack");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        [ActionName("BM_IV_CustBankContract_GetActiveContract")]
        [HttpPost]
        [ResponseType(typeof(CustBankContract_ActiveContractItemResult))]
        [Note("Màn hình SPTC > Thay đổi thông tin TK thanh toán HĐ", true)]
        public async Task<IHttpActionResult> BM_IV_CustBankContract_GetActiveContract(CustBankContract_GetActiveContract model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_CustBankContract_GetActiveContract");
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_CustBankContract_GetActiveContract(model.AccountNo, model.BondTypeGroup, lang);
                var returnData = getAwaiterData.ToList();

                SetLanguage(lang);
                if (returnData != null)
                {
                    CustBankContract_ActiveContractItemResult data = new CustBankContract_ActiveContractItemResult();
                    var data1 = (from a in returnData
                                 select new
                                 {
                                     a.CustBankID,
                                     a.BankName,
                                     a.SubBranchName,
                                     a.BankAccount,
                                     a.BankAccountName
                                 }).Distinct().ToList();

                    List<CustBankContract_ActiveContractItem> items = new List<CustBankContract_ActiveContractItem>();

                    foreach (var item in data1)
                    {
                        items.Add(new CustBankContract_ActiveContractItem
                        {
                            CustBankID = item.CustBankID,
                            BankName = item.BankName,
                            SubBranchName = item.SubBranchName,
                            BankAccount = item.BankAccount,
                            BankAccountName = item.BankAccountName,
                            ContractInfos = returnData.Where(x => x.CustBankID == item.CustBankID).ToList()
                        });
                    }
                    data.Items = items;
                    var custBankWait = await _bondService.TVSI_sAPI_BM_IV_GetCustBankInfo(model.UserId);
                    data.CustBankInfos = custBankWait.ToList();
                    var dataRet = new SuccessModel<CustBankContract_ActiveContractItemResult> { RetData = data };
                    return Ok(dataRet);
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoData");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_CustBankContract_GetActiveContract");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        [ActionName("BM_IV_CustBankContract_Confirm")]
        [HttpPost]
        [ResponseType(typeof(CustBankContract_ConfirmResult))]
        [Note("Màn hình SPTC > Xác nhận thay đổi thông tin TK thanh toán HĐ", true)]
        public async Task<IHttpActionResult> BM_IV_CustBankContract_Confirm(CustBankContract_ConfrimRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_CustBankContract_Confirm");
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_CustBankContract_Confirm(model.ContractIds, model.CustBankId, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                SetLanguage(lang);
                if (returnData != null)
                {
                    if (returnData.RetCode == RetCodeConst.SUCCESS_CODE)
                    {
                        var dataRet = new SuccessModel<CustBankContract_ConfirmResult> { RetData = returnData };
                        LogResult(log, dataRet, model.UserId, "BM_IV_CustBankContract_Confirm");
                        return Ok(dataRet);
                    }
                    else
                    {
                        var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_CBC_" + returnData.RetCode);
                        var dataRet = new ErrorModel { RetCode = returnData.RetCode, RetMsg = retMsg };
                        LogResult(log, dataRet, model.UserId, "BM_IV_CustBankContract_Confirm");
                        return Ok(dataRet);
                    }
                }
                else
                {
                    var retMsg = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_NoValid");
                    var dataRet = new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = retMsg };
                    LogResult(log, dataRet, model.UserId, "BM_IV_CustBankContract_Confirm");
                    return Ok(dataRet);
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_CustBankContract_Confirm");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        [ActionName("BM_IV_CustBankContract_GetList")]
        [HttpPost]
        [ResponseType(typeof(CustBankContract_GetListResult))]
        [Note("Màn hình SPTC > Lịch sử thay đổi thông tin TK thanh toán HĐ", true)]
        public async Task<IHttpActionResult> BM_IV_CustBankContract_GetList(CustBankContract_GetListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_CustBankContract_GetList");
                SetLanguage(lang);
                var fromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var data = await _bondService.TVSI_sAPI_BM_IV_CustBankContract_GetList(model.AccountNo, fromDate, toDate, lang);

                var retModel = new SuccessListModel<CustBankContract_GetListResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "BM_IV_CustBankContract_GetList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_CustBankContract_GetList");
                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.ERR999_SYSTEM_ERROR
                });
            }
        }

        [ActionName("BM_IV_CustBankContract_GetDetail")]
        [HttpPost]
        [ResponseType(typeof(CustBankContract_GetDetailResult))]
        [Note("Màn hình SPTC > Lịch sử thay đổi thông tin TK thanh toán HĐ", true)]
        public async Task<IHttpActionResult> BM_IV_CustBankContract_GetDetail(CustBankContract_GetDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_CustBankContract_GetDetail");
                SetLanguage(lang);
                var data = await _bondService.TVSI_sAPI_BM_IV_CustBankContract_GetDetail(model.id, lang);

                var retModel = new SuccessListModel<CustBankContract_GetDetailResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "BM_IV_CustBankContract_GetDetail");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_CustBankContract_GetDetail");
                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.ERR999_SYSTEM_ERROR
                });
            }
        }



        /// <summary>
        /// View xác nhận HĐ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_IV_ViewConfirmationBuyContractDataPrintInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<PrintContractDataInfo>>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > View xác nhận HĐ mua", true)]
        public async Task<IHttpActionResult> BM_IV_ViewConfirmationBuyContractDataPrintInfo(ConfirmationInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_ViewConfirmationBuyContractDataPrintInfo");
                var data = new List<PrintContractDataInfo>();

                var dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };

                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_ViewConfirmationContractBuyPrintInfo(model.RequestOrderID, lang);
                var dataPrint = getAwaiterData.ToList();
                SetLanguage(lang);
                if (dataPrint != null)
                {
                    var productTypeInfo = dataPrint.FirstOrDefault(x => x.Code == "ProductType");
                    var presaleInfo = dataPrint.FirstOrDefault(x => x.Code == "IsPreSale");
                    var sideInfo = dataPrint.FirstOrDefault(x => x.Code == "Side");
                    var mapPath = HostingEnvironment.MapPath("~");
                    if (productTypeInfo != null)
                    {
                        var templateName = "";
                        var title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_RecommendBuy");
                        if (productTypeInfo.CodeValue == "HDMG")
                        {
                            title = ResourceFile.BondModule.ResourceManager.GetString("MM_TitlePrint_RecommendBuy");
                            templateName = @"Buy_DeNghiHopDongMoiGioi.html";
                            if (presaleInfo.CodeValue == "1")
                                templateName = @"Buy_DeNghiHopDongMoiGioi_CoBanTruocHan.html";

                            if (sideInfo.CodeValue == "C")
                            {
                                templateName = @"CancelBuy.html";
                                title = ResourceFile.BondModule.ResourceManager.GetString("MM_TitlePrint_RecommendCancelBuy");
                            }

                            var content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\" + templateName, Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);

                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });
                        }
                        else
                        {
                            templateName = @"\Buy_DeNghiMua.html";

                            if (sideInfo.CodeValue == "C")
                            {
                                templateName = @"CancelBuy.html";
                                title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_RecommendCancelBuy");
                            }
                            var content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\" + templateName, Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);

                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });


                            if (productTypeInfo.CodeValue != "OUTRIGHT" && sideInfo.CodeValue != "C")
                            {
                                if (presaleInfo != null && presaleInfo.CodeValue == "1" && productTypeInfo.CodeValue == "FIX-FLEX")
                                    content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Buy_DeNghiBan_CoBanTruocHan.html", Encoding.UTF8);
                                else
                                    content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Buy_DeNghiBan.html", Encoding.UTF8);

                                content = ReplacePrintData(dataPrint, content, lang);

                                title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_RecommendSell");
                                data.Add(new PrintContractDataInfo
                                {
                                    Title = title,
                                    Content = content
                                });
                            }

                            title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_Confirm");
                            content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\XacNhanGiaoDich.html", Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });
                        }
                    }
                }
                dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };
                LogResult(log, dataRet, model.UserId, "BM_IV_ViewConfirmationBuyContractDataPrintInfo");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_ViewConfirmationBuyContractDataPrintInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        [ActionName("BM_IV_ViewConfirmationRollContractDataPrintInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<PrintContractDataInfo>>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > View xác nhận HĐ Gia hạn", false)]
        public async Task<IHttpActionResult> BM_IV_ViewConfirmationRollContractDataPrintInfo(ConfirmationInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_ViewConfirmationRollContractDataPrintInfo");
                var data = new List<PrintContractDataInfo>();
                var dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };

                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_ViewConfirmationContractRollPrintInfo(model.RequestOrderID, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                var dataPrint = getAwaiterData.ToList();

                SetLanguage(lang);
                if (dataPrint != null)
                {
                    var productTypeInfo = dataPrint.FirstOrDefault(x => x.Code == "ProductType");
                    var mapPath = HostingEnvironment.MapPath("~");
                    if (productTypeInfo != null)
                    {
                        if (productTypeInfo.CodeValue == "HDMG")
                        {
                            var content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Roll_DeNghiGiaHan.html", Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);
                            var title = ResourceFile.BondModule.ResourceManager.GetString("MM_TitlePrint_RecommendRoll");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });
                        }
                        else
                        {
                            var content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Roll_DeNghiGiaHan.html", Encoding.UTF8);

                            content = ReplacePrintData(dataPrint, content, lang);
                            var title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_RecommendRoll");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });

                            content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\XacNhanGiaoDich.html", Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);
                            title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_Confirm");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });
                        }
                    }
                }
                dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };
                LogResult(log, dataRet, model.UserId, "BM_IV_ViewConfirmationRollContractDataPrintInfo");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_ViewConfirmationRollContractDataPrintInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        [ActionName("BM_IV_ViewConfirmationSellContractDataPrintInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<PrintContractDataInfo>>))]
        [Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > View xác nhận HĐ bán", false)]
        public async Task<IHttpActionResult> BM_IV_ViewConfirmationSellContractDataPrintInfo(ConfirmationInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_ViewConfirmationSellContractDataPrintInfo");
                var data = new List<PrintContractDataInfo>();
                var dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };
                
                var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_ViewConfirmationContractSellPrintInfo(model.RequestOrderID, lang);
                var returnData = getAwaiterData.FirstOrDefault();

                var dataPrint = getAwaiterData.ToList();

                SetLanguage(lang);
                if (dataPrint != null)
                {
                    var productTypeInfo = dataPrint.FirstOrDefault(x => x.Code == "ProductType");
                    var sellAll = dataPrint.FirstOrDefault(x => x.Code == "IsSellAll");
                    var isAdvance = dataPrint.FirstOrDefault(x => x.Code == "IsAdvance");
                    var isPaymentInfo = dataPrint.FirstOrDefault(x => x.Code == "IsPaymentInfo");
                    var mapPath = HostingEnvironment.MapPath("~");
                    if (productTypeInfo != null)
                    {
                        var content = "";
                        if (productTypeInfo.CodeValue == "HDMG")
                        {
                            if (sellAll != null && sellAll.CodeValue == "1")
                                content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanToanBo.html", Encoding.UTF8);
                            else
                                content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanMotPhan.html", Encoding.UTF8);
                            if(isPaymentInfo!=null && isPaymentInfo.CodeValue == "1")
                            {
                                if (sellAll != null && sellAll.CodeValue == "1")
                                    content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanToanBo_ChiaDongTien.html", Encoding.UTF8);
                                else
                                    content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanMotPhan_ChiaDongTien.html", Encoding.UTF8);
                            }
                            content = ReplacePrintData(dataPrint, content, lang);
                            var title = ResourceFile.BondModule.ResourceManager.GetString("MM_TitlePrint_RecommendSell");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });
                        }
                        else
                        {
                            if (sellAll != null && sellAll.CodeValue == "1")
                                content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanToanBo.html", Encoding.UTF8);
                            else
                                content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanMotPhan.html", Encoding.UTF8);

                            if (isPaymentInfo != null && isPaymentInfo.CodeValue == "1")
                            {
                                if (sellAll != null && sellAll.CodeValue == "1")
                                    content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanToanBo_ChiaDongTien.html", Encoding.UTF8);
                                else
                                    content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanMotPhan_ChiaDongTien.html", Encoding.UTF8);
                            }
                            content = ReplacePrintData(dataPrint, content, lang);
                            var title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_RecommendSell");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = title,
                                Content = content
                            });

                            if (isAdvance != null && isAdvance.CodeValue == "1")
                            {
                                content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_BanCoUngTruoc.html", Encoding.UTF8);
                                content = ReplacePrintData(dataPrint, content, lang);
                                title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_Advance");
                                data.Add(new PrintContractDataInfo
                                {
                                    Title = "Hoàn trả ứng",
                                    Content = content
                                });
                            }

                            content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\XacNhanGiaoDich.html", Encoding.UTF8);
                            content = ReplacePrintData(dataPrint, content, lang);
                            title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_Confirm");
                            data.Add(new PrintContractDataInfo
                            {
                                Title = "Xác nhận GD",
                                Content = content
                            });
                        }
                    }
                }
                dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };
                LogResult(log, dataRet, model.UserId, "BM_IV_ViewConfirmationSellContractDataPrintInfo");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_ViewConfirmationSellContractDataPrintInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        //[ActionName("BM_IV_ViewRollContractDataPrintInfo")]
        //[HttpPost]
        //[ResponseType(typeof(SuccessModel<List<PrintContractDataInfo>>))]
        //[Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > View HĐ Gia hạn", false)]
        //public async Task<IHttpActionResult> BM_IV_ViewRollContractDataPrintInfo(ConfirmRollInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        //{
        //    try
        //    {
        //        LogRequest(log, model, "BM_IV_ViewRollContractDataPrintInfo");
        //        var data = new List<PrintContractDataInfo>();
        //        var dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };

        //        var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_ViewContractRollPrintInfo(model.AccountNo, model.ContractNo,
        //            model.RedeemCode, model.CustBankID, lang);
        //        var returnData = getAwaiterData.FirstOrDefault();

        //        var dataPrint = getAwaiterData.ToList();

        //        SetLanguage(lang);
        //        if (dataPrint != null)
        //        {
        //            var productTypeInfo = dataPrint.FirstOrDefault(x => x.Code == "ProductType");
        //            var mapPath = HostingEnvironment.MapPath("~");
        //            if (productTypeInfo != null)
        //            {
        //                if (productTypeInfo.CodeValue == "HDMG")
        //                {
        //                    var content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Roll_DeNghiGiaHan.html", Encoding.UTF8);
        //                    content = ReplacePrintData(dataPrint, content, lang);
        //                    var title = ResourceFile.BondModule.ResourceManager.GetString("MM_TitlePrint_RecommendRoll");
        //                    data.Add(new PrintContractDataInfo
        //                    {
        //                        Title = title,
        //                        Content = content
        //                    });
        //                }
        //                else
        //                {
        //                    var content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Roll_DeNghiGiaHan.html", Encoding.UTF8);

        //                    content = ReplacePrintData(dataPrint, content, lang);
        //                    var title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_RecommendRoll");
        //                    data.Add(new PrintContractDataInfo
        //                    {
        //                        Title = title,
        //                        Content = content
        //                    });

        //                    content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\XacNhanGiaoDich.html", Encoding.UTF8);
        //                    content = ReplacePrintData(dataPrint, content, lang);
        //                    title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_Confirm");
        //                    data.Add(new PrintContractDataInfo
        //                    {
        //                        Title = title,
        //                        Content = content
        //                    });
        //                }
        //            }
        //        }
        //        dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };
        //        LogResult(log, dataRet, "BM_IV_ViewRollContractDataPrintInfo");
        //        return Ok(dataRet);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogException(log, model, ex, "BM_IV_ViewRollContractDataPrintInfo");
        //        return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
        //    }
        //}
        //[ActionName("BM_IV_ViewSellContractDataPrintInfo")]
        //[HttpPost]
        //[ResponseType(typeof(SuccessModel<List<PrintContractDataInfo>>))]
        //[Note("Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > View HĐ bán", false)]
        //public async Task<IHttpActionResult> BM_IV_ViewSellContractDataPrintInfo(ConfirmSellInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        //{
        //    try
        //    {
        //        LogRequest(log, model, "BM_IV_ViewSellContractDataPrintInfo");
        //        var data = new List<PrintContractDataInfo>();
        //        var dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };

        //        var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //        var getAwaiterData = await _bondService.TVSI_sAPI_BM_IV_ViewContractSellPrintInfo(model.AccountNo, model.ContractNo, tradeDate,
        //            model.Volume, model.RedeemCode, model.CustBankID, lang);
        //        var returnData = getAwaiterData.FirstOrDefault();

        //        var dataPrint = getAwaiterData.ToList();

        //        SetLanguage(lang);
        //        if (dataPrint != null)
        //        {
        //            var productTypeInfo = dataPrint.FirstOrDefault(x => x.Code == "ProductType");
        //            var sellAll = dataPrint.FirstOrDefault(x => x.Code == "IsSellAll");
        //            var isAdvance = dataPrint.FirstOrDefault(x => x.Code == "IsAdvance");
        //            var mapPath = HostingEnvironment.MapPath("~");
        //            if (productTypeInfo != null)
        //            {
        //                var content = "";
        //                if (productTypeInfo.CodeValue == "HDMG")
        //                {
        //                    if (sellAll != null && sellAll.CodeValue == "1")
        //                        content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanToanBo.html", Encoding.UTF8);
        //                    else
        //                        content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanMotPhan.html", Encoding.UTF8);

        //                    content = ReplacePrintData(dataPrint, content, lang);
        //                    var title = ResourceFile.BondModule.ResourceManager.GetString("MM_TitlePrint_RecommendSell");
        //                    data.Add(new PrintContractDataInfo
        //                    {
        //                        Title = title,
        //                        Content = content
        //                    });
        //                }
        //                else
        //                {
        //                    if (sellAll != null && sellAll.CodeValue == "1")
        //                        content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanToanBo.html", Encoding.UTF8);
        //                    else
        //                        content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_DeNghiBanMotPhan.html", Encoding.UTF8);

        //                    content = ReplacePrintData(dataPrint, content, lang);
        //                    var title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_RecommendSell");
        //                    data.Add(new PrintContractDataInfo
        //                    {
        //                        Title = title,
        //                        Content = content
        //                    });

        //                    if (isAdvance != null && isAdvance.CodeValue == "1")
        //                    {
        //                        content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\Sell_BanCoUngTruoc.html", Encoding.UTF8);
        //                        content = ReplacePrintData(dataPrint, content, lang);
        //                        title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_Advance");
        //                        data.Add(new PrintContractDataInfo
        //                        {
        //                            Title = "Hoàn trả ứng",
        //                            Content = content
        //                        });
        //                    }

        //                    content = File.ReadAllText(mapPath + @"\Templates\" + productTypeInfo.CodeValue + @"\XacNhanGiaoDich.html", Encoding.UTF8);
        //                    content = ReplacePrintData(dataPrint, content, lang);
        //                    title = ResourceFile.BondModule.ResourceManager.GetString("MBMIV_TitlePrint_Confirm");
        //                    data.Add(new PrintContractDataInfo
        //                    {
        //                        Title = "Xác nhận GD",
        //                        Content = content
        //                    });
        //                }
        //            }
        //        }
        //        dataRet = new SuccessModel<List<PrintContractDataInfo>> { RetData = data };
        //        LogResult(log, dataRet, "BM_IV_ViewSellContractDataPrintInfo");
        //        return Ok(dataRet);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogException(log, model, ex, "BM_IV_ViewSellContractDataPrintInfo");
        //        return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
        //    }
        //}


        #endregion

        [ActionName("BM_IV_GetMaturityDate")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<DateTime>))]
        [Note("Lấy ngày đáo hạn")]
        public async Task<IHttpActionResult> BM_IV_GetMaturityDate(GetMaturityDateRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BM_IV_GetMaturityDate");
                var tradeDate = DateTime.Today;
                if (!string.IsNullOrEmpty(model.TradeDate))
                {
                    tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                var maturityDate = await _bondService.TVSI_sAPI_BM_IV_GetMaturityDate(model.ProductType, model.Term, tradeDate);

                var dataRet = new SuccessModel<DateTime> { RetData = maturityDate };

                LogResult(log, dataRet, model.UserId, "BM_IV_GetMaturityDate");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BM_IV_GetMaturityDate");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}
