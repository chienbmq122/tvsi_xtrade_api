﻿using System;
using System.Data.SqlTypes;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.TransactionHistory;
using TVSI.Bond.WebAPI.Lib.Services;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// Sao kê API
    /// </summary>
    public class TransactionHistoryController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(TransactionHistoryController));
        private ITransactionHistoryService _transHistoryService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="transactionHistoryService"></param>
        public TransactionHistoryController(ITransactionHistoryService transactionHistoryService)
        {
            _transHistoryService = transactionHistoryService;
        }

        /// <summary>
        /// Lấy danh sách Sao kê Chứng khoán (Sao kê tổng)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /TransactionHistory/TM_SH_StockTransHist
        ///     {
        ///         "FromDate": "04/12/2020",
        ///         "ToDate": "08/12/2020",
        ///         "UserId": "044007",
        ///         "AccountNo": "0440071"
        ///     }
        ///
        /// </remarks>
        [ActionName("TM_SH_StockTransHist")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<StockTransactionHistResponse>))]
        [Note("Màn hình sao kê chứng khoán > Tab sao kê tổng")]
        public async Task<IHttpActionResult> TM_SH_StockTransHist(StockTransactionHistRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TM_SH_StockTransHist");
                SetLanguage(lang);

                var dtxFromDate = SqlDateTime.MinValue;
                var dtxToDate = SqlDateTime.MaxValue;

                if (!string.IsNullOrEmpty(model.FromDate))
                    dtxFromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(model.ToDate))
                    dtxToDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _transHistoryService.GetStockTransactionHistList(model.AccountNo, (DateTime) dtxFromDate, (DateTime)dtxToDate);

                var retModel = new SuccessListModel<StockTransactionHistResponse> { RetData = data };
                LogResult(log, retModel, model.UserId, "TM_SH_StockTransHist");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TM_SH_StockTransHist");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách Sao kê Chứng khoán (Sao kê chi tiết)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TM_SH_StockTransDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<StockTransactionDetailResponse>))]
        [Note("Màn hình sao kê chứng khoán > Tab sao kê chi tiết")]
        public async Task<IHttpActionResult> TM_SH_StockTransDetail(StockTransactionDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TM_SH_StockTransDetail");
                SetLanguage(lang);

                var data = await _transHistoryService.GetStockTransactionDetailList(model);

                var retModel = new SuccessListModel<StockTransactionDetailResponse> { RetData = data };
                LogResult(log, retModel, model.UserId, "TM_SH_StockTransDetail");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TM_SH_StockTransDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách lịch sử đặt lênh
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /TransactionHistory/TM_OH_OrderHist
        ///     {
        ///       "FromDate": "12/05/2022",
        ///       "ToDate": "24/05/2022",
        ///       "Symbol": "",
        ///       "Side": "",
        ///       "Status": -1,
        ///       "Chanel": "",
        ///       "PageIndex": 1,
        ///       "PageSize": 10,
        ///       "UserId": "003001",
        ///       "AccountNo": "0030016"
        ///     }
        ///
        /// </remarks>
        [ActionName("TM_OH_OrderHist")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<OrderHistTransResponse>))]
        [Note("Màn hình Lịch sử đặt lệnh")]
        public async Task<IHttpActionResult> TM_OH_OrderHist(OrderHistTransRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TM_OH_OrderHist");
                SetLanguage(lang);

                var data = await _transHistoryService.GetOrderHistTransList(model);

                var retModel = new SuccessListModel<OrderHistTransResponse> { RetData = data };
                LogResult(log, retModel, model.UserId, "TM_OH_OrderHist");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TM_OH_OrderHist");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}
