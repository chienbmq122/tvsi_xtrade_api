﻿using System;
using System.Data.SqlTypes;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.Right;
using TVSI.Bond.WebAPI.Lib.Services;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// Thực hiện quyền API
    /// </summary>
    public class RightController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(OrderController));

        private IRightService _rightService;
       
        /// <summary>
        /// Controller Constructor
        /// </summary>
        public RightController(IRightService rightService)
        {
            _rightService = rightService;
        }

       
        /// <summary>
        /// Lấy danh sách thực hiện quyền đang có của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("RM_RI_RightInfoByAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<RightInfoResult>))]
        [Note("Màn hình thực hiện quyền > Tab Thông tin quyền")]
        public async Task<IHttpActionResult> RM_RI_RightInfoByAccount(RightInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "RM_RI_RightInfoByAccount");
                SetLanguage(lang);

                var data = await _rightService.GetRightInfo(model.AccountNo);

                var retModel = new SuccessModel<RightInfoResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "RM_RI_RightInfoByAccount");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "RM_RI_RightInfoByAccount");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Đăng ký mua
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("RM_RB_RegistBuyRight")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình thực hiện quyền > Tab Thông tin quyền")]
        public async Task<IHttpActionResult> RM_RB_RegistBuyRight(RegistBuyRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "RM_RB_RegistBuyRight");
                SetLanguage(lang);

                var retInt = await _rightService.RegistBuy(model, src);
               
                if (retInt == 1)
                {
                    var retModel = new SuccessModel();
                    LogResult(log, retModel, model.UserId, "RM_RB_RegistBuyRight");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel("002", "Không tìm thấy thông tin quyền mua"));

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "RM_RB_RegistBuyRight");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Hủy Đăng ký mua
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("RM_RB_CancelRegistBuyRight")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình thực hiện quyền > Tab Trạng thái quyền mua")]
        public async Task<IHttpActionResult> RM_RB_CancelRegistBuyRight(CancelRegistBuyRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "RM_RB_CancelRegistBuyRight");
                SetLanguage(lang);

                var retInt = await _rightService.CancelRegistBuy(model.AccountNo, model.ContractNo);
                
                if (retInt == 1)
                {
                    var retModel = new SuccessModel();
                    LogResult(log, retModel, model.UserId, "RM_RB_CancelRegistBuyRight");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel("002", "Không tìm thấy thông tin quyền mua"));

            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "RM_RB_CancelRegistBuyRight");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Chuyển nhượng quyền mua
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("RM_TB_TransferBuyRight")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình thực hiện quyền > Tab Thông tin quyền")]
        public async Task<IHttpActionResult> RM_TB_TransferBuyRight(TransferBuyRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "RM_TB_TransferBuyRight");
                SetLanguage(lang);


                var retInt = await _rightService.TransferBuy(model, src);
        
                if (retInt == 1)
                {
                    var retModel = new SuccessModel();
                    LogResult(log, retModel, model.UserId, "RM_TB_TransferBuyRight");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel("002", "Không tìm thấy thông tin quyền mua"));
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "RM_RB_TransferBuyRight");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách trạng thái thực hiện quyền mua của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("RM_RS_RightStatusByAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<RightStatusResult>))]
        [Note("Màn hình thực hiện quyền > Tab Trạng thái quyền")]
        public async Task<IHttpActionResult> RM_RS_RightStatusByAccount(RightStatusRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "RM_RS_RightStatusByAccount");
                SetLanguage(lang);

                var data = await _rightService.GetRightStatus(model.AccountNo, model.PageIndex, model.PageSize);

                var retModel = new SuccessListModel<RightStatusResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "RM_RS_RightStatusByAccount");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "RM_RS_RightStatusByAccount");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Lấy danh sách trạng thái thực hiện quyền mua của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("RM_RH_RightHistByAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<RightHistResult>))]
        [Note("Màn hình thực hiện quyền > Tab lịch sử quyền mua")]
        public async Task<IHttpActionResult> RM_RH_RightHistByAccount(RightHistRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "RM_RH_RightHistByAccount");
                SetLanguage(lang);

                var dtxFromDate = SqlDateTime.MinValue;
                var dtxToDate = SqlDateTime.MaxValue;

                if (!string.IsNullOrEmpty(model.FromDate))
                    dtxFromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(model.ToDate))
                    dtxToDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _rightService.GetRightHist(model.AccountNo, model.Symbol, model.XType, (DateTime)dtxFromDate, 
                    (DateTime)dtxToDate, model.PageIndex, model.PageSize);

                var retModel = new SuccessListModel<RightHistResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "RM_RH_RightHistByAccount");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "RM_RH_RightHistByAccount");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
    }


}
