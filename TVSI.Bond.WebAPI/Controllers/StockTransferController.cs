﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Lib.Models.StockTransfer;
using TVSI.Bond.WebAPI.Lib.Services;

namespace TVSI.Bond.WebAPI.Controllers
{
    public class StockTransferController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(StockTransferController));
        private IStockTransferService _stockTransferService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="StockTransferController"></param>
        public StockTransferController(IStockTransferService stockTransferService)
        {
            _stockTransferService = stockTransferService;
        }

        /// <summary>
        /// Lấy thông tin chứng khoán có thể chuyển
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("ST_CI_GetAvailStockTransferList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<AvailStockTransferInfoResult>))]
        [Note("GD chứng khoán > Chuyển chứng khoán", true)]
        public async Task<IHttpActionResult> ST_CI_GetAvailStockTransferList(AvailStockTransferListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "ST_CI_GetAvailStockTransferList");

                var listInfo = await _stockTransferService.GetAvailStockTransferList(model.AccountNo, "", "");


                var dataRet = new SuccessListModel<AvailStockTransferInfoResult> { RetData = listInfo };
                LogResult(log, dataRet, model.UserId, "ST_CI_GetAvailStockTransferList");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "ST_CI_GetAvailStockTransferList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin chứng khoán có thể chuyển theo mã
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("ST_CI_GetAvailStockTransferInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AvailStockTransferInfoResult>))]
        [Note("GD chứng khoán > Chuyển chứng khoán", false)]
        public async Task<IHttpActionResult> ST_CI_GetAvailStockTransferInfo(AvailStockTransferInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "ST_CI_GetAvailStockTransferInfo");

                var data = await _stockTransferService.GetAvailStockTransferList(model.AccountNo, model.ShareCode, model.Purpose);

                var dataRet = new SuccessModel<AvailStockTransferInfoResult> { RetData = data.FirstOrDefault() };
                LogResult(log, dataRet, model.UserId, "ST_CI_GetAvailStockTransferInfo");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "ST_CI_GetAvailStockTransferInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Thêm mới lệnh chuyển chứng khoán
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("ST_CI_AddStockTransfer")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AddStockTransferResult>))]
        [Note("GD chứng khoán > Chuyển chứng khoán", true)]
        public async Task<IHttpActionResult> ST_CI_AddStockTransfer(AddStockTransferRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "ST_CI_AddStockTransfer");
                var data = new AddStockTransferResult
                {
                    AccountNo = model.AccountNo,
                    ToAccount = model.ToAccount,
                    RetCode = "000",
                    Message = "SUCCESS",
                    RefNo = "TVSI0001",
                    SQE = -1,
                    TradeDate = DateTime.Today.ToString("yyyyMMdd")
                };

                if (model.AccountNo == model.ToAccount)
                {
                    data.RetCode = "999";
                    data.Message = "SO TK NHAN VA SO TK CHUYEN TRUNG NHAU";
                }
                if(model.AccountNo.Substring(0,6) == model.ToAccount.Substring(0, 6))
                {
                    data.RetCode = "999";
                    data.Message = "CHI DUOC PHEP CHUYEN CHUNG KHOAN NOI BO";
                }

                if (data.RetCode != "000")
                    data = await _stockTransferService.AddStockTransfer(model);

                var dataRet = new SuccessModel<AddStockTransferResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "ST_CI_AddStockTransfer");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "ST_CI_AddStockTransfer");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sac
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("ST_CI_GetStockTransferHist")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<StockTransferInfoResult>))]
        [Note("GD chứng khoán > Chuyển chứng khoán", true)]
        public async Task<IHttpActionResult> ST_CI_GetStockTransferHist(StockTransferHistRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "ST_CI_GetStockTransferHist");

                var data = await _stockTransferService.GetStockTransferHist(model.AccountNo, model.FromDate, model.ToDate, lang);

                var dataRet = new SuccessListModel<StockTransferInfoResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "ST_CI_GetStockTransferHist");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "ST_CI_GetStockTransferHist");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}