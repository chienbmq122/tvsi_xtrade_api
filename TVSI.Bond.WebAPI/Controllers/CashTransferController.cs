﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.CashTransfer;
using TVSI.Bond.WebAPI.Lib.Services;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// CashTransfer API
    /// </summary>
    public class CashTransferController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(CashTransferController));
        private ICashTransferService _cashTransferService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="cashTransferService"></param>
        public CashTransferController(ICashTransferService cashTransferService)
        {
            _cashTransferService = cashTransferService;
        }

        /// <summary>
        /// Lấy thông tin tỷ lệ sau rút
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CT_CI_AfterWithdrawalInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AfterWithdrawalInfoResult>))]
        [Note("Màn hình GD tiền > Chuyển tiền", true)]
        public async Task<IHttpActionResult> CT_CI_AfterWithdrawalInfo(AfterWithdrawalInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CI_AfterWithdrawalInfo");

                var data = await _cashTransferService.GetAfterWithdrawalInfo(model.AccountNo, model.Amount);
                
                var dataRet = new SuccessModel<AfterWithdrawalInfoResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "CT_CI_AfterWithdrawalInfo");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_CI_AfterWithdrawalInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        

        /// <summary>
        /// Lấy thông tin chuyển tiền ngân hàng của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_CI_CustCashTransferInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CashTransferInfoResult>))]
        [Note("Màn hình GD tiền > Chuyển tiền")]
        public async Task<IHttpActionResult> CT_CI_CustCashTransferInfo(CashTransferInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CI_CustCashTransferInfo");
                
                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var bankInfo = await _cashTransferService.GetBankInfo(model.UserId);

                var internalAccInfo = await _cashTransferService.GetInternalAccountInfo(model.UserId, model.AccountNo);

                var blockFeeList = _cashTransferService.GetBlockFeeInfos(model.AccountNo, lang);

                //var cashBalance = await _cashTransferService.GetBalance(model.AccountNo);
                var withdraw = 0m;
                var cashBalance = 0m;
                var cashInfo = new CashBalanceInfo();
                //var ipgws = new TVSIWS_IPG.IPGWS();

                if (model.AccountNo.EndsWith("1"))
                {
                    //var dataInfo_01 = ipgws.IPGWS530(model.AccountNo, "BOND_API");
                    //var ws530 = TVSI.Utility.WebServiceEntitySerializer.Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS530>(dataInfo_01);
                    //if (ws530?.so_tien > 0)
                    //{
                    //    withdraw = ws530.so_tien;
                    //    cashBalance = ws530.so_tien;
                    //}
                    cashInfo = await _cashTransferService.GetNormalAccountFromDB2(model.AccountNo);
                    // Tinh số tiền đang bị Hold
                    var holdAmount = await _cashTransferService.GetBuyHoldAmount(model.AccountNo);
                    if (holdAmount > 0)
                    {
                        blockFeeList.Add(new ShowBlockFeeInfo
                        {
                            FeeType = "TP",
                            Amount = holdAmount,
                            FeeName = "Tiền mua trái phiếu đang chờ"
                        });
                    }
                    cashInfo.MaxWithdrawal = cashInfo.MaxWithdrawal - holdAmount;
                    cashInfo.Withdrawal = cashInfo.Withdrawal - holdAmount;
                    cashInfo.BlockFee = cashInfo.BlockFee + holdAmount;
                    //cashInfo.MaxWithdrawal = cashInfo.MaxWithdrawal - holdAmount;
                }
                else if (model.AccountNo.EndsWith("6"))
                {
                    //var dataInfo_06 = ipgws.IPGWS540(model.AccountNo);
                    //var ws540 = TVSI.Utility.WebServiceEntitySerializer.Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS540>(dataInfo_06);
                    //if (ws540 != null)
                    //{
                    //    cashBalance = ws540.so_du_tien;
                    //    withdraw = ws540.so_tien_rut_nhanh;
                    //}

                    cashInfo = await _cashTransferService.GetMarginAccountFromDB2(model.AccountNo, true);
                }

                var data = new CashTransferInfoResult
                              {
                                  AccountNo = model.AccountNo,
                                  BankInfos = bankInfo.ToList(),
                                  CashBalanceInfo = cashInfo,
                                  InternalAccountInfos = internalAccInfo.ToList(),
                                  BlockFeeInfos = blockFeeList
                };
                var dataRet = new SuccessModel<CashTransferInfoResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "CT_CI_CustCashTransferInfo");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_CI_CustCashTransferInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Gửi yêu cầu chuyển tiền ngân hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_BT_CashTransRequestBankTransfer")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình GD tiền > Chuyển tiền > Chuyển tiền ngân hàng")]
        public async Task<IHttpActionResult> CT_BT_CashTransRequestBankTransfer(BankTransferRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_BT_CashTransRequestBankTransfer");

                // Validate empty data of request model
                var errModel = ValidateRequestModel(model, lang);
                if (errModel != null)
                    return Ok(errModel);

                var bankInfo = await _cashTransferService.GetBankInfoByID(model.CustBankID);
                if (_cashTransferService.CheckNoHopDongKYC(model.UserId))
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = RetCodeConst.MCTBT_E002_TRANSFER_FAILED,
                        RetMsg = ResourceFile.CashTransferModule.MCTKYC_001
                    };
                    LogResult(log, errorModel, model.UserId, "CT_BT_CashTransRequestBankTransfer");
                    return Ok(errorModel);
                }
            
                if (bankInfo != null)
                {
                    var ipgws = new TVSIWS_IPG.IPGWS();

                    var ma_chi_nhanh = "01";
                    var tvsi_tk_chung_khoan = model.AccountNo;
                    var bank_tk_ghi_co = bankInfo.BankAccountNo;
                    var bank_ten_tk_ghi_co = bankInfo.BankAccountName;
                    var so_tien = model.Amount;
                    var loai_tien = "VND";
                    var tieu_de = "CK Ngan hang";
                    var noi_dung = model.Note;
                    var kenh_giao_dich = "MB";
                    var nguoi_cap_nhat = model.UserId;

                    var data = ipgws.IPGWS204(ma_chi_nhanh, tvsi_tk_chung_khoan, bank_tk_ghi_co, bank_ten_tk_ghi_co,
                        so_tien, loai_tien, tieu_de, noi_dung, kenh_giao_dich, nguoi_cap_nhat);
                    var ws201 =
                        TVSI.Utility.WebServiceEntitySerializer
                            .Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS201>(data);

                    if (ws201 != null)
                    {
                        if (ws201.ma_loi == "000")
                        {
                            return Ok(new SuccessModel());
                        }
                        else
                        {
                            log.Error("CT_BT_CashTransRequestBankTransfer:" + model.UserId + ":" + ws201.ma_loi + "-" + ws201.thong_bao_loi);

                            var errorModel = new ErrorModel
                                             {
                                                 RetCode = RetCodeConst.MCTBT_E002_TRANSFER_FAILED,
                                                 RetMsg = ResourceFile.CashTransferModule.MCTBT_002 + ":" + ws201.thong_bao_loi

                                             };
                            LogResult(log, errorModel, model.UserId, "CT_BT_CashTransRequestBankTransfer");
                            return Ok(errorModel);
                        }

                    }

                    var errorModel_02 = new ErrorModel
                                     {
                                         RetCode = RetCodeConst.MCTBT_E002_TRANSFER_FAILED,
                                         RetMsg = ResourceFile.CashTransferModule.MCTBT_002

                                     };
                    LogResult(log, errorModel_02, model.UserId, "CT_BT_CashTransRequestBankTransfer");
                    return Ok(errorModel_02);
                }
                else
                {
                    // Khong tim thay thong tin ngan hang
                    var errorModel_03 = new ErrorModel
                                        {
                                            RetCode = RetCodeConst.MCTBT_E001_BANKINFO_NOT_FOUND,
                                            RetMsg = ResourceFile.CashTransferModule.MCTBT_001

                                        };
                    LogResult(log, errorModel_03, model.UserId, "CT_BT_CashTransRequestBankTransfer");
                    return Ok(errorModel_03);
                }

              
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_BT_CashTransRequestBankTransfer");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Lấy thông tin chuyển khoản nội bộ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_CI_CustInternalTransferInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<InternalTransferInfoResult>))]
        [Note("Màn hình GD tiền > Chuyển tiền > Chuyển khoản nội bộ")]
        public async Task<IHttpActionResult> CT_CI_CustInternalTransferInfo(InternalTransferInfoRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CI_CustInternalTransferInfo");

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var internalAccInfo = await _cashTransferService.GetInternalAccountInfo(model.UserId, model.AccountNo);
                var withdraw = 0m;

                //var ipgws = new TVSIWS_IPG.IPGWS();

                //if (model.AccountNo.EndsWith("1"))
                //{
                //    var dataInfo_01 = ipgws.IPGWS530(model.AccountNo, "BOND_API");
                //    var ws530 = TVSI.Utility.WebServiceEntitySerializer.Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS530>(dataInfo_01);
                //    if (ws530?.so_tien > 0)
                //        withdraw = ws530.so_tien;
                //}
                //else if (model.AccountNo.EndsWith("6"))
                //{
                //    var dataInfo_06 = ipgws.IPGWS540(model.AccountNo);
                //    var ws540 = TVSI.Utility.WebServiceEntitySerializer.Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS540>(dataInfo_06);
                //    if (ws540?.so_du_tien > 0)
                //        withdraw = ws540.so_du_tien;
                //}

                var cashInfo = new CashBalanceInfo();

                if (model.AccountNo.EndsWith("1"))
                {
                    cashInfo = await _cashTransferService.GetNormalAccountFromDB2(model.AccountNo);
                }
                else if (model.AccountNo.EndsWith("6"))
                {
                    cashInfo = await _cashTransferService.GetMarginAccountFromDB2(model.AccountNo, true);
                }

                // Tim số tiền đang bị Hold
                //var holdAmount = await _cashTransferService.GetBuyHoldAmount(model.AccountNo);

                var data = new InternalTransferInfoResult
                           {
                               AccountNo = model.AccountNo,
                               Withdrawal = cashInfo.MaxWithdrawal,
                               InternalAccountInfos = internalAccInfo.ToList()
                           };

                var dataRet = new SuccessModel<InternalTransferInfoResult> {RetData = data};

                LogResult(log, dataRet, model.UserId, "CT_CI_CustInternalTransferInfo");
                return Ok(data);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_CI_CustInternalTransferInfo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Gửi yêu cầu chuyển tiền sang tài khoản nội bộ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_IT_CashTransRequestInternalTransfer")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình GD tiền > Chuyển tiền > Chuyển tiền nội bộ")]
        public async Task<IHttpActionResult> CT_IT_CashTransRequestInternalTransfer(InternalTransferRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_IT_CashTransRequestInternalTransfer");
                // Validate empty data of request model
                var errModel = ValidateRequestModel(model, lang);
                if (errModel != null)
                    return Ok(errModel);

                if (_cashTransferService.CheckNoHopDongKYC(model.UserId))
                {
                    var errorModel = new ErrorModel
                    {
                        RetCode = RetCodeConst.MCTBT_E002_TRANSFER_FAILED,
                        RetMsg = ResourceFile.CashTransferModule.MCTKYC_001
                    };
                    LogResult(log, errorModel, model.UserId, "CT_BT_CashTransRequestBankTransfer");
                    return Ok(errorModel);
                }

                var internalAccountInfo = await _cashTransferService.GetInternalAccountInfoByID(model.UserId, model.ReceiveAccountNo);
                if (internalAccountInfo != null)
                {
                    var ipgws = new TVSIWS_IPG.IPGWS();

                    var ma_chi_nhanh = "01";
                    var tvsi_tk_chung_khoan = model.AccountNo;
                    var bank_tk_ghi_co = model.ReceiveAccountNo;
                    var bank_ten_tk_ghi_co = internalAccountInfo.InternalAccountName;
                    var so_tien = model.Amount;
                    var loai_tien = "VND";
                    var tieu_de = "CK Noi bo";
                    var noi_dung = model.Note;
                    var kenh_giao_dich = "MB";
                    var nguoi_cap_nhat = model.UserId;

                    var data = ipgws.IPGWS204(ma_chi_nhanh, tvsi_tk_chung_khoan, bank_tk_ghi_co, bank_ten_tk_ghi_co,
                        so_tien, loai_tien, tieu_de, noi_dung, kenh_giao_dich, nguoi_cap_nhat);
                    log.Error("CT_IT_CashTransRequestInternalTransfer:" + model.UserId + ":data=" + data);
                    var ws201 =
                        TVSI.Utility.WebServiceEntitySerializer
                            .Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS201>(data);

                    if (ws201 != null)
                    {
                        if (ws201.ma_loi == "000")
                        {
                            return Ok(new SuccessModel());
                        }
                        else
                        {
                            log.Error("CT_IT_CashTransRequestInternalTransfer:" + model.UserId + ":" + ws201.ma_loi + "-" + ws201.thong_bao_loi);
                            var errorModel_01 = new ErrorModel
                                                {
                                                    RetCode = RetCodeConst.MCTIT_E002_TRANSFER_FAILED,
                                                    RetMsg = ResourceFile.CashTransferModule.MCTIT_002 + ":" +
                                                        ws201.thong_bao_loi

                                                };
                            LogResult(log, errorModel_01, model.UserId, "CT_IT_CashTransRequestInternalTransfer");
                            return Ok(errorModel_01);
                        }

                    }

                    var errorModel_02 = new ErrorModel
                                        {
                                            RetCode = RetCodeConst.MCTIT_E002_TRANSFER_FAILED,
                                            RetMsg = ResourceFile.CashTransferModule.MCTIT_002

                                        };
                    LogResult(log, errorModel_02, model.UserId, "CT_IT_CashTransRequestInternalTransfer");
                    return Ok(errorModel_02);
                }
                else
                {
                    var errorModel_03 = new ErrorModel
                                        {
                                            RetCode = RetCodeConst.MCTIT_E001_INTERNALACCOUNT_NOT_FOUND,
                                            RetMsg = ResourceFile.CashTransferModule.MCTIT_001

                                        };
                    LogResult(log, errorModel_03, model.UserId, "CT_IT_CashTransRequestInternalTransfer");
                    return Ok(errorModel_03);
                   
                }
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_IT_CashTransRequestInternalTransfer");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy trạng thái chuyển tiền trong ngày của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_TO_CashTransToday")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<CashTransTodayResult>))]
        [Note("Màn hình GD tiền > Chuyển tiền > Trạng thái chuyển tiền")]
        public IHttpActionResult CT_TO_CashTransToday(CashTransTodayRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_TO_CashTransToday");

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var retData = new List<CashTransTodayResult>();

                var ipgws = new TVSIWS_IPG.IPGWS();

                var data_01 = ipgws.IPGWS812(model.UserId + "1", -1);
                var ws812_01 = TVSI.Utility.WebServiceEntitySerializer
                            .Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS813>(data_01);

                if (ws812_01 != null)
                {
                    
                    foreach (var item in ws812_01.rows)
                    {
                        retData.Add(new CashTransTodayResult
                                    {
                                        TradeDate = item.ngay_tao.ToString("HH:mm:ss"),
                                        Amount = item.so_tien,
                                        FromAccount = item.so_tai_khoan,
                                        ToAccount = item.so_tai_khoan_nhan,
                                        BankAccountName = item.ten_tai_khoan_nhan,
                                        BankName = item.ten_ngan_hang,
                                        SubBranchName = item.chi_nhanh_ngan_hang,
                                        Status = item.tvsi_trang_thai,
                                        TransferType = item.loai_giao_dich,
                                        Remark = item.noi_dung
                                    });
                        
                    }
                    
                }

                var data_06 = ipgws.IPGWS812(model.UserId + "6", -1);
                var ws812_06 = TVSI.Utility.WebServiceEntitySerializer
                            .Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS813>(data_06);

                if (ws812_06 != null)
                {

                    foreach (var item in ws812_06.rows)
                    {
                        retData.Add(new CashTransTodayResult
                        {
                            TradeDate = item.ngay_tao.ToString("HH:mm:ss"),
                            Amount = item.so_tien,
                            FromAccount = item.so_tai_khoan,
                            ToAccount = item.so_tai_khoan_nhan,
                            BankAccountName = item.ten_tai_khoan_nhan,
                            BankName = item.ten_ngan_hang,
                            SubBranchName = item.chi_nhanh_ngan_hang,
                            Status = item.tvsi_trang_thai,
                            TransferType = item.loai_giao_dich,
                            Remark = item.noi_dung
                        });
                    }

                }
                
                SetLanguage(lang);

                var retModel = new SuccessListModel<CashTransTodayResult> { RetData = retData.OrderByDescending(x => x.TradeDate).ToList() };
                LogResult(log, retModel, model.UserId, "CT_TO_CashTransToday");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_TO_CashTransToday");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy lịch sử chuyển tiền của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_CH_CashTransHist")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<CashTransHistResult>))]
        [Note("Màn hình GD tiền > Chuyển tiền > Lịch sử chuyển tiền")]
        public async Task<IHttpActionResult> CT_CH_CashTransHist(CashTransHistRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CH_CashTransHist");

                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var dtxFrom = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dtxTo = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var cashTransList = new List<CashTransHistResult>();

                var ipgws = new TVSIWS_IPG.IPGWS();

                var data_01 = ipgws.IPGWS810(model.UserId + "1", dtxFrom, dtxTo, -1);

                var ws811_01 = TVSI.Utility.WebServiceEntitySerializer
                            .Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS811>(data_01);

                if (ws811_01 != null)
                {
                    foreach (var item in ws811_01.rows)
                    {
                        cashTransList.Add(new CashTransHistResult
                        {
                            TradeDate = item.ngay_giao_dich,
                            Amount = item.so_tien,
                            FromAccount = item.so_tai_khoan,
                            ToAccount = item.so_tai_khoan_nhan,
                            BankAccountName = item.ten_tai_khoan_nhan,
                            BankName = item.ten_ngan_hang,
                            SubBranchName = item.chi_nhanh_ngan_hang,
                            Status = item.tvsi_trang_thai,
                            TransferType = item.loai_giao_dich,
                            Remark = item.noi_dung
                        });
                    }

                   
                }

                var data_06 = ipgws.IPGWS810(model.UserId + "6", dtxFrom, dtxTo, -1);

                var ws811_06 = TVSI.Utility.WebServiceEntitySerializer
                            .Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS811>(data_06);

                if (ws811_06 != null)
                {
                    foreach (var item in ws811_06.rows)
                    {
                        cashTransList.Add(new CashTransHistResult
                        {
                            TradeDate = item.ngay_giao_dich,
                            Amount = item.so_tien,
                            FromAccount = item.so_tai_khoan,
                            ToAccount = item.so_tai_khoan_nhan,
                            BankAccountName = item.ten_tai_khoan_nhan,
                            BankName = item.ten_ngan_hang,
                            SubBranchName = item.chi_nhanh_ngan_hang,
                            Status = item.tvsi_trang_thai,
                            TransferType = item.loai_giao_dich,
                            Remark = item.noi_dung
                        });
                    }


                }

                var retData = await Task.FromResult(cashTransList);

                SetLanguage(lang);
                var retModel = new SuccessListModel<CashTransHistResult> { RetData = retData.OrderByDescending(x=> x.TradeDate).ToList() };
                LogResult(log, retModel, model.UserId, "CT_CH_CashTransHist");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_CH_CashTransHist");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy sao kê tiền của Khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_CT_CashTransaction")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CashTransactionResult>))]
        [Note("Màn hình GD tiền > Chuyển tiền > Sao kê tiền" )]
        public async Task<IHttpActionResult> CT_CT_CashTransaction(CashTransactionRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CT_CashTransaction");
                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var dtxFrom = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dtxTo = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _cashTransferService.GetCashTransactionResult(model.AccountNo, dtxFrom, dtxTo, model.PageIndex,
                            model.PageSize);

                var retModel = new SuccessModel<CashTransactionResult> {RetData = data};
                LogResult(log, retModel, model.UserId, "CT_CT_CashTransaction");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_CT_CashTransaction");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy sao kê tiền chi tiết của Khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_CT_CashTransactionDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CashTransactionDetailResult>))]
        [Note("Màn hình GD tiền > Chuyển tiền > Sao kê tiền chi tiết")]
        public async Task<IHttpActionResult> CT_CT_CashTransactionDetail(CashTransactionDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CT_CashTransactionDetail");

                var dtxFrom = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dtxTo = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _cashTransferService.GetCashTransactionDetailResult(model.AccountNo, model.Symbol, model.TransType, dtxFrom, dtxTo, model.PageIndex,
                            model.PageSize);

                var retModel = new SuccessModel<CashTransactionDetailResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "CT_CT_CashTransactionDetail");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_CT_CashTransactionDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy sao kê phí giao dịch
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_CT_TransactionFeeStatementDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<TransactionFeeStatementResult>))]
        [Note("Màn hình GD tiền > Chuyển tiền > Sao kê phí giao dịch")]
        public async Task<IHttpActionResult> CT_CT_TransactionFeeStatementDetail(TransactionFeeStatementRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CT_TransactionFeeStatementDetail");

                var dtxFrom = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dtxTo = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _cashTransferService.GetTransactionFeeStatementResult(model.AccountNo, model.Side, dtxFrom, dtxTo, model.PageIndex,
                            model.PageSize);

                var retModel = new SuccessModel<TransactionFeeStatementResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "CT_CT_TransactionFeeStatementDetail");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_CT_TransactionFeeStatementDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy lãi lỗ tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_CT_RealizeGainLoss")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<RealizeGainLossResult>))]
        [Note("Màn hình GD tiền > Chuyển tiền > Sao kê phí giao dịch")]
        public async Task<IHttpActionResult> CT_CT_RealizeGainLoss(RealizeGainLossRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CT_RealizeGainLoss");

                var dtxFrom = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dtxTo = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _cashTransferService.GetRealizeGainLostResult(model.AccountNo, model.Symbol, dtxFrom, dtxTo, model.PageIndex,
                            model.PageSize);

                var retModel = new SuccessModel<RealizeGainLossResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "CT_CT_RealizeGainLoss");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_CT_RealizeGainLoss");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin lãi tiền gửi và lãi vay 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_CT_GetInComeAndExpense")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<InComeAndExpenseResult>))]
        [Note("Màn hình GD tiền > Chuyển tiền > Sao kê phí giao dịch")]
        public async Task<IHttpActionResult> CT_CT_GetInComeAndExpense(InComeAndExpenseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CT_GetInComeAndExpense");

                var dtxFrom = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dtxTo = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _cashTransferService.GetInComeAndExpenseResult(model.AccountNo, model.Symbol, dtxFrom, dtxTo, model.PageIndex,
                            model.PageSize);

                var retModel = new SuccessModel<InComeAndExpenseResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "CT_CT_GetInComeAndExpense");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_CT_GetInComeAndExpense");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách ngân hàng nộp tiền
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_CT_GetDepositBank")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<DepositBankResult>))]
        [Note("Màn hình GD tiền > Nộp tiền")]
        public async Task<IHttpActionResult> CT_CT_GetDepositBank(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CT_GetDepositBank");

                var data = await _cashTransferService.GetDepositBank(model);

                var retModel = new SuccessListModel<DepositBankResult> { RetData = data };
                LogResult(log, retModel, model.UserId, "CT_CT_GetDepositBank");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_CT_GetDepositBank");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}
