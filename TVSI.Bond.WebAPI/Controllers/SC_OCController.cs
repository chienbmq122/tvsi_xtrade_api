﻿using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.SC_OrderConfirm;
using TVSI.Bond.WebAPI.Lib.Services;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Controllers
{
    public class SC_OCController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(SC_OCController));


        private ISC_OrderConfirmationService _orderConfirmationService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="SC_OCController"></param>
        public SC_OCController(ISC_OrderConfirmationService orderConfirmationService)
        {
            _orderConfirmationService = orderConfirmationService;
        }


        /// <summary>
        /// Lấy thông tin danh sách xác nhận lệnh
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("SC_OC_GetOrderConfirmationStatus")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<SC_OrderConfirmResult>))]
        [Note("Dịch vụ tiện ích > Xác nhận lệnh", true)]
        public async Task<IHttpActionResult> SC_OC_GetOrderConfirmationStatus(SC_FindOrderConfirmRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SC_OC_GetOrderConfirmationStatus");

                if (string.IsNullOrWhiteSpace(model.SecSymbol))
                    model.SecSymbol = "all";

                var listInfo = await _orderConfirmationService.SC_GetOrderConfirmationStatus(model);

                SC_OrderConfirmResult data = new SC_OrderConfirmResult();

                if (listInfo.Count > 0)
                {
                    data.TotalRows = listInfo.FirstOrDefault().TotalRows;
                    data.TotalPages = listInfo.FirstOrDefault().TotaLPages;
                    data.Data = listInfo;
                }

                var dataRet = new SuccessModel<SC_OrderConfirmResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "SC_OC_GetOrderConfirmationStatus");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SC_OC_GetOrderConfirmationStatus");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin lịch sử danh sách xác nhận lệnh
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("SC_OC_GetOrderConfirmationHistory")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<SC_OrderConfirmResult>))]
        [Note("Dịch vụ tiện ích > Xác nhận lệnh > Lịch sử", true)]
        public async Task<IHttpActionResult> SC_OC_GetOrderConfirmationHistory(SC_FindOrderConfirmRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SC_OC_GetOrderConfirmationHistory");

                if (string.IsNullOrWhiteSpace(model.SecSymbol))
                    model.SecSymbol = "all";

                var listInfo = await _orderConfirmationService.SC_GetOrderConfirmationHistory(model);

                SC_OrderConfirmResult data = new SC_OrderConfirmResult();

                if (listInfo.Count > 0)
                {
                    data.TotalRows = listInfo.FirstOrDefault().TotalRows;
                    data.TotalPages = listInfo.FirstOrDefault().TotaLPages;
                    data.Data = listInfo;
                }

                var dataRet = new SuccessModel<SC_OrderConfirmResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "SC_OC_GetOrderConfirmationHistory");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SC_OC_GetOrderConfirmationHistory");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy Chi tiết thông tin lệnh đặt
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("SC_OC_OrderConfirmationDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<SC_OrderConfirmationDetailResult>))]
        [Note("Dịch vụ tiện ích > Xác nhận lệnh > Chi tiết thông tin lệnh đặt", true)]
        public async Task<IHttpActionResult> SC_OC_OrderConfirmationDetail(SC_OrderConfirmationDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SC_OC_OrderConfirmationDetail");

                var listInfo = await _orderConfirmationService.SC_OrderConfirmationDetail(model);

                SC_OrderConfirmationDetailResult data = new SC_OrderConfirmationDetailResult();

                if (listInfo.Count > 0)
                {
                    data.TotalRows = listInfo.FirstOrDefault().TotalRows;
                    data.Data = listInfo;
                }

                var dataRet = new SuccessModel<SC_OrderConfirmationDetailResult> { RetData = data };
                LogResult(log, dataRet, model.UserId, "SC_OC_OrderConfirmationDetail");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SC_OC_OrderConfirmationDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Lấy thông tin lịch sử danh sách xác nhận lệnh
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("SC_OC_UpdateOrderConfirmationByIDs")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<SC_OrderConfirmResult>))]
        [Note("Dịch vụ tiện ích > Xác nhận lệnh > Xác nhận theo lựa chọn", true)]
        public async Task<IHttpActionResult> SC_OC_UpdateOrderConfirmationByIDs(SC_UpdateConfirmOrderConfirmationRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SC_OC_UpdateOrderConfirmationByIDs");

                var returnData = await _orderConfirmationService.SC_ConfirmOrderConfirmation(model);

                SC_UpdateOrderConfirmResult data = new SC_UpdateOrderConfirmResult();

                if (returnData > 0)
                {
                    data.RetCode = RetCodeConst.SUCCESS_CODE;
                    data.Message = "Completed";
                }
                else
                {
                    data.RetCode = RetCodeConst.ERR999_SYSTEM_ERROR;
                    data.Message = "No data";
                }

                var dataRet = new SuccessModel();
                dataRet.RetCode = data.RetCode;
                //<SC_UpdateOrderConfirmResult> { RetData = data };

                LogResult(log, dataRet, model.UserId, "SC_OC_UpdateOrderConfirmationByIDs");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SC_OC_UpdateOrderConfirmationByIDs");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy thông tin lịch sử danh sách xác nhận lệnh
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("SC_OC_UpdateAllOrderConfirmations")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<SC_UpdateOrderConfirmResult>))]
        [Note("Dịch vụ tiện ích > Xác nhận lệnh > Xác nhận tất cả", true)]
        public async Task<IHttpActionResult> SC_OC_UpdateAllOrderConfirmations(SC_UpdateALLConfirmOrderConfirmationRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SC_OC_UpdateAllOrderConfirmations");
                
                if (string.IsNullOrWhiteSpace(model.SecSymbol))
                    model.SecSymbol = "all";

                var returnData = await _orderConfirmationService.SC_ConfirmALLOrderConfirmation(model);

                SC_UpdateOrderConfirmResult data = new SC_UpdateOrderConfirmResult();

                if (returnData > 0)
                {
                    data.RetCode = RetCodeConst.SUCCESS_CODE;
                    data.Message = "Completed";
                }
                else if (returnData == 0)
                {
                    data.RetCode = RetCodeConst.SUCCESS_CODE;
                    data.Message = "No data found!";
                }
                
                var dataRet = new SuccessModel();
                dataRet.RetCode = data.RetCode;
                LogResult(log, dataRet, model.UserId, "SC_OC_UpdateAllOrderConfirmations");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SC_OC_UpdateAllOrderConfirmations");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}