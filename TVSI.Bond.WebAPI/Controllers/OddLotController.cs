﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.OddLot;
using TVSI.Bond.WebAPI.Lib.Services;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Lib.Utility.OddLotParams;

namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// Odd Lot API
    /// </summary>
    public class OddLotController : BaseApiController
    {
        private ILog log = LogManager.GetLogger(typeof(OddLotController));
        private IOddLotService _oddLotService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="oddLotService"></param>
        public OddLotController(IOddLotService oddlotService)
        {
            _oddLotService = oddlotService;
        }

        /// <summary>
        /// Lấy danh mục chứng khoán lô lẻ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        /// POST /OddLot/OL_SR_Select
        /// {
        ///     "UserId": "000129",
        ///     "AccountNo": "0001291"
        /// }
        /// 
        /// </remarks>
        [ActionName("OL_SR_Select")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<OddLotStockRepository>>))]
        [Note("Màn hình GD Chứng khoán > Bán CP lô lẻ > Danh sách cổ phiếu lô lẻ")]
        public async Task<IHttpActionResult> OL_SR_Select([FromBody] OddLotStockRepositorySelectRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OL_SR_Select");
                SetLanguage(lang);

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = OddLotRetCodeConst.OL_902_MISSING_DATA,
                        RetMsg = OddLotRetMsgConst.OL_902_MISSING_DATA
                    });
                }

                var data = await _oddLotService.SelectStockRepository(model.AccountNo);

                if (data != null)
                {
                    var dataRet = new SuccessModel<List<OddLotStockRepository>> {RetData = data};
                    LogResult(log, dataRet, model.UserId, "OL_SR_Select");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = OddLotRetCodeConst.OL_903_NOT_FOUND,
                    RetMsg = OddLotRetMsgConst.OL_903_NOT_FOUND
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OL_SR_Select");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        /// <summary>
        /// Lấy danh sách hỗ trợ bán lô lẻ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        /// POST /OddLot/OL_SR_Search
        /// {
        ///     "UserId": "000129",
        ///     "AccountNo": "0001291"
        ///     "StockCode": ""
        /// }
        /// 
        /// </remarks>
        [ActionName("OL_SR_Search")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<OddLotStockRepositoryEmsSearchModel>>))]
        [Note("Màn hình GD Chứng khoán > Bán CP lô lẻ > Danh sách hỗ trợ bán lô lẻ")]
        public async Task<IHttpActionResult> OL_SR_Search([FromBody] OddLotStockRepositorySearchRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OL_SR_Search");
                SetLanguage(lang);

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = OddLotRetCodeConst.OL_902_MISSING_DATA,
                        RetMsg = OddLotRetMsgConst.OL_902_MISSING_DATA
                    });
                }

                var data = await _oddLotService.SearchStockRepository(model.StockCode);

                if (data != null)
                {
                    var dataRet = new SuccessModel<List<OddLotStockRepositoryEmsSearchModel>> {RetData = data};
                    LogResult(log, dataRet, model.UserId, "OL_SR_Search");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = OddLotRetCodeConst.OL_903_NOT_FOUND,
                    RetMsg = OddLotRetMsgConst.OL_903_NOT_FOUND
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OL_SR_Search");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Thêm mới lệnh bán chứng khoán lô lẻ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        /// POST /OddLot/OL_ORD_Insert
        /// {
        ///     "lstOrder": [
        ///     {
        ///         "stock_code": "EIB",
        ///         "stock_type": "02",
        ///         "stock_quantity": 2,
        ///         "stock_price": 29.850
        ///     },
        ///     {
        ///         "stock_code": "EIB",
        ///         "stock_type": "02",
        ///         "stock_quantity": 4,
        ///         "stock_price": 29.850
        ///     }
        ///     ],
        ///     "UserId": "000129",
        ///     "AccountNo": "0001291"
        /// }
        /// 
        /// </remarks>
        [ActionName("OL_ORD_Insert")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình GD Chứng khoán > Bán CP lô lẻ > Danh sách cổ phiếu lô lẻ > Button Đồng ý")]
        public async Task<IHttpActionResult> OL_ORD_Insert([FromBody] OddLotOrderInsertRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OL_ORD_Insert");
                SetLanguage(lang);

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = OddLotRetCodeConst.OL_902_MISSING_DATA,
                        RetMsg = OddLotRetMsgConst.OL_902_MISSING_DATA
                    });
                }

                var data = await _oddLotService.InsertOrder(model.AccountNo, model.UserId, model.lstOrder);

                if (data)
                {
                    var dataRet = new SuccessModel();
                    LogResult(log, dataRet, model.UserId, "OL_ORD_Insert");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,
                    RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OL_ORD_Insert");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Lấy danh sách lệnh bán chứng khoán lô lẻ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        /// POST /OddLot/OL_ORD_Search
        /// {
        ///     "stock_code": "",
        ///     "fromDate": "07/02/2021",
        ///     "toDate": "04/03/2022",
        ///     "order_status": -1,
        ///     "order_channel": -1,
        ///     "UserId": "001758",
        ///     "AccountNo": "0017586"
        /// }
        /// 
        /// </remarks>
        [ActionName("OL_ORD_Search")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<OddLotOrder>>))]
        [Note("Màn hình GD Chứng khoán > Bán CP lô lẻ > Danh sách cổ phiếu lô lẻ & Lịch sử bán cổ phiếu lô lẻ")]
        public async Task<IHttpActionResult> OL_ORD_Search([FromBody] OddLotOrderSearchRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OL_ORD_Search");
                SetLanguage(lang);

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = OddLotRetCodeConst.OL_902_MISSING_DATA,
                        RetMsg = OddLotRetMsgConst.OL_902_MISSING_DATA
                    });
                }

                var dtxFromDate = SqlDateTime.MinValue;
                var dtxToDate = SqlDateTime.MaxValue;

                if (!String.IsNullOrEmpty(model.fromDate))
                    dtxFromDate = DateTime.ParseExact(model.fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                
                if (!String.IsNullOrEmpty(model.toDate))
                    dtxToDate = DateTime.ParseExact(model.toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _oddLotService.SearchOrder(model.AccountNo,
                    model.stock_code, (DateTime)dtxFromDate,
                    (DateTime)dtxToDate,
                    model.order_status,
                    model.order_channel);

                if (data != null)
                {
                    var dataRet = new SuccessModel<List<OddLotOrder>> {RetData = data};
                    LogResult(log, dataRet, model.UserId, "OL_ORD_Search");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = OddLotRetCodeConst.OL_903_NOT_FOUND,
                    RetMsg = OddLotRetMsgConst.OL_903_NOT_FOUND
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OL_ORD_Search");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        /// <summary>
        /// Lấy danh sách lệnh bán lô lẻ (trạng thái "Chờ xử lý" & lệnh được tạo trong ngày hiện tại)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        /// POST /OddLot/OL_ORD_Select_Current_Order
        /// {
        ///     "stock_code": "",
        ///     "order_status": -1,
        ///     "order_channel": -1,
        ///     "UserId": "001758",
        ///     "AccountNo": "0017586"
        /// }
        /// 
        /// </remarks>
        [ActionName("OL_ORD_Select_Current_Order")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<OddLotOrder>>))]
        [Note("Màn hình GD Chứng khoán > Bán CP lô lẻ > Danh sách cổ phiếu lô lẻ & Lịch sử bán cổ phiếu lô lẻ")]
        public async Task<IHttpActionResult> OL_ORD_Select_Current_Order([FromBody] OddLotOrderSelectCurrentRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OL_ORD_Select_Current_Order");
                SetLanguage(lang);

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = OddLotRetCodeConst.OL_902_MISSING_DATA,
                        RetMsg = OddLotRetMsgConst.OL_902_MISSING_DATA
                    });
                }

                var data = await _oddLotService.SelectCurrentOrder(model.AccountNo,
                    model.stock_code,
                    model.order_status,
                    model.order_channel);

                if (data != null)
                {
                    var dataRet = new SuccessModel<List<OddLotOrder>> {RetData = data};
                    LogResult(log, dataRet, model.UserId, "OL_ORD_Select_Current_Order");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = OddLotRetCodeConst.OL_903_NOT_FOUND,
                    RetMsg = OddLotRetMsgConst.OL_903_NOT_FOUND
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OL_ORD_Select_Current_Order");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        /// <summary>
        /// Lấy danh sách lệnh bán lô lẻ (bỏ trạng thái "Chờ xử lý")
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        /// POST /OddLot/OL_ORD_Select_Historical_Order
        /// {
        ///     "stock_code": "",
        ///     "fromDate": "07/02/2021",
        ///     "toDate": "04/03/2022",
        ///     "order_status": -1,
        ///     "order_channel": -1,
        ///     "UserId": "001758",
        ///     "AccountNo": "0017586"
        /// }
        /// 
        /// </remarks>
        [ActionName("OL_ORD_Select_Historical_Order")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<OddLotOrder>>))]
        [Note("Màn hình GD Chứng khoán > Bán CP lô lẻ > Danh sách cổ phiếu lô lẻ & Lịch sử bán cổ phiếu lô lẻ")]
        public async Task<IHttpActionResult> OL_ORD_Select_Historical_Order([FromBody] OddLotOrderSearchRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OL_ORD_Select_Historical_Order");
                SetLanguage(lang);

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = OddLotRetCodeConst.OL_902_MISSING_DATA,
                        RetMsg = OddLotRetMsgConst.OL_902_MISSING_DATA
                    });
                }

                var dtxFromDate = SqlDateTime.MinValue;
                var dtxToDate = SqlDateTime.MaxValue;

                if (!String.IsNullOrEmpty(model.fromDate))
                    dtxFromDate = DateTime.ParseExact(model.fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                
                if (!String.IsNullOrEmpty(model.toDate))
                    dtxToDate = DateTime.ParseExact(model.toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _oddLotService.SelectHistoricalOrder(model.AccountNo,
                    model.stock_code, (DateTime)dtxFromDate,
                    (DateTime)dtxToDate,
                    model.order_status,
                    model.order_channel);

                if (data != null)
                {
                    var dataRet = new SuccessModel<List<OddLotOrder>> {RetData = data};
                    LogResult(log, dataRet, model.UserId, "OL_ORD_Select_Historical_Order");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = OddLotRetCodeConst.OL_903_NOT_FOUND,
                    RetMsg = OddLotRetMsgConst.OL_903_NOT_FOUND
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OL_ORD_Select_Historical_Order");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Hủy lệnh bán chứng khoán lô lẻ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        /// POST /OddLot/OL_ORD_Cancel
        /// {
        ///     "UserId": "000129",
        ///     "AccountNo": "0001291"
        ///     "odd_lot_order_id": 20101
        /// }
        /// 
        /// </remarks>
        [ActionName("OL_ORD_Cancel")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình GD Chứng khoán > Bán CP lô lẻ > Danh sách cổ phiếu lô lẻ > Button Huỷ")]
        public async Task<IHttpActionResult> OL_ORD_Cancel([FromBody] OddLotOrderCancelRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OL_ORD_Cancel");
                SetLanguage(lang);

                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = OddLotRetCodeConst.OL_902_MISSING_DATA,
                        RetMsg = OddLotRetMsgConst.OL_902_MISSING_DATA
                    });
                }

                var data = await _oddLotService.CancelOrder(model.odd_lot_order_id, model.UserId);

                if (data)
                {
                    var dataRet = new SuccessModel();
                    LogResult(log, dataRet, model.UserId, "OL_ORD_Cancel");
                    return Ok(dataRet);
                }

                return Ok(new ErrorModel
                {
                    RetCode = RetCodeConst.ERR999_SYSTEM_ERROR,
                    RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OL_ORD_Cancel");
                return Ok(new ErrorModel
                    {RetCode = RetCodeConst.ERR999_SYSTEM_ERROR, RetMsg = RetCodeConst.ERR999_SYSTEM_ERROR});
            }
        }
    }
}