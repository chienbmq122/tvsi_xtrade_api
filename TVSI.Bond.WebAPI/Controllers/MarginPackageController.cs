﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Bond.WebAPI.Areas.HelpPage.Models;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.MarginPackage;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Lib.Services;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Hosting;
using System.Text;
using TVSI.Utility;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Utility.Margin;

namespace TVSI.Bond.WebAPI.Controllers
{
    /// <summary>
    /// Margin API
    /// </summary>
    public class MarginPackageController : BaseApiController
    {

        private ILog log = LogManager.GetLogger(typeof(MarginPackageController));

        private IMarginService _marginService;
        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="marginService"></param>
        public MarginPackageController(IMarginService marginService)
        {
            _marginService = marginService;
        }




        /// <summary>
        /// Lấy thông tin các gói dịch vụ tài chính
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MarginPackageList")]
        [HttpPost]
        [ResponseType(typeof(MarginPackageResult))]
        [Note("Màn hình thay đổi gói dịch vụ", true)]
        public async Task<IHttpActionResult> MarginPackageList(MarginPackageRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
                try
                {
                    SetLanguage(lang);
                    LogRequest(log, model, "MarginPackageList");

                    // Validate empty data of request model
                    var errModel = ValidateRequestModel(model, lang);
                    if (errModel != null)
                        return Ok(errModel);

                    var retProductInfo = await _marginService.MarginPackageList(model);
                    var dataRet = new SuccessListModel<MarginPackageResult> { RetData = retProductInfo };

                    LogResult(log, dataRet, model.UserId, "MarginPackageList");

                    return Ok(dataRet);
                }
                catch (Exception ex)
                {
                    LogException(log, model, ex, "MarginPackageList");
                    return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
                }
        }



        /// <summary>
        /// Lấy thông tin chi tiết gói dịch vụ tài chính
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("MarginPackageDetails")]
        [HttpPost]
        [ResponseType(typeof(MarginDetailPackageResult))]
        [Note("Màn hình thay đổi gói dịch vụ", true)]
        public async Task<IHttpActionResult> MarginPackageDetails(MarginDetailPackageRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "MarginPackageList");

                // Validate empty data of request model
                var errModel = ValidateRequestModel(model, lang);
                if (errModel != null)
                    return Ok(errModel);

                var retProductInfo = await _marginService.MarginPackageDetails(model.MarginName,model.UserId);
                var dataRet = new SuccessModel<MarginDetailPackageResult> { RetData = retProductInfo };

                LogResult(log, dataRet, model.UserId, "MarginPackageDetails");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "MarginPackageDetails");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }




        /// <summary>
        /// Lấy thông tin các gói dịch vụ tài chính ưu đãi
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("PreferentialMarginList")]
        [HttpPost]
        [ResponseType(typeof(PreferentialMarginListResult))]
        [Note("Màn hình thay đổi gói dịch vụ", true)]
        public async Task<IHttpActionResult> PreferentialMarginList(PreferentialMarginListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "PreferentialMarginList");

                // Validate empty data of request model
                var errModel = ValidateRequestModel(model, lang);
                if (errModel != null)
                    return Ok(errModel);

                var retProductInfo = await _marginService.PreferentialMarginList(lang);
                var dataRet = new SuccessListModel<PreferentialMarginListResult> { RetData = retProductInfo };

                LogResult(log, dataRet, model.UserId, "PreferentialMarginList");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "PreferentialMarginList");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }



        /// <summary>
        /// Lấy thông tin chi tiết gói dịch vụ tài chính ưu đãi
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("PreferentialMarginDetail")]
        [HttpPost]
        [ResponseType(typeof(PreferentialMarginDetailResult))]
        [Note("Màn hình thay đổi gói dịch vụ", true)]
        public async Task<IHttpActionResult> PreferentialMarginDetails(PreferentialMarginDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "PreferentialMarginDetail");

                // Validate empty data of request model
                var errModel = ValidateRequestModel(model, lang);
                if (errModel != null)
                    return Ok(errModel);

                var retProductInfo = await _marginService.PreferentialMarginDetails(model.PrefMarginName);
                var dataRet = new SuccessModel<PreferentialMarginDetailResult> { RetData = retProductInfo };

                LogResult(log, dataRet, model.UserId, "PreferentialMarginDetail");

                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "PreferentialMarginDetail");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        } 
        
        
        /// <summary>
        /// Lưu thông tin gói dịch vụ ưu đãi
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("InsertServicePreferMargin")]
        [HttpPost]
        [ResponseType(typeof(bool))]
        [Note("Màn hình lưu thông tin gói dịch vụ ưu đãi", true)]
        public async Task<IHttpActionResult> InsertServicePreferMargin(InsertServicePreferRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "InsertServicePreferMargin");

                // Validate empty data of request model
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        RetMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _marginService.InsertServicePreferMargin(model);

                if (retData)
                {
                    var dataRet = new SuccessModel<bool>(){ RetData = true };
                    LogResult(log, dataRet, model.UserId, "InsertServicePreferMargin");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = MarginPackRetCodeConst.MG_1000_SAVE_FAIL,
                    RetMsg = MarginPackRetMsgConst.MG_1000_SAVE_FAIL
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "InsertServicePreferMargin");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        } 
        
        
        /// <summary>
        /// Update đăng ký gói dịch vụ ưu đãi
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("UpdateServicePackPreferMargin")]
        [HttpPost]
        [ResponseType(typeof(bool))]
        [Note("Màn hình thông tin gói dịch vụ ưu đãi", true)]
        public async Task<IHttpActionResult> UpdateServicePackPreferMargin(UpdateServicePreferRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "InsertSerivcePreferMargin");

                // Validate empty data of request model
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        RetMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _marginService.UpdateServicePackPreferMargin(model);

                if (retData)
                {
                    var dataRet = new SuccessModel<bool>(){ RetData = true };
                    LogResult(log, dataRet, model.UserId, "UpdateServicePackPreferMargin");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = MarginPackRetCodeConst.MG_1000_SAVE_FAIL,
                    RetMsg = MarginPackRetMsgConst.MG_1000_SAVE_FAIL
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "UpdateServicePackPreferMargin");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        } 
        
        /// <summary>
        /// Update đăng ký gói dịch vụ 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("UpdateServicePackMargin")]
        [HttpPost]
        [ResponseType(typeof(bool))]
        [Note("Màn hình lưu thông tin gói dịch vụ", true)]
        public async Task<IHttpActionResult> UpdateServicePackMargin(UpdateServicePackRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "UpdateServicePackMargin");

                // Validate empty data of request model
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        RetMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _marginService.UpdateServicePackMargin(model);

                if (retData)
                {
                    var dataRet = new SuccessModel<bool>(){ RetData = true };
                    LogResult(log, dataRet, model.UserId, "UpdateServicePackMargin");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = MarginPackRetCodeConst.MG_1000_SAVE_FAIL,
                    RetMsg = MarginPackRetMsgConst.MG_1000_SAVE_FAIL
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "UpdateServicePackMargin");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        } 
        
        
        /// <summary>
        /// Insert đăng ký gói dịch vụ 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("InsertServicePackMargin")]
        [HttpPost]
        [ResponseType(typeof(bool))]
        [Note("Màn hình lưu thông tin gói dịch vụ", true)]
        public async Task<IHttpActionResult> InsertServicePackMargin(InsertServiceRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "InsertServicePackMargin");

                // Validate empty data of request model
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        RetMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _marginService.InsertServiceMargin(model);

                if (retData)
                {
                    var dataRet = new SuccessModel<bool>(){ RetData = true };
                    LogResult(log, dataRet, model.UserId, "InsertServicePackMargin");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = MarginPackRetCodeConst.MG_1000_SAVE_FAIL,
                    RetMsg = MarginPackRetMsgConst.MG_1000_SAVE_FAIL
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "InsertServicePackMargin");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        } 
        
        
        
        /// <summary>
        /// Get gói dịch vụ ưu đãi đang sử dụng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GetServicePreferByAccountNo")]
        [HttpPost]
        [ResponseType(typeof(bool))]
        [Note("Màn hình lưu thông tin gói dịch vụ", true)]
        public async Task<IHttpActionResult> GetServicePreferByAccountNo(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "GetServicePreferByAccountNo");

                // Validate empty data of request model
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        RetMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _marginService.GetServicePerferByAccontNo(model);

                if (retData != null)
                {
                    var dataRet = new SuccessModel<bool>(){ RetData = true };
                    LogResult(log, dataRet, model.UserId, "GetServicePreferByAccountNo");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    RetMsg = MarginPackRetMsgConst.MG_100_EMPTY_DATA
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GetServicePreferByAccountNo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }  
        
        /// <summary>
        /// Get gói dịch vụ đang sử dụng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GetServiceByAccountNo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ServiceResult>))]
        [Note("Màn hình lưu thông tin gói dịch vụ", true)]
        public async Task<IHttpActionResult> GetServiceByAccountNo(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "GetServiceByAccountNo");

                // Validate empty data of request model
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        RetMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _marginService.GetServiceByAccontNo(model);

                if (retData != null)
                {
                    var dataRet = new SuccessModel<ServiceResult>(){ RetData = retData };
                    LogResult(log, dataRet, model.UserId, "GetServiceByAccountNo");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    RetMsg = MarginPackRetMsgConst.MG_100_EMPTY_DATA
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GetServiceByAccountNo");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        
        /// <summary>
        /// Get gói dịch vụ ưu đãi lịch sử
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GetServicePreferHist")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetServicePreferHistResult>))]
        [Note("Màn hình lịch sử gói dịch vụ ưu đãi", true)]
        public async Task<IHttpActionResult> GetServicePreferHist(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "GetServicePreferHist");

                // Validate empty data of request model
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        RetMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _marginService.GetServicePreferHist(model);

                if (retData != null)
                {
                    var dataRet = new SuccessModel<GetServicePreferHistResult>(){ RetData = retData };
                    LogResult(log, dataRet, model.UserId, "GetServicePreferHist");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    RetMsg = MarginPackRetMsgConst.MG_100_EMPTY_DATA
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GetServicePreferHist");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        
        /// <summary>
        /// Get gói dịch vụ lịch sử
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GetServicePackHist")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetServiceHistResult>))]
        [Note("Màn hình lịch sử gói dịch vụ", true)]
        public async Task<IHttpActionResult> GetServicePackHist(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "GetServicePackHist");

                // Validate empty data of request model
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        RetMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _marginService.GetServiceHist(model);

                if (retData != null)
                {
                    var dataRet = new SuccessModel<GetServiceHistResult>(){ RetData = retData };
                    LogResult(log, dataRet, model.UserId, "GetServicePackHist");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    RetMsg = MarginPackRetMsgConst.MG_100_EMPTY_DATA
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GetServicePackHist");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        
       
        /// <summary>
        /// Kiểm tra tài khoản Margin
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CK_MP_AccountMargin")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<LoginAccountResult>))]
        [Note("Màn hình lịch sử gói dịch vụ", true)]
        public async Task<IHttpActionResult> CK_MP_AccountMargin(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model, "CK_MP_AccountMargin");

                // Validate empty data of request model
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        RetMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _marginService.CheckAccountMargin(model);

                if (retData != null)
                {
                    var dataRet = new SuccessModel<LoginAccountResult>(){ RetData = retData };
                    LogResult(log, dataRet, model.UserId, "CK_MP_AccountMargin");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel()
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    RetMsg = MarginPackRetMsgConst.MG_100_EMPTY_DATA
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CK_MP_AccountMargin");
                return Ok(new ErrorModel { RetCode = RetCodeConst.ERR999_SYSTEM_ERROR });
            }
        }
        


    }
}