﻿using System.Net.Http.Headers;
using System.Web.Http;
using TVSI.Bond.WebAPI.Lib.Services;
using TVSI.Bond.WebAPI.Lib.Services.Impl;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using Unity;
using Unity.Lifetime;

namespace TVSI.Bond.WebAPI
{
    public static class WebApiConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = new UnityContainer();

            container.RegisterType<ISystemService, SystemService>();
            container.RegisterType<ICustomerService, CustomerService>();
            container.RegisterType<ICashTransferService, CashTransferService>();
            container.RegisterType<IBondService, BondService>();
            container.RegisterType<IOrderService, OrderService>();
            container.RegisterType<IEkycService, EkycService>();
            container.RegisterType<IMmService, MmService>();
            container.RegisterType<IMarginService, MarginService>();
            container.RegisterType<IAdvMoneyService, AdvMoneyService>();
            container.RegisterType<IStockTransferService, StockTransferService>();
            container.RegisterType<INoticationService, NoticationService>();
            container.RegisterType<IOddLotService, OddLotService>();
            container.RegisterType<IAdvMoneyService, AdvMoneyService>();
            container.RegisterType<IStockTransferService, StockTransferService>();
            container.RegisterType<IRightService, RightService>();
            container.RegisterType<ISC_OrderConfirmationService, SC_OrderConfirmationService>();
            container.RegisterType<IMarginService, MarginService>();
            container.RegisterType<ITransactionHistoryService, TransactionHistoryService>();
            container.RegisterType<IQRService, QRService>();



            // Register SQL Comman
            container.RegisterType<ISystemCommandText, SystemCommandText>();
            container.RegisterType<IOrderCommandText, OrderCommandText>();
            container.RegisterType<ICustomerCommandText, CustomerCommandText>();
            container.RegisterType<ICashTransferCommandText, CashTransferCommandText>();
            container.RegisterType<IEkycCommandText, EkycCommandText>();
            container.RegisterType<IMmCommandText, MmCommandText>();
            container.RegisterType<IOddLotCommandText, OddLotCommandText>();
            container.RegisterType<IMarginCommandText, MarginCommandText>();
            container.RegisterType<INoticationCommandText, NoticationCommandText>();
            container.RegisterType<IMarginCommandText, MarginCommandText>();


            config.DependencyResolver = new UnityResolver(container);

            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/octet-stream"));
            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
