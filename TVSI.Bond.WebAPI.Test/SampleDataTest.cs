﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using TVSI.Bond.WebAPI.Test.DTO;

namespace TVSI.Bond.WebAPI.Test
{
    public class SampleDataTest
    {
        public static CustBalanceDTO GetCashBalance(string custCode)
        {
            var xmlFile = HttpContext.Current.Server.MapPath("~/bin/SampleData/CashBalance.xml");
            var xdoc = XDocument.Load(xmlFile);

            var cashInfo = xdoc.Descendants("Account").FirstOrDefault(x => x.Attribute("CustCode").Value == custCode);

            if (cashInfo != null)
            {
                return new CustBalanceDTO
                       {
                            AccountNo = custCode + "1",
                            Balance = decimal.Parse(cashInfo.Descendants("Balance").Single().Value.Replace(",", "")),
                            StockBalance = decimal.Parse(cashInfo.Descendants("StockBalance").Single().Value.Replace(",", "")),
                            BondBalance = decimal.Parse(cashInfo.Descendants("BondBalance").Single().Value.Replace(",", "")),
                            FundBalance = decimal.Parse(cashInfo.Descendants("FundBalance").Single().Value.Replace(",", "")),
                            MmBalacne = decimal.Parse(cashInfo.Descendants("MmBalacne").Single().Value.Replace(",", "")),
                            Nav = decimal.Parse(cashInfo.Descendants("Nav").Single().Value.Replace(",", "")),
                            Equity = decimal.Parse(cashInfo.Descendants("Equity").Single().Value.Replace(",", "")),
                            Loan_01 = decimal.Parse(cashInfo.Descendants("Loan_01").Single().Value.Replace(",", "")),
                            Loan_02 = decimal.Parse(cashInfo.Descendants("Loan_02").Single().Value.Replace(",", "")),
                            TotalLoan = decimal.Parse(cashInfo.Descendants("TotalLoan").Single().Value.Replace(",", "")),
                };
            }


            return null;
        }
    }
}
