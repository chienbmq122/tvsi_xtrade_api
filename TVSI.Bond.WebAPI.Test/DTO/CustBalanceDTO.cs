﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Bond.WebAPI.Test.DTO
{
    public class CustBalanceDTO
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        [Description("Số dư tiền")]
        public decimal Balance { get; set; }
        [Description("Tài sản cổ phiếu")]
        public decimal StockBalance { get; set; }
        [Description("Tài sản trái phiếu")]
        public decimal BondBalance { get; set; }
        [Description("Tài sản Quỹ")]
        public decimal FundBalance { get; set; }
        [Description("Tài sản MM")]
        public decimal MmBalacne { get; set; }
        [Description("Tổng Nav")]
        public decimal Nav { get; set; }
        [Description("Tổng tài sản")]
        public decimal Equity { get; set; }
        [Description("Nợ trên TK 6")]
        public decimal Loan_01 { get; set; }
        [Description("Vay bên thứ 3")]
        public decimal Loan_02 { get; set; }
        [Description("Tổng nợ")]
        public decimal TotalLoan { get; set; }
    }
}
