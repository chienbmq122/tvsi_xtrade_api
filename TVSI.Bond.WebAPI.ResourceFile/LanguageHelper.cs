﻿using System.Collections.Generic;

namespace TVSI.Bond.WebAPI.ResourceFile
{
    public class LanguageHelper
    {
        private static Dictionary<string, List<ErrorParam>> _errorData = new Dictionary<string, List<ErrorParam>>();

        public static List<ErrorParam> GetErrorListByApiName(string apiName)
        {
            if (_errorData.ContainsKey(apiName))
                return _errorData[apiName];

            if ("SM_LO_SystemLogin".Equals(apiName))
            {
                var errorList = new List<ErrorParam>
                                {
                                    new ErrorParam("MSMLO_001", ResourceFile.SystemModule.MSMLO_001),
                                    new ErrorParam("MSMLO_002", ResourceFile.SystemModule.MSMLO_002)
                                };

                _errorData.Add("SM_LO_SystemLogin", errorList);

                return errorList;
            }

            return new List<ErrorParam>();
        }
    }

    public class ErrorParam
    {
        public ErrorParam(string errorCode, string errorMsg)
        {
            ErrorCode = errorCode;
            ErrorMsg = errorMsg;
        }

        public string ErrorCode { get; set; }
        public string ErrorMsg { get; set; }
    }


}
