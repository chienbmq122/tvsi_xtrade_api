﻿using System.Configuration;

namespace TVSI.Bond.WebAPI.Lib.Infrastructure
{
    public class ConnectionFactory
    {
        public static string XtradeInnoConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["XtradeInnoDB_Connection"].ConnectionString; }
        }

        public static string InnoConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["InnoDB_Connection"].ConnectionString; }
        }

        public static string TbmConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["TbmDB_Connection"].ConnectionString; }
        }

        public static string SystemNotificationConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["SystemNotificationDB_Connection"].ConnectionString; }
        }

        public static string FisDBConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["FISDB_Connection"].ConnectionString; }
        }

        public static string CRMDBConnectString
        {
            get{ return ConfigurationManager.ConnectionStrings["CRMDB_Connection"].ConnectionString; }
        }     
        public static string CommonDBConnectString
        {
            get{ return ConfigurationManager.ConnectionStrings["CommonDB_Connection"].ConnectionString; }
        }      
        public static string EmsDBConnectString
        {
            get{ return ConfigurationManager.ConnectionStrings["EmsDB_Connection"].ConnectionString; }
        }
        
        public static string IpgDBConnectString
        {
            get{ return ConfigurationManager.ConnectionStrings["IPGDB_Connection"].ConnectionString; }
        } 
        public static string WebsiteDB_Connection
        {
            get{ return ConfigurationManager.ConnectionStrings["WebsiteDB_Connection"].ConnectionString; }
        }

        public static string CentralizeDB_Connection
        {
            get{ return ConfigurationManager.ConnectionStrings["CentralizeDB_Connection"].ConnectionString; }
        }
    }
}
