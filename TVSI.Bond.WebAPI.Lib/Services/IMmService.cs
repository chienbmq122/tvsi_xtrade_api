﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models.Mm;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface IMmService
    {
        Task<IEnumerable<MmContractListResult>> GetMmContractList(string accountNo);

        Task<MmContractShortDetailResult> GetMmShortContractDetail(string accountNo, string contractNo);
        Task<MmContractDetailResult> GetMmContractDetail(string contractNo, string accountNo);

        Task<IEnumerable<MmOrderBookResult>> GetMmOrderBook(DateTime tradeDate, string accountNo);
        Task<MmOrderDetailResult> GetMmOrderDetail(string contractNo, string accountNo);

        Task<int> CancelMmOrder(string contractNo, string accountNo);

        Task<IEnumerable<MmOrderHistResult>> GetMmOrderHistList(DateTime fromDate, DateTime toDate, string accountNo);

        Task<IEnumerable<MMOrderConfirmListResult>> GetMmOrderConfirmList(DateTime fromDate, DateTime toDate, string accountNo);
        
        Task<int> ConfirmMmOrder(string accountNo, long requestOrderID);
        Task<IEnumerable<MMOrderPaymentInfoResult>> MM_OC_GetContractPaymentInfo(string contractNo);
        Task<MMOrderConfirmDetailResult> GetMMOrderConfirmDetail(string requestOrderID, string accountNo);
        Task<int> RefuseMmOrder(string accountNo, long requestOrderID, string note);
    }
}
