﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models.TransactionHistory;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface ITransactionHistoryService
    {
        Task<List<StockTransactionHistResponse>> GetStockTransactionHistList(string accountNo,
            DateTime fromDate, DateTime toDate);

        Task<List<StockTransactionDetailResponse>> GetStockTransactionDetailList(StockTransactionDetailRequest request);

        Task<List<OrderHistTransResponse>> GetOrderHistTransList(OrderHistTransRequest request);
    }
}
