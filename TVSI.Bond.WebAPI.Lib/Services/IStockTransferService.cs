﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.StockTransfer;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface IStockTransferService
    {
        Task<List<AvailStockTransferInfoResult>> GetAvailStockTransferList(string accountNo, string symbol, string purpose);

        Task<List<StockTransferInfoResult>> GetStockTransferHist(string accountNo, DateTime fromDate, DateTime toDate, string lang);

        Task<AddStockTransferResult> AddStockTransfer(AddStockTransferRequest model);
    }
}
