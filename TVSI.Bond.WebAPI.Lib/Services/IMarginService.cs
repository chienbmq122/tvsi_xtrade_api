﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.MarginPackage;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface IMarginService
    {
        Task<IEnumerable<MarginPackageResult>> MarginPackageList(MarginPackageRequest model);

        Task<MarginDetailPackageResult> MarginPackageDetails(string MarginName,string userID);

        Task<IEnumerable<PreferentialMarginListResult>> PreferentialMarginList(string lang);
        Task<PreferentialMarginDetailResult> PreferentialMarginDetails(string PrefMarginName);

        Task<bool> InsertServicePreferMargin(InsertServicePreferRequest model);
        Task<bool> InsertServiceMargin(InsertServiceRequest model);
        Task<bool> UpdateServicePackPreferMargin(UpdateServicePreferRequest model);
        Task<bool> UpdateServicePackMargin(UpdateServicePackRequest model);

        Task<ServicePreferResult> GetServicePerferByAccontNo(BaseRequest model);
        Task<ServiceResult> GetServiceByAccontNo(BaseRequest model);
        Task<GetServicePreferHistResult> GetServicePreferHist(BaseRequest model);
        
        Task<GetServiceHistResult> GetServiceHist(BaseRequest model);
        
        Task<LoginAccountResult> CheckAccountMargin(BaseRequest model);



    }
}
