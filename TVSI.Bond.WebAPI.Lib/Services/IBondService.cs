﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.Bond;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface IBondService
    {
        Task<IEnumerable<ProductInfoResult>> TVSI_sAPI_BM_PR_BondProductInfo(string lang);
        Task<IEnumerable<OutrightBoardResult>> TVSI_sAPI_BM_OR_BondOutRightBoard(string accountNo, string bondCode, string lang);
        Task<IEnumerable<BondInfoDetailResult>> TVSI_sAPI_BM_BI_BondInfo(string bondCode, string lang);
        Task<IEnumerable<ProductTypeResult>> TVSI_sAPI_BM_PR_BondProductInfoByType(string lang);
        Task<IEnumerable<ProductTermResult>> TVSI_sAPI_BM_IV_BondProductTermList(string accountNo, int productType, decimal amount, DateTime tradedate, string lang);
        Task<IEnumerable<ProductPayTermResult>> TVSI_sAPI_BM_IV_BondProductPayTermList(string accountNo, int productType, decimal amount, DateTime tradeDate, int term, string lang);
        Task<IEnumerable<FindBondRequestResult>> TVSI_sAPI_BM_IV_FindBondRequest(string accountNo, int productType, decimal amount, DateTime tradeDate, int term, bool isPreSale, int payTerm, string lang);
        Task<IEnumerable<CalBondInfo>> TVSI_sAPI_BM_IV_BondBuyCalBondRate(string accountNo, int productType, decimal amount, DateTime tradeDate, int term, bool isPreSale, int payTerm, string bondCode, DateTime sellDate, string lang);
        Task<IEnumerable<SellBeforeRateInfo>> TVSI_sAPI_BM_IV_GetMMSellBeforeRateInfos(string accountNo, string subBondCode, DateTime tradeDate, string redeemCode, string lang);
        Task<IEnumerable<CalCustBankInfo>> TVSI_sAPI_BM_IV_GetCustBankInfo(string custcode);
        Task<IEnumerable<RedeemCodeInfo>> TVSI_sAPI_BM_IV_CheckRedeemCode(string accountNo, string subBondCode, DateTime tradeDate, long volume, string redeemCode, int redeemType);
        Task<IEnumerable<BuyDataResult>> TVSI_sAPI_BM_IV_BondValidateBuyInfo(string accountNo, string bondCode, string subBondCode, long volume, DateTime tradeDate,
            decimal rate, string redeemCode, DateTime sellDate, int custBankID, decimal cashBalance, string lang);
        Task<IEnumerable<ConfirmBuyInfoResult>> TVSI_sAPI_BM_IV_BondConfirmBuyInfo(string accountNo, string bondCode, string subBondCode, long volume, DateTime tradeDate,
            decimal rate, string redeemCode, DateTime sellDate, int custBankID, bool isHardCopy, decimal cashBalance, string lang);
        Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewContractBuyPrintInfo(string accountNo, string bondCode, string subBondCode, long volume, DateTime tradeDate,
            decimal rate, string redeemCode, DateTime sellDate, int custBankID, string lang);
        Task<IEnumerable<FindSellContractInfoResult>> TVSI_sAPI_BM_IV_FindSellContractInfo(string accountNo, string contractNo, DateTime sellDate);
        Task<IEnumerable<CalcSellContractInfoResult>> TVSI_sAPI_BM_IV_CalcSellContractInfo(string accountNo, string contractNo, DateTime sellDate, long volume, string redeemCode);
        Task<IEnumerable<ValidateSellInfoResult>> TVSI_sAPI_BM_IV_BondValidateSellInfo(string accountNo, string contractNo, DateTime sellDate, long volume, string redeemCode, int custBankID, string lang);
        Task<IEnumerable<ConfirmSellInfoResult>> TVSI_sAPI_BM_IV_BondConfirmSellInfo(string accountNo, string contractNo, DateTime sellDate, long volume, string redeemCode, int custBankID, bool isHardCopy, string lang);
        Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewContractSellPrintInfo(string accountNo, string contractNo, DateTime sellDate, long volume, string redeemCode, int custBankID, string lang);
        Task<IEnumerable<FindRollContractInfoResult>> TVSI_sAPI_BM_IV_FindRollContractInfo(string accountNo, string contractNo);
        Task<IEnumerable<CalcRollContractInfoResult>> TVSI_sAPI_BM_IV_CalcRollContractInfo(string accountNo, string contractNo, string redeemCode);
        Task<IEnumerable<ValidateRollInfoResult>> TVSI_sAPI_BM_IV_BondValidateRollInfo(string accountNo, string contractNo, string redeemCode, int custBankID, string lang);
        Task<IEnumerable<ConfirmRollInfoResult>> TVSI_sAPI_BM_IV_BondConfirmRollInfo(string accountNo, string contractNo, string redeemCode, int custBankID, bool isHardCopy, string lang);
        Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewContractRollPrintInfo(string accountNo, string contractNo, string redeemCode, int custBankID, string lang);
        
        Task<IEnumerable<ProductInfoResult>> TVSI_sAPI_BM_PR_MMProductInfo(string lang);
        Task<IEnumerable<ProductTypeResult>> TVSI_sAPI_BM_PR_MMProductInfoByType(string lang);
        Task<IEnumerable<ProductTermResult>> TVSI_sAPI_BM_IV_MMProductTermList(string accountNo, int productType,
           decimal amount, DateTime tradedate, string lang);

        Task<IEnumerable<FindBondRequestResult>> TVSI_sAPI_BM_IV_FindMMRequest(string accountNo, int productType, decimal amount, DateTime tradeDate, int term, bool isPreSale, int payTerm, string lang);
        Task<IEnumerable<CustBankContract_ActiveContractModel>> TVSI_sAPI_BM_IV_CustBankContract_GetActiveContract(string accountNo, int bondTypeGroup, string lang);
        Task<IEnumerable<CustBankContract_ConfirmResult>> TVSI_sAPI_BM_IV_CustBankContract_Confirm(string contractIds, int custBankId, string lang);
        Task<IEnumerable<CustBankContract_GetListResult>> TVSI_sAPI_BM_IV_CustBankContract_GetList(string accountNo, DateTime fromDate, DateTime toDate, string lang);
        Task<IEnumerable<CustBankContract_GetDetailResult>> TVSI_sAPI_BM_IV_CustBankContract_GetDetail(int id, string lang);
        Task<DateTime> TVSI_sAPI_BM_IV_GetNextTradeDate();
        Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewConfirmationContractBuyPrintInfo(int requestOrderID, string lang);
        Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewConfirmationContractRollPrintInfo(int requestOrderID, string lang);
        Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewConfirmationContractSellPrintInfo(int requestOrderID, string lang);

        Task<IEnumerable<IssuerGroupInfoResult>> TVSI_sAPI_BM_BI_IssueGroupInfo();
        Task<DateTime> TVSI_sAPI_BM_IV_GetMaturityDate(int productType, int term, DateTime tradeDate);
    }
}
