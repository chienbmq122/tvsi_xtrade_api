﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models.Order;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface IOrderService
    {
        Task<IEnumerable<ContractListResult>> GetContractList(ContractListRequest contractListRequest, string lang);

        Task<ContractDetailResult> GetContractDetail(string contractNo, string accountNo);

        Task<IEnumerable<OrderBookResult>> GetOrderBook(DateTime tradeDate, string accountNo);

        Task<OrderDetailResult> GetOrderDetail(string contractNo, string accountNo);

        Task<IEnumerable<OrderHistResult>> GetOrderHistList(DateTime fromDate, DateTime toDate, string accountNo);

        Task<IEnumerable<OrderConfirmListResult>> GetOrderConfirmList(DateTime fromDate, DateTime toDate, string accountNo);

        Task<int> CancelOrder(string contractNo, string accountNo);
        Task<int> ConfirmOrder(long requestOrderID, string accountNo);
        Task<OrderConfirmDetailResult> GetOrderConfirmDetail(string requestOrderID, string accountNo);
        Task<int> RefuseOrder(long requestOrderID, string accountNo, string note);
    }
}
