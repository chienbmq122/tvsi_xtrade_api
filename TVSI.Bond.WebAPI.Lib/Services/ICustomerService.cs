﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models.Customer;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface ICustomerService
    {
        Task<CustBalanceResult> GetCustBalanceInfo(string custCode);

        Task<CustAssetTypeResult> GetCustAssetType(string custCode, int assetType);

        Task<CustInfoResult> GetCustInfo(string custCode, string accountNo);

        Task<CustEmailInfoResult> GetCustEmailInfo(string custCode);

        Task<CustPhoneInfoResult> GetCustPhoneInfo(string custCode);

        Task<IEnumerable<CustBankInfoResult>> GetCustBankInfo(string custCode);

        Task<IEnumerable<FullCustBankInfoResult>> GetFullCustBankInfo(string custCode);

        Task<IEnumerable<AccountNoListResult>> GetAccountNoList(string custCode);

        Task<int> SetDefaultAccount(DefaultAccountRequest model);

        Task<AlphaAccountResult> GetAlphaAccountInfo(string accountNo);

        Task<int> UpdateCustBankStatus(UpdateCustBankRequest model);
    }
}
