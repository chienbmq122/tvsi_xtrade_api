﻿using System.Collections.Generic;
using TVSI.Bond.WebAPI.Lib.Models.System;
using System.Threading.Tasks;
using System;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface ISystemService
    {
        Task<int?> GetUserStatus(string userName, string password);

        Task<IEnumerable<UserLoginResult>> GetAccountNoList(string customerId);

        Task<bool> CheckPassword(string customerId, string password);
        Task<bool> CheckPin(string customerId, string pin);
        
        Task<int> ChangePassword(string customerId, string newPassword, string oldPassword);

        Task<int> ForgotPassword(string customerId, string cardId);
        Task<int> ForgotPIN(string customerId, string cardId);

        Task<int> SendConfirmCode(string confirmCode);
        Task<int> ChangePin(string customerId, string newPin, string oldPin);

        Task<int> SendSms(string custCode, string message, string serviceCode);

        Task<int> SendSms_02(string mobile, string message);
        Task<bool> SendOTPEmail(string OTP, string custcode);

        Task<NotificationResult> GetNotificationList(int pageIndex, int pageSize,
            string accountNo, string lang);

        Task<int> SyncNotificationRead(List<long> idList, string accountNo);

        Task<SearchEventFilterResult> GetEventFilterData(string accountNo);
        Task<IEnumerable<EventListResult>> GetEventList(int pageIndex, int PageSize, string bondCode, int eventType, string accountNo);

        Task<EventDetailResult> GetEventDetail(int eventId);

        Task<DateTime> GetNextTradeDate();
        Task<DateTime> GetNextTradeDateByDate(DateTime date);

        Task<string> GetSBAReferNo();

        DateTime GetSystemDate();
        Task<int> VerifyAccount(string customerId, string email);
        Task<int> InactiveAccount(string customerId);

        Task<int> LoginLdap(string userName, string password);
        string GetLdapUserName(string userId);
        Task<IEnumerable<string>> GetBannerTvsiInfo();

    }
}
