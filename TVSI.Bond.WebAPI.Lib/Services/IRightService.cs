﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models.Right;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface IRightService
    {
        Task<RightInfoResult> GetRightInfo(string accountNo);
        Task<List<RightStatusResult>> GetRightStatus(string accountNo, int PageIndex, int PageSize);
        Task<List<RightHistResult>> GetRightHist(string accountNo, string symbol, int xType,
            DateTime fromDate, DateTime toDate, int pageIndex, int pageSize);

        Task<int> RegistBuy(RegistBuyRequest model, string source);

        Task<int> CancelRegistBuy(string accountNo, string contractNo);
        Task<int> TransferBuy(TransferBuyRequest model, string source);
    }
}
