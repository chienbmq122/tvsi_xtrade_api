﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IBM.Data.DB2;
using log4net;
using TVSI.Bond.WebAPI.Lib.Infrastructure;
using TVSI.Bond.WebAPI.Lib.Models.Customer;
using TVSI.Bond.WebAPI.Lib.Models.System;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class CustomerService : BaseService, ICustomerService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CustomerService));
        public ICustomerCommandText _commandText;
        public ISystemCommandText _systemCommandText;

        public CustomerService(ICustomerCommandText commandText, ISystemCommandText systemCommandText)
        {
            _commandText = commandText;
            _systemCommandText = systemCommandText;
        }

        public async Task<CustBalanceResult> GetCustBalanceInfo(string custCode)
        {
            try
            {
                // Tinh toan du lieu tra ve
                var balance = 0m;   // So du
                var debt = 0m;   // Tong no
                var equity = 0m;    // Tong tai san
                var stockBalance = 0m; // Tong tai san chung khoan
                var bondBalance = 0m;   // Tong tai san trai phieu
                var mmBalance = 0m;         // Tài sản Mm

                using (var conn = new DB2Connection(ConnectionFactory.FisDBConnectionString))
                {
                    // Tài sản tiền tại tài khoản 1 và 6
                    // 20211202 CHECK XEM TAI KHOAN CO DANG NO OVERDUE HAY KO
                    var sql = _commandText.GetCustBalance_01.Replace("{0}", custCode + "1");
                    var overDue = conn.QueryFirstOrDefault<decimal?>(_commandText.GetOverdue_01.Replace("{0}", custCode + "1"));
                    if (overDue.HasValue && overDue > 0)
                    {
                        sql = _commandText.GetCustBalance_01_Overdue.Replace("{0}", custCode + "1");
                    }

                    var balanceInfo_01 = conn.QueryFirstOrDefault<BalanceInfo>(sql);
                    if (balanceInfo_01 != null)
                    {
                        balance += balanceInfo_01.CashBalance;
                        equity += balanceInfo_01.CashBalance;
                        debt += balanceInfo_01.Debt;
                    }

                    // c.equity = c.cash_balance + c.LMV + sum_collateral + max((c.ap + c.ap_t1 + c.ap_t2 - c.Wd_AP), 0) - (c.debt + c.SMV) + remain
                    // asset           = c.cash_balance + c.LMV + collat + max((c.ap + c.ap_t1 + c.ap_t2 - c.Wd_AP),0)
                    var balanceInfo_06 = conn.QueryFirstOrDefault<BalanceInfo>(_commandText.GetCustBalance_06.Replace("{0}", custCode + "6"));
                    if (balanceInfo_06 != null)
                    {
                        balance += balanceInfo_06.CashBalance + balanceInfo_06.Collat
                                  + Math.Max(balanceInfo_06.Ap - balanceInfo_06.AdvWithdraw, 0);
                        
                        debt += balanceInfo_06.Debt;
                        equity += balanceInfo_06.CashBalance + balanceInfo_06.Collat
                                  + Math.Max(balanceInfo_06.Ap - balanceInfo_06.AdvWithdraw, 0);
                    }

                    // Tai san chung khoan + trai phieu
                    var sql_stockBalance = _commandText.GetStockBalance;
                    var stockBalanceList = conn.Query<StockBalanceInfo>(sql_stockBalance.Replace("{0}", custCode + "%")).ToList();
                    foreach (var item in stockBalanceList)
                    {
                        if (item.StockType == "S")
                        {
                            stockBalance += item.Volume * item.Price;
                        }
                        else if (item.Secsymbol == "HDMG" || item.Secsymbol == "HDMG-TP")
                        {
                            //mmBalance += item.Volume * item.Price;
                        }
                        else
                        {
                            //bondBalance += item.Volume * item.Price;
                        }
                    }

                    balance *= 1000;
                    debt *= 1000;
                    stockBalance *= 1000;
                    mmBalance *= 1000;
                    bondBalance *= 1000;
                    equity *= 1000;
                   
                    equity += stockBalance + mmBalance + bondBalance;
                    
                }

                // 20220119-Lấy tài sản BM/MM từ TBM
                // BEGIN
                var enterpriseBondList = await WithTbmConnection(async conn =>
                       await conn.QueryAsync<EnterpriseBondInfo>(_commandText.GetEntepriseBondAsset, new
                       {
                           AccountNo = custCode + "1"
                       }));

                var bondAmount = enterpriseBondList.Select(x => x.Amount).Sum();
                bondBalance += bondAmount;

                var mmList = await WithTbmConnection(async conn =>
                           await conn.QueryAsync<MmAssetInfo>(_commandText.GetMmAsset, new
                           {
                               AccountNo = custCode + "1"
                           }));

                var mmAmount = mmList.Select(x => x.Amount).Sum();
                mmBalance += mmAmount;

                equity += bondAmount + mmAmount;
                // END


                var advAmount = await WithTbmConnection(async conn => 
                    await conn.QueryFirstOrDefaultAsync<decimal>(_commandText.GetAdvanceAmount, new
                    { accountNo = custCode + "1" }));

                return new CustBalanceResult
                       {
                           AccountNo = custCode + "1",
                           Balance = balance,
                           StockBalance = stockBalance,
                           BondBalance = bondBalance,
                           FundBalance = 0,
                           MmBalance = mmBalance,
                           Nav = equity - debt - advAmount,
                           Equity = equity,
                           Loan_01 = debt,
                           Loan_02 = advAmount,
                           TotalLoan = debt + advAmount
                       };
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "GetCustBalanceInfo:custCode=" + custCode);
                throw;
            }

        }

        public async Task<CustAssetTypeResult> GetCustAssetType(string custCode, int assetType)
        {
            try
            {
                var retData = new CustAssetTypeResult
                              {
                                  AccountNo = custCode + "1",
                                  AssetType = assetType
                              };
                if (assetType == (int) AssetTypeEnum.StockAsset)
                {
                    using (var conn = new DB2Connection(ConnectionFactory.FisDBConnectionString))
                    {
                        var sql = _commandText.GetStockAsset;

                        retData.StockAssetList = conn.Query<StockAssetInfo>(sql.Replace("{0}", custCode + "%")).ToList();
                    }
                }
                else if (assetType == (int) AssetTypeEnum.BondAsset)
                {
                    retData.BondAssetInfo = new BondAssetInfo();
                    var enterpriseBondList = await WithTbmConnection(async conn =>
                           await conn.QueryAsync<EnterpriseBondInfo>(_commandText.GetEntepriseBondAsset, new
                           {
                               AccountNo = custCode + "1"
                           }));
                    retData.BondAssetInfo.EnterpriseBondList = enterpriseBondList.ToList();

                    //using (var conn = new DB2Connection(ConnectionFactory.FisDBConnectionString))
                    //{
                    //    var tvsiBondList = conn.Query<TvsiBondInfo>(_commandText.GetTvsiBondAsset, new {param1 = custCode + "%"});

                    //    retData.BondAssetInfo.TvsiBondList = tvsiBondList.ToList();
                    //}
                    retData.BondAssetInfo.TvsiBondList = new List<TvsiBondInfo>();
                }
                else if (assetType == (int) AssetTypeEnum.MmAsset)
                {
                    var mmList = await WithTbmConnection(async conn =>
                           await conn.QueryAsync<MmAssetInfo>(_commandText.GetMmAsset, new
                           {
                               AccountNo = custCode + "1"
                           }));
                    retData.MmAssetList = mmList.ToList();
                }
                else if (assetType == (int) AssetTypeEnum.AdvanceAsset)
                {
                    var advanceList = await WithTbmConnection(async conn =>
                           await conn.QueryAsync<AdvanceAssetInfo>(_commandText.GetAdvanceAsset, new
                           {
                               TradeDate = DateTime.Today.Date,
                               AccountNo = custCode + "1"
                           }));
                    retData.AdvanceAssetList = advanceList.ToList();
                }

                return retData;
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "GetCustAssetType:custCode=" + custCode);
                throw;
            }
        }

        public async Task<CustInfoResult> GetCustInfo(string custCode, string accountNo)
        {
            try
            {
                var data = await WithInnoConnection(async conn =>
                    {
                        var contactInfo = await conn.QueryFirstOrDefaultAsync<CustInfoResult>(
                            _commandText.GetCustInfo, new { CustCode  = custCode});

                        return contactInfo;
                    });

                var data_02 = await WithEmsDBConnection(async conn =>
                {
                    var branchInfo = await conn.QueryFirstOrDefaultAsync<CustInfoResult>(
                        _commandText.GetBranchInfo, new { CustCode = custCode });

                    return branchInfo;
                });

                if (data_02 != null)
                {
                    data.SaleID = data_02.SaleID;
                    data.Branch = data_02.Branch;
                    data.BranchName = data_02.BranchName;
                    data.SaleName = data_02.SaleName;
                }

                var data_03 = await WithCommonDBConnection(async conn =>
                {
                    var marginInfo = await conn.QueryFirstOrDefaultAsync<CustInfoResult>(
                        _commandText.GetMarginGroupInfo, new
                        {
                            AccountNo = accountNo, 
                            CurDate = DateTime.Today.Date
                        });

                    return marginInfo;
                });

                if (data_03 != null)
                {
                    data.CanBuy = data_03.CanBuy;
                    data.CanSell = data_03.CanSell;
                    data.MarginGroup = data_03.MarginGroup;
                }

                return data;
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "GetCustInfo:custCode=" + custCode);
                throw;
            }
        }

        public async Task<CustEmailInfoResult> GetCustEmailInfo(string custCode)
        {
            try
            {
                return await WithInnoConnection(async conn => 
                    await conn.QueryFirstOrDefaultAsync<CustEmailInfoResult>(_commandText.GetCustEmailInfo, new { CustomerID = custCode }));
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "GetCustEmailInfo:custCode=" + custCode);
                throw;
            }
        }

        public async Task<CustPhoneInfoResult> GetCustPhoneInfo(string custCode)
        {
            try
            {
                return await WithInnoConnection(async conn => 
                    await conn.QueryFirstOrDefaultAsync<CustPhoneInfoResult>(_commandText.GetCustPhoneInfo, 
                    new { CustomerID = custCode }));
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "GetCustPhoneInfo:custCode=" + custCode);
                throw;
            }
        }

        public async Task<IEnumerable<CustBankInfoResult>> GetCustBankInfo(string custCode)
        {
            try
            {
                return await WithInnoConnection(async conn => 
                    await conn.QueryAsync<CustBankInfoResult>(_commandText.GetCustBankInfo, new { CustomerID = custCode }));
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "GetCustBankInfo:custCode=" + custCode);
                throw;
            }
        }

        public async Task<IEnumerable<FullCustBankInfoResult>> GetFullCustBankInfo(string custCode)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                    await conn.QueryAsync<FullCustBankInfoResult>(_commandText.GetFullCustBankInfo, new { CustomerID = custCode }));
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "GetFullCustBankInfo:custCode=" + custCode);
                throw;
            }
        }

        public async Task<IEnumerable<AccountNoListResult>> GetAccountNoList(string custCode)
        {
            try
            {
                var accountList = new List<AccountNoListResult>();

                var innoAccountList = await WithInnoConnection(async conn =>
                    await conn.QueryAsync<AccountNoListResult>(_commandText.GetAccountNoList, new { CustomerID = custCode }));

                var centralAccountList = await WithCentralizeDBConnection(async conn 
                    => await conn.QueryAsync<UserLoginResult>(_systemCommandText.GetAccountNoList,
                    new { CustCode = custCode }));

                foreach (var account in centralAccountList)
                {
                    var item = new AccountNoListResult
                    {
                        CustCode = custCode,
                        AccountNo = account.AccountNo,
                        AccountType = !string.IsNullOrEmpty(account.AccountNo) && account.AccountNo.Length >= 7 
                            ? int.Parse(account.AccountNo.Substring(account.AccountNo.Length - 1)) : 0,
                        IsAllowBond = account.IsAllowBond,
                        IsAllowFund = account.IsAllowFund
                    };


                    var innoAccountItem = innoAccountList.FirstOrDefault(x => x.AccountNo == account.AccountNo);
                    if (innoAccountItem != null)
                    {
                        item.IsDefault = innoAccountItem.IsDefault;
                        item.AccountType = innoAccountItem.AccountType;
                    }

                    accountList.Add(item);
                }

                return accountList;
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "GetAccountNoList:custCode=" + custCode);
                throw;
            }
        }

        public Task<int> SetDefaultAccount(DefaultAccountRequest model)
        {
            try
            {
                return WithInnoConnection(async conn =>
                {
                    return await conn.ExecuteAsync(_commandText.SetDefaultAccount, new
                    {
                        DefaultAccountNo = model.DefaultAccountNo,
                        CustomerID = model.UserId
                    });

                });
            }
            catch (Exception ex)
            {
                LogException(log, model.AccountNo, ex, "SetDefaultAccount:accountNo=" + model.AccountNo);
                throw;
            }
        }

        public async Task<AlphaAccountResult> GetAlphaAccountInfo(string accountNo)
        {
            try
            {
                return await WithIpgDBConnection(async conn =>
                {
                    var data = new AlphaAccountResult {AccountNo = accountNo};

                    var alphaAccount = await conn.QueryFirstOrDefaultAsync<string>(_systemCommandText.GetAlphaAccount, new { so_tai_khoan = accountNo });
                    data.IsAlphaAccount = !string.IsNullOrEmpty(alphaAccount);

                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetAlphaAccountInfo:accountNo=" + accountNo);
                throw;
            }
        }

        public Task<int> UpdateCustBankStatus(UpdateCustBankRequest model)
        {
            try
            {
                return WithInnoConnection(async conn =>
                {
                    return await conn.ExecuteAsync(_commandText.UpdateCustBankStatus, new
                    {
                        BankAccountID = model.BankAccountID,
                        CustomerID = model.UserId,
                        BankAccount = model.BankAccount,
                        Status = model.Status
                    });

                });
            }
            catch (Exception ex)
            {
                LogException(log, model.AccountNo, ex, "UpdateCustBankStatus:accountNo=" + model.AccountNo);
                throw;
            }
        }
    }
}
