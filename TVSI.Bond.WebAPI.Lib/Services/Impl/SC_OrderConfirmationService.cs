﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using TVSI.Bond.WebAPI.Lib.Models.SC_OrderConfirm;
using log4net;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using TVSI.Bond.WebAPI.Lib.Utility;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class SC_OrderConfirmationService : BaseService, ISC_OrderConfirmationService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CustomerService));

        public SC_OrderConfirmationService()
        {
        }


        public async Task<List<SC_FindOrderConfirmResult>> SC_GetOrderConfirmationHistory(SC_FindOrderConfirmRequest request)
        {
            try
            {
                using (var conn = new SqlConnection(_innoConnectionString))
                {
                    var data = conn.Query<SC_FindOrderConfirmResult>("dbo.FO_UF_OC_SEL_OrderConfirmationHistory",
                        new
                        {
                            iCustomerID = request.UserId,
                            iAccountNo = request.AccountNo,
                            iSymbol = request.SecSymbol,
                            iOrderStatus = "-1",
                            iOrderSide = request.Side,
                            iBeginDate = request.FromDate.ToString("yyyyMMdd"),
                            iEndDate = request.ToDate.ToString("yyyyMMdd"),
                            iPageIndex = request.PageIndex,
                            iPageSize = request.PageSize
                        },
                        commandType: System.Data.CommandType.StoredProcedure).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                LogException(log, request.UserId, ex, "SC_OrderConfirmationHistory: reqeust =" + JsonConvert.SerializeObject(request));
                return new List<SC_FindOrderConfirmResult>();
            }
        }

        public async Task<List<SC_FindOrderConfirmResult>> SC_GetOrderConfirmationStatus(SC_FindOrderConfirmRequest request)
        {
            try
            {
                using (var conn = new SqlConnection(_innoConnectionString))
                {
                    var data = conn.Query<SC_FindOrderConfirmResult>("dbo.FO_UF_OC_SEL_OrderConfirmationStatus",
                        new
                        {
                            iCustomerID = request.UserId,
                            iAccountNo = request.AccountNo,
                            iSymbol = request.SecSymbol,
                            iOrderStatus = "-1",
                            iOrderSide = request.Side,
                            iBeginDate = request.FromDate.ToString("yyyyMMdd"),
                            iEndDate = request.ToDate.ToString("yyyyMMdd"),
                            iPageIndex = request.PageIndex,
                            iPageSize = request.PageSize
                        },
                        commandType: System.Data.CommandType.StoredProcedure).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                LogException(log, request.UserId, ex, "SC_GetOrderConfirmationStatus: reqeust =" + JsonConvert.SerializeObject(request));
                return new List<SC_FindOrderConfirmResult>();
            }
        }


        public async Task<int> SC_ConfirmOrderConfirmation(SC_UpdateConfirmOrderConfirmationRequest request)
        {
            try
            {
                using (var conn = new SqlConnection(_innoConnectionString))
                {
                    var data = conn.Execute("dbo.FO_UF_OC_SEL_ConfirmOrderConfirmationByIDs",
                        new
                        {
                            iAccountNo = request.AccountNo,
                            iOrderConfirmationIDs = request.OrderConfirmationIDs
                        },
                        commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                }
            }
            catch (Exception ex)
            {
                LogException(log, request.AccountNo, ex, "SC_ConfirmOrderConfirmation: reqeust =" + JsonConvert.SerializeObject(request));
                return 0;
            }
        }

        public async Task<List<SC_OrderConfirmationDetailData>> SC_OrderConfirmationDetail(SC_OrderConfirmationDetailRequest request)
        {
            try
            {
                using (var conn = new SqlConnection(_innoConnectionString))
                {
                    var data = conn.Query<SC_OrderConfirmationDetailData>("dbo.FO_UF_OC_SEL_OrderInfoExecTrans",
                        new
                        {
                            TransTime = request.TransTime,
                            CustomerID = request.AccountNo,
                            Symbol = "",
                            Side = "",
                            ID = request.OrderInfoID,
                            PageSize = request.PageSize ,
                            PageIndex = request.PageIndex
                        },
                        commandType: System.Data.CommandType.StoredProcedure).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                LogException(log, request.AccountNo, ex, "SC_OrderConfirmationDetailResult: reqeust =" + JsonConvert.SerializeObject(request));
                return new List<SC_OrderConfirmationDetailData>();
            }
        }

        public async Task<int> SC_ConfirmALLOrderConfirmation(SC_UpdateALLConfirmOrderConfirmationRequest request)
        {
            try
            {
                using (var conn = new SqlConnection(_innoConnectionString))
                {
                    var dtxFromDate = DateTime.MinValue;
                    var dtxToDate = DateTime.MaxValue;

                    if (!string.IsNullOrEmpty(request.FromDate))
                        dtxFromDate =  DateTime.ParseExact(request.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (!string.IsNullOrEmpty(request.ToDate))
                        dtxToDate = DateTime.ParseExact(request.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    var data = conn.Execute("dbo.FO_UF_OC_SEL_ConfirmAllOrderConfirmations",
                        new
                        {
                            iCustomerID = request.UserId,
                            iAccountNo = request.AccountNo,
                            iSymbol = request.SecSymbol,
                            iOrderSide = request.Side,
                            iBeginDate = dtxFromDate.ToString("yyyyMMdd"),
                            iEndDate = dtxToDate.ToString("yyyyMMdd")
                        },
                        commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                }
            }
            catch (Exception ex)
            {
                LogException(log, request.UserId, ex, "SC_ConfirmALLOrderConfirmation: reqeust =" + JsonConvert.SerializeObject(request));
                throw;
            }
        }

    }
}
