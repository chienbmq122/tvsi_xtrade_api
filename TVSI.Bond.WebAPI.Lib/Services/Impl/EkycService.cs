﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using IBM.Data.DB2;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.Ekyc;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Lib.Utility.Common.Data;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class EkycService : BaseService, IEkycService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(EkycService));
        private IEkycCommandText _commandText;

        public EkycService(IEkycCommandText commandText)
        {
            _commandText = commandText;
        }

        /// <summary>
        /// Đăng ký tài khoản eKYC
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> Tvsi_sDangKyTaiKhoanEkyc(EkycSaveUserInfoRequest model)
        {
            try
            {
                var confirmOTP = DateTime.Now;
                model.CardIssuer = EkycUtils.CutTextCardIssue(model.CardIssuer) ?? model.CardIssuer;

                var manModel = new Man();
                ManInfo maninfo = null;
                var mansql = await GetManInfo();

                var saleId = "";
                if (!string.IsNullOrEmpty(model.SaleID) && model.SaleID.Length >= 4)
                {
                    saleId = await GetSaleID(model.SaleID.Substring(0, 4));
                }

                var branchId = "";
                foreach (var item in mansql)
                {
                    if (!string.IsNullOrEmpty(saleId) && saleId.Length >= 4)
                    {
                        branchId = EkycUtils.ReturnBrandID(saleId.Substring(0, 4));
                        if (!string.IsNullOrEmpty(item.ma_chi_nhanh) &&
                            item.ma_chi_nhanh.Contains(branchId))
                        {
                            maninfo = item;
                            break;
                        }
                    }
                }

                if (maninfo == null)
                {
                    maninfo = mansql.FirstOrDefault(x => x.ma_chi_nhanh == "All");
                }

                if (maninfo != null)
                {
                    var arrPara = maninfo.gia_tri.Split('|');
                    manModel.manName = arrPara[0];
                    manModel.postionName = arrPara[1];
                    manModel.manNo = arrPara[2];
                    manModel.manDate = arrPara[3];
                    manModel.ManCardId = arrPara[4];
                    manModel.ManCardDate = arrPara[5];
                    manModel.ManCardIssue = arrPara[6];
                }

                var ekyccommon = new EkycCommonModel();
                if (model.BankAccountList != null)
                    if (model.BankAccountList.Count > 0)
                    {
                        for (int i = 0; i < model.BankAccountList.Count; i++)
                        {
                            if (i == 0)
                            {
                                if (!string.IsNullOrEmpty(model.BankAccountList[i].BankNo))
                                {
                                    ekyccommon.BankName_01 = await GetBankName(model.BankAccountList[i].BankNo);
                                    ekyccommon.SubBranchName_01 = await GetSubBranchName(
                                        model.BankAccountList[i].BankNo,
                                        model.BankAccountList[i].BranchNo);
                                }
                            }
                            else
                            {
                                ekyccommon.BankName_02 = await GetBankName(model.BankAccountList[i].BankNo);
                                ekyccommon.SubBranchName_02 = await GetSubBranchName(model.BankAccountList[i].BankNo,
                                    model.BankAccountList[i].BranchNo);
                            }
                        }
                    }

                DateTime? tvsiUyQuyen = null;
                if (!string.IsNullOrEmpty(manModel.manDate))
                {
                    tvsiUyQuyen = DateTime.ParseExact(manModel.manDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                DateTime? tvsiManCardDate = null;

                if (!string.IsNullOrEmpty(manModel.ManCardDate))
                {
                    tvsiManCardDate =
                        DateTime.ParseExact(manModel.ManCardDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                log.Info("Tvsi_sDangKyTaiKhoanEkyc -> tvsiUyQuyen" + tvsiUyQuyen);


                var ngaysinh = DateTime.ParseExact(model.Birthday, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var ngaycapcmnd = DateTime.ParseExact(model.IssueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                log.Info("Tvsi_sDangKyTaiKhoanEkyc -> ngaysinh" + ngaysinh);
                log.Info("Tvsi_sDangKyTaiKhoanEkyc -> ngaycapcmnd" + ngaycapcmnd);
                var confirmCode = EkycUtils.EncriptMd5(model.Email + EkycUtils.RandomNumber());
                if (confirmCode.Length > 50)
                    confirmCode = confirmCode.Substring(0, 50);

                var confirmSms = EkycUtils.RandomNumber();

                await WithCRMDBConnection(async conn =>
                {
                    var branchName = "";
                    if (!string.IsNullOrEmpty(saleId) && saleId.Length >= 4)
                        branchName = EkycUtils.ReturnBrandName(saleId.Substring(0, 4));

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@hoten", model.CustomerName.ToTitleCase());
                    queryParameters.Add("@ngaysinh", ngaysinh);
                    queryParameters.Add("@gioitinh", model.Sex);
                    queryParameters.Add("@cmnd", model.CardID);
                    queryParameters.Add("@ngaycapcmnd", ngaycapcmnd);
                    queryParameters.Add("@noicap", model.CardIssuer);
                    queryParameters.Add("@sdt", model.Mobile);
                    queryParameters.Add("@email", model.Email);
                    queryParameters.Add("@diachi", model.Address_02);
                    queryParameters.Add("@branch_id", branchId);
                    queryParameters.Add("@saleid", saleId);
                    queryParameters.Add("@branch_name", branchName);
                    queryParameters.Add("@tvsi_nguoi_dai_dien", manModel.manName ?? string.Empty);
                    queryParameters.Add("@tvsi_chuc_vu", manModel.postionName ?? string.Empty);
                    queryParameters.Add("@tvsi_giay_uq", manModel.manNo ?? string.Empty);
                    queryParameters.Add("@tvsi_ngay_uq", (object) tvsiUyQuyen ?? string.Empty);
                    queryParameters.Add("@tvsi_so_cmnd", manModel.ManCardId ?? string.Empty);
                    queryParameters.Add("@tvsi_noi_cap_cmnd", manModel.ManCardIssue ?? string.Empty);
                    queryParameters.Add("@tvsi_ngay_cap_cmnd", (object) tvsiManCardDate ?? string.Empty);
                    queryParameters.Add("@phuong_thuc_gd", model.TradingMethod ?? string.Empty);
                    queryParameters.Add("@phuong_thuc_nhan_kqgd", model.TradingResultMethod ?? string.Empty);
                    queryParameters.Add("@phuong_thuc_sao_ke", model.TradingReportMethod ?? string.Empty);
                    queryParameters.Add("@dv_chuyen_tien", model.BankTransferMethod);
                    if (model.BankAccountList != null)
                    {
                        if (model.BankAccountList.Count > 0)
                        {
                            for (int i = 0; i < model.BankAccountList.Count; i++)
                            {
                                if (i == 0)
                                {
                                    queryParameters.Add("@chu_tai_khoan_nh_01",
                                        model.BankAccountList[i].BankAccountName ?? string.Empty);
                                    queryParameters.Add("@so_tai_khoan_nh_01",
                                        model.BankAccountList[i].BankAccount ?? string.Empty);
                                    queryParameters.Add("@ma_ngan_hang_01",
                                        model.BankAccountList[i].BankNo ?? string.Empty);
                                    queryParameters.Add("@ngan_hang_01", ekyccommon.BankName_01 ?? string.Empty);
                                    queryParameters.Add("@chi_nhanh_nh_01",
                                        ekyccommon.SubBranchName_01 ?? string.Empty);
                                    queryParameters.Add("@ma_chi_nhanh_01",
                                        model.BankAccountList[i].BranchNo ?? string.Empty);
                                    queryParameters.Add("@tinh_tp_nh_01", "");

                                }
                                else
                                {
                                    queryParameters.Add("@chu_tai_khoan_nh_02",
                                        model.BankAccountList[i].BankAccountName ?? string.Empty);
                                    queryParameters.Add("@so_tai_khoan_nh_02",
                                        model.BankAccountList[i].BankAccount ?? string.Empty);
                                    queryParameters.Add("@ma_ngan_hang_02",
                                        model.BankAccountList[i].BankNo ?? string.Empty);
                                    queryParameters.Add("@ngan_hang_02", ekyccommon.BankName_02 ?? string.Empty);
                                    queryParameters.Add("@chi_nhanh_nh_02",
                                        ekyccommon.SubBranchName_02 ?? string.Empty);
                                    queryParameters.Add("@ma_chi_nhanh_02",
                                        model.BankAccountList[i].BranchNo ?? string.Empty);
                                    queryParameters.Add("@tinh_tp_nh_02", "");
                                }
                            }
                        }
                    }

                    queryParameters.Add("@confirmCode", confirmCode);
                    queryParameters.Add("@confirmSms", confirmSms);
                    queryParameters.Add("@confirmDate", confirmOTP);
                    queryParameters.Add("@fatca_kh_usa", model.Fatca.CustomerUSA);
                    queryParameters.Add("@fatca_noi_sinh_usa", model.Fatca.PlaceOfBirthUSA);
                    queryParameters.Add("@fatca_noi_cu_tru_usa", model.Fatca.DepositoryUSA);
                    queryParameters.Add("@fatca_sdt_usa", model.Fatca.CustomerPhoneNumberUSA);
                    queryParameters.Add("@fatca_lenh_ck_usa", model.Fatca.CustomerTransferBankUSA);
                    queryParameters.Add("@fatca_giay_uy_quyen_usa", model.Fatca.CustomerAuthorizationUSA);
                    queryParameters.Add("@fatca_dia_chi_nhan_thu_usa", model.Fatca.CustomerReceiveMailUSA);
                    queryParameters.Add("@dangkyid", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    await conn.QueryAsync<string>(_commandText.TVSI_sDangKyKhachHangEkycMobile, queryParameters,
                        commandType: CommandType.StoredProcedure);
                    ekyccommon.dangkyid = queryParameters.Get<int>("@dangkyid");
                    log.Info(string.Format("Tra ve Dangkyid - {0}", ekyccommon.dangkyid));
                });

                if (ekyccommon.dangkyid != 0)
                {
                    var tknnmorong = new EkycMorong
                    {
                        MarginAccount = model.MarginAccount,
                        ReceiveContractMethod = model.ReceiveContractMethod,
                        ReceiveContractData = model.ReceiveContractData,
                        FolderPath = model.FolderPath
                    };
                    var jsonData = JsonConvert.SerializeObject(tknnmorong, Formatting.None,
                        new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
                    await WithCRMDBConnection(async conn =>
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@ho_so_mo_tk_id", ekyccommon.dangkyid);
                        queryParameters.Add("@thong_tin_mo_rong", jsonData);
                        await conn.QueryAsync<string>(_commandText.TVSI_sDangKyTaiKhoanEkycMoRongMobile,
                            queryParameters, commandType: CommandType.StoredProcedure);
                    });
                }

                // Send Email to Customer
                if (!string.IsNullOrEmpty(model.Email))
                {
                    var path = "https://www.tvsi.com.vn/user/confirm-ekyc-open-account";
                    EkycUtils.SendEmailOpenEKYC(model.CustomerName, model.Mobile, model.Email, confirmCode, confirmSms,
                        path);
                    log.Info(string.Format("Send Mail Customer {0}", model.Email));
                }

                //Send Email to CS
                if (!string.IsNullOrEmpty(model.SaleID))
                {
                    var para_Content = EkycUtils.ReturnNoiDungMail(model);
                    var para_Subject = "TVSI: Đăng ký mở tài khoản trực tuyến";
                    var strEmail = ConfigurationManager.AppSettings["SendEmailEkyc"] + "";
                    var arrListStr = strEmail.Split(',');
                    var idBranchTrade = ConfigurationManager.AppSettings["IdBranchTransaction"].Split(',');
                    if (saleId.Equals(idBranchTrade[0]))
                    {
                        
                        DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(para_Subject,
                            para_Content, arrListStr[0], 0);
                        DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(para_Subject,
                            para_Content, arrListStr[3], 0);
                        
                    }else if (saleId.Equals(idBranchTrade[1]))
                    {
                        
                        DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(para_Subject,
                            para_Content, arrListStr[1], 0);
                        DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(para_Subject,
                            para_Content, arrListStr[2], 0);
                    }
                    else
                    {
                        var emailSale = await GetEmailSale(model.SaleID);
                        if (emailSale != null)
                        {
                            DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(para_Subject,
                                para_Content, emailSale, 0);
                        }
                    }
                }

                return ekyccommon.dangkyid > 0;
            }
            catch (Exception ex)
            {
                log.Error("Tvsi_sDangKyTaiKhoanEkyc - " + ex.Message);
                log.Error("Tvsi_sDangKyTaiKhoanEkyc - " + ex.InnerException);
                throw;
            }
        }
        
        
        /// <summary>
        /// Đăng ký tài khoản eKYC
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> Tvsi_sDangKyTaiKhoanEkyc_v2(EkycSaveUserInfoRequest_v2 model)
        {
           try
           {
               var saleId = "";
               var saleCheck = true;
               var branchIDCompare = "";
               if (string.IsNullOrEmpty(model.SaleID))
               {
                   saleCheck = false;
                   var provinceCode = !string.IsNullOrEmpty(model.ProvinceCode) ? model.ProvinceCode : "01";
                   var tyProvince = await ProcessSaleId(provinceCode, model.ServicePackage);
                   if (tyProvince > 0)
                   {
                       var data = await GenerateSaleId(model.ServicePackage, tyProvince);
                       if (data != null && data.SaleID.Length >= 4 && !string.IsNullOrEmpty(data.BranchID))
                       {
                           branchIDCompare = data.BranchID;
                           saleId = data.SaleID;
                       }
                   }

               }
               else
                   saleId = model.SaleID;
                
                
               var confirmOTP = DateTime.Now;
               model.CardIssuer = EkycUtils.CutTextCardIssue(model.CardIssuer) ?? model.CardIssuer;

               var manModel = new Man();
               ManInfo maninfo = null;
               var mansql = await GetManInfo();

               var branchId = "";
               foreach (var item in mansql)
               {
                   if (!string.IsNullOrEmpty(saleId) && saleId.Length >= 4)
                   {
                       branchId = EkycUtils.ReturnBrandID(saleId.Substring(0, 4));
                       if (!string.IsNullOrEmpty(item.ma_chi_nhanh) &&
                           item.ma_chi_nhanh.Contains(branchId))
                       {
                           maninfo = item;
                           break;
                       }
                   }
               }

               if (maninfo == null)
               {
                   maninfo = mansql.FirstOrDefault(x => x.ma_chi_nhanh == "All");
               }

               if (maninfo != null)
               {
                   var arrPara = maninfo.gia_tri.Split('|');
                   manModel.manName = arrPara[0];
                   manModel.postionName = arrPara[1];
                   manModel.manNo = arrPara[2];
                   manModel.manDate = arrPara[3];
                   manModel.ManCardId = arrPara[4];
                   manModel.ManCardDate = arrPara[5];
                   manModel.ManCardIssue = arrPara[6];
               }

               var ekyccommon = new EkycCommonModel();
               if (model.BankAccountList != null && model.BankAccountList.Count > 0)
               {
                   for (int i = 0; i < model.BankAccountList.Count; i++)
                   {
                       if (i == 0)
                       {
                           if (!string.IsNullOrEmpty(model.BankAccountList[i].BankNo))
                           {
                               ekyccommon.BankName_01 = await GetBankName(model.BankAccountList[i].BankNo);
                               ekyccommon.SubBranchName_01 = await GetSubBranchName(
                                   model.BankAccountList[i].BankNo,
                                   model.BankAccountList[i].BranchNo);
                           }
                       }
                       else
                       {
                           ekyccommon.BankName_02 = await GetBankName(model.BankAccountList[i].BankNo);
                           ekyccommon.SubBranchName_02 = await GetSubBranchName(model.BankAccountList[i].BankNo,
                               model.BankAccountList[i].BranchNo);
                       }
                   }

               }

               DateTime? tvsiUyQuyen = null;
               if (!string.IsNullOrEmpty(manModel.manDate))
                   tvsiUyQuyen = DateTime.ParseExact(manModel.manDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

               DateTime? tvsiManCardDate = null;

               if (!string.IsNullOrEmpty(manModel.ManCardDate))
                   tvsiManCardDate =
                       DateTime.ParseExact(manModel.ManCardDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
               var ngaysinh = DateTime.ParseExact(model.Birthday, "dd/MM/yyyy", CultureInfo.InvariantCulture);
               var ngaycapcmnd = DateTime.ParseExact(model.IssueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

               var cardExpDate = DateTime.ParseExact(model.CardExpDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
               
               var confirmCode = EkycUtils.EncriptMd5(model.Email + EkycUtils.RandomNumber());
               if (confirmCode.Length > 50)
                   confirmCode = confirmCode.Substring(0, 50);

               var confirmSms = EkycUtils.RandomNumber();

               await WithCRMDBConnection(async conn =>
               {
                   var branchName = "";
                   if (!string.IsNullOrEmpty(saleId) && saleId.Length >= 4)
                       branchName = EkycUtils.ReturnBrandName(saleId.Substring(0, 4));

                   var queryParameters = new DynamicParameters();
                   queryParameters.Add("@hoten", model.CustomerName.ToTitleCase());
                   queryParameters.Add("@ngaysinh", ngaysinh);
                   queryParameters.Add("@gioitinh", model.Sex);
                   queryParameters.Add("@cmnd", model.CardID);
                   queryParameters.Add("@ngaycapcmnd", ngaycapcmnd);
                   queryParameters.Add("@noicap", model.CardIssuer);
                   queryParameters.Add("@sdt", model.Mobile);
                   queryParameters.Add("@email", model.Email);
                   queryParameters.Add("@diachi", model.Address_02);
                   queryParameters.Add("@branch_id", branchId);
                   queryParameters.Add("@saleid", saleId);
                   queryParameters.Add("@branch_name", branchName);
                   queryParameters.Add("@tvsi_nguoi_dai_dien", manModel.manName ?? string.Empty);
                   queryParameters.Add("@tvsi_chuc_vu", manModel.postionName ?? string.Empty);
                   queryParameters.Add("@tvsi_giay_uq", manModel.manNo ?? string.Empty);
                   queryParameters.Add("@tvsi_ngay_uq", (object) tvsiUyQuyen ?? string.Empty);
                   queryParameters.Add("@tvsi_so_cmnd", manModel.ManCardId ?? string.Empty);
                   queryParameters.Add("@tvsi_noi_cap_cmnd", manModel.ManCardIssue ?? string.Empty);
                   queryParameters.Add("@tvsi_ngay_cap_cmnd", (object) tvsiManCardDate ?? string.Empty);
                   queryParameters.Add("@phuong_thuc_nhan_kqgd", model.TradingResultMethod ?? string.Empty);
                   queryParameters.Add("@dv_chuyen_tien", model.BankTransferMethod);
                   if (model.BankAccountList != null)
                   {
                       if (model.BankAccountList.Count > 0)
                       {
                           for (int i = 0; i < model.BankAccountList.Count; i++)
                           {
                               if (i == 0)
                               {
                                   queryParameters.Add("@chu_tai_khoan_nh_01",
                                       model.BankAccountList[i].BankAccountName ?? string.Empty);
                                   queryParameters.Add("@so_tai_khoan_nh_01",
                                       model.BankAccountList[i].BankAccount ?? string.Empty);
                                   queryParameters.Add("@ma_ngan_hang_01",
                                       model.BankAccountList[i].BankNo ?? string.Empty);
                                   queryParameters.Add("@ngan_hang_01", ekyccommon.BankName_01 ?? string.Empty);
                                   queryParameters.Add("@chi_nhanh_nh_01",
                                       ekyccommon.SubBranchName_01 ?? string.Empty);
                                   queryParameters.Add("@ma_chi_nhanh_01",
                                       model.BankAccountList[i].BranchNo ?? string.Empty);
                                   queryParameters.Add("@tinh_tp_nh_01", "");

                               }
                               else
                               {
                                   queryParameters.Add("@chu_tai_khoan_nh_02",
                                       model.BankAccountList[i].BankAccountName ?? string.Empty);
                                   queryParameters.Add("@so_tai_khoan_nh_02",
                                       model.BankAccountList[i].BankAccount ?? string.Empty);
                                   queryParameters.Add("@ma_ngan_hang_02",
                                       model.BankAccountList[i].BankNo ?? string.Empty);
                                   queryParameters.Add("@ngan_hang_02", ekyccommon.BankName_02 ?? string.Empty);
                                   queryParameters.Add("@chi_nhanh_nh_02",
                                       ekyccommon.SubBranchName_02 ?? string.Empty);
                                   queryParameters.Add("@ma_chi_nhanh_02",
                                       model.BankAccountList[i].BranchNo ?? string.Empty);
                                   queryParameters.Add("@tinh_tp_nh_02", "");
                               }
                           }
                       }
                   }
                   queryParameters.Add("@confirmCode", confirmCode);
                   queryParameters.Add("@confirmSms", confirmSms);
                   queryParameters.Add("@confirmDate", confirmOTP);
                   queryParameters.Add("@fatca_kh_usa", model.Fatca.CustomerUSA);
                   queryParameters.Add("@fatca_noi_sinh_usa", model.Fatca.PlaceOfBirthUSA);
                   queryParameters.Add("@fatca_noi_cu_tru_usa", model.Fatca.DepositoryUSA);
                   queryParameters.Add("@fatca_sdt_usa", model.Fatca.CustomerPhoneNumberUSA);
                   queryParameters.Add("@fatca_lenh_ck_usa", model.Fatca.CustomerTransferBankUSA);
                   queryParameters.Add("@fatca_giay_uy_quyen_usa", model.Fatca.CustomerAuthorizationUSA);
                   queryParameters.Add("@fatca_dia_chi_nhan_thu_usa", model.Fatca.CustomerReceiveMailUSA);
                   queryParameters.Add("@CardType", model.CardType);
                   queryParameters.Add("@CardExpDate", cardExpDate);
                   queryParameters.Add("@Note", model.Note);
                   queryParameters.Add("@MarginAccount", model.MarginAccount);
                   queryParameters.Add("@dangkyid", dbType: DbType.Int32, direction: ParameterDirection.Output);
                   await conn.ExecuteAsync("TVSI_sDANG_KY_MO_TAI_KHOAN_EKYC_MOBILE_API_V2", queryParameters,
                       commandType: CommandType.StoredProcedure);
                   ekyccommon.dangkyid = queryParameters.Get<int>("@dangkyid");
               });

               if (ekyccommon.dangkyid > 0)
               {
                   if (model.FolderPath != null)
                   {
                       var accountInfoExtend = new EkycMorong_v2
                       {
                           MarginAccount = model.MarginAccount,
                           FolderPath = model.FolderPath
                       };
                       var jsonData = JsonConvert.SerializeObject(accountInfoExtend, Formatting.None,
                           new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
                       if (!string.IsNullOrEmpty(jsonData))
                       {
                           await WithCRMDBConnection(async conn =>
                           {
                               var queryParameters = new DynamicParameters();
                               queryParameters.Add("@ho_so_mo_tk_id", ekyccommon.dangkyid);
                               queryParameters.Add("@thong_tin_mo_rong", jsonData);
                               await conn.QueryAsync<string>(_commandText.TVSI_sDangKyTaiKhoanEkycMoRongMobile,
                                   queryParameters, commandType: CommandType.StoredProcedure);
                           });   
                       }   
                   }

               }
               if (!string.IsNullOrEmpty(model.Email))
               {
                   var path = "https://www.tvsi.com.vn/user/confirm-ekyc-open-account";
                   EkycUtils.SendEmailOpenEKYC(model.CustomerName, model.Mobile, model.Email, confirmCode, confirmSms,
                       path);
               }

               // trường hợp chọn câu hỏi không có sale
               if (!saleCheck && saleId.Length >= 4)
               {
                   var emailList = ConfigurationManager.AppSettings["SendEmail"];
                   var emailBranch = ConfigurationManager.AppSettings["SendEmailBranchID"];
                   var emailBranchId = emailBranch.Split(',');
                   var item = emailList.Split(',');
                   // Gửi email cho 3 sale nếu chọn câu hỏi 3
                   if (model.ServicePackage == 3 && item != null && item.Length > 0)
                   {
                       for (int i = 0; i < item.Length; i++)
                           MailHelper.SendEmailToSaleEkyc(model,item[i]);
                   }
                   else
                   {
                       // Gửi Email cho quản lý
                       if (emailBranchId != null && emailBranchId.Length > 0)
                       {
                           if (branchIDCompare == UtilsBranchID.DVGDHS)
                               MailHelper.SendEmailToSaleEkyc(model,emailBranchId[0]);
                           else if (branchIDCompare == UtilsBranchID.DVGDHCM)
                               MailHelper.SendEmailToSaleEkyc(model,emailBranchId[1]);
                           else if (branchIDCompare == UtilsBranchID.DVDTHCM) 
                               MailHelper.SendEmailToSaleEkyc(model,emailBranchId[2]);
                           else if (branchIDCompare == UtilsBranchID.DVDTHS)
                               MailHelper.SendEmailToSaleEkyc(model,emailBranchId[3]);
                       }
                       
                   }
                   // Gửi Email cho sale
                   var saleInfo = await GetSaleInfo(saleId);
                   LogInfo(log,"CreateCustomer_v2", $"Get SaleInfo {saleInfo.SaleName}");
                   if (saleInfo != null && !string.IsNullOrEmpty(saleInfo.Email))
                   {
                       LogInfo(log,"CreateCustomer_v2",$"EK_CA_CreateCustomer_v2 thong tin sale gui email {saleInfo.Email} SaleName {saleInfo.SaleName}");
                       MailHelper.SendEmailToSaleEkyc(model,saleInfo.Email,"",saleInfo.SaleName);
                   }
               }
               // trường hợp chọn sale không chọn câu hỏi
               else
               {
                   if (!string.IsNullOrEmpty(saleId) &&  saleId.Length >= 4)
                   {
                       var saleInfo = await GetSaleInfo(saleId);
                       LogInfo(log,"EK_CA_CreateCustomer_v2", $"Get SaleInfo {saleInfo.SaleName}");
                       var departmentName = EkycUtils.ReturnBrandName(saleId.Substring(0, 4)) ?? "";
                       if (saleInfo != null && !string.IsNullOrEmpty(saleInfo.Email))
                       {
                           LogInfo(log,"CreateCustomer_v2",$"EK_CA_CreateCustomer_v2 thong tin sale gui email {saleInfo.Email} SaleName {saleInfo.SaleName}");
                           MailHelper.SendEmailToSaleEkyc(model, saleInfo.Email,departmentName,saleInfo.SaleName);
                       }
                   }
               }
               
               return ekyccommon.dangkyid > 0;
           }
           catch (Exception ex)
           {
               LogException(log, "CreateCustomer_v2", ex, "Tvsi_sDangKyTaiKhoanEkyc_v2");
               throw;
           }
        }
        

        /// <summary>
        /// Get thong tin theo CardID
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public async Task<AccountData> Tvsi_sGetInfoCustomerCardID(string cardId)
        {
            try
            {
                return await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<AccountData>(_commandText.TVSI_sGetInfoCustomer, new
                    {
                        @so_cmnd = cardId
                    }, commandType: CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("Tvsi_sGetInfoCustomerCardID" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<InternalAccountInfoResult>> GetInternalAccountInfo(BankAccountRequest model)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryAsync<InternalAccountInfoResult>(_commandText.GetInternalAccountInfo, new
                    {
                        CustCode = model.UserID,
                        AccountNoLike = model.UserID + "%",
                        AccountNo = model.AccountNo,
                    });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetInternalAccountInfo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<bool> SLogThayDoiThongTinKhEkyc(RegistCustomerServiceResult model, string statuscontent,
            string contentchange)
        {
            try
            {
                var custInfo = await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<CustInfo>(_commandText.GetCustInfo, new
                    {
                        @CustCode = model.UserID
                    });
                    return data;
                });
                if (custInfo != null)
                {
                    var jsonModel = JsonConvert.SerializeObject(model, Formatting.Indented);
                    await WithCommonDBConnection(async conn =>
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@trangthainoidung", statuscontent);
                        queryParameters.Add("@makhachhang", custInfo.UserId);
                        queryParameters.Add("@sotaikhoan", custInfo.AccountNo);
                        queryParameters.Add("@tenkhachhang", custInfo.CustName);
                        queryParameters.Add("@noidungthaydoi", jsonModel);
                        queryParameters.Add("@thongtinthaydoi", contentchange);
                        queryParameters.Add("@status", 0);
                        queryParameters.Add("@nguoitao", custInfo.UserId);
                        queryParameters.Add("@thongtinthaydoiid", dbType: DbType.Int32,
                            direction: ParameterDirection.Output);
                        await conn.QueryAsync<string>(_commandText.TVSI_SLogThayDoiThongTinKHEkyc, queryParameters,
                            commandType: CommandType.StoredProcedure);
                        custInfo.ChangeInfoID = queryParameters.Get<int>("@thongtinthaydoiid");
                    });
                    return custInfo.ChangeInfoID > 0;
                }

                return false;
            }
            catch (Exception ex)
            {
                log.Error("SLogThayDoiThongTinKhEkyc" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> SLogThayDoiThongTinKhEkycDelInterAcc(InternalAccountDetailRequest model,
            string statuscontent, string contentchange)
        {
            try
            {
                var custInfo = await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<CustInfo>(_commandText.GetCustInfo, new
                    {
                        @CustCode = model.UserID
                    });
                    return data;
                });
                if (custInfo != null)
                {
                    var jsonModel = JsonConvert.SerializeObject(model, Formatting.Indented);
                    await WithCommonDBConnection(async conn =>
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@trangthainoidung", statuscontent);
                        queryParameters.Add("@makhachhang", custInfo.UserId);
                        queryParameters.Add("@sotaikhoan", custInfo.AccountNo);
                        queryParameters.Add("@tenkhachhang", custInfo.CustName);
                        queryParameters.Add("@noidungthaydoi", jsonModel);
                        queryParameters.Add("@thongtinthaydoi", contentchange);
                        queryParameters.Add("@status", 0);
                        queryParameters.Add("@nguoitao", custInfo.UserId);
                        queryParameters.Add("@thongtinthaydoiid", dbType: DbType.Int32,
                            direction: ParameterDirection.Output);
                        await conn.QueryAsync<string>(_commandText.TVSI_SLogThayDoiThongTinKHEkyc, queryParameters,
                            commandType: CommandType.StoredProcedure);
                        custInfo.ChangeInfoID = queryParameters.Get<int>("@thongtinthaydoiid");
                    });
                }

                return custInfo != null && custInfo.ChangeInfoID > 0;
            }
            catch (Exception ex)
            {
                log.Error("SLogThayDoiThongTinKhEkycDelInterAcc" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> SLogThayDoiThongTinKhEkycRegisterBank(RegisterBankRequest model, string statuscontent,
            string contentchange)
        {
            try
            {
                var custInfo = await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<CustInfo>(_commandText.GetCustInfo, new
                    {
                        @CustCode = model.UserID
                    });
                    return data;
                });
                if (custInfo != null)
                {
                    var jsonModel = JsonConvert.SerializeObject(model, Formatting.Indented);
                    await WithCommonDBConnection(async conn =>
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@trangthainoidung", statuscontent);
                        queryParameters.Add("@makhachhang", custInfo.UserId);
                        queryParameters.Add("@sotaikhoan", custInfo.AccountNo);
                        queryParameters.Add("@tenkhachhang", custInfo.CustName);
                        queryParameters.Add("@noidungthaydoi", jsonModel);
                        queryParameters.Add("@thongtinthaydoi", contentchange);
                        queryParameters.Add("@status", 0);
                        queryParameters.Add("@nguoitao", custInfo.UserId);
                        queryParameters.Add("@thongtinthaydoiid", dbType: DbType.Int32,
                            direction: ParameterDirection.Output);
                        await conn.QueryAsync<string>(_commandText.TVSI_SLogThayDoiThongTinKHEkyc, queryParameters,
                            commandType: CommandType.StoredProcedure);
                        custInfo.ChangeInfoID = queryParameters.Get<int>("@thongtinthaydoiid");
                    });
                }

                return custInfo != null && custInfo.ChangeInfoID > 0;
            }
            catch (Exception ex)
            {
                log.Error("SLogThayDoiThongTinKhEkycRegisterBank" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> SLogThayDoiThongTinKhEkycDelBank(DeleteBankRequest model, string statuscontent,
            string contentchange)
        {
            try
            {
                var custInfo = await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<CustInfo>(_commandText.GetCustInfo, new
                    {
                        @CustCode = model.UserID
                    });
                    return data;
                });
                if (custInfo != null)
                {
                    var JsonModel = JsonConvert.SerializeObject(model, Formatting.Indented);
                    await WithCommonDBConnection(async conn =>
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@trangthainoidung", statuscontent);
                        queryParameters.Add("@makhachhang", custInfo.UserId);
                        queryParameters.Add("@sotaikhoan", custInfo.AccountNo);
                        queryParameters.Add("@tenkhachhang", custInfo.CustName);
                        queryParameters.Add("@noidungthaydoi", JsonModel);
                        queryParameters.Add("@thongtinthaydoi", contentchange);
                        queryParameters.Add("@status", 0);
                        queryParameters.Add("@nguoitao", custInfo.UserId);
                        queryParameters.Add("@thongtinthaydoiid", dbType: DbType.Int32,
                            direction: ParameterDirection.Output);
                        await conn.QueryAsync<string>(_commandText.TVSI_SLogThayDoiThongTinKHEkyc, queryParameters,
                            commandType: CommandType.StoredProcedure);
                        custInfo.ChangeInfoID = queryParameters.Get<int>("@thongtinthaydoiid");
                    });
                }

                return custInfo != null && custInfo.ChangeInfoID > 0;
            }
            catch (Exception ex)
            {
                log.Error("SLogThayDoiThongTinKhEkycRegisterBank" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Đặt lịch video call
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> SCancelDatLichVideoCall(CancelVideoCallScheduleRequest model)
        {
            try
            {
                var rowcount = 0;
                await WithCommonDBConnection(async conn =>
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@datlichvideocallid", model.ScheduleID);
                    queryParameters.Add("@makhachhang", model.UserID);
                    rowcount = await conn.QueryFirstOrDefaultAsync<int>(_commandText.TVSI_SCancelVideoCall,
                        queryParameters, commandType: CommandType.StoredProcedure);
                });
                return rowcount > 0;
            }
            catch (Exception ex)
            {
                log.Error("SCancelDatLichVideoCall" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Hàm gửi request TĐTT 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> ChangeDataCustInfo(ChangeUserInfoRequest model)
        {
            try
            {
                var dataCustType = await WithCommonDBConnection(async conn =>
                {
                    var queryPara = new DynamicParameters();
                    queryPara.Add("@so_tai_khoan", model.UserID);
                    return await conn.QueryFirstOrDefaultAsync<GetCustInfo044v2>(_commandText.TVSI_SGetCustInfoCommon,
                        queryPara, commandType: CommandType.StoredProcedure);
                });
                var ListChangeInfo = new List<ChangeCustInfo>();
                var keyThayDoi = string.Empty;
                var moTa = string.Empty;
                int isSbaInno = 0;
                var loaiKhvsd = "3";
                if (dataCustType != null)
                {
                    var CustType = dataCustType.loai_khach_hang + "";
                    if ("2".Equals(CustType)) // To chuc trong nuoc
                        loaiKhvsd = "5";
                    else if ("3".Equals(CustType)) // Ca nhan nuoc ngoai
                        loaiKhvsd = "4";
                    else if ("4".Equals(CustType)) // To chuc nuoc ngoai
                        loaiKhvsd = "6";
                }

                var dataCust = await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<GetUserCust>(_commandText.GetUserInfoCust, new
                    {
                        @ma_khach_hang = model.UserID,
                    });
                    return data;
                });

                var alreadyReg = new AlreadyRegistered
                {
                    AccountNo = dataCust.ma_kinh_doanh,
                    AccountName = dataCust.CustName,
                    AccountType = loaiKhvsd,
                    Address = dataCust.Address,
                    BirthDay = dataCust.ngay_sinh,
                    CardNo = dataCust.CardID,
                    CardIssueDate = dataCust.ngay_cap.ToString("dd/MM/yyyy"),
                    CardIssuer = dataCust.noi_cap,
                    Sex = dataCust.Gender
                };
                moTa += "<b>Thay đổi:</b> ";
                if (model.FieldChange.ChangeCardIssuer || model.FieldChange.ChangeIssueDate ||
                    model.FieldChange.ChangeCardID)
                {
                    if (!string.IsNullOrEmpty(model.IssueDate) || !string.IsNullOrEmpty(model.CardIssuer) ||
                        !string.IsNullOrEmpty(model.CardID))
                    {
                        var changeCardInfo = await GetCMNDChangeInfo(
                            alreadyReg.CardIssueDate, model.IssueDate,
                            alreadyReg.CardIssuer, model.CardIssuer, alreadyReg.CardNo, model.CardID);
                        if (changeCardInfo != null)
                        {
                            ListChangeInfo.Add(changeCardInfo);
                        }

                        keyThayDoi += "{" + EkycChangeCustInfoConst.NAME_CMND + "},";
                        if (!changeCardInfo.Values[0].Equals(changeCardInfo.Values[1]))
                            moTa += "Số CMND/Passport,";
                        else if (!changeCardInfo.Values[2].Equals(changeCardInfo.Values[3]))
                            moTa += "Ngày cấp CMND/Passport,";
                        else
                            moTa += "CMND/Passport,";
                        isSbaInno = 1;
                    }
                }


                if (model.FieldChange.ChangeAddress)
                {
                    if (!string.IsNullOrEmpty(model.Address))
                    {
                        var changeAddressInfo = await GetAddressChangeInfo(dataCust.Address, model.Address);
                        if (changeAddressInfo != null)
                        {
                            ListChangeInfo.Add(changeAddressInfo);
                            keyThayDoi += "{" + EkycChangeCustInfoConst.NAME_DIA_CHI + "},";
                            moTa += "Địa chỉ,";
                            isSbaInno = 1;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(keyThayDoi))
                {
                    moTa = moTa.Substring(0, moTa.Length - 1);
                    keyThayDoi = keyThayDoi.Substring(0, keyThayDoi.Length - 1);
                    moTa = moTa.Replace(",", ", ");
                    var noiDung = JsonConvert.SerializeObject(ListChangeInfo, Formatting.None,
                        new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
                    var thongTinDaDangKy = JsonConvert.SerializeObject(alreadyReg, Formatting.None,
                        new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        });
                    long newLogID = 0;
                    var saveLog = await SInsertChangeInfoCust(dataCust.CustCode, dataCust.ma_kinh_doanh,
                        dataCust.CustName,
                        thongTinDaDangKy, keyThayDoi, noiDung, moTa, (int) EkycChangeCustInfoStatus.CHO_XU_LY,
                        dataCust.CustCode, DateTime.Now, isSbaInno, EkycCommondConst.BRANHID_NO,
                        newLogID);
                    return saveLog;
                }

                return false;
            }
            catch (Exception ex)
            {
                log.Error("ChangeDataCustInfo that bai" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<bool> CheckChangeInfoCust(FieldChangeRequest model)
        {
            try
            {
                var checkChangeCust = await WithCommonDBConnection(async conn => await conn.QueryAsync<string>(
                    _commandText.CheckChangeInfoCust, new
                    {
                        @UserId = model.UserID
                    }));
                var check = false;
                var changeCust = checkChangeCust.ToList();
                if (changeCust.Any())
                {
                    if (model.ChangeAddress)
                        check = changeCust.Where(x => x.Trim().Contains(EkycChangeCustInfoConst.NAME_DIA_CHI))
                            .Select(x => x).Any();

                    if (!check && model.ChangeCard)
                        check = changeCust.Where(x => x.Trim().Contains(EkycChangeCustInfoConst.NAME_CMND))
                            .Select(x => x).Any();

                    if (!check && model.ChangePhoneSms)
                        check = changeCust.Where(x => x.Trim().Contains(EkycChangeCustInfoConst.NAME_SMS))
                            .Select(x => x).Any();
                }

                return check;
            }
            catch (Exception ex)
            {
                log.Error("CheckChangeInfoCust that bai" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// send request TDTT Phone
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> ChangePhoneCust(ChangePhoneCust model)
        {
            try
            {
                var dataCustType = await WithCommonDBConnection(async conn =>
                {
                    var queryPara = new DynamicParameters();
                    queryPara.Add("@so_tai_khoan", model.UserID);
                    return await conn.QueryFirstOrDefaultAsync<GetCustInfo044v2>(_commandText.TVSI_SGetCustInfoCommon,
                        queryPara, commandType: CommandType.StoredProcedure);
                });
                var listChangeInfo = new List<ChangeCustInfo>();
                var keyThayDoi = string.Empty;
                var moTa = string.Empty;
                int isSbaInno = 0;
                var loaiKhvsd = "3";
                if (dataCustType != null)
                {
                    var custType = dataCustType.loai_khach_hang + "";
                    if ("2".Equals(custType)) // To chuc trong nuoc
                        loaiKhvsd = "5";
                    else if ("3".Equals(custType)) // Ca nhan nuoc ngoai
                        loaiKhvsd = "4";
                    else if ("4".Equals(custType)) // To chuc nuoc ngoai
                        loaiKhvsd = "6";
                }

                var dataCust = await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<GetUserCust>(_commandText.GetUserInfoCust, new
                    {
                        @ma_khach_hang = model.UserID,
                    });
                    return data;
                });
                var sqlInno = "SELECT Sex FROM CUSTOMER T where CUSTOMERID = @CustID";
                var genderInfo = await WithInnoConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                    sqlInno,
                    new
                    {
                        @CustID = model.UserID
                    }));
                var alreadyReg = new AlreadyRegistered
                {
                    AccountNo = dataCust.ma_kinh_doanh,
                    AccountName = dataCust.CustName,
                    AccountType = loaiKhvsd,
                    Address = dataCust.Address,
                    BirthDay = dataCust.ngay_sinh,
                    CardNo = dataCust.CardID,
                    CardIssueDate = dataCust.ngay_cap.ToString("dd/MM/yyyy"),
                    CardIssuer = dataCust.noi_cap,
                    Sex = genderInfo
                };
                /*if (model.OptionsSms == 1 || model.OptionsSms == 2)
                {
                    moTa += "<b>Thay đổi:</b> ";
                    
                }*/
                if (model.OptionsSms == 1 || model.OptionsSms == 2)
                {
                    moTa += "<b>ĐK Dịch vụ</b>:";
                    var changeInfo = await GetSMSChangeInfo(model.OptionsSms);
                    if (changeInfo != null)
                    {
                        listChangeInfo.Add(changeInfo);
                        keyThayDoi += "{" + EkycChangeCustInfoConst.NAME_SMS + "},";
                        if (model.OptionsSms == 1)
                        {
                            moTa += "Đăng ký SMS,";
                        }
                        else if (model.OptionsSms == 2)
                        {
                            moTa += "Hủy đăng ký SMS,";
                        }
                        else
                        {
                            moTa += "SMS,";
                        }
                    }
                }

                if (model.OptionsSms == 1 || model.OptionsSms == 3)
                {
                    var changeInfo =
                        await GetSoDienThoai01ChangeInfo(model.PhoneChange_02, model.PhoneChange_01, model.OptionsSms);
                    if (changeInfo != null)
                    {
                        listChangeInfo.Add(changeInfo);
                        keyThayDoi += "{" + EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_01 + "},";

                        if (changeInfo.Options[0] == 1)
                            moTa += "Đăng ký Số ĐT 01,";
                        if (changeInfo.Options[0] == 2)
                            moTa += "Hủy đăng ký Số ĐT 01,";
                        if (changeInfo.Options[0] == 3)
                            moTa += "Thay đổi Số ĐT 01,";
                        else
                            moTa += "Số ĐT 01,";

                        isSbaInno = 1;
                    }
                }

                if (!string.IsNullOrEmpty(model.PhoneChange_02) && model.OptionsSms == 3)
                {
                    /*var phoneCC = await WithInnoConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                        _commandText.GetPhoneByNumCustInno02, new
                        {
                            @custid = model.UserID
                        }));*/
                    var changeInfo =
                        await GetSoDienThoai02ChangeInfo(model.PhoneChange_01, model.PhoneChange_02);
                    if (changeInfo != null)
                    {
                        listChangeInfo.Add(changeInfo);

                        keyThayDoi += "{" + EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_02 + "},";
                        if (changeInfo.Options[0] == 1)
                            moTa += "Đăng ký Số ĐT 02,";
                        if (changeInfo.Options[0] == 2)
                            moTa += "Hủy đăng ký Số ĐT 02,";
                        if (changeInfo.Options[0] == 3)
                            moTa += "Thay đổi Số ĐT 02,";
                        else
                            moTa += "Số ĐT 02,";

                        isSbaInno = 1;
                    }
                }

                if (!string.IsNullOrEmpty(keyThayDoi))
                {
                    moTa = moTa.Substring(0, moTa.Length - 1);
                    keyThayDoi = keyThayDoi.Substring(0, keyThayDoi.Length - 1);
                    moTa = moTa.Replace(",", ", ");
                    var noiDung = JsonConvert.SerializeObject(listChangeInfo, Formatting.None,
                        new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
                    var thongTinDaDangKy = JsonConvert.SerializeObject(alreadyReg, Formatting.None,
                        new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        });
                    long newLogID = 0;
                    return await SInsertChangeInfoCust(dataCust.CustCode, dataCust.ma_kinh_doanh, dataCust.CustName,
                        thongTinDaDangKy, keyThayDoi, noiDung, moTa, (int) EkycChangeCustInfoStatus.CHO_XU_LY, dataCust.CustCode,
                        DateTime.Now, isSbaInno, EkycCommondConst.BRANHID_NO, newLogID);
                }

                return false;
            }

            catch (Exception ex)
            {
                log.Error("ChangePhoneCust that bai" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<EkycCompareNotSameResult>> CompareTwoCust(CompareInfoCustRequest model,
            CompareInfoCustDBRequest dataCust)
        {
            try
            {
                var listNotSame = new List<EkycCompareNotSameResult>();
                if (model != null && dataCust != null)
                {
                    var cardIssueModelList = EkycUtils.CutTextCardIssueCompare(model.CardIssuer);
                    var cardIssueGetList = EkycUtils.CutTextCardIssueCompare(dataCust.CardIssuer);

                    var cardIssueCheck = false;

                    if (cardIssueModelList.Count > 0 && cardIssueGetList.Count > 0)
                    {
                        if (cardIssueGetList.Except(cardIssueModelList, StringComparer.OrdinalIgnoreCase) != null)
                        {
                            cardIssueCheck = true;
                        }
                    }
                    else if (cardIssueModelList.Count > 0 && cardIssueGetList.Count <= 0)
                    {
                        if (cardIssueModelList.Contains(dataCust.CardIssuer, StringComparer.CurrentCultureIgnoreCase))
                        {
                            cardIssueCheck = true;
                        }
                    }
                    else
                    {
                        if (dataCust.CardIssuer.Replace("CA", "").Trim().ToLower().IndexOf(
                                model.CardIssuer.Replace("CA", "").Trim().ToLower(),
                                StringComparison.OrdinalIgnoreCase) >=
                            0)
                        {
                            cardIssueCheck = true;
                        }
                    }

                    var issueDateModel =
                        DateTime.ParseExact(model.IssueDate.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture);

                    var birthDayModel = DateTime.ParseExact(model.BirthDay, "dd/MM/yyyy", CultureInfo.CurrentCulture);

                    if (dataCust.FullName.Trim().ToLower().IndexOf(model.FullName.Trim().ToLower(),
                            StringComparison.OrdinalIgnoreCase) >= 0
                        && model.CardID.Trim().Equals(dataCust.CardID.Trim())
                        && cardIssueCheck
                        && issueDateModel.Date == dataCust.IssueDate.Date
                        && birthDayModel.Date == dataCust.BirthDay.Date
                    )
                    {
                        if (model.Sex != 3)
                        {
                            if (model.Sex == dataCust.Sex)
                            {
                                return new List<EkycCompareNotSameResult>();
                            }
                        }
                        else
                        {
                            return new List<EkycCompareNotSameResult>();
                        }
                    }

                    if (dataCust.FullName.Trim().ToLower().IndexOf(model.FullName.Trim().ToLower(),
                        StringComparison.OrdinalIgnoreCase) < 0)
                    {
                        listNotSame.Add(new EkycCompareNotSameResult()
                        {
                            issueField = EkycCompareInfoCust.FULLNAME,
                            issueMessage = "Họ và Tên"
                        });
                    }

                    if (!string.Equals(model.CardID.Trim(), dataCust.CardID.Trim(),
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        listNotSame.Add(new EkycCompareNotSameResult()
                        {
                            issueField = EkycCompareInfoCust.CARID,
                            issueMessage = "Số CMT/CCCD"
                        });
                    }


                    if (!cardIssueCheck)
                    {
                        listNotSame.Add(new EkycCompareNotSameResult()
                        {
                            issueField = EkycCompareInfoCust.CARDISSUE,
                            issueMessage = "Nơi cấp CMT/CCCD"
                        });
                    }

                    if (issueDateModel.Date != dataCust.IssueDate.Date)
                    {
                        listNotSame.Add(new EkycCompareNotSameResult()
                        {
                            issueField = EkycCompareInfoCust.ISSUEDATE,
                            issueMessage = "Ngày cấp CMT/CCCD"
                        });
                    }

                    if (birthDayModel.Date != dataCust.BirthDay.Date)
                    {
                        listNotSame.Add(new EkycCompareNotSameResult()
                        {
                            issueField = EkycCompareInfoCust.BIRTHDAY,
                            issueMessage = "Ngày sinh"
                        });
                    }

                    if (model.Sex != 3)
                    {
                        if (model.Sex != dataCust.Sex)
                        {
                            listNotSame.Add(new EkycCompareNotSameResult()
                            {
                                issueField = EkycCompareInfoCust.SEX,
                                issueMessage = "Giới tính"
                            });
                        }
                    }
                }

                return listNotSame;
            }
            catch (Exception ex)
            {
                log.Error("CompareTwoCust that bai" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Compare thong tin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<EkycCompareNotSameResult>> CompareEkycCust(CompareInfoCustRequest model)
        {
            try
            {
                var dataGet = await WithCommonDBConnection(async conn
                    => await conn.QueryFirstOrDefaultAsync<CompareInfoCustDBRequest>(_commandText.CompareInfoCust,
                        new
                        {
                            @custcode = model.UserID
                        }));
                var sqlInno =
                    "SELECT CASE WHEN T.SEX = 'F' THEN 2 when T.SEX = 'M' then 1 end as Sex FROM CUSTOMER T where CUSTOMERID = @CustID";
                var genderInfo = await WithInnoConnection(async conn => await conn.QueryFirstOrDefaultAsync<int?>(
                    sqlInno,
                    new
                    {
                        @CustID = model.UserID
                    }));

                if (genderInfo != null)
                    dataGet.Sex = genderInfo;

                if (dataGet != null)
                {
                    return await CompareTwoCust(model, dataGet);
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("CompareEkycCust that bai" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<VideoCallScheduleHistInfoResult>> GetVideoCallScheduleHist(
            VideoCallHistRequest model)
        {
            try
            {
                var toDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy",
                    System.Globalization.CultureInfo.InvariantCulture);
                var sToDate = toDate.ToString("yyyy-MM-dd");
                var fromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy",
                    System.Globalization.CultureInfo.InvariantCulture);
                var sFromDate = fromDate.ToString("yyyy-MM-dd");
                return await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryAsync<VideoCallScheduleHistInfoResult>(
                        _commandText.GetVideoCallScheduleHist, new
                        {
                            @FromDate = sFromDate,
                            @ToDate = sToDate,
                            @custcode = model.UserID
                        });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetVideoCallScheduleHist" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<RegistServiceHistInfoResult>> GetChangeInfoHist(ChangeInfoHistRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;
                var toDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var sToDate = toDate.ToString("yyyy-MM-dd");
                var fromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var sFromDate = fromDate.ToString("yyyy-MM-dd");
                return await WithCommonDBConnection(async conn =>
                {
                    var result = new List<RegistServiceHistInfoResult>();
                    var data = await conn.QueryAsync<RegistServiceHistInfoResult>(_commandText.GetChangeInfoHist, new
                    {
                        @FromDate = sFromDate,
                        @ToDate = sToDate,
                        @custcode = model.UserID,
                        @Start = start,
                        @PageSize = model.PageSize
                    });

                    var reasonReject = await conn.QueryAsync<RegistServiceHistInfoResult>(
                        _commandText.GetChangeInfoHistReject, new
                        {
                            @FromDate = sFromDate,
                            @ToDate = sToDate,
                            @custcode = model.UserID,
                            @Start = start,
                            @PageSize = model.PageSize
                        });
                    foreach (var rs in data)
                    {
                        foreach (var reason in reasonReject)
                        {
                            if (rs.RegistServiceID == reason.RegistServiceID)
                            {
                                rs.ReasonsReject = reason.ReasonsReject;
                            }
                        }

                        if (rs.ChangeType.Contains("<b>Thay đổi:</b>"))
                        {
                            rs.ChangeType = rs.ChangeType.Replace("<b>", " ").Replace("</b>", " ");
                            rs.StatusNameChange = rs.ChangeType;
                            rs.StatusNameChangeNum = 5;
                        }
                        else if (rs.ChangeType.Contains("<b>ĐK Chuyển tiền</b>:"))
                        {
                            rs.ChangeType = rs.ChangeType.Replace("<b>", " ").Replace("</b>", " ");
                            rs.StatusNameChange = rs.ChangeType;
                            rs.StatusNameChangeNum = 4;
                        }
                        else if (rs.ChangeType.Contains("<b>ĐK Dịch vụ</b>:"))
                        {
                            rs.ChangeType = rs.ChangeType.Replace("<b>", " ").Replace("</b>", " ");
                            rs.StatusNameChange = rs.ChangeType;
                            rs.StatusNameChangeNum = 3;
                        }

                        var changeInfoList = JsonConvert.DeserializeObject<List<ChangeInfoDTO>>(rs.ContentChange);
                        rs.ContentChangeInfo = new List<ComboField>();
                        if (changeInfoList != null && changeInfoList.Count() > 0)
                        {
                            foreach (var item in changeInfoList)
                            {
                                if (EkycChangeCustInfoConst.NAME_HOTEN.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.FieldLabel = "Họ tên";

                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Họ tên",
                                        Values = item.Values[1]
                                    });

                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_CMND.Equals(item.Name))
                                {
                                    var comboField = new ComboField();

                                    comboField.FieldLabel = "Số CMND/Passport/Giấy Phép kinh doanh";
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Số",
                                        Values = item.Values[1]
                                    });
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Ngày cấp",
                                        Values = item.Values[3]
                                    });
                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_DIA_CHI.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.FieldLabel = "Địa chỉ";
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Địa chỉ",
                                        Values = item.Values[1]
                                    });
                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_NGUOI_DAI_DIEN.Equals(item.Name))
                                {
                                    var comboField = new ComboField();

                                    comboField.FieldLabel = "Người đại diện theo pháp luật";
                                    comboField.ComboValues = new List<ComboValues>();

                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Họ tên",
                                        Values = item.Values[0]
                                    });
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Chức vụ",
                                        Values = item.Values[1]
                                    });
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "CMND/Hộ chiếu",
                                        Values = item.Values[2]
                                    });
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Ngày cấp",
                                        Values = item.Values[3]
                                    });
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Nơi cấp",
                                        Values = item.Values[4]
                                    });
                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_NGUOI_UY_QUYEN.Equals(item.Name))
                                {
                                    var comboField = new ComboField();

                                    comboField.FieldLabel = "Người được ủy quyền";
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Họ tên",
                                        Values = item.Values[0]
                                    });
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Chức vụ",
                                        Values = item.Values[1]
                                    });
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "CMND/Hộ chiếu",
                                        Values = item.Values[2]
                                    });
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Ngày cấp",
                                        Values = item.Values[3]
                                    });
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Nơi cấp",
                                        Values = item.Values[4]
                                    });
                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_01.Equals(item.Name))
                                {
                                    var comboField = new ComboField();

                                    comboField.FieldLabel = "Số điện thoại 01";
                                    comboField.ComboValues = new List<ComboValues>();

                                    if (item.Options[0] == 1)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số điện thoại đăng ký",
                                            Values = item.Values[0]
                                        });
                                    }
                                    else if (item.Options[0] == 2)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số điện thoại hủy",
                                            Values = item.Values[0]
                                        });
                                    }
                                    else if (item.Options[0] == 3)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số điện thoại cũ",
                                            Values = item.Values[0]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số điện thoại mới",
                                            Values = item.Values[1]
                                        });
                                        rs.ContentChangeInfo.Add(comboField);
                                    }
                                }
                                else if (EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_02.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.FieldLabel = "Số điện thoại 02";
                                    comboField.ComboValues = new List<ComboValues>();

                                    if (item.Options[0] == 1)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số điện thoại đăng ký",
                                            Values = item.Values[0]
                                        });
                                    }
                                    else if (item.Options[0] == 2)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số điện thoại hủy",
                                            Values = item.Values[0]
                                        });
                                    }
                                    else if (item.Options[0] == 3)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số điện thoại cũ",
                                            Values = item.Values[0]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số điện thoại mới",
                                            Values = item.Values[1]
                                        });
                                    }

                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_EMAIL.Equals(item.Name))
                                {
                                    var comboField = new ComboField();

                                    comboField.FieldLabel = "Email";
                                    comboField.ComboValues = new List<ComboValues>();
                                    if (item.Options[0] == 1)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Email đăng ký",
                                            Values = item.Values[0]
                                        });
                                    }
                                    else if (item.Options[0] == 2)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Email hủy",
                                            Values = item.Values[0]
                                        });
                                    }
                                    else if (item.Options[0] == 3)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Email cũ",
                                            Values = item.Values[0]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Email mới",
                                            Values = item.Values[1]
                                        });
                                    }

                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_CHUKY.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.FieldLabel = "Thay đổi Chữ ký/ Mẫu dấu";
                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_FILE_CHUKY.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.FieldLabel = "File chữ ký/ Mẫu dấu";

                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_THAY_DOI_KHAC.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.FieldLabel = "Thay đổi khác";
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Thay đổi khác",
                                        Values = item.Values[0]
                                    });

                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_BANK_TRANSFER.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.FieldLabel = "Dịch vụ chuyển tiền ngân hàng";
                                    if (item.Options[0] == 1 || item.Options[0] == 2)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = item.Options[0] == 1 ? "Đăng ký mới" : "Đăng ký bổ sung",
                                            Values = null
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Chủ tài khoản 1",
                                            Values = item.Values[0]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số tài khoản 1",
                                            Values = item.Values[1]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Tên ngân hàng 1",
                                            Values = item.Values[2]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Chi nhánh 1",
                                            Values = item.Values[3]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Tên ngân hàng 1",
                                            Values = item.Values[4]
                                        });
                                        if (!string.IsNullOrEmpty(item.Values[5]))
                                        {
                                            comboField.ComboValues.Add(new ComboValues()
                                            {
                                                Name = "Số tài khoản 2",
                                                Values = item.Values[5]
                                            });
                                            comboField.ComboValues.Add(new ComboValues()
                                            {
                                                Name = "Số tài khoản 2",
                                                Values = item.Values[6]
                                            });
                                            comboField.ComboValues.Add(new ComboValues()
                                            {
                                                Name = "Tên ngân hàng 2",
                                                Values = item.Values[7]
                                            });
                                            comboField.ComboValues.Add(new ComboValues()
                                            {
                                                Name = "Chi nhánh 2",
                                                Values = item.Values[8]
                                            });
                                            comboField.ComboValues.Add(new ComboValues()
                                            {
                                                Name = "Tên ngân hàng 2",
                                                Values = item.Values[9]
                                            });
                                        }
                                    }
                                    else if (item.Options[0] == 3)
                                    {
                                        comboField.FieldLabel = "Hủy đăng ký";
                                        comboField.ComboValues = new List<ComboValues>();
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Chủ tài khoản",
                                            Values = item.Values[0]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số tài khoản",
                                            Values = item.Values[1]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Tên ngân hàng",
                                            Values = item.Values[2]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Chi nhánh",
                                            Values = item.Values[3]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Tên ngân hàng",
                                            Values = item.Values[4]
                                        });
                                    }

                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_ACCOUNT_TRANSFER.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.FieldLabel = "Dịch vụ chuyển tiền TK đối ứng";
                                    if (item.Options[0] == 1)
                                    {
                                        comboField.DetailInfo = "Đăng ký";
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số tài khoản Đối ứng 1",
                                            Values = item.Values[0]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Chủ tài khoản Đối ứng 1",
                                            Values = item.Values[1]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số tài khoản Đối ứng 2",
                                            Values = item.Values[2]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Chủ tài khoản Đối ứng 2",
                                            Values = item.Values[3]
                                        });
                                    }
                                    else if (item.Options[0] == 2)
                                    {
                                        comboField.FieldLabel = "Dịch vụ chuyển tiền TK đối ứng";
                                        comboField.DetailInfo = "Hủy đăng ký";
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Số tài khoản ",
                                            Values = item.Values[0]
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Chủ tài khoản",
                                            Values = item.Values[1]
                                        });
                                    }

                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_GDTT.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.FieldLabel = "Giao dịch trực tuyến";
                                    if (item.Options[0] == 1)
                                    {
                                        comboField.DetailInfo = "Đăng ký";
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Nhận MK và PIN trực tiếp tại quầy",
                                            Values = true
                                        });
                                    }
                                    else if (item.Options[0] == 2)
                                    {
                                        comboField.DetailInfo = "Đăng ký";
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Nhận MK và PIN qua Email",
                                            Values = true
                                        });
                                    }
                                    else if (item.Options[0] == 3)
                                    {
                                        comboField.DetailInfo = "Đăng ký";
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Nhận MK và PIN qua SMS",
                                            Values = true
                                        });
                                    }
                                    else if (item.Options[0] == 4)
                                    {
                                        comboField.DetailInfo = "Hủy đăng ký";
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Values = true
                                        });
                                    }

                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_RESET_PASS.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.FieldLabel = "Reset mật khẩu";
                                    if (item.Options[0] == 1)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Nhận mật khẩu trực tiếp tại quầy",
                                            Values = true
                                        });
                                    }
                                    else if (item.Options[0] == 2)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Nhận mật khẩu qua Email",
                                            Values = true
                                        });
                                    }
                                    else if (item.Options[0] == 3)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Nhận mật khẩu qua SMS",
                                            Values = true
                                        });
                                    }

                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_RESET_PIN.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.FieldLabel = "Reset PIN";
                                    comboField.ComboValues = new List<ComboValues>();
                                    if (item.Options[0] == 1)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Nhận PIN trực tiếp tại quầy",
                                            Values = true
                                        });
                                    }
                                    else if (item.Options[0] == 2)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Nhận PIN qua Email",
                                            Values = true
                                        });
                                    }
                                    else if (item.Options[0] == 3)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Nhận PIN qua SMS",
                                            Values = true
                                        });
                                    }

                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_UTTD.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.FieldLabel = "Dịch vụ UTTD";
                                    if (item.Options[0] == 1)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Đăng ký",
                                            Values = true
                                        });
                                    }
                                    else if (item.Options[0] == 2)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Hủy đăng ký",
                                            Values = true
                                        });
                                    }
                                    else if (item.Options[0] == 3)
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Chuyển đổi SĐT SMS",
                                            Values = true
                                        });
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "SĐT Chuyển đổi",
                                            Values = item.Values[0]
                                        });
                                    }

                                    rs.ContentChangeInfo.Add(comboField);
                                }
                                else if (EkycChangeCustInfoConst.NAME_SAO_KE.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.FieldLabel = "In sao kê";
                                    if (item.Values[0] == "Y")
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Sao kê số dư tiền, CK",
                                            Values = true
                                        });
                                    }

                                    if (item.Values[1] == "Y")
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Sao kê giao dịch tiền",
                                            Values = true
                                        });
                                    }

                                    if (item.Values[2] == "Y")
                                    {
                                        comboField.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Sao kê giao dịch CK",
                                            Values = true
                                        });
                                    }

                                    rs.ContentChangeInfo.Add(comboField);
                                    var comboField2 = new ComboField();
                                    comboField2.ComboValues = new List<ComboValues>();
                                    comboField2.FieldLabel = "Hình thức nhận sao kê";
                                    if (item.Options[0] == 1)
                                    {
                                        comboField2.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Trực tiếp tại quầy",
                                            Values = true
                                        });
                                    }
                                    else
                                    {
                                        comboField2.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Qua Email",
                                            Values = true
                                        });
                                    }

                                    rs.ContentChangeInfo.Add(comboField2);

                                    var comboField3 = new ComboField();
                                    comboField3.ComboValues = new List<ComboValues>();
                                    comboField3.FieldLabel = "Hình thức nộp phí sao kê";
                                    if (item.Options[1] == 1)
                                    {
                                        comboField3.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Nộp trực tiếp/CK ngân hàng",
                                            Values = true
                                        });
                                    }
                                    else
                                    {
                                        comboField3.ComboValues.Add(new ComboValues()
                                        {
                                            Name = "Thu từ TK chứng khoán",
                                            Values = true
                                        });
                                    }

                                    rs.ContentChangeInfo.Add(comboField3);
                                }
                                else if (EkycChangeCustInfoConst.NAME_DICH_VU_KHAC.Equals(item.Name))
                                {
                                    var comboField = new ComboField();
                                    comboField.FieldLabel = "Dịch vụ khác";
                                    comboField.ComboValues = new List<ComboValues>();
                                    comboField.ComboValues.Add(new ComboValues()
                                    {
                                        Name = "Dịch vụ khác",
                                        Values = item.Values[0]
                                    });
                                }
                            }
                        }

                        result.Add(rs);
                    }

                    return result;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetChangeInfoHist" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> CreateAccountMargin(CreateAccountMarginRequest model)
        {
            try
            {
                var description = "";
                var additionService = "";
                if (model.MarginAccount)
                {
                    additionService += AdditionContractConst.Margin.GetHashCode();
                    description += "HĐ EKYC-MOBILE Margin, ";
                }

                description = description.Trim();
                if (description.EndsWith(","))
                    description = description.Substring(0, description.Length - 1);

                var accountInfo = await WithCommonDBConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<OpenAccountDocumentMarginExtend>(
                        _commandText.GetAccountOpenMargin, new
                        {
                            @CustCode = model.UserId
                        }
                    ));
                var emailSql = "  SELECT CONTACTEMAIL Email FROM CUSTOMER WHERE CUSTOMERID =  @CustCode";
                var emailInfo = await WithInnoConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                    emailSql, new
                    {
                        CustCode = model.UserId
                    }));
                
                
                /*var sqlInfo = "select  branch_name BranchName, sale_id SaleID from TVSI_DANG_KY_MO_TAI_KHOAN where ma_khach_hang = @CustCode and email is not null";
                var creator = await WithCommonDBConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<getUserMargin>(sqlInfo, new
                    {
                        @CustCode = model.UserId
                    }));*/

                var sqlInfo = "  SELECT a.ma_quan_ly SaleID,b.ten_chi_nhanh BranchName FROM TVSI_THONG_TIN_KHACH_HANG A INNER JOIN TVSI_DANH_MUC_CHI_NHANH B ON A.ma_chi_nhanh = B.ma_chi_nhanh WHERE A.ma_khach_hang = @CustCode";
                var dataInfo = await WithEmsDBConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<getUserMargin>(sqlInfo, new
                    {
                        CustCode = model.UserId
                    }));
                
                
                if (accountInfo != null && dataInfo != null)
                {
                    return await WithCommonDBConnection(async conn =>
                    {
                        var parameters = new DynamicParameters();
                        parameters.Add("@CustCode", accountInfo.CustCode);
                        parameters.Add("@CustCode044c", "044C" + accountInfo.CustCode);
                        parameters.Add("@TypeProfile", DocumentTypeConst.ADDITION_DOCUMENT.GetHashCode());
                        parameters.Add("@TypeAccount", CustTypeConst.INDIVIDUAL_DOMESTIC_INVEST.GetHashCode());
                        parameters.Add("@FullName", accountInfo.FullName);
                        parameters.Add("@BirthDay", accountInfo.Birthday);
                        parameters.Add("@Gender", 0);
                        parameters.Add("@CardId", accountInfo.CardID);
                        parameters.Add("@IssueDate", accountInfo.IssueDate);
                        parameters.Add("@CardIssue", accountInfo.CardIssue);
                        parameters.Add("@Address", accountInfo.Address);
                        parameters.Add("@MainContract", false);
                        parameters.Add("@ContractExtend", additionService);
                        parameters.Add("@StatusAccount", OpenAccountStatus.KICH_HOAT_102);
                        parameters.Add("@StatusProfile", DocumentStatusConst.KO_HO_SO_102);
                        parameters.Add("@StatusService", ServiceStatusConst.KO_DANG_KY.GetHashCode());
                        parameters.Add("@StatusServiceBank", ServiceStatusConst.KO_DANG_KY.GetHashCode());
                        parameters.Add("@StatusServiceBank02", ServiceStatusConst.KO_DANG_KY.GetHashCode());
                        parameters.Add("@StatusServiceBankRep", ServiceStatusConst.KO_DANG_KY.GetHashCode());
                        parameters.Add("@StatusServiceBankRep2", ServiceStatusConst.KO_DANG_KY.GetHashCode());
                        parameters.Add("@StatusServiceInfo", ServiceStatusConst.KO_DANG_KY.GetHashCode());
                        parameters.Add("@Description", description);
                        parameters.Add("@CreateBy", accountInfo.CustCode);
                        parameters.Add("@BranchId", accountInfo.BranchId);
                        parameters.Add("@Creator", model.UserId);
                        parameters.Add("@SaleID", dataInfo.SaleID);
                        parameters.Add("@BranchName", dataInfo.BranchName);
                        parameters.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                        await conn.ExecuteAsync("TVSI_sDANG_KY_BO_SUNG_TAI_KHOAN_MARGIN_IWORK",
                            parameters, commandType: CommandType.StoredProcedure, commandTimeout: 60
                        );
                        var id = parameters.Get<int>("@ID");
                        if (!string.IsNullOrEmpty(emailInfo))
                        {
                            EkycUtils.SendEmailContactMargin(accountInfo.CardID, accountInfo.CustCode,
                                emailInfo, accountInfo.FullName);   
                        }

                        return id > 0;
                    });
                }
                return false;
            }
            catch (Exception ex)
            {
                log.Error("CreateAccountMargin " + ex.Message);
                log.Error("CreateAccountMargin " + ex.InnerException);
                throw;
            }
        }

        public async Task<CheckAccountMarginResult> CheckAccountMargin(string userId)
        {
            try
            {
                var marginSix = await WithEmsDBConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                    _commandText.CheckAccountSixMargin,
                    new
                    {
                        @CustCode = userId
                    })
                );
                if (string.IsNullOrEmpty(marginSix))
                {
                    var dataRet = await WithInnoConnection(async conn =>
                        await conn.QueryFirstOrDefaultAsync<CheckAccountMarginResult>(
                            _commandText.GetAccountRegisterMargin,
                            new
                            {
                                @CustCode = userId
                            })
                    );

                    dataRet.Code044C = EkycCommondConst.CODE044C;
                    return dataRet;
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("CheckAccountMargin" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<bool> CancellChangeInfoCust(CancellChangeInfoCustRequest model)
        {
            try
            {
                return await WithCommonDBConnection(async conn =>
                {
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            var parameter = new DynamicParameters();
                            parameter.Add("@Id", model.RegistServiceID);
                            parameter.Add("@CustId", model.UserId);
                            var result = await conn.ExecuteAsync("[dbo].[TVSI_sCANCELL_THAY_DOI_THONG_TIN_EKYC]",
                                parameter, commandType: CommandType.StoredProcedure, commandTimeout: 60,
                                transaction: trans);
                            trans.Commit();
                            return result > 0;
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            log.Error("CancellChangeInfoCust:WithCommonDBConnection" + ex.Message);
                            log.Error(ex.InnerException);
                            throw;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                log.Error("CancellChangeInfoCust" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Kiểm tra tài khoản có phải bổ sung thông tin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> CheckAddProfile(BaseRequest model)
        {
            try
            {
                return await WithCommonDBConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                        _commandText.CheckAddProfile, new
                        {
                            @CustId = model.UserId
                        }) != null
                );
            }
            catch (Exception ex)
            {
                log.Error("CheckAddProfile" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<GetInfoUserIdResult> GetInfoSale(GetInfoUserIdRequest model)
        {
            try
            {
                var sql =
                    " SELECT ho_va_ten FullName,dien_thoai_lien_he Phone, dia_chi_email  Email from [dbo].[TVSI_NHAN_SU] where ma_nhan_vien_quan_ly = @SaleID and trang_thai = 1";
                return await WithEmsDBConnection(async conn => await
                    conn.QueryFirstOrDefaultAsync<GetInfoUserIdResult>(sql, new
                    {
                        @SaleID = model.SaleID
                    }));
            }
            catch (Exception ex)
            {
                log.Error("GetInfoUserId " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<GetImageChangeInfoResult> GetImageChangeInfoCust(GetImageChangeInfoCustRequest model)
        {
            try
            {
                var folderEkycReg = ConfigurationManager.AppSettings["FOLDER_EKYC_CHANGE_INFO"];
                var folder = folderEkycReg + model.CardID + "/";

                if (Directory.Exists(folder))
                {
                    var dataRet = new GetImageChangeInfoResult();
                    if (File.Exists(folder + "frontid.jpg"))
                    {
                        var imageFront = File.ReadAllBytes(folder + "frontid.jpg");
                        var base64ImageFront = Convert.ToBase64String(imageFront);
                        dataRet.NewImageFront = EkycCommondConst.IMAGE_BASE64 + base64ImageFront;
                    }

                    if (File.Exists(folder + "backid.jpg"))
                    {
                        var imageBack = File.ReadAllBytes(folder + "backid.jpg");
                        var base64ImageBack = Convert.ToBase64String(imageBack);
                        dataRet.NewImageBack = EkycCommondConst.IMAGE_BASE64 + base64ImageBack;
                    }

                    if (File.Exists(folder + "frontid_old.jpg"))
                    {
                        var frontOldImage = File.ReadAllBytes(folder + "frontid_old.jpg");
                        var base64ImageFrontOld = Convert.ToBase64String(frontOldImage);
                        dataRet.OldImageFront = EkycCommondConst.IMAGE_BASE64 + base64ImageFrontOld;
                    }

                    if (File.Exists(folder + "backid_old.jpg"))
                    {
                        var backOldImage = File.ReadAllBytes(folder + "backid_old.jpg");
                        var base64ImageBackOld = Convert.ToBase64String(backOldImage);
                        dataRet.OldImageBack = EkycCommondConst.IMAGE_BASE64 + base64ImageBackOld;
                    }

                    if (File.Exists(folder + "identification_reference.jpg"))
                    {
                        var backOldImage = File.ReadAllBytes(folder + "identification_reference.jpg");
                        var base64ImageBackOld = Convert.ToBase64String(backOldImage);
                        dataRet.ImageOther = EkycCommondConst.IMAGE_BASE64 + base64ImageBackOld;
                    }

                    return dataRet;
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("ImageChangeInfoCust  " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<GetImageSignatureResult> GetImageAddProfile(GetImageChangeInfoCustRequest model)
        {
            try
            {
                var folderEkycReg = ConfigurationManager.AppSettings["FOLDER_EKYC_REGISTER"];
                var folder = folderEkycReg + model.CardID + "/";
                if (Directory.Exists(folder))
                {
                    if (File.Exists(folder + "signature.jpg"))
                    {
                        var dataRet = new GetImageSignatureResult();
                        var imageFront = File.ReadAllBytes(folder + "signature.jpg");
                        var base64ImageSignature = Convert.ToBase64String(imageFront);
                        dataRet.DataSignature = EkycCommondConst.IMAGE_BASE64 + base64ImageSignature;
                        return dataRet;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetImageAddProfile  " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<StringeeResult> GetAccessTokenStringee(StringeeRequest model)
        {
            try
            {
                var accountValid = await WithInnoConnection(async conn =>
                {
                    var sql = "SELECT CUSTOMERID FROM CUSTOMER WHERE CUSTOMERID = @userID";
                    var userID = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        userID = model.userId
                    });
                    if (!string.IsNullOrEmpty(userID))
                    {
                        var sqlPhone =
                            "SELECT CONTACTPHONE ContactPhone,CELLPHONE CellPhone,PHONE Phone FROM CUSTOMER WHERE CUSTOMERID = @CustID";
                        var dataPhone = await conn.QueryFirstOrDefaultAsync<PhoneInnoInfoResult>(sqlPhone, new
                        {
                            @CustID = model.userId
                        });
                        if (model.phone.Equals(dataPhone.Phone))
                            return true;
                        if (model.phone.Equals(dataPhone.CellPhone))
                            return true;
                        if (model.phone.Equals(dataPhone.ContactPhone))
                            return true;
                    }

                    return false;
                });

                if (accountValid)
                {
                    var stringeeToken = string.Empty;
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var keySecret = ConfigurationManager.AppSettings["KeySecret"] + "";
                    var sID = ConfigurationManager.AppSettings["SID"] + "";

                    var key = Encoding.UTF8.GetBytes(keySecret);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Expires = DateTime.UtcNow.AddDays(7),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                            SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateJwtSecurityToken(tokenDescriptor);
                    token.Header.Remove("typ");
                    token.Header.Remove("alg");
                    token.Header.Remove("cty");
                    token.Header.Add("typ", "JWT");
                    token.Header.Add("alg", "HS256");
                    token.Header.Add("cty", "stringee-api;v=1");

                    token.Payload.Add("jti", sID + "-" + Guid.NewGuid().ToString());
                    token.Payload.Add("iss", sID);
                    token.Payload.Add("rest_api", true);
                    token.Payload.Add("userId", model.userId);

                    stringeeToken = tokenHandler.WriteToken(token);
                    return new StringeeResult()
                    {
                        accessToken = stringeeToken,
                        userId = model.userId
                    };
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetAccessTokenStringee  " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<ProvinceResult>> GetProvince()
        {
            try
            {
                return await WithCRMDBConnection(async conn =>
                {
                    var sql = "SELECT * FROM Province WHERE status = 1";
                    return await conn.QueryAsync<ProvinceResult>(sql);
                });
            }
            catch (Exception ex)
            {
                log.Error("GetProvince  " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<DistrictResult>> GetDistrict(DistrictRequest model)
        {
            try
            {
                return await WithCRMDBConnection(async conn =>
                {
                    var sql = "SELECT * FROM District  WHERE status = 1 And ProvinceCode = @ProvinceCode";
                    return await conn.QueryAsync<DistrictResult>(sql, new
                    {
                        ProvinceCode = model.ProvinceCode
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error("GetDistrict  " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<WardResult>> GetWardList(WardRequest model)
        {
            try
            {
                return await WithCRMDBConnection(async conn =>
                {
                    var sql = "SELECT * FROM Ward  WHERE status = 1 AND DistrictCode = @DistrictCode";
                    return await conn.QueryAsync<WardResult>(sql, new
                    {
                        DistrictCode = model.DistrictCode
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error("GetWardList  " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public Task<bool> ChangeSmsInfo()
        {
            throw new NotImplementedException();
        }

        public async Task<GetImageByCardIdResult> GetImageByCardID(GetImageByCardIdRequest model)
        {
            try
            {
                var folderEkycReg = ConfigurationManager.AppSettings["FOLDER_EKYC_REGISTER"];
                var folder = folderEkycReg + model.CardId + "/";
                if (Directory.Exists(folder))
                {
                    var dataRet = new GetImageByCardIdResult();

                    if (File.Exists(folder + "frontid.jpg"))
                    {
                        var imageFront = File.ReadAllBytes(folder + "frontid.jpg");
                        var base64ImageFront = Convert.ToBase64String(imageFront);
                        dataRet.Front_Image = EkycCommondConst.IMAGE_BASE64 + base64ImageFront;
                    }

                    if (File.Exists(folder + "backid.jpg"))
                    {
                        var imageBack = File.ReadAllBytes(folder + "backid.jpg");
                        var base64ImageBack = Convert.ToBase64String(imageBack);
                        dataRet.Back_Image = EkycCommondConst.IMAGE_BASE64 + base64ImageBack;
                    }

                    if (File.Exists(folder + "face.jpg"))
                    {
                        var face = File.ReadAllBytes(folder + "face.jpg");
                        var base64ImageFace = Convert.ToBase64String(face);
                        dataRet.Face_Image = EkycCommondConst.IMAGE_BASE64 + base64ImageFace;
                    }

                    if (File.Exists(folder + "signature.jpg"))
                    {
                        var signature0 = File.ReadAllBytes(folder + "signature.jpg");
                        var base64ImageSignature0 = Convert.ToBase64String(signature0);
                        dataRet.Signature_Image_0 = EkycCommondConst.IMAGE_BASE64 + base64ImageSignature0;
                    }

                    /*if (File.Exists(folder + "signature_1.jpg"))
                    {
                        var signature1 = File.ReadAllBytes(folder + "signature_1.jpg");
                        var base64ImageSignature1 = Convert.ToBase64String(signature1);
                        dataRet.Signature_Image_1 = EkycCommondConst.IMAGE_BASE64 + base64ImageSignature1;
                    }

                    if (File.Exists(folder + "signature_0.mp4"))
                    {
                        var videoSignature0 = File.ReadAllBytes(folder + "signature_0.mp4");
                        var base64VideoSignature0 = Convert.ToBase64String(videoSignature0);
                        dataRet.Signature_Video_0 = EkycCommondConst.VIDEO_BASE64 + base64VideoSignature0;
                    }

                    if (File.Exists(folder + "signature_1.mp4"))
                    {
                        var videoSignature1 = File.ReadAllBytes(folder + "signature_1.mp4");
                        var base64VideoSignature1 = Convert.ToBase64String(videoSignature1);
                        dataRet.Signature_Video_1 = EkycCommondConst.VIDEO_BASE64 + base64VideoSignature1;
                    }*/

                    return dataRet;
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetImageByCardID" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /*public async Task<bool> UpdateCardIdeKYC(CardIdRequest model)
        {
            try
            {
                var data = await WithCRMDBConnection(async conn =>
                {
                    var param = new DynamicParameters();
                    param.Add("@CardId",model.CardId);
                   return await conn.ExecuteAsync(_commandText.TVSI_SUpdateCardId, param,
                        commandType: CommandType.StoredProcedure);
                });
                return data == -1;
            }
            catch (Exception ex)
            {
                log.Error("UpdateCardIdeKYC" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }*/


        public async Task<bool> SUpdateStatusVideoCall(UpdateStatusVideoCallRequest model)
        {
            try
            {
                var custInfo = await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<CustInfo>(_commandText.GetCustInfo, new
                    {
                        @CustCode = model.UserID
                    });
                    return data;
                });
                if (custInfo != null)
                {
                    await WithCommonDBConnection(async conn =>
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@ma_khach_hang", custInfo.UserId);
                        queryParameters.Add("@so_tai_khoan", custInfo.AccountNo);
                        queryParameters.Add("@ten_khach_hang", custInfo.CustName);
                        queryParameters.Add("@tong_thoi_gian_goi", model.TotalCallTime);
                        queryParameters.Add("@trang_thai_goi", model.StatusVideoCall);
                        queryParameters.Add("@thoi_gian_goi", model.CallDateTime);
                        queryParameters.Add("@lich_su_video_call_id", dbType: DbType.Int32,
                            direction: ParameterDirection.Output);
                        await conn.QueryAsync<string>(_commandText.TVSI_SUpdatLichSuVideoCall, queryParameters,
                            commandType: CommandType.StoredProcedure);
                        custInfo.ChangeInfoID = queryParameters.Get<int>("@lich_su_video_call_id");
                    });
                }

                return custInfo.ChangeInfoID > 0;
            }
            catch (Exception ex)
            {
                log.Error("SUpdateStatusVideoCall" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> sDatLichVideoCallEkyc(VideoCallScheduleRequest model)
        {
            try
            {
                var custInfo = await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<CustInfo>(_commandText.GetCustInfo, new
                    {
                        @CustCode = model.UserID
                    });
                    return data;
                });
                if (custInfo != null)
                {
                    await WithCommonDBConnection(async conn =>
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@ma_khach_hang", custInfo.UserId);
                        queryParameters.Add("@so_tai_khoan", custInfo.AccountNo);
                        queryParameters.Add("@ten_khach_hang", custInfo.CustName);
                        queryParameters.Add("@thoi_gian_dat_lich_goi", model.ScheduleTime);
                        queryParameters.Add("@trang_thai_ss_duyet", 0);
                        queryParameters.Add("@noi_dung", model.Description);
                        queryParameters.Add("@dat_lich_video_call_id", dbType: DbType.Int32,
                            direction: ParameterDirection.Output);
                        await conn.QueryAsync<string>(_commandText.TVSI_SDatLichVideoCall, queryParameters,
                            commandType: CommandType.StoredProcedure);
                        custInfo.ChangeInfoID = queryParameters.Get<int>("@dat_lich_video_call_id");
                    });
                }

                return custInfo.ChangeInfoID > 0;
            }
            catch (Exception ex)
            {
                log.Error("sDatLichVideoCallEkyc" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<VideoCallHistInfoRequest>> GetVideoCallHist(VideoCallHistRequest model)
        {
            try
            {
                var toDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy",
                    System.Globalization.CultureInfo.InvariantCulture);
                var sToDate = toDate.ToString("yyyy-MM-dd");
                var fromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy",
                    System.Globalization.CultureInfo.InvariantCulture);
                var sFromDate = fromDate.ToString("yyyy-MM-dd");
                return await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryAsync<VideoCallHistInfoRequest>(_commandText.GetVideoCallHist, new
                    {
                        @CustCode = model.UserID,
                        @FromDate = sFromDate,
                        @ToDate = sToDate
                    });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetVideoCallHist" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<InternalAccountDetailInfoResult> GetInternalAccountDetail(InternalAccountDetailRequest model)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<InternalAccountDetailInfoResult>(
                        _commandText.GetInternalAccountDetail, new
                        {
                            CustCode = model.UserID,
                            InternalAccountNo = model.InternalAccountNo,
                            AccountNo = model.AccountNo
                        });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetInternalAccountDetail" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> RegisterBankAccount(RegisterBankRequest model)
        {
            try
            {
                /*return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetBankInfo,new
                    {
                        @CustCode = model.BankName,
                    });
                    return data;
                });*/
                return true;
            }
            catch (Exception ex)
            {
                log.Error("RegisterBankAccount" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<bool> SInsertChangeInfoCust(string maKhachHang, string soTaiKhoan, string tenKhachHang,
            string thongTinDaDangKy, string thongTinThayDoi, string noiDungThayDoi, string moTa,
            int trangThai,
            string nguoiTao, DateTime ngayTao, int isSbaInno, string branchId, long newLogId)
        {
            try
            {
                var dataCommonDb = await WithCommonDBConnection(async conn =>
                {
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            var queryParam = new DynamicParameters();
                            queryParam.Add("@ma_khach_hang", maKhachHang);
                            queryParam.Add("@so_tai_khoan", soTaiKhoan);
                            queryParam.Add("@thong_tin_da_dang_ky", thongTinDaDangKy);
                            queryParam.Add("@ten_khach_hang", tenKhachHang);
                            queryParam.Add("@thong_tin_thay_doi", thongTinThayDoi);
                            queryParam.Add("@noi_dung_thay_doi", noiDungThayDoi);
                            queryParam.Add("@mo_ta", moTa);
                            queryParam.Add("@trang_thai", trangThai);
                            queryParam.Add("@nguoi_tao", nguoiTao);
                            queryParam.Add("@ngay_tao", ngayTao);
                            queryParam.Add("@isSbaInno", isSbaInno);
                            queryParam.Add("@branchId", branchId);
                            queryParam.Add("@typeAcc", 1);
                            queryParam.Add("@newLogID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                            await conn.ExecuteAsync(_commandText.TVSI_sTHAY_DOI_THONG_TIN_KHACH_HANG_INSERT, queryParam,
                                commandType: CommandType.StoredProcedure, commandTimeout: 60, transaction: trans);
                            trans.Commit();
                            var query = queryParam.Get<Int32>("@newLogID");
                            return query;
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            log.Error("SInsertChangeInfoCust" + ex.Message);
                            log.Error(ex.InnerException);
                            throw;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                });
                return dataCommonDb > 0;
            }
            catch (Exception ex)
            {
                log.Error("SInsertChangeInfoCust" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> InsertPathImageCardCust(string custId, string newImageFront, string newImageBack,
            string legalImage, string oldImageFront,
            string oldImageBack, int type)
        {
            try
            {
                var dataSave = await WithCommonDBConnection(async conn =>
                {
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            if (type == 1)
                            {
                                var data = await conn.ExecuteAsync(_commandText.TVSI_SInsertEkycFilesType1, new
                                {
                                    @ma_khach_hang = custId,
                                    @anh_mat_truoc_moi = newImageFront,
                                    @anh_mat_sau_moi = newImageBack,
                                    @anh_phap_ly_giay_to_cu = legalImage,
                                    @type = type
                                }, commandType: CommandType.StoredProcedure, transaction: trans);
                                trans.Commit();
                                return data;
                            }
                            else
                            {
                                var data = await conn.ExecuteAsync(_commandText.TVSI_SInsertEkycFilesType2, new
                                {
                                    @ma_khach_hang = custId,
                                    @anh_mat_truoc_moi = newImageFront,
                                    @anh_mat_sau_moi = newImageBack,
                                    @anh_mat_truoc_cu = oldImageFront,
                                    @anh_mat_sau_cu = oldImageBack,
                                    @type = type
                                }, commandType: CommandType.StoredProcedure, transaction: trans);
                                trans.Commit();
                                return data;
                            }
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            log.Error("InsertPathImageCardCust" + ex.Message);
                            log.Error(ex.InnerException);
                            throw;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                });
                return dataSave != 0;
            }
            catch (Exception ex)
            {
                log.Error("InsertPathImageCardCust" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> ConfirmEkycMobile(string codeM)
        {
            try
            {
                log.Info("ConfirmEkycMobile b1" + codeM);
                var dataCommonDb = await WithCRMDBConnection(async conn =>
                {
                    var queryParam = new DynamicParameters();
                    queryParam.Add("@ConfirmCode", codeM);
                    queryParam.Add("@ho_so_mo_tk_id", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    queryParam.Add("@crm_id_cf", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    await conn.QueryAsync<string>(_commandText.TVSI_SConfirmEkycMobile, queryParam,
                        commandType: CommandType.StoredProcedure, commandTimeout: 60);
                    return queryParam.Get<int>("@ho_so_mo_tk_id");
                });
                log.Info("ConfirmEkycMobile b2" + dataCommonDb);
                return dataCommonDb > 0;
            }
            catch (Exception ex)
            {
                log.Error("ConfirmEkycMobile service - " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<bool> DeleteBankAccount(DeleteBankRequest model)
        {
            try
            {
                /*return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetBankInfo,new
                    {
                        @CustCode = model.BankName,
                    });
                    return data;
                });*/
                return true;
            }
            catch (Exception ex)
            {
                log.Error("RegisterBankAccount" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<UserInfoResult> GetInfoCustomer(GetUserInfoRequest model)
        {
            try
            {
                var retData = await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<UserInfoResult>(_commandText.GetUserInfo, new
                    {
                        @ma_khach_hang = model.UserID
                    });
                    return data;
                });
                var sqlPhone =
                    "SELECT CONTACTPHONE ContactPhone,CELLPHONE CellPhone,PHONE Phone FROM CUSTOMER WHERE CUSTOMERID = @CustID";

                await WithInnoConnection(async conn =>
                {
                    var dataPhone = await conn.QueryFirstOrDefaultAsync<PhoneInnoInfoResult>(sqlPhone, new
                    {
                        @CustID = model.UserID
                    });
                    if (!string.IsNullOrEmpty(dataPhone.ContactPhone))
                        retData.Mobile = dataPhone.ContactPhone;
                    else if (string.IsNullOrEmpty(dataPhone.ContactPhone))
                    {
                        if (!string.IsNullOrEmpty(dataPhone.CellPhone))
                            retData.Mobile = dataPhone.CellPhone;
                        if (string.IsNullOrEmpty(dataPhone.CellPhone))
                        {
                            if (!string.IsNullOrEmpty(dataPhone.Phone))
                            {
                                retData.Mobile = dataPhone.Phone;
                            }
                        }
                    }
                });

                var sqlInno = "SELECT T.SEX FROM CUSTOMER T where CUSTOMERID = @CustID";
                var genderInfo = await WithInnoConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                    sqlInno,
                    new
                    {
                        @CustID = model.UserID
                    }));
                if (genderInfo != null)
                    retData.Gender = genderInfo;

                if (retData != null)
                {
                    var sourceData = await WithCommonDBConnection(async conn => await conn.QueryFirstOrDefaultAsync<int>
                    (_commandText.GetSourceData, new
                    {
                        @ma_khach_hang = model.UserID
                    }));
                    if (sourceData == 6)
                    {
                        var retDataExtend = await WithCommonDBConnection(async conn =>
                        {
                            var data = await conn.QueryFirstOrDefaultAsync<ExtendDb>(_commandText.GetUserInfoExtend, new
                            {
                                @ma_khach_hang = model.UserID,
                            });
                            return data;
                        });
                        if (retDataExtend != null)
                        {
                            var dataFolderConvert = JsonConvert.DeserializeObject<EkycMorong>(retDataExtend.Extend);
                            retData.FolderPath = dataFolderConvert.FolderPath;
                        }
                    }
                }


                return retData;
            }
            catch (Exception ex)
            {
                log.Error("GetInfoCustomer" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<EkycCheckMobileCust>> ValidationInputPhoneNum(string custId, string oldPhone,
            string newPhone, int statusPhone, int options)
        {
            try
            {
                var retList = new List<EkycCheckMobileCust>();
                /*if (newphone.Contains(" "))
                {
                    retList.Add( new EkycCheckMobileCust
                    {
                        issueMessage = "Số điện thoại không được chứa khoảng trống"
                    });
                }

                long n;
                if (!long.TryParse(newphone, out n))
                {
                    retList.Add( new EkycCheckMobileCust()
                    {
                        issueMessage = "Số điện thoại chỉ được nhập số"
                    });
                }

                switch (Options)
                {
                    case 1:
                         return await RegisterPhoneNumCust(newphone, StatusPhone, custid);
                    case 2:
                         return await CancellPhoneCust(Oldphone, StatusPhone, custid);
                    case 3:
                         return await ChangePhoneCust(newphone,Oldphone,StatusPhone,custid);
                }*/
                return retList;
            }
            catch (Exception ex)
            {
                log.Error("ValidationInputPhoneNum" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<EkycRegisterErrors>> ValidationFormRegisterUser(EkycSaveUserInfoRequest model)
        {
            try
            {
                var errors = new List<EkycRegisterErrors>();
                if (!string.IsNullOrEmpty(model.CardID))
                {
                    if (!await CardIDExsited(model.CardID))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK904_CARDID_EXISTED,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK904
                        });
                    }
                }

                if (!string.IsNullOrEmpty(model.Birthday))
                {
                    if (!EkycUtils.IsValidDateFormat(model.Birthday))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_908_BIRTHDAY_FORMAT_ERROR,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK908
                        });
                    }

                    if (!EkycUtils.IsAge18(model.Birthday))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_907_UNDER_AGE_18,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK907
                        });
                    }
                }

                if (!string.IsNullOrEmpty(model.IssueDate))
                {
                    if (!EkycUtils.IsValidDateFormat(model.IssueDate))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_909_ISSUEDATE_FORMAT_ERROR,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK909
                        });
                    }
                    else
                    {
                        if (!EkycUtils.IsValidCardId15Year(model.IssueDate))
                        {
                            errors.Add(new EkycRegisterErrors()
                            {
                                RetErrorCode = EkycRetCodeConst.EK_911_EXPIRED_CARDID,
                                RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK911
                            });
                        }
                    }
                }

                if (!string.IsNullOrEmpty(model.Mobile))
                {
                    if (model.Mobile.Length < 9)
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_912_IS_LENGTH_MOBILE,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK912
                        });
                    }
                }

                if (!string.IsNullOrEmpty(model.Email))
                {
                    if (!EkycUtils.IsValidEmail(model.Email))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_913_EMAIL_FORMAT_ERROR,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK913
                        });
                    }
                }

                if (!string.IsNullOrEmpty(model.Address_02))
                {
                    if (model.Address_02.Length < 14)
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_914_ADDRESS_LENGTH14,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK914
                        });
                    }
                }

                if (!string.IsNullOrEmpty(model.SaleID))
                {
                    var saleid = await GetSaleID(model.SaleID.Substring(0, 4));
                    if (string.IsNullOrEmpty(saleid))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK906_SALEID_NOT_EXISTED,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK906
                        });
                    }
                }

                if (model.BankAccountList != null)
                {
                    if (model.BankAccountList.Count > 0)
                    {
                        foreach (var bank in model.BankAccountList)
                        {
                            if (!EkycUtils.IsvalidBank1(bank.BankAccount,
                                bank.BankNo, bank.BranchNo
                                , bank.BankAccountName, bank.CityNo))
                            {
                                errors.Add(new EkycRegisterErrors()
                                {
                                    RetErrorCode = EkycRetCodeConst.EK_915_BANK_ONE_ISVALID_REQUIRED,
                                    RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK915
                                });
                            }
                        }
                    }
                    else
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_915_BANK_ONE_ISVALID_REQUIRED,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK915
                        });
                    }
                }

                return errors;
            }
            catch (Exception ex)
            {
                log.Error("ValidationFormRegisterUser" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<IEnumerable<EkycRegisterErrors>> ValidationFormRegisterUser_v2(EkycSaveUserInfoRequest_v2 model)
        {
            try
            {
                var errors = new List<EkycRegisterErrors>();
                if (!string.IsNullOrEmpty(model.CardID))
                {
                    if (!await CardIDExsited(model.CardID))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK904_CARDID_EXISTED,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK904
                        });
                    }
                }

                if (!string.IsNullOrEmpty(model.Birthday))
                {
                    if (!EkycUtils.IsValidDateFormat(model.Birthday))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_908_BIRTHDAY_FORMAT_ERROR,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK908
                        });
                    }

                    if (!EkycUtils.IsAge18(model.Birthday))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_907_UNDER_AGE_18,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK907
                        });
                    }
                }

                if (!string.IsNullOrEmpty(model.IssueDate))
                {
                    if (!EkycUtils.IsValidDateFormat(model.IssueDate))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_909_ISSUEDATE_FORMAT_ERROR,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK909
                        });
                    }
                    else
                    {
                        if (!EkycUtils.IsValidCardId15Year(model.IssueDate))
                        {
                            errors.Add(new EkycRegisterErrors()
                            {
                                RetErrorCode = EkycRetCodeConst.EK_911_EXPIRED_CARDID,
                                RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK911
                            });
                        }
                    }
                }
                if (!string.IsNullOrEmpty(model.Mobile))
                {
                    if (model.Mobile.Length < 9)
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_912_IS_LENGTH_MOBILE,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK912
                        });
                    }
                }
                if (!string.IsNullOrEmpty(model.Email))
                {
                    if (!EkycUtils.IsValidEmail(model.Email))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_913_EMAIL_FORMAT_ERROR,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK913
                        });
                    }
                }

                if (!string.IsNullOrEmpty(model.Address_02))
                {
                    if (model.Address_02.Length < 14)
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_914_ADDRESS_LENGTH14,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK914
                        });
                    }
                }
                if (!string.IsNullOrEmpty(model.SaleID))
                {
                    var saleid = await GetSaleID(model.SaleID.Substring(0, 4));
                    if (string.IsNullOrEmpty(saleid))
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK906_SALEID_NOT_EXISTED,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK906
                        });
                    }
                }
                if (model.BankAccountList != null)
                {
                    if (model.BankAccountList.Count > 0)
                    {
                        foreach (var bank in model.BankAccountList)
                        {
                            if (!EkycUtils.IsvalidBank1(bank.BankAccount,
                                bank.BankNo, bank.BranchNo
                                , bank.BankAccountName, bank.CityNo))
                            {
                                errors.Add(new EkycRegisterErrors()
                                {
                                    RetErrorCode = EkycRetCodeConst.EK_915_BANK_ONE_ISVALID_REQUIRED,
                                    RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK915
                                });
                            }
                        }
                    }
                    else
                    {
                        errors.Add(new EkycRegisterErrors()
                        {
                            RetErrorCode = EkycRetCodeConst.EK_915_BANK_ONE_ISVALID_REQUIRED,
                            RetErrorMsg = EkycRetMsgConst.EKRetMsg_EK915
                        });
                    }
                }

                return errors;
            }
            catch (Exception ex)
            {
                log.Error("ValidationFormRegisterUser" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetListPlaceOfIssueResult>> GetListPlaceOfIssue()
        {
            try
            {
                var listPlaceOfIssue = new List<GetListPlaceOfIssueResult>();
                return await WithCRMDBConnection(async conn =>
                {
                    var data = await conn.QueryAsync<GetListPlaceOfIssueResult>(_commandText.GetListPlaceOfIssue);
                    foreach (var item in data)
                    {
                        if (item.CategoryName.Equals("CCS ĐKQL Cư trú và DLQG về DC"))
                        {
                            listPlaceOfIssue.Add(new GetListPlaceOfIssueResult()
                            {
                                Category = item.Category,
                                CategoryName = item.CategoryName,
                                CategoryFullName = "Cục Cảnh sát đăng ký quản lý cư trú và dữ liệu quốc gia về dân cư"
                            });
                        }
                        else if (item.CategoryName.Contains("CCS QLHC"))
                        {
                            listPlaceOfIssue.Add(new GetListPlaceOfIssueResult()
                            {
                                Category = item.Category,
                                CategoryName = item.CategoryName,
                                CategoryFullName = "Cục trưởng cục cảnh sát quản lý hành chính về trật tự xã hội"
                            });
                        }
                        else if (item.CategoryName.Contains("Sở KHĐT TP.HCM"))
                        {
                            listPlaceOfIssue.Add(new GetListPlaceOfIssueResult()
                            {
                                Category = item.Category,
                                CategoryName = item.CategoryName,
                                CategoryFullName = "Sở Kế hoạch và Đầu tư Thành Phố Hồ Chí Minh"
                            });
                        }
                        else
                        {
                            listPlaceOfIssue.Add(item);
                        }
                    }

                    return listPlaceOfIssue;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListPlaceOfIssue" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<BankInfoListResult>> GetBankAccountList(string accountNo)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryAsync<BankInfoListResult>(_commandText.GetBankInfo, new
                    {
                        @CustCode = accountNo
                    });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetBankAccountList" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<BankDetailInfoResult> GetBankAccountDetail(AccNoDetailRequest model)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryFirstAsync<BankDetailInfoResult>(_commandText.GetBankById, new
                    {
                        @BankAccountID = model.BankAccountID,
                        @CustCode = model.UserID,
                    });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetBankAccountDetail" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<BankModel>> GetBankList()
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryAsync<BankModel>(_commandText.GetBankList);
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetBankList" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> UpdateStatusVideo(UpdateVideoSignatureRequest model)
        {
            try
            {
                return await WithCommonDBConnection(async conn =>
                {
                    var dataCommondb = await conn.QueryFirstOrDefaultAsync<ExtendAccountDb>(
                        _commandText.GetAccountExtend, new
                        {
                            @ma_khach_hang = model.UserID
                        });
                    if (dataCommondb != null)
                    {
                        var dataReturn = JsonConvert.DeserializeObject<EkycMorong>(dataCommondb.thong_tin_mo_rong);
                        dataReturn.FolderPath.VideoSignature = model.VideoSignature;

                        var data = await conn.ExecuteAsync(_commandText.UpdateVideoSigatureExtend, new
                        {
                            @ma_khach_hang = model.UserID,
                            @thong_tin_mo_rong = JsonConvert.SerializeObject(dataReturn,
                                Newtonsoft.Json.Formatting.None,
                                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore}),
                            @ngay_cap_nhat = DateTime.Now
                        });
                        if (data > 0)
                        {
                            return 99;
                        }

                        return -2;
                    }
                    else
                        return -1;
                });
            }
            catch (Exception ex)
            {
                log.Error("UpdateVideoSignature" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> CheckUpdateStatusVideo(string accountNo)
        {
            try
            {
                return await WithCommonDBConnection(async conn =>
                {
                    var dataCommondb = await conn.QueryFirstOrDefaultAsync<ExtendAccountDb>(_commandText.GetVideoUpdate,
                        new
                        {
                            @ma_khach_hang = accountNo
                        });
                    if (dataCommondb != null)
                    {
                        var dataReturn = JsonConvert.DeserializeObject<EkycMorong>(dataCommondb.thong_tin_mo_rong);
                        return !string.IsNullOrEmpty(dataReturn.FolderPath.VideoSignature);
                    }

                    return false;
                });
            }
            catch (Exception ex)
            {
                log.Error("CheckUpdateStatusVideo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// kiem tra cardID ton tai
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public async Task<bool> CardIDExsited(string cardId)
        {
            try
            {
                var cardIdAccount = await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetCardIDCommon, new
                    {
                        @so_cmnd = cardId
                    });
                    return data;
                });

                var sql = "  SELECT so_cmnd FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE so_cmnd = @CardId ";
                var cardIdIdRegis = await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        @CardId = cardId
                    });
                    return data;
                });

                var cardIdCrm = await WithCRMDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetCardIDCRM, new
                    {
                        @so_cmnd = cardId
                    });
                    return data;
                });
                return string.IsNullOrEmpty(cardIdAccount) && string.IsNullOrEmpty(cardIdCrm) && string.IsNullOrEmpty(cardIdIdRegis);
                
            }
            catch (Exception ex)
            {
                log.Error("CardIDExsited" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }  
        
        
        /// <summary>
        /// kiem tra cardID ton tai
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public async Task<int> CardIDExsited_v2(string cardId)
        {
            try
            {
                var sqlComm = "SELECT ma_khach_hang FROM TVSI_TAI_KHOAN_KHACH_HANG WHERE so_cmnd_passport = @CardId";
                var cardIdAccount = await WithCommonDBConnection(async conn =>
                {
                    return await conn.QueryFirstOrDefaultAsync<string>(sqlComm, new
                    {
                        @CardId = cardId
                    });
                });
                if (!string.IsNullOrEmpty(cardIdAccount))
                    return 1;

                var sql = "SELECT * FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE so_cmnd = @CardId";
                var cardIdIdRegis = await WithCommonDBConnection(async conn =>
                {
                    return await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        @CardId = cardId
                    });
                });
                if(!string.IsNullOrEmpty(cardIdIdRegis))
                   return 1;

                var cardIdCrm = await WithCRMDBConnection(async conn =>
                {
                    return await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        @CardId = cardId
                    });
                  
                });
                if (!string.IsNullOrEmpty(cardIdCrm))
                    return 2;

                return 0;
            }
            catch (Exception ex)
            {
                log.Error("CardIDExsited_v2" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<BranchInfoResult>> GetSubBranchList(string bankNo)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryAsync<BranchInfoResult>(
                        _commandText.GetSubBranchList,
                        new
                        {
                            @bankNo = bankNo,
                        });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetSubBranchList" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<ProvinceModelResult>> GetProvinceList()
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ProvinceModelResult>(
                        _commandText.GetProvinceList);
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetProvinceList" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<ManInfo>> GetManInfo()
        {
            try
            {
                return await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ManInfo>(_commandText.TVSI_sGetManInfo);
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetManInfo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<string> GetBankName(string bankNo)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetBankName, new
                    {
                        @bankNo = bankNo
                    });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetBankName" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<string> GetSubBranchName(string bankNo, string branchNo)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetSubBranchName, new
                    {
                        @bankNo = bankNo,
                        @branchNo = branchNo
                    });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetSubBranchName" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<string> GetSaleID(string saleId)
        {
            try
            {
                return await WithEmsDBConnection(async conn =>
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@id_he_thong", saleId);
                    var data = await conn.QueryFirstOrDefaultAsync<string>(_commandText.TVSI_sGetSaleId,
                        queryParameters, commandType: CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetSaleID" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<string> GetProvinceName(string provinceId)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetProvinceName, new
                    {
                        provinceID = provinceId
                    });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetProvinceName" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<ChangeCustInfo> GetSoDienThoaiChangeInfo(string newMobile, int statusMobile, string oldMobile,
            int options)
        {
            try
            {
                switch (options)
                {
                    case 1:
                        return new ChangeCustInfo()
                        {
                            Name = statusMobile == 1
                                ? EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_01
                                : EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_02,
                            Options = new List<int> {1},
                            Values = new List<string> {newMobile}
                        };
                    case 2:
                        return new ChangeCustInfo()
                        {
                            Name = statusMobile == 1
                                ? EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_01
                                : EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_02,
                            Options = new List<int> {2},
                            Values = new List<string> {oldMobile}
                        };
                    case 3:
                        return new ChangeCustInfo()
                        {
                            Name = statusMobile == 1
                                ? EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_01
                                : EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_02,
                            Options = new List<int> {3},
                            Values = new List<string> {oldMobile, newMobile}
                        };
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetSoDienThoai01ChangeInfo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<ChangeCustInfo> GetFullNameChangeInfo(string alreadName, string newName)
        {
            try
            {
                return new ChangeCustInfo()
                {
                    Name = EkycChangeCustInfoConst.NAME_HOTEN,
                    Values = new List<string>
                    {
                        alreadName, newName
                    }
                };
            }
            catch (Exception ex)
            {
                log.Error("GetFullNameChangeInfo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<ChangeCustInfo> GetCMNDChangeInfo(string oldCardIssueDate, string cardIssueDate,
            string oldCardIssue,
            string cardIssue, string oldCardId, string newCard)
        {
            try
            {
                return new ChangeCustInfo()
                {
                    Name = EkycChangeCustInfoConst.NAME_CMND,
                    Values = new List<string>()
                    {
                        oldCardId, newCard,
                        oldCardIssueDate, cardIssueDate,
                        oldCardIssue, cardIssue
                    }
                };
            }
            catch (Exception ex)
            {
                log.Error("GetCMNDChangeInfo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<ChangeCustInfo> GetAddressChangeInfo(string oldAddress, string newAddress)
        {
            try
            {
                return new ChangeCustInfo()
                {
                    Name = EkycChangeCustInfoConst.NAME_DIA_CHI,
                    Values = new List<string>()
                    {
                        oldAddress, newAddress
                    }
                };
            }
            catch (Exception ex)
            {
                log.Error("GetAddressChangeInfo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<ChangeCustInfo> GetSMSChangeInfo(int options)
        {
            try
            {
                switch (options)
                {
                    case 1:
                        return new ChangeCustInfo()
                        {
                            Name = EkycChangeCustInfoConst.NAME_SMS,
                            Options = new List<int> {1},
                        };
                    case 2:
                        return new ChangeCustInfo()
                        {
                            Name = EkycChangeCustInfoConst.NAME_SMS,
                            Options = new List<int> {2},
                        };
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetSMSChangeInfo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<ChangeCustInfo> GetSoDienThoai01ChangeInfo(string smsPhoneOld, string smsPhoneReceive,
            int options)
        {
            try
            {
                if (!string.IsNullOrEmpty(smsPhoneOld))
                {
                    if (smsPhoneOld.Contains(smsPhoneReceive.Trim()))
                        smsPhoneReceive = string.Empty;
                }

                if (options == 1)
                {
                    smsPhoneOld = null;
                    if (!string.IsNullOrEmpty(smsPhoneReceive.Trim()))
                    {
                        return new ChangeCustInfo()
                        {
                            Name = EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_01,
                            Options = new List<int> {1},
                            Values = new List<string> {smsPhoneReceive}
                        };
                    }
                }

                if (!string.IsNullOrEmpty(smsPhoneOld) && !string.IsNullOrEmpty(smsPhoneReceive.Trim()))
                {
                    return new ChangeCustInfo
                    {
                        Name = EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_01,
                        Options = new List<int> {3},
                        Values = new List<string> {smsPhoneOld, smsPhoneReceive}
                    };
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetSoDienThoai01ChangeInfo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<ChangeCustInfo> GetSoDienThoai02ChangeInfo(string smsPhoneOld, string smsPhoneReceive)
        {
            try
            {
                if (smsPhoneOld.Contains(smsPhoneReceive.Trim()))
                    smsPhoneReceive = string.Empty;

                if (!string.IsNullOrEmpty(smsPhoneOld) && !string.IsNullOrEmpty(smsPhoneReceive))
                    return new ChangeCustInfo
                    {
                        Name = EkycChangeCustInfoConst.NAME_SO_DIEN_THOAI_02,
                        Options = new List<int> {3},
                        Values = new List<string> {smsPhoneOld, smsPhoneReceive}
                    };


                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetSoDienThoai02ChangeInfo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<ChangeCustInfo> GetUTTDChangeInfo(int optionsAdvanceService)
        {
            try
            {
                switch (optionsAdvanceService)
                {
                    case 1:
                        return new ChangeCustInfo()
                        {
                            Name = EkycChangeCustInfoConst.NAME_UTTD,
                            Options = new List<int> {1}
                        };
                    case 2:
                        return new ChangeCustInfo()
                        {
                            Name = EkycChangeCustInfoConst.NAME_UTTD,
                            Options = new List<int> {2}
                        };
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetUTTDChangeInfo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<GetPhoneCustResult> GetPhoneCust(string custId)
        {
            try
            {
                var listPhone = new GetPhoneCustResult();
                return await WithInnoConnection(async conn =>
                {
                    listPhone.SmsPhone_01 = await conn.QueryFirstOrDefaultAsync<string>(
                        _commandText.GetPhoneByNumCustInno01, new
                        {
                            @custid = custId
                        });

                    var phoneListCC = await conn.QueryAsync<string>(_commandText.GetPhoneByNumCustInno02, new
                    {
                        @custid = custId
                    });
                    if (phoneListCC.Any())
                    {
                        foreach (var phone in phoneListCC)
                        {
                            if (!string.IsNullOrEmpty(phone))
                                listPhone.SmsPhone_02.Add(phone);
                        }
                    }

                    return listPhone;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetServiceChangeInfo" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> GetPhoneExisted(string customerID, string phone)
        {
            try
            {
                var sqlPhoneNew =
                    "SELECT Phone from CUSTOMERPHONE where CustomerID = @CusID and Phone = @Phone and IsActive = 1 and IsSMS = 1";
                var phoneNew = await WithInnoConnection(async conn =>
                    await conn.QueryAsync<string>(sqlPhoneNew, new
                    {
                        CusID = customerID,
                        Phone = phone
                    }));

                if (phoneNew.Any())
                    return 1;

                var sqlPhoneExis =
                    "SELECT Phone from CUSTOMERPHONE  where CustomerID = @CusID and IsActive = 1 and IsSMS = 1";
                var phoneExist = await WithInnoConnection(async conn =>
                    await conn.QueryAsync<string>(sqlPhoneExis, new
                    {
                        CusID = customerID
                    }));
                if (phoneExist.Any())
                    return 2;


                return 0;
            }
            catch (Exception ex)
            {
                log.Error("GetPhoneExsited: " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<UserCRM> GetUserCRMConfirm(string codem)
        {
            try
            {
                var sql =
                    "select ho_ten FullName, ngay_sinh DateOfBirthOn, so_cmnd CardID, ngay_cap_cmnd IssueDateOn,noi_cap_cmnd CardIssue, hop_dong_bo_sung TypeAccount,email Email from [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] where ConfirmCode = @confrimCode and ConfirmStatus = 1";
                return
                    await WithCRMDBConnection(async conn =>
                        await conn.QueryFirstOrDefaultAsync<UserCRM>(sql, new
                        {
                            @confrimCode = codem
                        }));
            }
            catch (Exception ex)
            {
                log.Error("GetUserCRMConfirm" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<SaleInfoModel> GetSaleInfo(string saleId)
        {
            try
            {
                    if (saleId != null && saleId.Length >= 4)
                    {
                        var sql = "SELECT dia_chi_email Email, danh_muc_phong_ban_id DepartmentID,ho_va_ten SaleName   FROM TVSI_NHAN_SU WHERE ma_quan_ly = @saleID AND trang_thai = 1";
                        return await WithEmsDBConnection(async conn =>
                        {
                            return await conn.QueryFirstOrDefaultAsync<SaleInfoModel>(sql, new
                            {
                                saleID = saleId.Substring(0,4)
                            });
                        });
                    }
                    return null;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("GetSaleInfo --- {0}", ex.Message));
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> UpdateCardID(string cardID)
        {
            try
            {
                var data = await WithCRMDBConnection(async conn =>
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@CardId", cardID);
                    return await conn.ExecuteAsync("TVSI_sUPDATE_CARDID_EKYC_MOBILE",
                        parameters, commandType: CommandType.StoredProcedure, commandTimeout: 60);
                });
                return data == -1;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("UpdateCardID --- {0}", ex.Message));
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<SaleIDResult> GetSaleName(string saleID)
        {
            try
            {
                return await WithEmsDBConnection(async conn =>
                {
                    var sql = "select ho_va_ten SaleName from TVSI_NHAN_SU where ma_quan_ly = @saleID and trang_thai = 1";
                    return conn.QueryFirstOrDefault<SaleIDResult>(sql, new
                    {
                        saleID = saleID.Substring(0, 4)
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("GetSaleName --- {0}", ex.Message));
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<string> GetEmailSale(string saleID)
        {
            try
            {
                return await  WithEmsDBConnection(async conn =>
                {
                    var sql = "SELECT dia_chi_email Email FROM TVSI_NHAN_SU WHERE ma_quan_ly = @saleID AND trang_thai = 1";
                    return await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        saleID = saleID.Substring(0, 4)
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("GetEmailSale --- {0}", ex.Message));
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> CheckCustomerFormInfomation(string email, string phone)
        {
            try
            {
                return await WithCRMDBConnection(async conn =>
                {
                    var sql =
                        "SELECT so_cmnd FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE email = @Email and so_dien_thoai_01 = @Phone and nguon_du_lieu = 3";
                    var datacrm = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        Email = email,
                        Phone = phone
                    });
                    if (!string.IsNullOrEmpty(datacrm))
                    {
                        var commonsql =
                            "SELECT ma_khach_hang FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE email = @Email and so_dien_thoai_01 = @Phone and nguon_du_lieu = 3";
                        var datacommon = await WithCommonDBConnection(async connn =>
                        {
                            return await connn.QueryFirstOrDefaultAsync<string>(commonsql, new
                            {
                                Email = email,
                                Phone = phone
                            });
                        });
                        if (!string.IsNullOrEmpty(datacommon))
                            return 1;
                        
                        return 2;
                    }
                    return 0;

                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("CheckInfomation --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<SaleData> GenerateSaleId(int value, int type)
        {
            try
            {
                if (type == 1)
                {
                    switch (value)
                    {
                        case 1: return await GetConditionSaleId(UtilsBranchID.DVGDHS,value);
                        case 2: return await GetConditionSaleId(UtilsBranchID.DVDTHS,value);
                        case 3: return await GetConditionSaleId(UtilsBranchID.DVKH,value);
                    }
                }
                if (type == 2)
                {
                    switch (value)
                    {
                        case 1: return await GetConditionSaleId(UtilsBranchID.DVGDHCM,value);
                        case 2: return await GetConditionSaleId(UtilsBranchID.DVDTHCM,value);
                        case 3: return await GetConditionSaleId(UtilsBranchID.DVKH,value);
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("GenerateSaleId --- {0}", ex.Message));
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> CheckCustomerEkycInfomation(string email, string phone)
        {
            try
            {
                var sql =
                    "SELECT so_cmnd FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE email = @Email and so_dien_thoai_01 = @Phone and  ( nguon_du_lieu = 4 or nguon_du_lieu = 6 ) and ConfirmStatus = 0";
                return await WithCRMDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        Email = email,
                        Phone = phone
                    });
                    if (!string.IsNullOrEmpty(data))
                        return 1;

                    return 0;
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("CheckCustomerEkycInfomation --- {0}", ex.Message));
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> ProcessSaleId(string provinCode, int servicePackage)
        {
            try
            {
                return await WithCRMDBConnection(async conn =>
                {
                    var sql = "SELECT Type FROM Province WHERE ProvinceCode = @ProvinceCode And Status = 1";
                    var typeProvince = await conn.QueryFirstOrDefaultAsync<int>(sql, new
                    {
                        ProvinceCode = provinCode
                    });
                    return typeProvince;

                });
            }
            catch (Exception ex)
            {

                log.Error(string.Format("CheckCustomerEkycInfomation --- {0}", ex.Message));

                log.Error(string.Format("ProcessSaleId --- {0}", ex.Message));

                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<string> GetAccountNumber(string email,string phone)
        {
            try
            {
                return await WithCommonDBConnection(async conn =>
                {
                    var sql = "SELECT ma_kinh_doanh FROM TVSI_TAI_KHOAN_KHACH_HANG WHERE dia_chi_email = @email AND so_dien_thoai = @phone AND trang_thai <> 99 ";
                    var dataAccount = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        email = email,
                        phone = phone
                    });
                    if (!string.IsNullOrEmpty(dataAccount))
                        return dataAccount;
                    var sqlRegis = "SELECT so_tai_khoan FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE email = @Email AND so_dien_thoai_01 = @Phone AND trang_thai_tk <> 999";
                    
                    var dataReg = await conn.QueryFirstOrDefaultAsync<string>(sqlRegis, new
                    {
                        email = email,
                        phone = phone
                    });
                    if (!string.IsNullOrEmpty(dataReg))
                        return dataReg;
                    return "";
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("GetAccountNumber --- {0}", ex.Message));
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<string> GetCustCodeByCardId(string cardId)
        {
            return await WithCommonDBConnection(async conn =>
            {
                var sql = "SELECT ma_kinh_doanh FROM TVSI_TAI_KHOAN_KHACH_HANG WHERE so_cmnd_passport = @CardId AND trang_thai <> 99 ";
                var dataAccount = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                {
                    CardId = cardId
                });
                if (!string.IsNullOrEmpty(dataAccount))
                    return dataAccount;
                
                var sqlRegis = "SELECT so_tai_khoan FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE so_cmnd = @CardId AND trang_thai_tk <> 999";
                var dataReg = await conn.QueryFirstOrDefaultAsync<string>(sqlRegis, new
                {
                    CardId = cardId
                });
                if (!string.IsNullOrEmpty(dataReg))
                    return dataReg;
                return "";
            });
        }

        public async Task<SaleData> GetConditionSaleId(string branchId, int value)
        {
            var sql = "SELECT * FROM WorkTime WHERE Status = 1 {condition}";
            if (value == 2)
                sql = sql.Replace("{condition}", "and BranchCode = @BranchCode");
            else 
                sql = sql.Replace("{condition}", "and BranchID = @BranchID");

            return await WithCRMDBConnection(async conn =>
            {
                var data = await conn.QueryAsync<EkycThreadingModel>(sql, new
                {
                    BranchID = branchId,
                    BranchCode = branchId,
                });
                if (data != null && data.Any())
                {
                    var min = data.Min(x => x.Count);
                    var dataSale = data.Select( t => t).Where(x => x.Count == min).FirstOrDefault();
                    if (dataSale != null)
                    {
                        var workTimeID = dataSale.WorkTimeID;
                        var countSet = Convert.ToInt64(dataSale.Count) + 1;
                        var sqlUP = "update WorkTime set Count = @Count where WorkTimeID = @WorkTimeID";
                        if (workTimeID > 0)
                        {
                            var result = await conn.ExecuteAsync(sqlUP, new
                            {
                                Count = countSet,
                                WorkTimeID = workTimeID
                            });
                        }
                    }

                    if (dataSale != null && dataSale.SaleID != null && dataSale.SaleID.Length >= 4)
                    {
                        return new SaleData()
                        {
                            BranchID = branchId,
                            SaleID = dataSale.SaleID.Substring(0, 4)
                        };
                    }
                }
                return null;
            });


        }
    }
}