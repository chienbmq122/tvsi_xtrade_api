﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using TVSI.Bond.WebAPI.Lib.Models.Order;
using log4net;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class OrderService : BaseService, IOrderService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CustomerService));
        private IOrderCommandText _commandText;

        public OrderService(IOrderCommandText commandText)
        {
            _commandText = commandText;
        }

        public async Task<IEnumerable<ContractListResult>> GetContractList(ContractListRequest contractListRequest, string lang)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                                                     {
                    var sql = _commandText.GetContractList;
                    if (lang == CommonConst.LANGUAGE_EN)
                        sql = sql.Replace("D.CodeContent", "D.CodeContentEn")                                                         ;

                    var data = await conn.QueryAsync<ContractListResult>(sql,
                        new
                        {
                            AccountNo = contractListRequest.AccountNo,
                            CurDate = DateTime.Today.Date,
                            ProductType = contractListRequest.ProductType,
                            ContractNo = contractListRequest.ContractNo,
                            BondCode = contractListRequest.BondCode,
                            MaturityDate = contractListRequest.MaturityDate,
                            FromDate = contractListRequest.FromDate,
                            ToDate = contractListRequest.ToDate
                        });

                    // Get danh sach nhung lenh dang cho xu ly
                    var contractNoList = data.Select(x => x.ContractNo).ToList();

                    var processingContract = await conn.QueryAsync<ProcessingContract>(_commandText.GetProcessingSellContract,
                            new {ContractNoList = contractNoList});

                    if (processingContract.Any())
                    {
                        foreach (var processingItem in processingContract)
                        {
                            var contractItem = data.FirstOrDefault(x => x.ContractNo == processingItem.ContractNo);
                            if (contractItem != null)
                            {
                                contractItem.IsSell = false;
                                contractItem.IsRoll = false;
                            }
                        }
                    }

                    // Update Bond Policy: Được phép Roll hay không
                    var bondCodeList = data.Where(x => x.IsRoll).Select(x => x.BondCode).Distinct().ToList();
                    if (bondCodeList.Count > 0)
                    {
                        var custCode = contractListRequest.AccountNo.Substring(0, 6);
                        var profesType = await conn.QueryFirstOrDefaultAsync<int?>(_commandText.GetProfesType, new {CustCode = custCode});
                        profesType = profesType ?? 0;

                        // Get chính sách
                        var bondPolicyList = await conn.QueryAsync<dynamic>(_commandText.GetBondPolicyInfo, new { BondCodeList = bondCodeList});
                        foreach (var policyItem in bondPolicyList)
                        {
                            if (profesType == 1 && policyItem.Pro_CanBuy == 0)
                            {
                                var contractItemList = data.Where(x => x.BondCode == policyItem.BondCode).ToList();
                                foreach (var contractItem in contractItemList)
                                {
                                    contractItem.IsRoll = false;
                                }

                            }
                            else if (profesType == 0 && policyItem.Nor_CanBuy == 0)
                            {
                                var contractItemList = data.Where(x => x.BondCode == policyItem.BondCode).ToList();
                                foreach (var contractItem in contractItemList)
                                {
                                    contractItem.IsRoll = false;
                                }
                            }
                        }
                    }


                    return data;
                });
                
            }
            catch (Exception ex)
            {
                LogException(log, contractListRequest.AccountNo, ex, "GetContractList:ProductType=" + contractListRequest.ProductType +
                            " ContractNo=" + contractListRequest.ContractNo + " BondCode=" + contractListRequest.BondCode +
                            " MaturityDate=" + contractListRequest.MaturityDate + " FromDate=" + contractListRequest.FromDate +
                            " ToDate=" + contractListRequest.ToDate);
                throw;
            }
        }

        public async Task<ContractDetailResult> GetContractDetail(string contractNo, string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                {
                    // Chi tiet hop dong
                    var detailInfo = await conn.QueryFirstOrDefaultAsync<ContractDetailResult>(_commandText.GetContractDetail, 
                            new {contractNo = contractNo, accountNo = accountNo, CurDate = DateTime.Today });

                    if (detailInfo != null) {
                        // 20220218 Them cot IsRoll, IsSell
                        // BEGIN
                        var contractNoList = new List<string> {contractNo};
                        var processingContract = await conn.QueryAsync<ProcessingContract>(_commandText.GetProcessingSellContract,
                            new { ContractNoList = contractNoList });

                        if (processingContract.Any())
                        {
                            foreach (var processingItem in processingContract)
                            {
                                if (processingItem.ContractNo == contractNo)
                                {
                                    detailInfo.IsSell = false;
                                    detailInfo.IsRoll = false;
                                    break;
                                }
                            }
                        }

                        // Update Bond Policy: Được phép Roll hay không
                        if (detailInfo.IsRoll)
                        {
                            var custCode = accountNo.Substring(0, 6);
                            var profesType = await conn.QueryFirstOrDefaultAsync<int?>(_commandText.GetProfesType, new { CustCode = custCode });
                            profesType = profesType ?? 0;

                            // Get chính sách
                            var bondPolicyList = await conn.QueryAsync<dynamic>(_commandText.GetBondPolicyInfo, new { BondCodeList = new List<string> {detailInfo.BondCode} });
                            //foreach (var policyItem in bondPolicyList)
                            //{
                            //    if (profesType == 1 && policyItem.Pro_CanBuy == 0)
                            //    {
                            //        if (detailInfo.BondCode == policyItem.BondCode)
                            //            detailInfo.IsRoll = false;

                            //    }
                            //    else if (profesType == 0 && policyItem.Nor_CanBuy == 0)
                            //    {
                            //        if (detailInfo.BondCode == policyItem.BondCode)
                            //            detailInfo.IsRoll = false;
                            //    }
                            //}
                        }
                        
                        // END

                        // Thông tin tài khoản nhận tiền
                        if (detailInfo.PaymentMethod == 1)
                        {
                            if (detailInfo.CustBankID.HasValue)
                            {
                                var custBankData = await conn.QueryFirstOrDefaultAsync<dynamic>(_commandText.GetCustBankInfo,
                                    new {CustBankID = detailInfo.CustBankID.Value});
                                if (custBankData != null)
                                {
                                    detailInfo.MoneyReceiver = custBankData.BankAccount + "-" + custBankData.BankAccountName
                                        + "-" + custBankData.BankName + "-" + custBankData.SubBranchName;
                                }
                            }
                        }
                        else
                        {
                            detailInfo.MoneyReceiver = ResourceFile.OrderModule.PaymentMethodName_0;
                        }
                         
                        // Chi tiết lệnh đặt
                        var orderDetailInfo = await conn.QueryAsync<OrderDetailInfo>("TVSI_sAPI_OM_CL_GetContractOrderDetail",
                                new { ContractNo = contractNo, AccountNo = accountNo }, commandType: System.Data.CommandType.StoredProcedure);
                        detailInfo.OrderDetailList = orderDetailInfo.ToList();

                        // Lãi định kỳ đã nhận
                        var receivedCouponInfo = await conn.QueryAsync<CouponDetailInfo>("TVSI_sAPI_OM_CL_GetReceivedCouponAndCash", 
                            new {ContractNo = detailInfo.ContractNo}, commandType: System.Data.CommandType.StoredProcedure);
                        detailInfo.ReceivedCouponList = receivedCouponInfo.ToList();

                        // Trường hợp chưa bán hết thì lấy thêm tổng tiền bán và lãi cuối kỳ
                        if (detailInfo.AvailableVolume > 0)
                        {
                            var couponInfo = await conn.QueryAsync<CouponDetailInfo>(_commandText.GetContractCoupon, 
                                new {ContractNo = detailInfo.ContractNo, Status = 0});

                            // Danh sách lãi sẽ được nhận
                            var couponInfoList = couponInfo.ToList();

                            var jsonData = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetContractPriceInfo,
                                    new { ContractNo = contractNo, AccountNo = accountNo });

                            if (!string.IsNullOrEmpty(jsonData))
                            {
                                var priceInfoData = JsonConvert.DeserializeObject<List<BondPriceCalc>>(jsonData);

                                var sellAmountAndLastCouponItem =
                                    priceInfoData.FirstOrDefault(x => x.RowCode == "SellAmountAndLastCoupon");
                                if (sellAmountAndLastCouponItem != null)
                                {
                                    couponInfoList.Add(new CouponDetailInfo
                                    {
                                        CouponDate =
                                            sellAmountAndLastCouponItem.ToDate?.ToString("dd/MM/yyyy") ?? "",
                                        Volume = detailInfo.AvailableVolume,
                                        Amount = sellAmountAndLastCouponItem.Income,
                                        Note = ResourceFile.OrderModule.Coupon_Note_03
                                    });
                                }

                                else
                                {
                                    var netAmountItem = priceInfoData.FirstOrDefault(x => x.RowCode == "SellAmount");
                                    if (netAmountItem != null)
                                    {
                                        couponInfoList.Add(new CouponDetailInfo
                                        {
                                            CouponDate = netAmountItem.ToDate?.ToString("dd/MM/yyyy") ?? "",
                                            Volume = detailInfo.AvailableVolume,
                                            Amount = netAmountItem.Income,
                                            Note = ResourceFile.OrderModule.Coupon_Note_03
                                        });
                                    }
                                }
                            }

                            detailInfo.CouponList = couponInfoList;
                        }
                        else
                        {
                            detailInfo.CouponList = new List<CouponDetailInfo>();                            
                        }

                    }

                    return detailInfo;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetContractDetail:contractNo=" + contractNo + ",accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<IEnumerable<OrderBookResult>> GetOrderBook(DateTime tradeDate, string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<OrderBookResult>(_commandText.GetOrderBook, 
                        new { tradeDate = tradeDate, accountNo = accountNo });
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetOrderBook:accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<OrderDetailResult> GetOrderDetail(string contractNo, string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn => await conn.QueryFirstOrDefaultAsync<OrderDetailResult>("TVSI_sAPI_OM_OB_OrderDetail",
                new { accountNo = accountNo, contractNo = contractNo }, commandType: System.Data.CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetOrderDetail:contractNo=" + contractNo + ",accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<IEnumerable<OrderHistResult>> GetOrderHistList(DateTime fromDate, DateTime toDate,
            string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<OrderHistResult>(_commandText.GetOrderHist, 
                        new { fromDate = fromDate, toDate = toDate, accountNo = accountNo });
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetOrderHistList:accountNo=" + accountNo + ",fromDate=" + fromDate.ToString("yyyyMMdd")
                    + ",toDate=" + toDate.ToString("yyyyMMdd"));
                throw;
            }
        }

        public async Task<IEnumerable<OrderConfirmListResult>> GetOrderConfirmList(DateTime fromDate, DateTime toDate,
            string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                { 
                    var data = await conn.QueryAsync<OrderConfirmListResult>("TVSI_sAPI_OM_OC_OrderConfirmList",
                        new { FromDate = fromDate, ToDate = toDate, AccountNo = accountNo }, commandType: System.Data.CommandType.StoredProcedure);

                    return data;

                    // var data = await conn.QueryAsync<OrderConfirmListResult>(_commandText.GetOrderConfirm,
                    //    new { fromDate = fromDate, toDate = toDate, accountNo = accountNo });
                    // return data;
                });
        }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetOrderConfrimList:accountNo=" + accountNo + ",fromDate=" + fromDate.ToString("yyyyMMdd")
                    + ",toDate=" + toDate.ToString("yyyyMMdd"));
                throw;
            }
        }

        public async Task<int> CancelOrder(string contractNo, string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn => await conn.QueryFirstOrDefaultAsync<int>("TVSI_sAPI_OM_OB_CancelOrder",
                     new { AccountNo = accountNo, ContractNo = contractNo }, commandType: System.Data.CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "CancelOrder:accountNo=" + accountNo + ",contractNo=" + contractNo);
                throw;
            }
        }

        public async Task<int> ConfirmOrder(long requestOrderID, string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn => await conn.QueryFirstOrDefaultAsync<int>("TVSI_sAPI_OM_OC_ConfirmOrder",
                     new { AccountNo = accountNo, RequestOrderID = requestOrderID }, commandType: System.Data.CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "ConfirmOrder:accountNo=" + accountNo + ",requestOrderID=" + requestOrderID);
                throw;
            }
        }

        public async Task<OrderConfirmDetailResult> GetOrderConfirmDetail(string requestOrderID, string accountNo)
        {
            try
            {
                var orderConfirmDetail = await WithTbmConnection(async conn => await conn.QueryFirstOrDefaultAsync<OrderConfirmDetailResult>("TVSI_sAPI_OM_OB_OrderConfirmDetail",
                new { accountNo = accountNo, requestOrderID = requestOrderID }, commandType: System.Data.CommandType.StoredProcedure));

                if (!string.IsNullOrEmpty(orderConfirmDetail.PriceInfo))
                {
                    var priceInfoData = JsonConvert.DeserializeObject<List<BondPriceCalc>>(orderConfirmDetail.PriceInfo);

                    List<PriceDetailInfo> priceDetailInfo = new List<PriceDetailInfo>();
                    foreach (var item in priceInfoData)
                    {
                        if (item.RowCode != "Yield")
                        {
                            priceDetailInfo.Add(new PriceDetailInfo
                            {
                                Content = item.Description,
                                CouponDate = item.ToDate?.ToString("dd/MM/yyyy") ?? "",
                                Value = item.Income.ToString("N0") + " " + item.Unit,
                            });
                        }
                        else
                        {
                            priceDetailInfo.Add(new PriceDetailInfo
                            {
                                Content = item.Description,
                                CouponDate = item.ToDate?.ToString("dd/MM/yyyy") ?? "",
                                Value = item.Income * 100 + " " + item.Unit,
                            });
                        }
                    }

                    orderConfirmDetail.PriceInfoList = priceDetailInfo;
                }

                return orderConfirmDetail;
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetOrderConfirmDetail:requestOrderID=" + requestOrderID + ",accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<int> RefuseOrder(long requestOrderID, string accountNo, string note)
        {
            try
            {
                return await WithTbmConnection(async conn => await conn.QueryFirstOrDefaultAsync<int>("TVSI_sAPI_OM_OC_RefuseOrder",
                     new { AccountNo = accountNo, RequestOrderID = requestOrderID, Note = note }, commandType: System.Data.CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "RefuseOrder:accountNo=" + accountNo + ",requestOrderID=" + requestOrderID + ",note=" + note);
                throw;
            }
        }
    }
}
