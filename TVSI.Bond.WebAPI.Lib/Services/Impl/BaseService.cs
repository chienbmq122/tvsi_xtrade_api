﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IBM.Data.DB2;
using log4net;
using Newtonsoft.Json;
using Serilog;
using TVSI.Bond.WebAPI.Lib.Infrastructure;
using TVSI.Bond.WebAPI.Lib.Models;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public abstract class BaseService
    {
        protected readonly string _xtradeInnoConnectionString;
        protected readonly string _innoConnectionString;
        protected readonly string _tbmConnectionString;
        protected readonly string _systemNotificationConnectionString;
        protected readonly string _fisdbConnectionString;
        protected readonly string _crmdbConnectionString;
        protected readonly string _commondbConnectionString;
        protected readonly string _emsdbConnectionString;
        protected readonly string _ipgdbConnectionString;
        protected readonly string _websitedbConnectionString;
        protected readonly string _centralizedbConnectionString;

        protected BaseService()
        {
            _xtradeInnoConnectionString = ConnectionFactory.XtradeInnoConnectionString;
            _innoConnectionString = ConnectionFactory.InnoConnectionString;
            _tbmConnectionString = ConnectionFactory.TbmConnectionString;
            _systemNotificationConnectionString = ConnectionFactory.SystemNotificationConnectionString;
            _fisdbConnectionString = ConnectionFactory.FisDBConnectionString;
            _crmdbConnectionString = ConnectionFactory.CRMDBConnectString;
            _commondbConnectionString = ConnectionFactory.CommonDBConnectString;
            _emsdbConnectionString = ConnectionFactory.EmsDBConnectString;
            _ipgdbConnectionString = ConnectionFactory.IpgDBConnectString;
            _websitedbConnectionString = ConnectionFactory.WebsiteDB_Connection;
            _centralizedbConnectionString = ConnectionFactory.CentralizeDB_Connection;
        }

        #region "Logging"

        protected void LogInfo(ILog log, string accountNo, string msg)
        {
            if (!string.IsNullOrEmpty(accountNo))
            {
                if (accountNo.Length > 6)
                    accountNo = accountNo.Substring(0, 6);
            }

            //log4net.LogicalThreadContext.Properties["AccountNo"] = accountNo;
            //log4net.Config.XmlConfigurator.Configure();
            //log.Info("CustCode=" + accountNo + "|Msg=" + msg);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + accountNo).Information("CustCode=" + accountNo + "\t Msg=" + msg);
        }

        protected void LogException(ILog log, string accountNo, Exception ex, string msg)
        {
            if (!string.IsNullOrEmpty(accountNo))
            {
                if (accountNo.Length > 6)
                    accountNo = accountNo.Substring(0, 6);
            }

            //log4net.LogicalThreadContext.Properties["AccountNo"] = accountNo;
            //log4net.Config.XmlConfigurator.Configure();
            //log.Error(msg + ":ExMsg=" + ex.Message + "\t InnerException=" + ex.InnerException);

            // Use Serilog to split file by UserID
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + accountNo).Error("CustCode=" + accountNo + "\t ErrMsg=" + ex.Message
                    + "\t InnerException=" + ex.InnerException);
        }

        #endregion

        #region "TbmDB Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithTbmConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_tbmConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithTbmConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_tbmConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithTbmConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_tbmConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region "XtradeInnoDB Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithXtradeInnoConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_xtradeInnoConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithXtradeInnoConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithXtradeInnoConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithXtradeInnoConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_xtradeInnoConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithXtradeInnoConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithXtradeInnoConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithXtradeInnoConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_xtradeInnoConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithXtradeInnoConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithXtradeInnoConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region "InnoDB Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithInnoConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_innoConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithInnoConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_innoConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithInnoConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_innoConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region "SystemNotificationDB Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithSystemNotificationConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_systemNotificationConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithSystemNotificationConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_systemNotificationConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithSystemNotificationConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_systemNotificationConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region "FisDB Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithDb2Connection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new DB2Connection(_fisdbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithDb2Connection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithDb2Connection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithDb2Connection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new DB2Connection(_fisdbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithDb2Connection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithDb2Connection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithDb2Connection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new DB2Connection(_fisdbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithDb2Connection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithDb2Connection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region "CRM connection"
        // use for buffered queries that return a type
        protected async Task<T> WithCRMDBConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_crmdbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithCRMDBConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_crmdbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithCRMDBConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_crmdbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion    
        
        #region "Commondb connection"
        // use for buffered queries that return a type
        protected async Task<T> WithCommonDBConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_commondbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithCommonDBConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_commondbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithCommonDBConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_commondbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion     
        
        #region "EmsDB connection"
        // use for buffered queries that return a type
        protected async Task<T> WithEmsDBConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_emsdbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithEmsDBConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_emsdbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithEmsDBConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_emsdbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion
        
        #region "IpgDB connection"
        // use for buffered queries that return a type
        protected async Task<T> WithIpgDBConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_ipgdbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithIpgDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithIpgDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithIpgDBConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_ipgdbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithIpgDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithIpgDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithIpgDBConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_ipgdbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithIpgDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithIpgDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region "WebsiteV2DB connection"
        // use for buffered queries that return a type
        protected async Task<T> WithWebsiteDBConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_websitedbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithWebsiteDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithWebsiteDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithWebsiteDBConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_websitedbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithWebsiteDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithWebsiteDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithWebsiteDBConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_websitedbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithWebsiteDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithWebsiteDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region "CentralizeDB Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithCentralizeDBConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_centralizedbConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCentralizeDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCentralizeDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithCentralizeDBConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_centralizedbConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCentralizeDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCentralizeDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithCentralizeDBConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_centralizedbConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCentralizeDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCentralizeDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion
    }
}
