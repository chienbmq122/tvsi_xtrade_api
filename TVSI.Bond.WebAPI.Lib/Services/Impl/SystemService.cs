﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Net.Http;
using System.Text;
using Dapper;
using log4net;
using TVSI.Bond.WebAPI.Lib.Models.System;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Models.Customer;
using TVSI.Bond.WebAPI.Lib.Utility;
using System.Data.SqlClient;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class SystemService : BaseService, ISystemService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(SystemService));
        private readonly ISystemCommandText _commandText;
        private readonly ICustomerCommandText _customerCommandText;

        public SystemService(ISystemCommandText commandText, ICustomerCommandText customerCommandText)
        {
            _commandText = commandText;
            _customerCommandText = customerCommandText;
        }

        /// <summary>
        /// Check đăng nhập
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>-1: KH không tồn tại, status: trạng thái KH</returns>
        public async Task<int?> GetUserStatus(string userName, string password)
        {
            try
            {
                //var chkLogin = await WithCommonDBConnection(async conn =>
                //{
                //    var sql = "SELECT CustCode FROM DANH_SACH_XTRADE WHERE CustCode = @CustCode AND Status = 1";
                    
                //    var custCode = await conn.QueryFirstOrDefaultAsync<string>(sql,
                //        new { CustCode = userName });

                //    return !string.IsNullOrEmpty(custCode);
                //});

                if (true)
                {
                    return await WithInnoConnection(async conn =>
                    {
                        var status = await conn.QueryFirstOrDefaultAsync<int?>(
                        _commandText.GetCustomerStatus, 
                                    new 
                                    {
                                        CustomerID = userName,
                                        Password = password
                                    });

                        if (status != null)
                            return status.Value;

                        return -1;
                    });
                }
                
                return -2;
            }
            catch (Exception ex)
            {
                LogException(log, userName, ex, "GetUserStatus");
                throw;
            }
        }

        /// <summary>
        /// Lấy danh sách tài khoản theo CustCode
        /// </summary>
        /// <param name="customerId">CustCode</param>
        /// <returns></returns>
        public async Task<IEnumerable<UserLoginResult>> GetAccountNoList(string customerId)
        {
            try
            {
                //var accountList =  await WithInnoConnection(async conn => await conn.QueryAsync<UserLoginResult>(_commandText.GetAccountNoList, 
                //    new { AccountNo = customerId + "%"}));

                //var accountList = await WithCommonDBConnection(async conn => await conn.QueryAsync<UserLoginResult>(_commandText.GetAccountNoList,
                //    new { CustCode = customerId }));

                var accountList = await WithCentralizeDBConnection(async conn => await conn.QueryAsync<UserLoginResult>(_commandText.GetAccountNoList,
                    new { CustCode = customerId }));

                var innoAccountList = await WithInnoConnection(async conn =>
                    await conn.QueryAsync<AccountNoListResult>(_customerCommandText.GetAccountNoList, new { CustomerID = customerId }));

                // Check tài khoản có phải tài khoản @ hay không
                using (var conn = new SqlConnection(_ipgdbConnectionString))
                {
                    conn.Open();
                    foreach (var account in accountList)
                    {
                        var alphaAccount = conn.QueryFirstOrDefault<string>(_commandText.GetAlphaAccount, new {so_tai_khoan=account.AccountNo});
                        account.IsAlphaAccount = !string.IsNullOrEmpty(alphaAccount);

                        var innoAccountItem = innoAccountList.FirstOrDefault(x => x.AccountNo == account.AccountNo);
                        if (innoAccountItem != null)
                            account.IsDefault = innoAccountItem.IsDefault;
                    }
                }

                return accountList;
            }
            catch (Exception ex)
            {
                LogException(log, customerId, ex, "GetAccountNoList");
                throw;
            }
        }

        public async Task<bool> CheckPassword(string customerId, string password)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var custcode = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetCustomerIdByPassword,
                        new { CustomerID = customerId, Password = password });

                    if (!string.IsNullOrEmpty(custcode))
                        return true;    // Mat khau ko dung

                    return false;
                });
            }
            catch (Exception ex)
            {
                LogException(log, customerId, ex, "CheckPassword");
                throw;
            }
        }

        public async Task<bool> CheckPin(string customerId, string pin)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var custcode = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetCustomerIdByPin,
                        new { CustomerID = customerId, Pin = pin });

                    if (!string.IsNullOrEmpty(custcode))
                        return true;    // Mat khau ko dung

                    return false;
                });
            }
            catch (Exception ex)
            {
                LogException(log, customerId, ex, "CheckPassword");
                throw;
            }
        }

        public async Task<int> ChangePassword(string customerId, string newPassword, string oldPassword)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var custcode = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetCustomerIdByPassword, 
                        new { CustomerID = customerId, Password = oldPassword});

                    if (string.IsNullOrEmpty(custcode))
                        return 1;    // Mat khau ko dung

                    // Cap nhap password
                    await conn.ExecuteAsync(_commandText.UpdatePasswordOfCustomer,
                                new { CustomerID = customerId, Password = newPassword });

                    return 0;
                });
            }
            catch (Exception ex)
            {
                LogException(log, customerId, ex, "ChangePassword");
                throw;
            }
        }


        public async Task<int> ForgotPassword(string customerId, string cardId)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var custInfo = await conn.QueryFirstOrDefaultAsync<CustInfo>(_commandText.GetForgotPassword,
                        new { CustomerID = customerId, IdentityCard = cardId });
                    if (string.IsNullOrEmpty(custInfo.CustID))
                    {
                        return 1;    // Mat khau ko dung
                    }
                    var confirmCode = EncryptHelper.EncriptMd5(customerId + EncryptHelper.RandomNumber(6)).Substring(0, 19);
                    // Cap nhat ma code gui mail
                    await conn.ExecuteAsync(_commandText.SendPassResetCode,
                        new { ConfirmCode = confirmCode, CustomerID = customerId, IdentityCard = cardId });
                    if (!string.IsNullOrEmpty(custInfo.Email))
                    {
                        try
                        {
                            var accountInfos = new Dictionary<string, string>
                                    {
                                        {"{CustName}", custInfo.CustName},
                                        {"{CustId}", custInfo.CustID},
                                        {"{CardId}",custInfo.CardId},
                                        {"{Code}", confirmCode}
                                    };
                            MailHelper.SendForgetPasswordMail(accountInfos, custInfo.Email);
                        }
                        catch (Exception ex)
                        {
                            LogException(log, customerId, ex, "ForgotPassword");
                            throw;
                        }
                    }
                    return 0;
                });
            }
            catch (Exception ex)
            {
                LogException(log, customerId, ex, "ForgotPassword");
                throw;
            }
        }



        public async Task<int> ForgotPIN(string customerId, string cardId)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var custInfo = await conn.QueryFirstOrDefaultAsync<CustInfo>(_commandText.GetForgotPIN,
                        new { CustomerID = customerId, IdentityCard = cardId });
                    if (string.IsNullOrEmpty(custInfo.CustID))
                    {
                        return 1;    // Mat khau ko dung
                    }
                    var confirmCode = EncryptHelper.EncriptMd5(customerId + EncryptHelper.RandomNumber(13)).Substring(0, 19);
                    // Cap nhat ma code gui mail
                    await conn.ExecuteAsync(_commandText.SendPINResetCode,
                        new { ConfirmCode = confirmCode, CustomerID = customerId, IdentityCard = cardId });
                    if (!string.IsNullOrEmpty(custInfo.Email))
                    {
                        try
                        {
                            var accountInfos = new Dictionary<string, string>
                                    {
                                        {"{CustName}", custInfo.CustName},
                                        {"{CustId}", custInfo.CustID},
                                        {"{CardId}",custInfo.CardId},
                                        {"{Code}", confirmCode}
                                    };
                            MailHelper.SendForgetPINMail(accountInfos, custInfo.Email);
                        }
                        catch (Exception ex)
                        {
                            LogException(log, customerId, ex, "ForgotPIN");
                            throw;
                        }
                    }
                    return 0;
                });
            }
            catch (Exception ex)
            {
                LogException(log, customerId, ex, "ForgotPIN");
                throw;
            }
        }




        public async Task<int> VerifyAccount(string customerId, string email)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var custInfo = await conn.QueryFirstOrDefaultAsync<CustInfo>(_commandText.GetVerifyAccount,
                        new { CustomerID = customerId, Email = email });
                    if (string.IsNullOrEmpty(custInfo?.CustID))
                    {
                        return 1;    // Mã khách hàng & Email không đúng
                    }
                    var confirmCode = EncryptHelper.EncriptMd5(customerId + EncryptHelper.RandomNumber(13)).Substring(0, 19);
                    // Cap nhat ma code gui mail
                    await conn.ExecuteAsync(_commandText.SendConfirmCode,
                        new { ConfirmCode = confirmCode, CustomerID = customerId, Email = email });
                    if (!string.IsNullOrEmpty(custInfo.Email))
                    {
                        try
                        {
                            var accountInfos = new Dictionary<string, string>
                                    {
                                        {"{CustName}", custInfo.CustName},
                                        {"{CustId}", custInfo.CustID},
                                        {"{CardId}",custInfo.CardId},
                                        {"{Code}", confirmCode}
                                    };
                            MailHelper.SendVerifyAccountMail(accountInfos, custInfo.Email);
                        }
                        catch (Exception ex)
                        {
                            LogException(log, customerId, ex, "VerifyAccount");
                            throw;
                        }
                    }
                    return 0;
                });
            }
            catch (Exception ex)
            {
                LogException(log, customerId, ex, "VerifyAccount");
                throw;
            }
        }





        public async Task<int> SendConfirmCode(string confirmCode)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var custInfo = await conn.QueryFirstOrDefaultAsync<CustInfo>(_commandText.CheckConfirmCode,
                        new {ConfirmCode = confirmCode });

                    var custInfoPIN = await conn.QueryFirstOrDefaultAsync<CustInfo>(_commandText.CheckConfirmCodePIN,
                        new {ConfirmCode = confirmCode });
                    if (custInfo==null && custInfoPIN==null)
                    {
                        return 1;    // Khong co thong tin khach hang
                    }
                    
                    // Reset password
                    if (custInfo != null)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(custInfo.Email))
                            {
                                var newPass = EncryptHelper.RandomNumber(8);
                                var _newPass = EncryptHelper.MD5EncodePassword(newPass);
                                var _confirmCode =
                                    EncryptHelper.EncriptMd5(custInfo.CustID + EncryptHelper.RandomNumber(13))
                                        .Substring(0, 19);
                                var _modifiedDatePass = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                                await conn.ExecuteAsync(_commandText.UpdateNewPass,
                                    new
                                    {
                                        CustomerID = custInfo.CustID,
                                        passWord = _newPass,
                                        ConfirmCode = _confirmCode,
                                        modifiedDatePass = _modifiedDatePass
                                    });
                                var accountInfos = new Dictionary<string, string>
                                {
                                    {"{CustName}", custInfo.CustName},
                                    {"{CustId}", custInfo.CustID},
                                    {"{NewPass}", newPass},
                                    {"{EffDate}", DateTime.Now.ToString("dd/MM/yyyy")}
                                };
                                MailHelper.SendNewPassMail(accountInfos, custInfo.Email);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogException(log, "confirm-code", ex, "SendConfirmCode");
                            throw;
                        }
                    }

                    if (custInfoPIN != null)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(custInfoPIN.Email))
                            {
                                var newPin = EncryptHelper.RandomNumber(6);
                                var _newPin = EncryptHelper.MD5EncodePassword(newPin);
                                var _pinConfirmCode =
                                    EncryptHelper.EncriptMd5(custInfoPIN.CustID + EncryptHelper.RandomNumber(13))
                                        .Substring(0, 19);
                                var _modifiedDatePin = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                                await conn.ExecuteAsync(_commandText.UpdateNewPin,
                                    new
                                    {
                                        CustomerID = custInfoPIN.CustID,
                                        pin = _newPin,
                                        ConfirmCode = _pinConfirmCode,
                                        modifiedDatePin = _modifiedDatePin
                                    });
                                var accountInfos = new Dictionary<string, string>
                                {
                                    {"{CustName}", custInfoPIN.CustName},
                                    {"{CustId}", custInfoPIN.CustID},
                                    {"{NewPin}", newPin},
                                    {"{EffDate}", DateTime.Now.ToString("dd/MM/yyyy")}
                                };
                                MailHelper.SendNewPINMail(accountInfos, custInfoPIN.Email);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogException(log, "confirm-code", ex, "SendPINConfirmCode");
                            throw;
                        }
                    }

                    return 0;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "confirm-code", ex, "SendConfirmCode");
                throw;
            }
        }


        public async Task<int> ChangePin(string customerId, string newPin, string oldPin)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var custcode = await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetCustomerIdByPin
                        , new { CustomerID = customerId, Pin = oldPin });

                    if (string.IsNullOrEmpty(custcode))
                        return 1;       // Ma Pin ko dung

                    // Cap nhap Pin
                    await conn.ExecuteAsync(_commandText.UpdatePinOfCustomer, new { CustomerID = customerId, Pin = newPin });

                    return 0;
                });
            }
            catch (Exception ex)
            {
                LogException(log, customerId, ex, "ChangePin");
                throw;
            }
        }

        public async Task<int> SendSms(string custCode, string message, string serviceCode)
        {
            try
            {
                var mobilePhoneData = await WithInnoConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<MobilePhoneData>(_commandText.GetCellPhone, new { custCode = custCode }));

                var mobilePhone = string.Empty;
                if (mobilePhoneData != null)
                {
                    if (!string.IsNullOrEmpty(mobilePhoneData.Mobile))
                        mobilePhone = mobilePhoneData.Mobile;

                    //else if (!string.IsNullOrEmpty(mobilePhoneData.Mobile_02))
                    //    mobilePhone = mobilePhoneData.Mobile_02;
                } 

                if (string.IsNullOrEmpty(mobilePhone))
                {
                    mobilePhone = await WithInnoConnection(async conn =>
                        await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetCustomerPhone, new { custCode = custCode }));
                }

                if (string.IsNullOrEmpty(mobilePhone))
                    return 1;

                if (!string.IsNullOrEmpty(serviceCode))
                {
                    await WithSystemNotificationConnection(async conn =>
                    {
                        var param = new DynamicParameters();
                        param.Add("@noi_dung",message);
                        param.Add("@so_dien_thoai",mobilePhone);
                        param.Add("@ma_dich_vu","SMS_OTP");
                        param.Add("@trang_thai",0);
                        param.Add("@smsid", dbType: DbType.Int32, direction: ParameterDirection.Output);
                        await conn.ExecuteAsync("TVSI_sHANG_DOI_SMS_INSERT_SMS_OTP",param,commandType: CommandType.StoredProcedure);
                    });
                }
                else
                {
                    await WithSystemNotificationConnection(async conn => await conn.ExecuteAsync("TVSI_sHANG_DOI_SMS_INSERT",
                        new { noi_dung = message, so_dien_thoai = mobilePhone, trang_thai = 0 }, commandType: CommandType.StoredProcedure));    
                }
                

                return 0;
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "SendSms");
                throw;
            }
        }

        public async Task<int> SendSms_02(string mobile, string message)
        {
            try
            {
                if (string.IsNullOrEmpty(mobile) || string.IsNullOrEmpty(message))
                {
                    return 0;
                }

                /*await WithSystemNotificationConnection(async conn => await conn.ExecuteAsync("TVSI_sHANG_DOI_SMS_INSERT",
                    new { noi_dung = message, so_dien_thoai = mobile, trang_thai = 0 }, commandType: CommandType.StoredProcedure));*/
                await WithSystemNotificationConnection(async conn =>
                {
                    var param = new DynamicParameters();
                    param.Add("@noi_dung",message);
                    param.Add("@so_dien_thoai",mobile);
                    param.Add("@ma_dich_vu","SMS_OTP");
                    param.Add("@trang_thai",0);
                    param.Add("@smsid", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    await conn.ExecuteAsync("TVSI_sHANG_DOI_SMS_INSERT_SMS_OTP",param,commandType: CommandType.StoredProcedure);
                });
                return 1;
            }
            catch (Exception ex)
            {
                LogException(log, "sms", ex, "SendSms_02");
                throw;
            }
        }


        public async Task<bool> SendOTPEmail(string otp, string custcode)
        {
            try
            {
                var keyandValues = new Dictionary<string, string>();

                if (custcode.Length >= 6)
                {
                    var email = await WithInnoConnection(async conn =>
                    {
                        var sql =
                            "SELECT ContactEmail Email FROM CUSTOMER WHERE CustomerID = @CustomerID";
                        return await conn.QueryFirstOrDefaultAsync<string>(sql, new
                        {
                            CustomerID = custcode.Substring(0, 6)
                        });
                    });

                    if (!string.IsNullOrEmpty(otp) && !string.IsNullOrEmpty(email))
                    {
                        keyandValues.Add("{OTPCode}", otp);
                        if (keyandValues.Count > 0)
                        {
                            MailHelper.SendOTPEmail(keyandValues, email);
                            return true;
                        }
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                LogException(log, "sendOTPEmail", ex, "SendOTPEmail");
                throw;
            }
        }

        public async Task<NotificationResult> GetNotificationList(int pageIndex, int pageSize,
            string accountNo, string lang)
        {
            try
            {
                var start = (pageIndex - 1) * pageSize;

                return await WithSystemNotificationConnection(async conn =>
                {
                    var sql = _commandText.GetNotificationList;

                    if (CommonConst.LANGUAGE_EN.Equals(lang))
                        sql = sql.Replace(" Message,", "COALESCE(MessageEn, Message) Message,");

                    var notificationData = await conn.QueryAsync<NotificationData>(sql, 
                            new { AccountNo = accountNo, Start = start, PageSize = pageSize});

                    var notificationCount = await conn.QueryFirstOrDefaultAsync<int>(_commandText.GetNumOfUnreadNotification, 
                        new { AccountNo = accountNo });

                    return new NotificationResult {NotificationCount = notificationCount,
                        NotificationDatas = notificationData};
                });

            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetNotificationList");
                throw;
            }
        }

        public async Task<int> SyncNotificationRead(List<long> idList, string accountNo)
        {
            try
            {
                return await WithSystemNotificationConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.SyncNotificationRead, new
                            {
                                MessageIDList = @idList, AccountNo = accountNo,
                                UpdatedBy = accountNo, UpdatedDate = DateTime.Now
                            });

                    return 1;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "SyncNotificationRead");
                throw;
            }
        }

        public async Task<SearchEventFilterResult> GetEventFilterData(string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                {
                   var bondCodeList = await conn.QueryAsync<string>(_commandText.GetBondFilterByAccount, new {AccountNo = accountNo});

                    var bondDataFilter = await conn.QueryAsync<EventFilterBondData>(_commandText.GetBondFilter, new {BondCodeList = bondCodeList });

                   var typeDataFilter = await conn.QueryAsync<EventFilterTypeData>(_commandText.GetEventTypeFilter);

                   return new SearchEventFilterResult
                          {
                              FilterBonds = bondDataFilter,
                              FilterTypes = typeDataFilter
                          };
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetEventFilterData");
                throw;
            }
        }

        public async Task<IEnumerable<EventListResult>> GetEventList(int pageIndex, int pageSize, string bondCode,
            int eventType, string accountNo)
        {
            try
            {
                var start = (pageIndex - 1) * pageSize;

                var sql = _commandText.GetEventList;
                sql = sql.Replace("{cond1}", !string.IsNullOrEmpty(bondCode) ? " and IssuerCode = @IssuerCode" : "");
                sql = sql.Replace("{cond2}", eventType > 0 ? " and EventType = @EventType" : "");

                return await WithTbmConnection(async conn =>
                {
                    var bondCodeList = await conn.QueryAsync<string>(_commandText.GetBondFilterByAccount, new { AccountNo = accountNo });

                    return await conn.QueryAsync<EventListResult>(sql,
                            new
                            {
                                IssuerCode = bondCode,
                                EventType = eventType,
                                BondCodeList = bondCodeList,
                                Start = start,
                                PageSize = pageSize
                            });
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetEventList");
                throw;
            }
        }

        public async Task<EventDetailResult> GetEventDetail(int eventId)
        {
            try
            {
                return await WithTbmConnection(async conn => await conn.QueryFirstAsync<EventDetailResult>(_commandText.GetEventDetail,
                    new { EventID = eventId }));
            }
            catch (Exception ex)
            {
                LogException(log, "system", ex, "GetEventDetail");
                throw;
            }
        }

        public async Task<DateTime> GetNextTradeDate()
        {
            try
            {
                return await WithEmsDBConnection(async conn =>
                {
                    // Chỉ trả về tài khoản đuôi 1
                    var data = await conn.QueryFirstOrDefaultAsync<DateTime>("TVSI_sSRV_NGAY_GIAO_DICH_GAN_NHAT_SAP_TOI", commandType: CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "system", ex, "GetNextTradeDate");
                throw;
            }
        }

        public async Task<DateTime> GetNextTradeDateByDate(DateTime date)
        {
            try
            {
                return await WithEmsDBConnection(async conn =>
                {
                    // Chỉ trả về tài khoản đuôi 1
                    var data = await conn.QueryFirstOrDefaultAsync<DateTime>("TVSI_sSRV_NGAY_GIAO_DICH_SAP_TOI_NGAY_KET_THUC", new { ngay_giao_dich = date }, commandType: CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "system", ex, "GetNextTradeDate");
                throw;
            }
        }

        public DateTime GetSystemDate()
        {
            try
            {
                using (var conn = new SqlConnection(_emsdbConnectionString))
                {
                    var data = conn.QueryFirst<DateTime>("TVSI_sST_GET_NGAY_HE_THONG",
                        new
                        {
                        },
                        commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                }
            }
            catch (Exception ex)
            {
                LogException(log, "system", ex, "GetAvaiableAdvanceInfo");
                return DateTime.Today;
            }
        }

        public async Task<string> GetSBAReferNo()
        {
            try
            {
                return await WithIpgDBConnection(async conn =>
                {
                    // Chỉ trả về tài khoản đuôi 1
                    var data = await conn.QueryFirstOrDefaultAsync<string>("TVSI_sST_GET_STT_NEW", new { ma_he_thong = "SBA" }, commandType: CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "system", ex, "GetNextTradeDate");
                throw;
            }
        }

        public async Task<int> InactiveAccount(string customerId)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var retData = await conn.ExecuteAsync(_commandText.InactiveCustomer, new
                    {
                        CustomerID = customerId
                    });

                    return retData;
                });
            }
            catch (Exception ex)
            {
                LogException(log, customerId, ex, "InactiveAccount");
                throw;
            }
        }

        public async Task<int> LoginLdap(string userId, string password)
        {
            using (var conn = new SqlConnection(_emsdbConnectionString))
            {
                var sql = @"SELECT user_domain FROM TVSI_THONG_TIN_TRUY_CAP_CHI_TIET 
                                WHERE id_he_thong = @userId AND trang_thai=1";

                var userDomain = await conn.QueryFirstOrDefaultAsync<string>(sql, new { userId = userId });
                LogInfo(log, userId, "userDomain=" + userDomain);
                if (string.IsNullOrEmpty(userDomain))
                    return -1;

                var isLoginLdapSuccess = IsLdapSuccess(userDomain, password);
                LogInfo(log, userId, "isLoginLdapSuccess=" + isLoginLdapSuccess);
                if (!isLoginLdapSuccess)
                {
                    // Check EMS
                    sql = @"SELECT A.ten_dang_nhap as UserName, B.ho_va_ten as FullName,
                                   B.ma_nhan_vien_quan_ly as SaleID, B.cap_quan_ly as Level,
                                   A.cap_do_he_thong_id as SystemID, C.ma_chi_nhanh as BranchID,
                                   A.nhan_su_id as UserID, C.ma_cap_do_he_thong as SystemCode
                                FROM TVSI_THONG_TIN_TRUY_CAP A 
                                INNER JOIN TVSI_NHAN_SU B on A.nhan_su_id = B.nhan_su_id 
                                LEFT JOIN TVSI_CAP_DO_HE_THONG C on A.cap_do_he_thong_id = C.cap_do_he_thong_id 
                                WHERE A.ten_dang_nhap = @ten_dang_nhap AND a.mat_khau_dang_nhap = @mat_khau_dang_nhap AND a.trang_thai = 1";
                    var userEmsInfo = await conn.QueryFirstOrDefaultAsync<dynamic>(sql,
                        new
                        {
                            ten_dang_nhap = userId,
                            mat_khau_dang_nhap = EncryptHelper.EncriptMd5(userId + password)
                        });
                    isLoginLdapSuccess = userEmsInfo != null;
                    LogInfo(log, userId, "isLoginLdapSuccess=" + isLoginLdapSuccess);
                }

                if (isLoginLdapSuccess)
                {
                    var roleList = await conn.QueryAsync<TVSI_sDANH_MUC_QUYEN_ByTenTruyCap_Result>(
                        "TVSI_sDANH_MUC_QUYEN_SU_DUNG_ByTenTruyCap",
                        new { ten_dang_nhap = userId }, commandType: CommandType.StoredProcedure);
                    var filterFunctionList = new List<string> { "XTR-VIEW", "XTR-ORDER", "XTR-FULL" };
                    foreach (var roleItem in roleList)
                    {
                        if (filterFunctionList.Contains(roleItem.ma_chuc_nang))
                        {
                            if ("XTR-FULL".Equals(roleItem.ma_chuc_nang))
                                return 3;

                            if ("XTR-ORDER".Equals(roleItem.ma_chuc_nang))
                                return 2;

                            if ("XTR-VIEW".Equals(roleItem.ma_chuc_nang))
                                return 1;
                        }
                    }

                    return -2;
                }
            }


            return -1;
        }

        public string GetLdapUserName(string userId)
        {
            using (var conn = new SqlConnection(_emsdbConnectionString))
            {
                var sql = @"SELECT B.ho_va_ten as FullName
                                FROM TVSI_THONG_TIN_TRUY_CAP A 
                                INNER JOIN TVSI_NHAN_SU B on A.nhan_su_id = B.nhan_su_id 
                                WHERE A.ten_dang_nhap = @ten_dang_nhap AND A.trang_thai = 1";

                return conn.QueryFirstOrDefault<string>(sql, new { ten_dang_nhap = userId });
            }
        }

        /// <summary>
        /// Check đăng nhập DOMAIN LDAP
        /// </summary>
        /// <param name="userName">UserName truy cập</param>
        /// <param name="password">Mật khẩu</param>
        /// <returns></returns>
        private bool IsLdapSuccess(string userName, string password)
        {
            var ldapServerIp = ConfigurationManager.AppSettings["LDAP_SERVER"] + "";
            var ldapConnection = new LdapConnection(new LdapDirectoryIdentifier(ldapServerIp));
            TimeSpan mytimeout = new TimeSpan(0, 0, 0, 1);
            try
            {
                ldapConnection.AuthType = AuthType.Negotiate;
                ldapConnection.AutoBind = false;
                ldapConnection.Timeout = mytimeout;
                ldapConnection.Credential = new NetworkCredential(userName, password);
                ldapConnection.Bind();
                return true;
            }
            catch (LdapException e)
            {
                return false;
            }
        }
        
        
        public async Task<IEnumerable<string>> GetBannerTvsiInfo()
        {
            try
            {
                var url = ConfigurationManager.AppSettings["WEBSITEPATH"];

                return await WithWebsiteDBConnection(async conn =>
                {
                    var sql = " SELECT '" +url +"' + c.FullPath FullPath " +
                              "FROM Banners a INNER JOIN Language_Banner b " +
                              "ON a.Id = B.BannerId INNER JOIN Media c " +
                              "ON B.mediaId = C.Id WHERE A.Type = 4 and A.DELETEDAT IS NULL " +
                              "ORDER BY A.CreatedDate DESC";
                    return  await conn.QueryAsync<string>(sql);
                });
            }
            catch (Exception ex)
            {
                LogException(log, "", ex, "GetBannerTvsiInfo");
                throw;
            }
        }
    }


}
