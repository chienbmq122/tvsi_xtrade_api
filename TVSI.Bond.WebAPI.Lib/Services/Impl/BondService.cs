﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models.Bond;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class BondService : BaseService, IBondService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(SystemService));

        public BondService()
        {
        }

        #region Sản phẩm trái phiếu

        /// <summary>
        /// Lấy thông tin sản phẩm trái phiếu của TVSI
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ProductInfoResult>> TVSI_sAPI_BM_PR_BondProductInfo(string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_PR_BondProductInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ProductInfoResult>(sql, new
                    {
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_PR_BondProductInfo");
                throw;
            }
        }

        public async Task<IEnumerable<OutrightBoardResult>> TVSI_sAPI_BM_OR_BondOutRightBoard(string accountNo, string bondCode, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_OR_BondOutRightBoard";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<OutrightBoardResult>(sql, new
                    {
                        AccountNo = accountNo,
                        BondCode = bondCode,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_OR_BondOutRightBoard");
                throw;
            }
        }

        public async Task<IEnumerable<BondInfoDetailResult>> TVSI_sAPI_BM_BI_BondInfo(string bondCode, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_BI_BondInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<BondInfoDetailResult>(sql, new
                    {
                        BondCode = bondCode,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_BI_BondInfo");
                throw;
            }
        }

        public async Task<IEnumerable<ProductTypeResult>> TVSI_sAPI_BM_PR_BondProductInfoByType(string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_PR_BondProductInfoByType";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ProductTypeResult>(sql, new
                    {
                        lang = lang,
                        Type = (int)CommonConst.BondProductType.Bond
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_PR_BondProductInfoByType");
                throw;
            }
        }

        public async Task<IEnumerable<ProductTermResult>> TVSI_sAPI_BM_IV_BondProductTermList(string accountNo, int productType, 
            decimal amount, DateTime tradedate, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_BondProductTermList";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ProductTermResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ProductType = productType,
                        Amount = amount,
                        TradeDate = tradedate,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_BondProductTermList");
                throw;
            }
        }

        public async Task<IEnumerable<ProductPayTermResult>> TVSI_sAPI_BM_IV_BondProductPayTermList(string accountNo, int productType, 
            decimal amount, DateTime tradeDate, int term, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_BondProductPayTermList";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ProductPayTermResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ProductType = productType,
                        Amount = amount,
                        TradeDate = tradeDate,
                        Term = term,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_BondProductPayTermList");
                throw;
            }
        }

        public async Task<IEnumerable<FindBondRequestResult>> TVSI_sAPI_BM_IV_FindBondRequest(string accountNo, int productType, decimal amount, DateTime tradeDate, int term, bool isPreSale, int payTerm, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_FindBondRequest";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<FindBondRequestResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ProductType = productType,
                        Amount = amount,
                        TradeDate = tradeDate,
                        Term = term,
                        IsPreSale = isPreSale == true? 1: 0,
                        PayTerm = payTerm,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_FindBondRequest");
                throw;
            }
        }

        public async Task<IEnumerable<CalBondInfo>> TVSI_sAPI_BM_IV_BondBuyCalBondRate(string accountNo, int productType, decimal amount, DateTime tradeDate, int term, bool isPreSale, int payTerm, string bondCode, DateTime sellDate, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_BondBuyCalBondRate";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<CalBondInfo>(sql, new
                    {
                        AccountNo = accountNo,
                        ProductType = productType,
                        Amount = amount,
                        TradeDate = tradeDate,
                        Term = term,
                        IsPreSale = isPreSale == true ? 1 : 0,
                        PayTerm = payTerm,
                        SubBondCode = bondCode,
                        SellDate = sellDate,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_BondBuyCalBondRate");
                throw;
            }
        }

        public async Task<IEnumerable<SellBeforeRateInfo>> TVSI_sAPI_BM_IV_GetMMSellBeforeRateInfos(string accountNo, string subBondCode, DateTime tradeDate, string redeemCode, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_GetMMSellBeforeRateInfos";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<SellBeforeRateInfo>(sql, new
                    {
                        AccountNo = accountNo,
                        SubBondCode = subBondCode,
                        SellDate = tradeDate,
                        RedeemCode = redeemCode,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_GetMMSellBeforeRateInfos");
                throw;
            }
        }

        public async Task<IEnumerable<RedeemCodeInfo>> TVSI_sAPI_BM_IV_CheckRedeemCode(string accountNo, string subBondCode, DateTime tradeDate, long volume, string redeemCode, int redeemType)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_CheckRedeemCode";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<RedeemCodeInfo>(sql, new
                    {
                        AccountNo = accountNo,
                        TradeDate = tradeDate,
                        SubBondCode = subBondCode,
                        Volume = volume,
                        RedeemCode = redeemCode,
                        RedeemType = redeemType
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_CheckRedeemCode");
                throw;
            }
        }

        public async Task<IEnumerable<CalCustBankInfo>> TVSI_sAPI_BM_IV_GetCustBankInfo(string custcode)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_GetCustBankInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<CalCustBankInfo>(sql, new
                    {
                        CustCode = custcode
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, custcode, ex, "TVSI_sAPI_BM_IV_GetCustBankInfo");
                throw;
            }
        }

        public async Task<IEnumerable<BuyDataResult>> TVSI_sAPI_BM_IV_BondValidateBuyInfo(string accountNo, string bondCode, string subBondCode, long volume, DateTime tradeDate,
            decimal rate, string redeemCode, DateTime sellDate, int custBankID, decimal cashBalance, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_BondConfirmBuyInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<BuyDataResult>(sql, new
                    {
                        AccountNo = accountNo,
                        BondCode = bondCode,
                        SubBondCode = subBondCode,
                        TradeDate = tradeDate,
                        Volume = volume,
                        Rate = rate,
                        RedeemCode = redeemCode,
                        SellDate = sellDate,
                        CustBankID = custBankID,
                        IsHardCopy = false,
                        CashBalance = cashBalance,
                        lang = lang,
                        Action = 0
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_BondConfirmBuyInfo");
                throw;
            }
        }

        public async Task<IEnumerable<ConfirmBuyInfoResult>> TVSI_sAPI_BM_IV_BondConfirmBuyInfo(string accountNo, string bondCode, string subBondCode, long volume, DateTime tradeDate,
            decimal rate, string redeemCode, DateTime sellDate, int custBankID, bool isHardCopy, decimal cashBalance, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_BondConfirmBuyInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ConfirmBuyInfoResult>(sql, new
                    {
                        AccountNo = accountNo,
                        BondCode = bondCode,
                        SubBondCode = subBondCode,
                        TradeDate = tradeDate,
                        Volume = volume,
                        Rate = rate,
                        RedeemCode = redeemCode,
                        SellDate = sellDate,
                        CustBankID = custBankID,
                        IsHardCopy = isHardCopy,
                        CashBalance = cashBalance,
                        lang = lang,
                        Action = 1
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_BondConfirmBuyInfo");
                throw;
            }
        }

        public async Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewContractBuyPrintInfo(string accountNo, string bondCode, string subBondCode, long volume, DateTime tradeDate,
            decimal rate, string redeemCode, DateTime sellDate, int custBankID, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_ViewContractBuyPrintInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<PrintCodeData>(sql, new
                    {
                        AccountNo = accountNo,
                        BondCode = bondCode,
                        SubBondCode = subBondCode,
                        TradeDate = tradeDate,
                        Volume = volume,
                        Rate = rate,
                        RedeemCode = redeemCode,
                        SellDate = sellDate,
                        CustBankID = custBankID,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_ViewContractBuyPrintInfo");
                throw;
            }
        }

        public async Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewConfirmationContractBuyPrintInfo(int requestOrderID, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_ViewConfirmationContractBuyPrintInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<PrintCodeData>(sql, new
                    {
                        RequestOrderID = requestOrderID,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_IV_ViewConfirmationContractBuyPrintInfo");
                throw;
            }
        }

        public async Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewConfirmationContractRollPrintInfo(int requestOrderID, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_ViewConfirmationContractRollPrintInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<PrintCodeData>(sql, new
                    {
                        RequestOrderID = requestOrderID,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_IV_ViewConfirmationContractRollPrintInfo");
                throw;
            }
        }

        public async Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewConfirmationContractSellPrintInfo(int requestOrderID, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_ViewConfirmationContractSellPrintInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<PrintCodeData>(sql, new
                    {
                        RequestOrderID = requestOrderID,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_IV_ViewConfirmationContractSellPrintInfo");
                throw;
            }
        }

        public async Task<IEnumerable<FindSellContractInfoResult>> TVSI_sAPI_BM_IV_FindSellContractInfo(string accountNo, string contractNo, DateTime sellDate)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_FindSellContractInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<FindSellContractInfoResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ContractNo = contractNo,
                        SellDate = sellDate
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_FindSellContractInfo");
                throw;
            }
        }

        public async Task<IEnumerable<CalcSellContractInfoResult>> TVSI_sAPI_BM_IV_CalcSellContractInfo(string accountNo, string contractNo, DateTime sellDate, long volume, string redeemCode)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_CalcSellContractInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<CalcSellContractInfoResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ContractNo = contractNo,
                        SellDate = sellDate,
                        Volume = volume,
                        RedeemCode = redeemCode
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_CalcSellContractInfo");
                throw;
            }
        }

        public async Task<IEnumerable<ValidateSellInfoResult>> TVSI_sAPI_BM_IV_BondValidateSellInfo(string accountNo, string contractNo, DateTime sellDate, long volume, string redeemCode, int custBankID, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_BondConfirmSellInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ValidateSellInfoResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ContractNo = contractNo,
                        SellDate = sellDate,
                        Volume = volume,
                        RedeemCode = redeemCode,
                        CustBankID = custBankID,
                        IsHardCopy = false,
                        lang = lang,
                        Action = 0
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_BondConfirmSellInfo");
                throw;
            }
        }

        public async Task<IEnumerable<ConfirmSellInfoResult>> TVSI_sAPI_BM_IV_BondConfirmSellInfo(string accountNo, string contractNo, DateTime sellDate, long volume, string redeemCode, int custBankID, bool isHardCopy, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_BondConfirmSellInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ConfirmSellInfoResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ContractNo = contractNo,
                        SellDate = sellDate,
                        Volume = volume,
                        RedeemCode = redeemCode,
                        CustBankID = custBankID,
                        IsHardCopy = isHardCopy,
                        lang = lang,
                        Action = 1
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_BondConfirmSellInfo");
                throw;
            }
        }

        public async Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewContractSellPrintInfo(string accountNo, string contractNo, DateTime sellDate, long volume, string redeemCode, int custBankID, string lang)

        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_ViewContractSellPrintInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<PrintCodeData>(sql, new
                    {
                        AccountNo = accountNo,
                        ContractNo = contractNo,
                        SellDate = sellDate,
                        Volume = volume,
                        RedeemCode = redeemCode,
                        CustBankID = custBankID,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_ViewContractSellPrintInfo");
                throw;
            }
        }

        public async Task<IEnumerable<FindRollContractInfoResult>> TVSI_sAPI_BM_IV_FindRollContractInfo(string accountNo, string contractNo)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_FindRollContractInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<FindRollContractInfoResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ContractNo = contractNo
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_FindRollContractInfo");
                throw;
            }
        }

        public async Task<IEnumerable<CalcRollContractInfoResult>> TVSI_sAPI_BM_IV_CalcRollContractInfo(string accountNo, string contractNo, string redeemCode)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_CalcRollContractInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<CalcRollContractInfoResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ContractNo = contractNo,
                        RedeemCode = redeemCode
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_CalcRollContractInfo");
                throw;
            }
        }

        public async Task<IEnumerable<ValidateRollInfoResult>> TVSI_sAPI_BM_IV_BondValidateRollInfo(string accountNo, string contractNo, string redeemCode, int custBankID, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_ConfirmRollContractInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ValidateRollInfoResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ContractNo = contractNo,
                        RedeemCode = redeemCode,
                        CustBankID = custBankID,
                        IsHardCopy = false,
                        lang = lang,
                        Action = 0
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_ConfirmRollContractInfo");
                throw;
            }
        }

        public async Task<IEnumerable<ConfirmRollInfoResult>> TVSI_sAPI_BM_IV_BondConfirmRollInfo(string accountNo, string contractNo, string redeemCode, int custBankID, bool isHardCopy, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_ConfirmRollContractInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ConfirmRollInfoResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ContractNo = contractNo,
                        RedeemCode = redeemCode,
                        CustBankID = custBankID,
                        IsHardCopy = isHardCopy,
                        lang = lang,
                        Action = 1
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_ConfirmRollContractInfo");
                throw;
            }
        }

        public async Task<IEnumerable<PrintCodeData>> TVSI_sAPI_BM_IV_ViewContractRollPrintInfo(string accountNo, string contractNo, string redeemCode, int custBankID, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_ViewContractRollPrintInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<PrintCodeData>(sql, new
                    {
                        AccountNo = accountNo,
                        ContractNo = contractNo,
                        RedeemCode = redeemCode,
                        CustBankID = custBankID,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_ViewContractRollPrintInfo");
                throw;
            }
        }

        #endregion

        #region Môi giới trái phiếu (MM)

        public async Task<IEnumerable<ProductInfoResult>> TVSI_sAPI_BM_PR_MMProductInfo(string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_PR_MMProductInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ProductInfoResult>(sql, new
                    {
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_PR_MMProductInfo");
                throw;
            }
        }

        public async Task<IEnumerable<ProductTypeResult>> TVSI_sAPI_BM_PR_MMProductInfoByType(string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_PR_BondProductInfoByType";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ProductTypeResult>(sql, new
                    {
                        lang = lang,
                        Type = (int)CommonConst.BondProductType.MM
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_PR_MMProductInfoByType");
                throw;
            }
        }

        public async Task<IEnumerable<ProductTermResult>> TVSI_sAPI_BM_IV_MMProductTermList(string accountNo, int productType,
           decimal amount, DateTime tradedate, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_BondProductTermList";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ProductTermResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ProductType = (int)CommonConst.ProductType.MM,
                        Amount = amount,
                        TradeDate = tradedate,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_MMProductTermList");
                throw;
            }
        }

        public async Task<IEnumerable<FindBondRequestResult>> TVSI_sAPI_BM_IV_FindMMRequest(string accountNo, int productType, decimal amount, DateTime tradeDate, int term, bool isPreSale, int payTerm, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_FindBondRequest";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<FindBondRequestResult>(sql, new
                    {
                        AccountNo = accountNo,
                        ProductType = CommonConst.ProductType.MM,
                        Amount = amount,
                        TradeDate = tradeDate,
                        Term = term,
                        IsPreSale = isPreSale == true ? 1 : 0,
                        PayTerm = payTerm,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_FindMMRequest");
                throw;
            }
        }

        #endregion

        public async Task<IEnumerable<CustBankContract_ActiveContractModel>> TVSI_sAPI_BM_IV_CustBankContract_GetActiveContract(string accountNo, int bondTypeGroup, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_CustBankContract_GetActiveContract";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<CustBankContract_ActiveContractModel>(sql, new
                    {
                        AccountNo = accountNo,
                        BondTypeGroup = bondTypeGroup,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_CustBankContract_GetActiveContract");
                throw;
            }
        }

        public async Task<IEnumerable<CustBankContract_ConfirmResult>> TVSI_sAPI_BM_IV_CustBankContract_Confirm(string contractIds, int custBankId, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_CustBankContract_Confirm";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<CustBankContract_ConfirmResult>(sql, new
                    {
                        ContractIds = contractIds,
                        CustBankId = custBankId,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_IV_CustBankContract_Confirm");
                throw;
            }
        }

        public async Task<IEnumerable<CustBankContract_GetListResult>> TVSI_sAPI_BM_IV_CustBankContract_GetList(string accountNo, DateTime fromDate, DateTime toDate, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_CustBankContract_GetList";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<CustBankContract_GetListResult>(sql, new
                    {
                        AccountNo = accountNo,
                        FromDate = fromDate,
                        ToDate = toDate,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "TVSI_sAPI_BM_IV_CustBankContract_GetList");
                throw;
            }
        }
        public async Task<IEnumerable<CustBankContract_GetDetailResult>> TVSI_sAPI_BM_IV_CustBankContract_GetDetail(int id, string lang)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_CustBankContract_GetDetail";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<CustBankContract_GetDetailResult>(sql, new
                    {
                        Id = id,
                        lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "GetAvaiableAdvanceInfo");
                throw;
            }
        }

        public async Task<DateTime> TVSI_sAPI_BM_IV_GetNextTradeDate()
        {
            try
            {
                // SQL Query
                var sql = " SELECT [dbo].[TVSI_fGET_NEXT_TRADE_DATE](@TradeDate)";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<DateTime>(sql, new
                    {
                        TradeDate = DateTime.Today
                    }, commandType: System.Data.CommandType.Text);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_IV_GetNextTradeDate");
                throw;
            }
        }

        public async Task<IEnumerable<IssuerGroupInfoResult>> TVSI_sAPI_BM_BI_IssueGroupInfo()
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_BI_IssueGroupInfo";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<IssuerGroupInfoResult>(sql, 
                            commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_BI_IssueGroupInfo");
                throw;
            }
        }

        public async Task<DateTime> TVSI_sAPI_BM_IV_GetMaturityDate(int productType, int term, DateTime tradeDate)
        {
            try
            {
                // SQL Query
                var sql = "dbo.TVSI_sAPI_BM_IV_GetMaturityDate";

                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<DateTime>(sql, new
                    {
                        ProductType = productType,
                        Term = term,
                        TradeDate = tradeDate
                    }, commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "TVSI_sAPI_BM_IV_GetMaturityDate");
                throw;
            }
        }
    }
}
