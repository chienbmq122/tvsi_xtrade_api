﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IBM.Data.DB2;
using log4net;
using TVSI.Bond.WebAPI.Lib.Infrastructure;
using TVSI.Bond.WebAPI.Lib.Models.Customer;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Lib.Models.AdvMoney;
using TVSI.WebServices.Entity.SBAGateway;
using TVSI.Utility;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;
using TVSI.Bond.WebAPI.Lib.Models.StockTransfer;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class StockTransferService : BaseService, IStockTransferService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(StockTransferService));
        public ISystemService _systemService;

        public StockTransferService(ISystemService systemService)
        {
            _systemService = systemService;
        }

        public decimal fGetDecimal(object obj)
        {
            decimal d = 0;
            if (decimal.TryParse(obj.ToString(), out d))
                return d;
            else
            {
                //không convert được
                return 0;
            }
        }

        public async Task<List<AvailStockTransferInfoResult>> GetAvailStockTransferList(string accountNo, string symbol, string purpose)
        {
            List<AvailStockTransferInfoResult> data = new List<AvailStockTransferInfoResult>();
            if (accountNo.Length == 7)
            {
                string strAccountType = accountNo.Substring(6, 1);
                var dataTemp = await GetTempStockTransfer(accountNo, symbol);
                if (strAccountType == "1")
                {
                    var stockBalance = await GetStockBalance(accountNo, symbol, purpose);
                    foreach (var item in stockBalance)
                    {
                        var temp = dataTemp.FirstOrDefault(x => x.ma_chung_khoan == item.SECSYMBOL && x.from_purpose == item.SECTYPE.ToString("00"));
                        var unit = item.AVAIVOLUME - (temp != null ? temp.khoi_luong : 0M);
                        data.Add(new AvailStockTransferInfoResult
                        {
                            AccountNo = accountNo,
                            AvgPrice = item.AVGPRICE,
                            Purpose = item.SECTYPE.ToString("00"),
                            ShareCode = item.SECSYMBOL.Trim(),
                            Unit = unit >= 0 ? unit : 0M
                        });
                    }
                }
                else if (strAccountType == "6")
                {
                    var stockBalance = await GetStockBalance(accountNo, symbol, purpose);
                    var stockMargin = await GetStockBalanceMargin(accountNo, symbol, purpose);
                    foreach(var item in stockMargin)
                    {
                        var temp = dataTemp.FirstOrDefault(x => x.ma_chung_khoan == item.STOCK_SYM && x.from_purpose == item.STOCK_TYPE.ToString("00"));
                        var waitSell = stockBalance.FirstOrDefault(x => x.SECSYMBOL == item.STOCK_SYM);
                        var amount = item.ACTUAL_VOL - (waitSell != null ? waitSell.SELLORDVOLUME : 0M);
                        var price = item.LASTSALE;
                        var unit = item.ACTUAL_VOL;
                        if (item.EE <= 0)
                            unit = 0;
                        else if (item.EE > 0 && item.TODAY_MARGIN == 100)
                            unit = amount;
                        else if (item.EE > 0 && item.TODAY_MARGIN > 0 && item.TODAY_MARGIN < 100)
                        {
                            var so_du = (item.EE / (1 - (item.TODAY_MARGIN / 100))) / price;
                            if (so_du > amount)
                                so_du = amount;
                            unit = so_du - (temp != null ? temp.khoi_luong : 0M);
                        }
                        else
                            unit = 0;
                        data.Add(new AvailStockTransferInfoResult
                        {
                            AccountNo = accountNo,
                            AvgPrice = item.AVGPRICE,
                            Purpose = item.STOCK_TYPE.ToString("00"),
                            ShareCode = item.STOCK_SYM.Trim(),
                            Unit = unit >= 0 ? decimal.Floor(unit) : 0M
                        });
                    }
                }
            }
            return data;
        }

        public async Task<List<StockBalanceDB2>> GetStockBalance(string account, string sharecode, string purpose)
        {
            //var sql = string.Format("SELECT A.ACCOUNTNO, TRIM(SECSYMBOL) SECSYMBOL, SECTYPE, STARTVOLUME, AVAIVOLUME, AVGPRICE, BRANCHID, SELLORDVOLUME - SELLCANCEL - SELLVOLUME SELLORDVOLUME " +
            //     "FROM CUSTPOSITIONTAB A LEFT JOIN ACCOUNTTAB B " +
            //     "ON A.ACCOUNTNO = B.ACCOUNTNO " +
            //     "WHERE A.ACCOUNTNO = '{0}' AND SECTYPE IN (2, 30) AND LENGTH(TRIM(SECSYMBOL)) <= 3 AND AVAIVOLUME > 0 ", account, purpose);

            var sql = @"SELECT A.ACCOUNTNO, TRIM(A.SECSYMBOL) SECSYMBOL, A.SECTYPE, STARTVOLUME, AVAIVOLUME, A.AVGPRICE, BRANCHID, SELLORDVOLUME - SELLCANCEL - SELLVOLUME SELLORDVOLUME
                 FROM CUSTPOSITIONTAB A LEFT JOIN ACCOUNTTAB B           
                 ON A.ACCOUNTNO = B.ACCOUNTNO
                 LEFT JOIN SECCTRLTAB C ON A.SECSYMBOL = C.SECSYMBOL
                 WHERE A.ACCOUNTNO = '{0}' AND A.SECTYPE IN (2, 30) AND A.AVAIVOLUME > 0
                 AND C.COMMSECTYPE <> 'H'";
            sql = string.Format(sql, account, purpose);

            if (sharecode.Trim().Length > 0)
                sql = string.Format("{0} AND A.SECSYMBOL = '{1}'", sql, sharecode);

            try
            {
                using (var conn = new DB2Connection(_fisdbConnectionString))
                {
                    var balanceInfo = conn.Query<StockBalanceDB2>(sql, commandType: System.Data.CommandType.Text).ToList();
                    if (string.IsNullOrWhiteSpace(purpose))
                        return balanceInfo;
                    else
                    {
                        var type = Functions.GetInt(purpose);
                        return balanceInfo.Where(x => x.SECTYPE == type).ToList();
                    }
                };
            }
            catch (Exception ex)
            {
                LogException(log, account, ex, "GetStockBalance");
                return new List<StockBalanceDB2>();
            }
        }

        public async Task<List<TempStockTransfer>> GetTempStockTransfer(string account, string symbol)
        {
            try
            {
                using (var conn = new SqlConnection(_ipgdbConnectionString))
                {
                    var data = conn.Query<TempStockTransfer>("TVSI_sCHUYEN_CHUNG_KHOAN_LAY_SO_DU_TAM_RUT",
                        new
                        {
                            so_tai_khoan = account,
                            ma_chung_khoan = symbol,
                        },
                        commandType: System.Data.CommandType.StoredProcedure).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                LogException(log, account, ex, "GetTempStockTransfer");
                return new List<TempStockTransfer>();
            }
        }

        public async Task<List<StockBalanceMarginDB2>> GetStockBalanceMargin(string account, string sharecode, string purpose)
        {
            //var sql = string.Format("SELECT A.ACCOUNTNO, STOCK_TYPE, TRIM(A.STOCK_SYM) STOCK_SYM, A.LASTSALE, A.TODAY_MARGIN, A.ACTUAL_VOL, A.AVG_COST AVGPRICE, B.EE, C.BRANCHID " +
            //        " FROM CUSTPOSITIONTAB_CB A " +
            //        " LEFT JOIN ACCOUNTTAB_CB B " +
            //        " ON A.ACCOUNTNO = B.CUST_ID " +
            //        " LEFT JOIN ACCOUNTTAB C ON A.ACCOUNTNO = C.ACCOUNTNO " +
            //        " WHERE A.ACCOUNTNO = '{0}' " +
            //        " AND STOCK_TYPE IN (2, 30) AND LENGTH(TRIM(A.STOCK_SYM)) <= 3 AND A.ACTUAL_VOL > 0"
            //        , account, purpose);

            var sql = @"SELECT A.ACCOUNTNO, STOCK_TYPE, TRIM(A.STOCK_SYM) STOCK_SYM, A.LASTSALE, A.TODAY_MARGIN, A.ACTUAL_VOL, A.AVG_COST AVGPRICE, B.EE, C.BRANCHID
                    FROM CUSTPOSITIONTAB_CB A LEFT JOIN ACCOUNTTAB_CB B ON A.ACCOUNTNO = B.CUST_ID
                    LEFT JOIN ACCOUNTTAB C ON A.ACCOUNTNO = C.ACCOUNTNO
                    LEFT JOIN SECCTRLTAB D ON A.STOCK_SYM = D.SECSYMBOL 
                    WHERE A.ACCOUNTNO = '{0}' AND STOCK_TYPE IN (2, 30)  AND A.ACTUAL_VOL > 0
                    AND D.COMMSECTYPE <> 'H'";
            sql = string.Format(sql, account, purpose);

            if (sharecode.Trim().Length > 0)
                sql = string.Format("{0} AND A.STOCK_SYM = '{1}'", sql, sharecode);

            try
            {
                using (var conn = new DB2Connection(_fisdbConnectionString))
                {
                    var balanceInfo = conn.Query<StockBalanceMarginDB2>(sql, commandType: System.Data.CommandType.Text).ToList();
                    if (string.IsNullOrWhiteSpace(purpose))
                        return balanceInfo;
                    else
                    {
                        var type = Functions.GetInt(purpose);
                        return balanceInfo.Where(x => x.STOCK_TYPE == type).ToList();
                    }
                };
            }
            catch (Exception ex)
            {
                LogException(log, account, ex, "GetStockBalanceMargin");
                return new List<StockBalanceMarginDB2>();
            }
        }

        public async Task<List<StockTransferInfoResult>> GetStockTransferHist(string accountNo, DateTime fromDate, DateTime toDate, string lang)
        {
            try
            {
                using (var conn = new SqlConnection(_ipgdbConnectionString))
                {
                    return conn.Query<StockTransferInfoResult>("TVSI_sTF_GetStockTransferHist",
                    new
                    {
                        AccountNo = accountNo,
                        FromDate = fromDate.ToString("yyyyMMdd"),
                        ToDate = toDate.ToString("yyyyMMdd"),
                        Lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetCustAdvanceHist");
                return new List<StockTransferInfoResult>();
            }
        }

        public async Task<AddStockTransferResult> AddStockTransfer(AddStockTransferRequest model)
        {
            var data = new AddStockTransferResult
            {
                AccountNo = model.AccountNo,
                ToAccount = model.ToAccount,
                RetCode = "",
                Message = "",
                RefNo = "TVSI0001",
                SQE = -1,
                TradeDate = DateTime.Today.ToString("yyyyMMdd")
            };
            try
            {

                var stockInfo = await GetAvailStockTransferList(model.AccountNo, model.ShareCode, model.FromPurpose);
                if (stockInfo.ToList().Count > 0)
                {
                    //if (model.Unit > stockInfo.FirstOrDefault().Unit)
                    //{
                    //    data.RetCode = "999";
                    //    data.Message = "VUOT QUA SO DU CHUNG KHOAN";
                    //}
                    //else
                    //{
                        using (var conn = new SqlConnection(_ipgdbConnectionString))
                        {
                            var stockTransferInfoIPG = conn.QueryFirstOrDefault<StockTransferInfoIPG>("TVSI_sCHUYEN_CHUNG_KHOAN_INSERT",
                                new
                                {
                                    ngay_giao_dich = DateTime.Today.ToString("yyyyMMdd"),
                                    ma_chi_nhanh = "01",
                                    so_tai_khoan = model.AccountNo,
                                    ma_chung_khoan = model.ShareCode,
                                    khoi_luong = model.Unit,
                                    so_tai_khoan_nhan = model.ToAccount,
                                    from_purpose = model.FromPurpose,
                                    to_purpose = model.ToPurpose,
                                    gia_trung_binh = stockInfo.FirstOrDefault().AvgPrice * 1000,
                                    remark = model.Remark,
                                    ghi_chu = "",
                                    tvsi_trang_thai = "00",
                                    sba_trang_thai = "00",
                                    nguoi_tao = model.UserId
                                },
                                commandType: System.Data.CommandType.StoredProcedure);
                            if (stockTransferInfoIPG != null)
                            {
                                data.RetCode = "000";
                                data.Message = "SUCCESS";
                                data.RefNo = stockTransferInfoIPG.so_tham_chieu;
                                data.SQE = stockTransferInfoIPG.so_sequence;
                            }
                        }
                    //}
                }
                else
                {
                    data.RetCode = "999";
                    data.Message = "KHONG LAY DUOC SO DU CHUNG KHOAN";
                }
                return data;
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "AddStockTransfer");
                return data;
            }
        }
    }
}
