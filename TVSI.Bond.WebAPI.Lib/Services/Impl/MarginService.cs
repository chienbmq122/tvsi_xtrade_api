﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.MarginPackage;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{

    public class MarginService : BaseService, IMarginService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(SystemService));
        private IMarginCommandText _marginCommand;

        public MarginService(IMarginCommandText marginCommand)
        {
            _marginCommand = marginCommand;
        }

        /// <summary>
        /// Lấy thông tin gói dịch vụ của TVSI
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<MarginPackageResult>> MarginPackageList(MarginPackageRequest model)
        {
            try
            {
                // SQL Query
                var sql =
                    "select goi_dich_vuid MarginID,ten_goi_dich_vu MarginName,lai_suat InterestRate,ty_le_call CallRate from TVSI_GOI_DICH_VU_MARGIN where trang_thai=1";

                return await WithEmsDBConnection(async conn =>
                {
                    var data = await conn.QueryAsync<MarginPackageResult>(sql,
                        commandType: System.Data.CommandType.Text);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "margin", ex, "MarginPackageList");
                throw;
            }
        }

        public async Task<GetBuyPowerPackResponse> GetBuyPowerPackage(string namePack, string accountNo)
        {
            try
            {
                return await  WithDb2Connection(conn =>
                {
                    var sql =
                        @"SELECT A.ACCOUNTNO,A.MARGIN_GROUP,A_CB.EE EE_HIEN_TAI ,(A_CB.EE+LMV_NEXT-A_CB.LMV-(MR_NEXT-A_CB.MR))  AS SUC_MUA,(A_CB.EE+LMV_NEXT-A_CB.LMV-(MR_NEXT-A_CB.MR))  AS EE_GOI_MOI
,(A_CB.EQUITY+A.LMV_NEXT-A_CB.LMV) AS SUC_MUA_TOI_DA, LMV_NEXT
 FROM 
(
 SELECT ACCOUNTNO,MARGIN_GROUP,SUM(LMV) LMV_NEXT,SUM(MR) MR_NEXT FROM
    (
SELECT ACCOUNTNO,MARGIN_GROUP,STOCK_SYM,STOCK_TYPE,LMV,LMV AS MR FROM (
select C_CB.ACCOUNTNO,MT.MARGIN_GROUP,MT.MRGRATE,C_CB.STOCK_SYM,C_CB.STOCK_TYPE,
(min(C_CB.LASTSALE,COALESCE(MT.LIMIT_PRICE-C_CB.AVG_COST, C_CB.LASTSALE))) * (CASE WHEN C_CB.STOCK_TYPE=42 AND C_CB.AVG_COST=0 THEN 0
ELSE C_CB.ACTUAL_VOL END +COALESCE(DECODE(CT.SECTYPE, 2, CT.BUYVOLUME),0))*MT.PLEDGERATE/100 AS LMV
        from  CUSTPOSITIONTAB_CB C_CB
        INNER JOIN MARGINGRPTAB MT ON C_CB.STOCK_SYM=MT.SYMBOL
LEFT JOIN CUSTPOSITIONTAB CT ON C_CB.ACCOUNTNO=CT.ACCOUNTNO AND CT.SECSYMBOL=C_CB.STOCK_SYM AND CT.SECTYPE=2
        where C_CB.ACCOUNTNO=? AND MT.MARGIN_GROUP=? AND C_CB.ACTUAL_VOL >0 and C_CB.STOCK_TYPE IN (9,10) AND C_CB.STOCK_TYPE != 43
UNION ALL 
select C_CB.ACCOUNTNO,MT.MARGIN_GROUP,MT.MRGRATE,C_CB.STOCK_SYM,C_CB.STOCK_TYPE,
min(C_CB.LASTSALE,COALESCE(MT.LIMIT_PRICE, C_CB.LASTSALE)) * (CASE WHEN C_CB.STOCK_TYPE=42 AND C_CB.AVG_COST=0 THEN 0
ELSE C_CB.ACTUAL_VOL END +COALESCE(DECODE(CT.SECTYPE, 2, CT.BUYVOLUME),0))*MT.PLEDGERATE/100 AS LMV
        from  CUSTPOSITIONTAB_CB C_CB
        INNER JOIN MARGINGRPTAB MT ON C_CB.STOCK_SYM=MT.SYMBOL
LEFT JOIN CUSTPOSITIONTAB CT ON C_CB.ACCOUNTNO=CT.ACCOUNTNO AND CT.SECSYMBOL=C_CB.STOCK_SYM AND CT.SECTYPE=2
        where C_CB.ACCOUNTNO=? AND MT.MARGIN_GROUP=? AND C_CB.ACTUAL_VOL >0 and C_CB.STOCK_TYPE NOT IN (9,10) AND C_CB.STOCK_TYPE != 43
) A WHERE LMV >=0 AND STOCK_TYPE IN (9,10,12,92)
UNION ALL
SELECT ACCOUNTNO,MARGIN_GROUP,STOCK_SYM,STOCK_TYPE,LMV,LMV*MRGRATE/100 AS MR FROM (
select C_CB.ACCOUNTNO,MT.MARGIN_GROUP,MT.MRGRATE,C_CB.STOCK_SYM,C_CB.STOCK_TYPE,
(min(C_CB.LASTSALE,COALESCE(MT.LIMIT_PRICE-C_CB.AVG_COST, C_CB.LASTSALE))) * (CASE WHEN C_CB.STOCK_TYPE=42 AND C_CB.AVG_COST=0 THEN 0
ELSE C_CB.ACTUAL_VOL END +COALESCE(DECODE(CT.SECTYPE, 2, CT.BUYVOLUME),0))*MT.PLEDGERATE/100 AS LMV
        from  CUSTPOSITIONTAB_CB C_CB
        INNER JOIN MARGINGRPTAB MT ON C_CB.STOCK_SYM=MT.SYMBOL
LEFT JOIN CUSTPOSITIONTAB CT ON C_CB.ACCOUNTNO=CT.ACCOUNTNO AND CT.SECSYMBOL=C_CB.STOCK_SYM AND CT.SECTYPE=2
        where C_CB.ACCOUNTNO=? AND MT.MARGIN_GROUP=? AND C_CB.ACTUAL_VOL >0 and C_CB.STOCK_TYPE IN (9,10) AND C_CB.STOCK_TYPE != 43
UNION ALL 
select C_CB.ACCOUNTNO,MT.MARGIN_GROUP,MT.MRGRATE,C_CB.STOCK_SYM,C_CB.STOCK_TYPE,
min(C_CB.LASTSALE,COALESCE(MT.LIMIT_PRICE, C_CB.LASTSALE)) * (CASE WHEN C_CB.STOCK_TYPE=42 AND C_CB.AVG_COST=0 THEN 0
ELSE C_CB.ACTUAL_VOL END +COALESCE(DECODE(CT.SECTYPE, 2, CT.BUYVOLUME),0))*MT.PLEDGERATE/100 AS LMV
        from  CUSTPOSITIONTAB_CB C_CB
        INNER JOIN MARGINGRPTAB MT ON C_CB.STOCK_SYM=MT.SYMBOL
LEFT JOIN CUSTPOSITIONTAB CT ON C_CB.ACCOUNTNO=CT.ACCOUNTNO AND CT.SECSYMBOL=C_CB.STOCK_SYM AND CT.SECTYPE=2
        where C_CB.ACCOUNTNO=? AND MT.MARGIN_GROUP=? AND C_CB.ACTUAL_VOL >0 and C_CB.STOCK_TYPE NOT IN (9,10) AND C_CB.STOCK_TYPE != 43

) A WHERE LMV >=0 AND STOCK_TYPE NOT IN (9,10,12,92)
) GROUP BY ACCOUNTNO,MARGIN_GROUP
)A INNER JOIN ACCOUNTTAB_CB A_CB ON A.ACCOUNTNO=A_CB.CUST_ID";
                    
                    var userInfo = conn.Query<GetBuyPowerPackResponse>(sql, new
                    {
                        param1 = accountNo,
                        param2 = namePack,
                        param3 = accountNo,
                        param4 = namePack,
                        param5 = accountNo,
                        param6 = namePack,
                        param7 = accountNo,
                        param8 = namePack
                    }).FirstOrDefault();
                    if (userInfo == null)
                    {
                        userInfo = new GetBuyPowerPackResponse();
                        userInfo.ACCOUNTNO = accountNo;
                        userInfo.LMV_NEXT = "0";
                        userInfo.MARGIN_GROUP = namePack;
                        userInfo.SUC_MUA = "0";
                        userInfo.SUC_MUA_TOI_DA = "0";
                        userInfo.EE_GOI_MOI = "0";
                        userInfo.EE_HIEN_TAI = "0";     
                    }
                    return Task.FromResult(userInfo);
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetBuyPowerPackage");
                throw;
            }
        }

        public async Task<string> GetCountMargin(string namePack)
        {
            try
            {
                                   
               return await WithDb2Connection(  conn =>
                {
                    var sqlList = @"select SYMBOL from MARGINGRPTAB where MARGIN_GROUP = '"+namePack.Trim()+"' and PLEDGERATE > 0";

                    var data = conn.Query<string>(
                       sqlList);
                    var dataRet = data.Count() > 0 ? data.Count().ToString() : "0";
                    return Task.FromResult(dataRet);
                });

            }
            catch (Exception ex)
            {
                LogException(log, "margin", ex, "GetCountMargin");
                throw;
            }
        }

        

        /// <summary>
        /// Lấy thông tin chi tiết gói dịch vụ của TVSI
        /// </summary>
        /// <returns></returns>
        public async Task<MarginDetailPackageResult> MarginPackageDetails(string MarginName,string userID)
        {
            try
            {
                var accountNo = userID + "6";
                var acceptM = ConfigurationManager.AppSettings["AcceptMargin"];

                // SQL Query
                var sql = "select goi_dich_vuid MarginID,ten_goi_dich_vu MarginName,lai_suat InterestRate,ty_le_call CallRate,cap_nhat_suc_mua BuyCreditUpdate,so_ngay_mien_lai DaysInterestFree,ty_le_ky_quy DepositRate,ty_le_canh_bao AlertRate, ty_le_vay MarginRate from TVSI_GOI_DICH_VU_MARGIN where ten_goi_dich_vu=@ten_goi_dich_vu and trang_thai=1";
                return await WithEmsDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<MarginDetailPackageResult>(sql, new
                    {
                        ten_goi_dich_vu = MarginName
                    });
                    if (data != null)
                    {
                        if (!acceptM.Equals(data.MarginName))
                        {
                            var sqlConn =
                                @"select ten_goi_dich_vu MarginName,lai_suat InterestRate ,'50000' BuyCredit,ty_le_call CallRate,cap_nhat_suc_mua BuyCreditUpdate,so_ngay_mien_lai DaysInterestFree ,ty_le_ky_quy DepositRate,ty_le_duy_tri MaintainRate,ty_le_canh_bao AlertRate,ty_le_vay MarginRate,du_no_toi_da MaxLoan from TVSI_GOI_DICH_VU_MARGIN where ten_goi_dich_vu=@NamePack and trang_thai=1";
                            var userInfo = await conn.QueryFirstOrDefaultAsync<MarginDetailPackageResult>(sqlConn, new
                            {
                                NamePack = data.MarginName
                            });
                            double rateMin = 0;
                            if (double.Parse(userInfo.DepositRate) < double.Parse(userInfo.MaintainRate))
                                rateMin = double.Parse(userInfo.DepositRate);
                            else
                                rateMin = double.Parse(userInfo.MaintainRate);

                            var buycredit = await GetBuyPowerPackage(data.MarginName, accountNo.Trim());
                            var stockCount = await GetCountMargin(userInfo.MarginName);

                            if (buycredit != null)
                            {
                                data.BuyCredit = (
                                    double.Parse(GetBuyPowerPackage(data.MarginName,
                                        accountNo.Trim()).Result.SUC_MUA.Trim()) * 100 /
                                    double.Parse(userInfo.DepositRate)
                                    * 1000
                                ).ToString();

                                data.BuyCreditMax = ((double.Parse(GetBuyPowerPackage(
                                        data.MarginName.Trim(), accountNo.Trim()).Result.SUC_MUA_TOI_DA.Trim()) *
                                    100 / rateMin - double.Parse(GetBuyPowerPackage(data.MarginName.Trim(),
                                        accountNo.Trim()).Result.LMV_NEXT.Trim())) * 1000).ToString();
                            }

                            if (!string.IsNullOrEmpty(stockCount))
                            {
                                data.StockCount = await GetCountMargin(userInfo.MarginName);
                            }

                            return data;
                        }

                        data = new MarginDetailPackageResult()
                        {
                            MarginAccept = false
                        };
                    }
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, userID, ex, "MarginPackageDetails");
                throw;
            }
        }



        /// <summary>
        /// Lấy thông tin gói dịch vụ ưu đãi của TVSI
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PreferentialMarginListResult>> PreferentialMarginList(string lang)
        {
            try
            {
                // SQL Query
                var sql = "select goi_dich_vu_uu_daiID PrefMarginID, ten_goi_dich_vu_uu_dai PrefMarginName, mo_ta Note from TVSI_GOI_DICH_VU_UU_DAI_MARGIN where trang_thai = 1";
               
                return await WithEmsDBConnection(async conn =>
                {
                    var data = await conn.QueryAsync<PreferentialMarginListResult>(sql, commandType: System.Data.CommandType.Text);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, "margin", ex, "GetAvaiableAdvanceInfo");
                throw;
            }
        }


        /// <summary>
        /// Lấy thông tin chi tiết gói dịch vụ ưu đãi của TVSI
        /// </summary>
        /// <returns></returns>
        public async Task<PreferentialMarginDetailResult> PreferentialMarginDetails(string PrefMarginName)
        {
            try
            {
                // SQL Query 
                var sql =
                    "select goi_dich_vu_uu_daiID PrefMarginID, ten_goi_dich_vu_uu_dai PrefMarginName, lai_suat_toi_thieu InterestMin, ly_le_phi_toi_thieu FeeRateMin, phi_giao_dich_toi_thieu FeeTransactionMin,so_ngay_mien_lai DaysOfInterestFree, vong_quay_du_no_thang RotationDebtMonth, du_no_toi_da DebtMax, phi_dich_vu_goi FeeMargin, ty_le_vay_toi_da MarginRateMax, so_luong_ma_cho_vay StockCount, doi_tuong ObjectForLoan, mo_ta note from TVSI_GOI_DICH_VU_UU_DAI_MARGIN where trang_thai = 1 and ten_goi_dich_vu_uu_dai=@PrefMarginName";

                var acceptPrefer = ConfigurationManager.AppSettings["AcceptMarginPrefer"];

                if (!acceptPrefer.Equals(PrefMarginName))
                {
                    var data = await WithEmsDBConnection(async conn =>
                    {
                        return await conn.QueryFirstOrDefaultAsync<PreferentialMarginDetailResult>(sql, new
                        {
                            PrefMarginName = PrefMarginName
                        }, commandType: System.Data.CommandType.Text);
                    });
                    return data;
                }

                return new PreferentialMarginDetailResult()
                {
                    MarginAccept = false
                };
            }
            catch (Exception ex)
            {
                LogException(log, "margin", ex, "PreferentialMarginDetail");
                throw;
            }
        }

        public async Task<bool> InsertServicePreferMargin(InsertServicePreferRequest model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.CurrentPack) || model.CurrentPack.Contains("BASIC"))
                    model.CurrentPack = "Khong Dang Ky";
                
                var endDate = new DateTime();
                endDate = (DateTime) SqlDateTime.MaxValue;
                
                var regDate = DateTime.ParseExact(model.RegDate, "dd/MM/yyyy", CultureInfo.CurrentCulture);

                
                return await WithEmsDBConnection(async conn =>
                {
                   return await conn.ExecuteAsync(_marginCommand.InsertServicePreferMargin, new
                    {
                        so_tai_khoan = model.UserId + "6",
                        ngay_dang_ky = regDate,
                        ngay_ket_thuc = endDate,
                        goi_dich_vu_hien_tai = model.CurrentPack,
                        goi_dich_vu_moi = model.CurrentNew,
                        nguoi_tao = model.UserId,
                        ho_ten_khach_hang=model.FullName
                    }) > 0;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "InsertServicePreferMargin");
                throw;
            }
        }

        public async Task<bool> UpdateServicePackPreferMargin(UpdateServicePreferRequest model)
        {
            try
            {
                return await WithEmsDBConnection(async conn =>
                {
                    var userInfo = await conn.QueryFirstOrDefaultAsync<UpdateServicePreferResponse>(_marginCommand.GetInfoPreferById, new
                    {
                        goi_dich_vu_uu_daiid = model.PackPreferID
                    });
                    if (userInfo.Status == 0)
                    {
                        return await conn.ExecuteAsync(_marginCommand.UpdateServicePreferMargin, new
                        {
                            goi_dich_vu_uu_daiid = model.PackPreferID,
                            trang_thai = model.Status 
                        }) > 0;
                    }
                    return false;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "UpdateServicePackPreferMargin");
                throw;
            }
            
            
        }

        public async Task<bool> UpdateServicePackMargin(UpdateServicePackRequest model)
        {
            try
            {
                return await WithEmsDBConnection(async conn =>
                {
                    var userInfo = await conn.QueryFirstOrDefaultAsync<UpdateServiceResponse>(_marginCommand.GetInfoServiceById,new
                    {
                        goi_dich_vuid = model.PackID
                    });
                    if (userInfo.Status == 0)
                    {
                        return await conn.ExecuteAsync(_marginCommand.UpdateServiceMargin, new
                        {
                            goi_dich_vuid= model.PackID,
                            trang_thai= model.Status  
                        }) > 0;
                    }
                    return false;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "UpdateServicePackMargin");
                throw;
            }
        }

        public async Task<ServicePreferResult> GetServicePerferByAccontNo(BaseRequest model)
        {
            try
            {
                return await WithIpgDBConnection(async conn => await conn.QueryFirstOrDefaultAsync<ServicePreferResult>(_marginCommand.GetServicePreferByAccountNo,new
                {
                    ma_khach_hang = model.AccountNo
                }));
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "GetServicePerferByAccontNo");
                throw;
            }
        }

        public async Task<bool> InsertServiceMargin(InsertServiceRequest model)
        {
            try
            {

                if (string.IsNullOrEmpty(model.CurrentPack) || model.CurrentPack.Contains("BASIC"))
                    model.CurrentPack = "BASIC";

                var endDate = new DateTime();
                endDate = (DateTime) SqlDateTime.MaxValue;

                var regDate = DateTime.ParseExact(model.RegDate, "dd/MM/yyyy", CultureInfo.CurrentCulture);

                return await WithEmsDBConnection(async conn =>
                {
                    return await conn.ExecuteAsync(_marginCommand.InsertServiceMargin, new
                    {
                        so_tai_khoan = model.UserId + "6",
                        ho_ten_khach_hang = model.FullName,
                        ngay_dang_ky = regDate,
                        ngay_ket_thuc = endDate,
                        goi_dich_vu_hien_tai = model.CurrentPack,
                        goi_dich_vu_moi = model.CurrentNew,
                        nguoi_tao = model.UserId 
                    }) > 0;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "InsertServiceMargin");
                throw;
            }
        }

        public async Task<ServiceResult> GetServiceByAccontNo(BaseRequest model)
        {
            try
            {
                var userInfo = await WithDb2Connection(  conn =>
                {
                    var data =  conn.QueryFirstOrDefault<ServiceResult>(
                        _marginCommand.GetServiceMarginByAccountNo, new
                        {
                            @AccountNo = model.UserId + "6"
                        });
                    return Task.FromResult(data);
                });
                
                if (userInfo != null)
                {
                    var sql = @"select goi_dich_vuid PackID,ten_goi_dich_vu PackName,
       lai_suat Rate,ty_le_call CallMargin,cap_nhat_suc_mua UpdateBuying,so_ngay_mien_lai DateFree,ty_le_ky_quy MarginRate,ty_le_canh_bao MarginAlert,ty_le_vay MarginLoan from TVSI_GOI_DICH_VU_MARGIN where ten_goi_dich_vu=@PackName and trang_thai=1";
                    var infoPack = await WithEmsDBConnection(async infoback =>
                        await infoback.QueryFirstOrDefaultAsync<ServicePackMargin>(sql, new
                        {
                            PackName = userInfo.CurrentPack
                        }));
                    userInfo.Rate = infoPack.Rate;
                    userInfo.UpdateBuying = infoPack.UpdateBuying;
                    userInfo.DateFree = infoPack.DateFree;
                    userInfo.MarginRate = infoPack.MarginRate;
                    userInfo.MarginAlert = infoPack.MarginAlert;
                    userInfo.MarginLoan = infoPack.MarginLoan;
                    var sqlList = @"select MARGIN_GROUP,SYMBOL,MRGRATE,PLEDGERATE,CANBUY,CANSELL,DEFAULT,LIMIT_PRICE from MARGINGRPTAB where MARGIN_GROUP=@servicePack and PLEDGERATE > 0";
                   
                   var dataStocks =   await WithDb2Connection( conn =>
                    {
                        var data =  conn.QueryFirstOrDefault<ServiceResult>(
                            _marginCommand.GetServiceMarginByAccountNo, new
                            {
                                @acocuntNo = model.UserId + "6"
                            });
                        return Task.FromResult(data);
                    });
                   

                    return userInfo;   
                }

                return null;

            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "GetServiceByAccontNo");
                throw;
            }
        }

        public async Task<GetServicePreferHistResult> GetServicePreferHist(BaseRequest model)
        {
            try
            {
                return await WithEmsDBConnection(async conn =>
                {
                    var data = new GetServicePreferHistResult();

                    var accountNo = model.UserId + "6";
                    var dataHist = await conn.QueryAsync<GetServicePreferHist>(_marginCommand.GetServicePreferHist
                        , new
                        {
                            @accountNo = accountNo
                        });
                    foreach (var value in dataHist)
                    {
                        if (value.Status == 1)
                        {
                            value.StatusName = "Thành công";
                        }
                        if (value.Status == -1)
                        {
                            value.StatusName = "Từ chối";
                            
                        }
                        if (value.Status == 0)
                        {
                            value.StatusName = "Tạo mới";
                        }
                        if (value.Status == 99)
                        {
                            value.StatusName = "Xóa";

                        }
                        if (value.Status == -2)
                        {
                            value.StatusName = "Đã hủy";

                        }
                        if (value.Status == 5)
                        {
                            value.StatusName = "Chờ xác nhận hủy";
                        }
                        if (value.Status == -3)
                        {
                            value.StatusName = "Ngừng sử dụng";
                        }
                    }
                    data.MarginList = dataHist.ToList();

                    if (dataHist.Any())
                        return new GetServicePreferHistResult()
                        {
                            AccountNo = accountNo,
                            MarginList = data.MarginList
                        };
                    
                    return null;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "GetServicePreferHist");
                throw;
            }
        }

        public async Task<GetServiceHistResult> GetServiceHist(BaseRequest model)
        {
            try
            {
                var accountNo = model.UserId + "6";
                var data = new GetServiceHistResult();
                return await WithEmsDBConnection(async conn =>
                {
                    var dataHist = await conn.QueryAsync<GetServiceHist>(_marginCommand.GetServiceHist
                        , new
                        {
                            @accountNo = accountNo
                        });
                    foreach (var value in dataHist)
                    {
                        if (value.Status == 1)
                        {
                            value.StatusName = "Thành công";
                        }
                        if (value.Status == -1)
                        {
                            value.StatusName = "Từ chối";
                            
                        }
                        if (value.Status == 0)
                        {
                            value.StatusName = "Tạo mới";
                        }
                        if (value.Status == 99)
                        {
                            value.StatusName = "Xóa";

                        }
                        if (value.Status == -2)
                        {
                            value.StatusName = "Đã hủy";

                        }
                        if (value.Status == 5)
                        {
                            value.StatusName = "Chờ xác nhận hủy";
                        }
                        if (value.Status == -3)
                        {
                            value.StatusName = "Ngừng sử dụng";
                        }
                    }
                    data.MarginList = dataHist.ToList();

                    
                    if (dataHist.Any())
                        return new GetServiceHistResult()
                        {
                            AccountNo = accountNo,
                            MarginList = data.MarginList
                        };
                    
                    return null;
                });

            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "GetServiceHist");
                throw;
            }
        }

        public async Task<LoginAccountResult> CheckAccountMargin(BaseRequest model)
        {
            try
            {
                var retData = new LoginAccountResult();
                var accountNo = model.UserId + "6";
                
                var dataRes = await WithXtradeInnoConnection(async conn =>
                {
                    
                    var sql = "SELECT AccountNo FROM ACCOUNT WHERE AccountNo = @AccountNo";
                    var dataMargin =  conn.QueryFirstOrDefault<string>(sql, new
                    {
                        AccountNo = accountNo
                    });
                    if (string.IsNullOrEmpty(dataMargin))
                    {
                        retData.Status = 2;
                    }
                    else
                    {
                        var sqlSecond =
                            "select  AccountNo from ACCOUNTPERMISSION  where Permission=3 and AccountNo = @AccountNo";

                        var dataSecond = string.Empty; /*await conn.QueryFirstOrDefaultAsync<string>(sqlSecond, new
                        {
                            @AccountNo = accountNo
                        });*/
                        if (string.IsNullOrEmpty(dataSecond))
                        {
                            retData.Status = 1;
                            var sqlList = "select goi_dich_vuid PackID,ten_goi_dich_vu PackName,lai_suat Rate,ty_le_call CallMargin from TVSI_GOI_DICH_VU_MARGIN where trang_thai=1";
                            var dataMList = await WithEmsDBConnection(async connM
                                => await connM.QueryAsync<PackListResult>(sqlList));
                            retData.MarginList = dataMList.ToList();
                        }
                        else
                        {
                            retData.Status = 2;
                        }
                    }
                    return retData;
                });


                if (dataRes != null)
                {
                    dataRes.AccountNo = accountNo;
                    dataRes.FullName = "";
                    return dataRes;
                }
                dataRes.AccountNo = accountNo;
                dataRes.FullName = "";
                dataRes.Status = 0;
                dataRes.MarginList = null;
                return dataRes;
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "CheckAccountMargin");
                throw;
            }
        }
    }
}
