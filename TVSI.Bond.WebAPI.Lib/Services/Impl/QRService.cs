﻿using System;
using System.Threading.Tasks;
using Dapper;
using log4net;
using TVSI.Bond.WebAPI.Lib.Models.QRCode;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class QRService : BaseService,IQRService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(EkycService));

        public async Task<QRCodeResult> GetQRCode(QRCodeRequest model)
        {
            try
            {
                var result = new QRCodeResult();
                var sql = "SELECT AssignUser FROM CustInfo WHERE CustCode = @CustCode";
                return await WithCRMDBConnection(async conn =>
                {
                    var assignUser = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        @CustCode = model.UserId
                    });
                    if (!string.IsNullOrEmpty(assignUser))
                    {
                        var saleID = assignUser.Substring(0, 4);

                        var sqlQR = "SELECT * FROM QRCode WHERE saleID = @SaleID and Status = 1";
                        var data = await conn.QueryFirstOrDefaultAsync<CrmQRCodeResult>(sqlQR, new
                        {
                            SaleID = saleID
                        });
                        if (data != null)
                        {
                            var convertBase64 = string.Empty;
                            if (data.QRImage.Length > 0)
                                convertBase64 = "data:image/png;base64," + Convert.ToBase64String(data.QRImage);
                            result.FullName = data.FullName;
                            result.QRBase64 = convertBase64;
                            result.SaleID = data.SaleID;
                            result.Phone = result.SaleID.Contains("1001") ? "19001885" : data.Phone;
                            return result;

                        }
                    }
                    return null;
                });

            }
            catch (Exception ex)
            {
                log.Error($"Loi method GetQRCode Message - {ex.Message}, InnerException {ex.InnerException}");
                throw;
            }
        }
    }
}