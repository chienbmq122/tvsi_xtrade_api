﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using log4net;
using TVSI.Bond.WebAPI.Lib.Models.TransactionHistory;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using System.Data.SqlTypes;
using System.Globalization;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class TransactionHistoryService : BaseService, ITransactionHistoryService
    {
        private readonly ILog log = LogManager.GetLogger(typeof (SystemService));

        public async Task<List<StockTransactionHistResponse>> GetStockTransactionHistList(string accountNo,
            DateTime fromDate, DateTime toDate)
        {
            try
            {
                var fromDateFirst = fromDate;
                fromDateFirst = fromDateFirst.AddDays(-1);
                var toDateLast = toDate;

                // Hiệu chỉnh lại ngày đầu kỳ, cuối kỳ
                using (var conn = new SqlConnection(_commondbConnectionString))
                {
                    var firstDateList = await conn.QueryAsync<DateTime>("dbo.TVSI_s5NGAY_GIAO_DICH_GAN_NHAT",
                        new {ngay_hien_tai = fromDateFirst}, commandType: System.Data.CommandType.StoredProcedure);

                    if (firstDateList.Count() > 0)
                        fromDateFirst = firstDateList.ToList()[0];

                    var toDateList = await conn.QueryAsync<DateTime>("dbo.TVSI_s5NGAY_GIAO_DICH_GAN_NHAT",
                        new { ngay_hien_tai = toDateLast }, commandType: System.Data.CommandType.StoredProcedure);

                    if (toDateList.Count() > 0)
                        toDateLast = toDateList.ToList()[0];
                }

                using (var conn = new SqlConnection(_xtradeInnoConnectionString))
                {
                    await conn.OpenAsync();

                    var retData = new List<StockTransactionHistResponse>();

                    #region Get số đầu kỳ
                    var firtBalanceList =
                        await conn.QueryAsync<SP_SEL_BALANCE_TRANSACTION_Query>("dbo.SP_SEL_BALANCE_TRANSACTION",
                            new
                            {
                                AccountNo = accountNo,
                                StockSymbol = "",
                                StockType = "",
                                Date = fromDateFirst.ToString("yyyyMMdd")
                            },
                            commandType: System.Data.CommandType.StoredProcedure);

                    foreach (var item in firtBalanceList)
                    {
                        var existData = retData.FirstOrDefault(x => x.Symbol == item.StockSymbol.Trim());
                        if (existData != null)
                        {
                            if (item.StockType == "02")
                                existData.FirstSellableStocks += item.Quantity;

                            if (item.StockType == "47")
                                existData.FirstPledgeStocks += item.Quantity;

                            if (item.StockType == "49")
                                existData.FirstRestrictedStocks += item.Quantity;

                            if (item.StockType == "12")
                                existData.FirstRightStock += item.Quantity;

                            if (item.StockType == "03")
                                existData.FirstRegisterStocks += item.Quantity;
                        }
                        else
                        {
                            var newData = new StockTransactionHistResponse();
                            newData.Symbol = item.StockSymbol.Trim();
                            if (item.StockType == "02")
                                newData.FirstSellableStocks += item.Quantity;

                            if (item.StockType == "47")
                                newData.FirstPledgeStocks += item.Quantity;

                            if (item.StockType == "49")
                                newData.FirstRestrictedStocks += item.Quantity;

                            if (item.StockType == "12")
                                newData.FirstRightStock += item.Quantity;

                            if (item.StockType == "03")
                                newData.FirstRegisterStocks += item.Quantity;
                            retData.Add(newData);

                        }
                    }

                    #endregion

                    #region Get StockTransaction

                    var stockTransList =
                        await conn.QueryAsync<SP_SEL_STOCK_TRANSACTION_Query>("dbo.SP_SEL_STOCK_TRANSACTION",
                            new
                            {
                                AccountNo = accountNo,
                                BeginDate = fromDateFirst.ToString("yyyyMMdd"),
                                EndDate = toDateLast.ToString("yyyyMMdd"),
                                PageNum = 0,
                                PageSize = 100
                            },
                            commandType: System.Data.CommandType.StoredProcedure);

                    foreach (var item in stockTransList)
                    {
                        if (string.IsNullOrEmpty(item.AccountNo) || string.IsNullOrEmpty(item.Symbol))
                            continue;

                        var existData = retData.FirstOrDefault(x => x.Symbol == item.Symbol.Trim());
                        if (existData != null)
                        {
                            if (existData.StockTransList == null)
                                existData.StockTransList = new List<StockTransData>();

                            var stockTransItem = new StockTransData
                                                 {
                                                     Symbol = item.Symbol.Trim(),
                                                     Remark = item.Remark.Trim(),
                                                     Price = item.Price,
                                                     TransType_01 = item.Trans_Type1,
                                                     TransType_02 = item.Trans_Type2,
                                                     TransDate = item.TransDate.ToString("yyyyMMdd"),
                                                     DueDate = item.DueDate.ToString("yyyyMMdd"),
                                                     RefType = item.RefType,
                                                     CustType = item.CustType + "",
                                                     StockType = item.StockType,
                                                     StockType2 = item.STOCKTYPE2
                                                 };


                            GetStockQuantity(stockTransItem, item.Quantity);

                            existData.StockTransList.Add(stockTransItem);
                        }
                        else
                        {
                            // Tạo số dư đầu ky = 0
                            var newSymbolData = new StockTransactionHistResponse
                                                {
                                                    StockTransList = new List<StockTransData>(),
                                                    Symbol = item.Symbol.Trim(),
                                                    FirstSellableStocks = 0,
                                                    FirstPledgeStocks = 0,
                                                    FirstRestrictedStocks = 0,
                                                    FirstRightStock = 0,
                                                    FirstRegisterStocks = 0
                                                };


                            var stockTransItem = new StockTransData
                                                 {
                                                     Symbol = item.Symbol.Trim(),
                                                     Remark = item.Remark.Trim(),
                                                     Price = item.Price,
                                                     TransType_01 = item.Trans_Type1,
                                                     TransType_02 = item.Trans_Type2,
                                                    TransDate = item.TransDate.ToString("yyyyMMdd"),
                                                    DueDate = item.DueDate.ToString("yyyyMMdd"),
                                                    RefType = item.RefType,
                                                    CustType = item.CustType + "",
                                                    StockType = item.StockType,
                                                    StockType2 = item.STOCKTYPE2
                                                };

                            GetStockQuantity(stockTransItem, item.Quantity);

                            newSymbolData.StockTransList.Add(stockTransItem);

                            retData.Add(newSymbolData);
                        }
                    }

                    #endregion

                    #region Get số cuối kỳ

                    var lastBalanceList =
                        await conn.QueryAsync<SP_SEL_BALANCE_TRANSACTION_Query>("dbo.SP_SEL_BALANCE_TRANSACTION",
                            new
                            {
                                AccountNo = accountNo,
                                StockSymbol = "",
                                StockType = "",
                                Date = toDateLast.ToString("yyyyMMdd")
                            },
                            commandType: System.Data.CommandType.StoredProcedure);
                    foreach (var item in lastBalanceList)
                    {
                        var existData = retData.FirstOrDefault(x => x.Symbol == item.StockSymbol.Trim());
                        if (existData != null)
                        {
                            if (item.StockType == "02")
                                existData.LastSellableStocks += item.Quantity;

                            if (item.StockType == "47")
                                existData.LastPledgeStocks += item.Quantity;

                            if (item.StockType == "49")
                                existData.LastRestrictedStocks += item.Quantity;

                            if (item.StockType == "12")
                                existData.LastRightStock += item.Quantity;

                            if (item.StockType == "03")
                                existData.LastRegisterStocks += item.Quantity;
                        }

                    }

                    #endregion

                    return retData;
                }
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetStockTransactionHistList:accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<List<StockTransactionDetailResponse>> GetStockTransactionDetailList(
            StockTransactionDetailRequest request)
        {
            try
            {
                using (var conn = new SqlConnection(_xtradeInnoConnectionString))
                {
                    await conn.OpenAsync();

                    var dtxFromDate = SqlDateTime.MinValue;
                    var dtxToDate = SqlDateTime.MaxValue;

                    if (!string.IsNullOrEmpty(request.FromDate))
                        dtxFromDate = DateTime.ParseExact(request.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (!string.IsNullOrEmpty(request.ToDate))
                        dtxToDate = DateTime.ParseExact(request.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    var stockTransList =
                        await
                            conn.QueryAsync<FO_SEL_STOCK_TRANSACTIONS_DETAIL_Query>(
                                "dbo.FO_SEL_STOCK_TRANSACTIONS_DETAIL",
                                new
                                {
                                    AccountNo = request.AccountNo,
                                    Symbol = request.Symbol,
                                    StockType = request.TransType,
                                    BeginDate = ((DateTime) dtxFromDate).ToString("yyyyMMdd"),
                                    EndDate = ((DateTime) dtxToDate).ToString("yyyyMMdd"),
                                    PageIndex = request.PageIndex,
                                    PageSize = request.PageSize
                                },
                                commandType: System.Data.CommandType.StoredProcedure);

                    var retData = new List<StockTransactionDetailResponse>();

                    foreach (var item in stockTransList)
                    {
                        var stockTrans = new StockTransactionDetailResponse
                                         {
                                             Symbol = item.SYMBOL.Trim(),
                                             Remark = item.REMARK.Trim(),
                                             Price = item.PRICE.Value,
                                            RefType = item.REFTYPE,
                                            DueDate = item.DUEDATE,
                                            TransDate = item.TRANSDATE.Value,
                                            TransType_01 = item.TRANS_TYPE1,
                                            TransType_02 = item.TRANS_TYPE2,
                                            CustType = item.CUSTTYPE,
                                            StockType = item.STOCKTYPE,
                                            

                        };

                        var stockTransTemp = new StockTransData()
                                              {
                                                  RefType = stockTrans.RefType,
                                                  StockType = stockTrans.StockType,
                                                  StockType2 = stockTrans.StockType2,
                                                  TransType_01 = stockTrans.TransType_01,
                                                  TransType_02 = stockTrans.TransType_02,
                                              };

                        GetStockQuantity(stockTransTemp, item.QUANTITY);

                        stockTrans.SellableStocks = stockTransTemp.SellableStocks;
                        stockTrans.PledgeStocks = stockTransTemp.PledgeStocks;
                        stockTrans.RestrictedStocks = stockTransTemp.RestrictedStocks;
                        stockTrans.RightStock = stockTransTemp.RightStock;
                        stockTrans.RegisterStocks = stockTransTemp.RegisterStocks;

                        retData.Add(stockTrans);
                    }

                    return retData;
                }
            }
            catch (Exception ex)
            {
                LogException(log, request.AccountNo, ex, "GetStockTransactionDetailList:accountNo=" + request.AccountNo);
                throw;
            }
        }

        public async Task<List<OrderHistTransResponse>> GetOrderHistTransList(OrderHistTransRequest request)
        {
            try
            {
                using (var conn = new SqlConnection(_xtradeInnoConnectionString))
                {
                    await conn.OpenAsync();

                    var dtxFromDate = SqlDateTime.MinValue;
                    var dtxToDate = SqlDateTime.MaxValue;

                    if (!string.IsNullOrEmpty(request.FromDate))
                        dtxFromDate = DateTime.ParseExact(request.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (!string.IsNullOrEmpty(request.ToDate))
                        dtxToDate = DateTime.ParseExact(request.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    var orderHistList =
                        await conn.QueryAsync<SP_SEL_FO_ORDERHISTORY_Query>("dbo.TVSI_GET_ORDER_HISTORY",
                            new
                            {
                                AccountNo = request.AccountNo,
                                Symbol = request.Symbol,
                                Side = request.Side,
                                OrderStatus = request.Status,
                                OrderChannel = request.Chanel,
                                BeginDate = ((DateTime) dtxFromDate).ToString("yyyyMMdd"),
                                EndDate = ((DateTime) dtxToDate).ToString("yyyyMMdd"),
                                PageIndex = request.PageIndex,
                                PageSize = request.PageSize
                            },
                            commandType: System.Data.CommandType.StoredProcedure);

                    var retData = new List<OrderHistTransResponse>();

                    foreach (var item in orderHistList)
                    {
                        var orderTrans = new OrderHistTransResponse
                                         {
                                             TradeTime = item.OrderDate + " " + item.OrderTime,
                                             OrderNo = item.OrderNo + "",
                                             Side = item.Side,
                                             Symbol = item.SecSymbol,
                                             OrderPrice = item.Price,
                                             OrderQty = item.volume,
                                             DealPrice = item.MatchPrice,
                                             DealQty = item.MatchVolume,
                                            Fee = item.Comm,
                                            Tax = item.Tax,
                                            Amount = item.MatchValue_Net,
                                            Status = item.OrderStatus,
                                            Channel = item.Channel,
                                            EnterID = item.EnterID,
                                            Market = item.Market,
                                            ConditionPrice = !string.IsNullOrEmpty(item.ConditionPrice) ? item.ConditionPrice.Trim() : ""
                                        };
                        var orderStatus = item.OrderStatus;

                        if (!string.IsNullOrEmpty(orderStatus) && (orderStatus.ToUpper() == "C" 
                            || orderStatus.ToUpper() == "X" || (orderStatus.ToUpper() == "XA" 
                            || orderStatus.ToUpper() == "XC") || orderStatus.ToUpper() == "XAC"))
                        {
                            orderTrans.CancelQty = item.MatchVolume <= 0 ? item.volume 
                                    : item.volume - item.MatchVolume;
                            if (item.CancelVolume < 0)
                                item.CancelVolume = 0;
                        }

                        retData.Add(orderTrans);
                    }

                    return retData;
                }
            }
            catch (Exception ex)
            {
                LogException(log, request.AccountNo, ex, "GetOrderHistTransList:accountNo=" + request.AccountNo);
                throw;
            }
        }

        #region "Private function"

        private void GetStockQuantity(StockTransData stockTransDetail, decimal quantity)
        {
            switch (stockTransDetail.RefType)
            {
                case "DE":
                    stockTransDetail.RegisterStocks = quantity;
                    break;
                case "WD":
                    stockTransDetail.RegisterStocks = -quantity;
                    break;
                case "TI":
                    if (stockTransDetail.StockType == "49")
                    {
                        stockTransDetail.RestrictedStocks = quantity;
                        break;
                    }
                    stockTransDetail.SellableStocks = quantity;
                    break;
                case "TO":
                    if (stockTransDetail.StockType == "49")
                    {
                        stockTransDetail.RestrictedStocks = -quantity;
                        break;
                    }
                    stockTransDetail.SellableStocks = -quantity;
                    break;
                case "TC":
                    switch (stockTransDetail.TransType_02)
                    {
                        case "0":
                            stockTransDetail.SellableStocks = -quantity;
                            break;
                        default:
                            stockTransDetail.SellableStocks = quantity;
                            break;
                    }
                    break;
                case "GC":
                    if (stockTransDetail.StockType == "02" && stockTransDetail.StockType2 == "47")
                    {
                        int num4 = stockTransDetail.TransType_02 == "1" ? 1 : -1;
                        stockTransDetail.SellableStocks = num4*quantity;
                        stockTransDetail.PledgeStocks = -num4*quantity;
                        break;
                    }
                    if (stockTransDetail.StockType == "02" && stockTransDetail.StockType2 == "49")
                    {
                        int num4 = stockTransDetail.TransType_02 == "1" ? 1 : -1;
                        stockTransDetail.SellableStocks = num4*quantity;
                        stockTransDetail.RestrictedStocks = -num4*quantity;
                        break;
                    }
                    if (stockTransDetail.StockType == "02" && stockTransDetail.StockType2 == "03")
                    {
                        int num4 = stockTransDetail.TransType_02 == "1" ? 1 : -1;
                        stockTransDetail.SellableStocks = num4*quantity;
                        stockTransDetail.RegisterStocks = -num4*quantity;
                        break;
                    }
                    if (stockTransDetail.StockType == "03" && stockTransDetail.StockType2 == "49")
                    {
                        int num4 = stockTransDetail.TransType_02 == "1" ? 1 : -1;
                        stockTransDetail.RegisterStocks = num4*quantity;
                        stockTransDetail.RestrictedStocks = -num4*quantity;
                        break;
                    }
                    if (stockTransDetail.StockType == "02")
                    {
                        int num4 = stockTransDetail.TransType_02 == "1" ? 1 : -1;
                        stockTransDetail.SellableStocks = num4*quantity;
                        break;
                    }
                    if (stockTransDetail.StockType == "03")
                    {
                        int num4 = stockTransDetail.TransType_02 == "1" ? 1 : -1;
                        stockTransDetail.RegisterStocks = num4*quantity;
                        break;
                    }
                    if (stockTransDetail.StockType == "47")
                    {
                        stockTransDetail.SellableStocks = -quantity;
                        stockTransDetail.PledgeStocks = quantity;
                        break;
                    }
                    if (stockTransDetail.StockType == "49")
                    {
                        stockTransDetail.SellableStocks = -quantity;
                        stockTransDetail.RestrictedStocks = quantity;
                        break;
                    }
                    break;
                case "XR":
                    stockTransDetail.RightStock = quantity;
                    break;
                case "RX":
                    if (stockTransDetail.StockType == "49")
                        stockTransDetail.RestrictedStocks = quantity;
                    else
                        stockTransDetail.SellableStocks = quantity;

                    stockTransDetail.RightStock = -quantity;

                    break;
                case "BU":
                    if (stockTransDetail.StockType == "49")
                    {
                        stockTransDetail.RestrictedStocks = quantity;
                        break;
                    }
                    stockTransDetail.SellableStocks = quantity;
                    break;
                case "SE":
                    if (stockTransDetail.StockType == "49")
                    {
                        stockTransDetail.RestrictedStocks = quantity;
                        break;
                    }
                    stockTransDetail.SellableStocks = -quantity;
                    break;

                case "EX":
                    if (stockTransDetail.StockType == "02")
                    {
                        stockTransDetail.SellableStocks = quantity;
                        stockTransDetail.RightStock = -quantity;
                        break;
                    }
                    if (stockTransDetail.StockType == "12")
                    {
                        stockTransDetail.RightStock = quantity;
                        break;
                    }
                    if (stockTransDetail.StockType == "49")
                    {
                        stockTransDetail.RestrictedStocks = quantity;
                        break;
                    }
                    break;
                case "XE":
                    stockTransDetail.RightStock = stockTransDetail.StockType == "12"
                        ? quantity
                        : stockTransDetail.RightStock;
                    break;
                case "XN":
                    if (stockTransDetail.StockType == "02")
                    {
                        stockTransDetail.SellableStocks = quantity;
                        break;
                    }
                    if (stockTransDetail.StockType == "08")
                    {
                        stockTransDetail.RegisterStocks = -quantity;
                        break;
                    }
                    if (stockTransDetail.StockType == "49")
                    {
                        stockTransDetail.RestrictedStocks = quantity;
                        break;
                    }
                    break;

            }
            stockTransDetail.Balance = stockTransDetail.SellableStocks + stockTransDetail.PledgeStocks + stockTransDetail.RestrictedStocks + stockTransDetail.RegisterStocks + stockTransDetail.RightStock;
        }

        #endregion
    }
}
