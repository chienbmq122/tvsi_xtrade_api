﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using log4net;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Models.Mm;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Lib.Models.Order;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class MmService : BaseService, IMmService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(MmService));
        private IOrderCommandText _orderCommandText;
        private IMmCommandText _mmCommandText;

        public MmService(IOrderCommandText orderCommandText, IMmCommandText mmCommandText)
        {
            _orderCommandText = orderCommandText;
            _mmCommandText = mmCommandText;
        }

        public async Task<IEnumerable<MmContractListResult>> GetMmContractList(string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<MmContractListResult>(
                        _mmCommandText.GetMmContractList, new {AccountNo = accountNo});
                    return data;
                });

            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetMmContractList:accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<MmContractShortDetailResult> GetMmShortContractDetail(string accountNo, string contractNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryFirstAsync<MmContractShortDetailResult>(_mmCommandText.GetMmShortContractDetail, 
                        new { AccountNo = accountNo, ContractNo = contractNo });
                    return data;
                });

            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetMmShortContractDetail:accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<MmContractDetailResult> GetMmContractDetail(string contractNo, string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                {
                    var detailInfo = await conn.QueryFirstOrDefaultAsync<MmContractDetailResult>("TVSI_sAPI_MM_CL_GetContractDetail",
                                new { ContractNo = contractNo}, commandType: System.Data.CommandType.StoredProcedure);

                    if (detailInfo != null)
                    {
                        // Thông tin tài khoản nhận tiền
                        if (detailInfo.PaymentMethod == 1)
                        {
                            if (detailInfo.CustBankID.HasValue)
                            {

                                var custBankData = await conn.QueryFirstOrDefaultAsync<dynamic>(_orderCommandText.GetCustBankInfo,
                                    new { CustBankID = detailInfo.CustBankID.Value });
                                if (custBankData != null)
                                {
                                    detailInfo.MoneyReceiver = custBankData.BankAccount + "-" + custBankData.BankAccountName
                                        + "-" + custBankData.BankName + "-" + custBankData.SubBranchName;
                                }
                            }
                        }
                        else
                        {
                            detailInfo.MoneyReceiver = ResourceFile.OrderModule.PaymentMethodName_0;
                        }

                        // Chi tiết lệnh đặt
                        var orderDetailInfo = await conn.QueryAsync<MmOrderDetailInfo>("TVSI_sAPI_MM_CL_GetContractOrderDetail",
                                new { ContractNo = contractNo, AccountNo = accountNo }, commandType: System.Data.CommandType.StoredProcedure);
                        detailInfo.MmOrderDetailList = orderDetailInfo.ToList();

                        // Dòng tiền đã nhận
                        detailInfo.MmReceivedCouponList = new List<MmCouponDetailInfo>();
                        // Lãi định kỳ đã nhận
                        var receivedCouponInfo = await conn.QueryAsync<MmCouponDetailInfo>("TVSI_sAPI_MM_CL_GetReceivedCouponAndCash",
                            new { ContractNo = detailInfo.ContractNo }, commandType: System.Data.CommandType.StoredProcedure);
                        detailInfo.MmReceivedCouponList = receivedCouponInfo.ToList();

                        // Dong tien se nhan
                        var jsonData = await conn.QueryFirstOrDefaultAsync<string>(_orderCommandText.GetContractPriceInfo,
                                    new { ContractNo = contractNo, AccountNo = accountNo });
                        detailInfo.CouponList = new List<MmCouponDetailInfo>();
                        if (!string.IsNullOrEmpty(jsonData))
                        {
                            var priceInfoData = JsonConvert.DeserializeObject<List<BondPriceCalc>>(jsonData);
                            var sellAmountItem =
                                    priceInfoData.FirstOrDefault(x => x.RowCode == "SellAmount");
                            if (sellAmountItem != null)
                            {
                                detailInfo.CouponList.Add(
                                    new MmCouponDetailInfo
                                    {
                                        CouponDate = sellAmountItem.ToDate?.ToString("dd/MM/yyyy") ?? "",
                                        Amount = sellAmountItem.Income,
                                        Note = "Gốc và lãi tiền cọc"

                                    });
                            }
                        }
                    }

                    return detailInfo;
                });

            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetMmContractDetail:accountNo=" + accountNo);
                throw;
            }
        }

        #region Order book

        public async Task<IEnumerable<MmOrderBookResult>> GetMmOrderBook(DateTime tradeDate, string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<MmOrderBookResult>(_mmCommandText.GetMmOrderBook,
                        new { tradeDate = tradeDate, accountNo = accountNo });
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetMmOrderBook:accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<MmOrderDetailResult> GetMmOrderDetail(string contractNo, string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn => await conn.QueryFirstOrDefaultAsync<MmOrderDetailResult>("TVSI_sAPI_MM_OB_OrderDetail",
                new { accountNo = accountNo, contractNo = contractNo }, commandType: System.Data.CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetMmOrderDetail:contractNo=" + contractNo + ",accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<int> CancelMmOrder(string contractNo, string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn => await conn.QueryFirstOrDefaultAsync<int>("TVSI_sAPI_OM_OB_CancelOrder",
                     new { AccountNo = accountNo, ContractNo = contractNo }, commandType: System.Data.CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "CancelMmOrder:accountNo=" + accountNo + ",contractNo=" + contractNo);
                throw;
            }
        }

        public async Task<IEnumerable<MmOrderHistResult>> GetMmOrderHistList(DateTime fromDate,
            DateTime toDate, string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<MmOrderHistResult>(_mmCommandText.GetMmOrderHistList,
                        new { fromDate = fromDate, toDate = toDate, accountNo = accountNo });
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetMmOrderBook:accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<IEnumerable<MMOrderConfirmListResult>> GetMmOrderConfirmList(DateTime fromDate,
            DateTime toDate, string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                {
                    var data = await conn.QueryAsync<MMOrderConfirmListResult>("TVSI_sAPI_MM_OrderConfirmList",
                       new { FromDate = fromDate, ToDate = toDate, AccountNo = accountNo }, commandType: System.Data.CommandType.StoredProcedure);

                    return data;

                    //var data = await conn.QueryAsync<MmOrderHistResult>(_mmCommandText.GetMmOrderConfrimList,
                    //    new { fromDate = fromDate, toDate = toDate, accountNo = accountNo });

                    //return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetMmOrderConfrimList:accountNo=" + accountNo + ",fromDate=" + fromDate.ToString("yyyyMMdd")
                    + ",toDate=" + toDate.ToString("yyyyMMdd"));
                throw;
            }
        }

        public async Task<IEnumerable<MMOrderPaymentInfoResult>> MM_OC_GetContractPaymentInfo(string contractNo)
        {
            try
            {
                return await WithTbmConnection(async conn => await conn.QueryAsync<MMOrderPaymentInfoResult>("TVSI_sAPI_MM_CM_GetContractPaymentInfo",
                     new { ContractNo = contractNo }, commandType: System.Data.CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                LogException(log, "bond", ex, "MM_OC_GetContractPaymentInfo:,contractNo=" + contractNo);
                throw;
            }
        }

        public async Task<int> ConfirmMmOrder(string accountNo, long requestOrderID)
        {
            try
            {
                return await WithTbmConnection(async conn => await conn.QueryFirstOrDefaultAsync<int>("TVSI_sAPI_MM_OC_ConfirmOrder",
                      new { RequestOrderID = requestOrderID }, commandType: System.Data.CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "ConfirmOrder:accountNo=" + accountNo + ",requestOrderID=" + requestOrderID);
                throw;
            }
        }

        public async Task<MMOrderConfirmDetailResult> GetMMOrderConfirmDetail(string requestOrderID, string accountNo)
        {
            try
            {
                var orderConfirmDetail = await WithTbmConnection(async conn => await conn.QueryFirstOrDefaultAsync<MMOrderConfirmDetailResult>("TVSI_sAPI_MM_OB_OrderConfirmDetail",
                new { accountNo = accountNo, requestOrderID = requestOrderID }, commandType: System.Data.CommandType.StoredProcedure));

                if (!string.IsNullOrEmpty(orderConfirmDetail.PriceInfo))
                {
                    var priceInfoData = JsonConvert.DeserializeObject<List<BondPriceCalc>>(orderConfirmDetail.PriceInfo);

                    List<MmPriceDetailInfo> priceDetailInfo = new List<MmPriceDetailInfo>();
                    foreach (var item in priceInfoData)
                    {
                        if (item.RowCode != "Yield")
                        {
                            priceDetailInfo.Add(new MmPriceDetailInfo
                            {
                                Content = item.Description,
                                CouponDate = item.ToDate?.ToString("dd/MM/yyyy") ?? "",
                                Value = item.Income.ToString("N0") + " " + item.Unit,
                            });
                        }
                        else
                        {
                            priceDetailInfo.Add(new MmPriceDetailInfo
                            {
                                Content = item.Description,
                                CouponDate = item.ToDate?.ToString("dd/MM/yyyy") ?? "",
                                Value = item.Income * 100 + " " + item.Unit,
                            });
                        }
                    }

                    orderConfirmDetail.PriceInfoList = priceDetailInfo;
                }

                return orderConfirmDetail;
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetOrderConfirmDetail:requestOrderID=" + requestOrderID + ",accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<int> RefuseMmOrder(string accountNo, long requestOrderID, string note)
        {
            try
            {
                return await WithTbmConnection(async conn => await conn.QueryFirstOrDefaultAsync<int>("TVSI_sAPI_MM_OC_RefuseOrder",
                      new { AccountNo = accountNo, RequestOrderID = requestOrderID, Note = note }, commandType: System.Data.CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "RefuseOrder:accountNo=" + accountNo + ",requestOrderID=" + requestOrderID);
                throw;
            }
        }
        #endregion
    }
}
