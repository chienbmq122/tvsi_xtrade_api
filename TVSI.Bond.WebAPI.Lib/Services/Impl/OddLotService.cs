﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using log4net;
using TVSI.Bond.WebAPI.Lib.Infrastructure;
using TVSI.Bond.WebAPI.Lib.Models.OddLot;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Lib.Utility.OddLotParams;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class OddLotService : BaseService, IOddLotService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(OddLotService));
        private IOddLotCommandText _commandText;

        public OddLotService(IOddLotCommandText commandText)
        {
            _commandText = commandText;
        }

        /// <summary>
        /// Lấy danh mục cổ phiếu lô lẻ của khách hàng
        /// </summary>
        /// <param name="customerAccount"></param>
        /// <returns></returns>
        public async Task<List<OddLotStockRepository>> SelectStockRepository(string customerAccount)
        {
            try
            {
                return await WithDb2Connection(conn =>
                {
                    var data = conn.Query<OddLotStockRepository>(_commandText.SelectStockRepository, new
                    {
                        param1 = customerAccount
                    }).ToList();

                    List<OddLotOrder> lstOddLotOrder = SelectProcessList(customerAccount);

                    for (int i = 0; i < data.Count; i++)
                    {
                        switch (data[i].stock_market)
                        {
                            case "O":
                                data[i].stock_market = "HOSE";
                                break;
                            case "N":
                                data[i].stock_market = "HNX";
                                break;
                            case "C":
                                data[i].stock_market = "UPCOM";
                                break;
                        }

                        data[i].stock_code = data[i].stock_code.Trim();

                        if (data[i].stock_type.Equals("2"))
                        {
                            data[i].stock_type = "02";
                        }

                        data[i].stock_quantity = data[i].stock_quantity % 100;

                        int processQuantity = lstOddLotOrder.Where(p =>
                                p.stock_code == data[i].stock_code && p.stock_type == data[i].stock_type)
                            .Sum(x => x.stock_quantity);

                        data[i].stock_quantity = data[i].stock_quantity - processQuantity;
                    }

                    List<OddLotStockRepositoryEmsModel> lstEmsStockRepository =
                        SelectEmsStockRepository(String.Empty, DateTime.MinValue, DateTime.MinValue);
                    List<OddLotStockRepository> resultData = new List<OddLotStockRepository>();

                    foreach (var record in data)
                    {
                        var myEmsStockRepository = lstEmsStockRepository.Where(item =>
                            item.stock_code.Trim().ToUpper() == record.stock_code.Trim().ToUpper()).ToList();
                        /*if (lstEmsStockRepository.Any(item =>
                            item.stock_code.Trim().ToUpper() == record.stock_code.Trim().ToUpper()))*/
                        if(myEmsStockRepository.Count > 0)
                        {
                            record.active_status = 1;
                            record.fee_rate = myEmsStockRepository[0].fee_rate;
                        }
                        else
                        {
                            record.active_status = 0;
                        }

                        if (record.stock_quantity > 0 && record.stock_code.Length == 3)
                        {
                            resultData.Add(record);
                        }
                    }

                    return Task.FromResult(resultData);
                });
            }
            catch (Exception ex)
            {
                LogException(log, customerAccount, ex, "SelectStockRepository");
                throw;
            }
        }
        
        /// <summary>
        /// Lấy danh sách hỗ trợ bán lô lẻ
        /// </summary>
        /// <param name="stockCode"></param>
        /// <returns></returns>
        public async Task<List<OddLotStockRepositoryEmsSearchModel>> SearchStockRepository(string stockCode)
        {
            try
            {
                    List<OddLotStockRepositoryEmsModel> lstEmsStockRepository =
                        SelectEmsStockRepository(stockCode, DateTime.MinValue, DateTime.MinValue);
                    List<OddLotStockRepositoryEmsSearchModel> resultData = new List<OddLotStockRepositoryEmsSearchModel>();

                    foreach (var record in lstEmsStockRepository)
                    {
                        var newRecord = new OddLotStockRepositoryEmsSearchModel
                        {
                            stock_code = record.stock_code,
                            fee_rate = record.fee_rate
                        };
                        
                        resultData.Add(newRecord);
                    }

                    return resultData;
            }
            catch (Exception ex)
            {
                LogException(log, "oddlot", ex, "SearchStockRepository");
                throw;
            }
        }

        /// <summary>
        /// Thêm mới lệnh bán lô lẻ
        /// </summary>
        /// <param name="lstOrder"></param>
        /// <returns></returns>
        public async Task<bool> InsertOrder(string customerAccount, string userId,
            List<OddLotOrderInsertModel> lstOrder)
        {
            try
            {
                return await WithIpgDBConnection(conn =>
                {
                    dynamic drAccountInfo = GetAccountInfo(customerAccount.Substring(0, 6));

                    if (drAccountInfo != null)
                    {
                        var customer_name = drAccountInfo.ten_khach_hang + "";
                        var customer_type = drAccountInfo.loai_khach_hang + "";
                        int stock_quantity = 0;
                        decimal stock_price = 0m;
                        decimal gia_tri = 0m;
                        decimal transaction_tax = 0m;
                        decimal xr_tax = 0m;
                        decimal phi_tvsi_thuc_thu = 0m;
                        decimal tien_mua_ck_tvsi_tra_ndt = 0m;
                        decimal gia_von_tam_tinh = 0m;
                        decimal x = 0m;
                        decimal fee_rate = 0m;
                        List<OddLotOrder> lstOddLotOrder = new List<OddLotOrder>();

                        foreach (var item in lstOrder)
                        {
                            // Tính toán thuế/phí --- Begin
                            stock_quantity = item.stock_quantity;
                            stock_price = item.stock_price * 1000;
                            gia_tri = stock_quantity * stock_price;
                            transaction_tax = Math.Round(gia_tri / 1000, 0, MidpointRounding.AwayFromZero);
                            xr_tax = Math.Round(Math.Min(10000, stock_price) * stock_quantity * 5 / 100, 0,
                                MidpointRounding.AwayFromZero);

                            // --- Nếu (Giá vốn tạm tính * Số lượng) chênh lệch với Tiền mua CK TVSI trả NĐT --- Begin
                            var myStockRepository = SelectEmsStockRepositoryByStockCode(item.stock_code);
                            if (myStockRepository != null)
                            {
                                fee_rate = myStockRepository.fee_rate;
                            }
                            phi_tvsi_thuc_thu = Math.Round(gia_tri * fee_rate, 0, MidpointRounding.AwayFromZero);
                            //phi_tvsi_thuc_thu = Math.Round(gia_tri * 30 / 100, 0, MidpointRounding.AwayFromZero);
                            tien_mua_ck_tvsi_tra_ndt = Math.Round(gia_tri - phi_tvsi_thuc_thu, 0,
                                MidpointRounding.AwayFromZero);
                            gia_von_tam_tinh = Math.Round(tien_mua_ck_tvsi_tra_ndt / stock_quantity, 0,
                                MidpointRounding.AwayFromZero);

                            x = gia_von_tam_tinh * stock_quantity - tien_mua_ck_tvsi_tra_ndt;
                            if (x != 0)
                            {
                                // Phí TVSI thực thu trừ đi X
                                phi_tvsi_thuc_thu = phi_tvsi_thuc_thu - x;
                            }
                            // --- Nếu (Giá vốn tạm tính * Số lượng) chênh lệch với Tiền mua CK TVSI trả NĐT --- End

                            if ("02".Equals(item.stock_type))
                            {
                                xr_tax = 0;
                            }

                            // --- Khách hàng tổ chức --- Begin
                            if ("2".Equals(customer_type) || "4".Equals(customer_type))
                            {
                                transaction_tax = 0;
                                xr_tax = 0;
                            }
                            // --- Khách hàng tổ chức --- End
                            // Tính toán thuế/phí --- End

                            OddLotOrder newOddLotOrder = new OddLotOrder()
                            {
                                customer_account = customerAccount,
                                customer_name = customer_name,
                                stock_code = item.stock_code,
                                stock_type = item.stock_type,
                                stock_quantity = item.stock_quantity,
                                stock_price = item.stock_price,
                                transaction_tax = transaction_tax,
                                xr_tax = xr_tax,
                                transaction_fee = phi_tvsi_thuc_thu,
                                sell_date = DateTime.Now,
                                order_status = OddLotOrderStatusConst.CHO_XU_LY,
                                order_channel = OddLotOrderChannelConst.GATEWAY,
                                vsd_stock_api_id = -1,
                                created_by = userId,
                                created_date = DateTime.Now,
                                updated_by = userId,
                                updated_date = DateTime.Now
                            };

                            lstOddLotOrder.Add(newOddLotOrder);
                        }

                        var result = conn.ExecuteScalar<int>(
                            _commandText.TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_INSERT, new
                            {
                                @OddLotOrderList = OddLotUtils.ToDataTable(lstOddLotOrder)
                            }, commandType: CommandType.StoredProcedure);

                        if (result == lstOddLotOrder.Count)
                        {
                            return Task.FromResult(true);
                        }

                        return Task.FromResult(false);
                    }

                    return Task.FromResult(false);
                });
            }
            catch (Exception ex)
            {
                LogException(log, customerAccount, ex, "InsertOrder");
                throw;
            }
        }

        /// <summary>
        /// Lấy danh sách lệnh bán lô lẻ
        /// </summary>
        /// <param name="stock_code"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="order_status"></param>
        /// <param name="order_channel"></param>
        /// <returns></returns>
        public async Task<List<OddLotOrder>> SearchOrder(string customer_account, string stock_code, DateTime fromDate,
            DateTime toDate, int order_status, int order_channel)
        {
            try
            {
                return await WithIpgDBConnection(conn =>
                {
                    var lstOrder = conn.Query<OddLotOrder>(
                        _commandText.TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_SEARCH, new
                        {
                            @customer_account = customer_account,
                            @customer_name = String.Empty,
                            @stock_code = stock_code,
                            @fromDate = fromDate,
                            @toDate = toDate,
                            @created_by = String.Empty,
                            @order_status = order_status,
                            @order_channel = order_channel
                        }, commandType: CommandType.StoredProcedure).ToList();

                    return Task.FromResult(lstOrder);
                });
            }
            catch (Exception ex)
            {
                LogException(log, customer_account, ex, "SearchOrder");
                throw;
            }
        }

        /// <summary>
        /// Lấy danh sách lệnh bán lô lẻ (trạng thái "Chờ xử lý" & lệnh được tạo trong ngày hiện tại)
        /// </summary>
        /// <param name="stock_code"></param>
        /// <param name="order_status"></param>
        /// <param name="order_channel"></param>
        /// <returns></returns>
        public async Task<List<OddLotOrder>> SelectCurrentOrder(string customer_account, string stock_code,
            int order_status, int order_channel)
        {
            try
            {
                return await WithIpgDBConnection(conn =>
                {
                    var lstOrder = conn.Query<OddLotOrder>(
                        _commandText.TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_SELECT_CURRENT_ORDER, new
                        {
                            @customer_account = customer_account,
                            @stock_code = stock_code,
                            @order_status = order_status,
                            @order_channel = order_channel
                        }, commandType: CommandType.StoredProcedure).ToList();

                    return Task.FromResult(lstOrder);
                });
            }
            catch (Exception ex)
            {
                LogException(log, customer_account, ex, "SelectCurrentOrder");
                throw;
            }
        }

        /// <summary>
        /// Lấy danh sách lệnh bán lô lẻ (bỏ trạng thái "Chờ xử lý")
        /// </summary>
        /// <param name="stock_code"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="order_status"></param>
        /// <param name="order_channel"></param>
        /// <returns></returns>
        public async Task<List<OddLotOrder>> SelectHistoricalOrder(string customer_account, string stock_code,
            DateTime fromDate,
            DateTime toDate, int order_status, int order_channel)
        {
            try
            {
                return await WithIpgDBConnection(conn =>
                {
                    var lstOrder = conn.Query<OddLotOrder>(
                        _commandText.TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_SELECT_HISTORICAL_ORDER, new
                        {
                            @customer_account = customer_account,
                            @stock_code = stock_code,
                            @fromDate = fromDate,
                            @toDate = toDate,
                            @order_status = order_status,
                            @order_channel = order_channel
                        }, commandType: CommandType.StoredProcedure).ToList();

                    return Task.FromResult(lstOrder);
                });
            }
            catch (Exception ex)
            {
                LogException(log, customer_account, ex, "SelectHistoricalOrder");
                throw;
            }
        }

        /// <summary>
        /// Hủy lệnh bán lô lẻ từ phía khách hàng
        /// </summary>
        /// <param name="lstOrder"></param>
        /// <returns></returns>
        public async Task<bool> CancelOrder(long odd_lot_order_id, string updated_by)
        {
            try
            {
                return await WithIpgDBConnection(conn =>
                {
                    var lstOrder = conn.Query<OddLotOrder>(
                        _commandText.TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_SELECT_BY_ID, new
                        {
                            @odd_lot_order_id = odd_lot_order_id
                        }, commandType: CommandType.StoredProcedure).ToList();

                    if (lstOrder.Count == 1 && lstOrder[0].order_status == OddLotOrderStatusConst.CHO_XU_LY)
                    {
                        lstOrder[0].order_status = OddLotOrderStatusConst.HUY_BO;

                        var result = conn.ExecuteScalar<int>(
                            _commandText.TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_UPDATE_STATUS, new
                            {
                                @OddLotOrderList = OddLotUtils.ToDataTable(lstOrder),
                                @order_status = OddLotOrderStatusConst.HUY_BO,
                                @updated_by = updated_by
                            }, commandType: CommandType.StoredProcedure);

                        if (result == lstOrder.Count)
                        {
                            return Task.FromResult(true);
                        }

                        return Task.FromResult(false);
                    }

                    return Task.FromResult(false);
                });
            }
            catch (Exception ex)
            {
                LogException(log, "oddlot", ex, "CancelOrder");
                throw;
            }
        }

        private List<OddLotStockRepositoryEmsModel> SelectEmsStockRepository(string stockCode, DateTime fromDate,
            DateTime toDate)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.IpgDBConnectString))
                {
                    conn.Open();
                    var procedure = _commandText.TVSI_sODDLOTSTOCKTRANSFERENCE_STOCK_REPOSITORY_SEARCH;
                    var values = new
                    {
                        @stock_code = stockCode,
                        @fromDate = (fromDate == DateTime.MinValue) ? (DateTime?) null : fromDate,
                        @toDate = (toDate == DateTime.MinValue) ? (DateTime?) null : toDate
                    };

                    return conn.Query<OddLotStockRepositoryEmsModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                LogException(log, "oddlot", ex, "SearchEms");
                throw;
            }
        }
        
        private OddLotStockRepositoryEmsModel SelectEmsStockRepositoryByStockCode(string stockCode)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.IpgDBConnectString))
                {
                    conn.Open();
                    var procedure = _commandText.TVSI_sODDLOTSTOCKTRANSFERENCE_STOCK_REPOSITORY_SELECT_BY_STOCK_CODE;
                    var values = new
                    {
                        @stock_code = stockCode
                    };

                    return conn.Query<OddLotStockRepositoryEmsModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                log.Error("TVSI_sODDLOTSTOCKTRANSFERENCE_STOCK_REPOSITORY_SELECT_BY_STOCK_CODE: " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        private List<OddLotOrder> SelectProcessList(string customerAccount)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.IpgDBConnectString))
                {
                    conn.Open();
                    var procedure = _commandText.TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_SELECT_PROCESS_LIST;
                    var values = new
                    {
                        @customer_account = customerAccount
                    };

                    return conn.Query<OddLotOrder>(procedure, values, commandType: CommandType.StoredProcedure)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                LogException(log, customerAccount, ex, "SelectProcessList");
                throw;
            }
        }

        public dynamic GetAccountInfo(string accountNumber)
        {
            try
            {
                var procedureName = _commandText.TVSI_sLAY_THONG_TIN_TAI_KHOAN_KHACH_HANG_THEO_SO_TAI_KHOAN_044_v2;
                var parameters = new
                {
                    @so_tai_khoan = accountNumber
                };
                using (var connection = new SqlConnection(ConnectionFactory.CommonDBConnectString))
                {
                    connection.Open();
                    return connection
                        .Query<dynamic>(procedureName, parameters, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LogException(log, accountNumber, ex, "GetAccountInfo");
                throw;
            }
        }
    }
}