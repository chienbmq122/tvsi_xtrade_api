﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IBM.Data.DB2;
using log4net;
using TVSI.Bond.WebAPI.Lib.Infrastructure;
using TVSI.Bond.WebAPI.Lib.Models.Customer;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using TVSI.Bond.WebAPI.Lib.Utility;
using TVSI.Bond.WebAPI.Lib.Models.AdvMoney;
using TVSI.WebServices.Entity.SBAGateway;
using TVSI.Utility;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class AdvMoneyService : BaseService, IAdvMoneyService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(AdvMoneyService));
        public ICashTransferService _cashTransferService;
        public ISystemService _systemService;

        public AdvMoneyService(ICashTransferService cashTransferService, ISystemService systemService)
        {
            _cashTransferService = cashTransferService;
            _systemService = systemService;
        }

        public decimal fGetDecimal(object obj)
        {
            decimal d = 0;
            if (decimal.TryParse(obj.ToString(), out d))
                return d;
            else
            {
                //không convert được
                return 0;
            }
        }

        public async Task<AvaiableAdvanceInfoResult> GetAvaiableAdvanceInfo(string accountNo)
        {
            try
            {
                var API510Input = new API510();

                API510Input.Header = new global::TVSI.Utility.SBAGatewayEntity.EntityHeader("API510", accountNo, "01", "tvsi", DateTime.Today.ToString("dd/MM/yyyy"));

                API510Input.Refer = await _systemService.GetSBAReferNo();

                var systemDate = _systemService.GetSystemDate();

                var data = new AvaiableAdvanceInfoResult { AccountNo = accountNo };
                var sellInfos = new List<SellStockInfo>();
                if (!string.IsNullOrWhiteSpace(API510Input.Refer))
                {
                    API510Input.Params = new object[Enum.GetNames(typeof(API510.ParamName)).Length];
                    API510Input.Params[0] = accountNo;

                    var API510Str = TVSI.Utility.SBAGatewayEntity.EntitySerializer.Serialize<API510, API510.ParamName, API510.FieldName>(API510Input);
                    
                    LogInfo(log, accountNo, "input: " + API510Str);

                    FWGetway.TransmitServiceClient transmitService = new FWGetway.TransmitServiceClient();
                    var API511In = transmitService.apiService(API510Str);

                    var api511 = TVSI.Utility.SBAGatewayEntity.EntitySerializer.Deserialize<API511, API511.ParamName, API511.FieldName>(API511In);

                    if (api511 == null)
                    {
                        TVSI.Utility.SBAGatewayEntity.ErrorEntity errorReturn = new global::TVSI.Utility.SBAGatewayEntity.ErrorEntity();
                        errorReturn = TVSI.Utility.SBAGatewayEntity.EntitySerializer.GetErrorEntity(API511In);

                        LogInfo(log, accountNo, "Get thong tin ung tien cua tai khoan " + accountNo + ", err = " + errorReturn.Code + "," +
                                                                        errorReturn.ErrorMsg);
                    }
                    else
                    {
                        if (null != api511 && api511.Rows.Count > 0)
                        {
                            var sellStockInfos = data.SellStockInfos;

                            var tempAdvs = (await GetCustAdvanceToday(accountNo, "vi")).Where(x => x.Status == "02" || x.Status == "00").ToList();

                            for (int i = 0; i < api511.Rows.Count; i++)
                            {
                                var item = new SellStockInfo();
                                
                                item.Availamt = Math.Floor(fGetDecimal(api511.Rows[i][(int)API511.FieldName.availamt - 1]));
                                item.Commamt = fGetDecimal(api511.Rows[i][(int)API511.FieldName.commamt - 1]);
                                item.Duedate = DateTime.ParseExact(api511.Rows[i][(int)API511.FieldName.duedate - 1].ToString(), "yyyyMMdd", CultureInfo.CurrentCulture);
                                item.Feerate = fGetDecimal(api511.Rows[i][(int)API511.FieldName.feerate - 1]);
                                item.Loanamt = fGetDecimal(api511.Rows[i][(int)API511.FieldName.loanamt - 1]);
                                item.Pledgeamt = fGetDecimal(api511.Rows[i][(int)API511.FieldName.pledgeamt - 1]);
                                item.Pledgevalue = fGetDecimal(api511.Rows[i][(int)API511.FieldName.pledgevalue - 1]);
                                item.Sellamt = fGetDecimal(api511.Rows[i][(int)API511.FieldName.sellamt - 1]);
                                item.Sellvalue = fGetDecimal(api511.Rows[i][(int)API511.FieldName.sellvalue - 1]);
                                item.Taxamt = fGetDecimal(api511.Rows[i][(int)API511.FieldName.taxamt - 1]);
                                item.Totalamt = fGetDecimal(api511.Rows[i][(int)API511.FieldName.totalamt - 1]);
                                item.Tradedate = DateTime.ParseExact(api511.Rows[i][(int)API511.FieldName.tradedate - 1].ToString(), "yyyyMMdd", CultureInfo.CurrentCulture);
                                item.Vatamt = fGetDecimal(api511.Rows[i][(int)API511.FieldName.vatamt - 1]);
                                item.Wdbef = fGetDecimal(api511.Rows[i][(int)API511.FieldName.wdbef - 1]);
                                item.NumDay = ((TimeSpan)(item.Duedate - systemDate)).Days;
                                var processingAdv = tempAdvs != null ? tempAdvs.Where(x => x.Duedate == item.Duedate).Sum(x => x.Amount) : 0M;
                                if (processingAdv > 0)
                                {
                                    if (item.Availamt - processingAdv > 0)
                                    {
                                        item.Availamt = item.Availamt - processingAdv;
                                        sellInfos.Add(item);
                                    }
                                }
                                else sellInfos.Add(item);
                            }
                        }
                    }

                    data.SellStockInfos = sellInfos;
                }
                return data;
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetAvaiableAdvanceInfo");
                return new AvaiableAdvanceInfoResult { AccountNo = accountNo };
            }
        }

        public async Task<AddAdvanceInfoResult> AddCustAdvance(AddAdvanceInfoRequest request)
        {
            var resultInfo = new AddAdvanceInfoResult();
            var systemDate = _systemService.GetSystemDate();
            resultInfo.TradeDate = systemDate;

            var sellInfo = await GetAvaiableAdvanceInfo(request.AccountNo);
            if(sellInfo!=null && sellInfo.SellStockInfos!=null && sellInfo.SellStockInfos.Count > 0)
            {
                var data = sellInfo.SellStockInfos.FirstOrDefault(x => x.Duedate == request.Duedate && x.Tradedate == request.Tradedate);
                if (data != null)
                {
                    if(request.Amount > 0 && request.Amount <= data.Sellamt)
                    {
                        try
                        {
                            var feeamt = _cashTransferService.GetAdvanceFee(request.AccountNo, systemDate.ToString("yyyy-MM-dd"), Functions.GetDouble(request.Amount), data.NumDay);
                            using (var conn = new SqlConnection(_ipgdbConnectionString))
                            {
                                var p = new DynamicParameters();
                                p.Add("ngay_giao_dich", systemDate.ToString("yyyyMMdd"));
                                p.Add("ma_chi_nhanh", "01");
                                p.Add("tvsi_tk_chung_khoan", request.AccountNo);
                                p.Add("so_tien", request.Amount);
                                p.Add("kenh_giao_dich", "OL");
                                p.Add("loai_giao_dich", "LN");
                                p.Add("so_tham_chieu", string.Format("TVSI{0}00000", systemDate.ToString("yyyyMMdd")));
                                p.Add("tvsi_trang_thai", "00");
                                p.Add("sba_trang_thai", "00");
                                p.Add("nguoi_tao", request.AccountNo);
                                p.Add("ngay_tao", DateTime.Now);
                                p.Add("sba_tradedate", data.Tradedate.ToString("yyyyMMdd"));
                                p.Add("sba_duedate", data.Duedate.ToString("yyyyMMdd"));
                                p.Add("sba_feerate", data.Feerate);
                                p.Add("sba_approveflag", "0");
                                p.Add("sba_loanamt", data.Loanamt);
                                p.Add("sba_sellvalue", data.Sellvalue);
                                p.Add("sba_pledgeamt", data.Pledgeamt);
                                p.Add("sba_totalamt", data.Totalamt);
                                p.Add("sba_wdbef", data.Wdbef);
                                p.Add("sba_taxamt", 0);
                                p.Add("sba_availamt", data.Availamt);
                                p.Add("sba_sellamt", data.Sellamt);
                                p.Add("sba_pledgevalue", data.Pledgevalue);
                                p.Add("sba_commamt", data.Commamt);
                                p.Add("sba_feeamt", feeamt);
                                p.Add("sba_vatamt", data.Vatamt);
                                p.Add("sba_result", "N");
                                p.Add("so_sequence_out", dbType: DbType.Int32, direction: ParameterDirection.Output);

                                conn.Query<int>("TVSI_sLN_INSERT_LENH_UNG_TIEN",
                                    p, commandType: System.Data.CommandType.StoredProcedure);

                                var so_sequence = p.Get<int>("so_sequence_out");
                                if(so_sequence > 0)
                                {
                                    var refNo = conn.QueryFirst<string>("TVSI_sLN_UNG_TIEN_CAP_NHAT_SO_THAM_CHIEU",
                                    new
                                    {
                                        ngay_giao_dich = systemDate.ToString("yyyyMMdd"),
                                        so_sequence = so_sequence
                                    }, commandType: System.Data.CommandType.StoredProcedure);
                                    resultInfo.SEQ = so_sequence;
                                    resultInfo.RetCode = "000";
                                    resultInfo.Message = "SUCCESS";
                                }
                                else
                                {
                                    resultInfo.RetCode = "022";
                                    resultInfo.Message = "ERROR";
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogException(log, request.AccountNo, ex, "AddCustAdvance");
                            resultInfo.RetCode = "023";
                            resultInfo.Message = "Không tồn tại thông tin lệnh bán có thể ứng";
                        }
                    }
                    else
                    {
                        resultInfo.RetCode = "024";
                        resultInfo.Message = "Số tiền ứng vượt quá số tiền có thể ứng";
                    }
                }
                else
                {
                    resultInfo.RetCode = "023";
                    resultInfo.Message = "Không tồn tại thông tin lệnh bán có thể ứng";
                }
            }
            else
            {
                resultInfo.RetCode = "023";
                resultInfo.Message = "Không tồn tại thông tin lệnh bán có thể ứng";
            }

            return resultInfo;
        }

        public async Task<List<CustAdvanceInfoResult>> GetCustAdvanceToday(string account, string lang)
        {
            try
            {
                using (var conn = new SqlConnection(_ipgdbConnectionString))
                {
                    return conn.Query<CustAdvanceInfoResult>("TVSI_sLN_GetCustAdvanceToday",
                    new
                    {
                        AccountNo = account,
                        Lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                LogException(log, account, ex, "GetCustAdvanceToday");
                return new List<CustAdvanceInfoResult>();
            }
        }

        public async Task<List<CustAdvanceInfoResult>> GetCustAdvanceHist(string account, DateTime fromDate, DateTime toDate, string lang)
        {
            try
            {
                using (var conn = new SqlConnection(_ipgdbConnectionString))
                {
                    return conn.Query<CustAdvanceInfoResult>("TVSI_sLN_GetCustAdvanceHist",
                    new
                    {
                        AccountNo = account,
                        FromDate = fromDate.ToString("yyyyMMdd"),
                        ToDate = toDate.ToString("yyyyMMdd"),
                        Lang = lang
                    }, commandType: System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                LogException(log, account, ex, "GetCustAdvanceHist");
                return new List<CustAdvanceInfoResult>();
            }
        }
    }
}
