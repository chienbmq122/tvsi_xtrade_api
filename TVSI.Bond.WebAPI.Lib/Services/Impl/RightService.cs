﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using log4net;
using TVSI.Bond.WebAPI.Lib.Models.Right;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class RightService : BaseService, IRightService
    {
        private readonly ILog log = LogManager.GetLogger(typeof (SystemService));

        public async Task<RightInfoResult> GetRightInfo(string accountNo)
        {
            try
            {
                using (var conn = new SqlConnection(_innoConnectionString))
                {
                    await conn.OpenAsync();

                    var rightInfoList = await conn.QueryAsync<RightInfoQuery>("dbo.SP_SEL_FO_RIR_RIGHTINFO_TVSI",
                        new
                        {
                            AccountNo = accountNo,
                            Symbol = "",
                            RightType = -1,
                            PageIndex = 0,
                            PageSize = int.MaxValue
                        },
                        commandType: System.Data.CommandType.StoredProcedure);

                    var retData = new RightInfoResult
                                  {
                                      BuyRightList = new List<BuyRightData>(),
                                      StockRightList = new List<StockRightData>()
                                  };

                    foreach (var rightInfo in rightInfoList)
                    {
                        var balanceIntraday = 0m;
                        var balanceIntradayData = await conn.QueryFirstOrDefaultAsync<BalanceIntradayQuery>
                            ("dbo.SP_SEL_FO_GETRIRINTRADAY_BYRRID", new {RRId = rightInfo.ID},
                                commandType: System.Data.CommandType.StoredProcedure);

                        if (balanceIntradayData != null)
                            balanceIntraday = balanceIntradayData.BalanceIntraday;

                        // Quyen mua
                        if (rightInfo.XTYPE == 0)
                        {
                            var item = new BuyRightData();
                            item.ID = rightInfo.ID;
                            item.AccountNo = rightInfo.ACCOUNTNO;
                            item.XType = rightInfo.XTYPE ?? 0;
                            item.Symbol = rightInfo.SYMBOL;
                            item.Old = rightInfo.OLD ?? 0;
                            item.New = rightInfo.NEW ?? 0;
                            item.Price = rightInfo.PRICE ?? 0;
                            item.NewShareCode = rightInfo.NEWSHARECODE;
                            item.XDate = rightInfo.XDate?.ToString("dd/MM/yyyy") ?? "";
                            item.ClosedDate = rightInfo.CLOSEDATE?.ToString("dd/MM/yyyy") ?? "";
                            item.TransferToDate = rightInfo.CONFIRMDATE?.ToString("dd/MM/yyyy") ?? "";
                            item.CompUnitNew = (rightInfo.COMPUNITNEW ?? 0) - balanceIntraday;
                            item.CompUnitConfirm = (rightInfo.COMPUNITCONFIRM ?? 0) + balanceIntraday;
                            item.Status = rightInfo.STATUS ?? 0;

                            if ("1".Equals(rightInfo.CONFIRMFLAG) && "0".Equals(rightInfo.DELFLAG)
                                && rightInfo.COMPUNITCONFIRM > 0)
                                item.CompUnitNew -= item.CompUnitConfirm;
                            if (item.CompUnitNew < 0)
                                item.CompUnitNew = 0;

                            if (item.CompUnitNew > 0)
                            {
                                if (rightInfo.COMPUNITWD > 0)
                                {
                                    if (rightInfo.STOCKTYPE.Equals("U") && (item.CompUnitConfirm > 0
                                                                            || item.CompUnitNew > 0 ||
                                                                            balanceIntraday > 0))
                                        retData.BuyRightList.Add(item);
                                }
                                else if (item.CompUnitConfirm > 0 || item.CompUnitNew > 0 || balanceIntraday > 0)
                                    retData.BuyRightList.Add(item);
                            }
                            else if (item.CompUnitConfirm > 0 || item.CompUnitNew > 0 || balanceIntraday > 0)
                            {
                                retData.BuyRightList.Add(item);
                            }


                        }
                        else // XType khác
                        {
                            var item = new StockRightData();
                            item.AccountNo = rightInfo.ACCOUNTNO;
                            item.XType = rightInfo.XTYPE ?? 0;
                            item.Symbol = rightInfo.SYMBOL;
                            item.Volume = rightInfo.COMPUNITBFXR ?? 0;
                            item.NewShareCode = rightInfo.NEWSHARECODE;
                            item.CompUnitNew = (rightInfo.COMPUNITNEW ?? 0) - balanceIntraday;

                            // Xử lý tỷ lệ (Inno)
                            var UF_RIR_ValueToRound = 1000;
                            var newPercent = rightInfo.NEW.Value;
                            var oldPercent = rightInfo.OLD.Value;
                            if (newPercent >= UF_RIR_ValueToRound || oldPercent >= UF_RIR_ValueToRound)
                            {
                                if (oldPercent > newPercent)
                                {
                                    oldPercent = 1;
                                    newPercent = Math.Round(rightInfo.NEW.Value/rightInfo.OLD.Value, 2,
                                        MidpointRounding.AwayFromZero);
                                }
                                else
                                {
                                    newPercent = 1;
                                    oldPercent = Math.Round(rightInfo.NEW.Value/rightInfo.OLD.Value, 2,
                                        MidpointRounding.AwayFromZero);
                                }
                            }

                            item.New = newPercent;
                            item.Old = oldPercent;
                            item.PayRate = rightInfo.PAYRATE.Value;
                            item.CompAmt = rightInfo.COMPAMT.Value;
                            item.ClosedDate = rightInfo.CLOSEDATE?.ToString("dd/MM/yyyy");
                            
                            retData.StockRightList.Add(item);
                        }
                    }

                    // Hiệu chỉnh lại ngày đăng ký cuối cùng CloseDate = XDate + 1
                    using (var commonConn = new SqlConnection(_commondbConnectionString))
                    {
                        foreach (var item in retData.BuyRightList)
                        {
                            var dtxXDate = DateTime.ParseExact(item.XDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); ;

                            var nextDateList = await commonConn.QueryAsync<DateTime>("dbo.TVSI_s5NGAY_GIAO_DICH_SAP_TOI",
                                new { ngay_hien_tai = dtxXDate }, commandType: System.Data.CommandType.StoredProcedure);

                            if (nextDateList.Count() > 1)
                                item.ClosedDate = nextDateList.ToList()[1].ToString("dd/MM/yyyy");
                        }

                    }

                    return retData;
                }
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetRightInfo:accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<List<RightStatusResult>> GetRightStatus(string accountNo, int pageIndex, int pageSize)
        {
            try
            {
                using (var conn = new SqlConnection(_innoConnectionString))
                {
                    await conn.OpenAsync();

                    var rightInfoList = await conn.QueryAsync<RightStatusQuery>("dbo.SP_SEL_FO_RIR_STATUS",
                        new
                        {
                            AccountNo = accountNo,
                            Symbol = "",
                            Status = -1,
                            XType = 0,
                            PageIndex = pageIndex,
                            PageSize = pageSize - 1
                        },
                        commandType: System.Data.CommandType.StoredProcedure);

                    var retData = new List<RightStatusResult>();

                    foreach (var rightInfo in rightInfoList)
                    {
                        var item = new RightStatusResult
                                   {
                                       AccountNo = rightInfo.ACCOUNTNO,
                                       EditTime =
                                           rightInfo.EDITTIME.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                                       Symbol = rightInfo.SYMBOL,
                                       Old = rightInfo.OLd.Value,
                                       New = rightInfo.NEW.Value,
                                       Price = rightInfo.PRICE.Value,
                                       NewShareCode = rightInfo.NEWSHARECODE,
                                       XDate = rightInfo.CLOSEDATE.Value.ToString("dd/MM/yyyy"),
                                       CloseDate = rightInfo.CLOSEDATE.Value.ToString("dd/MM/yyyy"),
                                       TransferToDate = rightInfo.TRANSFERTODATE.Value.ToString("dd/MM/yyyy"),
                                       CompUnitNew = rightInfo.COMPUNITNEW.Value > 0 ? rightInfo.COMPUNITNEW.Value : 0,
                                       CompUnitConfirm = rightInfo.COMPUNITCONFIRM.Value,
                                       Status = rightInfo.STATUS.Value,
                                       ContractNo = rightInfo.CONTRACTNO,
                                       TotalRow = rightInfo.RowCount,
                                       Reason = rightInfo.REASON
                                   };

                        retData.Add(item);
                    }

                    return retData;
                }

            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetRightStatus:accountNo=");
                throw;
            }
        }

        public async Task<List<RightHistResult>> GetRightHist(string accountNo, string symbol, int xType,
            DateTime fromDate, DateTime toDate, int pageIndex, int pageSize)
        {
            try
            {
                using (var conn = new SqlConnection(_xtradeInnoConnectionString))
                {
                    await conn.OpenAsync();

                    var rightInfoList = await conn.QueryAsync<RightHistQuery>("dbo.SP_SEL_FO_RIR_HISTORY",
                        new
                        {
                            AccountNo = accountNo,
                            Symbol = symbol,
                            XType = xType,
                            Status = -1,
                            BeginDate = fromDate.ToString("yyyyMMdd"),
                            EndDate = toDate.ToString("yyyyMMdd"),
                            PageIndex = pageIndex,
                            PageSize = pageSize
                        },
                        commandType: System.Data.CommandType.StoredProcedure);

                    var retData = new List<RightHistResult>();

                    foreach (var rightInfo in rightInfoList)
                    {
                        var item = new RightHistResult
                                   {
                                       AccountNo = rightInfo.ACCOUNTNO,
                                       Symbol = rightInfo.SYMBOL,
                                       XType = rightInfo.XTYPE.Value,
                                       Old = rightInfo.OLd.Value,
                                       New = rightInfo.NEW.Value,
                                       PayRate = rightInfo.PAYRATE.Value,
                                       CompAmt = rightInfo.COMPAMT.Value,
                                       NewShareCode = rightInfo.NEWSHARECODE,
                                       CompUnitNew = rightInfo.COMPUNITNEW.Value,
                                       CompUnitConfirm = rightInfo.COMPUNITCONFIRM.Value,
                                       Price = rightInfo.PRICE.Value,
                                       CloseDate = rightInfo.CLOSEDATE.Value.ToString("dd/MM/yyyy"),
                                       RightDate = rightInfo.RIGHTDATE.Value.ToString("dd/MM/yyyy"),
                                       TotalRow = rightInfo.RowCount
                        };


                        retData.Add(item);
                    }

                    return retData;
                }

            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetRightHist:accountNo=");
                throw;
            }
        }

        public async Task<int> RegistBuy(RegistBuyRequest model, string source)
        {
            try
            {
                using (var conn = new SqlConnection(_innoConnectionString))
                {
                    await conn.OpenAsync();

                    // Lấy thông tin quyền mua
                    var rightInfo =
                        await conn.QueryFirstOrDefaultAsync<SP_SEL_FO_RI_BYID_Result>("dbo.SP_SEL_FO_RI_BYID",
                            new {Id = model.ID}, commandType: System.Data.CommandType.StoredProcedure);

                    if (rightInfo != null)
                    {
                        if ("1".Equals(rightInfo.CONFIRMFLAG) && "0".Equals(rightInfo.DELFLAG) &&
                            rightInfo.COMPUNITCONFIRM > 0)
                            rightInfo.COMPUNITNEW -= rightInfo.COMPUNITCONFIRM;

                        var balanceIntraday = 0m;
                        var balanceIntradayData = await conn.QueryFirstOrDefaultAsync<BalanceIntradayQuery>
                            ("dbo.SP_SEL_FO_GETRIRINTRADAY_BYRRID", new {RRId = rightInfo.ID},
                                commandType: System.Data.CommandType.StoredProcedure);
                        if (balanceIntradayData != null)
                            balanceIntraday = balanceIntradayData.BalanceIntraday;

                        rightInfo.COMPUNITNEW = rightInfo.COMPUNITNEW - balanceIntraday - model.Volume;

                        // Call Stored Inno để insert vào DB
                        var contractNo = await conn.QueryFirstOrDefaultAsync<string>("dbo.SP_INS_RIGHTSTK",
                            new
                            {
                                ContractNo = "",
                                AccountNo = model.AccountNo,
                                Symbol = rightInfo.SYMBOL,
                                Price = rightInfo.PRICE,
                                Old = rightInfo.OLD,
                                New = rightInfo.NEW,
                                Payrate = rightInfo.PAYRATE,
                                CompAmt = rightInfo.COMPAMT,
                                CompUnitNew = rightInfo.COMPUNITNEW,
                                CompUnitConfirm = model.Volume,
                                XType = rightInfo.XTYPE,
                                Status = 1,
                                UserId = string.Empty,
                                RightDate = DateTime.Now,
                                TransferToDate = rightInfo.TRANSFERTODATE,
                                CloseDate = rightInfo.CLOSEDATE,
                                IsNew = 1,
                                Reason = string.Empty,
                                Source = source,
                                CustomerId = model.UserId,
                                EditTime = DateTime.Now,
                                ApproveTime = DateTime.Now,
                                ApproveId = string.Empty,
                                Volume = rightInfo.COMPUNITBFXR + rightInfo.COMPUNITDEP,
                                NewShareCode = model.Symbol,
                                ReceiverAccount = "",
                                ConfirmDate = rightInfo.CONFIRMDATE,
                                RRId = int.Parse(model.ID + ""),
                                RirNo = rightInfo.RIRNO
                            }, commandType: System.Data.CommandType.StoredProcedure);

                        return 1;
                    }
                    return 0;

                }
            }
            catch (Exception ex)
            {
                LogException(log, model.AccountNo, ex, "RegistBuy:accountNo=");
                throw;
            }
        }

        public async Task<int> CancelRegistBuy(string accountNo, string contractNo)
        {
            try
            {
                using (var conn = new SqlConnection(_innoConnectionString))
                {
                    await conn.OpenAsync();

                    // Lấy thông tin quyền mua theo ContractNo
                    var sql = "SELECT STATUS FROM RIGHTSTK WHERE CONTRACTNO=@ContractNo";
                    var status = await conn.QueryFirstOrDefaultAsync<int?>(sql, new {ContractNo = contractNo});

                    if (status != null)
                    {
                        var processStatusList = new List<int> {4, 5, 9, 7, 2};
                        if (processStatusList.Contains(status.Value))
                        {
                            return -1; // Trạng thái lệnh đặt thay đổi
                        }

                        if (status != 3)
                        {
                            var cancelStatus = 5;
                            sql =
                                "UPDATE RIGHTSTK SET [STATUS] = @Status, [edittime] = GETDATE() WHERE CONTRACTNO = @ContractNo";
                            await conn.ExecuteAsync(sql, new
                                                         {
                                                             Status = cancelStatus,
                                                             ContractNo = contractNo
                                                         });

                            return 1;
                        }
                    }

                    return 0;
                }
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "CancelRegistBuy:accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<int> TransferBuy(TransferBuyRequest model, string source)
        {
            try
            {
                using (var conn = new SqlConnection(_innoConnectionString))
                {
                    await conn.OpenAsync();
         
                    // Lấy thông tin quyền mua
                    var rightInfo =
                        await conn.QueryFirstOrDefaultAsync<SP_SEL_FO_RI_BYID_Result>("dbo.SP_SEL_FO_RI_BYID",
                            new {Id = model.ID}, commandType: System.Data.CommandType.StoredProcedure);

                    if (rightInfo != null)
                    {
                        if ("1".Equals(rightInfo.CONFIRMFLAG) && "0".Equals(rightInfo.DELFLAG) &&
                            rightInfo.COMPUNITCONFIRM > 0)
                            rightInfo.COMPUNITNEW -= rightInfo.COMPUNITCONFIRM;

                        var balanceIntraday = 0m;
                        var balanceIntradayData = await conn.QueryFirstOrDefaultAsync<BalanceIntradayQuery>
                            ("dbo.SP_SEL_FO_GETRIRINTRADAY_BYRRID", new {RRId = rightInfo.ID},
                                commandType: System.Data.CommandType.StoredProcedure);
                        if (balanceIntradayData != null)
                            balanceIntraday = balanceIntradayData.BalanceIntraday;

                        rightInfo.COMPUNITNEW = rightInfo.COMPUNITNEW - balanceIntraday - model.Volume;

                        // Call Stored Inno để insert vào DB
                        var contractNo = await conn.QueryFirstOrDefaultAsync<string>("dbo.SP_INS_RIGHTSTK",
                            new
                            {
                                ContractNo = "",
                                AccountNo = model.AccountNo,
                                Symbol = rightInfo.SYMBOL,
                                Price = rightInfo.PRICE,
                                Old = rightInfo.OLD,
                                New = rightInfo.NEW,
                                Payrate = rightInfo.PAYRATE,
                                CompAmt = rightInfo.COMPAMT,
                                CompUnitNew = rightInfo.COMPUNITNEW,
                                CompUnitConfirm = model.Volume,
                                XType = 4,
                                Status = 1,
                                UserId = string.Empty,
                                RightDate = DateTime.Now,
                                TransferToDate = rightInfo.TRANSFERTODATE,
                                CloseDate = rightInfo.CLOSEDATE,
                                IsNew = 1,
                                Reason = string.Empty,
                                Source = source,
                                CustomerId = model.UserId,
                                EditTime = DateTime.Now,
                                ApproveTime = DateTime.Now,
                                ApproveId = string.Empty,
                                Volume = rightInfo.COMPUNITBFXR + rightInfo.COMPUNITDEP,
                                NewShareCode = model.Symbol,
                                ReceiverAccount = model.AccountReceive,
                                ConfirmDate = rightInfo.CONFIRMDATE,
                                RRId = int.Parse(model.ID + ""),
                                RirNo = rightInfo.RIRNO
                            }, commandType: System.Data.CommandType.StoredProcedure);

                        return 1;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                LogException(log, model.AccountNo, ex, "TransferBuy:accountNo=" + model.AccountNo);
                throw;
            }
        }
    }
}
