﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using log4net;
using TVSI.Bond.WebAPI.Lib.Models.CashTransfer;
using TVSI.Bond.WebAPI.Lib.Models.CashTransfer.SqlData;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;
using System.Data.SqlClient;
using System.Linq;
using TVSI.Utility;
using System.Configuration;
using TVSI.Bond.WebAPI.Lib.Models;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class CashTransferService : BaseService, ICashTransferService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CashTransferService));
        public ICashTransferCommandText _commandText;
        private ISystemService _systemService;

        public CashTransferService(ICashTransferCommandText commandText, ISystemService systemService)
        {
            _commandText = commandText;
            _systemService = systemService;
        }

        public async Task<IEnumerable<BankInfo>> GetBankInfo(string custCode)
        {
            try
            {
                return await WithInnoConnection(async conn => await conn.QueryAsync<BankInfo>(
                        _commandText.GetBankInfo, new { CustCode = custCode }));
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "GetBankInfo:custCode=" + custCode);
                throw;
            }
        }

        public async Task<decimal> GetBalance(string accountNo)
        {
            try
            {
                return await WithDb2Connection(conn =>
                {
                    var balance = conn.QueryFirstOrDefault<decimal>(_commandText.GetBalance, new { AccountNo = accountNo });
                    return Task.FromResult(balance);
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetBalance:accountNo=" + accountNo);

                throw;
            }
        }

        public async Task<decimal> GetBuyHoldAmount(string accountNo)
        {
            try
            {
                return await WithTbmConnection(async conn =>
                {
                    var balance = await conn.QueryFirstOrDefaultAsync<decimal?>(_commandText.GetHoldAmount, new { AccountNo = accountNo });

                    return balance ?? 0;

                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetBuyHoldAmount:accountNo=" + accountNo);
                throw;
            }
        }

        public async Task<BankInfo> GetBankInfoByID(long id)
        {
            try
            {
                return await WithInnoConnection(async conn => await conn.QueryFirstOrDefaultAsync<BankInfo>(
                    _commandText.GetBankInfoByID, new { BankAccountID = id }));
            }
            catch (Exception ex)
            {
                LogException(log, "cash", ex, "GetBankInfoByID:id=" + id);
                throw;
            }
        }

        public async Task<IEnumerable<InternalAccountInfo>> GetInternalAccountInfo(string custCode, string accountNo)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryAsync<InternalAccountInfo>(
                        _commandText.GetInternalAccountInfo,
                        new
                        {
                            CustCode = custCode,
                            AccountNoLike = custCode + "%",
                            AccountNo = accountNo
                        });

                    return data;
                });

            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "GetInternalAccountInfo:custCode=" + custCode);
                throw;
            }
        }

        public async Task<InternalAccountInfo> GetInternalAccountInfoByID(string custCode, string internalAccountNo)
        {
            try
            {
                return await WithInnoConnection(async conn => await conn.QueryFirstOrDefaultAsync<InternalAccountInfo>(
                    _commandText.GetInternalAccountInfoByID, new { CustCode = custCode, InternalAccountNo = internalAccountNo, AccountNo = internalAccountNo }));
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "GetInternalAccountInfo:custCode=" + custCode + ":internalAccountNo="
                    + internalAccountNo);
                throw;
            }
        }

        public async Task<decimal> GetFirstBalance(string accountNo, DateTime fromDate)
        {
            try
            {
                return await WithXtradeInnoConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<decimal>("SP_SEL_FIRST_BALANCE",
                            new { AccountNo = accountNo, BeginDate = fromDate.ToString("yyyyMMdd") }
                            , commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetFirstBalance:accountNo=" + accountNo + ":fromDate="
                    + fromDate.ToString("dd/MM/yyyy"));
                throw;
            }
        }

        public async Task<TransactionFeeStatementResult> GetTransactionFeeStatementResult(string accountNo, string side, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var cashTransaction = await conn.QueryAsync<SP_SEL_FO_ORDERHISTORY>("SP_SEL_FO_ORDERHISTORY",
                               new
                               {
                                   AccountNo = accountNo,
                                   Symbol = "",
                                   Side = side,
                                   OrderStatus = 2,
                                   OrderChannel = "",
                                   BeginDate = fromDate.ToString("yyyyMMdd"),
                                   EndDate = toDate.ToString("yyyyMMdd"),
                                   PageIndex = pageIndex,
                                   PageSize = pageSize
                               }
                               , commandType: System.Data.CommandType.StoredProcedure);
                    var retData = new TransactionFeeStatementResult();
                    var data = new List<TransactionFeeStatementData>();
                    retData.TotalRow = 0;
                    retData.AccountNo = accountNo;
                    foreach (var item in cashTransaction)
                    {
                        data.Add(new TransactionFeeStatementData
                        {
                            TransactionDate = item.TransactionDate,
                            AccountNo = item.AccountNo,
                            Comm = item.Comm,
                            Symbol = item.SecSymbol,
                            MatchPrice = item.MatchPrice,
                            MatchValue = item.MatchValue,
                            MatchValue_Net = item.MatchValue_Net,
                            MatchVolume = item.MatchVolume,
                            OrderDate = item.OrderDate,
                            OrderNo = item.OrderNo,
                            OrderTime = item.OrderTime,
                            Price = item.Price,
                            Side = item.Side,
                            Volume = item.Volume,
                            Tax = item.Tax
                        });

                        retData.TotalRow = item.TOTAL_ROW;
                    }
                    retData.TransactionFeeStatementDatas = data;
                    return retData;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetTransactionFeeStatementResult:accountNo=" + accountNo + ":fromDate="
                    + fromDate.ToString("dd/MM/yyyy") + ":toDate="
                    + toDate.ToString("dd/MM/yyyy"));
                throw;
            }
        }
        public async Task<InComeAndExpenseResult> GetInComeAndExpenseResult(string accountNo, string symbol, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var data = await conn.QueryAsync<InComeAndExpenseResult>("FO_CM_RGRL_GetInComeAndExpense",
                               new
                               {
                                   AccountNo = accountNo,
                                   Symbol = "",
                                   BeginDate = fromDate.ToString("yyyyMMdd"),
                                   EndDate = toDate.ToString("yyyyMMdd"),
                                   PageIndex = 0,
                                   PageSize = 0
                               }
                               , commandType: System.Data.CommandType.StoredProcedure);                    
                    return data.FirstOrDefault();
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetInComeAndExpense:accountNo=" + accountNo + ":fromDate="
                    + fromDate.ToString("dd/MM/yyyy") + ":toDate="
                    + toDate.ToString("dd/MM/yyyy"));
                throw;
            }
        }

        public async Task<RealizeGainLossResult> GetRealizeGainLostResult(string accountNo, string symbol, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var cashTransaction = await conn.QueryAsync<SP_SEL_FO_RGRL>("SP_SEL_FO_RGRL",
                               new
                               {
                                   AccountNo = accountNo,
                                   Symbol = symbol,
                                   BeginDate = fromDate.ToString("yyyyMMdd"),
                                   EndDate = toDate.ToString("yyyyMMdd"),
                                   PageIndex = pageIndex,
                                   PageSize = pageSize
                               }
                               , commandType: System.Data.CommandType.StoredProcedure);
                    var retData = new RealizeGainLossResult();
                    var data = new List<RealizeGainLossData>();
                    retData.TotalRow = 0;
                    retData.AccountNo = accountNo;
                    foreach (var item in cashTransaction)
                    {
                        data.Add(new RealizeGainLossData
                        {
                            TrateDate = item.TRADEDATE,
                            BuyPrice = item.BUYPRICE,
                            SellPrice = item.SELLPRICE,
                            GainLost = item.GAINLOSS,
                            PercentGL = item.PERCENTGL,
                            Symbol = item.SYMBOL,
                            Volume = item.VOLUME
                        });

                        retData.TotalRow = item.TOTAL_ROW;
                        retData.TotalGainLoss = item.SELLVOLUME - item.BUYVOLUME;
                        retData.PercentGL = item.BUYVOLUME != 0 ? (item.SELLVOLUME - item.BUYVOLUME) * 100 / item.BUYVOLUME : 0M;
                    }
                    retData.RealizeGainLossDatas = data;
                    return retData;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetRealizeGainLostResult:accountNo=" + accountNo + ":fromDate="
                    + fromDate.ToString("dd/MM/yyyy") + ":toDate="
                    + toDate.ToString("dd/MM/yyyy"));
                throw;
            }
        }

        public async Task<CashTransactionDetailResult> GetCashTransactionDetailResult(string accountNo, string symbol, string transType, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var cashTransaction = await conn.QueryAsync<SP_SEL_CASHTRANS_DETAIL_Result>("FO_SEL_CASHTRANS_DETAIL",
                               new
                               {
                                   AccountNo = accountNo,
                                   Symbol = symbol,
                                   TransType = transType,
                                   BeginDate = fromDate.ToString("yyyyMMdd"),
                                   EndDate = toDate.ToString("yyyyMMdd"),
                                   PageIndex = pageIndex,
                                   PageSize = pageSize
                               }
                               , commandType: System.Data.CommandType.StoredProcedure);
                    var retData = new CashTransactionDetailResult();
                    var data = new List<CashTransactionDetailData>();
                    retData.TotalRow = 0;
                    retData.AccountNo = accountNo;
                    foreach (var item in cashTransaction)
                    {
                        data.Add(new CashTransactionDetailData
                        {
                            TradeDate = item.TRANSDATE.ToString("dd/MM/yyyy"),
                            Remark = item.REMARK,
                            CashTransNo = item.TRANS_NO,
                            Symbol = !string.IsNullOrEmpty(item.STOCKSYMBOL) ? item.STOCKSYMBOL.Trim() : "",
                            Amount = item.AMOUNT,
                            TransactionName = ResourceFile.CashTransferModule.ResourceManager.GetString("CashTransactionHist_" + item.TRANS_TYPE1.Trim() + "1")
                        });

                        retData.TotalRow = item.ROWCOUNT;
                    }
                    retData.CashTransactionDatas = data;
                    return retData;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetCashTransactionDetailResult:accountNo=" + accountNo + ":fromDate="
                    + fromDate.ToString("dd/MM/yyyy") + ":toDate="
                    + toDate.ToString("dd/MM/yyyy"));
                throw;
            }
        }
        public async Task<CashTransactionResult> GetCashTransactionResult(string accountNo, DateTime fromDate,
            DateTime toDate, int pageIndex, int pageSize)
        {
            try
            {
                return await WithInnoConnection(async conn =>
                {
                    var cashBalanceFirst = await conn.QueryFirstOrDefaultAsync<decimal>("SP_SEL_FIRST_BALANCE",
                            new { AccountNo = accountNo, BeginDate = fromDate.ToString("yyyyMMdd") }
                            , commandType: System.Data.CommandType.StoredProcedure);

                    var cashTransaction = await conn.QueryAsync<SP_SEL_CASHTRANS_Result>("SP_SEL_CASHTRANS",
                            new
                            {
                                AccountNo = accountNo,
                                //OpenBal = cashBalanceFirst,
                                //CloseBal = 0,
                                BeginDate = fromDate.ToString("yyyyMMdd"),
                                EndDate = toDate.ToString("yyyyMMdd"),
                                PageIndex = pageIndex,
                                PageSize = pageSize,
                                Option = 0
                            }
                            , commandType: System.Data.CommandType.StoredProcedure);


                    var retData = new CashTransactionResult();

                    retData.AccountNo = accountNo;
                    retData.CashBalanceFirst = cashBalanceFirst;
                    retData.CashTransactionDatas = new List<CashTransactionData>();
                    retData.TotalRow = 0;

                    if (cashTransaction.Count() > 0)
                    {
                        retData.CashBalanceWithdraw = cashTransaction.ToList()[0].MoneyWaitPayment.HasValue
                            ? cashTransaction.ToList()[0].MoneyWaitPayment.Value
                            : 0;
                    }

                    foreach (var item in cashTransaction)
                    {
                        var cashTransItem = new CashTransactionData();
                        cashTransItem.TradeDate = item.DUEDATE.ToString("dd/MM/yyyy");

                        var transDate = item.TRANSDATE.ToString("dd/MM/yyyy");
                        var volume = item.VOLUME.HasValue ? item.VOLUME.Value.ToString("N0") : "";
                        var price = item.PRICE.HasValue ? item.PRICE.Value.ToString("N0") : "";
                        var transType1 = item.TRANS_TYPE1.Trim();
                        var transType2 = item.TRANS_TYPE2.Trim();
                        var symbol = !string.IsNullOrEmpty(item.STOCKSYMBOL) ? item.STOCKSYMBOL.Trim() : "";
                        retData.TotalRow = item.RowCount;
                        switch (transType1)
                        {
                            case "ADVAMT":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_ADVAMT, transDate);
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "ADVFEE":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_ADVFEE, transDate);
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "ADVVAT":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_ADVVAT, transDate);
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "BU":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_BU_SE,
                                    ResourceFile.OrderModule.Side_B, volume, symbol, price, transDate);
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "SE":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_BU_SE,
                                    ResourceFile.OrderModule.Side_S, volume, symbol, price, transDate);
                                cashTransItem.Amount = item.AMOUNT;
                                break;

                            case "COMM":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_COMM,
                                    item.TRANS_NO.Contains("SE") ? ResourceFile.OrderModule.Side_S : ResourceFile.OrderModule.Side_B,
                                    volume, symbol, price, transDate);
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "DH":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_DH,
                                    item.REMARK);
                                cashTransItem.Amount = item.AMOUNT;
                                break;

                            case "IN":
                                cashTransItem.TransactionName = ResourceFile.CashTransferModule.CashTransactionHist_IN;
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "IP":
                                cashTransItem.TransactionName = ResourceFile.CashTransferModule.CashTransactionHist_IP;
                                cashTransItem.Amount = item.AMOUNT;
                                break;

                            case "IPTAX":
                                cashTransItem.TransactionName = ResourceFile.CashTransferModule.CashTransactionHist_IPTAX;
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "LOANIN":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_LOANIN, transDate);
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "MTHINT":
                                cashTransItem.TransactionName = ResourceFile.CashTransferModule.CashTransactionHist_MTHINT;
                                cashTransItem.Amount = item.AMOUNT;
                                break;

                            case "OI":
                                cashTransItem.TransactionName = ResourceFile.CashTransferModule.CashTransactionHist_OI;
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "PAYOT":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_PAYOT,
                                    item.REMARK);
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "RECADV":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_RECADV, transDate);
                                cashTransItem.Amount = item.AMOUNT;
                                break;

                            case "RECOT":
                                cashTransItem.TransactionName = ResourceFile.CashTransferModule.CashTransactionHist_RECOT;
                                cashTransItem.Amount = item.AMOUNT;
                                break;

                            case "RECREF":
                                cashTransItem.TransactionName = ResourceFile.CashTransferModule.CashTransactionHist_RECREF;
                                cashTransItem.Amount = item.AMOUNT;
                                break;

                            case "TAX":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_TAX,
                                    item.TRANS_NO.Contains("SE") ? ResourceFile.OrderModule.Side_S : ResourceFile.OrderModule.Side_B,
                                    volume, symbol, price, transDate);
                                cashTransItem.Amount = -item.AMOUNT;

                                break;

                            case "WH":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_WH,
                                    item.REMARK);
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "XD":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_XD,
                                        volume, symbol, transDate);
                                cashTransItem.Amount = item.AMOUNT;
                                break;

                            case "XD-":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_XD_,
                                        volume, symbol, transDate);
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "XI-":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_XI_,
                                        volume, symbol, transDate);
                                cashTransItem.Amount = -item.AMOUNT;
                                break;

                            case "XI+":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_XI_Plus,
                                        volume, symbol, transDate);
                                cashTransItem.Amount = item.AMOUNT;
                                break;

                            case "XR":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_XR,
                                        volume, symbol, transDate);
                                cashTransItem.Amount = "1".Equals(transType2) ? -item.AMOUNT : item.AMOUNT;
                                break;

                            case "XRODD":
                                cashTransItem.TransactionName = string.Format(ResourceFile.CashTransferModule.CashTransactionHist_XRODD,
                                        symbol, transDate);
                                cashTransItem.Amount = item.AMOUNT;
                                break;

                            default:
                                cashTransItem.TransactionName = ResourceFile.CashTransferModule.CashTransactionHist_OTHER;
                                cashTransItem.Amount = "1".Equals(transType2) ? -item.AMOUNT : item.AMOUNT;
                                break;
                        }

                        //// Cac truong hop phat sinh giam
                        //if ("XR".Equals(item.TRANS_TYPE1) && "1".Equals(item.TRANS_TYPE2))
                        //{
                        //    cashTransItem.Amount = -item.AMOUNT;
                        //}
                        //else if ("WH,BU,COMM,TAX,ADVFEE,ADVVAT,ADVAMT,XD-,IPTAX,TAX,PAYOT,XI-,OI,LOANIN,IN".Contains(item.TRANS_TYPE1))
                        //{
                        //    cashTransItem.Amount = -item.AMOUNT;
                        //}

                        retData.CashTransactionDatas.Add(cashTransItem);
                    }

                    return retData;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetCashTransactionResult:accountNo=" + accountNo + ":fromDate="
                    + fromDate.ToString("dd/MM/yyyy") + ":toDate="
                    + toDate.ToString("dd/MM/yyyy"));
                throw;
            }
        }

        public async Task<CashBalanceInfo> GetNormalAccountFromDB2(string accountNo)
        {
            CashBalanceInfo returnData = new CashBalanceInfo();
            returnData.AccountNo = accountNo;
            try
            {
                var sql = string.Format("SELECT A.*, COALESCE(TOTALBUY, 0) TOTALBUY FROM (" +
                           "  SELECT ACCOUNTNO, CASHBALANCE * 1000 CASHBALANCE, COALESCE(T_BUY, 0) * 1000 T_BUY, PACKAGETYPE FROM ACCOUNTTAB WHERE ACCOUNTNO = '{0}' ) AS A " +
                           " LEFT JOIN ( SELECT ACCOUNTNO, SUM(TOTAL_BUY) TOTALBUY FROM ( " +
                           "    SELECT A.ORDERNO, A.ACCOUNTNO, A.SECSYMBOL, (A.VOLUME - COALESCE(SUM(B.DEALVOLUME), 0)) * A.PRICE * 1000 AS TOTAL_BUY FROM ORDERTAB A " +
                           "    LEFT JOIN DEALTAB B ON A.ORDERNO = B.ORDERNO WHERE A.ACCOUNTNO = '{0}' AND A.SIDE = 'B' AND A.ORDERSTATUS IN ('A','O','OA','OAC','PO') " +
                           "    GROUP BY A.ORDERNO, B.ORDERNO, A.PRICE, A.VOLUME, A.SECSYMBOL, A.ACCOUNTNO ) AS A GROUP BY ACCOUNTNO ) AS B ON A.ACCOUNTNO = B.ACCOUNTNO ", accountNo);

                return await WithDb2Connection(conn =>
                {
                    var balanceInfo = conn.QueryFirstOrDefault<CashNormalAccountInfoDB2>(sql, commandType: System.Data.CommandType.Text);
                    var cashBalance = 0M;
                    if (balanceInfo != null)
                    {
                        cashBalance = balanceInfo.CASHBALANCE;
                        var feeRate = GetPackageTypeInfo(balanceInfo.PACKAGETYPE);
                        cashBalance = cashBalance - balanceInfo.T_BUY * (1 + (feeRate / 100)) - balanceInfo.TOTALBUY * (1 + (feeRate / 100));
                    }
                    var phi_tam_tinh = GetBlockFeeInfo(accountNo, false);
                    var temp = GetTempWithdrawal(accountNo);
                    returnData.Balance = cashBalance;
                    returnData.BlockFee = phi_tam_tinh;
                    returnData.FastWithdrawal = cashBalance - temp - phi_tam_tinh;
                    returnData.MaxWithdrawal = cashBalance - temp - phi_tam_tinh;
                    returnData.MinMarginRate = 1;
                    returnData.Withdrawal = cashBalance - temp - phi_tam_tinh;

                    return Task.FromResult(returnData);
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetNormalAccountFromDB2:accountNo=" + accountNo);
            }
            return returnData;
        }

        public async Task<CashBalanceInfo> GetMarginAccountFromDB2(string accountNo, bool isConfig)
        {
            CashBalanceInfo returnData = new CashBalanceInfo();
            returnData.AccountNo = accountNo;
            var cashInfo = await GetDataCalcMarginAccountInfoDB2(accountNo);
            if (cashInfo != null)
            {
                var phi_tam_tinh = GetBlockFeeInfo(accountNo, false);
                var so_tien_dang_rut = GetTempWithdrawal(accountNo);
                string config = ConfigurationManager.AppSettings["IS_WH_FLAG"];

                if (string.IsNullOrEmpty(config))
                    config = "false";

                if (cashInfo.WH_FLAG == "Y" && config == "true")
                {
                    var theo_quyen = false;
                    var co_ck_dang_ve = false;
                    var co_danh_muc = false;
                    var ty_le_ky_quy = 0M;
                    var ty_trong_mack = 0M;

                    var stockInfo = await GetStockBalanceInfoDB2(accountNo);
                    if (stockInfo != null && stockInfo.ASSETTOLMV > 0)
                    {
                        co_danh_muc = true;
                        ty_trong_mack = stockInfo.ASSETTOLMV;
                        if (ty_trong_mack > 1)
                            ty_trong_mack = 1;
                        ty_le_ky_quy = stockInfo.MRGRATE / 100M;
                        if (stockInfo.BUYVOLUMET1T2 > 0)
                            co_ck_dang_ve = true;
                        if (stockInfo.R > 0)
                            theo_quyen = true;
                    }

                    var ty_le_duy_tri = cashInfo.BRK_SELL_LMV;
                    if (isConfig)
                    {
                        ty_le_duy_tri = GetRateCashLoan(cashInfo.MARGIN_GROUP, theo_quyen, co_ck_dang_ve, co_danh_muc, ty_le_ky_quy, ty_trong_mack);
                        ty_le_duy_tri = ty_le_duy_tri < cashInfo.BRK_SELL_LMV ? cashInfo.BRK_SELL_LMV : ty_le_duy_tri;
                        returnData.MinMarginRate = ty_le_duy_tri;
                    }
                    else
                        returnData.MinMarginRate = GetRateCashLoan(cashInfo.MARGIN_GROUP, theo_quyen, co_ck_dang_ve, co_danh_muc, ty_le_ky_quy, ty_trong_mack);
                    var so_tien_lenh_dang_ung = 0M;

                    LogInfo(log, accountNo, string.Format("Ty le duy tri:{0}", ty_le_duy_tri));

                    LogInfo(log, accountNo, string.Format("IsConfig:{0}", isConfig));
                    returnData.Balance = cashInfo.CASH_BALANCE;
                    if (ty_le_duy_tri > 0)
                    {
                        var phi_ung_tien_tam_tinh = 0M;
                        if (cashInfo.AP + cashInfo.AP_T1 > 0)
                        {
                            int so_ngay_ung = 0;
                            var date1 = await _systemService.GetNextTradeDateByDate(DateTime.Today);
                            so_ngay_ung = ((TimeSpan)(date1 - DateTime.Today)).Days;
                            if (so_ngay_ung < 0)
                                so_ngay_ung = 0;
                            if (cashInfo.AP_T1 > 0)
                                phi_ung_tien_tam_tinh = GetAdvanceFee(accountNo, DateTime.Today.ToString("yyyy-MM-dd"),
                                    Functions.GetDouble(cashInfo.AP_T1), so_ngay_ung);

                            DateTime time2 = await _systemService.GetNextTradeDateByDate(date1);
                            so_ngay_ung = ((TimeSpan)(time2 - DateTime.Today)).Days;
                            if (so_ngay_ung < 0)
                                so_ngay_ung = 0;
                            if (cashInfo.AP > 0)
                                phi_ung_tien_tam_tinh = phi_ung_tien_tam_tinh + GetAdvanceFee(accountNo, DateTime.Today.ToString("yyyy-MM-dd"),
                                    Functions.GetDouble(cashInfo.AP), so_ngay_ung);
                        }
                        var so_tien_rut_toi_da = cashInfo.CASH_BALANCE + cashInfo.LMV + cashInfo.AP + cashInfo.AP_T1 + cashInfo.COLLAT + cashInfo.P_BUYUNMATCH - cashInfo.DEBT - cashInfo.BUYUNMATCH -
                                cashInfo.LOAN_INTEREST - phi_tam_tinh - so_tien_dang_rut + so_tien_lenh_dang_ung - cashInfo.ADV_WITHDRAW - phi_ung_tien_tam_tinh -
                                (cashInfo.LMV + cashInfo.COLLAT + cashInfo.PPAPP + cashInfo.P_BUYUNMATCH) * ty_le_duy_tri;

                        var so_tien_rut_toi_da_force_lmv = cashInfo.CASH_BALANCE + cashInfo.LMV + cashInfo.AP + cashInfo.AP_T1 + cashInfo.COLLAT + cashInfo.P_BUYUNMATCH - cashInfo.DEBT - cashInfo.BUYUNMATCH -
                            cashInfo.LOAN_INTEREST - phi_tam_tinh - so_tien_dang_rut + so_tien_lenh_dang_ung - cashInfo.ADV_WITHDRAW - phi_ung_tien_tam_tinh -
                             (cashInfo.LMV + cashInfo.COLLAT + cashInfo.PPAPP + cashInfo.P_BUYUNMATCH) * cashInfo.BRK_SELL_LMV;

                        var so_tien_rut_nhanh = Math.Min(Math.Max(cashInfo.EE, 0), cashInfo.CASH_BALANCE + cashInfo.LMV + cashInfo.AP + cashInfo.AP_T1 + cashInfo.COLLAT + cashInfo.P_BUYUNMATCH - cashInfo.DEBT - cashInfo.BUYUNMATCH -
                                cashInfo.LOAN_INTEREST - phi_tam_tinh - so_tien_dang_rut + so_tien_lenh_dang_ung - cashInfo.ADV_WITHDRAW - phi_ung_tien_tam_tinh -
                                (cashInfo.LMV + cashInfo.COLLAT + cashInfo.PPAPP + cashInfo.P_BUYUNMATCH) * 0.5M);
                        if (ty_le_duy_tri > 0.5M)
                        {
                            so_tien_rut_nhanh = Math.Min(Math.Max(cashInfo.EE - so_tien_dang_rut, 0), cashInfo.CASH_BALANCE + cashInfo.LMV + cashInfo.AP + cashInfo.AP_T1 + cashInfo.COLLAT + cashInfo.P_BUYUNMATCH - cashInfo.DEBT - cashInfo.BUYUNMATCH -
                                cashInfo.LOAN_INTEREST - phi_tam_tinh - so_tien_dang_rut + so_tien_lenh_dang_ung - cashInfo.ADV_WITHDRAW - phi_ung_tien_tam_tinh -
                                (cashInfo.LMV + cashInfo.COLLAT + cashInfo.PPAPP + cashInfo.P_BUYUNMATCH) * ty_le_duy_tri);
                        }

                        int isMH6 = CheckCashLoanMH6(cashInfo.MARGIN_GROUP);

                        LogInfo(log, accountNo, string.Format("ISMH6:{0}", isMH6));

                        if (isMH6 > 0)
                        {

                            LogInfo(log, accountNo, string.Format("EE:{0}; Interest: {1}; phi_ung:{2}", cashInfo.EE, cashInfo.LOAN_INTEREST, phi_ung_tien_tam_tinh));

                            so_tien_rut_nhanh = cashInfo.EE - cashInfo.LOAN_INTEREST - phi_tam_tinh - so_tien_dang_rut - phi_ung_tien_tam_tinh;
                            so_tien_rut_toi_da = so_tien_rut_nhanh;

                            if (cashInfo.LMV + cashInfo.COLLAT + cashInfo.PPAPP + cashInfo.P_BUYUNMATCH != 0)
                            {
                                ty_le_duy_tri = (cashInfo.CASH_BALANCE + cashInfo.LMV + cashInfo.AP + cashInfo.AP_T1 + cashInfo.COLLAT + cashInfo.P_BUYUNMATCH - cashInfo.DEBT - cashInfo.BUYUNMATCH -
                                cashInfo.LOAN_INTEREST - phi_tam_tinh - so_tien_dang_rut - so_tien_rut_nhanh - cashInfo.ADV_WITHDRAW - phi_ung_tien_tam_tinh) / (cashInfo.LMV + cashInfo.COLLAT + cashInfo.PPAPP + cashInfo.P_BUYUNMATCH);
                                ty_le_ky_quy = (cashInfo.CASH_BALANCE + cashInfo.LMV + cashInfo.AP + cashInfo.AP_T1 + cashInfo.COLLAT + cashInfo.P_BUYUNMATCH - cashInfo.DEBT - cashInfo.BUYUNMATCH -
                                cashInfo.LOAN_INTEREST - phi_tam_tinh - so_tien_dang_rut - so_tien_rut_nhanh - cashInfo.ADV_WITHDRAW - phi_ung_tien_tam_tinh) / (cashInfo.LMV + cashInfo.COLLAT + cashInfo.PPAPP + cashInfo.P_BUYUNMATCH);

                                LogInfo(log, accountNo, string.Format("Ty le duy tri:{0}", ty_le_duy_tri));
                            }
                            else
                            {
                                ty_le_duy_tri = 1;
                                ty_le_ky_quy = 1;
                            }
                            if (!isConfig)
                            {
                                ty_le_duy_tri = cashInfo.BRK_SELL_LMV;
                            }
                            returnData.MinMarginRate = ty_le_duy_tri;
                            returnData.FastWithdrawal = so_tien_rut_nhanh;
                            returnData.MaxWithdrawal = so_tien_rut_toi_da;
                            returnData.BlockFee = phi_tam_tinh;
                            returnData.Withdrawal = cashInfo.WITHDRAWAL - phi_tam_tinh - so_tien_dang_rut + so_tien_lenh_dang_ung;
                        }

                        returnData.MinMarginRate = ty_le_duy_tri;
                        returnData.FastWithdrawal = so_tien_rut_nhanh;
                        returnData.MaxWithdrawal = so_tien_rut_toi_da;
                        returnData.BlockFee = phi_tam_tinh;
                        returnData.Withdrawal = cashInfo.WITHDRAWAL - phi_tam_tinh - so_tien_dang_rut + so_tien_lenh_dang_ung;
                    }
                }
                else
                {
                    returnData.Balance = cashInfo.CASH_BALANCE;
                    returnData.MinMarginRate = 1;
                    returnData.FastWithdrawal = cashInfo.WITHDRAWAL - phi_tam_tinh - so_tien_dang_rut;
                    returnData.MaxWithdrawal = cashInfo.WITHDRAWAL - phi_tam_tinh - so_tien_dang_rut;
                    returnData.BlockFee = phi_tam_tinh;
                    returnData.Withdrawal = cashInfo.WITHDRAWAL - phi_tam_tinh - so_tien_dang_rut;
                }
            }
            return returnData;
        }

        public decimal GetPackageTypeInfo(string package)
        {
            try
            {
                using (var conn = new SqlConnection(_emsdbConnectionString))
                {
                    var balance = conn.QueryFirstOrDefault<decimal>("TVSI_sPHI_DAT_LENH_GETTYLE", new { TVSI_TEN_GOI = package }, commandType: System.Data.CommandType.StoredProcedure);
                    return balance;
                }
            }
            catch (Exception ex)
            {
                LogException(log, "cash", ex, "GetPackageTypeInfo:package=" + package);
                return 0;
            }
        }

        public async Task<CalcMarginAccountInfoDB2> GetDataCalcMarginAccountInfoDB2(string accountNo)
        {
            string sql = string.Format("SELECT A.*,COALESCE(C.WH_FLAG, 'N') WH_FLAG, COALESCE(B.P_BUYUNMATCH, 0) * 1000 P_BUYUNMATCH " +
                            "FROM ( " +
                            "    SELECT  A.CUST_ID ACCOUNTNO, A.CASH_BALANCE * 1000 CASH_BALANCE, A.WITHDRAWAL * 1000 WITHDRAWAL, A.LMV * 1000 LMV, COALESCE(A.AP, 0) * 1000 AP, COALESCE(A.AP_T1, 0) * 1000 AP_T1, COALESCE(A.COLLAT, 0) * 1000 COLLAT, A.DEBT * 1000 DEBT, A.BUYUNMATCH * 1000 BUYUNMATCH, A.LOAN_INTEREST * 1000 LOAN_INTEREST, " +
                            "            A.ADV_WITHDRAW * 1000 ADV_WITHDRAW, A.EE * 1000 EE, A.PP * 1000 PP, A.CALL_FORCE * 1000 CALL_FORCE, B.MRGCODE, BRK_SELL_LMV, A.EEAPP * 1000 EEAPP, " +
                            "           CASE WHEN A.EEAPP > 0 THEN ((EE + A.EEAPP) * 100 / B.MRGCODE ) ELSE 0 END * 1000  PPAPP, A.MARGIN_GROUP " +
                            "    FROM ACCOUNTTAB_CB A " +
                            "    LEFT JOIN ACCOUNTTAB B " +
                            "    ON A.CUST_ID = B.ACCOUNTNO " +
                            "    WHERE A.CUST_ID = '{0}' " +
                            "    ) AS A " +
                            "LEFT JOIN  " +
                            "    ( " +
                            "    SELECT ACCOUNTNO, SUM(BUYUNMATCH * PLEDGERATE/100) P_BUYUNMATCH  " +
                            "    FROM ( " +
                            "        SELECT  A.*, PLEDGERATE  " +
                            "        FROM (  " +
                            "                SELECT ACCOUNTNO, SECSYMBOL, SUM((VOLUME - MATCHVOLUME) * PRICE) BUYUNMATCH, MAX(C.BUYUNMATCH) BUYUNMATCH_ACC, MARGIN_GROUP   " +
                            "                FROM ORDERTAB A  " +
                            "                LEFT JOIN ACCOUNTTAB_CB C  " +
                            "                ON A.ACCOUNTNO = C.CUST_ID  " +
                            "                WHERE ORDERSTATUS in ('A','O','OA','OAC','PO') AND ACCTYPE = 'B' AND SIDE = 'B'  " +
                            "                GROUP BY ACCOUNTNO, A.SECSYMBOL, MARGIN_GROUP) AS A  " +
                            "                LEFT JOIN MARGINGRPTAB B  " +
                            "                ON A.MARGIN_GROUP = B.MARGIN_GROUP AND A.SECSYMBOL = B.SYMBOL  " +
                            "                WHERE ACCOUNTNO = '{0}' " +
                            "        ) AS A " +
                            "        GROUP BY ACCOUNTNO  " +
                            ") AS B " +
                            "ON A.ACCOUNTNO = B.ACCOUNTNO LEFT JOIN CUSCTRLTAB C ON A.ACCOUNTNO = C.ACCOUNTID", accountNo);
            try
            {
                return await WithDb2Connection(conn =>
                {
                    var balanceInfo = conn.QueryFirstOrDefaultAsync<CalcMarginAccountInfoDB2>(
                        sql, commandType: System.Data.CommandType.Text);
                    return balanceInfo;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetDataCalcMarginAccountInfoDB2:accountNo=" + accountNo);
                return null;
            }
        }

        public async Task<StockBalanceInfoDB2> GetStockBalanceInfoDB2(string accountNo)
        {
            #region sql
            string sql = string.Format(" call TVSI_SGET_STOCKINFO_CALC_EE_WH('{0}')", accountNo);
            #endregion
            try
            {
                return await WithDb2Connection(conn =>
                {
                    var data = conn.QueryFirstOrDefaultAsync<StockBalanceInfoDB2>(sql, commandType: System.Data.CommandType.Text);
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetDataCalcMarginAccountInfoDB2:accountNo=" + accountNo);
                return null;
            }
        }

        public decimal GetRateCashLoan(string marginGroup, bool theo_quyen, bool co_ck_dang_ve, bool co_danh_muc, decimal ty_le_ky_quy, decimal ty_trong_mck)
        {
            try
            {
                using (var conn = new SqlConnection(_ipgdbConnectionString))
                {
                    var data = conn.QueryFirstOrDefault<decimal>("TVSI_sFS_TINH_TY_LE_DUY_TRI_CHO_VAY_RUT_TIEN",
                        new
                        {
                            goi_dich_vu = marginGroup,
                            theo_quyen = theo_quyen,
                            co_ck_dang_ve = co_ck_dang_ve,
                            co_danh_muc = co_danh_muc,
                            ty_le_ky_quy = ty_le_ky_quy,
                            ty_trong_mack = ty_trong_mck
                        },
                        commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                }
            }
            catch (Exception ex)
            {
                LogException(log, "cash", ex, "GetRateLoan");
                return 0;
            }
        }

        public int CheckCashLoanMH6(string marginGroup)
        {
            try
            {
                using (var conn = new SqlConnection(_ipgdbConnectionString))
                {
                    var data = conn.Query("TVSI_sFS_TINH_TY_LE_DUY_TRI_CHO_VAY_RUT_TIEN_CHECK_MH6",
                        new
                        {
                            goi_dich_vu = marginGroup,
                            ty_le_ky_quy = 1,
                        },
                        commandType: System.Data.CommandType.StoredProcedure).ToList();
                    return data.Count;
                }
            }
            catch (Exception ex)
            {
                LogException(log, "cash", ex, "CheckCashLoanMH6");
                return 0;
            }
        }


        public bool CheckNoHopDongKYC(string custCode)
        {
            try
            {
                using (var conn = new SqlConnection(_commondbConnectionString))
                {
                    var data = conn.Query<object>("TVSI_sTF_SO_TIEN_TAM_RUT_CUA_TAI_KHOAN_TU_INTERNET",
                        new
                        {
                            CustCode = custCode
                        },
                        commandType: System.Data.CommandType.StoredProcedure).ToList();
                    return data.Count > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                LogException(log, custCode, ex, "CheckNoHopDongKYC");
                return false;
            }
        }


        public decimal GetAdvanceFee(string accountNo, string ngayUng, double amountAdv, int so_ngay)
        {
            var sbaWS = new TVSISBAWS.TVSISBAWS();
            var xml_SBAInfo120 = sbaWS.SBAInfo120("tvsi", "tvsi", "tvsi", accountNo, ngayUng, amountAdv, so_ngay);

            TVSIWebservice.Entity.SBAInfoMessage.SBAInfo121 SBAInfo121Entity = new TVSIWebservice.Entity.SBAInfoMessage.SBAInfo121(xml_SBAInfo120);

            if (!SBAInfo121Entity.ToString().Equals("INVALID_OBJECT") || xml_SBAInfo120.IndexOf("SBAINFO121") > 0)
            {
                LogInfo(log, accountNo, "SBAInfo121Entity: " + SBAInfo121Entity.So_Tai_Khoan + "," + SBAInfo121Entity.Ngay_Ung_Tien + "," +
                                   amountAdv.ToString() + "," + so_ngay.ToString());
                return decimal.Parse(SBAInfo121Entity.Phi_Ung_Tien.ToString());
            }
            else
            {
                LogInfo(log, accountNo, "Khong lay duoc thong tin ung tien: " + accountNo + "," + ngayUng + "," +
                                    amountAdv.ToString() + "," + so_ngay.ToString());
                return 0;
            }
        }

        public decimal GetTempWithdrawal(string accountNo)
        {
            try
            {
                using (var conn = new SqlConnection(_ipgdbConnectionString))
                {
                    var data = conn.QueryFirstOrDefault<decimal>("TVSI_sTF_SO_TIEN_TAM_RUT_CUA_TAI_KHOAN_TU_INTERNET",
                        new
                        {
                            tvsi_account = accountNo
                        },
                        commandType: System.Data.CommandType.StoredProcedure);
                    return data;
                }
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetTempWithdrawal");
                return 0;
            }

        }

        public decimal GetBlockFeeInfo(string accountNo, bool sms)
        {
            try
            {
                using (var conn = new SqlConnection(_emsdbConnectionString))
                {
                    var data = conn.Query<BlockFeeInfo>("TVSI_sTHONG_TIN_PHI_getACCOUNT",
                        new
                        {
                            so_tai_khoan = accountNo
                        },
                        commandType: System.Data.CommandType.StoredProcedure).ToList();
                    //if (sms)
                    return data.Sum(x => x.So_tien);
                    //return data.Where(x => x.Loai_phi != "SMS").Sum(x => x.So_tien);
                }
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetBlockFeeInfo");
                return 0;
            }
        }

        public List<ShowBlockFeeInfo> GetBlockFeeInfos(string accountNo, string lang)
        {
            try
            {
                using (var conn = new SqlConnection(_emsdbConnectionString))
                {
                    var data = conn.Query<ShowBlockFeeInfo>("TVSI_sTHONG_TIN_PHI_GetBlockFees",
                        new
                        {
                            so_tai_khoan = accountNo,
                            lang = lang
                        },
                        commandType: System.Data.CommandType.StoredProcedure).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                LogException(log, accountNo, ex, "GetBlockFeeInfos");
                return new List<ShowBlockFeeInfo>();
            }
        }

        public async Task<AfterWithdrawalInfoResult> GetAfterWithdrawalInfo(string accountNo, decimal so_tien)
        {
            var data = new AfterWithdrawalInfoResult();
            var cashInfo = await GetDataCalcMarginAccountInfoDB2(accountNo);
            if (cashInfo != null)
            {
                var phi_tam_tinh = GetBlockFeeInfo(accountNo, false);
                var so_tien_dang_rut = GetTempWithdrawal(accountNo);
                data.EE = cashInfo.EE - phi_tam_tinh - so_tien_dang_rut;

                var phi_ung_tien_tam_tinh = 0M;
                if (cashInfo.AP + cashInfo.AP_T1 > 0)
                {
                    int so_ngay_ung = 0;
                    var date1 = await _systemService.GetNextTradeDateByDate(DateTime.Today);
                    so_ngay_ung = ((TimeSpan)(date1 - DateTime.Today)).Days;
                    if (so_ngay_ung < 0)
                        so_ngay_ung = 0;
                    if (cashInfo.AP_T1 > 0)
                        phi_ung_tien_tam_tinh = GetAdvanceFee(accountNo, DateTime.Today.ToString("yyyy-MM-dd"),
                            Functions.GetDouble(cashInfo.AP_T1), so_ngay_ung);

                    DateTime time2 = await _systemService.GetNextTradeDateByDate(date1);
                    so_ngay_ung = ((TimeSpan)(time2 - DateTime.Today)).Days;
                    if (so_ngay_ung < 0)
                        so_ngay_ung = 0;
                    if (cashInfo.AP > 0)
                        phi_ung_tien_tam_tinh = phi_ung_tien_tam_tinh + GetAdvanceFee(accountNo, DateTime.Today.ToString("yyyy-MM-dd"),
                            Functions.GetDouble(cashInfo.AP), so_ngay_ung);
                }
                if (cashInfo.LMV + cashInfo.COLLAT + cashInfo.PPAPP + cashInfo.P_BUYUNMATCH != 0)
                    data.MarginRate = (cashInfo.CASH_BALANCE + cashInfo.LMV + cashInfo.AP + cashInfo.AP_T1 + cashInfo.COLLAT + cashInfo.P_BUYUNMATCH - cashInfo.DEBT - cashInfo.BUYUNMATCH -
                             cashInfo.LOAN_INTEREST - phi_tam_tinh - so_tien_dang_rut - so_tien - cashInfo.ADV_WITHDRAW - phi_ung_tien_tam_tinh) / (cashInfo.LMV + cashInfo.COLLAT + cashInfo.PPAPP + cashInfo.P_BUYUNMATCH);
                else
                    data.MarginRate = 1;

                if (data.MarginRate > 1)
                    data.MarginRate = 1;
            }
            return data;
        }

        public async Task<List<DepositBankResult>> GetDepositBank(BaseRequest model)
        {
            try
            {
                var sql = @"SELECT BankName, BankNameEN, BankAccountNo, BankAccountName, BankAccountNameEN, Abbre
                    FROM TVSI_THONG_TIN_NGAN_HANG_NOP_TIEN WHERE Status = 1";

                return await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryAsync<DepositBankResult>(sql);
                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.AccountNo, ex, "GetDepositBank:accountNo=" + model.AccountNo);
                throw;
            }
        }
    }
}
