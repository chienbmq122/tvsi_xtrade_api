﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.Notications;
using TVSI.Bond.WebAPI.Lib.Services.SqlQueries;

namespace TVSI.Bond.WebAPI.Lib.Services.Impl
{
    public class NoticationService : BaseService, INoticationService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(EkycService));
        public INoticationCommandText _commandText;

        public NoticationService(INoticationCommandText commandText)
        {
            _commandText = commandText;
        }

        public async Task<NoticationResponseModel> GetHistoryNotication(NoticationRequestModel model)
        {
            try
            {
                return await WithSystemNotificationConnection(async conn =>
                {
                    var dataResponse = new NoticationResponseModel();
                    var start = (model.PageIndex - 1) * model.PageSize;
                    var data = await conn.QueryAsync<NoticationModel>(
                        _commandText.GetHistoryNotication, new { custcode = model.UserId, Start = start, PageSize = model.PageSize });

                    dataResponse.PageSize = model.PageSize;
                    dataResponse.Items = data.ToList();
                    var totalCount = await conn.QueryFirstOrDefaultAsync<NoticationCount>(_commandText.CountGetHistoryNotication, new { custcode = model.UserId });
                    dataResponse.TotalItem = totalCount.TotalItem;
                    dataResponse.UnRead = totalCount.UnRead;
                    return dataResponse;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "GetHistoryNotication");
                throw;
            }
        }

        public async Task<NoticationResponseModel> GetNoticationOfDay(NoticationRequestModel model)
        {
            try
            {
                return await WithSystemNotificationConnection(async conn =>
                {
                    var dataResponse = new NoticationResponseModel();
                    var start = (model.PageIndex - 1) * model.PageSize;
                    var dateTimeRequest = DateTime.Now.ToString("yyyy-MM-dd");
                    var data = await conn.QueryAsync<NoticationModel>(
                        _commandText.GetNoticationOfDay, new { custcode = model.UserId, Start = start, PageSize = model.PageSize, Date = dateTimeRequest });

                    dataResponse.PageSize = model.PageSize;
                    dataResponse.Items = data.ToList();
                    var totalCount = await conn.QueryFirstOrDefaultAsync<NoticationCount>(_commandText.CountGetNoticationOfDay, new { custcode = model.UserId, Date = dateTimeRequest });
                    dataResponse.TotalItem = totalCount.TotalItem;
                    dataResponse.UnRead = totalCount.UnRead;
                    return dataResponse;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "GetNoticationOfDay");
                throw;
            }
        }

        public async Task<NoticationContractResponseModel> GetRegisterNotication(BaseRequest model)
        {
            try
            {
                return await WithSystemNotificationConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<NoticationContractResponseModel>(_commandText.GetRegisterNotication, new { custcode = model.UserId });
                    List<string> dataLoaiThongBao = data.type_notification_id.Split(',').ToList();
                    data.list_type_notification_id = dataLoaiThongBao.Select(s => int.Parse(s)).ToList();
                    return data;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "GetRegisterNotication");
                throw;
            }
        }

        public async Task<bool> PostRegisterNotication(NoticationContractModel model)
        {
            try
            {
                var dataLoaiThongBaoId = "";
                foreach(var id in model.list_type_notification_id)
                {
                    dataLoaiThongBaoId += id.ToString() + ",";
                }
                return await WithSystemNotificationConnection(async conn =>
                {
                    // var hasData = await conn.QueryFirstOrDefaultAsync<int>(_commandText.CountRegisterNotication, new { custcode = model.UserId });
                    if(model.register_notification_id == 0)
                    {
                        await conn.ExecuteAsync(_commandText.PostRegisterNotication, new
                        {
                            loai_thongbao_id = dataLoaiThongBaoId.Length == 0 ? "" : dataLoaiThongBaoId.Remove(dataLoaiThongBaoId.Length - 1),
                            custcode = model.UserId,
                            userName = model.UserId
                        });
                    }
                    else
                    {
                        await conn.ExecuteAsync(_commandText.UpdateRegisterNotication, new
                        {
                            loai_thongbao_id = dataLoaiThongBaoId.Length == 0 ? "" : dataLoaiThongBaoId.Remove(dataLoaiThongBaoId.Length - 1),
                            custcode = model.UserId,
                            dang_ky_thongbao_id = model.register_notification_id
                        });
                    }
                    return true;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "PostRegisterNotication");
                throw;
            }
        }

        public async Task<List<NoticationTypeModel>> GetListTypeNotication(BaseRequest model)
        {
            try
            {
                return await WithSystemNotificationConnection(async conn =>
                {
                    var data = await conn.QueryAsync<NoticationTypeModel>(
                        _commandText.GetNoticationType, new { });

                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "GetListTypeNotication");
                throw;
            }
        }


        public async Task<bool> ReadNotication(ReadNoticationContact model)
        {
            try
            {
                return await WithSystemNotificationConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.ReadNotication, new
                    {
                        thongbao_id = model.notification_id,
                        custcode = model.UserId,
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "ReadNotication");
                throw;
            }
        }

        public async Task<bool> CreateNotication(CreateNoticationRequest model)
        {
            try
            {
                string dateTime = DateTime.ParseExact(model.time_send, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
                
                return await WithSystemNotificationConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.CreateNotication, new
                    {
                        loai_thongbao_id = model.type_notification_id,
                        tieu_de = model.title_notification,
                        noidung = model.content_notification,
                        noidung_en = model.content_en_notification,
                        custcode = model.custcode,
                        priority = model.priority,
                        nguon_thongbao = model.src_notification,
                        thoi_gian_gui = dateTime,
                        userName = model.UserId,
                        src_screen = model.src_screen
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "CreateNotication");
                throw;
            }
        }

        public async Task<NoticationResponseModel> GetHistoryWarning(NoticationRequestModel model)
        {
            try
            {
                return await WithSystemNotificationConnection(async conn =>
                {
                    var dataResponse = new NoticationResponseModel();
                    var start = (model.PageIndex - 1) * model.PageSize;
                    var data = await conn.QueryAsync<NoticationModel>(
                        _commandText.GetHistoryWarning, new { custcode = model.UserId, Start = start, PageSize = model.PageSize });

                    dataResponse.PageSize = model.PageSize;
                    dataResponse.Items = data.ToList();
                    var totalCount = await conn.QueryFirstOrDefaultAsync<NoticationCount>(_commandText.CountGetHistoryWarning, new { custcode = model.UserId });
                    dataResponse.TotalItem = totalCount.TotalItem;
                    dataResponse.UnRead = totalCount.UnRead;
                    return dataResponse;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "GetHistoryWarning");
                throw;
            }
        }

        public async Task<NoticationResponseModel> GetWarningOfDay(NoticationRequestModel model)
        {
            try
            {
                return await WithSystemNotificationConnection(async conn =>
                {
                    var dataResponse = new NoticationResponseModel();
                    var start = (model.PageIndex - 1) * model.PageSize;
                    var dateTimeRequest = DateTime.Now.ToString("yyyy-MM-dd");
                    var data = await conn.QueryAsync<NoticationModel>(
                        _commandText.GetWarningOfDay, new { custcode = model.UserId, Start = start, PageSize = model.PageSize, Date = dateTimeRequest });

                    dataResponse.PageSize = model.PageSize;
                    dataResponse.Items = data.ToList();
                    var totalCount = await conn.QueryFirstOrDefaultAsync<NoticationCount>(_commandText.CountWarningOfDay, new { custcode = model.UserId, Date = dateTimeRequest });
                    dataResponse.TotalItem = totalCount.TotalItem;
                    dataResponse.UnRead = totalCount.UnRead;
                    return dataResponse;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "GetWarningOfDay");
                throw;
            }
        }

        public async Task<bool> UpdateStatusNotication(UpdateStatusNoticationContact model)
        {
            try
            {
                return await WithSystemNotificationConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.UpdateStatusNotication, new
                    {
                        thongbao_id = model.notification_id,
                        status = model.status,
                        custcode = model.UserId,
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "UpdateStatusNotication");
                throw;
            }
        }

        public async Task<bool> ReadAllNotication(ReadAllNoticationRequest model)
        {
            try
            {
                return await WithSystemNotificationConnection(async conn =>
                {
                    if(model.type == 1)
                    {
                        await conn.ExecuteAsync(_commandText.readAllNotication, new
                        {
                            custcode = model.UserId,
                            kenh_gui_tin = 1,
                        });
                    }

                    if(model.type == 2)
                    {
                        await conn.ExecuteAsync(_commandText.readAllWarning, new
                        {
                            custcode = model.UserId,
                            kenh_gui_tin = 1,
                        });
                    }
                    return true;
                });
            }
            catch (Exception ex)
            {
                LogException(log, model.UserId, ex, "ReadAllNotication");
                throw;
            }
        }
        
    }
}
