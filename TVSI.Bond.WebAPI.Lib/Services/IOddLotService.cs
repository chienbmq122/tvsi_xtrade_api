﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models.OddLot;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface IOddLotService
    {
        Task<List<OddLotStockRepository>> SelectStockRepository(string customerAccount);
        
        Task<List<OddLotStockRepositoryEmsSearchModel>> SearchStockRepository(string stockCode);

        Task<bool> InsertOrder(string customerAccount, string userId, List<OddLotOrderInsertModel> lstOrder);

        Task<List<OddLotOrder>> SearchOrder(string customer_account, string stock_code, DateTime fromDate, DateTime toDate, int order_status, int order_channel);
        
        Task<List<OddLotOrder>> SelectCurrentOrder(string customer_account, string stock_code, int order_status, int order_channel);
        
        Task<List<OddLotOrder>> SelectHistoricalOrder(string customer_account, string stock_code, DateTime fromDate, DateTime toDate, int order_status, int order_channel);

        Task<bool> CancelOrder(long odd_lot_order_id, string updated_by);
    }
}