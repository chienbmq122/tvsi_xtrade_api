﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.AdvMoney;
namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface IAdvMoneyService
    {
        Task<AvaiableAdvanceInfoResult> GetAvaiableAdvanceInfo(string accountNo);
        Task<AddAdvanceInfoResult> AddCustAdvance(AddAdvanceInfoRequest request);
        Task<List<CustAdvanceInfoResult>> GetCustAdvanceToday(string account, string lang);

        Task<List<CustAdvanceInfoResult>> GetCustAdvanceHist(string account, DateTime fromDate, DateTime toDate, string lang);
    }
}
