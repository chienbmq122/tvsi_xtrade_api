﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.CashTransfer;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface ICashTransferService
    {
        Task<IEnumerable<BankInfo>> GetBankInfo(string custCode);

        Task<decimal> GetBalance(string accountNo);

        Task<BankInfo> GetBankInfoByID(long id);

        Task<IEnumerable<InternalAccountInfo>> GetInternalAccountInfo(string custCode, string accountNo);

        Task<InternalAccountInfo> GetInternalAccountInfoByID(string custCode, string internalAccountNo);

        Task<decimal> GetFirstBalance(string accountNo, DateTime fromDate);

        Task<CashTransactionResult> GetCashTransactionResult(string accountNo, DateTime fromDate, DateTime toDate, int PageIndex, int PageSize);
        Task<CashTransactionDetailResult> GetCashTransactionDetailResult(string accountNo, string symbol, string transType, DateTime fromDate, DateTime toDate, int PageIndex, int PageSize);
        Task<TransactionFeeStatementResult> GetTransactionFeeStatementResult(string accountNo, string side, DateTime fromDate, DateTime toDate, int PageIndex, int PageSize);
        Task<RealizeGainLossResult> GetRealizeGainLostResult(string accountNo, string symbol, DateTime fromDate, DateTime toDate, int PageIndex, int PageSize);
        Task<InComeAndExpenseResult> GetInComeAndExpenseResult(string accountNo, string symbol, DateTime fromDate, DateTime toDate, int PageIndex, int PageSize);

        Task<decimal> GetBuyHoldAmount(string custCode);
        Task<CashBalanceInfo> GetNormalAccountFromDB2(string accountNo);
        Task<CashBalanceInfo> GetMarginAccountFromDB2(string accountNo, bool isConfig);
        Task<AfterWithdrawalInfoResult> GetAfterWithdrawalInfo(string accountNo, decimal so_tien);
        decimal GetAdvanceFee(string accountNo, string ngayUng, double amountAdv, int so_ngay);
        List<ShowBlockFeeInfo> GetBlockFeeInfos(string accountNo, string lang);
        bool CheckNoHopDongKYC(string custCode);
        Task<List<DepositBankResult>> GetDepositBank(BaseRequest model);
    }
}
