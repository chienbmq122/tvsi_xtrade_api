﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.Notications;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface INoticationService
    {
        Task<NoticationResponseModel> GetNoticationOfDay(NoticationRequestModel model);

        Task<NoticationResponseModel> GetHistoryNotication(NoticationRequestModel model);

        Task<bool> PostRegisterNotication(NoticationContractModel model);

        Task<NoticationContractResponseModel> GetRegisterNotication(BaseRequest model);

        Task<List<NoticationTypeModel>> GetListTypeNotication(BaseRequest model);

        Task<bool> ReadNotication(ReadNoticationContact model);

        Task<bool> CreateNotication(CreateNoticationRequest model);

        Task<NoticationResponseModel> GetWarningOfDay(NoticationRequestModel model);

        Task<NoticationResponseModel> GetHistoryWarning(NoticationRequestModel model);

        Task<bool> UpdateStatusNotication(UpdateStatusNoticationContact model);

        Task<bool> ReadAllNotication(ReadAllNoticationRequest model);

    }
}
