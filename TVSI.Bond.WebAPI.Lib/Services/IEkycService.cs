﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models;
using TVSI.Bond.WebAPI.Lib.Models.Ekyc;
using TVSI.Bond.WebAPI.Lib.Utility.Common.Data;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface IEkycService
    {
        Task<bool> Tvsi_sDangKyTaiKhoanEkyc(EkycSaveUserInfoRequest model);
        Task<bool> Tvsi_sDangKyTaiKhoanEkyc_v2(EkycSaveUserInfoRequest_v2 model);
        /*Task<string> ConFirmOTPEkyc(ConfirmEkycModel model);*/
        Task<AccountData> Tvsi_sGetInfoCustomerCardID(string cardId);
        Task<IEnumerable<InternalAccountInfoResult>> GetInternalAccountInfo(BankAccountRequest model);
        Task<bool> SLogThayDoiThongTinKhEkyc(RegistCustomerServiceResult model,string statusContent,string contentChange);
        Task<bool> SLogThayDoiThongTinKhEkycDelInterAcc(InternalAccountDetailRequest model,string statusContent,string contentChange);
        Task<bool> SLogThayDoiThongTinKhEkycRegisterBank(RegisterBankRequest model,string statusContent,string contentChange);
        Task<bool> SLogThayDoiThongTinKhEkycDelBank(DeleteBankRequest model,string statusContent,string contentChange);
        Task<bool> SUpdateStatusVideoCall(UpdateStatusVideoCallRequest model); 
        Task<bool> sDatLichVideoCallEkyc(VideoCallScheduleRequest model);
        Task<bool> SCancelDatLichVideoCall(CancelVideoCallScheduleRequest model);
        Task<bool> RegisterBankAccount(RegisterBankRequest model);
        Task<bool> SInsertChangeInfoCust(string maKhachHang, string soTaiKhoan, 
            string tenKhachHang, string thongTinDaDangKy,
            string thongTinThayDoi, string noiDungThayDoi, string moTa, int trangThai,
            string nguoiTao, DateTime ngayTao, int isSbaInno, string branchId, long newLogId);

        Task<bool> InsertPathImageCardCust(string custId,string newImageFront,string newImageBack,string legalImage,
            string oldImageFront,string oldImageBack,int type);
        Task<bool> ConfirmEkycMobile(string codeM);
        Task<int> UpdateStatusVideo(UpdateVideoSignatureRequest model);
        Task<bool> DeleteBankAccount(DeleteBankRequest model);
        Task<bool> CardIDExsited(string cardId);
        Task<int> CardIDExsited_v2(string cardId);
        Task<bool> ChangeDataCustInfo(ChangeUserInfoRequest model);
        Task<bool> CheckChangeInfoCust(FieldChangeRequest model);
        Task<bool> ChangePhoneCust(ChangePhoneCust model);
        
        Task<IEnumerable<EkycCompareNotSameResult>> CompareEkycCust(CompareInfoCustRequest model);
        Task<IEnumerable<EkycCompareNotSameResult>> CompareTwoCust(CompareInfoCustRequest model,CompareInfoCustDBRequest dataCust);
        Task<IEnumerable<VideoCallScheduleHistInfoResult>> GetVideoCallScheduleHist(VideoCallHistRequest model);
        Task<IEnumerable<RegistServiceHistInfoResult>> GetChangeInfoHist(ChangeInfoHistRequest model);
        Task<bool> CreateAccountMargin(CreateAccountMarginRequest model );
        Task<CheckAccountMarginResult> CheckAccountMargin(string userId);
        Task<bool> CancellChangeInfoCust(CancellChangeInfoCustRequest model);
        Task<bool> CheckAddProfile(BaseRequest model);
        Task<GetInfoUserIdResult> GetInfoSale(GetInfoUserIdRequest model);
        Task<GetImageChangeInfoResult> GetImageChangeInfoCust(GetImageChangeInfoCustRequest model);
        Task<GetImageSignatureResult> GetImageAddProfile(GetImageChangeInfoCustRequest model);
        Task<StringeeResult> GetAccessTokenStringee(StringeeRequest model);
        Task<IEnumerable<ProvinceResult>> GetProvince();
        Task<IEnumerable<DistrictResult>> GetDistrict(DistrictRequest model);
        Task<IEnumerable<WardResult>> GetWardList(WardRequest model);

        Task<bool> ChangeSmsInfo();


        Task<GetImageByCardIdResult> GetImageByCardID(GetImageByCardIdRequest model);

        /*
        Task<bool> UpdateCardIdeKYC(CardIdRequest model);
        */
        Task<IEnumerable<VideoCallHistInfoRequest>> GetVideoCallHist(VideoCallHistRequest model);
        Task<InternalAccountDetailInfoResult> GetInternalAccountDetail(InternalAccountDetailRequest model); 
        Task<UserInfoResult> GetInfoCustomer(GetUserInfoRequest model);
        Task<IEnumerable<EkycCheckMobileCust>> ValidationInputPhoneNum(string custId, string oldPhone, string newPhone,
            int statusPhone, int options);
        Task<IEnumerable<EkycRegisterErrors>> ValidationFormRegisterUser(EkycSaveUserInfoRequest model);
        Task<IEnumerable<EkycRegisterErrors>> ValidationFormRegisterUser_v2(EkycSaveUserInfoRequest_v2 model);
        Task<IEnumerable<GetListPlaceOfIssueResult>> GetListPlaceOfIssue();
        Task<IEnumerable<BankInfoListResult>> GetBankAccountList(string accountNo);
        Task<BankDetailInfoResult> GetBankAccountDetail(AccNoDetailRequest model);
        Task<IEnumerable<BankModel>> GetBankList();
        Task<bool> CheckUpdateStatusVideo(string accountNo);
        Task<IEnumerable<BranchInfoResult>> GetSubBranchList(string bankNo);
        Task<IEnumerable<ProvinceModelResult>> GetProvinceList();
        Task<string> GetBankName(string bankNo);
        Task<string> GetSubBranchName(string bankNo, string branchNo);
        Task<string> GetSaleID(string saleId);
        Task<string> GetProvinceName(string provinceId);
        
        // change thong tin khach hang
        Task<ChangeCustInfo> GetSoDienThoaiChangeInfo(string newMobile,int statusMobile,string oldMobile, int options);
        
        Task<ChangeCustInfo> GetFullNameChangeInfo(string alreadName, string newName);
        Task<ChangeCustInfo> GetCMNDChangeInfo(string oldCardIssueDate,string cardIssueDate, string oldCardIssue, 
            string cardIssue,string oldCardId,string newCardId);
        Task<ChangeCustInfo> GetAddressChangeInfo(string oldAddress,string newAddress);
        Task<ChangeCustInfo> GetSMSChangeInfo(int options);
        Task<ChangeCustInfo> GetUTTDChangeInfo(int optionsAdvanceSerivce);
        Task<GetPhoneCustResult> GetPhoneCust(string custId);

        Task<ChangeCustInfo> GetSoDienThoai01ChangeInfo(string smsPhoneOld,string smsPhoneReceive,int options);
        // Tài khoản Margin
        Task<int> GetPhoneExisted(string CustomerID,string phone); 

        //Gửi mail HĐ eKYC
        Task<UserCRM> GetUserCRMConfirm(string codem);

        Task<bool> UpdateCardID(string cardID);
        Task<SaleIDResult> GetSaleName(string saleID);
        Task<string> GetEmailSale(string saleID);
        Task<int> CheckCustomerFormInfomation(string email, string phone);
        Task<int> CheckCustomerEkycInfomation(string email, string phone);
        Task<string> GetAccountNumber(string email,string phone);
        Task<string> GetCustCodeByCardId(string cardId);
    }
}