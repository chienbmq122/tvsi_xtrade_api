﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models.SC_OrderConfirm;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface ISC_OrderConfirmationService
    {
        Task<List<SC_FindOrderConfirmResult>> SC_GetOrderConfirmationHistory(SC_FindOrderConfirmRequest request);
        Task<List<SC_FindOrderConfirmResult>> SC_GetOrderConfirmationStatus(SC_FindOrderConfirmRequest request);

        Task<int> SC_ConfirmOrderConfirmation(SC_UpdateConfirmOrderConfirmationRequest request);

        Task<List<SC_OrderConfirmationDetailData>> SC_OrderConfirmationDetail(SC_OrderConfirmationDetailRequest request);

        Task<int> SC_ConfirmALLOrderConfirmation(SC_UpdateALLConfirmOrderConfirmationRequest request);
    }
}
