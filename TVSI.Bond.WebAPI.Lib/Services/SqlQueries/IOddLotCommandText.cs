﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public interface IOddLotCommandText
    {
        string SelectStockRepository {get;} 
        string TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_SELECT_BY_ID {get;}
        
        string TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_UPDATE_STATUS {get;}
        
        string TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_SEARCH {get;}
        
        string TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_SELECT_CURRENT_ORDER {get;}
        
        string TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_SELECT_HISTORICAL_ORDER {get;}
        
        string TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_INSERT {get;}
        
        string TVSI_sLAY_THONG_TIN_TAI_KHOAN_KHACH_HANG_THEO_SO_TAI_KHOAN_044_v2 {get;}
        
        string TVSI_sODDLOTSTOCKTRANSFERENCE_STOCK_REPOSITORY_SEARCH {get;}
        
        string TVSI_sODDLOTSTOCKTRANSFERENCE_ORDER_SELECT_PROCESS_LIST {get;}
        
        string TVSI_sODDLOTSTOCKTRANSFERENCE_STOCK_REPOSITORY_SELECT_BY_STOCK_CODE {get;}
    }
}