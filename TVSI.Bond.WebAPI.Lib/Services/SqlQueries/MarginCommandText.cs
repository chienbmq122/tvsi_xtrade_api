﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public class MarginCommandText : IMarginCommandText
    {
        public string InsertServicePreferMargin => @"insert into TVSI_DOI_GOI_DICH_VU_UU_DAI
(so_tai_khoan,ngay_dang_ky,ngay_ket_thuc,goi_dich_vu_hien_tai,goi_dich_vu_moi,ngay_tao,nguoi_tao,ho_ten_khach_hang) values 
(@so_tai_khoan,@ngay_dang_ky,@ngay_ket_thuc,@goi_dich_vu_hien_tai,@goi_dich_vu_moi,getdate(),@nguoi_tao,@ho_ten_khach_hang)";

        public string InsertServiceMargin => @"insert into TVSI_DOI_GOI_DICH_VU
(so_tai_khoan,ho_ten_khach_hang,ngay_dang_ky,ngay_ket_thuc,goi_dich_vu_hien_tai,goi_dich_vu_moi,ngay_tao,nguoi_tao) values 
(@so_tai_khoan,@ho_ten_khach_hang,@ngay_dang_ky,@ngay_ket_thuc,@goi_dich_vu_hien_tai,@goi_dich_vu_moi,getdate(),@nguoi_tao)";

        public string UpdateServicePreferMargin =>
            @"update TVSI_DOI_GOI_DICH_VU_UU_DAI set trang_thai=@trang_thai where goi_dich_vu_uu_daiid=@goi_dich_vu_uu_daiid and trang_thai=0";

        public string UpdateServiceMargin =>
            @"update TVSI_DOI_GOI_DICH_VU set trang_thai=@trang_thai where goi_dich_vuid=@goi_dich_vuid and trang_thai=0";

        public string GetInfoPreferById =>
            @"select goi_dich_vu_uu_daiid PackPreferID,trang_thai Status from TVSI_DOI_GOI_DICH_VU_UU_DAI where goi_dich_vu_uu_daiid=@goi_dich_vu_uu_daiid";

        public string GetInfoServiceById =>
            @"select goi_dich_vuid PackID,trang_thai Status from TVSI_DOI_GOI_DICH_VU where goi_dich_vuid=@goi_dich_vuid";

        public string GetServicePreferByAccountNo => @"select ma_khach_hang , ma_dich_vu from TVSI_MARGIN_ACTIVE_DANG_KY_SU_DUNG_DICH_VU A INNER JOIN TVSI_MARGIN_ACTIVE_DICH_VU B ON A.loai_dich_vu_id=B.loai_dich_vu_id where A.trang_thai=1 and B.trang_thai=1 and ma_khach_hang=@ma_khach_hang";
        public string GetServiceMarginByAccountNo => @"select ATCB.CUST_ID AccountNo,ATCB.MARGIN_GROUP CurrentPack,ATCB.PP*1000 BuyingPower,ATCB.BRK_CALL_LMV CallMargin,
(select count(*) from MARGINGRPTAB where MARGIN_GROUP=ATCB.MARGIN_GROUP and PLEDGERATE > 0) AS ma_cho_vay
 from ACCOUNTTAB_CB AS ATCB where CUST_ID= ?";

        public string GetServicePreferHist => @"SELECT * FROM (
select goi_dich_vu_uu_daiid PackPreferID ,so_tai_khoan AccountNo,ngay_dang_ky RegDateOn,
                        ngay_hieu_luc EffDateOn,ngay_ket_thuc EndDateOn,goi_dich_vu_uu_dai_hien_tai CurrentPack
       , goi_dich_vu_uu_dai_moi CurrentNewOn,ly_do_tu_choi Reject,trang_thai Status,
                        ngay_tao CreateDateOn,ngay_phe_duyet ApproveDateOn,nguoi_phe_duyet ApproveBy from 
                        TVSI_DOI_GOI_DICH_VU_UU_DAI_LICH_SU where so_tai_khoan = @accountNo UNION ALL select goi_dich_vu_uu_daiid,so_tai_khoan,ngay_dang_ky, ngay_hieu_luc,ngay_ket_thuc,goi_dich_vu_hien_tai, goi_dich_vu_moi,ly_do_tu_choi,trang_thai, ngay_tao,ngay_phe_duyet,nguoi_phe_duyet from TVSI_DOI_GOI_DICH_VU_UU_DAI where so_tai_khoan=@accountNo) A ORDER BY A.PackPreferID desc";

        public string GetServiceHist =>
            @"select * from (select goi_dich_vuid PackID,so_tai_khoan AccountNo,ngay_dang_ky RegDateOn,ngay_hieu_luc EffDateOn
                    ,ngay_ket_thuc EndDateOn,goi_dich_vu_hien_tai CurrentPack, goi_dich_vu_moi CurrentNew,
                      ly_do_tu_choi Reject,trang_thai Status,ngay_tao CreateDateOn,ngay_phe_duyet ApproveDateOn,nguoi_phe_duyet ApproveBy  from TVSI_DOI_GOI_DICH_VU_LICH_SU where so_tai_khoan= @accountNo
             UNION ALL select goi_dich_vuid,so_tai_khoan,ngay_dang_ky,ngay_hieu_luc,ngay_ket_thuc,goi_dich_vu_hien_tai, goi_dich_vu_moi,ly_do_tu_choi,trang_thai, ngay_tao,ngay_phe_duyet,nguoi_phe_duyet from TVSI_DOI_GOI_DICH_VU where so_tai_khoan= @accountNo
            ) A order by A.PackID desc";
    }
}