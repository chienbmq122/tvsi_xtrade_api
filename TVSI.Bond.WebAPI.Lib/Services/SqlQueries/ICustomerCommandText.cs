﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public interface ICustomerCommandText
    {
        string GetOverdue_01 { get; }
        string GetCustBalance_01 { get; }
        string GetCustBalance_01_Overdue { get; }
        string GetCustBalance_06 { get; }
        string GetStockBalance { get; }
        string GetAdvanceAmount { get; }
        string GetStockAsset { get; }
        string GetEntepriseBondAsset { get; }
        string GetTvsiBondAsset { get; }
        string GetMmAsset { get; }
        string GetAdvanceAsset { get; }
        string GetCustInfo { get; }
        string GetMarginGroupInfo { get; }
        string GetBranchInfo { get; }
        string GetCustEmailInfo { get; }
        string GetCustPhoneInfo { get; }
        string GetCustBankInfo { get; }
        string GetFullCustBankInfo { get; }

        string GetAccountNoList { get; }
        string SetDefaultAccount { get; }

        string UpdateCustBankStatus { get; }
    }
}
