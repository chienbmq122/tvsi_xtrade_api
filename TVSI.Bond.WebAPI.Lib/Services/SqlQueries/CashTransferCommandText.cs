﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public class CashTransferCommandText : ICashTransferCommandText
    {
        #region SQL Command List

        public string GetBankInfo => @"SELECT A.BANKACCOUNTID as CustBankId, A.BANKACCOUNT AS BankAccountNo, A.BENEFICIARY AS BankAccountName,
                                              B.BANKNAME AS BankName
                                        FROM BANKACCOUNT A
                                        LEFT JOIN BANKLIST B ON A.BankNo = B.BankNo
                                        WHERE A.STATUS = 1 AND A.CUSTOMERID = @CustCode";

        public string GetBalance => @"SELECT CASHBALANCE - NVL(T_BUY, 0) FROM AccountTab WHERE AccountNo = ?";

        public string GetBankInfoByID => @"SELECT A.BANKACCOUNTID as CustBankId, A.BANKACCOUNT AS BankAccountNo, A.BENEFICIARY AS BankAccountName,
                                                  B.BANKNAME AS BankName
                                            FROM BANKACCOUNT A
                                            LEFT JOIN BANKLIST B ON A.BankNo = B.BankNo
                                            WHERE A.STATUS = 1 AND A.BANKACCOUNTID = @BankAccountID";

        public string GetInternalAccountInfo => @"SELECT A.CustomerId as CustCode, A.InternalAccountNo AS InternalAccountNo, A.InternalCustomerName AS InternalAccountName
                                                        FROM INTERNALACCOUNT A
                                                        WHERE A.InternalStatus = 1 AND A.CustomerId = @CustCode
                                                  UNION ALL                                                
                                                  SELECT A.CustomerId CustCode, RTRIM(AccountNo) InternalAccountNo, CustomerName AS InternalAccountName 
                                                        FROM Account A
                                                        INNER JOIN Customer B ON A.CustomerId = B.CustomerId
                                                        WHERE A.AccountNo like @AccountNoLike AND RIGHT(RTRIM(A.AccountNo),1) != '8'
                                                        AND RTRIM(A.AccountNo) != @AccountNo";
        public string GetInternalAccountInfoByID => @"SELECT A.CustomerId as CustCode, A.InternalAccountNo AS InternalAccountNo, A.InternalCustomerName AS InternalAccountName
                  FROM INTERNALACCOUNT A
                  WHERE A.InternalStatus = 1 AND A.CustomerId = @CustCode AND A.InternalAccountNo = @InternalAccountNo
          UNION ALL                                                
          SELECT A.CustomerId CustCode, RTRIM(AccountNo) InternalAccountNo, CustomerName AS InternalAccountName 
                FROM Account A
                INNER JOIN Customer B ON A.CustomerId = B.CustomerId
                WHERE RTRIM(A.AccountNo) = @AccountNo";

        public string GetHoldAmount => @"SELECT Sum(Amount) Amount FROM Contract WHERE Side = 'B' AND accountNo = @AccountNo and Status = 0";

        #endregion


        #region Private function

        ///// <summary>
        ///// Biến đánh dấu file xml thay đổi thì load lại xml
        ///// </summary>
        //private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);
        //private static readonly ILog log = LogManager.GetLogger(typeof(CashTransferCommandText));

        //private static string LoadSqlQuery(string sqlName)
        //{
        //    var xmlFile = HttpContext.Current.Server.MapPath("~/bin/Services/SqlQueries/XMLFiles/CashTransferSqlQuery.xml");

        //    var fileInfo = new FileInfo(xmlFile);

        //    if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
        //    {
        //        log.Info("Load File CashTransferSqlQuery.xml");
        //        LoadSystemSqlXMLFile(xmlFile);
        //        xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
        //    }
        //    return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        //}

        ///// <summary>
        ///// Biến lưu danh sach câu query trong bộ nhớ
        ///// </summary>
        //private static Dictionary<string, string> _sqlQueriesList;
        //private static void LoadSystemSqlXMLFile(string filePath)
        //{
        //    _sqlQueriesList = new Dictionary<string, string>();

        //    var xdoc = XDocument.Load(filePath);
        //    var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
        //    foreach (var sqlItem in sqlQueryList)
        //    {
        //        _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
        //           sqlItem.Descendants("Sql").Single().Value);
        //    }
        //}

        #endregion
    }
}
