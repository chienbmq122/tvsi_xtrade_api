﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public interface IMmCommandText
    {
        string GetMmContractList { get; }

        string GetMmShortContractDetail { get; }

        string GetMmOrderBook { get; }

        string GetMmOrderHistList { get; }

        string GetMmOrderConfrimList { get; }

        string ConfirmMmOrder { get; }
    }
}
