﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public interface IOrderCommandText
    {
        string GetContractList { get; }
        string GetContractDetail { get; }
        string GetContractCoupon { get; }
        string GetContractPriceInfo { get; }
        string GetOrderBook { get; }
        string GetOrderHist { get; }
        string GetOrderConfirm { get; }
        string GetCustBankInfo { get; }

        string ConfirmOrder { get; }
        string GetProcessingSellContract { get; }
        string GetBondPolicyInfo { get; }
        string GetProfesType { get; }
    }
}
