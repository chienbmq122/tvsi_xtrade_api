﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public class CustomerCommandText : ICustomerCommandText
    {
        #region SQL Command List
        public string GetOverdue_01 => @"SELECT Overdue FROM CUS_CASH_DUETAB WHERE ACCOUNTNO = '{0}' ";

        public string GetCustBalance_01 => @"SELECT AccountNo, SUM(CashBalance + Sell_T1 - Buy_T0) CashBalance, SUM(Debt) Debt
                                          FROM (
                                                  SELECT TRIM(AccountNo) AccountNo, CashBalance + NVL(T_SELL,0) CashBalance, 0 Sell_T1, 0 Buy_T0,  Debt
                                                  FROM ACCOUNTTAB WHERE AccountNo = '{0}'
                                                  UNION ALL
                                                  SELECT TRIM(AccountNo) AccountNo, 0 CashBalance, -NVL(WTR_CASH_T2, 0) Sell_T1, NVL(PAYMENT, 0) Buy_T0, 0 Debt
                                                  FROM CUS_CASH_DUETAB WHERE ACCOUNTNO = '{0}' 
                                             ) A
                                          GROUP BY AccountNo";

        public string GetCustBalance_01_Overdue => @"SELECT TRIM(AccountNo) AccountNo, CashBalance, Debt
                                                  FROM ACCOUNTTAB WHERE AccountNo = '{0}'";

        public string GetCustBalance_06 => @"SELECT Cash_Balance CashBalance, NVL(LMV, 0) Lmv, NVL(Collat, 0) Collat, 
                                                NVL(AP,0) + NVL(AP_t1, 0) + NVL(AP_t2, 0) Ap, NVL(ADV_WITHDRAW, 0) AdvWithdraw, Debt
                                            FROM Accounttab_cb WHERE Cust_ID = '{0}'";

        public string GetStockBalance => @"SELECT Secsymbol, Volume, NVL(Price, StartPrice) Price,
                                            CASE 
                                                  WHEN CommSectype = 'H' AND (LENGTH(Secsymbol) >= 10 OR (SECSYMBOL like 'HDMG%')) THEN 'T'
                                                  ELSE 'S' END StockType
                                            FROM
                                            (SELECT TRIM(A.Secsymbol) Secsymbol, NVL(A.WTR,0) + NVL(A.WTR_T1,0) + NVL(A.WTR_T2,0) AS Volume,
                                                    CASE WHEN LastSale > 0 THEN LastSale
                                                    ELSE Closeprice END Price, 0 AS StartPrice, CommSectype
                                            FROM Cus_Stock_DueTab A 
                                            LEFT JOIN SecuritiesTab B ON A.SECSYMBOL = B.SECSYMBOL
                                            LEFT JOIN SecCtrlTab C on A.SECSYMBOL = C.SECSYMBOL
                                            WHERE accountNo LIKE '{0}'
                                            UNION ALL
                                            SELECT TRIM(A.Secsymbol) Secsymbol, ActualVolume Volume,
                                                    CASE WHEN LastSale > 0 THEN LastSale
                                                    ELSE Closeprice END Price, StartPrice, CommSectype
                                            FROM CustpositionTab A 
                                            LEFT JOIN SecuritiesTab B ON A.SECSYMBOL = B.SECSYMBOL
                                            LEFT JOIN SecCtrlTab C on A.SECSYMBOL = C.SECSYMBOL
                                            WHERE accountNo LIKE '{0}' AND ActualVolume > 0 AND A.Sectype in (2,30)
                                            ) A
                                            WHERE A.Volume > 0";

        public string GetAdvanceAmount => @"SELECT ISNULL(SUM(Amount), 0) Amount FROM AdvMortagePosition 
                                            WHERE Amount > 0 AND AccountNo = @accountNo and Status = 1";

        public string GetStockAsset => @"SELECT SECSYMBOL StockCode, Price, SUM(Volume) Available, SUM(Incoming) Incoming
                                          FROM
                                          (SELECT Secsymbol, Volume, Incoming, NVL(Price, StartPrice) Price,
                                                  CASE 
                                              WHEN CommSectype = 'H' AND (LENGTH(Secsymbol) >= 10 OR (SECSYMBOL like 'HDMG%')) THEN 'T'
                                              ELSE 'S' END StockType
                    
                                                  FROM
                                                      (SELECT TRIM(A.Secsymbol) Secsymbol, 0 Volume, NVL(A.WTR,0) + NVL(A.WTR_T1,0) + NVL(A.WTR_T2,0) as Incoming,
                                                              CASE WHEN LastSale > 0 THEN LastSale
                                                              ELSE Closeprice END Price, 0 AS StartPrice, CommSectype
                                                      FROM Cus_Stock_DueTab A 
                                                      LEFT JOIN SecuritiesTab B ON A.SECSYMBOL = B.SECSYMBOL
                                                      LEFT JOIN SecCtrlTab C on A.SECSYMBOL = C.SECSYMBOL
                                                      WHERE accountNo LIKE '{0}'
                                                      UNION ALL
                                                      SELECT TRIM(A.Secsymbol) Secsymbol, ActualVolume Volume, 0 as Incoming,
                                                              CASE WHEN LastSale > 0 then LastSale
                                                              ELSE Closeprice END Price, StartPrice, CommSectype
                                                      FROM CustpositionTab A 
                                                      LEFT JOIN SecuritiesTab B ON A.SECSYMBOL = B.SECSYMBOL
                                                      LEFT JOIN SecCtrlTab C on A.SECSYMBOL = C.SECSYMBOL
                                                      WHERE accountNo LIKE '{0}' AND ActualVolume > 0 AND A.Sectype in (2,30)
                       
                                                  ) A
                                                  WHERE (A.Volume > 0 OR A.Incoming > 0)
                                          ) B
                                          WHERE StockType = 'S'
                                          GROUP BY Secsymbol, Price, StockType";

        public string GetEntepriseBondAsset => @"SELECT B.IssuerCode BondCode, A.ContractNo, A.Volume, A.Volume * B.ParValue Amount
                                                FROM AccountPosition A
                                                INNER JOIN BondInfo B ON A.BondCode = B.BondCode
                                                WHERE A.Accountno = @AccountNo AND Volume > 0 AND B.BondType NOT IN (80,81)
                                                AND A.Status != 99";

        public string GetTvsiBondAsset => @"SELECT Secsymbol BondCode, Volume, Volume * Price * 1000 Amount
                                              FROM    
                                                  (SELECT Secsymbol, Volume, NVL(Price, StartPrice) Price,
                                                          CASE WHEN Price IS NULL THEN 'T'
                                                                  ELSE 'S' END StockType
                                                          FROM
                                                              (SELECT TRIM(A.Secsymbol) Secsymbol, ActualVolume Volume,
                                                                          CASE WHEN LastSale > 0 THEN LastSale
                                                                          ELSE Closeprice END Price, StartPrice
                                                                  FROM CUSTPOSITIONTAB A LEFT JOIN SecuritiesTab B ON A.SECSYMBOL = B.SECSYMBOL
                                                                  WHERE accountNo LIKE ? AND ActualVolume > 0 AND A.Sectype = 2

                                                          ) A
                                                          WHERE A.Volume > 0) B
                                                  WHERE B.StockType = 'T' AND B.Secsymbol LIKE 'TVSI_TP_%'";

        public string GetMmAsset => @"SELECT A.ContractNo, CONVERT(varchar, C.CouponDate, 103) StartDate, 
                                        CONVERT(varchar, C.MaturityDate, 103) EndDate, C.Rate, A.Amount 
                                        FROM AccountPosition  A
                                        INNER JOIN ContractHist C ON A.ContractNo = C.ContractNo
                                        INNER JOIN BondInfo B ON A.BondCode = B.BondCode
                                    WHERE A.Accountno = @AccountNo AND BondType in (80,81) and A.Volume > 0
                                    AND A.Status != 99";
        public string GetAdvanceAsset => @"SELECT A.ContractNo, Convert(varchar,TradeDate,103) AdvanceDate, convert(varchar, WithdrawDate, 103) EndDate, AdvContractRate AdvanceRate,
	                                          A.Amount - ISNULL(B.Amount,0) Amount
	                                      FROM (
		                                          SELECT AdvMortOrderID, AccountNo, AdvMortNo, BondCode, TradeDate, ContractNo, Amount
		                                          FROM AdvMortOrder WHERE Status = 1
		                                          AND AdvMortType = 1 AND TradeDate <= @TradeDate
	                                          ) AS A
	                                      LEFT JOIN (
		                                    SELECT ContraAdvMortNo, SUM(Amount) Amount FROM AdvMortOrder WHERE Status = 1
			                                    AND AdvMortType = 2 AND TradeDate <= @TradeDate
		                                        GROUP BY ContraAdvMortNo
	                                        ) AS B
	                                        ON A.AdvMortNo = B.ContraAdvMortNo INNER JOIN AdvMortOrderDetail C
	                                        ON A.AdvMortOrderID = C.AdvMortOrderID
	                                        WHERE A.Amount - ISNULL(B.Amount,0) > 0 AND A.AccountNo = @AccountNo";

        //public string GetCustInfo => @"SELECT A.ma_khach_hang CustCode, A.ten_khach_hang CustName, CONVERT(varchar, A.ngay_sinh, 103) BirthDate,
        //        A.so_cmnd_passport CardID, CONVERT(varchar, A.ngay_cap, 103) IssueDate,
        //        A.noi_cap CardIssuer, B.dia_chi_khach_hang Address, B.so_dien_thoai_mb Mobile, B.dia_chi_email Email
        //        FROM TVSI_TAI_KHOAN_KHACH_HANG A
        //        LEFT JOIN TVSI_THONG_TIN_LIEN_HE_KHACH_HANG B ON B.so_tai_khoan = A.ma_khach_hang + '1'
        //        WHERE ma_khach_hang = @CustCode AND loai_khach_hang=1";

        public string GetCustInfo => @"SELECT CUSTOMERID CustCode, CUSTOMERNAME CustName, CONVERT(varchar, BIRTHDAY, 103) BirthDate, IDENTITYCARD CardID,
                                       CONVERT(varchar, CARDISSUE, 103) IssueDate, PLACEISSUE CardIssuer, 
                                        ADDRESS as Address, CONTACTEMAIL as Email, CONTACTPHONE as Mobile, B.Name CustomerGroupName 
                                       FROM Customer A
                                       LEFT JOIN CustomerGroup B on A.CustomerGroupID = B.CustomerGroupID
                                       WHERE CUSTOMERID =  @custCode";

        public string GetBranchInfo => @"SELECT A.ma_nhan_vien_quan_ly SaleID, B.ma_chi_nhanh + '-' + B.ten_chi_nhanh Branch, B.ten_chi_nhanh BranchName,
                C.ho_va_ten SaleName
                FROM TVSI_THONG_TIN_KHACH_HANG A
                LEFT JOIN TVSI_DANH_MUC_CHI_NHANH B ON A.ma_chi_nhanh= B.ma_chi_nhanh
                LEFT JOIN TVSI_NHAN_SU C ON A.ma_nhan_vien_quan_ly = C.ma_nhan_vien_quan_ly
                WHERE A.ma_khach_hang = @CustCode";

        public string GetMarginGroupInfo =>
            @"SELECT A.so_tai_khoan AccountNo, COALESCE(co_the_mua, 0) CanBuy, COALESCE(co_the_ban, 0) CanSell, COALESCE(B.ten_nhom, 'BASIC') MarginGroup
                FROM TVSI_TAI_KHOAN_GIAO_DICH A
                LEFT JOIN TVSI_PHAN_NHOM_TAI_KHOAN B ON A.so_tai_khoan = B.so_tai_khoan AND B.trang_thai = 0 
                    AND B.ngay_co_hieu_luc <= @CurDate AND @CurDate < B.ngay_het_han
                WHERE A.so_tai_khoan = @AccountNo";

        public string GetCustEmailInfo => @"SELECT ContactEmail Email, 1 AS Status FROM Customer WHERE CustomerID = @CustomerID";

        public string GetCustPhoneInfo => @"SELECT CustomerID, Phone, isActive AS Status FROM CustomerPhone WHERE CustomerID = @CustomerID AND IsCC=1";

        public string GetCustBankInfo => @"SELECT A.CustomerID, A.BankAccount, A.Beneficiary AS BankAccountName,
                            B.BankName, A.Province, A.Status 
                            FROM BankAccount A 
                            LEFT JOIN BANKLIST B ON A.BankNo = B.BankNo
                            WHERE A.CustomerID = @CustomerID AND A.Status = 1";

        public string GetFullCustBankInfo => @"SELECT A.BankAccountID, A.CustomerID, A.BankAccount, A.Beneficiary AS BankAccountName,
                            B.BankName, A.Province, A.Status 
                            FROM BankAccount A 
                            LEFT JOIN BANKLIST B ON A.BankNo = B.BankNo
                            WHERE A.CustomerID = @CustomerID AND A.Status in (1,3)";

        public string GetAccountNoList => @"SELECT A.CustomerId CustCode, RTRIM(AccountNo) AccountNo, AccountType,
            CASE WHEN A.AccountNo = B.PickAccount THEN 1 ELSE 0 END IsDefault
            FROM Account A
            INNER JOIN Customer B on A.CustomerID = B.CustomerID
            WHERE A.CustomerID = @CustomerID AND RIGHT(RTRIM(A.AccountNo),1) != '8'
            AND RTRIM(A.AccountNo) NOT IN (SELECT DISTINCT AccountNo from AccountPermission WHERE Permission = 3)";

        public string SetDefaultAccount => @"UPDATE Customer SET PickAccount=@DefaultAccountNo WHERE CustomerID = @CustomerID";

        public string UpdateCustBankStatus => @"UPDATE BankAccount SET Status=@Status
                            WHERE BankAccountID=@BankAccountID AND CustomerID=@CustomerID and BankAccount=@BankAccount";
        #endregion


        #region Private function

        ///// <summary>
        ///// Biến đánh dấu file xml thay đổi thì load lại xml
        ///// </summary>
        //private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);
        //private static readonly ILog log = LogManager.GetLogger(typeof(CustomerCommandText));

        //private static string LoadSqlQuery(string sqlName)
        //{
        //    var xmlFile = HttpContext.Current.Server.MapPath("~/bin/Services/SqlQueries/XMLFiles/CustomerSqlQuery.xml");

        //    var fileInfo = new FileInfo(xmlFile);

        //    if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
        //    {
        //        log.Info("Load File CustomerSqlQuery.xml");
        //        LoadSystemSqlXMLFile(xmlFile);
        //        xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
        //    }
        //    return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        //}

        ///// <summary>
        ///// Biến lưu danh sach câu query trong bộ nhớ
        ///// </summary>
        //private static Dictionary<string, string> _sqlQueriesList;
        //private static void LoadSystemSqlXMLFile(string filePath)
        //{
        //    _sqlQueriesList = new Dictionary<string, string>();

        //    var xdoc = XDocument.Load(filePath);
        //    var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
        //    foreach (var sqlItem in sqlQueryList)
        //    {
        //        _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
        //           sqlItem.Descendants("Sql").Single().Value);
        //    }
        //}

        #endregion
    }
}
