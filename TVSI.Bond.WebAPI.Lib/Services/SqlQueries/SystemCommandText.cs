﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public class SystemCommandText : ISystemCommandText
    {
        #region SQL Command List
        public string GetCustomerStatus => @"SELECT Status FROM Customer WHERE CustomerID = @CustomerID AND Password = @Password";

        //public string GetAccountNoList => @"SELECT A.CustomerId UserId, RTRIM(AccountNo) AccountNo, CustomerName AS AccountName 
        //                                    FROM Account A
        //                                    INNER JOIN Customer B ON A.CustomerId = B.CustomerId
        //                                    WHERE A.AccountNo like @AccountNo AND RIGHT(RTRIM(A.AccountNo),1) != '8'
        //                                    AND RTRIM(A.AccountNo) NOT IN (SELECT DISTINCT AccountNo from AccountPermission WHERE Permission = 3)";

        //public string GetAccountNoList => @"SELECT B.ma_khach_hang UserId, A.so_tai_khoan AccountNo, B.ten_khach_hang AccountName 
        //                                    FROM TVSI_TAI_KHOAN_GIAO_DICH A
        //                                    INNER JOIN TVSI_TAI_KHOAN_KHACH_HANG B ON A.khach_hang_id = B.khach_hang_id
        //                                    AND A.trang_thai = 1 AND B.trang_thai = 1 AND A.loai_tai_khoanid != 3 AND B.ma_khach_hang = @CustCode";

        public string GetAccountNoList => @"SELECT B.CustCode UserId, A.AccountNo AccountNo, B.CustName AccountName, COALESCE(C.AllowBond, 1) IsAllowBond, COALESCE(C.AllowFund, 0) IsAllowFund 
                                            FROM CustAccountInfo A
                                            INNER JOIN CustInfo B ON A.CustCode = B.CustCode
                                            LEFT JOIN CustAccountService C on A.AccountNo = C.AccountNo
                                            WHERE A.Status = 1 AND B.Status = 1 AND A.AccountType != 8 AND B.CustCode = @CustCode";

        public string GetAlphaAccount => @"SELECT so_tai_khoan FROM TVSI_BG_DANH_MUC_SU_DUNG_DICH_VU WHERE so_tai_khoan=@so_tai_khoan AND trang_thai=1";

        public string GetCustomerIdByPassword => @"SELECT CustomerID FROM Customer 
                                                WHERE CustomerID = @CustomerID AND Password = @Password";
        public string UpdatePasswordOfCustomer => @"UPDATE Customer SET Password = @password, ModifiedDatePass=GETDATE() WHERE CustomerID = @CustomerID";

        public string GetCustomerIdByPin => @"SELECT CustomerID FROM Customer WHERE CustomerID = @CustomerID AND Pin = @Pin";

        public string UpdatePinOfCustomer => @"UPDATE Customer SET Pin = @Pin, ModifiedDatePin=GETDATE() WHERE CustomerID = @CustomerID";

        public string GetNotificationList => @"SELECT MessageID MessageId, MessageType NotificationType, TradeDate TradeDateDT, 
                                                Title, Message, IsRead
                                                FROM TVSI_THONG_BAO_TBM 
                                                WHERE AccountNo = @AccountNo 
                                                ORDER BY MessageID DESC
                                                OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        public string GetNumOfUnreadNotification => @"SELECT count(*) FROM TVSI_THONG_BAO_TBM WHERE AccountNo = @AccountNo AND IsRead = 0";
        public string SyncNotificationRead => @"UPDATE TVSI_THONG_BAO_TBM SET IsRead = 1, UpdatedBy = @UpdatedBy, UpdatedDate = @UpdatedDate 
                                                WHERE AccountNo = @AccountNo AND IsRead = 0 AND MessageID IN @MessageIDList";

        public string GetBondFilterByAccount => @"SELECT DISTINCT B.IssuerGroup FROM AccountPosition A
                    INNER JOIN BondInfo B ON A.BondCode = B.BondCode
                    WHERE AccountNo = @AccountNo AND A.Volume > 0 AND B.BondType NOT IN (80,81)";

        public string GetBondFilter => @"SELECT DISTINCT A.IssuerCode BondCode, B.Issuer BondName
                    FROM BondEvent A
                    LEFT JOIN BondInfo B ON A.IssuerCode = B.IssuerGroup
                    WHERE A.Status = 1 and B.BondType not in (80,81) and A.EventType in (2,4,7,8,10) AND A.IssuerCode IN @BondCodeList";
        public string GetEventTypeFilter => @"SELECT DISTINCT EventType FROM BondEvent WHERE Status = 1 AND EventType IN (2,4,7,8,10)";

        public string GetEventList => @"SELECT EventID, CONVERT(varchar, EventDate, 103) EventDate, IssuerCode BondCode, 
                                          Title FROM BondEvent A WHERE status = 1 AND EventType in (2,4,7,8,10)
                                            AND IssuerCode in @BondCodeList {cond1} {cond2}
                                          ORDER BY A.EventDate DESC, EventID DESC
                                          OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        public string GetEventDetail => @"SELECT EventID, CONVERT(varchar, EventDate, 103) EventDate, IssuerCode BondCode, 
                                            Title, EventType, Description FROM BondEvent WHERE status = 1 AND EventID = @EventID";
        public string GetCellPhone => @"SELECT CONTACTPHONE AS Mobile, CELLPHONE AS Mobile_02 FROM Customer WHERE CUSTOMERID = @custCode";
        public string GetCustomerPhone => @"SELECT Phone AS Mobile FROM CustomerPhone WHERE CUSTOMERID = @custCode AND IsActive=1 ORDER BY IsSMS, DateCreate DESC";


        public string GetForgotPassword => @"SELECT CustomerID as CustID, CustomerName as CustName, IdentityCard  as CardId, ContactEmail Email FROM CUSTOMER 
                                                WHERE CustomerID = @CustomerID AND IdentityCard = @IdentityCard";

        public string SendPassResetCode => @"UPDATE CUSTOMER SET CONFIRMCODE = @ConfirmCode
                                                WHERE CustomerID = @CustomerID AND IdentityCard = @IdentityCard";


        public string GetForgotPIN => @"SELECT CustomerID as CustID, CustomerName as CustName, IdentityCard  as CardId, ContactEmail Email FROM CUSTOMER 
                                                WHERE CustomerID = @CustomerID AND IdentityCard = @IdentityCard";

        public string GetVerifyAccount => @"SELECT CustomerID as CustID, CustomerName as CustName, IdentityCard  as CardId, ContactEmail Email FROM CUSTOMER 
                                                WHERE CustomerID = @CustomerID AND ContactEmail = @Email";


        public string SendPINResetCode => @"UPDATE CUSTOMER SET PINCONFIRMCODE = @ConfirmCode
                                                WHERE CustomerID = @CustomerID AND IdentityCard = @IdentityCard";


        public string SendConfirmCode => @"UPDATE CUSTOMER SET CONFIRMCODE = @ConfirmCode
                                                WHERE CustomerID = @CustomerID AND ContactEmail = @Email";

        public string CheckConfirmCode => @"SELECT CustomerID as CustID, CustomerName as CustName, IdentityCard  as CardId, ContactEmail Email FROM CUSTOMER 
                                                WHERE ConfirmCode = @ConfirmCode";

        public string CheckConfirmCodePIN => @"SELECT CustomerID as CustID, CustomerName as CustName, IdentityCard  as CardId, ContactEmail Email FROM CUSTOMER 
                                                WHERE PinConfirmCode = @ConfirmCode";


        public string UpdateNewPass => @"UPDATE CUSTOMER SET Password = @passWord, CONFIRMCODE = @ConfirmCode, ValidatePass = 1, ModifiedDatePass = @modifiedDatePass
                                                WHERE CustomerID = @CustomerID";

        public string UpdateNewPin => @"UPDATE CUSTOMER SET PIN = @pin, PINCONFIRMCODE = @ConfirmCode, ValidatePin = 1, ModifiedDatePin = @modifiedDatePin
                                                WHERE CustomerID = @CustomerID";

        public string InactiveCustomer => @"UPDATE CUSTOMER SET Status = 0 WHERE CUSTOMERID = @CustomerID";

        #endregion

        #region Private function

        ///// <summary>
        ///// Biến đánh dấu file xml thay đổi thì load lại xml
        ///// </summary>
        //private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);
        //private static readonly ILog log = LogManager.GetLogger(typeof(SystemCommandText));
        //private static string LoadSqlQuery(string sqlName)
        //{
        //    var xmlFile = HttpContext.Current.Server.MapPath("~/bin/Services/SqlQueries/XMLFiles/SystemSqlQuery.xml");

        //    var fileInfo = new FileInfo(xmlFile);

        //    if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
        //    {
        //        log.Info("Load File SystemSqlQuery.xml");
        //        LoadSystemSqlXMLFile(xmlFile);
        //        xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
        //    }
        //    return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        //}

        ///// <summary>
        ///// Biến lưu danh sach câu query trong bộ nhớ
        ///// </summary>
        //private static Dictionary<string, string> _sqlQueriesList; 
        //private static void LoadSystemSqlXMLFile(string filePath)
        //{
        //    _sqlQueriesList = new Dictionary<string, string>();

        //    var xdoc = XDocument.Load(filePath);
        //    var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
        //    foreach (var sqlItem in sqlQueryList)
        //    {
        //        _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
        //           sqlItem.Descendants("Sql").Single().Value);
        //    }
        //}

        #endregion

    }
}
