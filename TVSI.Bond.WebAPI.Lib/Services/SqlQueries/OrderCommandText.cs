﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public class OrderCommandText : IOrderCommandText
    {
        public string GetContractList => @"SELECT * FROM
                                        (SELECT A.ContractNo, B.IssuerCode BondCode,
	                                    (CASE WHEN B.BondType IN (4,5,6) THEN 1
		                                        WHEN B.BondType in (3) THEN 2
                                                WHEN B.BondType in (7) AND B.BondTypeGroup in (2) THEN 5
		                                        WHEN B.BondType in (7,8,11) THEN 3
		                                        WHEN B.BondType in (9,10) THEN 4
	                                    END) ProductType,
	                                    A.Volume, B.Term, CONVERT(varchar, C.TradeDate, 103) TradeDate, 
	                                    CONVERT(varchar, C.MaturityDate, 103) MaturityDate,
	                                    (CASE WHEN (C.ServiceType = 2 OR C.ConfirmMethod != 0) AND DATEDIFF(day, @CurDate, C.MaturityDate) <= 120 AND @CurDate < C.MaturityDate 
                                            AND A.Volume=C.Volume AND B.BondType in (4,5) THEN 1
		                                    ELSE 0 
	                                    END) IsRoll, 
                                      (CASE WHEN (C.ServiceType = 2 OR C.ConfirmMethod != 0) AND C.TradeDate < @CurDate AND @CurDate < C.MaturityDate AND B.BondType NOT IN (7,8,9,10,11) THEN 1 ELSE 0 END) IsSell, D.CodeContent Issuer,
                                        POSITIONID
	                                    FROM AccountPosition A
	                                    INNER JOIN BondInfo B ON A.BondCode = B.BondCode
                                        LEFT JOIN (SELECT B.GroupCode, B.CodeContent, CodeContentEn FROM [dbo].[DefCodeTemp] A
                                        INNER JOIN [dbo].[DetailCodeTemp] B
                                        ON A.DefId = B.DefId 
                                        WHERE A.Status = 1 AND  A.CodeName = 'BOND_NAME') D ON B.IssuerCode = D.GroupCode
	                                    INNER JOIN ContractHist C ON A.ContractNo = C.ContractNo
                                      WHERE A.Accountno = @AccountNo AND A.Volume > 0 AND B.BondType NOT IN (80,81)
                                      AND A.Status != 99 AND (A.TradeDate >= CONVERT(DATETIME, @FromDate, 103) OR @FromDate = '')
                                      AND (A.TradeDate <= CONVERT(DATETIME, @ToDate, 103) OR @ToDate = '')) Result
                                      WHERE (Result.ProductType IN (SELECT * FROM TVSI_fBREAK_COMMA_SEPARATED_STRING(@ProductType, ',')) OR @ProductType = '-1')
                                            AND Result.ContractNo like '%' + @ContractNo + '%'
                                            AND Result.BondCode like '%' + @BondCode + '%'
                                            AND (Result.MaturityDate = @MaturityDate OR @MaturityDate = '')
                                      ORDER BY Result.POSITIONID DESC";

        public string GetContractDetail => @"SELECT C.ContractNo, C.BondCode, B.IssuerCode,
		                                      (CASE WHEN B.BondType IN (4,5,6) THEN 1
		                                        WHEN B.BondType IN (3) THEN 2
                                                WHEN B.BondType in (7) AND B.BondTypeGroup in (2) THEN 5
		                                        WHEN B.BondType IN (7,8,11) THEN 3
		                                        WHEN B.BondType IN (9,10) THEN 4
		                                      END) ProductType, B.Term, 
		                                      CONVERT(varchar, C.TradeDate, 103) TradeDate, CONVERT(varchar, C.CouponDate, 103) CouponDate, 
		                                      C.Volume BuyVolume, C.Amount BuyAmount, ISNULL(A.Volume , 0) AvailableVolume, 
		                                      CONVERT(varchar, C.MaturityDate, 103) MaturityDate, C.DealPrice Price, B.Frequency, B.BondType,
                                          PaymentMethod, CustBankID,
                                           (CASE WHEN (C.ServiceType = 2 OR C.ConfirmMethod != 0) AND DATEDIFF(day, GetDate(), C.MaturityDate) <= 120 AND GetDate() < C.MaturityDate 
                                                    AND A.Volume=C.Volume AND B.BondType in (3, 4,5) THEN 1
		                                    ELSE 0 
	                                        END) IsRoll,
                                            (CASE WHEN (((C.ServiceType = 2 OR C.ConfirmMethod != 0) AND B.BondTypeGroup = 1 AND B.BondType NOT IN (7,8,11)) 
	                                        OR B.BondTypeGroup = 2) AND C.TradeDate < @CurDate AND @CurDate < C.MaturityDate THEN 1 ELSE 0 END)  IsSell
	                                      FROM ContractHist C
	                                      INNER JOIN BondInfo B ON C.BondCode = B.BondCode
	                                      LEFT JOIN AccountPosition A ON C.ContractNo = A.ContractNo
	                                      WHERE C.ContractNo =  @contractNo AND C.ACCOUNTNO = @accountNo";

        public string GetContractCoupon => @"SELECT CONVERT(varchar, PayDate, 103) CouponDate,
                                              Volume, Amount, 1 AS CouponType
                                              FROM CouponContractInfo 
                                              WHERE ContractNo = @ContractNo AND Status = @Status";

        public string GetContractPriceInfo => @"SELECT UsePriceInfo FROM ContractPriceInfo 
                                                WHERE ContractNo = @ContractNo AND AccountNo = @AccountNo AND Status = 1";

        public string GetOrderBook => @"SELECT (CASE WHEN B.BondType IN (4,5,6) THEN 1
		                                        WHEN B.BondType IN (3) THEN 2
                                                WHEN B.BondType in (7) AND B.BondTypeGroup in (2) THEN 5
		                                        WHEN B.BondType IN (7,8,11) THEN 3
		                                        WHEN B.BondType IN (9,10) THEN 4
	                                      END) ProductType, C.ContractNo, C.ContraContractNo, 
                                        B.IssuerCode BondCode, C.Volume, C.Side, ServiceType as ChannelType, c.Status,
                                        (CASE 
                                            WHEN C.Side='S' AND C.Status = 0 AND C.IsSell IN (1,2) AND C.ServiceType = 2 THEN 1
                                            WHEN C.Side='S' AND C.Status = -23 AND C.ServiceType = 2 THEN 1
                                            WHEN C.Side='B' AND C.Status = 0 AND C.ServiceType = 2 THEN 1
                                            ELSE 0 
                                            END) IsCancel, C.CreatedDate, Convert(varchar, C.TradeDate, 103) TradeDate
	                                      FROM Contract C
	                                      INNER JOIN BondInfo B ON C.BondCode = B.BondCode
	                                      WHERE B.BondTypeGroup in (1,2) AND C.AccountNo = @accountNo AND B.BondType NOT IN (80,81)
                                        AND C.Status in (0,-23)
	                                      UNION ALL
	                                      SELECT (CASE WHEN B.BondType IN (4,5,6) THEN 1
			                                        WHEN B.BondType IN (3) THEN 2
                                                    WHEN B.BondType in (7) AND B.BondTypeGroup in (2) THEN 5
			                                        WHEN B.BondType IN (7,8,11) THEN 3
			                                        WHEN B.BondType IN (9,10) THEN 4
		                                      END) ProductType, C.ContractNo, C.ContraContractNo, 
                                          B.IssuerCode BondCode, C.Volume, C.Side, ServiceType as ChannelType, c.Status,
                                          (CASE 
                                            WHEN C.Side='S' AND C.Status = 0 AND C.IsSell IN (1,2) AND C.ServiceType = 2 THEN 1
                                            WHEN C.Side='S' AND C.Status = -23 AND C.ServiceType = 2 THEN 1
                                            WHEN C.Side='B' AND C.Status = 0 AND C.ServiceType = 2 THEN 1 
                                            ELSE 0 
                                            END) IsCancel, C.CreatedDate, Convert(varchar, C.TradeDate, 103) TradeDate
	                                      FROM ContractHist C
	                                      INNER JOIN BondInfo B ON C.BondCode = B.BondCode
	                                      WHERE C.TradeDate >= @tradeDate AND B.BondTypeGroup in (1,2) AND C.AccountNo = @accountNo AND B.BondType NOT IN (80,81)
                                        AND C.Status != 99 ORDER BY CreatedDate DESC";

        public string GetOrderHist => @"SELECT CONVERT(varchar, C.TradeDate, 103) TradeDate, (CASE WHEN B.BondType in (4,5,6) THEN 1
			                                      WHEN B.BondType IN (3) THEN 2
                                                  WHEN B.BondType in (7) AND B.BondTypeGroup in (2) THEN 5
			                                      WHEN B.BondType IN (7,8,11) THEN 3
			                                      WHEN B.BondType IN (9,10) THEN 4
		                                    END) ProductType, C.ContractNo, C.ContraContractNo, 
                                        B.IssuerCode BondCode, C.Volume, C.Side, ServiceType as ChannelType, c.Status, c.IsSell,
                                        0 IsCancel
	                                    FROM ContractHist C
	                                    INNER JOIN BondInfo B ON C.BondCode = B.BondCode
	                                    WHERE (C.TradeDate >= @fromDate AND C.TradeDate <= @toDate) 
                                      AND B.BondTypeGroup in (1,2) AND C.AccountNo = @accountNo  AND B.BondType NOT IN (80,81)
                                      AND C.Status != 99 AND C.Status != -1 ORDER BY C.CreatedDate DESC";

        public string GetOrderConfirm => @"SELECT CONVERT(varchar, C.TradeDate, 103) TradeDate, (CASE WHEN B.BondType IN (4,5,6) THEN 1
			                                    WHEN B.BondType in (3) THEN 2
                                                WHEN B.BondType in (7) AND B.BondTypeGroup in (2) THEN 5
			                                    WHEN B.BondType in (7,8,11) THEN 3
			                                    WHEN B.BondType in (9,10) THEN 4
		                                    END) ProductType, C.ContractNo, C.ContraContractNo, 
                                          B.IssuerCode BondCode, C.Volume, C.Side, ServiceType AS ChannelType, c.Status
	                                      FROM ContractHist C
	                                      INNER JOIN BondInfo B ON C.BondCode = B.BondCode
	                                      WHERE (C.TradeDate >= @fromDate AND C.TradeDate <= @toDate) 
	                                      AND B.BondTypeGroup = 1 AND C.AccountNo = @accountNo AND C.DocStatus = 1
                                          AND C.Status != 99 AND C.Status != -1";

        public string ConfirmOrder => @"UPDATE ContractHist SET DocStatus = 3 WHERE ContractNo = @ContractNo and AccountNo = @AccountNo";

        public string GetProcessingSellContract => @"SELECT DISTINCT ContractNo, Status FROM (
                                                      SELECT ContractNo, Status
                                                      FROM Contract WHERE Side = 'S' AND Status IN (0,1,-22,-23) AND ContractNo IN @ContractNoList
                                                      UNION ALL
                                                      SELECT ContraContractNo ContractNo, Status
                                                      FROM ContractHist WHERE Side = 'S' AND Status IN (0,1,2,-22,-23) AND ContractNo IN @ContractNoList
                                                      UNION ALL
                                                      SELECT A.ContractNo, A.Status
                                                      FROM AccountPosition A
                                                      INNER JOIN ContractHist B on A.ContractNo = B.ContraContractNo
                                                      WHERE B.Side = 'S' AND A.Status IN (2) AND B.ContractNo IN @ContractNoList
                                                      ) A";

        public string GetCustBankInfo => @"SELECT A.BankAccount, A.BankAccountName, B.BankName, A.SubBranchName
                                              FROM CustBank A
                                              INNER JOIN BankInfo B ON A.BankID = B.BankID
                                              WHERE A.Status = 1 AND CustBankID = @CustBankID";
        public string GetBondPolicyInfo => @"SELECT Bondcode, Pro_CanBuy, Nor_CanBuy 
                                              FROM BondPolicyInfo 
                                              WHERE BondCode IN @BondCodeList
                                                AND EffectivedDate <= getDate() AND ExpiredDate > getdate()";
        public string GetProfesType => @"SELECT ProfesType FROM CustInfo WHERE CustCode = @CustCode";



        #region "Backup Load SQL from XML File"
        #region SQL Command List

        //public string GetContractList => LoadSqlQuery("GetContractList");
        //public string GetContractDetail => LoadSqlQuery("GetContractDetail");
        //public string GetContractCoupon => LoadSqlQuery("GetContractCoupon");
        //public string GetContractPriceInfo => LoadSqlQuery("GetContractPriceInfo");
        //public string GetOrderBook => LoadSqlQuery("GetOrderBook");
        //public string GetOrderHist => LoadSqlQuery("GetOrderHist");
        //public string GetOrderConfirm => LoadSqlQuery("GetOrderConfirm");
        //public string ConfirmOrder => LoadSqlQuery("ConfirmOrder");
        //public string GetProcessingSellContract => LoadSqlQuery("GetProcessingSellContract");
        //public string GetCustBankInfo => LoadSqlQuery("GetCustBankInfo");
        //public string GetBondPolicyInfo => LoadSqlQuery("GetBondPolicyInfo");
        //public string GetProfesType => LoadSqlQuery("GetProfesType");
        #endregion

        #region Private function
        ///// <summary>
        ///// Biến đánh dấu file xml thay đổi thì load lại xml
        ///// </summary>
        //private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);
        //private static readonly ILog log = LogManager.GetLogger(typeof(OrderCommandText));
        //private static string LoadSqlQuery(string sqlName)
        //{
        //    var xmlFile = HttpContext.Current.Server.MapPath("~/bin/Services/SqlQueries/XMLFiles/OrderSqlQuery.xml");

        //    var fileInfo = new FileInfo(xmlFile);

        //    if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
        //    {
        //        log.Info("Load File OrderSqlQuery.xml");
        //        LoadSystemSqlXMLFile(xmlFile);
        //        xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
        //    }
        //    return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        //}

        ///// <summary>
        ///// Biến lưu danh sach câu query trong bộ nhớ
        ///// </summary>
        //private static Dictionary<string, string> _sqlQueriesList;
        //private static void LoadSystemSqlXMLFile(string filePath)
        //{
        //    _sqlQueriesList = new Dictionary<string, string>();

        //    var xdoc = XDocument.Load(filePath);
        //    var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
        //    foreach (var sqlItem in sqlQueryList)
        //    {
        //        _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
        //           sqlItem.Descendants("Sql").Single().Value);
        //    }
        //}
        #endregion
        #endregion
    }
}
