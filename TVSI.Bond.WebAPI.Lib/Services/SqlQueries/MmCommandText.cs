﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public class MmCommandText : IMmCommandText
    {
        public string GetMmContractList => @"SELECT CONVERT(varchar, C.TradeDate, 103) TradeDate, A.ContractNo, A.Volume, 
                CONVERT(varchar, C.MaturityDate, 103) MaturityDate, A.Volume*B.ParValue Amount, 
                        CASE WHEN (C.ServiceType = 2 OR C.ConfirmMethod != 0) AND B.AllowPreSale = 1 THEN 1 ELSE 0 END IsRoll,
                        CASE WHEN (C.ServiceType = 2 OR C.ConfirmMethod != 0) AND B.AllowPreSale = 1 THEN 1 ELSE 0 END IsSell 
                        FROM AccountPosition A
                        INNER JOIN BondInfo B ON A.BondCode = B.BondCode
                        LEFT JOIN ContractHist C ON A.ContractNo = C.ContractNo
                        WHERE A.AccountNo = @AccountNo AND B.BondType IN (80, 81) AND C.Side = 'B' AND A.Status != 99
                        AND A.Volume > 0 ORDER BY A.ContractNo";

        // FOR Golive
        //public string GetMmContractList => @"SELECT CONVERT(varchar, C.TradeDate, 103) TradeDate, A.ContractNo, A.Volume, 
        //        CONVERT(varchar, C.MaturityDate, 103) MaturityDate, A.Volume*B.ParValue Amount, 
        //                CASE WHEN C.ServiceType = 2 AND B.AllowPreSale = 1 THEN 1 ELSE 0 END IsRoll,
        //                CASE WHEN C.ServiceType = 2 AND B.AllowPreSale = 1 THEN 1 ELSE 0 END IsSell 
        //                FROM AccountPosition A
        //                INNER JOIN BondInfo B ON A.BondCode = B.BondCode
        //                LEFT JOIN ContractHist C ON A.ContractNo = C.ContractNo
        //                LEFT JOIN CustOnline D on A.AccountNo = D.AccountNo
        //                WHERE A.AccountNo = @AccountNo AND B.BondType IN (80, 81) AND C.Side = 'B' AND A.Status != 99
        //                AND A.Volume > 0 ORDER BY A.ContractNo";

        public string GetMmShortContractDetail => @"SELECT A.ContractNo, CONVERT(varchar, C.TradeDate, 103) TradeDate, A.Volume, A.Amount, C.Rate, 
                        CONVERT(varchar, C.MaturityDate, 103) MaturityDate FROM AccountPosition A
                        LEFT JOIN ContractHist C ON A.ContractNo = C.ContractNo
                        WHERE A.Accountno = @AccountNo AND A.ContractNo = @ContractNo";

        public string GetMmOrderBook => @"SELECT CONVERT(varchar, C.TradeDate, 103) TradeDate, C.ContractNo, C.ContraContractNo, 
                            (CASE WHEN C.Side = 'S' THEN C.Volume ELSE C.Amount END) Amount, C.Side, C.Status,
	                        (CASE 
                                WHEN C.Side='S' AND C.Status = 0 AND C.IsSell IN (1,2) AND C.ServiceType = 2 THEN 1
                                WHEN C.Side='S' AND C.Status = -23 AND C.ServiceType = 2 THEN 1
                                WHEN C.Side='B' AND C.Status = 0 AND C.ServiceType = 2 THEN 1 
                                ELSE 0 
                            END) IsCancel, CONVERT(varchar, C.TradeDate, 103) CreatedDate
	                        FROM Contract C
	                           INNER JOIN BondInfo B ON C.BondCode = B.BondCode
	                           WHERE  C.AccountNo = @accountNo AND B.BondType IN (80,81)
                               AND C.Status in (0,-23)
                        UNION ALL
                        SELECT CONVERT(varchar, C.TradeDate, 103) TradeDate, C.ContractNo, C.ContraContractNo, 
                            (CASE WHEN C.Side = 'S' THEN C.Volume ELSE C.Amount END) Amount, C.Side, C.Status,
                            (CASE 
                                WHEN C.Side='S' AND C.Status = 0 AND C.IsSell IN (1,2) AND C.ServiceType = 2 THEN 1
                                WHEN C.Side='S' AND C.Status = -23 AND C.ServiceType = 2 THEN 1
                                WHEN C.Side='B' AND C.Status = 0 AND C.ServiceType = 2 THEN 1  
                                ELSE 0 
                            END) IsCancel, CONVERT(varchar, C.TradeDate, 103) CreatedDate
	                        FROM ContractHist C
	                        INNER JOIN BondInfo B ON C.BondCode = B.BondCode
	                        WHERE C.TradeDate >= @tradeDate AND C.AccountNo = @accountNo AND B.BondType IN (80,81)
                                                                AND C.Status != 99
                            ORDER BY CreatedDate DESC";

        public string GetMmOrderHistList => @"SELECT CONVERT(varchar, C.TradeDate, 103) TradeDate, C.ContractNo, C.ContraContractNo, 
                                        C.Amount, C.Side, c.Status, 0 IsCancel, CONVERT(varchar, C.TradeDate, 103) CreatedDate
	                                    FROM ContractHist C
	                                    INNER JOIN BondInfo B ON C.BondCode = B.BondCode
	                                    WHERE (C.TradeDate >= @fromDate AND C.TradeDate <= @toDate) 
                                      AND C.AccountNo = @accountNo  AND B.BondType IN (80,81)
                                      AND C.Status != 99 AND C.Status != -1
                                      ORDER BY C.CreatedDate DESC";

        public string GetMmOrderConfrimList => @"SELECT CONVERT(varchar, C.TradeDate, 103) TradeDate, C.ContractNo, C.ContraContractNo, 
                                        C.Amount, C.Side, c.Status, 0 IsCancel
	                                    FROM ContractHist C
	                                    INNER JOIN BondInfo B ON C.BondCode = B.BondCode
	                                    WHERE (C.TradeDate >= @fromDate AND C.TradeDate <= @toDate) 
                                      AND C.AccountNo = @accountNo  AND B.BondType IN (80,81)
                                      AND C.Status in (3, -10, -11)";

        //public string GetMmOrderConfrimList => @"SELECT CONVERT(varchar, C.TradeDate, 103) TradeDate, C.ContractNo, C.ContraContractNo, 
        //                                C.Amount, C.Side, c.Status, 0 IsCancel
	       //                             FROM ContractHist C
	       //                             INNER JOIN BondInfo B ON C.BondCode = B.BondCode
        //                                INNER JOIN CustOnline D on C.AccountNo = D.AccountNo
	       //                             WHERE (C.TradeDate >= @fromDate AND C.TradeDate <= @toDate) 
        //                              AND C.AccountNo = @accountNo  AND B.BondType IN (80,81)
        //                              AND C.Status in (3, -10, -11) AND D.IsMM = 1";

        public string ConfirmMmOrder => @"UPDATE ContractHist SET DocStatus = 3 
                        WHERE ContractNo = @ContractNo and AccountNo = @AccountNo";
    }
}
