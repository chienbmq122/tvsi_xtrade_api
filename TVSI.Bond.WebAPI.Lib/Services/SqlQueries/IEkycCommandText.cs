﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public interface IEkycCommandText
    {
        string TVSI_sDangKyKhachHangEkycMobile {get;} 
        string TVSI_sGetSaleId { get; }
        string TVSI_sDangKyTaiKhoanEkycMoRongMobile { get; }
        string TVSI_SLogThayDoiThongTinKHEkyc{get;}
        string TVSI_SDatLichVideoCall { get; }
        string TVSI_SConfirmEkycMobile { get; }
        string TVSI_sTHAY_DOI_THONG_TIN_KHACH_HANG_INSERT { get; }
        string TVSI_SGetCustCRM { get; }
        string TVSI_SGetCodeBranch { get; }
        string TVSI_SUpdatLichSuVideoCall { get; }
        string TVSI_sGetInfoCustomer { get; }
        string TVSI_sGetManInfo { get; }
        string TVSI_SCancelVideoCall { get; }
        string TVSI_SGetCustInfoCommon { get; }
        string TVSI_SInsertEkycFilesType1 { get; }
        string TVSI_SInsertEkycFilesType2 { get; }
        string TVSI_SUpdateCardId { get; }

        string CompareInfoCust { get; }
        string GetPhoneByNumCustInno01 { get; }
        string GetPhoneByNumCustInno02 { get; }
        string GetPhoneByCustIdInno01 { get; }
        string GetInternalAccountInfo { get; }
        string GetChangeInfoHist { get; }
        string GetChangeInfoHistReject { get; }
        string GetUserInfoCust { get; }
        string GetVideoCallScheduleHist { get; }
        string GetInternalAccountDetail { get; }
        string GetCardIDCommon { get; }
        string GetUserInfoExtendCRM { get; }
        string GetCustInfo { get; }
        string GetUserInfo { get; }
        string GetSourceData { get; }
        string GetUserInfoExtend { get; }
        string GetCardIDCRM { get; }
        string GetCMNDCRM { get; }
        string GetBankList { get; }
        string GetSubBranchList { get; }
        string GetProvinceList { get; }
        string GetBankName { get; }
        string GetSubBranchName { get; }
        string GetProvinceName { get; }
        string GetBankInfo { get; }
        string GetListPlaceOfIssue { get; }
        string GetVideoCallHist { get; }
        string GetBankById { get; }
        string CheckOTPAccept { get; }
        string CheckChangeInfoCust { get; }
        string GetVideoUpdate { get; }
        string GetAccountExtend { get; }
        string UpdateVideoSigatureExtend { get; }
        string CheckCustCodeMargin { get; }
        string CheckAccountSixMargin { get; }
        string GetAccountRegisterMargin { get; }
        string GetAccountOpenMargin { get; }
        string CheckAddProfile { get; }
    }
}