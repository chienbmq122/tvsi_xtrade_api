﻿
using TVSI.Bond.WebAPI.Lib.Models.Ekyc;

namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public interface ISystemCommandText
    {
        string GetCustomerStatus { get; }
        string GetAccountNoList { get; }

        string GetAlphaAccount { get; }

        string GetCustomerIdByPassword { get; }
        string UpdatePasswordOfCustomer { get; }

        string GetCustomerIdByPin { get; }
        string UpdatePinOfCustomer { get; }

        string GetNotificationList { get; }
        string GetNumOfUnreadNotification { get; }
        string SyncNotificationRead { get; }

        string GetBondFilterByAccount { get; }
        string GetBondFilter { get; }
        string GetEventTypeFilter { get; }

        string GetEventList { get; }
        string GetEventDetail { get; }

        string GetCellPhone { get; }
        string GetCustomerPhone { get; }
        string GetForgotPassword { get; }

        string SendPassResetCode { get; }

        string GetForgotPIN { get; }

        string GetVerifyAccount { get;}
        string SendConfirmCode { get; }

        string SendPINResetCode { get; }

        string CheckConfirmCode { get; }

        string CheckConfirmCodePIN { get; }

        string UpdateNewPass { get; }
        string UpdateNewPin { get; }

        string InactiveCustomer { get; }
    }
}
