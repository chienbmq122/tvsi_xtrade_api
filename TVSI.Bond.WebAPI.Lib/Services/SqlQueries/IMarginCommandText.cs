﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public interface IMarginCommandText
    {
        string InsertServicePreferMargin { get; }
        string InsertServiceMargin { get; }
        string UpdateServicePreferMargin { get; }
        string UpdateServiceMargin { get; }
        string GetInfoPreferById { get; }
        string GetInfoServiceById { get; }
        string GetServicePreferByAccountNo { get; }
        
        string GetServiceMarginByAccountNo { get; }
        string GetServicePreferHist { get; }
        string GetServiceHist { get; }
    }
}