﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public class EkycCommandText : IEkycCommandText
    {
        #region Stored

        public string TVSI_sDangKyKhachHangEkycMobile => @"TVSI_sDANG_KY_MO_TAI_KHOAN_EKYC_MOBILE_API";
        public string TVSI_sGetSaleId => @"TVSI_sGET_ID_SALE";
        public string TVSI_sGetManInfo => @"TVSI_sLAY_THONG_TIN_NGUOI_DAI_DIEN";

        public string TVSI_SCancelVideoCall => @"TVSI_sCANCEL_DAT_LICH_VIDEO_CALL_EKYC";
        public string TVSI_SGetCustInfoCommon => @"TVSI_sLAY_THONG_TIN_TAI_KHOAN_KHACH_HANG_THEO_SO_TAI_KHOAN_044_v2";
        public string TVSI_SInsertEkycFilesType1 => @"TVSI_sINSERT_EKYC_FILES_TYPE_1";
        public string TVSI_SInsertEkycFilesType2 => @"TVSI_sINSERT_EKYC_FILES_TYPE_2";
        public string TVSI_SUpdateCardId => @"TVSI_sUPDATE_CARDID_EKYC_MOBILE";

        public string TVSI_sDangKyTaiKhoanEkycMoRongMobile =>
            @"TVSI_sDANG_KY_MO_TAI_KHOAN_NUOC_NGOAI_MO_RONG_EKYC_MOBILE";

        public string TVSI_SLogThayDoiThongTinKHEkyc => @"TVSI_sLOG_THAY_DOI_THONG_TIN_KHACH_HANG_EKYC";
        public string TVSI_SDatLichVideoCall => @"TVSI_sDAT_LICH_VIDEO_CALL_INSERT";
        public string TVSI_SConfirmEkycMobile => @"TVSI_sCONFIRM_EKYC_API_MOBILE";
        public string TVSI_sTHAY_DOI_THONG_TIN_KHACH_HANG_INSERT => @"TVSI_sTHAY_DOI_THONG_TIN_KHACH_HANG_INSERT_IWORK";
        public string TVSI_SGetCustCRM => @"TVSI_sGET_TAI_KHOAN_KH_EKYC_CRM";
        public string TVSI_SGetCodeBranch => @"TVSI_sGET_MA_CHI_NHANH_CUA_SALE";
        public string TVSI_SUpdatLichSuVideoCall => @"TVSI_sLICH_SU_VIDEO_CALL_INSERT";
        public string TVSI_sGetInfoCustomer => @"TVSI_sGET_INFO_CUSTOMER";

        #endregion


        #region CompareInfo

        public string CompareInfoCust =>
            @"  SELECT T.ma_khach_hang as UserID,T.so_tai_khoan as AccountNo, T.ten_khach_hang as FullName,T.so_cmnd_passport as CardID,T.ngay_sinh as BirthDay,T.ngay_cap as IssueDate ,T.noi_cap as CardIssuer, CASE WHEN T.gioi_tinh = 'F' THEN 2 when t.gioi_tinh = 'M' then 1 end as Sex  FROM TVSI_TAI_KHOAN_KHACH_HANG T where T.ma_khach_hang =  @custcode";

        #endregion

        #region Sql

        /*
        public string DeleteBankAccount => @"UPDATE BANKACCOUNT SET ";
        */

        public string GetPhoneByNumCustInno01 => @"SELECT Phone as SmsPhone_01 FROM [dbo].[CUSTOMERPHONE] WHERE CustomerID = @custid and IsActive = 1  and IsSMS = 1 ";
        public string GetPhoneByNumCustInno02 => @"SELECT Phone as SmsPhone_02 FROM [dbo].[CUSTOMERPHONE] WHERE CustomerID = @custid and IsActive = 1 and IsSMS = 0 and IsCC = 1";
        public string GetPhoneByCustIdInno01 => @"SELECT ID, CustomerID ,Phone,IsSMS,IsCC,IsActive FROM [dbo].[CUSTOMERPHONE] WHERE CustomerID = @custid and IsActive = 1  and IsSMS = 1 ";

        public string GetInternalAccountInfo =>
            @"SELECT A.CustomerId as CustCode, A.InternalAccountNo AS InternalAccountNo, A.InternalCustomerName AS InternalAccountName
                                                        FROM INTERNALACCOUNT A
                                                        WHERE A.InternalStatus = 1 AND A.CustomerId = @CustCode";

        public string GetChangeInfoHist =>
            @"SELECT lich_su_thay_doi_id As RegistServiceID,
              noi_dung_thay_doi ContentChange,
       convert(varchar,ngay_tao, 103) as RegistTime,
                                               convert(varchar,ngay_cap_nhat_cuoi, 103)  as  EffectiveDate,
                                           mo_ta ChangeType,
                                            branchId as RegistTypeName,case WHEN trang_thai = 0 THEN N'Chờ xử lý' 
                                                when trang_thai = 1 THEN N'Đang xử lý' 
                                                WHEN trang_thai = 2 THEN N'Đã xử lý' 
                                                WHEN trang_thai = 9 THEN N'Từ chối' 
                                                WHEN trang_thai = 99 THEN N'Đã hủy' 
                                                WHEN trang_thai = 3 THEN N'Đang xử lý (VSD)'
                                                WHEN trang_thai = 10 THEN N'Hoàn thành (VSD)' 
                                                WHEN trang_thai = 999 THEN N'Từ chối (VSD)' END  as StatusName,
                                                     trang_thai as StatusNameNum
                                                     FROM TVSI_LOG_THAY_DOI_THONG_TIN_KHACH_HANG
                                            WHERE  CONVERT(date,ngay_tao) >= CONVERT(date,@FromDate) and CONVERT(date,ngay_tao) <= CONVERT(date,@ToDate) and ma_khach_hang = @custcode 
                                            ORDER BY lich_su_thay_doi_id DESC
                                            OFFSET @start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string GetChangeInfoHistReject => @"SELECT lich_su_thay_doi_id As RegistServiceID,
                                                    ly_do_tu_choi ReasonsReject
                                                     FROM TVSI_LOG_THAY_DOI_THONG_TIN_KHACH_HANG
                                            WHERE  CONVERT(date,ngay_tao) >= CONVERT(date,@FromDate) and CONVERT(date,ngay_tao) <= CONVERT(date,@ToDate) and ma_khach_hang = @custcode and trang_thai = 9
                                            ORDER BY lich_su_thay_doi_id DESC
                                            OFFSET @start ROWS FETCH NEXT @PageSize ROWS ONLY";

        /*public string GetChangeInfoHist => @"SELECT thong_tin_thay_doi_id AS RegistServiceID, convert(varchar,ngay_tao, 103)  as RegistTime,
                                             loai_hinh_tai_khoan as RegistTypeName, trang_thai as StatusName 
                                             FROM TVSI_LOG_THAY_DOI_THONG_TIN_KHACH_HANG_EKYC 
                                             where CONVERT(date,ngay_tao) >= CONVERT(date,@FromDate) and CONVERT(date,ngay_tao) <= CONVERT(date,@ToDate) and ma_khach_hang = @custcode";*/


        public string GetVideoCallScheduleHist =>
            @"SELECT dat_lich_video_call_id AS ScheduleID,convert(varchar,ngay_tao, 103) AS ScheduleTime, 
                                                    noi_dung AS Description, thoi_gian_dat_lich_goi as CallScheduleTime, 
                                                    trang_thai_ss_duyet AS Status FROM TVSI_DAT_LICH_VIDEO_CALL_EKYC
                                                    WHERE ma_khach_hang = @custcode and CONVERT(date,ngay_tao) >= CONVERT(date,@FromDate) and CONVERT(date,ngay_tao) <= CONVERT(date,@ToDate)";

        public string GetInternalAccountDetail =>
            @"SELECT A.CustomerId as CustCode, A.InternalCustomerNo AS InternalCustomerNo, A.InternalCustomerName AS InternalAccountName,A.InternalStatus as Status
                  FROM INTERNALACCOUNT A
                  WHERE A.InternalStatus = 1 AND A.CustomerId = @CustCode AND A.InternalAccountNo = @InternalAccountNo";


        public string GetCardIDCommon =>
            @"SELECT so_cmnd_passport FROM TVSI_TAI_KHOAN_KHACH_HANG WHERE trang_thai = 1 AND so_cmnd_passport = @so_cmnd";

        public string GetCustInfo =>
            @"SELECT ma_khach_hang as UserId, ma_kinh_doanh as AccountNo, ten_khach_hang as CustName FROM TVSI_TAI_KHOAN_KHACH_HANG where ma_khach_hang = @CustCode";
        
        public string GetUserInfo =>
            @"SELECT dk.ma_khach_hang as CustCode,dk.so_tai_khoan as AccountNo,dk.gioi_tinh as Gender,dk.noi_cap as CardIssue,dk.ngay_sinh as BirthDayOn, dk.ngay_cap as  IssueDateOn,
                                        dk.ten_khach_hang as CustName,dk.so_cmnd_passport as CardID,
                                       dk.dia_chi_lien_lac as Address , dk.so_dien_thoai as Mobile,dk.dia_chi_email as Email FROM TVSI_TAI_KHOAN_KHACH_HANG dk
                                        where dk.ma_khach_hang = @ma_khach_hang";

        public string GetSourceData => @"select nguon_du_lieu from TVSI_DANG_KY_MO_TAI_KHOAN where ma_khach_hang = @ma_khach_hang";

        public string GetUserInfoCust =>
            @"SELECT dk.ma_khach_hang as CustCode,dk.so_tai_khoan  as AccountNo, dk.ten_khach_hang as CustName,dk.so_cmnd_passport as CardID,
                                       dk.dia_chi_lien_lac as Address , dk.so_dien_thoai as Mobile,dk.dia_chi_email as Email,dk.ma_kinh_doanh, dk.ngay_cap,dk.noi_cap, dk.ngay_sinh FROM TVSI_TAI_KHOAN_KHACH_HANG dk where dk.ma_khach_hang = @ma_khach_hang";


        public string GetUserInfoExtend =>
            @"SELECT et.thong_tin_mo_rong as extend FROM TVSI_DANG_KY_MO_TAI_KHOAN_MO_RONG et where et.ma_khach_hang = @ma_khach_hang";

        public string GetUserInfoExtendCRM =>
            @"SELECT t.thong_tin_mo_rong as extend FROM TVSI_DANG_KY_MO_TAI_KHOAN_MO_RONG t where t.ho_so_mo_tk_id = @id";

        public string GetCardIDCRM =>
            @"SELECT * from TVSI_DANG_KY_MO_TAI_KHOAN where so_cmnd=@so_cmnd and trang_thai_tk <> 99";

        public string GetCMNDCRM =>
            @"SELECT T.so_cmnd from TVSI_DANG_KY_MO_TAI_KHOAN T WHERE T.dang_kyid = @dangkyid AND ConfirmStatus = 1";

        public string GetProvinceName => @"SELECT A.ProvinceName FROM PROVINCES A where A.ProvinceID=@provinceID";

        public string GetBankInfo =>
            @"SELECT A.BANKACCOUNTID as BankAccountID, A.BANKACCOUNT AS BankAccount, A.BENEFICIARY AS BankAccountName,
                                              A.BANKNAME AS BankName
                                        FROM BANKACCOUNT A
                                        WHERE A.STATUS = 1 AND A.CUSTOMERID = @CustCode";

        public string GetListPlaceOfIssue =>
            @"SELECT A.Category AS Category , A.CategoryName as CategoryName FROM ExtendCust A WHERE A.CategoryType = 13";

        public string GetVideoCallHist =>
            @"SELECT A.lich_su_video_call_id as VideoCallID, A.trang_thai_goi as SourceType,
                                             A.thoi_gian_goi as VideoCallTime, CAST(CONVERT(TIME(0),A.tong_thoi_gian_goi) AS VARCHAR(15)) as VideoCallTotalTime FROM TVSI_LICH_SU_VIDEO_CALL_EKYC A where A.ma_khach_hang =@CustCode and CONVERT(date,thoi_gian_goi) >= CONVERT(date,@FromDate) and CONVERT(date,thoi_gian_goi) <= CONVERT(date,@ToDate)";

        public string GetBankById =>
            @"SELECT A.BankAccountID as BankAccountID, A.BANKACCOUNT AS BankAccount, A.BENEFICIARY AS BankAccountName,
                                                  A.BANKNAME AS BankName, A.PROVINCE as ProvinceName, A.Status as Status
                                            FROM BANKACCOUNT A
                                            WHERE A.STATUS = 1 AND A.CUSTOMERID = @CustCode AND  A.BANKACCOUNTID = @BankAccountID";

        public string CheckOTPAccept =>
            @"select ConfirmCode from TVSI_DANG_KY_MO_TAI_KHOAN where ConfirmCode = @ConfirmCode and ConfirmSms = @ConfirmOtp and ConfirmStatus = 0";

        public string CheckChangeInfoCust => @"
         select thong_tin_thay_doi from [dbo].[TVSI_LOG_THAY_DOI_THONG_TIN_KHACH_HANG] where ma_khach_hang = @UserId and trang_thai = 0";

        public string GetVideoUpdate =>
            @"SELECT T.* FROM [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN_MO_RONG] T WHERE T.ma_khach_hang = @ma_khach_hang AND T.trang_thai_update = 1";

        public string GetAccountExtend =>
            @"SELECT T.* FROM [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN_MO_RONG] T WHERE T.ma_khach_hang = @ma_khach_hang";

        public string GetBankList =>
            @"SELECT A.BankNo as BankNo, A.ShortName as ShortName FROM BANKLIST A WHERE STATUS = 1";

        public string GetSubBranchList =>
            @"SELECT A.BranchNo as BranchNo,A.BankNo as BankNo, A.BranchName as BranchName FROM BRANCHNOLIST A WHERE BankNo = @bankNo AND Status in (1,3)";

        public string GetProvinceList => @"SELECT A.ProvinceID, A.ProvinceName FROM PROVINCES A";
        public string GetBankName => @"SELECT A.ShortName as ShortName FROM BANKLIST A WHERE A.BankNo = @bankNo";

        public string GetSubBranchName =>
            @"SELECT A.ShortBranchName as ShortBranchName FROM BRANCHNOLIST A WHERE BankNo = @bankNo AND A.BranchNo=@branchNo";

        public string UpdateVideoSigatureExtend =>
            @"UPDATE TVSI_DANG_KY_MO_TAI_KHOAN_MO_RONG SET thong_tin_mo_rong = @thong_tin_mo_rong, trang_thai_update = 1,ngay_cap_nhat = @ngay_cap_nhat WHERE ma_khach_hang = @ma_khach_hang";

        public string CheckCustCodeMargin => @"select ma_khach_hang from [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] where trang_thai_tk = '999' and ma_khach_hang = @CustCode ";

        public string CheckAccountSixMargin =>
            @"select ma_khach_hang from TVSI_THONG_TIN_TAI_KHOAN where loai_tai_khoan = 6 and ma_khach_hang = @CustCode and trang_thai = 1";

        public string GetAccountRegisterMargin => @" select CUSTOMERID + '6'  as CustCode, CUSTOMERNAME as FullName, IDENTITYCARD as CardID, CELLPHONE as PhoneNumber , CONTACTEMAIL as Email from CUSTOMER where CUSTOMERID = @CustCode and STATUS = 1 ";
        public string GetAccountOpenMargin => @"
  select ma_khach_hang as CustCode, so_tai_khoan as CustCode044c, ten_khach_hang as FullName, ngay_sinh as Birthday, 
  so_cmnd_passport as CardID, ngay_cap as IssueDate, noi_cap as CardIssue, dia_chi_lien_lac as Address, 
         ma_chi_nhanh as BranchId from TVSI_TAI_KHOAN_KHACH_HANG where ma_khach_hang = @CustCode and trang_thai = 1 ";

        public string CheckAddProfile => @"SELECT MA_KHACH_HANG FROM [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] WHERE trang_thai_ho_so = '101' and ma_khach_hang = @CustId and ( nguon_du_lieu =  4 or nguon_du_lieu = 6 )";

        #endregion
    }
}