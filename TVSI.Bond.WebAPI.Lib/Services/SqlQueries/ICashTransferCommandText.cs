﻿namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public interface ICashTransferCommandText
    {
        string GetBankInfo { get; }
        string GetBalance { get; }
        string GetBankInfoByID { get; }
        string GetInternalAccountInfo { get; }
        string GetInternalAccountInfoByID { get; }
        string  GetHoldAmount { get; }
    }
}
