using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public class NoticationCommandText : INoticationCommandText
    {
        #region SQL Command List
        public string GetHistoryNotication => @"SELECT notification_id, type_notification_id, template_notification_id, title_notification,
                                                       content_notification, content_en_notification, custcode, priority , status, src_notification,
                                                       time_send as dateSend, img, method_create, channel_send, is_read, is_active, create_by, create_at,
                                                       update_by,update_at,src_screen 
                                                  FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                  WHERE custcode = @custcode AND channel_send = 1 AND is_push = 1 AND src_notification <> 'WarningMarket' AND src_notification <> 'WarningStock' 
                                                  ORDER BY time_send DESC 
                                                  OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        public string CountGetHistoryNotication => @"SELECT (SELECT COUNT(1) FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                            WHERE custcode = @custcode AND channel_send = 1 AND is_push = 1 AND src_notification <> 'WarningMarket' AND src_notification <> 'WarningStock') as TotalItem,
                                                            (SELECT COUNT(1) FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                            WHERE custcode = @custcode AND channel_send = 1 AND is_push = 1 AND src_notification <> 'WarningMarket' AND src_notification <> 'WarningStock' AND is_read = 0) as UnRead";

        public string GetNoticationOfDay => @"SELECT notification_id, type_notification_id, template_notification_id, title_notification,
                                                       content_notification, content_en_notification, custcode, priority , status, src_notification,
                                                       time_send as dateSend, img, method_create, channel_send, is_read, is_active, create_by, create_at,
                                                       update_by,update_at,src_screen  
                                                  FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                  WHERE custcode = @custcode AND channel_send = 1 AND CAST(time_send as Date) = @Date 
                                                  AND is_push = 1 AND src_notification <> 'WarningMarket' AND src_notification <> 'WarningStock' 
                                                  ORDER BY time_send DESC 
                                                  OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        public string CountGetNoticationOfDay => @"SELECT (SELECT COUNT(1) FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                          WHERE custcode = @custcode AND channel_send = 1 AND CAST(time_send as Date) = @Date 
                                                          AND is_push = 1 AND src_notification <> 'WarningMarket' AND src_notification <> 'WarningStock') as TotalItem,
                                                        (SELECT COUNT(1) FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                          WHERE custcode = @custcode AND channel_send = 1 AND CAST(time_send as Date) = @Date 
                                                          AND is_push = 1 AND src_notification <> 'WarningMarket' AND src_notification <> 'WarningStock' AND is_read = 0) as UnRead";
                                                         
        public string GetRegisterNotication => @"SELECT register_notification_id, custcode, type_notification_id, channel_send, customer_reject,
                                                        is_active, create_by, create_at, update_by, update_at
                                                 FROM TVSI_KHACH_HANG_DANG_KY_THONG_BAO WHERE custcode = @custcode AND channel_send = 1";
        public string CountRegisterNotication => @"SELECT COUNT(1) FROM TVSI_KHACH_HANG_DANG_KY_THONG_BAO 
                                                   WHERE custcode = @custcode AND channel_send = 1";
        public string PostRegisterNotication => @"INSERT INTO TVSI_KHACH_HANG_DANG_KY_THONG_BAO(custcode, type_notification_id, channel_send, 
                                                        customer_reject, create_by, create_at, update_by, update_at) 
                                                  VALUES(@custcode, @loai_thongbao_id, 1, 1, @userName, CURRENT_TIMESTAMP, @userName, CURRENT_TIMESTAMP)";
        public string UpdateRegisterNotication => @"UPDATE TVSI_KHACH_HANG_DANG_KY_THONG_BAO SET type_notification_id = @loai_thongbao_id, 
                                                            customer_reject = 1, update_by = @custcode, update_at = CURRENT_TIMESTAMP 
                                                    WHERE custcode = @custcode AND channel_send = 1 AND register_notification_id = @dang_ky_thongbao_id";

        public string GetNoticationType => @"SELECT type_notification_id, name_notification, method_notification, type_notification_parent_id
                                             FROM TVSI_DANH_SACH_LOAI_THONG_BAO WHERE is_active = 1";

        public string ReadNotication => @"UPDATE TVSI_HANG_DOI_THONG_BAO_KHACH_HANG SET is_read = 1, update_by = @custcode, update_at = CURRENT_TIMESTAMP WHERE notification_id IN @thongbao_id";

        public string CreateNotication => @"INSERT INTO TVSI_HANG_DOI_THONG_BAO_KHACH_HANG(
	                                            type_notification_id, title_notification, content_notification, content_en_notification, custcode, priority, status, 
	                                            src_notification, time_send, method_create, channel_send, is_read, 
	                                            is_active, create_by, create_at, update_by, update_at, src_screen, is_push)
                                            VALUES(@loai_thongbao_id, @tieu_de, @noidung, @noidung_en, @custcode, @priority, 1, 
	                                            @nguon_thongbao, @thoi_gian_gui, 1, 1, 0, 1, @userName, CURRENT_TIMESTAMP, @userName,
                                                CURRENT_TIMESTAMP, @src_screen, 1)";
        public string GetHistoryWarning => @"SELECT notification_id, type_notification_id, template_notification_id, title_notification,
                                                       content_notification, content_en_notification, custcode, priority , status, src_notification,
                                                       time_send as dateSend, img, method_create, channel_send, is_read, is_active, create_by, create_at,
                                                       update_by,update_at,src_screen 
                                                  FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                  WHERE custcode = @custcode AND channel_send = 1 AND is_push = 1 AND (src_notification = 'WarningMarket' OR src_notification = 'WarningStock') 
                                                  ORDER BY time_send  DESC 
                                                  OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        public string CountGetHistoryWarning => @"SELECT (SELECT COUNT(1) FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                        WHERE custcode = @custcode AND channel_send = 1 AND is_push = 1 AND (src_notification = 'WarningMarket' OR src_notification = 'WarningStock')) as TotalItem,
                                                        (SELECT COUNT(1) FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                        WHERE custcode = @custcode AND channel_send = 1 AND is_push = 1 AND (src_notification = 'WarningMarket' OR src_notification = 'WarningStock') AND is_read = 0) as UnRead";


        public string GetWarningOfDay => @"SELECT notification_id, type_notification_id, template_notification_id, title_notification,
                                                       content_notification, content_en_notification, custcode, priority , status, src_notification,
                                                       time_send as dateSend, img, method_create, channel_send, is_read, is_active, create_by, create_at,
                                                       update_by,update_at,src_screen  
                                                  FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                  WHERE custcode = @custcode AND channel_send = 1 AND is_push = 1 AND (src_notification = 'WarningMarket' OR src_notification = 'WarningStock') AND CAST(time_send as Date) = @Date 
                                                  ORDER BY time_send DESC 
                                                  OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        public string CountWarningOfDay => @"SELECT (SELECT COUNT(1) FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                    WHERE custcode = @custcode AND channel_send = 1 AND is_push = 1 AND (src_notification = 'WarningMarket' OR src_notification = 'WarningStock') AND CAST(time_send as Date) = @Date) as TotalItem,
                                                    (SELECT COUNT(1) FROM TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                    WHERE custcode = @custcode AND channel_send = 1 AND is_push = 1 AND (src_notification = 'WarningMarket' OR src_notification = 'WarningStock') AND CAST(time_send as Date) = @Date AND is_read = 0) as UnRead";

        public string UpdateStatusNotication => @"UPDATE TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                                  SET status = @status, update_by = @custcode, update_at = CURRENT_TIMESTAMP 
                                                  WHERE notification_id IN @thongbao_id";

        public string readAllNotication => @"UPDATE TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                             SET is_read = 1, update_by = @custcode, update_at = CURRENT_TIMESTAMP 
                                             WHERE custcode = @custcode AND channel_send = @kenh_gui_tin AND src_notification <> 'WarningMarket' 
                                             AND src_notification <> 'WarningStock' AND is_read = 0";

        public string readAllWarning => @"UPDATE TVSI_HANG_DOI_THONG_BAO_KHACH_HANG 
                                          SET is_read = 1, update_by = @custcode, update_at = CURRENT_TIMESTAMP 
                                          WHERE custcode = @custcode AND channel_send = @kenh_gui_tin 
                                          AND (src_notification = 'WarningMarket' OR src_notification = 'WarningStock') AND is_read = 0";
        #endregion
    }
}
