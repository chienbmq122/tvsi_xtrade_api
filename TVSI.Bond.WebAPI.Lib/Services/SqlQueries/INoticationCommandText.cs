﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Bond.WebAPI.Lib.Services.SqlQueries
{
    public interface INoticationCommandText
    {
        string GetHistoryNotication { get; }
        string CountGetHistoryNotication { get; }
        string GetNoticationOfDay { get; }
        string CountGetNoticationOfDay { get; }
        string GetRegisterNotication { get; }
        string CountRegisterNotication { get; }
        string PostRegisterNotication { get; }
        string UpdateRegisterNotication { get; }
        string GetNoticationType { get; }
        string ReadNotication { get; }
        string CreateNotication { get; }
        string GetHistoryWarning { get; }
        string CountGetHistoryWarning { get; }
        string GetWarningOfDay { get; }
        string CountWarningOfDay { get; }
        string UpdateStatusNotication { get; }
        string readAllNotication { get; }
        string readAllWarning { get; }
    }
}
