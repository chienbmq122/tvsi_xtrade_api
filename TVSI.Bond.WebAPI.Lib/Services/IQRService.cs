﻿using System.Threading.Tasks;
using TVSI.Bond.WebAPI.Lib.Models.QRCode;

namespace TVSI.Bond.WebAPI.Lib.Services
{
    public interface IQRService
    {
        Task<QRCodeResult> GetQRCode(QRCodeRequest model);
    }
}