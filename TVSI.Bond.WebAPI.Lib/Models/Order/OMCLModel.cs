﻿using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Models.Order
{
    public class ContractListRequest : BaseRequest
    {
        [Description("Loại sản phẩm")]
        public string ProductType { get; set; }
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

        [Description("Mã Trái phiếu")]
        public string BondCode { get; set; }
        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }
        [Description("Ngày mua FROM")]
        public string FromDate { get; set; }
        [Description("Ngày mua TO")]
        public string ToDate { get; set; }
    }

    public class ContractListResult
    {
        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }

        [Description("Tên sản phẩm")]
        public string ProductTypeName {
            get { return ResourceFile.OrderModule.ResourceManager.GetString("ProductType_" + ProductType); }
        }

        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }
        [Description("Mã Trái phiếu")]
        public string BondCode { get; set; }
        [Description("Tên trái phiếu")]
        public string Issuer { get; set; }
        [Description("Ngày mua")]
        public string TradeDate { get; set; }
        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }
        [Description("Số lượng")]
        public int Volume { get; set; }
        [Description("Kỳ hạn")]
        public int Term { get; set; }
        [Description("Được phép gia hạn?")]
        public bool IsRoll { get; set; }
        [Description("Được phép bán?")]
        public bool IsSell { get; set; }

    }

    public class ContractDetailRequest : BaseRequest
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }
    }

    #region Contract Detail
    public class ContractDetailResult
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

        [JsonIgnore]
        public string BondCode { get; set; }

        [Description("Mã Trái phiếu")]
        public string IssuerCode { get; set; }

        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }
        [Description("Tên sản phẩm")]
        public string ProductTypeName
        {
            get
            {
                if (BondCode.StartsWith("TVSI_TP_") || ProductType == 5)
                    return ResourceFile.OrderModule.ResourceManager.GetString("ProductType_" + ProductType);

                if (!string.IsNullOrEmpty(BondCode) && !string.IsNullOrEmpty(IssuerCode))
                {
                    if (BondCode.Contains(IssuerCode))
                    {
                        return BondCode.Replace(IssuerCode, "").Replace("-", "");
                    }
                }

                if (!string.IsNullOrEmpty(BondCode) && BondCode.IndexOf("-") > 0)
                    return BondCode.Substring(BondCode.IndexOf("-") + 1);

                return BondCode;
            }
        }

        [Description("Kỳ hạn")]
        [JsonIgnore]
        public int Term { get; set; }

        [Description("Kỳ hạn/Kỳ trả lãi")]
        public string TermName {
            get
            {
                if ((new List<int>{3, 5, 6, 80, 81}).Contains(BondType))
                {
                    return ResourceFile.OrderModule.Term_Name_02;
                }

                if ((new List<int> {7,8,9,10,11}).Contains(BondType))
                {
                    return ResourceFile.OrderModule.Term_Name_03;
                }

                if (Frequency > 0 && BondType == 4)
                {
                    var repeatTime = 12/Frequency;
                    return string.Format(ResourceFile.OrderModule.Term_Name_01, repeatTime + "");
                }

                return "";
            }
        }

        [Description("Ngày mua")]
        public string TradeDate { get; set; }

        [Description("Ngày tính lãi")]
        public string CouponDate { get; set; }

        [Description("Số lượng mua")]
        public int BuyVolume { get; set; }

        [Description("Đơn giá")]
        public decimal Price { get; set; }

        [Description("Tổng giá trị mua")]
        public decimal BuyAmount { get; set; }
        [Description("Số lượng khả dụng")]
        public int AvailableVolume { get; set; }

        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }

        [Description("Tài khoản nhận tiền")]
        public string MoneyReceiver { get; set; }

        [Description("Danh sách Chi tiết lệnh")]
        public List<OrderDetailInfo> OrderDetailList { get; set; }
        [Description("Dòng tiền đã nhận")]
        public List<CouponDetailInfo> ReceivedCouponList { get; set; }

        [Description("Lịch thanh toan dự kiến")]
        public List<CouponDetailInfo> CouponList { get; set; }

        #region Property Ignore
        [JsonIgnore]
        public int Frequency { get; set; }
        [JsonIgnore]
        public int BondType { get; set; }
        [JsonIgnore]
        public int PaymentMethod { get; set; }
        [JsonIgnore]
        public int? CustBankID { get; set; }

        public bool IsSell { get; set; }
        public bool IsRoll { get; set; }
        #endregion
    }

    #endregion

    public class OrderDetailInfo
    {
        [Description("Ngày")]
        public string TradeDate { get; set; }
        [Description("Khối lượng")]
        public decimal Volume { get; set; }
        [Description("Lãi suất")]
        public decimal Rate { get; set; }

        [Description("Loại GD")]
        public string SideName {
            get
            {
                if (Side == CommonConst.SIDE_BUY)
                {
                    return ResourceFile.OrderModule.Side_B;
                }

                if (Side == CommonConst.SIDE_SELL)
                {
                    if (Status == -11)
                        return ResourceFile.OrderModule.Side_R;

                    return ResourceFile.OrderModule.ResourceManager.GetString("IsSell_" + IsSell);
                }

                return "";
            }
        }

        #region Property Ignore
        [JsonIgnore]
        public string Side { get; set; }
        [JsonIgnore]
        public int Status { get; set; }
        [JsonIgnore]
        public int IsSell { get; set; }
        #endregion
    }

    public class CouponDetailInfo
    {
        [Description("Ngày trả")]
        public string CouponDate { get; set; }
        [Description("Khối lượng (TP)")]
        public decimal Volume { get; set; }
        [Description("Giá trị")]
        public decimal Amount { get; set; }

        private string _note;
        [Description("Nội dung")]
        public string Note {
            get
            {
                if (CouponType == 1)
                {
                    return ResourceFile.OrderModule.Coupon_Note_01;
                }

                if (CouponType == 4)
                {
                    return ResourceFile.OrderModule.Coupon_Note_04;
                }

                return _note;
            }
            set { _note = value; }
            
        }

        #region Property Ignore
        [JsonIgnore]
        public int CouponType { get; set; }
        #endregion
    }

    public class ProcessingContract
    {
        public string ContractNo { get; set; }
        public int Status { get; set; }
    }

    public class BondPolicyInfo
    {
        
    }
    
}
