﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Models.Order
{
    #region Danh sach lenh trong ngay
    public class OrderBookRequest : BaseRequest { }

    public class OrderBookResult
    {
        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }
        [Description("Tên sản phẩm")]
        public string ProductTypeName
        {
            get { return ResourceFile.OrderModule.ResourceManager.GetString("ProductType_" + ProductType); }
        }
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

        [Description("Số hợp đồng đối ưng")]
        public string ContraContractNo { get; set; }

        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Khối lượng")]
        public int Volume { get; set; }

        private string _side;
        [Description("Loại giao dịch")]
        public string Side {
            get
            {
                if (Status == -11 || Status == -33 || Status == -22 || Status == -23)
                    return CommonConst.SIDE_ROLL;

                return _side;
            }
            set { _side = value; } }

        [Description("Tên Loại giao dịch")]
        public string SideName
        {
            get
            {
                if (Status == -11 || Status == -33 || Status == -22 || Status == -23)
                    return ResourceFile.OrderModule.Side_R;

                return ResourceFile.OrderModule.ResourceManager.GetString("Side_" + Side);
            }
        }

        [Description("Kênh đặt lệnh")]
        public int ChannelType { get; set; }
        [Description("Tên kênh đặt lệnh")]
        public string ChannelTypeName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("ChannelType_" + ChannelType);
            }
        }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                if (Status >= 0)
                    return ResourceFile.OrderModule.ResourceManager.GetString("Status_" + Side + Status);

                return ResourceFile.OrderModule.ResourceManager.GetString("Status_" + Side + "_" + (-Status));
            }
        }

        [Description("Được phép hủy?")]
        public bool IsCancel { get; set; }
        [Description("B-Ngày giao dịch, S-Ngày thực hiện")]
        public string TradeDate { get; set; }
    }
    #endregion

    #region "Chi tiet lenh"

    public class OrderDetailRequest : BaseRequest
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }
    }

    public class OrderDetailResult
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

        [Description("Số hợp đồng đối ưng")]
        public string ContraContractNo { get; set; }

        [Description("Số hợp đồng gốc")]
        public string OrginalContractNo { get; set; }

        [Description("Trái phiếu")]
        public string BondCode { get; set; }

        [Description("Khối lượng")]
        public int Volume { get; set; }

        [Description("B-Mua, S-Bán, R-Roll")]
        public string Side { get; set; }

        [Description("B-Ngày giao dịch, S-Ngày thực hiện")]
        public string TradeDate { get; set; }

        [Description("Ngày tính lãi")]
        public string CouponDate { get; set; }

        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }

        [Description("Loại Sản phẩm")]
        public int ProductType { get; set; }

        [Description("Tên sản phẩm")]
        public string ProductTypeName
        {
            get { return ResourceFile.OrderModule.ResourceManager.GetString("ProductType_" + ProductType); }
        }

        [Description("Kỳ hạn")]
        [JsonIgnore]
        public decimal Term { get; set; }

        [Description("Kỳ hạn/Kỳ trả lãi")]
        public string TermName
        {
            get
            {
                if (Side == CommonConst.SIDE_SELL)
                    return string.Format(ResourceFile.OrderModule.TermName, string.Format("{0:0.##}", (Term)));

                return string.Format(ResourceFile.OrderModule.TermName_01, string.Format("{0:0.##}", (Term)));
            }
        }

        [Description("Tên Loại giao dịch")]
        public string SideName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("Side_" + Side);
            }
        }

        [Description("B-Tổng giá trị mua, S-Tổng giá trị bán, R-Tổng giá trị gia hạn")]
        public decimal Amount { get; set; }

        [Description("S-Tổng tiền nhận")]
        public decimal RealAmount { get; set; }

        [Description("S-Lãi đầu tư")]
        public decimal Interest { get; set; }

        [Description("Thuế TNCN( nếu có)")]
        public decimal Tax { get; set; }

        [JsonIgnore]
        public decimal Rate { get; set; }

        [Description("Mức sinh lời (Outright không có trường này)")]
        public string RateInfo {
            get
            {
                if (ProductType != 3)
                {
                    return Rate.ToString("P2");
                }

                return string.Empty;
            }
        }
        [Description("Kênh đặt lệnh")]
        public int ChannelType { get; set; }
        [Description("Tên kênh đặt lệnh")]
        public string ChannelTypeName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("ChannelType_" + ChannelType);
            }
        }
        
        public string IssuerCode { get; set; }

        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên chủ TK ngân hàng")]
        public string BankAccountName { get; set; }
        [Description("Tên ngân hàng")]
        public string BankName { get; set; }
        [Description("Tên chi nhánh")]
        public string SubBranchName { get; set; }

        [Description("Kỳ trả lãi Coupon")]
        public string CouponTerm {
            get
            {
                if ((new List<int> { 3, 5, 6, 80, 81 }).Contains(BondType))
                {
                    return ResourceFile.OrderModule.Term_Name_02;
                }

                if ((new List<int> { 7, 8, 9, 10, 11 }).Contains(BondType))
                {
                    return ResourceFile.OrderModule.Term_Name_03;
                }

                if (Frequency > 0 && BondType == 4)
                {
                    var repeatTime = 12 / Frequency;
                    return string.Format(ResourceFile.OrderModule.Term_Name_01, repeatTime + "");
                }

                return "";
            }
        }

        [Description("Đơn giá mua")]
        public decimal BuyPrice { get; set; }

        [JsonIgnore]
        public int Frequency { get; set; }
        [JsonIgnore]
        public int BondType { get; set; }
    }

    public class OrderSellDataInfo
    {
        public string TradeDate { get; set; }
        public int Status { get; set; }
    }

    public class BondPriceCalc
    {
        public string Description { get; set; }

        public DateTime? FromDate { get; set; }
        
        public DateTime? ToDate { get; set; }

        public decimal Income { get; set; }

        public decimal? Interest { get; set; }

        public decimal? TotalIncome { get; set; }

        public decimal? ReinvestRate { get; set; }

        public int? UnitType { get; set; }

        public string Unit { get; set; }

        public string RowCode { get; set; }
    }
    #endregion

    #region Huy lenh
    public class OrderCancelRequest : BaseRequest
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }
        //[Description("Mã Pin")]
        //public string PinCode { get; set; }
    }

    #endregion

    #region Xác nhận đặt lệnh
    public class OrderConfirmDetailRequest : BaseRequest
    {
        [Description("ID yêu cầu đặt lệnh")]
        public string RequestOrderID { get; set; }
    }

    public class OrderConfirmDetailResult
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

        [Description("B-Mua, S-Bán, R-Roll, C-Hủy")]
        public string Side { get; set; }

        [Description("Tên Loại giao dịch")]
        public string SideName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("Side_" + Side);
            }
        }

        [Description("Tổ chức phát hành")]
        public string Issuer { get; set; }

        [Description("Nhóm Trái phiếu")]
        public string IssuerCode { get; set; }

        [Description("Mã Trái phiếu")]
        public string BondCode { get; set; }

        [Description("Loại Sản phẩm")]
        public int ProductType { get; set; }

        [Description("Tên sản phẩm")]
        public string ProductTypeName
        {
            get { return ResourceFile.OrderModule.ResourceManager.GetString("ProductType_" + ProductType); }
        }

        [Description("Ngày phát hành")]
        public string IssuerDate { get; set; }

        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }

        [Description("B-Ngày giao dịch, S-Ngày thực hiện")]
        public string TradeDate { get; set; }

        [Description("Ngày tính lãi")]
        public string CouponDate { get; set; }

        [Description("Trạng thái")]
        public int Status { get; set; }

        [Description("Tên trạng thái")]
        public string StatusName 
        { 
            get 
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("Status_CF" + Status);
            } 
        }

        [Description("Khối lượng")]
        public int Volume { get; set; }

        [Description("Ngày mua")]
        public string BuyDate { get; set; }

        [Description("Đơn giá mua")]
        public decimal BuyPrice { get; set; }

        [Description("Tổng giá trị mua")]
        public decimal BuyAmount { get; set; }

        [Description("Mức sinh lời")]
        public decimal Rate { get; set; }

        [Description("Ngày bán")]
        public string SellDate { get; set; }

        [Description("Đơn giá bán")]
        public decimal SellPrice { get; set; }

        [Description("Tổng giá bán trước thuế")]
        public decimal SellAmount { get; set; }

        [Description("Tổng giá bán thực nhận")]
        public decimal RealSellAmount { get; set; }

        [Description("Thuế TNCN (nếu có)")]
        public decimal Tax { get; set; }

        [Description("Lãi đầu tư")]
        public decimal Interest { get; set; }

        [Description("Phí chuyển nhượng")]
        public decimal Fee { get; set; }

        [Description("Kỳ hạn")]
        [JsonIgnore]
        public decimal Term { get; set; }

        [Description("Kỳ hạn/Kỳ trả lãi")]
        public string TermName
        {
            get
            {
                if (Side == CommonConst.SIDE_SELL)
                    return string.Format(ResourceFile.OrderModule.TermName, string.Format("{0:0.##}", (Term)));

                return string.Format(ResourceFile.OrderModule.TermName_01, string.Format("{0:0.##}", (Term)));
            }
        }

        [Description("Bảng dòng tiền")]
        [JsonIgnore]
        public string PriceInfo { get; set; }

        [Description("Lịch thanh toán")]
        public List<PriceDetailInfo> PriceInfoList { get; set; }

        [Description("Bảng tài khoản nhận tiền")]
        [JsonIgnore]
        public string PaymentInfo { get; set; }

        [Description("Kênh đặt lệnh")]
        public int ChannelType { get; set; }

        [Description("Tên kênh đặt lệnh")]
        public string ChannelTypeName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("ChannelType_" + ChannelType);
            }
        }

        [Description("CustBankID1")]
        public int CustBankID1 { get; set; }

        [Description("Số TK ngân hàng 1")]
        public string BankAccountNo1 { get; set; }

        [Description("Tên chủ TK ngân hàng 1")]
        public string BankAccountName1 { get; set; }

        [Description("Tên ngân hàng 1")]
        public string BankName1 { get; set; }

        [Description("Tên chi nhánh 1")]
        public string SubBranchName1 { get; set; }

        [Description("Số tiền 1")]
        public decimal Amount1 { get; set; }

        [Description("CustBankID2")]
        public int CustBankID2 { get; set; }

        [Description("Số TK ngân hàng 2")]
        public string BankAccountNo2 { get; set; }

        [Description("Tên chủ TK ngân hàng 2")]
        public string BankAccountName2 { get; set; }

        [Description("Tên ngân hàng 2")]
        public string BankName2 { get; set; }

        [Description("Tên chi nhánh 2")]
        public string SubBranchName2 { get; set; }

        [Description("Số tiền 2")]
        public decimal Amount2 { get; set; }

        [Description("Kỳ trả lãi Coupon")]
        public int CouponTerm { get; set; }
    }

    public class OrderRefuseRequest : BaseRequest
    {
        [Description("Số hợp đồng")]
        public long RequestOrderID { get; set; }

        [Description("Lý do")]
        public string Note { get; set; }
    }

    public class PriceDetailInfo
    {
        [Description("Nội dung")]
        public string Content { get; set; }

        [Description("Ngày nhận")]
        public string CouponDate { get; set; }

        [Description("Giá trị")]
        public string Value { get; set; }
    }
    #endregion
}
