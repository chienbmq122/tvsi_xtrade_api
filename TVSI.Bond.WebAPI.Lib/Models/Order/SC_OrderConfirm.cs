﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Newtonsoft.Json;

namespace TVSI.Bond.WebAPI.Lib.Models.SC_OrderConfirm
{
    public class SC_FindOrderConfirmRequest: BaseRequest
    {        
        public string SecSymbol { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Side { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }

    public class SC_UpdateConfirmOrderConfirmationRequest : BaseRequest
    {
        public string OrderConfirmationIDs { get; set; }        
    }

    public class SC_OrderConfirmationDetailRequest : BaseRequest
    {
        public DateTime TradeDate { get; set; }

        [JsonIgnore]
        public string TransTime {
            get
            {
                return TradeDate.ToString("yyyyMMdd");
            }
        }
        public long OrderInfoID { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }

    public class SC_OrderConfirmationDetailResult
    {
        public int TotalRows { get; set; }
        public List<SC_OrderConfirmationDetailData> Data { get; set; }
    }

    public class SC_OrderConfirmationDetailData
    {
        public int TotalRows { get; set; }
        public string Account { get; set; }
        [Description("Thời gian giao dịch")]
        public DateTime TransTime { get; set; }
        public int FISOrderID { get; set; }
        public string SecSymbol { get; set; }

        [Description("Loại giao dịch B: Mua; S: Bán")]
        public string Side { get; set; }
        [Description("Khối lượng đặt")]
        public long Volume { get; set; }
        [Description("Hiển thị thông tin giá đặc biệt: ATO, ATC, ...")]
        public string PriceText
        {
            get
            {
                string priceText;
                switch (this.ConPrice)
                {
                    case "A":
                        priceText = "ATO";
                        break;
                    case "C":
                        priceText = "ATC";
                        break;
                    case "M":
                        priceText = "MP";
                        break;
                    case "O":
                        priceText = "MOK";
                        break;
                    case "K":
                        priceText = "MAK";
                        break;
                    case "S":
                        priceText = "SBO";
                        break;
                    case "B":
                        priceText = "OBO";
                        break;
                    case "P":
                        priceText = "PLO";
                        break;
                    case "<":
                        priceText = "SO>";
                        break;
                    case ">":
                        priceText = "SO<";
                        break;
                    case "I":
                        priceText = "IO";
                        break;
                    default:
                        priceText = TVSI.Utility.Functions.FormatDouble(this.Price, 2);
                        break;
                }
                return priceText;
            }
        }

        [Description("Giá")]
        public decimal Price { get; set; }
        public long ExecutedVol { get; set; }
        public decimal ExecutedPrice { get; set; }
        public long CancelledVolume { get; set; }
        public int OrderStatus { get; set; }
        public int CancelStatus { get; set; }
        public string ConPrice { get; set; }
    }

    public class SC_UpdateALLConfirmOrderConfirmationRequest : BaseRequest
    {
        public string SecSymbol { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Side { get; set; }
    }

    public class SC_OrderConfirmResult
    {
        public int TotalRows { get; set; }
        public int TotalPages { get; set; }
        public List<SC_FindOrderConfirmResult> Data { get; set; }
    }

    public class SC_FindOrderConfirmResult
    {
        public int TotalRows { get; set; }
        public int TotaLPages { get; set; }

        [Description("Số tài khoản")]
        public string AccountNo { get; set; }

        [Description("Thời gian giao dịch")]
        public DateTime TransTime { get; set; }
        public int OrderConfirmationID { get; set; }
        public long FisOrderID { get; set; }
        public long OrderInfoId { get; set; } 
        [Description ("Loại giao dịch B: Mua; S: Bán")]
        public string Side { get; set; }
        [Description("Mã chứng khoán")]
        public string SecSymbol { get; set; }

        [Description("Khối lượng đặt")]
        public long Volume { get; set; }

        [Description("Hiển thị thông tin giá đặc biệt: ATO, ATC, ...")]
        public string PriceText
        {
            get
            {
                string priceText;
                switch (this.ConPrice)
                {
                    case "A":
                        priceText = "ATO";
                        break;
                    case "C":
                        priceText = "ATC";
                        break;
                    case "M":
                        priceText = "MP";
                        break;
                    case "O":
                        priceText = "MOK";
                        break;
                    case "K":
                        priceText = "MAK";
                        break;
                    case "S":
                        priceText = "SBO";
                        break;
                    case "B":
                        priceText = "OBO";
                        break;
                    case "P":
                        priceText = "PLO";
                        break;
                    case "<":
                        priceText = "SO>";
                        break;
                    case ">":
                        priceText = "SO<";
                        break;
                    case "I":
                        priceText = "IO";
                        break;
                    default:
                        priceText = TVSI.Utility.Functions.FormatDouble(this.Price, 2);
                        break;
                }
                return priceText;
            }
        }

        [Description("Giá")]
        public decimal Price { get; set; }
        public long ExecutedVol { get; set; }
        public decimal ExecutedPrice { get; set; }
        public long CancelledVolume { get; set; }
        public int OrderStatus { get; set; }
        public int CancelStatus { get; set; }

        [Description("Trạng thái xác nhận: 1: Đã xác nhận; 0: Chờ xác nhận; Nếu TP_Use = 1 và Status = 0 thì hiển thị là TVSI đang xử lý")]
        public int Status { get; set; }
        public DateTime? SignDate { get; set; }
        public string ConfirmCode { get; set; }
        public string TP_Code { get; set; }

        [Description("1: Trạng thái tại TVSI; #1:TVSI đang xử lý")]
        public int TP_Status { get; set; }

        [Description("TVSI sử dụng: 1: Đang sử dụng; 0: Chưa sửa dụng (Nếu TP_USE = 1 thì giao diện không được chọn lệnh này để xác nhận)")]
        public int TP_Use { get; set; }
        public string ConPrice { get; set; }

        [Description("Nguồn đặt lệnh: B: Môi giới; #B: Khác")]
        public string OrderSource { get; set; }
    }

    public class SC_UpdateOrderConfirmResult
    {
        public string RetCode { get; set; }
        public string Message { get; set; }
    }
}
