﻿using System.ComponentModel;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Models.Order
{
    #region Order Confirm List
    public class OrderConfirmListRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }
    }

    public class OrderConfirmListResult
    {
        [Description("ID của lệnh")]
        public long RequestOrderID { get; set; }

        [Description("Ngày thực hiện")]
        public string TradeDate { get; set; }

        [Description("Số Hợp đồng")]
        public string ContractNo { get; set; }
        

        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }

        [Description("Tên sản phẩm")]
        public string ProductTypeName
        {
            get { return ResourceFile.OrderModule.ResourceManager.GetString("ProductType_" + ProductType); }
        }

        [Description("Loại giao dịch")]
        public string Side { get; set; }

        [Description("Tên Loại giao dịch")]
        public string SideName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("Side_" + Side);
            }
        }

        [Description("Khối lượng")]
        public int Volume { get; set; }

        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("Status_CF" + Status);
            }
        }
        [Description("Kênh đặt lệnh")]
        public int ChannelType { get; set; }
        [Description("Tên kênh đặt lệnh")]
        public string ChannelTypeName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("ChannelType_" + ChannelType);
            }
        }
        [Description("Mã Trái phiếu")]
        public string BondCode;
    }
    #endregion

    #region Post Confirm Order

    public class OrderConfirmRequest : BaseRequest
    {
        [Description("ID của bảng")]
        public long RequestOrderID { get; set; }
    }
    #endregion
}
