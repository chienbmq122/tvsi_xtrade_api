﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.Order
{
    public class ContractBuySellDetailRequest : BaseRequest
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }
    }

    public class ContractBuySellDetailResult
    {
        [Description("Thông tin lệnh mua")]
        public BuyDataInfo BuyDataInfo { get; set; }
        [Description("Thông tin lệnh bán")]
        public SellDataInfo SellDataInfo { get; set; }

        [Description("Thông tin dòng tiền")]
        public List<CashDataInfo> CashDataInfo { get; set; }
    }

    public class BuyDataInfo
    {
        [Description("Tổ chức phát hành")]
        public string Issuer { get; set; }
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Ngày mua")]
        public string TradeDate { get; set; }
        [Description("Số lượng mua")]
        public int Volume { get; set; }
        [Description("Đon giá mua")]
        public decimal Price { get; set; }
        [Description("Tổng giá trị mua")]
        public decimal Amount { get; set; }
    }

    public class SellDataInfo
    {
        [Description("Ngày bán")]
        public DateTime SellDate { get; set; }
        [Description("Giá bán")]
        public decimal SellPrice { get; set; }
        [Description("Giá bán trước thuế")]
        public decimal SellPriceAmount { get; set; }
        [Description("Thuế thu nhập cá nhân")]
        public decimal PIT { get; set; }
        [Description("Tiền bán thực nhận")]
        public decimal NetAmount { get; set; }

        [Description("TK nhận tiền")]
        public string BankAccountInfo { get; set; }
    }

    public class CashDataInfo
    {
        [Description("Mô tả")]
        public string Description { get; set; }
        [Description("Ngày nhận")]
        public string ReceiveDate { get; set; }
        [Description("Tiền nhận")]
        public string Amount { get; set; }
    }
}
