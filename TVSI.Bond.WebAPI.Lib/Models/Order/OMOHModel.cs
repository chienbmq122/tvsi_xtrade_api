﻿using System.ComponentModel;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Models.Order
{
    public class OrderHistRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }
    }

    public class OrderHistResult
    {
        [Description("Ngày giao dịch")]
        public string TradeDate { get; set; }

        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }

        [Description("Tên sản phẩm")]
        public string ProductTypeName
        {
            get { return ResourceFile.OrderModule.ResourceManager.GetString("ProductType_" + ProductType); }
        }
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

        [Description("Số hợp đồng đối ưng")]
        public string ContraContractNo { get; set; }

        [Description("Mã Trái phiếu")]
        public string BondCode { get; set; }
        private string _side;
        [Description("Loại giao dịch")]
        public string Side
        {
            get
            {
                if (Status == -11 || Status == -33 || Status == -22 || Status == -23)
                    return CommonConst.SIDE_ROLL;

                return _side;
            }
            set { _side = value; }
        }
        [Description("Tên Loại giao dịch")]
        public string SideName
        {
            get
            {
                if (Status == -11 || Status == -33 || Status == -22 || Status == -22)
                    return ResourceFile.OrderModule.Side_R;

                return ResourceFile.OrderModule.ResourceManager.GetString("Side_" + Side);
            }
        }

        [JsonIgnore]
        public int IsSell { get; set; }
        [Description("Khối lượng")]
        public int Volume { get; set; }

        [Description("Kênh đặt lệnh")]
        public int ChannelType { get; set; }
        [Description("Tên kênh đặt lệnh")]
        public string ChannelTypeName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("ChannelType_" + ChannelType);
            }
        }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName {
            get
            {
                if (Status >= 0)
                    return ResourceFile.OrderModule.ResourceManager.GetString("Status_" + Side + Status);
                
                return ResourceFile.OrderModule.ResourceManager.GetString("Status_" + Side + "_" + (-Status));
            }
        }

        [Description("Được phép hủy?")]
        public bool IsCancel { get; set; }
    }

    
}
