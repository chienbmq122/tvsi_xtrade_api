﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Bond.WebAPI.Lib.Models.Notications
{
    public class NoticationModel
    {
        [Description("Thông báo id")]
        public int notification_id { get; set; }
        [Description("loại thông báo id")]
        public int type_notification_id { get; set; }
        [Description("mẫu thông báo id")]
        public int template_notification_id { get; set; }
        [Description("Tiêu đề")]
        public string title_notification { get; set; }
        [Description("nội dung")]
        public string content_notification { get; set; }
        [Description("nội dung tiếng anh")]
        public string content_en_notification { get; set; }
        [Description("Custcode")]
        public string custcode { get; set; }
        [Description("độ ưu tiên")]
        public int priority { get; set; }
        [Description("trạng thái")]
        public int status { get; set; }
        [Description("nguồn thông báo")]
        public string src_notification { get; set; }

        public DateTime dateSend { get; set; }

        [Description("thời gian gửi")]
        public string time_send
        {
            get
            {
                return dateSend.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [Description("ảnh")]
        public string img { get; set; }
        [Description("đã đọc")]
        public bool is_read { get; set; }
        [Description("is active")]
        public bool is_active { get; set; }
        [Description("người tạo")]
        public string create_by { get; set; }
        [Description("ngày tạo")]
        public DateTime create_at { get; set; }
        [Description("người cập nhật")]
        public string update_by { get; set; }
        [Description("ngày cập nhật")]
        public DateTime update_at { get; set; }
        [Description("link màn hình")]
        public string src_screen { get; set; }
    }
    public class NoticationRequestModel : BaseRequest
    {
        [Description("Trang")]
        public int PageIndex { get; set; }
        [Description("Page size")]
        public int PageSize { get; set; }
    }

    public class NoticationResponseModel
    {
        [Description("Trang")]
        public int PageSize { get; set; }
        [Description("Tong so luong")]
        public int TotalItem { get; set; }
        [Description("Tong so luong chưa đọc")]
        public int UnRead { get;set;}
        [Description("data")]
        public List<NoticationModel> Items { get; set; }
    }

    public class NoticationCount
    {
        [Description("Tong so luong")]
        public int TotalItem { get; set; }
        [Description("Tong so luong chưa đọc")]
        public int UnRead { get;set;}
    }

    public class CreateNoticationRequest : BaseRequest
    {
        [Description("loại thông báo id")]
        public int type_notification_id { get; set; }
        [Description("Tiêu đề")]
        public string title_notification { get; set; }
        [Description("nội dung")]
        public string content_notification { get; set; }
        [Description("nội dung tiếng anh")]
        public string content_en_notification { get; set; }
        [Description("Custcode")]
        public string custcode { get; set; }
        [Description("độ ưu tiên")]
        public int priority { get; set; }
        [Description("nguồn thông báo: cảnh báo thị trường: WarningMarket || cảnh báo ck: WarningStock")]
        public string src_notification { get; set; }
        [Description("thời gian gửi format dd/MM/yyyy HH:mm:ss")]
        public string time_send { get; set; }
        [Description("link màn hình")]
        public string src_screen { get; set; }
    }
}
