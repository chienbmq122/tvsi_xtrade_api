﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Bond.WebAPI.Lib.Models.Notications
{
    public class NoticationContractModel : BaseRequest
    {
        [Description("id")]
        public int register_notification_id { get; set; }
        [Description("loại thông báo id")]
        public List<int> list_type_notification_id { get; set; }
    }

    public class NoticationContractResponseModel
    {
        [Description("id")]
        public int register_notification_id { get; set; }
        [Description("cust code")]
        public string custCode { get; set; }
        [Description("loại thông báo id")]
        public string type_notification_id { get; set; }
        [Description("Danh sách đăng ký thông báo")]
        public List<int> list_type_notification_id { get; set; }
        [Description("người tạo")]
        public string create_by { get; set; }
        [Description("ngày tạo")]
        public DateTime create_at { get; set; }
        [Description("người cập nhật")]
        public string update_by { get; set; }
        [Description("ngày cập nhật")]
        public DateTime update_at { get; set; }
    }

    public class ReadNoticationContact : BaseRequest
    {
        [Description("danh sách tin đã đọc")]
        public List<int> notification_id { get; set; }
    }

    public class UpdateStatusNoticationContact : BaseRequest
    {
        [Description("danh sách tin đã đọc")]
        public int notification_id { get; set; }

        [Description("trạng thái 1: chờ xử lý , 2: hoàn thành, 3: lỗi")]
        public int status { get; set; }
    }
    public class ReadAllNoticationRequest : BaseRequest
    {
        [Description("cust code")]
        public string custCode { get; set; }
        [Description("kênh gửi tin")]
        public int channel_send { get; set; }
        [Description("Type Notication: 1 - notication , 2 - warning")]
        public int type { get; set; }
    }

    public class GetBannerTvsiRequest : BaseEkycRequest
    {
        
    }
    
    
}
