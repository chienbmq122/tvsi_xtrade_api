﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Bond.WebAPI.Lib.Models.Notications
{
    public class NoticationTypeModel
    {
        [Description("id")]
        public int type_notification_id { get; set; }

        [Description("Tên loại thông báo")]
        public string name_notification { get; set; }

        [Description("Kiểu thông báo")]
        public string method_notification { get; set; }

        [Description("Id danh mục thông báo")]
        public int type_notification_parent_id { get; set; }
    }
}
