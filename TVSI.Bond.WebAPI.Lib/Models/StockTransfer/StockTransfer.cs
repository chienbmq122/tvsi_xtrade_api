﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Bond.WebAPI.Lib.Models.StockTransfer
{
    public class AvailStockTransferListRequest : BaseRequest
    {

    }

    public class AvailStockTransferInfoRequest : BaseRequest
    {
        public string AccountNo { get; set; }
        public string ShareCode { get; set; }
        public string Purpose { get; set; }
    }

    public class AvailStockTransferInfoResult
    {
        public string AccountNo { get; set; }
        public string ShareCode { get; set; }

        [Description("Loại CK: 02|Cổ phiếu thường; 30|CP thưởng; 31|Cổ phiểu thưởng hạn chế GD; 46|Cầm cố; 49|Hạn chế giao dịch")]
        public string Purpose { get; set; }
        public decimal Unit { get; set; }
        public decimal AvgPrice { get; set; }
    }

    public class AddStockTransferRequest: BaseRequest
    {
        public string ShareCode { get; set; }

        [Description("Loại CK: 02|Cổ phiếu thường; 30|CP thưởng; 31|Cổ phiểu thưởng hạn chế GD; 46|Cầm cố; 49|Hạn chế giao dịch")]
        public string FromPurpose { get; set; }
        public int Unit { get; set; }
        public string ToAccount { get; set; }

        [Description("Loại CK: 02|Cổ phiếu thường; 30|CP thưởng; 31|Cổ phiểu thưởng hạn chế GD; 46|Cầm cố; 49|Hạn chế giao dịch")]
        public string ToPurpose { get; set; }
        [Description("Mô tả tiếng việt không dấu")]
        public string Remark { get; set; }
        public string Note { get; set; }
    }

    public class AddStockTransferResult
    {
        public string RetCode { get; set; }
        public int SQE { get; set; }
        public string TradeDate { get; set; }
        public string AccountNo { get; set; }
        public string ToAccount { get; set; }
        public string RefNo { get; set; }
        public string Message { get; set; }
    }

    public class StockTransferHistRequest : BaseRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class StockTransferInfoResult
    {
        public DateTime TradeDate { get; set; }
        public string RefNo { get; set; }
        public int SEQ { get; set; }
        public string AccountNo { get; set; }
        public string ShareCode { get; set; }

        [Description("Loại CK: 02|Cổ phiếu thường; 30|CP thưởng; 31|Cổ phiểu thưởng hạn chế GD; 46|Cầm cố; 49|Hạn chế giao dịch")]
        public string Purpose { get; set; }
        public int Unit { get; set; }
        public string ToAccount { get; set; }
        public string Remark { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Description("Trạng thái: 02: Đang xử lý; 00: Chờ xử lý; 01: Từ chối; 04: Lỗi xử lý; 03: Thành công")]
        public string Status { get; set; }
    }

    public class StockBalanceDB2
    {
        public string ACCOUNTNO { get; set; }
        public string SECSYMBOL { get; set; }
        public int SECTYPE { get; set; }
        public decimal STARTVOLUME { get; set; }
        public decimal AVAIVOLUME { get; set; }
        public decimal AVGPRICE { get; set; }
        public decimal SELLORDVOLUME { get; set; }
    }

    public class TempStockTransfer
    {
        public string so_tai_khoan { get; set; }
        public string ma_chung_khoan { get; set; }
        public decimal khoi_luong { get; set; }

        [Description("Loại CK: 02|Cổ phiếu thường; 30|CP thưởng; 31|Cổ phiểu thưởng hạn chế GD; 46|Cầm cố; 49|Hạn chế giao dịch")]
        public string from_purpose { get; set; }
    }

    public class StockBalanceMarginDB2
    {
        public string ACCOUNTNO { get; set; }
        public int STOCK_TYPE { get; set; }
        public string STOCK_SYM { get; set; }
        public string BRANCHID { get; set; }
        public decimal TODAY_MARGIN { get; set; }
        public decimal ACTUAL_VOL { get; set; }
        public decimal LASTSALE { get; set; }
        public decimal AVGPRICE { get; set; }
        public decimal EE { get; set; }
    }

    public class StockTransferInfoIPG
    {
        public int so_sequence { get; set; }
        public string ngay_giao_dich { get; set; }
        public string so_tham_chieu { get; set; }
    }
}
