﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.Bond.WebAPI.Lib.Models.OddLot
{
    public class BaseOddLotRequest : BaseRequest
    {
    }

    public class OddLotStockRepositorySelectRequest : BaseOddLotRequest
    {
    }
    
    public class OddLotStockRepositorySearchRequest : BaseOddLotRequest
    {
        [Description("Mã chứng khoán")] public string StockCode { get; set; }
    }

    public class OddLotOrderInsertRequest : BaseOddLotRequest
    {
        [Description("Danh mục lệnh bán lô lẻ")]
        public List<OddLotOrderInsertModel> lstOrder { get; set; }
    }

    public class OddLotOrderSearchRequest : BaseOddLotRequest
    {
        [Description("Mã chứng khoán")] public string stock_code { get; set; }

        [Description("Từ ngày")] [Required] public string fromDate { get; set; }
        [Description("Đến ngày")] [Required] public string toDate { get; set; }

        [Description("Trạng thái lệnh bán: -1 = ALL | 0 = Chờ xử lý | 10 = Đang xử lý | 20 = Đang chờ VSD | 30 = Đã xử lý | 40 = Từ chối | 50 = Hủy bỏ")]
        [Required]
        public int order_status { get; set; }

        [Description("Kênh đặt lệnh: -1 = ALL | 0 = EMS | 1 = GATEWAY (HOME, XTRADE)")]
        [Required]
        public int order_channel { get; set; }
    }
    
    public class OddLotOrderSelectCurrentRequest : BaseOddLotRequest
    {
        [Description("Mã chứng khoán")] public string stock_code { get; set; }

        [Description("Trạng thái lệnh bán: -1 = ALL | 0 = Chờ xử lý | 10 = Đang xử lý | 20 = Đang chờ VSD | 30 = Đã xử lý | 40 = Từ chối | 50 = Hủy bỏ")]
        [Required]
        public int order_status { get; set; }

        [Description("Kênh đặt lệnh: -1 = ALL | 0 = EMS | 1 = GATEWAY (HOME, XTRADE)")]
        [Required]
        public int order_channel { get; set; }
    }

    public class OddLotOrderCancelRequest : BaseOddLotRequest
    {
        [Description("Mã định danh lệnh bán")]
        [Required]
        public long odd_lot_order_id { get; set; }
    }

    public class OddLotStockRepository
    {
        [Description("Mã sàn giao dịch")] public string stock_market { get; set; }

        [Description("Mã chứng khoán")] public string stock_code { get; set; }

        [Description("Loại chứng khoán: 02 = Cổ phiếu thường | 30 = Cổ phiếu thưởng")] public string stock_type { get; set; }

        [Description("Số lượng cổ phiếu lẻ")] public int stock_quantity { get; set; }

        [Description("Giá sàn")] public decimal stock_price { get; set; }
        
        [Description("Tỷ lệ phí: active_status = 0 => không hiển thị Tỷ lệ phí")] public decimal fee_rate { get; set; }
        
        [Description("Hỗ trợ bán lô lẻ")] public int active_status { get; set; }
    }

    public class OddLotOrderInsertModel
    {
        [Description("Mã chứng khoán")]
        [Required]
        public string stock_code { get; set; }

        [Description("Loại chứng khoán: 02 = Cổ phiếu thường | 30 = Cổ phiếu thưởng")]
        [Required]
        public string stock_type { get; set; }

        [Description("Số lượng đặt bán")]
        [Required]
        public int stock_quantity { get; set; }

        [Description("Giá bán")] [Required] public decimal stock_price { get; set; }
    }

    public class OddLotOrder
    {
        [Description("Mã định danh lệnh bán")] public long odd_lot_order_id { get; set; }
        [Description("Tiểu khoản")] public string customer_account { get; set; }
        [Description("Họ và tên khách hàng")] public string customer_name { get; set; }
        [Description("Mã chứng khoán")] public string stock_code { get; set; }
        [Description("Loại chứng khoán: 02 = Cổ phiếu thường | 30 = Cổ phiếu thưởng")] public string stock_type { get; set; }
        [Description("Số lượng đặt bán")] public int stock_quantity { get; set; }
        [Description("Giá bán")] public decimal stock_price { get; set; }

        [Description("Thuế giao dịch chứng khoán")]
        public decimal transaction_tax { get; set; }

        [Description("Thuế XR")] public decimal xr_tax { get; set; }
        [Description("Phí TVSI thực thu")] public decimal transaction_fee { get; set; }
        [Description("Lý do từ chối")] public string rejection_reason { get; set; }
        [Description("Ngày bán")] public DateTime sell_date { get; set; }

        [Description("Ngày thực hiện lệnh GC")]
        public DateTime? vsd_date { get; set; }

        [Description("Trạng thái lệnh: -1 = ALL | 0 = Chờ xử lý | 10 = Đang xử lý | 20 = Đang chờ VSD | 30 = Đã xử lý | 40 = Từ chối | 50 = Hủy bỏ")] public int order_status { get; set; }
        [Description("Kênh đặt lệnh: -1 = ALL | 0 = EMS | 1 = GATEWAY (HOME, XTRADE)")] public int order_channel { get; set; }
        [Description("Mã định danh lệnh GC")] public long vsd_stock_api_id { get; set; }
        [Description("Người tạo")] public string created_by { get; set; }
        [Description("Ngày tạo")] public DateTime? created_date { get; set; }
        [Description("Người sửa")] public string updated_by { get; set; }
        [Description("Ngày sửa")] public DateTime? updated_date { get; set; }
    }

    public class OddLotStockRepositoryEmsModel
    {
        public long odd_lot_stock_repository_id { get; set; }
        public string stock_code { get; set; }
        public DateTime valid_from { get; set; }
        public DateTime valid_to { get; set; }
        public decimal fee_rate { get; set; }
        public int status { get; set; }
        public string created_by { get; set; }
        public DateTime? created_date { get; set; }
        public string updated_by { get; set; }
        public DateTime? updated_date { get; set; }
    }
    
    public class OddLotStockRepositoryEmsSearchModel
    {
        public string stock_code { get; set; }
        public decimal fee_rate { get; set; }
    }
}