﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using TVSI.Bond.WebAPI.Lib.Utility;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Models.Ekyc;

namespace TVSI.Bond.WebAPI.Lib.Models
{
    #region "API Success return" 
    public class SuccessModel
    {
        public SuccessModel()
        {
            RetCode = RetCodeConst.SUCCESS_CODE;
        }

        [Description("Mã kết quả thực hiện")]
        public string RetCode { get; set; }
    }

    public class SuccessModel<T>
    {
        public SuccessModel()
        {
            RetCode = RetCodeConst.SUCCESS_CODE;
        }

        [Description("Mã kết quả thực hiện")]
        public string RetCode { get; set; }

        [Description("Dữ liệu trả về")]
        public T RetData { get; set; }
    }

    public class SuccessListModel<T>
    {
        public SuccessListModel()
        {
            RetCode = RetCodeConst.SUCCESS_CODE;
        }

        [Description("Mã kết quả thực hiện")]
        public string RetCode { get; set; }

        [Description("Dữ liệu trả về")]
        public IEnumerable<T> RetData { get; set; } 
    }
    #endregion

    #region "API Error return"

    public class ErrorModel
    {

        public ErrorModel()
        {
        }

        public ErrorModel(string retCode)
        {
            RetCode = retCode;
        }

        public ErrorModel(string retCode, string retMsg)
        {
            RetCode = retCode;
            _retMsg = retMsg;
        }

        public ErrorModel(string retCode, string retMsg, string paramName)
        {
            RetCode = retCode;
            _retMsg = retMsg;
            ParamName = paramName;
        }

        public string RetCode { get; set; }
        [JsonIgnore]
        public string ParamName { get; set; }


        private string _retMsg;
        public string RetMsg
        {
            get
            {
                if (!string.IsNullOrEmpty(_retMsg))
                    return _retMsg + "(" + RetCode + ")";

                return GetDefaultMsg(RetCode, ParamName);
                
            }
            set { _retMsg = value; }
        }

        private static string GetDefaultMsg(string retCode, string paramName)
        {
            if (RetCodeConst.ERR900_ISNULLOREMPTY.Equals(retCode))
                return string.Format(ResourceFile.Errors.CommonErrors.E900, paramName) + "(" + retCode + ")";
            if (RetCodeConst.ERR901_DATETIME_INVALID.Equals(retCode))
                return string.Format(ResourceFile.Errors.CommonErrors.E901, paramName) + "(" + retCode + ")";
            if (RetCodeConst.ERR998_NOT_IMPLEMENT.Equals(retCode))
                return string.Format(ResourceFile.Errors.CommonErrors.E998, paramName) + "(" + retCode + ")";
            if (RetCodeConst.ERR999_SYSTEM_ERROR.Equals(retCode))
                return string.Format(ResourceFile.Errors.CommonErrors.E999, paramName) + "(" + retCode + ")";
            /*if (EkycRetCodeConst.EK903_DATA_EMPTYORNULL.Equals(retCode))
                return string.Format(ResourceFile.Errors.CommonErrors.EK903, paramName) + "(" + retCode + ")";*/
            
                return "";
        }
    }

    public class ErrorListModel
    {
        public string RetCode { get; set; }
        public List<string> RetMsgList { get; set; }
    }
    #endregion

    #region "Other"

    public class BaseRequest
    {
        [Description("Tên đăng nhập")]
        [Required]
        public string UserId { get; set; }
        [Description("Số tài khoản")]
        [Required]
        public string AccountNo { get; set; }
    }

    public class BaseEkycRequest
    {
        
    }
    
    public class BaseResult
    {
        
    }
    #endregion

    #region Store Model
    public class CodeTempModel
    {
        public string CodeName { get; set; }
        public string DisplayName { get; set; }
        public string CodeContent { get; set; }
        public string CodeContenEn { get; set; }
        public string GroupCode { get; set; }
    }
    #endregion
    
}
