﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Utility;


namespace TVSI.Bond.WebAPI.Lib.Models.Mm
{
    public class MmContractListRequest : BaseRequest
    {
        
    }

    public class MmContractListResult
    {
        [Description("Ngày giao dịch")]
        public string TradeDate { get; set; }
        [Description("Số Hợp đồng")]
        public string ContractNo { get; set; }
        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }
        [Description("Khối lượng")]
        public long Volume { get; set; }
        [Description("Giá trị cọc")]
        public decimal Amount { get; set; }
        [Description("Cờ đánh dấu được phép gia hạn hay không")]
        public bool IsRoll { get; set; }
        [Description("Cờ đánh dấu được phép rút cọc hay không")]
        public bool IsSell { get; set; }
    }

    public class MmContractShortDetailRequest : BaseRequest
    {
        [Description("Số Hợp đồng")]
        public string ContractNo { get; set; }
    }

    public class MmContractShortDetailResult
    {
        [Description("Số Hợp đồng")]
        public string ContractNo { get; set; }
        [Description("Ngày đặt cọc")]
        public string TradeDate { get; set; }
        [Description("Khối lượng")]
        public long Volume { get; set; }
        [Description("Giá trị cọc hiện tại")]
        public decimal Amount { get; set; }
 
        [Description("Tỷ lệ lợi tức")]
        public decimal Rate { get; set; }
        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }

    }

    public class MmContractDetailRequest : BaseRequest
    {
        [Description("Số Hợp đồng")]
        public string ContractNo { get; set; }
    }

    public class MmContractDetailResult
    {
        [Description("Số Hợp đồng")]
        public string ContractNo { get; set; }
        [Description("Tên sản phẩm")]
        public string ProductTypeName { get; set; }
        
        [Description("Ngày ký HĐ")]
        public string ConfirmDate { get; set; }
        [Description("Ngày đặt cọc")]
        public string TradeDate { get; set; }
        [Description("Giá trị cọc ban đầu")]
        public string OriginAmount { get; set; }
        [Description("Giá trị cọc hiện tại")]
        public string Amount { get; set; }
        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }
        
        [Description("Tài khoản nhận tiền")]
        public string MoneyReceiver { get; set; }

        [Description("Chi tiết lệnh")]
        public List<MmOrderDetailInfo> MmOrderDetailList { get; set; }

        [Description("Dòng tiền đã nhận")]
        public List<MmCouponDetailInfo> MmReceivedCouponList { get; set; }
        [Description("Lịch thanht toán dự kiến")]
        public List<MmCouponDetailInfo> CouponList { get; set; }

        [JsonIgnore]
        public int? CustBankID { get; set; }
        [JsonIgnore]
        public int PaymentMethod { get; set; }
        [JsonIgnore]
        public int ProductType { get; set; }

        [Description("Cờ đánh dấu được phép gia hạn hay không")]
        public bool IsRoll { get; set; }
        [Description("Cờ đánh dấu được phép rút cọc hay không")]
        public bool IsSell { get; set; }
    }

    public class MmOrderDetailInfo
    {
        [Description("Ngày")]
        public string TradeDate { get; set; }
        [Description("Giá trị cọc")]
        public decimal Amount { get; set; }
        [Description("Lãi suất")]
        public decimal Rate { get; set; }

        [Description("Loại GD")]
        public string SideName
        {
            get
            {
                if (Side == CommonConst.SIDE_BUY)
                {
                    return ResourceFile.OrderModule.SideMM_B;
                }

                if (Side == CommonConst.SIDE_SELL)
                {
                    if (Status == -11)
                        return ResourceFile.OrderModule.Side_R;

                    return ResourceFile.OrderModule.ResourceManager.GetString("IsSell_MM_" + IsSell);
                }

                return "";
            }
        }

        #region Property Ignore
        [JsonIgnore]
        public string Side { get; set; }
        [JsonIgnore]
        public int Status { get; set; }
        [JsonIgnore]
        public int IsSell { get; set; }
        #endregion
    }

    public class MmCouponDetailInfo
    {
        [Description("Ngày trả")]
        public string CouponDate { get; set; }
       
        [Description("Giá trị")]
        public decimal Amount { get; set; }

        [JsonIgnore]
        public int IsSell { get; set; }

        private string _note;

        [Description("Ghi chú")]
        public string Note
        {
            get
            {
                if (IsSell > 0 )
                {
                    return ResourceFile.OrderModule.ResourceManager.GetString("IsSell_MM_" + IsSell);
                }

                return _note;
            }
            set { _note = value; }
        }
    }
}
