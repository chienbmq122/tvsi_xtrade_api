﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Models.Bond;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Models.Mm
{
    public class MmOrderBookRequest : BaseRequest { }

    public class MmOrderBookResult
    {
        [Description("Ngày giao dịch")]
        public string TradeDate { get; set; }
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

        [Description("Số hợp đồng gốc (trường hợp Bán hoặc RollBack)")]
        public string ContraContractNo { get; set; }


        [Description("Số tiền cọc")]
        public decimal Amount { get; set; }

        private string _side;
        [Description("Loại giao dịch")]
        public string Side
        {
            get
            {
                if (Status == -11 || Status == -33 || Status == -22 || Status == -23)
                    return CommonConst.SIDE_ROLL;

                return _side;
            }
            set { _side = value; }
        }
     
        [Description("Tên Loại giao dịch")]
        public string SideName
        {
            get
            {
                if (Status == -11 || Status == -33 || Status == -22 || Status == -23)
                    return ResourceFile.OrderModule.Side_R;

                return ResourceFile.OrderModule.ResourceManager.GetString("SideMM_" + Side);
            }
        }
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                if (Status >= 0)
                    return ResourceFile.OrderModule.ResourceManager.GetString("Status_" + Side + Status);

                return ResourceFile.OrderModule.ResourceManager.GetString("Status_" + Side + "_" + (-Status));
            }
        }

        [Description("Được phép hủy?")]
        public bool IsCancel { get; set; }
    }

    public class MmOrderDetailRequest : BaseRequest
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }
    }

    public class MmOrderDetailResult
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

        [Description("Loại Sản phẩm")]
        public int ProductType { get; set; }

        [Description("Tên sản phẩm")]
        public string ProductTypeName
        {
            get
            {
//                return ResourceFile.OrderModule.ResourceManager.GetString("ProductType_" + ProductType);
                return BondCode;
            }
        }

        [JsonIgnore]
        public string BondCode { get; set; }

        [Description("Ngày đặt lệnh")]
        public string CreatedDate { get; set; }

        private string _side;
        [Description("Loại giao dịch")]
        public string Side
        {
            get
            {
                if (Status == -11 || Status == -33 || Status == -22 || Status == -23)
                    return CommonConst.SIDE_ROLL;

                return _side;
            }
            set { _side = value; }
        }

        [Description("Tên Loại giao dịch")]
        public string SideName
        {
            get
            {
                if (Status == -11 || Status == -33 || Status == -22 || Status == -23)
                    return ResourceFile.OrderModule.Side_R;

                return ResourceFile.OrderModule.ResourceManager.GetString("SideMM_" + Side);
            }
        }
        [Description("Kênh đặt lệnh")]
        public int ChannelType { get; set; }
        [Description("Tên kênh đặt lệnh")]
        public string ChannelTypeName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("ChannelType_" + ChannelType);
            }
        }
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                if (Status >= 0)
                    return ResourceFile.OrderModule.ResourceManager.GetString("Status_" + Side + Status);

                return ResourceFile.OrderModule.ResourceManager.GetString("Status_" + Side + "_" + (-Status));
            }
        }
        [Description("Ngày đặt cọc")]
        public string TradeDate { get; set; }
        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }
        [Description("Giá trị")]
        public decimal Amount { get; set; }
        [Description("Tỷ lệ")]
        public decimal Rate { get; set; }
        [Description("TK nhận tiền")]
        public string ReceiverAccountNo { get; set; }
        [Description("Thuế thu nhập cá nhân")]
        public decimal Tax { get; set; }
        [Description("Số tiền thực nhận")]
        public decimal RealAmount { get; set; }
        [Description("Kỳ hạn")]
        public decimal Term { get; set; }
        [Description("Đơn vị kỳ hạn")]
        public string TermName {
            get
            {
                if (Side == "B")
                    return string.Format(ResourceFile.OrderModule.TermName, Math.Round(Term));

                return string.Format(ResourceFile.OrderModule.TermName, Math.Round(Term));
            }
        }

    }

    public class MmOrderCancelRequest : BaseRequest
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

    }

    public class MmOrderHistRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }
    }

    public class MmOrderHistResult
    {
        [Description("Ngày đặt lệnh")]
        public string CreatedDate { get; set; }

        [Description("Ngày đặt cọc")]
        public string TradeDate { get; set; }
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

        [Description("Số hợp đồng gốc (trường hợp Bán hoặc RollBack)")]
        public string ContraContractNo { get; set; }

        private string _side;
        [Description("Loại giao dịch")]
        public string Side
        {
            get
            {
                if (Status == -11 || Status == -33 || Status == -22 || Status == -23)
                    return CommonConst.SIDE_ROLL;

                return _side;
            }
            set { _side = value; }
        }

        [Description("Tên Loại giao dịch")]
        public string SideName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("SideMM_" + Side);
            }
        }

        [Description("Giá trị cọc")]
        public decimal Amount { get; set; }

        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                if (Status >= 0)
                    return ResourceFile.OrderModule.ResourceManager.GetString("Status_" + Side + Status);

                return ResourceFile.OrderModule.ResourceManager.GetString("Status_" + Side + "_" + (-Status));
            }
        }
    }

    public class MmOrderHistDetailRequest : BaseRequest
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }
    }

    public class MmOrderHistDetailResult
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }
        [Description("Ngày thực hiện")]
        public string ConfirmDate { get; set; }
        [Description("Tỷ lệ lợi tức")]
        public decimal Rate { get; set; }
        [Description("Lợi tức sau thuế TNCN")]
        public decimal InterestAmount { get; set; }
        [Description("Thuế TNCN")]
        public decimal Tax { get; set; }
        [Description("Tổng tiền nhận sau thuế TNCN")]
        public decimal RealAmount { get; set; }
        public int ChannelType { get; set; }
    }

    public class MmOrderConfirmListRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }
    }

    public class MMOrderConfirmListResult
    {
        [Description("ID của lệnh")]
        public long RequestOrderID { get; set; }

        [Description("Ngày thực hiện")]
        public string TradeDate { get; set; }

        [Description("Số Hợp đồng")]
        public string ContractNo { get; set; }


        [Description("Loại giao dịch")]
        public string Side { get; set; }

        [Description("Tên Loại giao dịch")]
        public string SideName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("SideMM_" + Side);
            }
        }


        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("Status_CF" + Status);
            }
        }
        [Description("Kênh đặt lệnh")]
        public int ChannelType { get; set; }
        [Description("Tên kênh đặt lệnh")]
        public string ChannelTypeName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("ChannelType_" + ChannelType);
            }
        }
        [Description("Số tiền")]
        public decimal Amount;
        [Description("Mã sản phẩm")]
        public string BondCode;
    }

    public class MmOrderConfirmRequest : BaseRequest
    {
        [Description("Số hợp đồng")]
        public long RequestOrderID { get; set; }
    }

    public class MMOrderPaymentInfoRequet : BaseRequest
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }
    }

    public class MMOrderPaymentInfoResult
    {
        [Description("Phương thức thanh toán")]
        public int PaymentMethod { get; set; }
        [Description("Số tài khoản")]
        public string BenefitAccount { get; set; }
        [Description("Người thụ hưởng")]
        public string BenefitAccountName { get; set; }
        [Description("Mở tại...")]
        public string Branch { get; set; }
        [Description("Số tiền")]
        public decimal Amount { get; set; }

    }

    public class MMOrderConfirmDetailRequest : BaseRequest
    {
        [Description("ID yêu cầu đặt lệnh")]
        public string RequestOrderID { get; set; }
    }

    public class MMOrderConfirmDetailResult
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

        [Description("B-Mua, S-Bán, R-Roll, C-Hủy")]
        public string Side { get; set; }

        [Description("Tên Loại giao dịch")]
        public string SideName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("SideMM_" + Side);
            }
        }

        [Description("Tổ chức phát hành")]
        public string Issuer { get; set; }

        [Description("Nhóm Trái phiếu")]
        public string IssuerCode { get; set; }

        [Description("Mã Trái phiếu")]
        public string BondCode { get; set; }

        [Description("Loại Sản phẩm")]
        public int ProductType { get; set; }

        [Description("Tên sản phẩm")]
        public string ProductTypeName
        {
            get { return ResourceFile.OrderModule.ResourceManager.GetString("ProductType_" + ProductType); }
        }

        [Description("Ngày phát hành")]
        public string IssuerDate { get; set; }

        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }

        [Description("B-Ngày giao dịch, S-Ngày thực hiện")]
        public string TradeDate { get; set; }

        [Description("Ngày tính lãi")]
        public string CouponDate { get; set; }

        [Description("Trạng thái")]
        public int Status { get; set; }

        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("Status_CF" + Status);
            }
        }

        [Description("Khối lượng")]
        public decimal Volume { get; set; }

        [Description("Ngày mua")]
        public string BuyDate { get; set; }

        [Description("Đơn giá mua")]
        public decimal BuyPrice { get; set; }

        [Description("Tổng giá trị mua")]
        public decimal BuyAmount { get; set; }

        [Description("Mức sinh lời")]
        public decimal Rate { get; set; }

        [Description("Ngày bán")]
        public string SellDate { get; set; }

        [Description("Đơn giá bán")]
        public decimal SellPrice { get; set; }

        [Description("Tổng giá bán trước thuế")]
        public decimal SellAmount { get; set; }

        [Description("Tổng giá bán thực nhận")]
        public decimal RealSellAmount { get; set; }

        [Description("Thuế TNCN (nếu có)")]
        public decimal Tax { get; set; }

        [Description("Lãi đầu tư")]
        public decimal Interest { get; set; }

        [Description("Phí chuyển nhượng")]
        public decimal Fee { get; set; }

        [Description("Kỳ hạn")]
        [JsonIgnore]
        public decimal Term { get; set; }

        [Description("Kỳ hạn/Kỳ trả lãi")]
        public string TermName
        {
            get
            {
                return string.Format(ResourceFile.OrderModule.TermName, string.Format("{0:0.##}", (Term)));
            }
        }

        [Description("Bảng dòng tiền")]
        [JsonIgnore]
        public string PriceInfo { get; set; }

        [Description("Lịch thanh toán")]
        public List<MmPriceDetailInfo> PriceInfoList { get; set; }

        [Description("Bảng tài khoản nhận tiền")]
        [JsonIgnore]
        public string PaymentInfo { get; set; }

        [Description("Kênh đặt lệnh")]
        public int ChannelType { get; set; }

        [Description("Tên kênh đặt lệnh")]
        public string ChannelTypeName
        {
            get
            {
                return ResourceFile.OrderModule.ResourceManager.GetString("ChannelType_" + ChannelType);
            }
        }

        [Description("CustBankID1")]
        public int CustBankID1 { get; set; }

        [Description("Số TK ngân hàng 1")]
        public string BankAccountNo1 { get; set; }

        [Description("Tên chủ TK ngân hàng 1")]
        public string BankAccountName1 { get; set; }

        [Description("Tên ngân hàng 1")]
        public string BankName1 { get; set; }

        [Description("Tên chi nhánh 1")]
        public string SubBranchName1 { get; set; }

        [Description("Số tiền 1")]
        public decimal Amount1 { get; set; }

        [Description("CustBankID2")]
        public int CustBankID2 { get; set; }

        [Description("Số TK ngân hàng 2")]
        public string BankAccountNo2 { get; set; }

        [Description("Tên chủ TK ngân hàng 2")]
        public string BankAccountName2 { get; set; }

        [Description("Tên ngân hàng 2")]
        public string BankName2 { get; set; }

        [Description("Tên chi nhánh 2")]
        public string SubBranchName2 { get; set; }

        [Description("Số tiền 2")]
        public decimal Amount2 { get; set; }

        [Description("Kỳ trả lãi Coupon")]
        public int CouponTerm { get; set; }
    }

    public class MmOrderRefuseRequest : BaseRequest
    {
        [Description("Số hợp đồng")]
        public long RequestOrderID { get; set; }

        [Description("Lý do")]
        public string Note { get; set; }
    }

    public class MmPriceDetailInfo
    {
        [Description("Nội dung")]
        public string Content { get; set; }

        [Description("Ngày nhận")]
        public string CouponDate { get; set; }

        [Description("Giá trị")]
        public string Value { get; set; }
    }
}
