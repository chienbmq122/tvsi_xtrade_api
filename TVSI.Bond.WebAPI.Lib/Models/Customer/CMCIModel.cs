﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace TVSI.Bond.WebAPI.Lib.Models.Customer
{
    #region Customer Info
    public class CustInfoRequest : BaseRequest
    {
        
    }

    public class CustInfoResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get { return CustCode + "1"; } }

        [Description("Mã Khách hàng")]
        public string CustCode { get; set; }

        [Description("Họ và tên")]
        public string CustName { get; set; }
        [Description("Số CMND")]
        public string CardID { get; set; }
        [Description("Ngày cấp")]
        public string IssueDate { get; set; }
        [Description("Nơi cấp")]
        public string CardIssuer { get; set; }

        [Description("Địa chỉ")]
        public string Address { get; set; }
        [Description("Ngày sinh")]
        public string BirthDate { get; set; }
        [Description("Mã nhân viên quản lý")]
        public string SaleID { get; set; }

        [Description("Tên nhân viên quản lý")]
        public string SaleName { get; set; }

        [Description("Chi nhánh")]
        public string Branch { get; set; }

        [Description("Loại khách hàng")]
        public string CustomerGroupName { get; set; }

        [Description("Tên chi nhánh")]
        public string BranchName { get; set; }

        [Description("Có thể mua: 0-Yes, 1-No")]
        public int CanBuy { get; set; }
        [Description("Có thể bán: 0-Yes, 1-No")]
        public int CanSell { get; set; }

        [Description("Nhóm ký quỹ")]
        public string MarginGroup { get; set; }

        [Description("Số điện thoại")]
        public string Mobile { get; set; }

        [Description("Email")]
        public string Email { get; set; }

        //[JsonProperty("Mobile")]
        //[Description("Số điện thoại")]
        //public string MobileStr
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(Mobile))
        //        {
        //            return Mobile.Length >= 3 ? Mobile.Remove(Mobile.Length - 3) + "***" : "***";
        //        }

        //        return Mobile;
        //    }
        //}

        //[JsonProperty("Email")]
        //[Description("Email")]
        //public string EmailStr
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(Email))
        //        {
        //            return Email.Length >= 3 ? "***" + Email.Substring(3) : "***";
        //        }

        //        return Email;
        //    }
        //}

    }
    #endregion

    #region Email Info
    public class CustEmailInfoRequest : BaseRequest
    {

    }

    public class CustEmailInfoResult
    {
        public CustEmailInfoResult()
        {
            Status = 1;
        }

        [JsonIgnore]
        public string Email { get; set; }

        [JsonProperty("Email")]
        [Description("Email")]
        public string EmailStr
        {
            get
            {
                if (!string.IsNullOrEmpty(Email))
                {
                    return Email.Length >= 3 ? "***" + Email.Substring(3) : "***";
                }

                return Email;
            }
        }

        [JsonIgnore]
        public int Status { get; set; }

        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                if (!string.IsNullOrEmpty(Email))
                    return ResourceFile.CustomerModule.ResourceManager.GetString("DefaultStatus_" + Status);

                return string.Empty;
            }
        }
        #endregion
    }

    #region Phone ContactCenter
    public class CustPhoneInfoRequest : BaseRequest
    {

    }

    public class CustPhoneInfoResult
    {
        [Description("Mã khách hàng")]
        [JsonIgnore]
        public string CustomerID { get; set; }
        [JsonIgnore]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        [Description("Số điện thoại")]
        public string MobileStr
        {
            get
            {
                if (!string.IsNullOrEmpty(Phone))
                {
                    return Phone.Length >= 3 ? Phone.Remove(Phone.Length - 3) + "***" : "***";
                }

                return Phone;
            }
        }

        [JsonIgnore]
        public int Status { get; set; }

        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                if (!string.IsNullOrEmpty(Phone))
                    return ResourceFile.CustomerModule.ResourceManager.GetString("DefaultStatus_" + Status);

                return string.Empty;
            }
        }
    }
    #endregion

    #region Bank Info
    public class CustBankInfoRequest : BaseRequest
    {

    }

    public class CustBankInfoResult
    {
        [JsonIgnore]
        public string CustomerID { get; set; }
        [Description("Số tài khoản (Bank)")]
        public string BankAccount { get; set; }
        [Description("Tên người thụ hưởng")]
        public string BankAccountName { get; set; }
        [Description("Tại ngân hàng")]
        public string BankName { get; set; }
        [Description("Tỉnh/TP")]
        public string Province { get; set; }

        [JsonIgnore]
        public int Status { get; set; }

        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                if (Status == 1)
                    return ResourceFile.CustomerModule.DefaultStatus_1;

                if (Status == 3)
                    return ResourceFile.CustomerModule.DefaultStatus_3;

                if (Status == 4)
                    return ResourceFile.CustomerModule.DefaultStatus_4;

                if (Status == 5)
                    return ResourceFile.CustomerModule.DefaultStatus_5;

                return ResourceFile.CustomerModule.DefaultStatus_0;
            }
        }
    }

    public class FullCustBankInfoRequest : BaseRequest
    {

    }

    public class FullCustBankInfoResult
    {
        [Description("ID")]
        public long BankAccountID { get; set; }

        [JsonIgnore]
        public string CustomerID { get; set; }
        [Description("Số tài khoản (Bank)")]
        public string BankAccount { get; set; }
        [Description("Tên người thụ hưởng")]
        public string BankAccountName { get; set; }
        [Description("Tại ngân hàng")]
        public string BankName { get; set; }
        [Description("Tỉnh/TP")]
        public string Province { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }

        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                if (Status == 1)
                    return ResourceFile.CustomerModule.DefaultStatus_1;

                if (Status == 3)
                    return ResourceFile.CustomerModule.DefaultStatus_3;

                if (Status == 4)
                    return ResourceFile.CustomerModule.DefaultStatus_4;

                if (Status == 5)
                    return ResourceFile.CustomerModule.DefaultStatus_5;

                return ResourceFile.CustomerModule.DefaultStatus_0;
            }
        }
    }

    public class UpdateCustBankRequest : BaseRequest
    {
        [Description("ID")]
        public long BankAccountID { get; set; }
        [Description("Số tài khoản (Bank)")]
        public string BankAccount { get; set; }
        [Description("Trạng thái: 1: Kích hoạt, 5: Khách hàng hủy")]
        public int Status { get; set; }
    }
    #endregion
}
