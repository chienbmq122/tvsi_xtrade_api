﻿using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Models.Customer
{
    #region "Object trả về"
    public class CustAssetTypeRequest : BaseRequest
    {
        [Description("Loại tài sản: 1=Chứng khoán, 2=Trái phiếu, 3=MM, 4=Chi tiet HĐ ứng trước tiền bán")]
        public int AssetType { get; set; }
    }

    public class CustAssetTypeResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }

        [Description("Loại tài sản: 1=Chứng khoán, 2=Trái phiếu, 3=MM, 4=Chi tiet HĐ ứng trước tiền bán")]
        public int AssetType { get; set; }

        [Description("Tài sản Cổ phiếu (Chỉ trả về khi AssetType = 1)")]
        public List<StockAssetInfo> StockAssetList { get; set; }
        public bool ShouldSerializeStockAssetList()
        {
            return AssetType == (int)AssetTypeEnum.StockAsset;
        }

        [Description("Tài sản Trái phiếu (Chỉ trả về khi AssetType = 2)")]
        public BondAssetInfo BondAssetInfo { get; set; }
        public bool ShouldSerializeBondAssetInfo()
        {
            return AssetType == (int)AssetTypeEnum.BondAsset;
        }

        [Description("Tài sản MM (Chỉ trả về khi AssetType = 3)")]
        public List<MmAssetInfo> MmAssetList { get; set; }
        public bool ShouldSerializeMmAssetList()
        {
            return AssetType == (int)AssetTypeEnum.MmAsset;
        }

        [Description("HĐ ứng trước (Chỉ trả về khi AssetType = 4)")]
        public List<AdvanceAssetInfo> AdvanceAssetList { get; set; }

        public bool ShouldSerializeAdvanceAssetList()
        {
            return AssetType == (int)AssetTypeEnum.AdvanceAsset;
        }
    }
    #endregion

    public class StockAssetInfo
    {
        [Description("Mã chứng khoán")]
        public string StockCode { get; set; }
        [Description("Khả dụng")]
        public decimal Available { get; set; }
        [Description("Mua chờ về")]
        public decimal Incoming { get; set; }

        [Description("Quyền")]
        public decimal Corporate { get; set; }

        [Description("Giá trị")]
        public decimal Amount {
            get { return (Available + Incoming + Corporate)*Price*1000; }
        }

        #region Property Ingnored
        [JsonIgnore]
        public decimal Price { get; set; }
        #endregion
    }

    public class BondAssetInfo
    {
        [Description("Danh mục TP Doanh nghiệp")]
        public List<EnterpriseBondInfo> EnterpriseBondList { get; set; }

        [Description("Danh mục TP TVSI")]
        public List<TvsiBondInfo> TvsiBondList { get; set; }
    }

    public class EnterpriseBondInfo
    {
        [Description("Tên TP")]
        public string BondCode { get; set; }
        [Description("Số HĐ")]
        public string ContractNo { get; set; }
        [Description("Khối lượng")]
        public decimal Volume { get; set; }
        [Description("Giá trị")]
        public decimal Amount { get; set; }
    }

    public class TvsiBondInfo
    {
        [Description("Tên TP")]
        public string BondCode { get; set; }
        [Description("Khối lượng")]
        public decimal Volume { get; set; }
        [Description("Giá trị")]
        public decimal Amount { get; set; }
    }

    public class MmAssetInfo
    {
        [Description("Số HĐ")]
        public string ContractNo { get; set; }
        [Description("Ngày cọc")]
        public string StartDate { get; set; }
        [Description("Ngày kết thúc")]
        public string EndDate { get; set; }
        [Description("Lợi tức")]
        public decimal Rate { get; set; }
        [Description("Số tiền đặt cọc")]
        public decimal Amount { get; set; }
    }

    public class AdvanceAssetInfo
    {
        [Description("Số HĐ")]
        public string ContractNo { get; set; }
        [Description("Ngày ứng")]
        public string AdvanceDate { get; set; }
        [Description("Hạn hoàn ứng")]
        public string EndDate { get; set; }
        [Description("Phí ứng")]
        public decimal AdvanceRate { get; set; }
        [Description("Số dư ứng trước")]
        public decimal Amount { get; set; }
    }

    public class AccountNoListRequest : BaseRequest
    {
        
    }

    public class AccountNoListResult
    {
        [JsonIgnore]
        public string CustCode { get; set; }

        [Description("Số tài khoản")]
        public string AccountNo { get; set; }

        [Description("Loại tài khoản")]
        public int AccountType { get; set; }

        [Description("Tên loại tài khoản")]
        public string AccountTypeName {
            get
            {
                return ResourceFile.CustomerModule.ResourceManager.GetString("AccountType_" + AccountType);
            }
        }

        [Description("Cờ đánh dấu tài khoản mặc định")]
        public bool IsDefault { get; set; }

        [Description("Cờ đánh dấu được phép sử dụng Module CCQ")]
        public bool IsAllowFund { get; set; } = true;

        [Description("Cờ đánh dấu được phép sử dụng Module Trái phiếu")]
        public bool IsAllowBond { get; set; } = true;
    }

    public class AlphaAccountRequest : BaseRequest
    {
        
    }

    public class AlphaAccountResult
    {
        public string AccountNo { get; set; }
        public bool IsAlphaAccount { get; set; }
    }

    public class DefaultAccountRequest : BaseRequest
    {
        [Description("Số tài khoản mặc định")]
        public string DefaultAccountNo { get; set; }
    }
}
