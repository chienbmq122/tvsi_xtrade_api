﻿using System.Collections.Generic;
using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.Customer
{
    public class CustBalanceRequest : BaseRequest
    {
        
    }

    public class CustBalanceResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        [Description("Số dư tiền")]
        public decimal Balance { get; set; }
        [Description("Tài sản cổ phiếu")]
        public decimal StockBalance { get; set; }
        [Description("Tài sản trái phiếu")]
        public decimal BondBalance { get; set; }
        [Description("Tài sản Quỹ")]
        public decimal FundBalance { get; set; }
        [Description("Tài sản MM")]
        public decimal MmBalance { get; set; }
        [Description("Tổng Nav")]
        public decimal Nav { get; set; }
        [Description("Tổng tài sản")]
        public decimal Equity { get; set; }
        [Description("Nợ trên TK 6")]
        public decimal Loan_01 { get; set; }
        [Description("Nợ ứng trước HĐ TP")]
        public decimal Loan_02 { get; set; }
        [Description("Tổng nợ")]
        public decimal TotalLoan { get; set; }

    }

    public class CustomerBalanceInfo
    {
        public BalanceInfo BalanceInfo_01 { get; set; }
        public BalanceInfo BalanceInfo_06 { get; set; }
        public List<StockBalanceInfo> StockBalanceInfoes { get; set; }
    }

    public class BalanceInfo
    {
        public string AccountNo { get; set; }
        public decimal CashBalance { get; set; }
        public decimal Debt { get; set; }

        public decimal Lmv { get; set; }
        public decimal Collat { get; set; }
        public decimal Ap { get; set; }
        public decimal AdvWithdraw { get; set; }
    }

    public class StockBalanceInfo
    {
        public string Secsymbol { get; set; }
        public decimal Volume { get; set; }
        public decimal Price { get; set; }
        public string StockType { get; set; }
    }
}
