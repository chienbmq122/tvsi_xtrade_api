﻿using System.Collections.Generic;
using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.Customer
{
    public class CashTransferRequest : BaseRequest
    {
        
    }

    public class CashTransferResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        [Description("Đanh sách ngân hàng")]
        public List<BankInfo> BankInfoes { get; set; }
        [Description("Số tiền có thể chuyển")]
        public CashBalanceInfo CahBalanceInfo { get; set; }
    }

    public class BankInfo
    {
        [Description("ID")]
        public int CustBankId { get; set; }
        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên người thụ hưởng")]
        public string BankAccountName { get; set; }
        [Description("Tên ngân hàng")]
        public string BankName { get; set; }
    }

    public class CashBalanceInfo
    {
        [Description("Số dư tiền")]
        public decimal Balance { get; set; }
        [Description("Số dư có thể rút")]
        public decimal Withdrawal { get; set; }
    }
}
