﻿using System;
using System.Globalization;

namespace TVSI.Bond.WebAPI.Lib.Models.CashTransfer.SqlData
{
    public class SP_SEL_CASHTRANS_Result
    {
        public string AccountNo2 { get; set; }
        public DateTime TRANSDATE { get; set; }
        public DateTime DUEDATE { get; set; }
        public string TRANS_NO { get; set; }
        public string TRANS_TYPE1 { get; set; }
        public int OrderBy1 { get; set; }
        public int OrderBy2 { get; set; }
        public string TRANS_TYPE2 { get; set; }
        public string STOCKSYMBOL { get; set; }
        public decimal? VOLUME { get; set; }
        public decimal? PRICE { get; set; }
        public decimal AMOUNT { get; set; }
        public string REMARK { get; set; }
        public int RowCount { get; set; }
        public decimal? MoneyWaitPayment { get; set; }

    }

    public class SP_SEL_CASHTRANS_DETAIL_Result
    {
        public string AccountNo2 { get; set; }
        public DateTime TRANSDATE { get; set; }
        public DateTime DUEDATE { get; set; }
        public string TRANS_NO { get; set; }
        public string TRANS_TYPE1 { get; set; }
        public int OrderBy1 { get; set; }
        public int OrderBy2 { get; set; }
        public string TRANS_TYPE2 { get; set; }
        public string STOCKSYMBOL { get; set; }
        public decimal? VOLUME { get; set; }
        public decimal? PRICE { get; set; }
        public decimal AMOUNT { get; set; }
        public string REMARK { get; set; }
        public int ROWCOUNT { get; set; }
    }

    public class SP_SEL_FO_ORDERHISTORY
    {
        public int OrderNo{ get; set; }
        public string AccountNo{ get; set; }
        public string SecSymbol { get; set; }
        public string Side{ get; set; }
        public int Volume{ get; set; }
        public DateTime TransactionDate
        {
            get
            {
                try
                {
                    return DateTime.ParseExact(OrderDate + " " + OrderTime, "yyyyMMdd HHmmss", CultureInfo.CurrentCulture);
                }
                catch
                {
                    return DateTime.Now;
                }
            }
        }

        public Decimal Price{ get; set; }
        public int MatchVolume{ get; set; }
        public Decimal MatchPrice{ get; set; }
        public Decimal Comm{ get; set; }
        public Decimal MatchValue { get; set; }
        public Decimal MatchValue_Net { get; set; }
        public Decimal Tax { get; set; }
        public string OrderDate{ get; set; }
        public string OrderTime{ get; set; }
        public int TOTAL_ROW{ get; set; }
    }
    public class SP_SEL_FO_RGRL
    {
        public string ACCOUNTNO { get; set; }
        public DateTime TRADEDATE { get; set; }
        public int TOTAL_ROW { get; set; }
        public string SYMBOL { get; set; }
        public decimal VOLUME { get; set; }
        public decimal BUYPRICE { get; set; }
        public decimal SELLPRICE { get; set; }
        public decimal GAINLOSS { get; set; }
        public decimal PERCENTGL { get; set; }
        public decimal BUYVOLUME { get; set; }
        public decimal SELLVOLUME { get; set; }
    }
}
