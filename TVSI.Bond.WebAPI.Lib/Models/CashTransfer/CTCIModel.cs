﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.CashTransfer
{
    public class CashTransferInfoRequest : BaseRequest { }

    public class CashTransferInfoResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        [Description("Thông tin ngân hàng")]
        public List<BankInfo> BankInfos { get; set; }
        [Description("Số tiền có thể chuyển khoản")]
        public CashBalanceInfo CashBalanceInfo { get; set; }
        [Description("Thông tin TK nội bộ")]
        public List<InternalAccountInfo> InternalAccountInfos { get; set; }
        public List<ShowBlockFeeInfo> BlockFeeInfos { get; set; }
    }

    public class InternalTransferInfoRequest : BaseRequest { }

    public class InternalTransferInfoResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        
        [Description("Số tiền có thể rút")]
        public decimal Withdrawal { get; set; }

        [Description("Thông tin TK nội bộ")]
        public List<InternalAccountInfo> InternalAccountInfos { get; set; }
    }

    public class BankInfo
    {
        [Description("ID")]
        public long CustBankId { get; set; }
        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên người thụ hưởng")]
        public string BankAccountName { get; set; }
        [Description("Tên ngân hàng")]
        public string BankName { get; set; }

    }

    public class InternalAccountInfo
    {
        [Description("Mã khách hàng")]
        public string CustCode { get; set; }
        [Description("Số tài khoản đối ứng")]
        public string InternalAccountNo { get; set; }
        [Description("Tên tài khoản đối ứng")]
        public string InternalAccountName { get; set; }
    }

    public class CashBalanceInfo
    {

        [Description("Số tài khoản")]
        public string AccountNo { get; set; }

        [Description("Số dư tiền")]
        public decimal Balance { get; set; }

        [Description("Số tiền có thể rút")]
        public decimal Withdrawal { get; set; }

        [Description("Phí tạm phong tỏa")]
        public decimal BlockFee { get; set; }

        [Description("Tỷ lệ ký quỹ duy trì / tối thiểu")]
        public decimal MinMarginRate { get; set; }

        [Description("Số tiền rút tối đa")]
        public decimal MaxWithdrawal { get; set; }

        [Description("Số tiền rút nhanh")]
        public decimal FastWithdrawal { get; set; }
    }

    public class AfterWithdrawalInfo
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }

        [Description("Tỷ lệ ký quỹ sau rút")]
        public decimal MarginRate { get; set; }

        [Description("EE sau rút")]
        public decimal EE { get; set; }

        [Description("Mô tả")]
        public string Note { get; set; }
    }

    public class AfterWithdrawalInfoRequest : BaseRequest
    {
        [Description("Số tiền rút")]
        public decimal Amount { get; set; }
    }

    public class AfterWithdrawalInfoResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }

        [Description("Tỷ lệ ký quỹ sau rút")]
        public decimal MarginRate { get; set; }

        [Description("EE sau rút")]
        public decimal EE { get; set; }

        [Description("Mô tả")]
        public string Note { get; set; }
    }
}
