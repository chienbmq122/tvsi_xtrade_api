﻿using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.CashTransfer
{
    public class BankTransferRequest : BaseRequest
    {
        [Description("ID")]
        public int CustBankID { get; set; }
        [Description("Số tài khoản ngân  hàng")]
        public string BankAccountNo { get; set; }
        [Description("Nội dung chuyển")]
        public string Note { get; set; }
        [Description("Số tiền chuyển")]
        public decimal Amount { get; set; }

    }
}
