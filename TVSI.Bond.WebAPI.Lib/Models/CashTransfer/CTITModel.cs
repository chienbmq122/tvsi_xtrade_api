﻿using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.CashTransfer
{
    public class InternalTransferRequest : BaseRequest
    {
        [Description("Số Tài khoản nhận")]
        public string ReceiveAccountNo { get; set; }
        [Description("Nội dung chuyển")]
        public string Note { get; set; }
        [Description("Số tiền chuyển")]
        public decimal Amount { get; set; }

    }
}
