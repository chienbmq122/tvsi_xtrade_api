﻿using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.CashTransfer
{
    public class CashTransHistRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }
    }

    public class CashTransHistResult
    {
        [Description("Ngày thực hiện")]
        public string TradeDate { get; set; }
        [Description("Số tiền chuyển")]
        public decimal Amount { get; set; }
        [Description("Từ ngày")]
        public string FromAccount { get; set; }
        [Description("Đến ngày")]
        public string ToAccount { get; set; }

        [Description("Tên chủ TK ngân hàng")]
        public string BankAccountName { get; set; }
        [Description("Tên ngân hàng")]
        public string BankName { get; set; }
        [Description("Tên chi nhánh")]
        public string SubBranchName { get; set; }
        [Description("Trạng thái")]
        public string Status { get; set; }

        [Description("Tên Trạng thái")]
        public string StatusName
        {
            get { return ResourceFile.CashTransferModule.ResourceManager.GetString("CashTransStatus_" + Status); }
        }

        [Description("Loại giao dịch. 0-Nộp lên VSD, 1-Rút từ VSD, 2-Nội bộ, 3-Ngân hàng")]
        public int TransferType { get; set; }

        [Description("Tên loại giao dịch")]
        public string TransferTypeName {
            get
            {
                return ResourceFile.CashTransferModule.ResourceManager.GetString("CashTransType_" + TransferType);
            }
        }

        [Description("Nội dung chuyển tiền")]
        public string Remark { get; set; }
    }
}
