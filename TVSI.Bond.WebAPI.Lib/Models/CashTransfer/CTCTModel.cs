﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.CashTransfer
{
    public class CashTransactionRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }

        [Description("Thứ tự trang")]
        public int PageIndex { get; set; }

        [Description("Số Item trên 1 trang")]
        public int PageSize { get; set; }
    }

    public class CashTransactionDetailRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }
        public string Symbol { get; set; }
        [Description("Loại giao dịch: = '' Là lấy tất cả; ")]
        public string TransType { get; set; }

        [Description("Thứ tự trang")]
        public int PageIndex { get; set; }

        [Description("Số Item trên 1 trang")]
        public int PageSize { get; set; }
    }

    public class RealizeGainLossRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }
        public string Symbol { get; set; }

        [Description("Thứ tự trang")]
        public int PageIndex { get; set; }

        [Description("Số Item trên 1 trang")]
        public int PageSize { get; set; }
    }

    public class InComeAndExpenseRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }
        public string Symbol { get; set; }

        [Description("Thứ tự trang")]
        public int PageIndex { get; set; }

        [Description("Số Item trên 1 trang")]
        public int PageSize { get; set; }
    }

    public class TransactionFeeStatementRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }

        [Description("Loại giao dịch: = '' Là lấy tất cả; B: Buy; S: Sell ")]
        public string Side { get; set; }

        [Description("Thứ tự trang")]
        public int PageIndex { get; set; }

        [Description("Số Item trên 1 trang")]
        public int PageSize { get; set; }
    }

    public class CashTransactionResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }

        [Description("Số dư đầu kỳ")]
        public decimal CashBalanceFirst { get; set; }
        public decimal TotalRow { get; set; }

        public List<CashTransactionData> CashTransactionDatas { get; set; }
        [Description("Số dư chờ thanh toán")]
        public decimal CashBalanceWithdraw { get; set; }
    }

    public class CashTransactionDetailResult
    {

        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        public decimal TotalRow { get; set; }
        public List<CashTransactionDetailData> CashTransactionDatas { get; set; }
    }

    public class TransactionFeeStatementResult
    {

        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        public decimal TotalRow { get; set; }
        public List<TransactionFeeStatementData> TransactionFeeStatementDatas { get; set; }
    }

    public class RealizeGainLossResult
    {

        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        public decimal TotalRow { get; set; }
        public decimal TotalGainLoss { get; set; }
        public decimal PercentGL { get; set; }
        public List<RealizeGainLossData> RealizeGainLossDatas { get; set; }
    }

    public class InComeAndExpenseResult
    {
        [Description("Cổ tức")]
        public decimal? DividendAmount { get; set; }

        [Description("Lãi vay")]
        public decimal? InterestIncome { get; set; }
        [Description("Lãi tiền gửi")]
        public decimal? InterestExpense { get; set; }
    }


    public class RealizeGainLossData
    {
        public DateTime TrateDate { get; set; }
        public string Symbol { get; set; }
        public decimal BuyPrice { get; set; }
        public decimal Volume { get; set; }
        public decimal SellPrice { get; set; }
        public decimal GainLost { get; set; }
        public decimal PercentGL { get; set; }
    }

    public class TransactionFeeStatementData
    {
        public int OrderNo { get; set; }
        public string AccountNo { get; set; }
        public string Symbol { get; set; }
        public string Side { get; set; }
        public int Volume { get; set; }
        public DateTime TransactionDate { get; set; }
        public Decimal Price { get; set; }
        public int MatchVolume { get; set; }
        public Decimal MatchPrice { get; set; }
        public Decimal Comm { get; set; }
        public Decimal Tax { get; set; }
        public Decimal MatchValue { get; set; }
        public Decimal MatchValue_Net { get; set; }
        public string OrderDate { get; set; }
        public string OrderTime { get; set; }
    }

    public class CashTransactionDetailData
    {
        [Description("Ngày thực hiện")]
        public string TradeDate { get; set; }
        [Description("Mã giao dịch")]
        public string CashTransNo { get; set; }
        [Description("Mô tả giao dịch")]
        public string TransactionName { get; set; }
        [Description("Mã chứng khoán")]
        public string Symbol { get; set; }
        [Description ("Mô tả")]
        public string Remark { get; set; }
        [Description("Giá trị")]
        public decimal Amount { get; set; }
    }

    public class CashTransactionData
    {
        [Description("Ngày thực hiện")]
        public string TradeDate { get; set; }
        [Description("Mô tả giao dịch")]
        public string TransactionName { get; set; }
        [Description("Giá trị")]
        public decimal Amount { get; set; }
    }

    public class CashNormalAccountInfoDB2
    {
        public string ACCOUNTNO { get; set; }
        public decimal CASHBALANCE { get; set; }
        public decimal T_BUY { get; set; }
        public decimal TOTALBUY { get; set; }
        public string PACKAGETYPE { get; set; }
    }
    
    public class CalcMarginAccountInfoDB2
    {
        public string ACCOUNTNO { get; set; }
        public decimal CASH_BALANCE { get; set; }
        public decimal WITHDRAWAL { get; set; }
        public decimal LMV { get; set; }
        public decimal AP { get; set; }
        public decimal AP_T1 { get; set; }
        public decimal COLLAT { get; set; }
        public decimal DEBT { get; set; }
        public decimal BUYUNMATCH { get; set; }
        public decimal LOAN_INTEREST { get; set; }
        public decimal ADV_WITHDRAW { get; set; }
        public decimal EE { get; set; }
        public decimal PP { get; set; }
        public decimal CALL_FORCE { get; set; }
        public decimal BRK_SELL_LMV { get; set; }
        public decimal EEAPP { get; set; }
        public decimal PPAPP { get; set; }
        public string MARGIN_GROUP { get; set; }
        public string WH_FLAG { get; set; }
        public decimal P_BUYUNMATCH { get; set; }
    }

    public class StockBalanceInfoDB2
    {
        public string ACCOUNTNO { get; set; }
        public decimal ASSETTOLMV { get; set; }
        public string SECSYMBOL { get; set; }
        public string MARGIN_GROUP { get; set; }
        public decimal AVAIVOLUME { get; set; }
        public decimal BUYVOLUMET { get; set; }
        public decimal BUYVOLUMET1T2 { get; set; }
        public int PLEDGERATE { get; set; }
        public int MRGRATE { get; set; }
        public decimal TOTALVOLUME { get; set; }
        public decimal LVM { get; set; }
        public decimal DEBT { get; set; }
        public decimal ASSETVALUE { get; set; }
        public decimal EQUITY { get; set; }
        public decimal R { get; set; }
    }

    public class BlockFeeInfo
    {
        public string So_tai_khoan { get; set; }
        public string Loai_phi { get; set; }
        public decimal So_tien { get; set; }
    }
    
    public class ShowBlockFeeInfo
    {
        public string FeeType { get; set; }
        public string FeeName { get; set; }
        public decimal Amount { get; set; }
    }

    public class DepositBankResult
    {
        [Description("Tên ngân hàng")]
        public string BankName { get; set; }
        [Description("Tên ngân hàng tiếng Anh")]
        public string BankNameEN { get; set; }
        [Description("Số tài khoản")]
        public string BankAccountNo { get; set; }
        [Description("Tên tài khoản")]
        public string BankAccountName { get; set; }
        [Description("Tên tài khoản tiếng Anh")]
        public string BankAccountNameEN { get; set; }
        [Description("Tên viết tắt")]
        public string Abbre { get; set; }
    }


}
