﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Utility;


namespace TVSI.Bond.WebAPI.Lib.Models.MarginPackage
{
    public class MarginPackageRequest : BaseRequest
    {

    }
    public class MarginPackageResult
    {
        [Description("ID gói dịch vụ")]
        public int MarginID { get; set; }
        
        [Description("Tên gói dịch vụ")]
        public string MarginName { get; set; }

        [Description("Lãi suất")]
        public string InterestRate { get; set; }
        [Description("Tỷ lệ call")]
        public string CallRate { get; set; }
        //[Description("Cập nhật sức mua")]
        //public string BuyCreditUpdate { get; set; }

        //[Description("Số ngày miễn lãi")]
        //public string DaysInterestFree { get; set; }

        //[Description("Tỷ lệ ký quỹ")]
        //public string DepositRate { get; set; }

        //[Description("Tỷ lệ cảnh báo")]
        //public string AlertRate { get; set; }

        //[Description("Tỷ lệ vay")]
        //public string MarginRate { get; set; }
    }

    public class MarginPackageResponse
    {
        [Description("Tên gói dịch vụ")]
        public string MarginName { get; set; }

        [Description("Lãi suất")]
        public string InterestRate { get; set; }

        [Description("Sức mua")]
        public string BuyCredit { get; set; }

        [Description("Sức mua tối đa")]
        public string BuyCreditMax { get; set; }

        [Description("Tỷ lệ call")]
        public string CallRate { get; set; }
        [Description("Cập nhật sức mua")]
        public string BuyCreditUpdate { get; set; }

        [Description("Số ngày miễn lãi")]
        public string DaysInterestFree { get; set; }

        [Description("Tỷ lệ ký quỹ")]
        public string DepositRate { get; set; }

        [Description("Tỷ lệ duy trì")]
        public string MaintainRate { get; set; }

        [Description("Tỷ lệ cảnh báo")]
        public string AlertRate { get; set; }

        [Description("Tỷ lệ vay")]
        public string MarginRate { get; set; }

        [Description("Dư nợ tối đa")]
        public string MaxLoan { get; set; }

        [Description("Số lượng mã chứng khoán")]
        public string StockCount { get; set; }
        
        
    }

    public class GetBuyPowerPackResponse
    {
        public string ACCOUNTNO { get; set; }
        public string MARGIN_GROUP { get; set; }
        public string SUC_MUA { get; set; }
        public string SUC_MUA_TOI_DA { get; set; }
        public string LMV_NEXT { get; set; }
        
        public string EE_HIEN_TAI { get; set; }
        public string EE_GOI_MOI { get; set; }

    }

    public class MarginDetailPackageRequest : BaseRequest
    {
        [Description("Tên gói dịch vụ tìm kiếm")]
        public string MarginName { get; set; }
    }
    public class MarginDetailPackageResult
    {
        [Description("ID gói dịch vụ")]
        public int MarginID { get; set; }

        [Description("Cho phép đăng ký gói")] 
        public bool MarginAccept { get; set; } = true;
        
        [Description("Tên gói dịch vụ")]
        public string MarginName { get; set; }

        [Description("Lãi suất")]
        public string InterestRate { get; set; }

        [Description("Sức mua")]
        public string BuyCredit { get; set; }

        [Description("Sức mua tối đa")]
        public string BuyCreditMax { get; set; }

        [Description("Tỷ lệ call")]
        public string CallRate { get; set; }
        [Description("Cập nhật sức mua")]
        public string BuyCreditUpdate { get; set; }

        [Description("Số ngày miễn lãi")]
        public string DaysInterestFree { get; set; }

        [Description("Tỷ lệ ký quỹ")]
        public string DepositRate { get; set; }

        [Description("Tỷ lệ duy trì")]
        public string MaintainRate { get; set; }

        [Description("Tỷ lệ cảnh báo")]
        public string AlertRate { get; set; }

        [Description("Tỷ lệ vay")]
        public string MarginRate { get; set; }

        [Description("Dư nợ tối đa")]
        public string MaxLoan { get; set; }

        [Description("Số lượng mã chứng khoán")]
        public string StockCount { get; set; }
    }

    public class PreferentialMarginListRequest : BaseRequest
    {

    }
    public class PreferentialMarginListResult
    {
        [Description("ID gói ưu đãi")]
        public int PrefMarginID { get; set; }
        [Description("Tên gói ưu đãi")]
        public string PrefMarginName { get; set; }
        [Description("Mô tả")]
        public string Note { get; set; }
    }

    public class PreferentialMarginDetailRequest : BaseRequest
    {
        [Description("Tên gói dịch vụ ưu đãi tìm kiếm")]
        public string PrefMarginName { get; set; }
    }
    public class PreferentialMarginDetailResult
    {
        [Description("ID gói ưu đãi")]
        public int PrefMarginID { get; set; }
        [Description("Cho phép đăng ký gói")] 
        public bool MarginAccept { get; set; } = true;
        [Description("Tên gói ưu đãi")]
        public string PrefMarginName { get; set; }
        [Description("Lãi suất tối thiểu")]
        public string InterestMin { get; set; }
        [Description("Tỷ lệ phí tối thiểu")]
        public string FeeRateMin { get; set; }
        [Description("Phí giao dịch tối thiểu")]
        public string FeeTransactionMin { get; set; }

        [Description("Số ngày miễn lãi")]
        public string DaysOfInterestFree { get; set; }

        [Description("Vòng quay dư nợ tháng")]
        public string RotationDebtMonth { get; set; }

        [Description("Dư nợ tối đa")]
        public string DebtMax { get; set; }

        [Description("Phí dịch vụ gói")]
        public string FeeMargin { get; set; }

        [Description("Tỷ lệ vay tối đa")]
        public string MarginRateMax { get; set; }

        [Description("Số lượng mã cho vay")]
        public string StockCount { get; set; }

        [Description("Đối tượng cho vay")]
        public string ObjectForLoan { get; set; }

        [Description("Mô tả")]
        public string note { get; set; }
    }

    public class InsertServicePreferRequest : BaseRequest
    {
        [Description("ngày đăng ky")]
        [Required]
        public string RegDate { get; set; }
        [Description("Gói hiện tại")]
        
        [Required]
        public string CurrentPack { get; set; }
        [Description("gói mới")]
        [Required]
        public string CurrentNew { get; set; }
        [Description("Họ tên KH")]
        [Required]
        public string FullName { get; set; }
    }  
    public class InsertServiceRequest : BaseRequest
    {
        [Description("ngày đăng ky")]
        [Required]
        public string RegDate { get; set; }

        [Description("Gói hiện tại")]
        [Required]
        public string CurrentPack { get; set; }
        [Description("gói mới")]
        [Required]
        public string CurrentNew { get; set; }
        
        [Description("Họ tên KH")]
        [Required]
        public string FullName { get; set; }
    }

    public class UpdateServicePreferRequest : BaseRequest
    {   
        [Description("ID gói dịch vụ")]
        [Required]
        public int PackPreferID { get; set; }
        [Description("Trạng thái")]
        [Required]
        public int Status { get; set; }
        
    }

    public class UpdateServicePackRequest : BaseRequest
    {
        [Description("ID gói dịch vụ")]
        [Required]
        public int PackID { get; set; }
        [Description("Trạng thái")]
        [Required]
        public int Status { get; set; }
    }

    public class UpdateServicePreferResponse
    {
        public int PackPreferID { get; set; }
        public int Status { get; set; }
    }  
    public class UpdateServiceResponse
    {
        public int PackID { get; set; }
        public int Status { get; set; }
    }

    public class ServicePreferResult
    {
        [Description("Mã khách hàng")]
        public string AccountNo { get; set; }
        [Description("Mã dịch vụ")]
        public string ServiceCode { get; set; }
    }

    public class ServiceResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        [Description("Lãi suất")]
        public string Rate { get; set; }
        [Description("Gói dịch vụ")]
        public string CurrentPack { get; set; }
        [Description("Sức mua")]
        public string BuyingPower { get; set; }
        [Description("Tỷ lệ call")]
        public string CallMargin { get; set; }
        [Description("Mã cho vay")]
        public string CodeLoan { get; set; }
        [Description("Cập nhật sức mua")]
        public string UpdateBuying { get; set; }
        [Description("Số ngày miễn lãi")]
        public string DateFree { get; set; }
        [Description("Tỷ lệ ký quỹ")]
        public string MarginRate { get; set; }
        [Description("Tỷ lệ cảnh báo")]
        public string MarginAlert { get; set; }
        [Description("Tỷ lệ vay")]
        public string MarginLoan { get; set; }
        public IEnumerable<MarginGRPTabResponse> StockList { get; set; }

    }
    public class ServicePackMargin
    {
        [Description("Id")]
        public string PackID { get; set; }
        
        [Description("Tên gói")]
        public string PackName { get; set; }
        [Description("Lãi suất")]
        public string Rate { get; set; }
        [Description("Tỷ lệ call")]
        public string CallMargin { get; set; }
        [Description("Cập nhật sức mua")]
        public string UpdateBuying { get; set; }
        [Description("Số ngày miễn lãi")]
        public string DateFree { get; set; }
        [Description("Tỷ lệ ký quỹ")]
        public string MarginRate { get; set; }
        [Description("Tỷ lệ cảnh báo")]
        public string MarginAlert { get; set; }
        [Description("Tỷ lệ vay")]
        public string MarginLoan { get; set; }

    }
    public class MarginGRPTabResponse
    {
        public string SYMBOL { get; set; }
        public string MRGRATE { get; set; }
        public string PLEDGERATE { get; set; }
        public string CANBUY { get; set; }
        public string CANSELL { get; set; }
        public string DEFAULT { get; set; }
        public string LIMIT_PRICE { get; set; }
    }


    public class GetServicePreferHistResult
    {
        public string AccountNo { get; set; }
        public  IEnumerable<GetServicePreferHist> MarginList { get; set; }
    } 
    public class GetServiceHistResult
    {
        public string AccountNo { get; set; }
        public  List<GetServiceHist> MarginList { get; set; }
    }

    public class GetServicePreferHist
    {
        [Description("ID")]
        public int PackPreferID { get; set; }
        
        [Description("Ngày đăng ký")]
        public string RegDate {
            get
            {
                return RegDateOn.ToString("dd/MM/yyyy");
            }

        } 
        
        [JsonIgnore]
        public DateTime RegDateOn { get; set; } 
        
        
        [Description("Ngày hiệu lực")]
        public string EffDate {
            get
            {
                return EffDateOn.ToString("dd/MM/yyyy");
            }
            
        }
        
        [JsonIgnore]
        public DateTime EffDateOn { get; set; }
        
        [Description("Ngày kết thúc")]
        public string EndDate {
            get
            {
                return EndDateOn.ToString("dd/MM/yyyy");
            }
            
        }
        
        [JsonIgnore]
        public DateTime EndDateOn { get; set; }
        
        
        [Description("Gói dv ưu đãi hiện tại")]
        public string CurrentPack { get; set; }
        
        [Description("Gói dv ưu đãi mới")]
        public string CurrentNew { get; set; }
        
        [Description("Lý do từ chối")]
        public string Reject { get; set; }
        
        [Description("Ngày tạo")]
        public string CreateDate {
            get
            {
                return CreateDateOn?.ToString("dd/MM/yyyy") ?? "";
            }
            
        }
        
        [JsonIgnore]
        public DateTime? CreateDateOn { get; set; }
        
        [Description("Ngày phê duyệt")]
        public string ApproveDate {
            get
            {
                return ApproveDateOn.ToString("dd/MM/yyyy");
            }
            
        }
        
        [JsonIgnore]
        public DateTime ApproveDateOn { get; set; }
        
        [Description("Ngươi phê duyệt")]
        public string ApproveBy { get; set; }
        
        [Description("trạng thái")]
        public int Status { get; set; }
        
        [Description("trạng thái STR")]
        public string StatusName { get; set; }
    }
    public class GetServiceHist
    {
        [Description("ID")]
        public int PackID { get; set; }
        
        [Description("Ngày đăng ký")]
        public string RegDate {
            get
            {
                return RegDateOn?.ToString("dd/MM/yyyy") ?? "";
            }

        } 
        
        [JsonIgnore]
        public DateTime? RegDateOn { get; set; } 
        
        
        [Description("Ngày hiệu lực")]
        public string EffDate {
            get
            {
                return EffDateOn?.ToString("dd/MM/yyyy") ?? "";
            }
        }
        
        [JsonIgnore]
        public DateTime? EffDateOn { get; set; }
        
        [Description("Ngày kết thúc")]
        public string EndDate {
            get
            {
                return EndDateOn?.ToString("dd/MM/yyyy") ?? "";
            }
            
        }
        
        [JsonIgnore]
        public DateTime? EndDateOn { get; set; }
        
        [Description("Gói dv ưu đãi hiện tại")]
        public string CurrentPack { get; set; }
        [Description("Gói dv ưu đãi mới")]
        public string CurrentNew { get; set; }
        [Description("Lý do từ chối")]
        public string Reject { get; set; }
        [Description("Ngày tạo")]
        public string CreateDate {
            get
            {
                return CreateDateOn.ToString("dd/MM/yyyy");
            }
            
        }
        
        [JsonIgnore]
        public DateTime CreateDateOn { get; set; }
        
        [Description("Ngày phê duyệt")]
        public string ApproveDate {
            get
            {
                return ApproveDateOn?.ToString("dd/MM/yyyy") ?? "";
            }
            
        }
        
        [JsonIgnore]
        public DateTime? ApproveDateOn { get; set; }
        
        [Description("Ngươi phê duyệt")]
        public string ApproveBy { get; set; }
        [Description("trạng thái")]
        public int Status { get; set; }
        [Description("trạng thái STR")]
        public string StatusName { get; set; }
    }

    public class LoginAccountResult
    {
        [Description("Số tk khách hàng")]
        public string AccountNo { get; set; }
        [Description("Họ tên KH")]
        public string FullName { get; set; }
        [Description("0: đăng nhập không thành công, 1: là thành công, 2: chưa đăng ký margin")]
        public int Status { get; set; }
        public List<PackListResult> MarginList { get; set; }
    }

    public class PackListResult
    {
        [Description("Id")]
        public int PackID { get; set; }
        [Description("Tên gói")]
        public string PackName { get; set; }

    }
    
}
