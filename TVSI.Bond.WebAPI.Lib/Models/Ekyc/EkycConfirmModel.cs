﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.Bond.WebAPI.Lib.Models.Ekyc
{
    public class EkycConfirmModelRequest
    {
        [Description("code confirm Ekyc Mobile")]
        [Required]
        public string codem { get; set; }
    }

    public class GetDataModel
    {
        public string crm_id { get; set; }
    }

    public class CustCRM
    {
        public string fullname { get; set; }
        public string email { get; set; }
        public string saleid { get; set; }
    }

    public class AddressBranch
    {
        public string mcn { get; set; } 
        public string Ten_CN { get; set; } 
        public string Dia_Chi { get; set; } 
    }
}