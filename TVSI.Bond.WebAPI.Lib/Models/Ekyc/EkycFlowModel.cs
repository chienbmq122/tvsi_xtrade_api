﻿namespace TVSI.Bond.WebAPI.Lib.Models.Ekyc
{
    public class EkycFlowRequest : BaseEkycRequest
    {
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// Số điện thoai
        /// </summary>
        public string Phone { get; set; }
    }
}