﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TVSI.Bond.WebAPI.Lib.Models.Ekyc
{
    public class EkycInfoRequest : BaseRequest
    {
        
    }

    public class CardIdExsitedRequest : BaseEkycRequest
    {
        [Description("Kiểm tra thông tin CMND/CCCD có tồn tại trong hệ thống")]
        [Required]
        public string CardId { get; set; }
    }

    public class UserInfoResult
    {
        [Description("Mã khách hàng")]
        public string CustCode { get; set; }       
        [Description("Số tài khoản")]
        
        public string AccountNo { get; set; }  
        [Description("Họ và tên")]
        public string CustName { get; set; }   
        [Description("CMND/CCCD")]
        public string CardID { get; set; }

        [Description("giới tính")]
        public int Sex {
            get
            {
                if (!string.IsNullOrEmpty(Gender))
                    return Gender.Equals("F") ? 2 : 1;

                return 0;
            } 
        }
        
        [JsonIgnore]
        public string Gender { get; set; }
        
        [Description("Ngày sinh")]
        public string BirthDay {
            get
            {
                return BirthDayOn.ToString("dd/MM/yyyy");
            }
            
        }
        
        [JsonIgnore]
        public DateTime BirthDayOn { get; set; }
        
        [Description("Ngày cấp cmnd")]
        public string IssueDate {
            get
            {
                return IssueDateOn.ToString("dd/MM/yyyy");
            } 
        }
        [JsonIgnore]
        public DateTime IssueDateOn { get; set; }
        
        [Description("Nơi cấp cmnd")]
        public string CardIssue { get; set; }
        
        
        
        [Description("Địa chỉ")]
        public string Address { get; set; }  
        [Description("Số điện thoại")]
        public string Mobile { get; set; }  
        [Description("Email")]
        public string Email { get; set; }  
        [Description("Đường dẫn chứa ảnh + video EKYC")]
        public FolderPath FolderPath { get; set; }
    }

    public class GetUserCust : UserInfoResult
    {
        public string ma_kinh_doanh { get; set; }
        public DateTime ngay_cap { get; set; }
        public DateTime ngay_sinh { get; set; }
        public string noi_cap { get; set; }
    }

    public class ExtendDb
    {
        public string Extend { get; set; }
    }

    public class ExtendAccountDb
    {
        public int ho_so_mo_rong_id { get; set; }
        public string ho_so_mo_tk_id { get; set; }
        public string ma_khach_hang { get; set; }
        public string thong_tin_mo_rong { get; set; }
        public string nguoi_tao { get; set; }
        public DateTime ngay_tao { get; set; }
        public string trang_thai_update { get; set; }
        
    }

    public class EkycSaveUserInfoRequest : BaseEkycRequest
    {
        [Description("Họ và Tên")] 
        [Required]
        public string CustomerName { get; set; }

        [Description("Số CMND/CCCD của KH dd/MM/yyyy")]
        [Required]
        public string CardID { get; set; }

        [Description("Ngày cấp")] 
        [Required]
        public string IssueDate { get; set; }

        [Description("Nơi cấp CMND/CCCD của KH")]
        [Required]
        public string CardIssuer { get; set; }

        [Description("Giới Tính 1 là Nam, 2 là Nữ,3 là không xác định")]
        [Required]
        public int Sex { get; set; }

        [Description("Ngày sinh dd/MM/yyyy")] 
        [Required]
        public string Birthday { get; set; }

        [Description("Nơi đăng ký Hộ Khẩu Thường Chú")]
        [Required]
        public string Address_01 { get; set; }

        [Description("Số điện thoại của KH")]
        [Required]
        public string Mobile { get; set; }

        [Description("Địa chỉ email")]
        [Required]
        public string Email { get; set; }

        [Description("Địa chỉ liên hệ")] 
        [Required]
        public string Address_02 { get; set; }  
        
        

        [Description("Mã nhân viên tư vấn. Nếu nhập thì bắt buộc là 4 ký tự = số")] 
        [Required]
        public string SaleID { get; set; }

        [Description("Có Đăng ký dịch vụ chuyển tiền trực tuyến 0:Không đăng ký, 1: là đăng ký")]
        public int BankTransferMethod { get; set; }

        [Description("Danh sách tài khoản ngân hàng List<BankAccountData>")]
        public List<BankAccountData> BankAccountList { get; set; }
        
        [Description("Có đăng ký tài khoản giao dịch ký quỹ? 0:Không đăng ký, 1: Có đăng ký")]
        public int MarginAccount { get; set; }
        
        [Description("Phương thức giao dịch 1:Qua điện thoại,2:Qua Email,12:Qua điện thoại + Email")]
        public string TradingMethod { get; set; }
        
        [Description("Phương thức nhận KQGD")]
        public string TradingResultMethod { get; set; }
        
        [Description("Phương thức nhận sao kê cuối ngày 2:Qua Email")]
        public string TradingReportMethod { get; set; }
        
        [Description("Phương thức nhận hợp đòng 1:Nhận bản mềm, 2:Nhận bản cứng")]
        public string ReceiveContractMethod { get; set; }
        
        [Description("Thông tin nhận chuyển phát nhanh,Trường hợp nhận bản cứng thì phải gửi thông tin này")]
        public ReceiveContractData ReceiveContractData { get; set; }
        
        [Description("Đường dẫn chưa các video + ảnh upload EKYC")]
        [Required]
        public FolderPath FolderPath { get; set; }
        public Fatca Fatca { get; set; }
    }
    public class EkycSaveUserInfoRequest_v2 : BaseEkycRequest
    {
        [Description("Họ và Tên")] 
        [Required]
        public string CustomerName { get; set; }

        [Description("Số CMND/CCCD của KH dd/MM/yyyy")]
        [Required]
        public string CardID { get; set; }

        [Description("Ngày cấp")] 
        [Required]
        public string IssueDate { get; set; }

        [Description("Nơi cấp CMND/CCCD của KH")]
        [Required]
        public string CardIssuer { get; set; }

        [Description("Giới Tính 1 là Nam, 2 là Nữ,3 là không xác định")]
        [Required]
        public int Sex { get; set; }

        [Description("Ngày sinh dd/MM/yyyy")] 
        [Required]
        public string Birthday { get; set; }

        [Description("Nơi đăng ký Hộ Khẩu Thường Chú")]
        [Required]
        public string Address_01 { get; set; }

        [Description("Số điện thoại của KH")]
        [Required]
        public string Mobile { get; set; }

        [Description("Địa chỉ email")]
        [Required]
        public string Email { get; set; }

        [Description("Địa chỉ liên hệ")] 
        [Required]
        public string Address_02 { get; set; }  
        
        
        [Description("Code tỉnh thành phố")]
        [Required]
        public string ProvinceCode { get; set; }
        
        
        [Description("gói dịch vụ")]
        [Required]
        public int ServicePackage { get; set; }
        
        
        [Description("Loại CMT/CCCD")]
        [Required]
        public int CardType { get; set; }
        
        [Description("Ngày hết hạn CMT/CCCD dd/MM/yyyy")]
        [Required]
        public string CardExpDate { get; set; }
        

        [Description("Mã nhân viên tư vấn. Nếu nhập thì bắt buộc là 4 ký tự = số")] 
        public string SaleID { get; set; }

        [Description("Có Đăng ký dịch vụ chuyển tiền trực tuyến 0:Không đăng ký, 1: là đăng ký")]
        public int BankTransferMethod { get; set; }

        [Description("Danh sách tài khoản ngân hàng List<BankAccountData>")]
        public List<BankAccountData> BankAccountList { get; set; }
        
        [Description("Có đăng ký tài khoản giao dịch ký quỹ? 0:Không đăng ký, 1: Có đăng ký")]
        public int MarginAccount { get; set; }

        [Description("Phương thức nhận KQGD 1:Qua điện thoại,2:Qua Email,12:Qua điện thoại + Email")]
        public string TradingResultMethod { get; set; }
        
        [Description("Yêu cầu")]
        public string Note { get; set; }

        [Description("Đường dẫn chưa các video + ảnh upload EKYC")]
        [Required]
        public FolderPath FolderPath { get; set; }
        public Fatca Fatca { get; set; }
    }


    public class Fatca
    {
        [Description("KH là công dân hoa kỳ hoặc đối tượng cư trú")]
        [Required]
        public int CustomerUSA { get; set; } // KH là công dân hoa kỳ hoặc đối tượng cư trú
        
        [Description("nơi sinh tại hoa kỳ")]
        [Required]
        public int PlaceOfBirthUSA { get; set; }// nơi sinh tại hoa kỳ
        
        [Description("lưu ký tại hoa kỳ")]
        [Required]
        public int DepositoryUSA { get; set; } // lưu ký tại hoa kỳ
        
        [Description(" KH có sđt tại hoa kỳ")]
        [Required]
        public int CustomerPhoneNumberUSA { get; set; }  // KH có sđt tại hoa kỳ
        
        [Description("KH có lệnh định kỳ chuyển khoản")]
        [Required]
        public int CustomerTransferBankUSA { get; set; }  // KH có lệnh định kỳ chuyển khoản
        
        [Description("KH có giấy ủy quyên từ Hoa Kỳ")]
        [Required]
        public int CustomerAuthorizationUSA { get; set; }// KH có giấy ủy quyên từ Hoa Kỳ
        
        [Description("KH Có sử dụng địa chỉ nhận thư hộ hoặc giữ thư tại hoa kỳ")]
        [Required]
        public int CustomerReceiveMailUSA { get; set; }// KH Có sử dụng địa chỉ nhận thư hộ hoặc giữ thư tại hoa kỳ
    }

    public class FolderPath
    {
        [Description("Đường dẫn chứa ảnh mặt trước CCCD/CMND")]
        
        public string ImageFront { get; set; }
        
        [Description("Đường dẫn chứa ảnh sau trước CCCD/CMND")]
       
        public string ImageBack { get; set; }
        
        [Description("Đường dẫn chứa ảnh chân dung")]
        
        public string ImageFace { get; set; }
        
        [Description("Đường dẫn chứa ảnh chứ ký")]
        
        public string ImageSignature { get; set; }
        
        [Description("Đường dẫn chứa video xác thực chữ ký")]
        public string VideoSignature { get; set; }
        
        [Description("Đường dẫn chứa video chân dung + chữ ký xác thực")]
        public string VideoFaceSignature { get; set; }
    }
    

    public class BankAccountData
    {
        [Description("Số tài khoản")]
        public string BankAccount { get; set; }
        [Description("Mã ngân hàng")]
        public string BankNo { get; set; }
        [Description("Chi nhánh")]
        public string BranchNo { get; set; }
        [Description("Tên chủ tài khoản")]
        public string BankAccountName { get; set; }
        
        [Description("Mã thành phố")]
        public string CityNo { get; set; }
    }

    public class ReceiveContractData
    {
        [Description("Tên người nhận")]
        public string RecieverName { get; set; }  
        
        [Description("Số điện thoại người nhận")]
        public string ReceiverMobile { get; set; }  
        
        [Description("Địa chỉ người nhận")]
        public string ReceiverAddress { get; set; }
        
    }

    public class GetBankBranchListRequest : BaseEkycRequest
    {
        [Description("Mã ngân hàng")]
        [Required]
        public string BankNo { get; set; }
    }

    public class AccountData
    {
        [Description("Mã khách hàng. Mã KH 6 số")]
        public string CustCode { get; set; }
        
        [Description("Số TK thường. Mã KH 6 số + '1'")]
        public string AccountNo { get; set; }
        
        [Description("Đường dẫn chứa file ảnh, video của EKYC sau khi đổi tên")]
        public string FolderPath { get; set; }
    }

    public class ConfirmOTP
    {
        [Description("Mã OTP gửi về số điện thoại")]
        [Required]
        public string OTP { get; set; }
        
        [Description("Số CMND/CCCD của Khách hàng")]
        [Required]
        public string CardID { get; set; }
    }

    public class GetUserOtp
    {
        [Description("Số CMND/CCCD của Khách hàng")]
        [Required]
        public string CardID { get; set; }
    }

    public class ConfirmEkycModel
    {
        public string Code { get; set; }
        public string Otp { get; set; }
    }

    public class TvsiBranch
    {
        [Description("Mã chi nhánh")]
        public string BranchNo { get; set; }    
        
        [Description("Tên chi nhánh")]
        public string BranchName { get; set; }
    }

    public class BankInfo
    {
        [Description("Mã ngân hàng")]
        public string BankNo { get; set; }
        [Description("Tên ngân hàng")]
        public string BankName { get; set; }
    }

    public class BranchInfoResult
    { 
        [Description("Mã ngân hàng")]
        public string BankNo { get; set; }
        
        [Description("Mã chi nhánh")]
        public string BranchNo { get; set; }

        [Description("Tên ngân hàng")]
        public string BranchName { get; set; }
    }
    public class GetUserInfoRequest : BaseEkycRequest
    {
        [Description("Mã khách hàng")]
        [Required]
        public string UserID { get; set; }
        [Description("Số tài khoản")]
        [Required]
        public string AccountNo { get; set; }
    }

    public class CheckChangeInfoCust : GetUserInfoRequest
    {
        public bool ChangeCardID { get; set; }
        public bool ChangeIssueDate { get; set; }
        public bool ChangeCardIssuer { get; set; }
        public bool ChangeAddress { get; set; }
    }

    public class UpdateVideoSignatureRequest : GetUserInfoRequest
    {
        [Description("Đường dẫn video Chữ ký Update")]
        [Required]
        public string VideoSignature { get; set; }
    }

    public class Man
    {
        public string manName { get; set; }
        public string postionName { get; set; }
        public string manNo { get; set; }
        public string manDate { get; set; }
        public string ManCardId { get; set; }
        public string ManCardDate { get; set; }
        public string ManCardIssue { get; set; }
    }

    public class ChangeUserInfoRequest : GetUserInfoRequest, IValidatableObject
    {

        [Description("Số CMND của KH")]
        public string CardID { get; set; }

        [Description("Thay đổi ngày cấp CMND/CCCD")]
        public string IssueDate { get; set; }
        
        [Description("Thay đổi nơi cấp CMND/CCCD")]
        public string CardIssuer { get; set; }
        
        [Description("Thay đổi địa chỉ liên hệ")]
        public string Address { get; set; }
        
        [Description("Đường dẫn ảnh TĐTT")]
        public NewImage NewImage { get; set; }
        
        [Description("Ảnh/Giấy tờ chứng minh liên quan đến CCCD/CMND Cũ")]
        public OldImage ProveImage { get; set; } 
        
        [Description("Trang thái thay đổi 1: TĐTT CMND/CCCD Fail Ekyc, 2:TĐTT cmnd/cccd Pass Ekyc")]
        [Required]
        public int Options { get; set; }
        
        [Required]
        public FieldChange FieldChange { get; set; }
        

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();
            if (Options == 1)
            {
                if (NewImage != null)
                {
                    if (string.IsNullOrEmpty(NewImage.NewImageFront) || string.IsNullOrEmpty(NewImage.NewImageBack))
                    {
                        if (!FieldChange.ChangeAddress)
                        {
                            result.Add(new ValidationResult("Field Required"));   
                        }
                    }   
                }

                if (ProveImage != null)
                {
                    if (string.IsNullOrEmpty(ProveImage.ImageOther) 
                        && string.IsNullOrEmpty(ProveImage.OldImageFront) 
                        && string.IsNullOrEmpty(ProveImage.OldImageBack))
                    {
                        if (!FieldChange.ChangeAddress)
                        {
                            result.Add(new ValidationResult("Field Required"));

                        }
                    }
                }
            }
            if (Options == 2)
            {
                
                if (NewImage != null)
                {
                    if (string.IsNullOrEmpty(NewImage.NewImageFront) || string.IsNullOrEmpty(NewImage.NewImageBack))
                    {
                        if (!FieldChange.ChangeAddress)
                        {
                            result.Add(new ValidationResult("Field Required"));

                        }
                    }
                }

                if (ProveImage != null)
                {
                    if (string.IsNullOrEmpty(ProveImage.OldImageFront) && string.IsNullOrEmpty(ProveImage.OldImageBack))
                    {
                        if (!FieldChange.ChangeAddress)
                        {
                            result.Add(new ValidationResult("Field Required"));

                        }
                    }
                    if (string.IsNullOrEmpty(ProveImage.OldImageFront) || string.IsNullOrEmpty(ProveImage.OldImageBack))
                    {
                        if (!FieldChange.ChangeAddress)
                        {
                            result.Add(new ValidationResult("Field Required"));
                        }
                    }
                }
            }
            return result;
        }
    }

    public class FieldChange 
    {
        [Required]
        public bool ChangeCardID { get; set; }
        
        [Required]
        public bool ChangeIssueDate { get; set; }
        
        [Required]
        public bool ChangeCardIssuer { get; set; }
        
        [Required]
        public bool ChangeAddress { get; set; }
    } 
    public class FieldChangeRequest : GetUserInfoRequest
    {
        [Required]
        public bool ChangeCard { get; set; }

        [Required]
        public bool ChangeAddress { get; set; }  
        
        [Required]
        public bool ChangePhoneSms { get; set; }
    }

    public class NewImage
    {
        [Description("ảnh mặt trước CCCD/CMND Mới")]
        public string NewImageFront { get; set; }  
        
        [Description("ảnh mặt sau trước CCCD/CMND Mới")]
        public string NewImageBack { get; set; }
        
    }

    public class OldImage
    {
        [Description("Giấy tờ pháp lý liên quan đến CCCD/CMND Cũ")]
        public string ImageOther { get; set; }
        
        [Description("Mặt trước CMND/CCCD cũ ")]
        public string OldImageFront { get; set; } 
        
        [Description("Mặt sau CMND/CCCD cũ ")]
        public string OldImageBack { get; set; }
    }

    public class ChangePhoneCust : GetUserInfoRequest,IValidatableObject
    {

        [Description("Dịch vụ nhận tin nhắn sms 1:Đăng ký, 2:Hủy đăng ký, 3: Tay đổi")]
        [Required]
        public int OptionsSms { get; set; }
        
        /*[Description("Số điện thoại thực hiện chuyển đổi 1: là số SMS Và CC, 2 là số CC")]
        [Required]
        public int OptionsPhone { get; set; }*/
        
        [Description("Số điện thoại nhận SMS 01")]
        public string  PhoneChange_01 { get; set; }
        
        [Description("Số điện thoai CC 02")]
        public string  PhoneChange_02 { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var listVaid = new List<ValidationResult>();
            if (OptionsSms == 1)
            {
                if (string.IsNullOrEmpty(PhoneChange_01))
                {
                    listVaid.Add(new ValidationResult("Field PhoneChange_01 is Required."));
                }
            }

            if (OptionsSms == 3)
            {
                if (string.IsNullOrEmpty(PhoneChange_01) || string.IsNullOrEmpty(PhoneChange_02))
                {
                    listVaid.Add(new ValidationResult("Field PhoneChange_01 and PhoneChange_02 is Required."));
                }
            }
            return listVaid;
        }
    }

    public class ChangeCustInfo
    {
        public string Name { get; set; }
        public IList<int> Options { get; set; }
        public IList<string> Values { get; set; }
    }

    public class CustInfo
    {
        public int ChangeInfoID { get; set; }
        public string UserId { get; set; }
        public string AccountNo { get; set; }
        public string CustName { get; set; }
    }

    public class GetCustInfo044v2
    {
        public int khach_hang_id { get; set; }
        public string ma_khach_hang { get; set; }
        public int loai_khach_hang { get; set; }
        public string chi_nhanhid { get; set; }
        public string ma_chi_nhanh { get; set; }
        public string ten_khach_hang { get; set; }
        public DateTime ngay_sinh { get; set; }
        public string so_cmnd_passport { get; set; }
        public string giay_phep_kinh_doanh { get; set; }
        public DateTime ngay_cap { get; set; }
        public string noi_cap { get; set; }
        public string dia_chi_lien_lac { get; set; }
        public string so_dien_thoai { get; set; }
        public string dia_chi_email { get; set; }
        public string so_dien_thoai_mb { get; set; }
        public string muc_do_vip { get; set; }
        public string loai_tai_khoanid { get; set; }
        public string so_tai_khoan { get; set; }
        public string ma_quoc_tich { get; set; }
        public string ten_quoc_tich { get; set; }
        public string ma_kinh_doanh { get; set; }
    }
    public class AlreadyRegistered
    {
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string AccountType { get; set; }
        public string CardNo { get; set; }
        public string CardIssueDate { get; set; }
        public string CardIssuer { get; set; }
        public string Address { get; set; }
        public DateTime BirthDay { get; set; }
        public string Sex { get; set; }
    }

    public class CompareInfoCustRequest : GetUserInfoRequest
    {
        [Description("Tên KH")]
        [Required]
        public string FullName { get; set; }
        
        [Description("Số cmnd/passport")]
        [Required]
        public string CardID { get; set; }
        
        [Description("Giới Tính 1 là Nam, 2 là Nữ,3 là không xác định")]
        public int Sex { get; set; }
        
        [Description("Ngày sinh dd/MM/yyyy")]
        [Required]
        public string BirthDay { get; set; }
        
        [Description("Ngày cấp dd/MM/yyyy")]
        [Required]
        public string IssueDate { get; set; }
        
        [Description("Nơi cấp CMND/CCCD của KH")]
        [Required]
        public string CardIssuer { get; set; }
        
    }
    public class CompareInfoCustDBRequest : GetUserInfoRequest
    {
        [Description("Tên KH")]
        [Required]
        public string FullName { get; set; }
        
        [Description("Số cmnd/passport")]
        [Required]
        public string CardID { get; set; }
        
        [Description("Giới Tính 1 là Nam, 2 là Nữ,3 là không xác định")]
        public int? Sex { get; set; }
        
        [Description("Ngày sinh dd/MM/yyyy")]
        [Required]
        public DateTime BirthDay { get; set; }
        
        [Description("Ngày cấp dd/MM/yyyy")]
        [Required]
        public DateTime IssueDate { get; set; }
        
        [Description("Nơi cấp CMND/CCCD của KH")]
        [Required]
        public string CardIssuer { get; set; }
    }
    
    public class OpenAccountDocumentMarginExtend
    {
        // ma_khach_hang
        public string CustCode { get; set; }
        // so_tai_khoan 044c
        public string CustCode044c { get; set; }
        //ho ten kh
        public string FullName { get; set; }
        // ngay sinh
        public string Birthday { get; set; }
        //gioi tinh
        public string CardID { get; set; }
        //ngay cap cmnd
        public DateTime IssueDate { get; set; }
        public string CardIssue { get; set; }
        public string Address { get; set; }
        public string BranchId { set; get; }

    }

    public class getUserMargin
    {
        public string SaleID { get; set; }
        public string BranchName { get; set; }
        public string Creator { get; set; }
        public string Email { get; set; }
        
    }
    public class CancellChangeInfoCustRequest : BaseRequest
    {
        public int RegistServiceID { get; set; }
    }

    public class SendOTPCONFIRMRequest : BaseRequest
    {
        public string Phone { get; set; }
        public string Message { get; set; }
    }
    
    
    public class UserCRM
    {
        public  string FullName { get; set; }

        public string DateOfBirth
        {
            get
            {
                return DateOfBirthOn.ToString("dd/MM/yyyy");
            }
        }

        public  DateTime DateOfBirthOn { get; set; }
        public  string CardID { get; set; }

        public string IssueDate
        {
            get
            {
                return IssueDateOn.ToString("dd/MM/yyyy");
            }
        }

        public  DateTime IssueDateOn { get; set; }
        public  string CardIssue { get; set; }
        public  string TypeAccount { get; set; }
        public  string Email { get; set; }
    }

    public class GetInfoUserIdResult
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class GetInfoUserIdRequest : BaseRequest
    {
        public string SaleID { get; set; }
    }

    public class GetImageChangeInfoCustRequest : BaseEkycRequest
    {
        public string CardID { get; set; }
    }
    
    public class CheckOpenAccountInfoRequest : BaseEkycRequest {
        /// <summary>
        /// Email mo tai khoan
        /// </summary>
        [Required]
        public string Email { get; set; }
        
        /// <summary>
        /// So dien thoai
        /// </summary>
        [Required]
        public string Phone { get; set; }
    }

    public class StringeeRequest : BaseEkycRequest
    {
        [Required]
       public string userId { get; set; }
       [Required]
       public string phone { get; set; }
    }

    public class DistrictRequest : BaseEkycRequest
    {
        [Required]
        public string ProvinceCode { get; set; }
    }
    public class WardResult
    {
        public int WardID { get; set; }
        public string WardCode { get; set; }
        public string WardName { get; set; }
    }

    public class WardRequest : BaseEkycRequest
    {
        public string DistrictCode { get; set; }
    }
    
    
    
    

    public class GetImageChangeInfoResult : BaseEkycRequest
    {
        [Description("ảnh mặt trước CCCD/CMND Mới")]
        public string NewImageFront { get; set; }  
        
        [Description("ảnh mặt sau trước CCCD/CMND Mới")]
        public string NewImageBack { get; set; }
        
        [Description("Giấy tờ pháp lý liên quan đến CCCD/CMND Cũ")]
        public string ImageOther { get; set; }
        
        [Description("Mặt trước CMND/CCCD cũ ")]
        public string OldImageFront { get; set; } 
        
        [Description("Mặt sau CMND/CCCD cũ ")]
        public string OldImageBack { get; set; }
    }

    public class GetImageSignatureResult
    {
        public string DataSignature { get; set; }


    }
    public class ChangeInfoDTO
    {
        public string Name { get; set; }
        public IList<int> Options { get; set; }
        public IList<string> Values { get; set; }
    }

    public class ProvinceResult
    {
        public int ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public string ProvinceCode { get; set; }
    }
    public class DistrictResult
    {
        public int DistrictID { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
    }

    public class SaleIDRequest : BaseEkycRequest
    {
        public string saleID { get; set; }
    }

    public class SaleIDResult
    {
        [Description("Ten nhan vien moi gioi")]
        public string SaleName { get; set; }
    }

}