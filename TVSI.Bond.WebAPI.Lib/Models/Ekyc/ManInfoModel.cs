﻿using System;

namespace TVSI.Bond.WebAPI.Lib.Utility.Common.Data
{
    
    public class ManInfo : IComparable
    {
        private DateTime _ngay_tao;
        public ManInfo(string gia_tri, string ma_chi_nhanh)
        {
            this.gia_tri = gia_tri;
            this.ma_chi_nhanh = ma_chi_nhanh;
        }

        public string gia_tri { get; set; }

        public string ma_chi_nhanh { get; set; }
        public DateTime ngay_tao { get { return (this._ngay_tao); } }
        
        public int CompareTo(object obj)
        {
            if (obj is ManInfo)
            {
                ManInfo q = (ManInfo)obj;
                return this.ngay_tao.CompareTo(q.ngay_tao);
            }
            else
            {
                throw new ArgumentException("object is not a ManInfo");
            }
        }
    }
}