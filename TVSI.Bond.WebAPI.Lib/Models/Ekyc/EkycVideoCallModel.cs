﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.Bond.WebAPI.Lib.Models.Ekyc
{
    
    public class VideoCallHistRequest : BaseEkycRequest
    {
        [Description("Mã khác hàng")]
        [Required]
        public string UserID { get; set; }
        [Description("Số tài khoản")]
        [Required]
        public string AccountNo { get; set; }    
        [Description("Từ ngày ( Format: dd/MM/yyyy )")]
        [Required]
        public string FromDate { get; set; }     
        [Description("Đến ngày ( Format: dd/MM/yyyy )")]
        [Required]
        public string ToDate { get; set; }
    }

    public class CancelVideoCallScheduleRequest : BaseEkycRequest
    {
        [Description("Mã khác hàng")]
        public string UserID { get; set; }   
        [Description("Số tài khoản")]
        public string AccountNo { get; set; } 
        [Description("ID")]
        public long ScheduleID { get; set; } 
        
    }

    public class VideoCallScheduleRequest : BaseEkycRequest
    {
        [Description("Mã khác hàng")]
        [Required]
        public string UserID { get; set; }
        [Description("Số tài khoản")]
        [Required]
        public string AccountNo { get; set; }    
        [Description("Thời gian, Format: dd/MM/yyyy HH:mm:ss")]
        [Required]
        public string ScheduleTime { get; set; }  
        [Description("Nội dung")]
        [Required]
        public string Description { get; set; }   
    }
    public class VideoCallHistInfoRequest
    {
        [Description("ID")]
        public long VideoCallID { get; set; } 
        
        [Description("Trạng thái cuộc gọi, 99 Tvsi gọi cho bạn lỗi kết nối, " +
                     "-99 bạn gọi cho Tvsi lỗi kết nối," +
                     " -10 bạn đã từ chối cuộc gọi TVSI, 10 TVSI đã từ trối cuộc gọi của bạn, 1 là TVSI đã gọi cho bạn. thời gian..., -1 bạn đã gọi cho TVSI thời gian gọi..., 5 TVSI đã gọi nhỡ cho bạn ")]
        public int SourceType { get; set; }
        
        [Description("Thời gian , gọi. Format: dd/MM/yyyy HH :mm:ss")]
        public string VideoCallTime { get; set; }     
        
        [Description("Tổng thời gian gọi Format HH:mm:ss")]
        public string VideoCallTotalTime { get; set; }
        
        [Description("Trạng thái")]
        public int Status { get; set; }     
        [Description("Trạng thái mô tả")]
        public string StatusName { get; set; }
    }

    public class VideoCallScheduleHistInfoResult
    {
        [Description("ID")]
        public long ScheduleID { get; set; }     
        [Description("Thời gian Format: dd/MM/yyyy HH :mm:ss")]
        public string ScheduleTime { get; set; }   
        [Description("Thời gian đặt lịch gọi")]
        public string CallScheduleTime { get; set; }
        [Description("Nội dung")]
        public string Description { get; set; }      
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Trạng thái mô tả")]
        public string StatusName { get; set; } 
    }

    public class UpdateStatusVideoCallRequest : GetUserInfoRequest
    {
        [Required]
        [Description("Trạng thái cuộc gọi, 99 Tvsi gọi cho bạn lỗi kết nối, " +
                     "-99 bạn gọi cho Tvsi lỗi kết nối," +
                     " -10 bạn đã từ chối cuộc gọi TVSI, 10 TVSI đã từ trối cuộc gọi của bạn, 1 là TVSI đã gọi cho bạn. thời gian..., -1 bạn đã gọi cho TVSI thời gian gọi..., 5 TVSI đã gọi nhỡ cho bạn ")]
        public int StatusVideoCall { get; set; }
        
        [Required]
        [Description("Thời gian Format: dd/MM/yyyy HH:mm:ss")]
        public string CallDateTime { get; set; }
        
        [Description("Tổng thời gian gọi, khi mà trạng thái StatusVideoCall là 1 và -1, Format: HH:mm:ss")]
        public string TotalCallTime { get; set; }
        
    }
}