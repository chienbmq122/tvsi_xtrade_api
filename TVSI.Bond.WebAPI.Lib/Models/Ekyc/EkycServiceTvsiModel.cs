﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TVSI.Bond.WebAPI.Lib.Models.Ekyc
{
   public class RegistCustomerServiceResult : BaseEkycRequest
   {
      [Description("Mã Khách Hàng")]
      [Required]
      public string UserID { get; set; }
      
      [Description("Số tài khoản")]
      [Required]
      public string AccountNo { get; set; }
      
      [Description("Dịch vụ ứng trước tự động ( 0:Không đăng ký, 1: Đăng ký )")]
      [Required]
      public int AdvantageRegist { get; set; }
      
      [Description("Dịch vụ xác nhận lệnh trực tuyến ( 0:Không đăng ký, 1: Đăng ký )")]
      [Required]
      public int ConfirmOrderRegist { get; set; }
      
      [Description("Số điện thoại đăng ký nhận SMS 01")]
      public string SmsPhone_01 { get; set; }
      
      [Description("Số điện thoại đăng ký nhận SMS 02")]
      public string SmsPhone_02 { get; set; }
      
      [Description("Số điện thoại Contact Center 01")]
      public string CCPhone_01 { get; set; }
      
      [Description("Số điện thoại Contact Center 02")]
      public string CCPhone_02 { get; set; }
   }

   public class ChangeInfoHistRequest : VideoCallHistRequest
   {
      
      [Description("Thứ tự trang (tính từ 1)")]
      [Required]
      public int PageIndex { get; set; }
      [Description("Số bản ghi cần lấy trên một trang")]
      [Required]
      public int PageSize { get; set; }
      
   }

   public class CreateAccountMarginRequest : BaseRequest
   {
      public bool MarginAccount { get; set; }
   }

   public class GetImageByCardIdRequest : BaseEkycRequest
   {
      public string CardId { get; set; }
   }

   public class GetImageByCardIdResult
   {
      public string Front_Image { get; set; }
      public string Back_Image { get; set; }
      public string Face_Image { get; set; }
      public string Signature_Image_0 { get; set; }
      
      /*public string Signature_Image_1 { get; set; }
      public string Signature_Video_0 { get; set; }
      public string Signature_Video_1 { get; set; }*/
   }

   public class CheckAccountMarginResult
   {
      public string FullName { get; set; }
      public string CardID { get; set; }
      public string Code044C { get; set; }
      public string CustCode { get; set; }
      public string PhoneNumber { get; set; }
      public string Email { get; set; }
   }
   

   public class RegistServiceHistInfoResult
   {
      [Description("ID")]
      public int RegistServiceID { get; set; }
      [Description("Thời gian")]
      public string RegistTime { get; set; }
      
      [Description("Nội dung thay đổi")]
      [JsonIgnore]
      public string ContentChange { get; set; }
      [Description("Loại thay đổi")]
      public string RegistTypeName { get; set; }
      [Description("Loại thay đổi")]
      public string ChangeType { get; set; }
      [Description("Ngày hiệu lực")]
      public string EffectiveDate { get; set; }
      [Description("Trạng thái ")]
      public string StatusName { get; set; }
      [Description("Trạng thái Number")]
      public int StatusNameNum { get; set; }
      
      [Description("Lý do từ chối")]
      public string ReasonsReject { get; set; }
      
      [Description("kiểu thay đổi ")]
      public string StatusNameChange { get; set; }
      [Description("kiểu thay đổi num")]
      public int StatusNameChangeNum { get; set; }
      public List<ComboField> ContentChangeInfo { get; set; }
      
   }

   public class ComboField
   {
      public string FieldLabel { get; set; }
      public string DetailInfo { get; set; }
      public List<ComboValues> ComboValues { get; set; }
   }

   public class ComboValues
   {
      public string Name { get; set; }
      public object Values { get; set; }
   }







   public class GetServiceChangeInfoResult
   {
      
      [Description("SĐT Đã đăng ký sms với tvsi")]
      public string SmsPhone_01 { get; set; }
      
      [Description("SĐT chưa đăng ký sms với tvsi")]
      public string SmsPhone_02 { get; set; }
   }

   public class GetPhoneCustResult
   {
      [Description("SĐT Đã đăng ký sms với tvsi")]
      public string SmsPhone_01 { get; set; }

      [Description("SĐT chưa đăng ký sms với tvsi")]
      public List<string> SmsPhone_02 { get; set; } = new List<string>();
   }

   public class GetAdvanceRegister
   {
   }
   public class GetListPlaceOfIssueResult
   {
      public long Category { get; set; }
      public string CategoryName { get; set; }
      public string CategoryFullName { get; set; } = "";
   }

   public class OtherReRequest
   {
      public string SaleId { get; set; }
   }

   public class PhoneInnoInfoResult
   {
      public string ContactPhone { get; set; }
      public string CellPhone { get; set; }
      public string Phone { get; set; }
   }

   /*public class CardIdRequest : BaseEkycRequest
   {
      public string CardId { get; set; }
   }*/
}