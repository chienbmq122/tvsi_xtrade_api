﻿namespace TVSI.Bond.WebAPI.Lib.Models.Ekyc
{
    public class EkycThreadingModel
    {
        public int WorkTimeID { get; set; }
        public string SaleID { get; set; }
        public int Count { get; set; }
    }
    public class SaleInfoModel
    {
        public string Email { get; set; }
        public int DepartmentID { get; set; }
        public string SaleName { get; set; }
    }
    public class SaleData
    {
        public string SaleID { get; set; }
        public string BranchID { get; set; }
    }
}