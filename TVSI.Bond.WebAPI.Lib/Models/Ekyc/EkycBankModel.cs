﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.Bond.WebAPI.Lib.Models.Ekyc
{
    public class BankAccountRequest : BaseEkycRequest
    {
        [Description("Mã khách hàng")]
        [Required]
        public string UserID { get; set; }

        [Description("Số tài khoản")]
        [Required]
        public string AccountNo { get; set; }
    }

    public class BankModel
    {
        [Description("Mã ngân hàng")]
        public string BankNo { get; set; }
        [Description("Tên ngân hàng")]
        public string ShortName { get; set; }
    }
    public class ProvinceModelResult
    {
        public string ProvinceID { get; set; }
        public string ProvinceName { get; set; }
    }

    public class GetSubBranchList
    {
        public string BranchNo { get; set; }
        public string ShortBranchName { get; set; }
    }

    public class BankInfoListResult
    {
        [Description("ID")]
        public int BankAccountID { get; set; }
        [Description("Số tài khoản ngân hàng")]
        public string BankAccount { get; set; }
        [Description("Tên ngân hàng")]
        public string BankName { get; set; }
    }

    public class DeleteBankRequest : BankAccountRequest
    {
        [Description("ID")]
        [Required]
        public int BankAccountID { get; set; }  
    }
    

    public class AccNoDetailRequest : BankAccountRequest
    {
        [Description("ID")]
        public int BankAccountID { get; set; }
    }

    public class RegisterBankRequest : BankAccountRequest
    {
        [Description("Số tài khoản ngân hàng")]
        public string BankAccount { get; set; }
        [Description("Tên người thụ hưởng")]
        public string BankAccountName { get; set; } 
        [Description("Mã ngân hàng")]
        public string BankNo { get; set; }
        [Description("Tên ngân hàng")]
        public string BankName { get; set; }     
        [Description("Mã chi nhánh")]
        public string BranchNo { get; set; }     
        [Description("Tên chi nhánh")]
        public string BranchName { get; set; }   
        [Description("Tỉnh/Thành phố")]
        public string ProvinceName { get; set; }
    }
    

    public class BankDetailInfoResult : BankInfoListResult
    {
        [Description("Tên người thụ hưởng")]
        public string BankAccountName { get; set; }   
        [Description("Tỉnh/Thành phố")]
        public string ProvinceName { get; set; }   
        [Description("Trạng thái")]
        public int Status { get; set; }      
        [Description("Trạng thái mô tả")]
        public string StatusName { get; set; }
    }

    public class InternalAccountInfoResult
    {
        [Description("ID")]
        public int CustCode { get; set; }  
        [Description("Số tài khoản đối ứng")]
        public string InternalAccountNo { get; set; } 
        [Description("Tên tài khoản đối ứng")]
        public string InternalAccountName { get; set; }  
    }


    public class InternalAccountDetailInfoResult
    {
        [Description("ID")]
        public int CustCode { get; set; }  
        [Description("Số tài khoản đối ứng")]
        public string InternalCustomerNo { get; set; } 
        [Description("Tên tài khoản đối ứng")]
        public string InternalAccountName { get; set; }  
        [Description("Trạng thái")]
        public int Status { get; set; }      
        [Description("Tên trạng thái")]
        public string StatusName { get; set; }
    }
    public class InternalAccountDetailRequest : BankAccountRequest
    {
        [Description("ID")]
        [Required]
        public string InternalAccountNo { get; set; }  
    }
}