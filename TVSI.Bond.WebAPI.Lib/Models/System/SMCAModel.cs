﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.Bond.WebAPI.Lib.Models.System
{
    public class ChangePasswordRequest : BaseRequest
    {
        [Description("Password cũ")]
        [Required]
        public string OldPassword { get; set; }
        [Description("Password mới")]
        [Required]
        public string NewPassword { get; set; }
    }

    public class CheckPasswordRequest : BaseRequest
    {
        [Description("Password đã mã hóa cần check")]
        [Required]
        public string Password { get; set; }
    }  
    public class CheckPinRequest : BaseRequest
    {
        [Description("Password đã mã hóa cần check")]
        [Required]
        public string Pin { get; set; }
    }
}
