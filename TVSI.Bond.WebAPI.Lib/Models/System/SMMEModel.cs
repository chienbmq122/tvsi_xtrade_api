﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.Bond.WebAPI.Lib.Models.System
{
    public class SendSmsRequest : BaseRequest
    {
        [Description("Nội dung tin nhắn")]
        public string Message { get; set; }
        
        [Description("Ma Dich Vu")]
        public string ServiceCode { get; set; }
        
    }

    public class SendSmsRequest_02
    {
        [Description("Số điện thoại cần gừi")]
        public string Mobile { get; set; }

        [Description("Nội dung tin nhắn")]
        public string Message { get; set; }
    }

    public class SendOTPEmailRequest : BaseEkycRequest
    {
        [Description("Ma otp")]
        [Required]
        public string Otp { get; set; }
        
        [Description("Mã khách hàng")]
        [Required]
        
        public string CustCode { get; set; }
    }

    public class MobilePhoneData 
    {
        public string Mobile { get; set; }
        public string Mobile_02 { get; set; }
        
    }
    public class smsVNPAY
    {
        public string messageId { get; set; }
        public string destination { get; set; }
        public string sender { get; set; }
        public string keyword { get; set; }
        public string shortMessage { get; set; }
        public string encryptMessage { get; set; }
        public int isEncrypt { get; set; }
        public int type { get; set; }
        public long requestTime { get; set; }
        public string partnerCode { get; set; }
        public string sercretKey { get; set; }
    }
    
    public class smsModel
    {
        public int smsid { get; set; }
        public string noi_dung { get; set; }
        public string so_dien_thoai { get; set; }
        public string so_tai_khoan { get; set; }
        public string ma_dich_vu { get; set; }
        public int loai_tin_nhan { get; set; }
        public int priority { get; set; }
        public int kenh_gui_tin { get; set; }
        public DateTime thoi_gian_nhan { get; set; }
        public DateTime thoi_gian_gui { get; set; }
        public int trang_thai { get; set; }
        public string rowguid { get; set; }
        public string Mo_ta_loi { get; set; }
    }
    public class responseVNPAY
    {
        public string messageId { get; set; }
        public string status { get; set; }
        public string description { get; set; }
        public int isMnp { get; set; }
        public string providerId { get; set; }
    }
}
