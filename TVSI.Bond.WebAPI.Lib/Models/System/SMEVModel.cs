﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.Bond.WebAPI.Lib.Models.System
{
    public class SearchEventFilterRequest:BaseRequest
    {
        
    }

    #region Filter Data
    public class SearchEventFilterResult
    {
        public IEnumerable<EventFilterBondData> FilterBonds { get; set; }
        public IEnumerable<EventFilterTypeData> FilterTypes { get; set; }
    }

    public class EventFilterBondData
    {
        [Description("Mã Trái phiếu")]
        public string BondCode { get; set; }
        [Description("Tên Trái phiếu")]
        public string BondName { get; set; }
    }

    public class EventFilterTypeData
    {
        [Description("Event Type")]
        public int EventType { get; set; }

        [Description("Tên Event Type")]
        public string EventTypeName
        {
            get { return ResourceFile.SystemModule.ResourceManager.GetString("EventType_" + EventType); }
        }
    }
    #endregion

    #region Event List

    public class EventListRequest : BaseRequest
    {
        [Description("Thứ tự trang (tính từ 1)")]
        [Required]
        public int PageIndex { get; set; }
        [Description("Số bản ghi cần lấy trên một trang")]
        [Required]
        public int PageSize { get; set; }

        [Description("Mã Trái phiếu tìm kiếm")]
        public string BondCode { get; set; }
        [Description("Loại Event tìm kiếm")]
        public int EventType { get; set; }
    }

    public class EventListResult
    {
        [Description("Event ID")]
        public int EventID { get; set; }
        [Description("Ngày sự kiện")]
        public string EventDate { get; set; }
        [Description("Mã Trái phiếu")]
        public string BondCode { get; set; }
        [Description("Tóm tắt sự kiện")]
        public string Title { get; set; }
    }
    #endregion

    #region Event Detail

    public class EventDetailRequest : BaseRequest
    {
        [Description("Event ID")]
        public int EventID { get; set; }
    }

    public class EventDetailResult
    {
        [Description("Event ID")]
        public int EventID { get; set; }

        [Description("Event Type")]
        public int EventType { get; set; }

        [Description("Tên Event Type")]
        public string EventTypeName
        {
            get { return ResourceFile.SystemModule.ResourceManager.GetString("EventType_" + EventType); }
        }

        [Description("Ngày sự kiện")]
        public string EventDate { get; set; }

        [Description("Mã Trái phiếu")]
        public string BondCode { get; set; }

        [Description("Tóm tắt sự kiện")]
        public string Title { get; set; }

        [Description("Mô tả sự kiện")]
        public string Description { get; set; }
    }

    #endregion
}
