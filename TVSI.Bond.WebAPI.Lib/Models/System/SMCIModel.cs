﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.Bond.WebAPI.Lib.Models.System
{
    public class ChangePinRequest : BaseRequest
    {
        [Description("Mã Pin cũ")]
        [Required]
        public string OldPin { get; set; }
        [Description("Mã Pin mới")]
        [Required]
        public string NewPin { get; set; }
    }
}
