﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Bond.WebAPI.Lib.Models.System
{
    public class ForgotPasswordRequest : BaseRequest
    {
        [Description("CMND,Hộ Chiếu, Mã số công ty")]
        [Required]
        public string CardId { get; set; }
    }


    public class ForgotPINRequest : BaseRequest
    {
        [Description("CMND,Hộ Chiếu, Mã số công ty")]
        [Required]
        public string CardId { get; set; }
    }

}
