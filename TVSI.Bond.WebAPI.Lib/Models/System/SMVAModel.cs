﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Bond.WebAPI.Lib.Models.System
{
    public class VerifyAccountRequest : BaseRequest
    {
        [Description("Email khách hàng")]
        [Required]
        public string Email { get; set; }
    }
}
