﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Bond.WebAPI.Lib.Models.System
{
    public class ConfirmCodeRequest : BaseRequest
    {
        [Description("Mã code kích hoạt nhận mật khẩu hoặc pin")]
        [Required]
        public string ConfirmCode { get; set; }
    }




    public class CustInfo
    {
        [Description("Mã khách hàng")]
        public string CustID { get; set; }

        [Description("Tên khách hàng")]
        public string CustName { get; set; }

        [Description("CMNN/CCCD")]
        public string CardId { get; set; }

        [Description("Email")]
        public string Email { get; set; }
    }
}
