﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.Bond.WebAPI.Lib.Models.System
{
    public class UserLoginRequest
    {
        [Required]
        [Description("Tên đăng nhập")]
        public string UserId { get; set; }

        [Description("Mật khẩu")]
        [Required]
        public string Password { get; set; }
    }

    /// <summary>
    /// Thông tin đăng nhập trả về
    /// </summary>
    public class UserLoginResult : BaseResult
    {
        [Description("Tên đăng nhập")]
        public string UserId { get; set; }

        [Description("Số tài khoản")]
        public string AccountNo { get; set; }

        [Description("Tên tài khoản")]
        public string AccountName { get; set; }

        [Description("Cờ đánh dấu tài khoản @")]
        public bool IsAlphaAccount { get; set; }

        [Description("Loại tài khoản: 0-Khách hàng, 1: Quyền View, 2: Quyền View và đặt lênh, 3: Full Quyền")]
        public int UserType { get; set; }

        [Description("Cờ đánh dấu được phép sử dụng Module CCQ")]
        public bool IsAllowFund { get; set; }
        [Description("Cờ đánh dấu được phép sử dụng Module Trái phiếu")]
        public bool IsAllowBond { get; set; } = true;
        [Description("Cờ đánh dấu tài khoản mặc định")]
        public bool IsDefault { get; set; }
    }

    public class CurrentTradeDateRequest : BaseRequest { }

    public class CurrentTradeDateResult : BaseResult
    {
        [Description("Ngày giao dịch")]
        public string CurTradeDate { get; set; }
    }

    public class TVSI_sDANH_MUC_QUYEN_ByTenTruyCap_Result
    {
        public int IsAllBranch { get; set; }

        public int IsApproval { get; set; }

        public int IsCreate { get; set; }

        public int IsDelete { get; set; }

        public int IsEdit { get; set; }

        public int IsFind { get; set; }

        public int IsPrint { get; set; }

        public int IsRead { get; set; }

        public int IsRekey { get; set; }

        public string ma_chuc_nang { get; set; }

        public string ten_chuc_nang { get; set; }
    }
}
