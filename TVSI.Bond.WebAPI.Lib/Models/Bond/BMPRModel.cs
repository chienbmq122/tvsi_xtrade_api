﻿using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.Bond
{
    public class ProductInfoRequest : BaseRequest
    {
                
    }

    public class ProductInfoResult
    {
        [Description("Text giới thiệu")]
        public string Text_Introduce { get; set; }
        [Description("Text header")]
        public string Text_SectionHeader_01 { get; set; }
        [Description("Text giới thiệu")]
        public string Text_Item_01 { get; set; }
        [Description("Text giới thiệu")]
        public string Text_Item_02 { get; set; }
        [Description("Text giới thiệu")]
        public string Text_Item_03 { get; set; }
        [Description("Text header")]
        public string Text_SectionHeader_02 { get; set; }
        [Description("Sản phẩm 01")]
        public string Text_Item_04 { get; set; }
        [Description("Sản phẩm 02")]
        public string Text_Item_05 { get; set; }
        [Description("Sản phẩm 03")]
        public string Text_Item_06 { get; set; }
        [Description("Sản phẩm 04")]
        public string Text_Item_07 { get; set; }

    }    

    public class ProductTypeRequest : BaseRequest
    {
    }

    public class ProductTypeResult
    {
        [Description("Loại sản phẩm trái phiếu")]
        public int BondType { get; set; }

        [Description("Tên sản phẩm")]
        public string BondTypeName { get; set; }

        [Description("Tên viết tắt")]
        public string ShortName { get; set; }

        [Description("Mô tả")]
        public string Description { get; set; }

        [Description("Hiển thị sản phẩm")]
        public bool IsInfo { get; set; }
        [Description("Hiển thị màn hình đầu tư")]
        public bool IsInvest { get; set; }

    }
}
