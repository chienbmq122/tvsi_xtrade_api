﻿using System.ComponentModel;


namespace TVSI.Bond.WebAPI.Lib.Models.Bond
{
    public class OutrightBoardRequest : BaseRequest
    {

    }

    public class OutRightBoardCodeRequest : BaseRequest
    {
        [Description("Mã trái phiếu tìm kiếm")]
        public string SearchBondCode { get; set; }
    }

    public class OutrightBoardResult
    {
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Tên tổ chức phát hành")]
        public string Issuer { get; set; }

        [Description("Kỳ trả lãi")]
        public int Term { get; set; }
        [Description("Lãi suất")]
        public decimal RateBasic { get; set; }
        [Description("Thời gian còn lại")]
        public decimal MonthRemain { get; set; }
        [Description("Lãi suất đầu tư (%/năm)")]
        public decimal Rate { get; set; }
        [Description("Số tiền đầu tư")]
        public decimal Amount { get; set; }
        [Description("Số tiền đầu tư")]
        public long RemainVolume { get; set; }

    }

    public class OutRightListRequest : BaseRequest
    {
        
    }

    public class OutRightListResult
    {
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Tổ chức phát hành")]
        public string Issuer { get; set; }
    }
}
