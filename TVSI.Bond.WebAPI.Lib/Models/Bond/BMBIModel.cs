﻿using System.ComponentModel;


namespace TVSI.Bond.WebAPI.Lib.Models.Bond
{
    public class BondInfoDetailRequest : BaseRequest
    {
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
    }

    public class BondInfoDetailResult
    {
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Tên trái phiếu")]
        public string BondName { get; set; }
        [Description("Loại hình trái phiếu")]
        public string BondTypeName { get; set; }
        [Description("Mệnh giá")]
        public string ParValue { get; set; }
        [Description("Ngày phát hành")]
        public string IssueDate { get; set; }
        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }
        [Description("Lãi suất Coupon")]
        public decimal CouponRate { get; set; }
        [Description("Kỳ tính lãi")]
        public int Term { get; set; }
        [Description("Mô tả")]
        public string Description { get; set; }
        [Description("Đại lý phát hành")]
        public string AgencyIssuer { get; set; }
        [Description("Kỳ tính lãi hiện tại")]
        public string CurCouponTerm { get; set; }
        [Description("Loại trái phiếu")]
        public int BondType { get; set; }

    }

    

    public class BuyProductInfo
    {
        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }
        [Description("Tên sản phẩm")]
        public string ProductName { get; set; }
        [Description("Mô tả")]
        public string ProductDescription { get; set; }
    }

    public class BuyOrderTypeInfo
    {
        [Description("Loại lệnh")]
        public int OrderType { get; set; }
        [Description("Tên loại lệnh")]
        public string OrderTypeName { get; set; }
        [Description("Mô tả")]
        public string OrderTypeDescription { get; set; }
    }

    public class IssuerGroupInfoRequest : BaseRequest {}

    public class IssuerGroupInfoResult
    {
        public string IssuerGroup { get; set; }
        public string BondTypeGroup { get; set; }
        public string IssuerGroupName { get; set; }
    }
}
