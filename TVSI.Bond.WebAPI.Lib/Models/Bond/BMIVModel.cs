﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.Bond
{
    public class BuyInfoRequest : BaseRequest
    {       
    }

    public class BuyInfoResult
    {
        [Description("Số tiền có thể rút")]
        public decimal Withdrawal { get; set; }
        [Description("DS sản phẩm TP có thể mua")]
        public List<BuyProductInfo> ProductInfoList { get; set; }
    }

    public class ProductTermRequest : BaseRequest
    {
        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }
        [Description("Số tiền")]
        public decimal Amount { get; set; }
        [Description("Ngày mua")]
        public string TradeDate { get; set; }
    }


    public class ProductPayTermRequest : BaseRequest
    {
        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }
        [Description("Số tiền")]
        public decimal Amount { get; set; }
        [Description("Kỳ hạn")]
        public int Term { get; set; }

        [Description("Ngày mua")]
        public string TradeDate { get; set; }
    }

    public class ProductPayTermResult
    {
        [Description("Thời gian nhận mức sinh lời")]
        public int PayTerm { get; set; }
        [Description("Mô tả")]
        public string PayTermNote { get; set; }
        [Description("Cho phép thay đổi nút bán trước hạn")]
        public bool IsLocked { get; set; }

        [Description("Giá trị mặc định cho nút bán trước hạn")]
        public bool IsPreSale { get; set; }
    }

    public class ProductTermResult
    {
        [Description("Kỳ hạn")]
        public int Term { get; set; }
        [Description("Mô tả kỳ hạn")]
        public string TermNote { get; set; }

        [Description("Thời gian nhận mức sinh lời")]
        public DateTime MaturityDate { get; set; }

        [Description("Cho phép thay đổi nút bán trước hạn")]
        public bool IsLocked { get; set; }

        [Description("Giá trị mặc định cho nút bán trước hạn")]
        public bool IsPreSale { get; set; }

        [Description("Cho phép sửa ngày đáo hạn không")]
        public bool IsChangeMaturityDate { get; set; }
        [Description("Ngày bán tối thiếu")]
        public DateTime MinSellDate { get; set; }
    }

    public class FindBondRequest : BaseRequest
    {
        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }
        [Description("Số tiền")]
        public decimal Amount { get; set; }
        [Description("Ngày mua")]
        public string TradeDate { get; set; }

        [Description("Kỳ hạn")]
        public int Term { get; set; }

        [Description("Bán trước hạn hay không")]
        public bool IsPreSale { get; set; }

        [Description("Thời gian nhận mức sinh lời")]
        public int PayTerm { get; set; }
    }

    public class BuyCalRateRequest : BaseRequest
    {
        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }
        [Description("Số tiền")]
        public decimal Amount { get; set; }
        [Description("Ngày mua")]
        public string TradeDate { get; set; }

        [Description("Kỳ hạn")]
        public int Term { get; set; }

        [Description("Bán trước hạn hay không")]
        public bool IsPreSale { get; set; }

        [Description("Thời gian nhận mức sinh lời")]
        public int PayTerm { get; set; }

        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        
        [Description("Định danh trái phiếu")]
        public string SubBondCode { get; set; }

        [Description("Ngày bán (đối với trường hợp chọn ngày bán")]
        public string SellDate { get; set; }
    }

    public class CustBankContract_ConfirmResult
    {
        public string RetCode { get; set; }
    }

    public class CustBankContract_ActiveContractItemResult
    {
        public List<CustBankContract_ActiveContractItem> Items { get; set; }
        public List<CalCustBankInfo> CustBankInfos { get; set; }
    }

    public class CustBankContract_ActiveContractItem
    {
        public int CustBankID { get; set; }
        public string BankName { get; set; }
        public string BankAccount { get; set; }
        public string BankAccountName { get; set; }
        public string SubBranchName { get; set; }
        public List<CustBankContract_ActiveContractModel> ContractInfos { get; set; }
    }

    public class CustBankContract_ActiveContractModel
    {
        public string AccountNo { get; set; }
        public int ContractID { get; set; }
        public string ContractNo { get; set; }
        public string IssuerCode { get; set; }
        public long Volume { get; set; }
        public DateTime MaturityDate { get; set; }
        public int PaymentMethod { get; set; }
        public int CustBankID { get; set; }
        public string BankName { get; set; }
        public string BankAccount { get; set; }
        public string BankAccountName { get; set; }
        public string SubBranchName { get; set; }
        public string ProductName { get; set; }
        public string Amount { get; set; }
    }

    public class FindBondRequestResult
    {
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Tên trái phiếu")]
        public string BondName { get; set; }
        public string SubBondCode { get; set; }
        public string MaxInvest { get; set; }
        public string HaveBond { get; set; }
        public string RemainTime { get; set; }
        public string Price { get; set; }
        public bool IsShowBeforeRate { get; set; }
    }

    public class BuyCalRateResult
    {
        [Description("Mã trái phiếu được áp dụng")]
        public CalBondInfo CalBondInfo { get; set; }
        [Description("Danh sách TK ngân hàng của TK")]
        public List<CalCustBankInfo> CalCustBankInfos { get; set; }
    }

    public class RedeemCodeInfo
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }
        [Description("Mã đổi thưởng, ưu đãi")]
        public string RedeemCode { get; set; }
        public decimal RedeemValue { get; set; }
    }

    public class CalBondInfo
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }
        [Description("Mã trái phiếu được áp dụng")]
        public string SubBondCode { get; set; }

        [Description("Số tiền đầu tư thực tế")]
        public decimal Amount { get; set; }

        [Description("Khối lượng mua thực tế")]
        public long Volume { get; set; }
        [Description("Tỷ suất lợi nhuận dự kiến")]
        public decimal FirstRate { get; set; }
        [Description("Coupon Rate")]
        public decimal CouponRate { get; set; }
        [Description("Tỷ suất lợi nhuận nhận được")]
        public decimal RealRate { get; set; }
        [Description("Sale ID")]
        public string SaleID { get; set; }
        [Description("Tên Sale")]
        public string SaleName { get; set; }
    }

    public class SellBeforeRateRequest : BaseRequest
    {
        public string RedeemCode { get; set; }
        public string SubBondCode { get; set; }
        public string TradeDate { get; set; }
    }

    public class SellBeforeRateInfo
    {
        public int FromDays { get; set; }
        public int? ToDays { get; set; }
        public decimal Rate { get; set; }
    }

    public class CalCustBankInfo
    {
        [Description("ID")]
        public string CustBankID { get; set; }

        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên chủ TK ngân hàng")]
        public string BankAccountName { get; set; }
        [Description("Tên ngân hàng")]
        public string BankName { get; set; }
        [Description("Tên chi nhánh")]
        public string SubBranchName { get; set; }
        [Description("Tỉnh/Thành phố")]
        public string Province { get; set; }

    }

    public class CalBondCode
    {
        [Description("Mã TP")]
        public string BondCode { get; set; }
        [Description("Tổ chức phát hành")]
        public string Issuer { get; set; }
    }

    public class FindBuyInfoRequest : BaseRequest
    {
        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }
        [Description("Số tiền")]
        public decimal Amount { get; set; }
        [Description("Ngày mua")]
        public DateTime TradeDate { get; set; }

        [Description("Kỳ hạn")]
        public int Term { get; set; }

        [Description("Bán trước hạn hay không")]
        public bool IsPreSale { get; set; }

        [Description("Thời gian nhận mức sinh lời")]
        public int PayTerm { get; set; }

        [Description("Mã trái phiếu")]
        public string BondCOde { get; set; }

        [Description("Ngày bán (đối với trường hợp chọn ngày bán")]
        public DateTime? SellDate { get; set; }

    }

    public class CheckRedeemCodeRequest: BaseRequest
    {
        [Description("Mã ưu đãi")]
        public string RedeemCode { get; set; }

        [Description("Ngày giao dịch")]
        public string TradeDate { get; set; }
        public string SubBondCode { get; set; }
        public long Volume { get; set; }
        public int RedeemType { get; set; }
    }

    public class FindBuyInfoResult
    {
        [Description("Tổ chức phát hành")]
        public string Issuer { get; set; }
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Ngày phát hành")]
        public string IssueDate { get; set; }
        [Description("Ngày đáo hạn")]
        public string MaturityDate { get; set; }
        [Description("Ngày mua")]
        public string BuyDate { get; set; }
        [Description("Số lượng mua")]
        public long Volume { get; set; }
        [Description("Đơn giá mua")]
        public decimal Price { get; set; }
        [Description("Dòng tiền KH nhận")]
        public List<CashData> CashDataInfo { get; set; }
    }

    public class CashData
    {
        [Description("Mô tả")]
        public string Description { get; set; }
        [Description("Ngày nhận")]
        public string ReceiveDate { get; set; }
        [Description("Tiền nhận")]
        public string Amount { get; set; }
        public string RowCode { get; set; }
    }

    public class ValidateBuyInfoRequest : BaseRequest
    {
        [Description("Thông tin mua")]
        public BuyDataRequest BuyInfo;
    }

    public class ConfirmBuyInfoRequest : BaseRequest
    {
        [Description("Thông tin mua")]
        public BuyDataRequest BuyInfo;
    }

    public class PrintCodeData
    {
        public string Code { get; set; }
        public string CodeValue { get; set; }
        public string DataType { get; set; }
        public string IsReadByWord { get; set; }
    }

    public class PrintContractDataInfo
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }

    public class BondPriceCalc
    {
        public string Description { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        public decimal Income { get; set; }

        public decimal? Interest { get; set; }

        public decimal? TotalIncome { get; set; }

        public decimal? ReinvestRate { get; set; }

        public int? UnitType { get; set; }

        public string Unit { get; set; }

        public string RowCode { get; set; }
    }


    public class ConfirmBuyInfoResult
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }
        [Description("Ten khach hang")]
        public string CustName { get; set; }
        [Description("SaleID")]
        public string SaleID { get; set; }
        [Description("Mã trái phiếu")]
        public string Issuer { get; set; }
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Mã TP chi tiết")]
        public string SubBondCode { get; set; }
        [Description("Ngày mua")]
        public DateTime TradeDate { get; set; }
        [Description("Ngày bán")]
        public DateTime SellDate { get; set; }
        [Description("Khối lượng")]
        public long Volume { get; set; }

        [Description("Giá mua")]
        public decimal Price { get; set; }
        [Description("Giá trị mua")]
        public decimal Amount { get; set; }

        [Description("Giá bán")]
        public decimal SellPrice { get; set; }
        [Description("Giá bán trước thuế")]
        public decimal SellPriceAmount { get; set; }
        [Description("Thuế")]
        public decimal PIT { get; set; }
        [Description("Tiền bán thực nhận")]
        public decimal NetAmount { get; set; }
        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên TK")]
        public string BankAccountName { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Chi nhánh ngân hàng")]
        public string SubBranchName { get; set; }

        [Description("Thông tin dòng tiền")]
        [JsonIgnore]
        public string DataPriceInfo { get; set; }
        [Description("Thông tin dòng tiền")]
        public List<CashData> CashDataInfo
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<CashData>>(DataPriceInfo);
                }
                catch
                {
                    return null;
                }
            }
        }
    }

    public class BuyDataRequest
    {
        [Description("Số Tài khoản")]
        public string AccountNo { get; set; }
        [Description("Mã TP")]
        public string BondCode { get; set; }
        [Description("Mã TP con")]
        public string SubBondCode { get; set; }
        [Description("KL")]
        public long Volume { get; set; }
        [Description("Ngày giao dịch")]
        public string TradeDate { get; set; }
        [Description("Lãi suất thực tế")]
        public decimal Rate { get; set; }
        public string RedeemCode { get; set; }

        [Description("Ngày bán (đối với trường hợp chọn ngày bán")]
        public string SellDate { get; set; }
        public int CustBankID { get; set; }
        public bool IsHardCopy { get; set; }
    }

    public class BuyDataResult
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }
        [Description("Tổ chức phát hành")]
        public string Issuer { get; set; }
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Mã TP con")]
        public string SubBondCode { get; set; }
        [Description("Ngày phát hành")]
        public DateTime IssueDate { get; set; }

        [Description("Ngày đáo hạn")]
        public DateTime MaturityDate { get; set; }

        [Description("Ngày mua")]
        public DateTime TradeDate { get; set; }
        [Description("Ngày bán")]
        public DateTime SellDate { get; set; }
        [Description("Khối lượng")]
        public long Volume { get; set; }

        [Description("Giá mua")]
        public decimal Price { get; set; }
        [Description("Giá trị mua")]
        public decimal Amount { get; set; }

        [Description("Giá bán")]
        public decimal SellPrice { get; set; }
        [Description("Giá bán trước thuế")]
        public decimal SellPriceAmount { get; set; }
        
        [Description("Thuế")]
        public decimal Interest { get; set; }
        [Description("Thuế")]
        public decimal PIT { get; set; }
        [Description("Tiền bán thực nhận")]
        public decimal NetAmount { get; set; }
        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên TK")]
        public string BankAccountName { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Chi nhánh ngân hàng")]
        public string SubBranchName { get; set; }

        [Description("Thông tin dòng tiền")]
        [JsonIgnore]
        public string DataPriceInfo { get; set; }
        [Description("Thông tin dòng tiền")]
        public List<CashData> CashDataInfo {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<CashData>>(DataPriceInfo);
                }
                catch
                {
                    return null;
                }
            }
        }
    }

    public class SellInfo
    {
        [Description("Mã Trái phiếu")]
        public string BondCode { get; set; }
        [Description("Ngày phát hành")]
        public string IssueDate { get; set; }
        [Description("Khối lượng")]
        public long Volume { get; set; }
        [Description("Giá bán")]
        public decimal Price { get; set; }
        [Description("Lãi suất ban đầu")]
        public decimal FirstAmount { get; set; }
        [Description("Thuế TNCN")]
        public decimal Tax { get; set; }
        [Description("Lãi suất thực tế")]
        public decimal RealAmount { get; set; }
        [Description("Ngày bán")]
        public string SellDate { get; set; }
        [Description("TK nhận tiền")]
        public string ReceiveAccountNo { get; set; }
        [Description("Dòng tiền (Lịch thanh toán)")]
        public CashData CashDataInfo { get; set; }
    }

    public class ValidateSellInfoRequest : BaseRequest
    {
        [Description("Số HĐ")]
        public string ContractNo { get; set; }
        [Description("Ngày bán")]
        public string TradeDate { get; set; }
        [Description("KL")]
        public long Volume { get; set; }

        [Description("RedeemCode")]
        public string RedeemCode { get; set; }

        [Description("ID ngân hàng")]
        public int CustBankID { get; set; }
    }

    public class ValidateSellInfoResult
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }

        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Mã TP con")]
        public string SubBondCode { get; set; }
        [Description("Ngày mua")]
        public DateTime BuyDate { get; set; }
        [Description("Ngày bán")]
        public DateTime TradeDate { get; set; }
        [Description("KL")]
        public long Volume { get; set; }

        [Description("Giá bán")]
        public decimal SellPrice { get; set; }
        [Description("Giá bán trước thuế")]
        public decimal SellPriceAmount { get; set; }
        [Description("Thuế")]
        public decimal PIT { get; set; }
        [Description("Lợi tức sau thuế")]
        public decimal Interest { get; set; }
        [Description("Phí")]
        public decimal Fee  { get; set; }
        [Description("Tiền bán thực nhận")]
        public decimal NetAmount { get; set; }
        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên TK")]
        public string BankAccountName { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Chi nhánh ngân hàng")]
        public string SubBranchName { get; set; }

        [Description("Thông tin dòng tiền")]
        [JsonIgnore]
        public string DataPriceInfo { get; set; }
        [Description("Thông tin dòng tiền")]
        public List<CashData> CashDataInfo
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<CashData>>(DataPriceInfo);
                }
                catch
                {
                    return null;
                }
            }
        }
    }

    public class ConfirmSellInfoResult
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }

        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Mã TP con")]
        public string SubBondCode { get; set; }
        [Description("Ngày mua")]
        public DateTime BuyDate { get; set; }
        [Description("Ngày bán")]
        public DateTime TradeDate { get; set; }
        [Description("KL")]
        public long Volume { get; set; }

        [Description("Giá bán")]
        public decimal SellPrice { get; set; }
        [Description("Giá bán trước thuế")]
        public decimal SellPriceAmount { get; set; }
        [Description("Thuế")]
        public decimal PIT { get; set; }
        [Description("Phí")]
        public decimal Fee { get; set; }

        [Description("Tổng lợi tức nhận được")]
        public decimal Interest { get; set; }

        [Description("Tiền bán thực nhận")]
        public decimal NetAmount { get; set; }
        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên TK")]
        public string BankAccountName { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Chi nhánh ngân hàng")]
        public string SubBranchName { get; set; }

        [Description("Thông tin dòng tiền")]
        [JsonIgnore]
        public string DataPriceInfo { get; set; }
        [Description("Thông tin dòng tiền")]
        public List<CashData> CashDataInfo
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<CashData>>(DataPriceInfo);
                }
                catch
                {
                    return null;
                }
            }
        }
    }

    public class FindSellContractInfo : BaseRequest
    {
        [Description("Số HĐ")]
        public string ContractNo { get; set; }
        [Description("Ngày bán")]
        public string TradeDate { get; set; }
    }

    public class FindRollContractInfo : BaseRequest
    {
        [Description("Số HĐ")]
        public string ContractNo { get; set; }
    }

    public class CalcSellContractInfo : BaseRequest
    {
        [Description("Số HĐ")]
        public string ContractNo { get; set; }
        [Description("Ngày bán")]
        public string TradeDate { get; set; }
        [Description("KL")]
        public long Volume { get; set; }

        [Description("RedeemCode")]
        public string RedeemCode { get; set; }
    }

    public class CalcRollContractInfo : BaseRequest
    {
        [Description("Số HĐ")]
        public string ContractNo { get; set; }

        [Description("RedeemCode")]
        public string RedeemCode { get; set; }
    }

    public class CalcSellContractInfoResult
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }
        [Description("Mã hợp đồng")]
        public string ContractNo { get; set; }
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        public long Volume { get; set; }
        [Description("Giá bán")]
        public decimal Price { get; set; }
        [Description("Lãi suất")]
        public decimal Rate { get; set; }
        [Description("Ngày bán")]
        public string TradeDate { get; set; }

        [Description("Tổng lợi tức nhận được")]
        public decimal Interest { get; set; }

        [Description("Tổng thực nhận")]
        public decimal NetAmount { get; set; }
    }
    public class CalcRollContractInfoResult
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }
        [Description("Mã hợp đồng")]
        public string ContractNo { get; set; }
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }

        [Description("Giá bán")]
        public decimal Price { get; set; }
        [Description("Lãi suất ban đầu")]
        public decimal Rate { get; set; }
        [Description("Ngày roll")]
        public string TradeDate { get; set; }

        [Description("Ngày bán")]
        public string MaturityDate { get; set; }

        [Description("Lợi tức nhận được")]
        public decimal NetAmount { get; set; }

        [Description("Danh sách TK ngân hàng của TK")]
        public List<CalCustBankInfo> CalCustBankInfos { get; set; }
    }
    public class FindSellContractInfoResult
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }
        [Description("Mã hợp đồng")]
        public string ContractNo { get; set; }
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        public long Volume { get; set; }
        [Description("Giá bán")]
        public decimal Price { get; set; }
        [Description("Lãi suất ban đầu")]
        public decimal Rate { get; set; }
        [Description("Ngày bán")]
        public string TradeDate { get; set; }

        [Description("Tổng lợi tức nhận được")]
        public decimal Interest { get; set; }

        [Description("Tổng thực nhận")]
        public decimal NetAmount { get; set; }

        [Description("Danh sách TK ngân hàng của TK")]
        public List<CalCustBankInfo> CalCustBankInfos { get; set; }
        [Description("ID thông tin ngân hàng")]
        public int CustBankID { get; set; }
    }
    public class FindRollContractInfoResult
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }
        [Description("Mã hợp đồng")]
        public string ContractNo { get; set; }
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }

        [Description("Kỳ hạn")]
        public int Term { get; set; }
        [Description("Khối lượng")]
        public long Volume { get; set; }

        [Description("Giá bán")]
        public decimal Price { get; set; }
        [Description("Lãi suất ban đầu")]
        
        public decimal Rate { get; set; }
        [Description("Ngày roll")]
        public string TradeDate { get; set; }

        [Description("Ngày bán")]
        public string MaturityDate { get; set; }

        [Description("Lợi tức nhận được")]
        public decimal NetAmount { get; set; }

        [Description("Danh sách TK ngân hàng của TK")]
        public List<CalCustBankInfo> CalCustBankInfos { get; set; }
        [Description("ID thông tin ngân hàng")]
        public int CustBankID { get; set; }
    }
    public class SellContractInfoResult
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }
        [Description("Mã hợp đồng")]
        public string ContractNo { get; set; }
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Khối lượng")]
        public long Volume { get; set; }
        [Description("Giá bán")]
        public decimal Price { get; set; }
        [Description("Lãi suất ban đầu")]
        public decimal Rate { get; set; }

        [Description("Ngày bán")]
        public string SellDate { get; set; }
        [Description("TK nhận tiền")]
        public string BankAccountNo { get; set; }
        [Description("Tên TK")]
        public string BankAccountName { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Chi nhánh ngân hàng")]
        public string SubBranchName { get; set; }

        [Description("Thông tin dòng tiền")]
        [JsonIgnore]
        public string DataPriceInfo { get; set; }
        [Description("Thông tin dòng tiền")]
        public List<CashData> CashDataInfo
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<CashData>>(DataPriceInfo);
                }
                catch
                {
                    return null;
                }
            }
        }
    }
    public class ConfirmSellInfoRequest : BaseRequest
    {
        [Description("Số HĐ")]
        public string ContractNo { get; set; }
        [Description("Ngày bán")]
        public string TradeDate { get; set; }
        [Description("KL")]
        public long Volume { get; set; }

        [Description("RedeemCode")]
        public string RedeemCode { get; set; }

        [Description("ID ngân hàng")]
        public int CustBankID { get; set; }

        [Description("Lấy bản cứng hay không")]
        public bool IsHardCopy { get; set; }
    }

    public class CustBankContract_GetActiveContract : BaseRequest
    {
        public int BondTypeGroup { get; set; }
    }

    public class CustBankContract_GetListRequest: BaseRequest
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class CustBankContract_GetDetailRequest : BaseRequest
    {
        public int id { get; set; }
    }

    public class CustBankContract_GetListResult
    {
        public int id { get; set; }
        public string AccountNo { get; set; }
        public DateTime TradeDate { get; set; }
        public string BondTypeGroupName { get; set; }
        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên TK")]
        public string BankAccountName { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Chi nhánh ngân hàng")]
        public string SubBranchName { get; set; }
        public string Status { get; set; }
    }

    public class CustBankContract_GetDetailResult
    {
        public string AccountNo { get; set; }
        public string PaymentMethod { get; set; }
        public string BondTypeGroupName { get; set; }
        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên TK")]
        public string BankAccountName { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Chi nhánh ngân hàng")]
        public string SubBranchName { get; set; }
        [JsonIgnore]
        public string DataDetails { get; set; }
        public List<CustBankContract_Detail> Details
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<CustBankContract_Detail>>(DataDetails);
                }
                catch
                {
                    return null;
                }
            }
        }
         public string Status { get; set; }
    }

    public class CustBankContract_Detail
    {
        public string ContractNo { get; set; }
        [JsonIgnore]
        public int PaymentMethod { get; set; }

        [Description("Số TK ngân hàng")]
        public string BankAccountNo
        {
            get; set;
        }
        [Description("Tên TK")]
        public string BankAccountName { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Chi nhánh ngân hàng")]
        public string SubBranchName { get; set; }
    }

    public class CustBankContract_ConfrimRequest : BaseRequest
    {
        [Description("Danh sách id: 1,2,3,4")]
        public string ContractIds { get; set; }
        [Description("-1: Chuyển về TKCK; khác: Về ngân hàng")]
        public int CustBankId { get; set; }
    }

    public class ConfirmationInfoRequest: BaseRequest
    {
        public int RequestOrderID { get; set; }
    }

    public class ConfirmRollInfoRequest : BaseRequest
    {
        [Description("Số HĐ")]
        public string ContractNo { get; set; }

        [Description("RedeemCode")]
        public string RedeemCode { get; set; }

        [Description("ID ngân hàng")]
        public int CustBankID { get; set; }

        [Description("Lấy bản cứng hay không")]
        public bool IsHardCopy { get; set; }
    }

    public class ValidateRollInfoRequest : BaseRequest
    {
        [Description("Số HĐ")]
        public string ContractNo { get; set; }

        [Description("RedeemCode")]
        public string RedeemCode { get; set; }

        [Description("ID ngân hàng")]
        public int CustBankID { get; set; }
    }

    public class ValidateRollInfoResult
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }
        [Description("Tổ chức phát hành")]
        public string BondIssuer { get; set; }
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }

        [Description("Ngày mua")]
        public DateTime BuyDate { get; set; }

        [Description("Mã TP con")]
        public string SubBondCode { get; set; }
        [Description("Ngày roll")]
        public DateTime TradeDate { get; set; }
        [Description("Ngày đáo hạn")]
        public DateTime MaturityDate { get; set; }
        [Description("KL")]
        public long Volume { get; set; }

        [Description("Giá bán")]
        public decimal SellPrice { get; set; }
        [Description("Giá bán trước thuế")]
        public decimal SellPriceAmount { get; set; }
        [Description("Thuế")]
        public decimal PIT { get; set; }
        [Description("Lợi tức sau thuế")]
        public decimal Interest { get; set; }
        [Description("Phí")]
        public decimal Fee { get; set; }
        [Description("Tiền bán thực nhận")]
        public decimal NetAmount { get; set; }
        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên TK")]
        public string BankAccountName { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Chi nhánh ngân hàng")]
        public string SubBranchName { get; set; }

        [Description("Thông tin dòng tiền")]
        [JsonIgnore]
        public string DataPriceInfo { get; set; }
        [Description("Thông tin dòng tiền")]
        public List<CashData> CashDataInfo
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<CashData>>(DataPriceInfo);
                }
                catch
                {
                    return null;
                }
            }
        }
    }

    public class ConfirmRollInfoResult
    {
        [Description("Mã response")]
        [JsonIgnore]
        public string RetCode { get; set; }
        [Description("Tổ chức phát hành")]
        public string BondIssuer { get; set; }
        [Description("Mã trái phiếu")]
        public string BondCode { get; set; }
        [Description("Mã TP con")]
        public string SubBondCode { get; set; }
        [Description("Ngày roll")]
        public DateTime TradeDate { get; set; }
        [Description("Ngày đáo hạn")]
        public DateTime MaturityDate { get; set; }
        [Description("KL")]
        public long Volume { get; set; }

        [Description("Giá bán")]
        public decimal SellPrice { get; set; }
        [Description("Giá bán trước thuế")]
        public decimal SellPriceAmount { get; set; }
        [Description("Thuế")]
        public decimal PIT { get; set; }
        [Description("Phí")]
        public decimal Fee { get; set; }
        [Description("Tiền bán thực nhận")]
        public decimal NetAmount { get; set; }
        [Description("Số TK ngân hàng")]
        public string BankAccountNo { get; set; }
        [Description("Tên TK")]
        public string BankAccountName { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Chi nhánh ngân hàng")]
        public string SubBranchName { get; set; }

        [Description("Thông tin dòng tiền")]
        [JsonIgnore]
        public string DataPriceInfo { get; set; }
        [Description("Thông tin dòng tiền")]
        public List<CashData> CashDataInfo
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<CashData>>(DataPriceInfo);
                }
                catch
                {
                    return null;
                }
            }
        }
    }

    public class GetMaturityDateRequest : BaseRequest
    {
        [Description("Loại sản phẩm")]
        public int ProductType { get; set; }
        [Description("Kỳ hạn")]
        public int Term { get; set; }
        [Description("Ngày giao dịch")]
        public string TradeDate { get; set; }
    }
}
