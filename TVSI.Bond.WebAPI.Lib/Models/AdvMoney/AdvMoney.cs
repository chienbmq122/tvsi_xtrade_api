﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Bond.WebAPI.Lib.Models.AdvMoney
{
    public class AvaiableAdvanceInfoRequest : BaseRequest
    {

    }
    public class AvaiableAdvanceInfoResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }

        [Description("Thông tin bán chứng khoán")]
        public List<SellStockInfo> SellStockInfos { get; set; }
    }

    public class SellStockInfo
    {
        [Description("Số tiền có thể ứng")]
        public decimal Availamt { get; set; }
        [Description("Phí giao dịch")]
        public decimal Commamt { get; set; }

        [Description("Ngày về")]
        public DateTime Duedate { get; set; }

        [Description("Ngày giao dịch")]
        public DateTime Tradedate { get; set; }

        [Description("Số tiền bán")]
        public decimal Sellamt { get; set; }

        [Description("Giá trị bán")]
        public decimal Sellvalue { get; set; }
        public decimal Vatamt { get; set; }
        public decimal Loanamt { get; set; }
        public decimal Pledgeamt { get; set; }
        public decimal Pledgevalue { get; set; }
        public decimal Taxamt { get; set; }
        public decimal Totalamt { get; set; }

        [Description("Tỷ lệ ứng tiền")]
        public decimal Feerate { get; set; }

        [Description("Số tiền đã ứng")]
        public decimal Wdbef { get; set; }

        [Description("Số ngày ứng")]
        public int NumDay { get; set; }
    }

    public class FeeAdvanceInfoRequest : BaseRequest
    {
        [Description("Số tiền ứng/số tiền thực nhận")]
        public decimal Amount { get; set; }

        [Description("Số ngày ứng")]
        public int NumDay { get; set; }

        [Description("Lấy theo số tiền ứng / thực nhận (1/2)")]
        public int Type { get; set; }
    }

    public class FeeAdvanceInfoResult
    {
        [Description("Phí ứng tiền tạm tính")]
        public decimal FeeAmt { get; set; }
    }

    public class CustAdvanceTodayRequest : BaseRequest
    {

    }

    public class CustAdvanceHistRequest : BaseRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class CustAdvanceInfoResult
    {
        [Description("Ngày ứng")]
        public DateTime AdvDate { get; set; }

        [Description("Số SEQ")]
        public int SEQ { get; set; }

        [Description("Ngày bán")]
        public DateTime TradeDate { get; set; }

        [Description("Ngày về")]
        public DateTime Duedate { get; set; }

        [Description("Số tiền bán")]
        public decimal Sellamt { get; set; }

        [Description("Số tiền ứng")]
        public decimal Amount { get; set; }

        [Description("Phí ứng")]
        public decimal FeeAdv { get; set; }

        [Description("Số ngày ứng")]
        public decimal NumDay { get; set; }

        [Description("Ngày tạo")]
        public DateTime CreatedDate { get; set; }

        [Description("Số tham chiếu")]
        public string RefNo { get; set; }

        [Description("Số tiền thực nhận")]
        public decimal RealAmount { get; set; }

        [Description("Trạng thái")]
        public string Status { get; set; }

        [Description("Trạng thái")]
        public string StatusName { get; set; }

        [Description("Note")]
        public string Note { get; set; }
    }

    public class AddAdvanceInfoRequest : BaseRequest
    {
        [Description("Số tiền ứng")]
        public decimal Amount { get; set; }

        [Description("Ngày giao dịch bán")]
        public DateTime Tradedate { get; set; }

        [Description("Ngày CK về")]
        public DateTime Duedate { get; set; }
    }

    public class AddAdvanceInfoResult
    {
        [Description("Mã lỗi")]
        public string RetCode { get; set; }

        [Description("Ngày giao dịch")]
        public DateTime TradeDate { get; set; }

        [Description("Số sequence")]
        public int SEQ { get; set; }

        [Description("Mô tả lỗi")]
        public string Message { get; set; }
    }
}
