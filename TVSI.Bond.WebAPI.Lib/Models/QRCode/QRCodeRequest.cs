﻿using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.QRCode
{
    public class QRCodeRequest : BaseRequest
    {
        
    }
    
    
    public class CrmQRCodeResult
    {
        public long QRCodeID { get; set; }
        public byte[] QRImage { get; set; }
        public string SaleID { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public int Status { get; set; }
    }
    public class QRCodeResult
    {
        /// <summary>
        /// Mã MKTID
        /// </summary>
        public string SaleID { get; set; }
        
        /// <summary>
        /// Họ tên nhân viên môi giới
        /// </summary>
        [Description("Họ tên nhân viên môi giới")]
        public string FullName { get; set; }
        
        /// <summary>
        /// QR Code base64
        /// </summary>
        [Description("Mã base64 ảnh QR")]
        public string QRBase64 { get; set; }
        
        /// <summary>
        /// Phone
        /// </summary>
        [Description("Số điện thoại nhân viên môi giới")]
        public string Phone { get; set; }
    }
}