﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.Right
{
    public class RightInfoRequest : BaseRequest { }
    
    public class RightInfoResult
    {
        /// <summary>
        /// Danh sách quyền mua
        /// </summary>
        [Description("Danh sách quyền mua")]
        public List<BuyRightData> BuyRightList { get; set; }

        /// <summary>
        /// Quyền cổ tức bằng tiền, cổ phiếu
        /// </summary>
        [Description("Danh sách quyền cổ tức = tiền, cổ phiếu")]
        public List<StockRightData> StockRightList { get; set; }
    }

    public class BuyRightData
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }

        [Description("Loại quyền: 0-Quyền mua, 1: Cổ phiếu thưởng, 2: Cổ tức = cổ phiêu, 3: Cổ tức bằng tiền")]
        public int XType { get; set; }
        [Description("Mã CK hưởng quyền")]
        public string Symbol { get; set; }
       
        [Description("Tỷ lệ")]
        public decimal Old { get; set; }
        [Description("Tỷ lệ")]
        public decimal New { get; set; }
        [Description("Giá phát hành")]
        public decimal Price { get; set; }
        [Description("Mã CK được mua")]
        public string NewShareCode { get; set; }

        [Description("Ngày GD không hưởng quyền")]
        public string XDate { get; set; }

        [Description("Ngày Đăng ký cuối cùng")]
        public string ClosedDate { get; set; }

        [Description("Ngày hết hạn đăng ký")]
        public string TransferToDate { get; set; }
        [Description("Số CK được mua")]
        public decimal CompUnitNew { get; set; }
        [Description("Số CK đã đăng ký mua")]
        public decimal CompUnitConfirm { get; set; }
        [Description("Số tiền phải nộp")]
        public decimal CompAmountNew {
            get
            {
                return CompUnitNew * Price;
            } }
        [Description("Số tiền đã nộp")]
        public decimal CompAmountConfirm {
            get
            {
                return CompUnitConfirm * Price; 
                
            } }
        [Description("Trạng thái")]
        public int Status { get; set; }

        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                if (Status == 6)
                {
                    if (CompUnitNew == 0)
                        return ResourceFile.RightModule.RIRStatus_31;
                    if (CompUnitConfirm > 0)
                        return ResourceFile.RightModule.RIRStatus_3;

                }

                return ResourceFile.RightModule.ResourceManager.GetString("RIRStatus_" + Status);
            }
        }

        public long ID { get; set; }
    }

    public class StockRightData
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }

        [Description("1-Quyền cổ tức bằng cổ phiếu,...")]
        public int XType { get; set; }
        [Description("Mã CK hưởng quyền")]
        public string Symbol { get; set; }
        [Description("Số CK hưởng quyền")]
        public decimal Volume { get; set; }
        [Description("Tỷ lệ (XType=1,2)")]
        public decimal Old { get; set; }
        [Description("Tỷ lệ (XType=1,2)")]
        public decimal New { get; set; }
        [Description("Tỷ lệ quyền cổ tức = tiền (XType = 3)")]
        public decimal PayRate { get; set; }
        [Description("Số tiền được nhận (XType = 3)")]
        public decimal CompAmt { get; set; }

        [Description("Mã CK được nhận")]
        public string NewShareCode { get; set; }
        [Description("Số tiền/Số CK được nhận")]
        public decimal CompUnitNew { get; set; }
        [Description("Ngày giao dịch không hưởng quyền")]
        public string ClosedDate { get; set; }

    }

    public class RightInfoQuery
    {
        public long ID { get; set; }
        public string ACCOUNTNO { get; set; }
        public string SYMBOL { get; set; }
        public decimal? PRICE { get; set; }
        public decimal? OLD { get; set; }
        public decimal? NEW { get; set; }
        public decimal? PAYRATE { get; set; }
        public DateTime? CLOSEDATE { get; set; }
        public DateTime? TRANSFERFROMDATE { get; set; }
        public DateTime? TRANSFERTODATE { get; set; }
        public DateTime? PAYDATE { get; set; }
        public int? XTYPE { get; set; }
        public string STOCKTYPE { get; set; }
        public decimal? COMPUNITBFXR { get; set; }
        public decimal? COMPUNITDEP { get; set; }
        public decimal? COMPUNITWD { get; set; }
        public decimal? COMPUNITNEW { get; set; }
        public decimal? COMPUNITCONFIRM { get; set; }
        public string DELFLAG { get; set; }
        public string CONFIRMFLAG { get; set; }
        public decimal? ROUNDUP { get; set; }
        public string PURPOSE { get; set; }
        public string REMARK { get; set; }
        public int? STATUS { get; set; }
        public decimal? COMPAMT { get; set; }
        public string NEWSHARECODE { get; set; }
        public DateTime? CONFIRMDATE { get; set; }
        public int RowCount { get; set; }
        public DateTime? XDate { get; set; }

    }

    public class BalanceIntradayQuery
    {
        public decimal BalanceIntraday { get; set; }
        public decimal Amount { get; set; }
        public string AccountNo { get; set; }
    }
}
