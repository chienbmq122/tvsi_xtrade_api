﻿using System;
using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.Right
{
    public class RightHistRequest : BaseRequest
    {
        [Description("Mã Chứng khoán")]
        public string Symbol { get; set; }
        [Description("Loại THQ: -1: All, 0: Quyền mua, 1: Cổ phiếu thưởng, 2: Cổ tức bằng cổ phiếu, 3-Cổ tức bằng tiền")]
        public int XType { get; set; }
        [Description("Mã Chứng khoán")]
        public string FromDate { get; set; }
        [Description("Mã Chứng khoán")]
        public string ToDate { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }

    public class RightHistResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        [Description("Mã CK hưởng quyền")]
        public string Symbol { get; set; }

        [Description("0: Quyền mua, 1: Cổ phiếu bằng cổ phiếu, 2: Cổ phiếu thưởng, 3-Cổ tức bằng tiền")]
        public int XType { get; set; }
        [Description("Tỷ lệ")]
        public decimal Old { get; set; }
        [Description("Tỷ lệ")]
        public decimal New { get; set; }
        [Description("Tỷ lệ")]
        public decimal PayRate { get; set; }
        [Description("Số tiền được nhận")]
        public decimal CompAmt { get; set; }
        [Description("Mã CK được nhận")]
        public string NewShareCode { get; set; }
        [Description("Số CK được nhận")]
        public decimal CompUnitNew { get; set; }
        [Description("Số CK đăng ký mua")]
        public decimal CompUnitConfirm { get; set; }
        [Description("Giá phát hành")]
        public decimal Price { get; set; }
        [Description("Số tiền đã nộp")]
        public decimal Amount
        {
            get { return Price*CompUnitConfirm; }
        }
        [Description("Ngày GD không hưởng quyền")]
        public string CloseDate { get; set; }
        [Description("Ngày thực hiện")]
        public string RightDate { get; set; }

        public int TotalRow { get; set; }
    }

    public class RightHistQuery
    {
        public long ID { get; set; }
        public string ACCOUNTNO { get; set; }
        public string CONTRACTNO { get; set; }
        public string SYMBOL { get; set; }
        public decimal? PRICE { get; set; }
        public decimal? OLd { get; set; }
        public decimal? NEW { get; set; }
        public decimal? PAYRATE { get; set; }
        public decimal? COMPAMT { get; set; }
        public decimal? COMPUNITNEW { get; set; }
        public decimal? COMPUNITCONFIRM { get; set; }
        public int? XTYPE { get; set; }
        public int? STATUS { get; set; }
        public string USERID { get; set; }
        public DateTime? RIGHTDATE { get; set; }
        public DateTime? TRANSFERTODATE { get; set; }
        public DateTime? CLOSEDATE { get; set; }
        public bool? ISNEW { get; set; }
        public string REASON { get; set; }
        public string CUSTOMERID { get; set; }
        public DateTime? EDITTIME { get; set; }
        public DateTime? APPROVETIME { get; set; }
        public string APPROVEID { get; set; }
        public decimal? VOLUME { get; set; }
        public string NEWSHARECODE { get; set; }
        public string RECEIVERACCOUNT { get; set; }
        public DateTime? CONFIRMDATE { get; set; }
        public int RowCount { get; set; }
    }
}
