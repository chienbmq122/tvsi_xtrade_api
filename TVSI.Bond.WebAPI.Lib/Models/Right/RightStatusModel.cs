﻿using System;
using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.Right
{
    public class RightStatusRequest : BaseRequest
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }

    public class RightStatusResult
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        [Description("Thời gian yêu cầu")]
        public string EditTime { get; set; }
        [Description("Mã CK hưởng quyền")]
        public string Symbol { get; set; }
        [Description("Tỷ lệ")]
        public decimal Old { get; set; }
        [Description("Tỷ lệ")]
        public decimal New { get; set; }
        [Description("Giá phát hành")]
        public decimal Price { get; set; }
        [Description("Mã CK được mua")]
        public string NewShareCode { get; set; }
        [Description("Ngày GD không hưởng quyền")]
        public string XDate { get; set; }

        [Description("Ngày Đăng ký cuối cùng")]
        public string CloseDate { get; set; }
        [Description("Ngày hết hạn đăng ký")]
        public string TransferToDate { get; set; }
        [Description("Số CK được mua")]
        public decimal CompUnitNew { get; set; }
        [Description("Số tiền phải nộp")]
        public decimal Amount
        {
            get { return Price*CompUnitNew; }
        }
        [Description("Số CK đã đăng ký mua")]
        public decimal CompUnitConfirm { get; set; }

        [Description("Số tiền đã nộp")]
        public decimal ConfirmAmount
        {
            get { return Price*CompUnitConfirm; }
        }

        [Description("Trạng thái")]
        public int Status { get; set; }

        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
                if (Status == 6)
                {
                    if (CompUnitNew == 0)
                        return ResourceFile.RightModule.RIRStatus_31;
                    if (CompUnitConfirm > 0)
                        return ResourceFile.RightModule.RIRStatus_3;

                }

                return ResourceFile.RightModule.ResourceManager.GetString("RIRStatus_" + Status);
            }
        }

        [Description("Số hiệu lệnh")]
        public string ContractNo { get; set; }

        [Description("Tổng số bản ghi")]
        public int TotalRow { get; set; }

        [Description("Lý do từ chối")]
        public string Reason { get; set; }
        
    }

    public class RightStatusQuery
    {
        public long ID { get; set; }
        public string ACCOUNTNO { get; set; }
        public string CONTRACTNO { get; set; }
        public string SYMBOL { get; set; }
        public decimal? PRICE { get; set; }
        public decimal? OLd { get; set; }
        public decimal? NEW { get; set; }
        public decimal? PAYRATE { get; set; }
        public decimal? COMPAMT { get; set; }
        public decimal? COMPUNITNEW { get; set; }
        public decimal? COMPUNITCONFIRM { get; set; }
        public int? XTYPE { get; set; }
        public int? STATUS { get; set; }
        public string USERID { get; set; }
        public DateTime? RIGHTDATE { get; set; }
        public DateTime? TRANSFERTODATE { get; set; }
        public DateTime? CLOSEDATE { get; set; }
        public bool? ISNEW { get; set; }
        public string REASON { get; set; }
        public string CUSTOMERID { get; set; }
        public DateTime? EDITTIME { get; set; }
        public DateTime? APPROVETIME { get; set; }
        public string APPROVEID { get; set; }
        public decimal? VOLUME { get; set; }
        public string NEWSHARECODE { get; set; }
        public string RECEIVERACCOUNT { get; set; }
        public DateTime? CONFIRMDATE { get; set; }
        public int RowCount { get; set; }
    }
}
