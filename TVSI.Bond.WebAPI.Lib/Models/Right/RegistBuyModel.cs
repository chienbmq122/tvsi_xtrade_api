﻿using System;
using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.Right
{
    public class RegistBuyRequest : BaseRequest
    {
        [Description("Mã CK")]
        public string Symbol { get; set; }

        [Description("Khối lượng")]
        public long Volume { get; set; }
        [Description("Giá phát hành")]
        public decimal Price { get; set; }
        [Description("ID")]
        public long ID { get; set; }

    }

    public class TransferBuyRequest : BaseRequest
    {
        [Description("Mã CK")]
        public string Symbol { get; set; }

        [Description("Khối lượng")]
        public long Volume { get; set; }
        [Description("Giá phát hành")]
        public decimal Price { get; set; }
        [Description("Số tài khoản nhận")]
        public string AccountReceive { get; set; }

        [Description("ID")]
        public long ID { get; set; }
    }

    public class SP_SEL_FO_RI_BYID_Result
    {
        public long ID { get; set; }
        public string ACCOUNTNO { get; set; }
        public string SYMBOL { get; set; }
        public decimal? PRICE { get; set; }
        public decimal? OLD { get; set; }
        public decimal? NEW { get; set; }
        public decimal? PAYRATE { get; set; }
        public DateTime? CLOSEDATE { get; set; }
        public DateTime? TRANSFERFROMDATE { get; set; }
        public DateTime? TRANSFERTODATE { get; set; }
        public DateTime? PAYDATE { get; set; }
        public int? XTYPE { get; set; }
        public string STOCKTYPE { get; set; }
        public decimal? COMPUNITBFXR { get; set; }
        public decimal? COMPUNITDEP { get; set; }
        public decimal? COMPUNITWD { get; set; }
        public decimal? COMPUNITNEW { get; set; }
        public decimal? COMPUNITCONFIRM { get; set; }
        public string DELFLAG { get; set; }
        public string CONFIRMFLAG { get; set; }
        public decimal? ROUNDUP { get; set; }
        public string PURPOSE { get; set; }
        public string REMARK { get; set; }
        public int? STATUS { get; set; }
        public decimal? COMPAMT { get; set; }
        public string NEWSHARECODE { get; set; }
        public DateTime? CONFIRMDATE { get; set; }
        public string RIRNO { get; set; }
    }

    public class CancelRegistBuyRequest : BaseRequest
    {
        [Description("Số HĐ quyền mua")]
        public string ContractNo { get; set; }
    }
}
