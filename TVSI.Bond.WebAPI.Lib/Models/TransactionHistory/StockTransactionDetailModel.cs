﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Models.TransactionHistory
{
    public class StockTransactionDetailRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }
        [Description("Mã CK")]
        public string Symbol { get; set; }
        [Description("Bút toán")]
        public string TransType { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }

    public class StockTransactionDetailResponse
    {
        public string Symbol { get; set; }

        public string TransType
        {
            get
            {
                return GetTypeText(RefType);
            }
        }

        public decimal Price { get; set; }

        [Description("Chứng khoán khả dụng")]
        public decimal SellableStocks { get; set; }

        [Description("Chứng khoán cầm cố")]
        public decimal PledgeStocks { get; set; }

        [Description("Chứng khoán Hạn chế chuyển nhượng")]
        public decimal RestrictedStocks { get; set; }

        [Description("Chứng khoán Đăng ký lưu ký")]
        public decimal RegisterStocks { get; set; }

        [Description("Chứng khoán Hưởng quyền")]
        public decimal RightStock { get; set; }

        [JsonIgnore]
        public string RefType { get; set; }
        [JsonIgnore]
        public DateTime DueDate { get; set; }
        [JsonIgnore]
        public DateTime TransDate { get; set; }
        [JsonIgnore]
        public string TransType_01 { get; set; }
        [JsonIgnore]
        public string TransType_02 { get; set; }
        [JsonIgnore]
        public string CustType { get; set; }
        [JsonIgnore]
        public string StockType { get; set; }
        [JsonIgnore]
        public string StockType2 { get; set; }
        [JsonIgnore]
        public string Remark { get; set; }

        [Description("Ghi chú Loại GD")]
        [JsonProperty("Remark")]
        public string Content
        {
            get
            {
                return TransHistUtil.GetContent(TransDate.ToString("yyyyMMdd"), Remark, Price + "", RefType, CustType, TransType_02,
                    StockType, StockType2);
            }
        }

        [Description("Ngày")]
        [JsonProperty("TradeDate")]
        public string TradeDateStr
        {
            get
            {
                if (RefType == "SE")
                    return TransHistUtil.ConvertDateByLang(TransDate.ToString("yyyyMMdd"));

                return TransHistUtil.ConvertDateByLang(DueDate.ToString("yyyyMMdd"));
            }
        }

        public string GetTypeText(string refNo)
        {
            string str = string.Empty;
            switch (refNo)
            {
                case "DE":
                    str = ResourceFile.TransactionHistModule.Deposit;
                    break;
                case "WD":
                    str = ResourceFile.TransactionHistModule.WithDraw;
                    break;
                case "TI":
                    str = ResourceFile.TransactionHistModule.TransferFromOtherBroker;
                    break;
                case "TO":
                    str = ResourceFile.TransactionHistModule.TransferToOtherBroker;
                    break;
                case "XR":
                    str = ResourceFile.TransactionHistModule.RightBuyStock;
                    break;
                case "RX":
                    str = ResourceFile.TransactionHistModule.ReceiveRightStock;
                    break;
                case "BU":
                    str = ResourceFile.TransactionHistModule.BuyDate2;
                    break;
                case "SE":
                    str = ResourceFile.TransactionHistModule.SellDate3;
                    break;
                case "XE":
                    str = ResourceFile.TransactionHistModule.RightStocks2;
                    break;
            }
            return str;
        }
    }



    public class FO_SEL_STOCK_TRANSACTIONS_DETAIL_Query
    {
        public long ID { get; set; }
        public string ACCOUNTNO { get; set; }
        public string ACCOUNTNO2 { get; set; }
        public DateTime? TRANSDATE { get; set; }
        public DateTime DUEDATE { get; set; }
        public string SYMBOL { get; set; }
        public string REFTYPE { get; set; }
        public string STOCKTYPE { get; set; }
        public string TRANS_NO { get; set; }
        public string TRANS_TYPE1 { get; set; }
        public string TRANS_TYPE2 { get; set; }
        public string CUSTTYPE { get; set; }
        public decimal QUANTITY { get; set; }
        public decimal? PRICE { get; set; }
        public string REMARK { get; set; }
        public string USERID { get; set; }
        public string TRAN_DATE_TIME { get; set; }
        public string AUTHID { get; set; }
        public string APPR_DATE_TIME { get; set; }
        public DateTime SynDateTime { get; set; }
        public string SeqNo { get; set; }
        public long RowIndex { get; set; }
        public int RowCount { get; set; }
    }
}
