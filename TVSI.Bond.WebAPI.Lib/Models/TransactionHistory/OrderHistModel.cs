﻿using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Models.TransactionHistory
{
    public class OrderHistTransRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }
        [Description("Mã CK")]
        public string Symbol { get; set; }
        [Description("Mua/Bán")]
        public string Side { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Kênh đặt lệnh")]
        public string Chanel { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }

    public class OrderHistTransResponse
    {
        [Description("Thời gian")]
        public string TradeTime { get; set; }
        [Description("Số hiệu lệnh")]
        public string OrderNo { get; set; }
        [Description("Mua/Bán")]
        public string Side { get; set; }

        public string EnterID { get; set; }

        public string SideName
        {
            get
            {
                switch (Side)
                {
                    case "B":
                        return ResourceFile.TransactionHistModule.TO_Buy;
                    case "S":
                        return ResourceFile.TransactionHistModule.TO_Sell;
                    default:
                        return Side;
                }

            }

        }
        [Description("Mã chứng khoán")]
        public string Symbol { get; set; }
        [Description("Giá đặt")]
        public decimal OrderPrice { get; set; }
        [Description("Khối lượng đặt")]
        public decimal OrderQty { get; set; }
        [Description("Giá khớp")]
        public decimal DealPrice { get; set; }
        [Description("Khối lượng khớp")]
        public decimal DealQty { get; set; }
        [Description("Khối lượng hủy")]
        public decimal CancelQty { get; set; }
        [Description("Phí")]
        public decimal Fee { get; set; }
        [Description("Thuế")]
        public decimal Tax { get; set; }
        [Description("Thành tiền")]
        public decimal Amount { get; set; }
        [Description("Trạng thái")]
        public string Status { get; set; }
        [Description("Mã Sàn")]
        public string Market { get; set; }
        [Description("Loại giá đặt")]
        public string ConditionPrice { get; set; }

        public string ConditionPriceName
        {
            get
            {
                if (!string.IsNullOrEmpty(ConditionPrice))
                {
                    if (ConditionPrice == "A")
                        return "ATO";

                    if (ConditionPrice == "C")
                        return "ATC";

                    if (ConditionPrice == "M")
                    {
                        if ("HOSE".Equals(Market))
                            return "MP";

                        return "MTL";
                    }
                    
                    if (ConditionPrice == "O")
                        return "MOK";

                    if (ConditionPrice == "K")
                        return "MAK";
                }
                
                return ConditionPrice;
            }
        }

        public string StatusName
        {
            get
            {
                switch (Status)
                {
                    case "A":
                        return ResourceFile.TransactionHistModule.DealStatus_Approved;
                    case "C":
                        return ResourceFile.TransactionHistModule.DealStatus_Canceled;
                    case "D":
                        return ResourceFile.TransactionHistModule.DealStatus_Rejected;
                    case "M":
                        return ResourceFile.TransactionHistModule.DealStatus_Matched;
                    case "MA":
                        return ResourceFile.TransactionHistModule.DealStatus_Matched;
                    case "MC":
                        return ResourceFile.TransactionHistModule.DealStatus_Change;
                    case "MAC":
                        return ResourceFile.TransactionHistModule.DealStatus_Approved;
                    case "O":
                        return ResourceFile.TransactionHistModule.DealStatus_Open;
                    case "OA":
                        return ResourceFile.TransactionHistModule.DealStatus_Approved;
                    case "OAC":
                        return ResourceFile.TransactionHistModule.DealStatus_Change;
                    case "OC":
                        return ResourceFile.TransactionHistModule.DealStatus_Change;
                    case "PO":
                        return ResourceFile.TransactionHistModule.DealStatus_Pending;
                    case "POA":
                        return ResourceFile.TransactionHistModule.DealStatus_Pending;
                    case "PX":
                        return ResourceFile.TransactionHistModule.DealStatus_Pending;
                    case "PXC":
                        return ResourceFile.TransactionHistModule.DealStatus_Pending;
                    case "R":
                        return ResourceFile.TransactionHistModule.DealStatus_Rejected;
                    case "RC":
                        return ResourceFile.TransactionHistModule.DealStatus_Rejected;
                    case "X":
                        return ResourceFile.TransactionHistModule.DealStatus_Canceled;
                    case "XA":
                        return ResourceFile.TransactionHistModule.DealStatus_Canceled;
                    case "XC":
                        return ResourceFile.TransactionHistModule.DealStatus_Canceled;
                    case "XAC":
                        return ResourceFile.TransactionHistModule.DealStatus_Canceled;
                    case "UO":
                        return ResourceFile.TransactionHistModule.DealStatus_Other;
                    case "UX":
                        return ResourceFile.TransactionHistModule.DealStatus_Other;
                    case "DS":
                        return ResourceFile.TransactionHistModule.DealStatus_Pending;
                    case "DC":
                        return ResourceFile.TransactionHistModule.DealStatus_Pending;
                    case "SD":
                        return ResourceFile.TransactionHistModule.DealStatus_Rejected;
                    case "CD":
                        return ResourceFile.TransactionHistModule.DealStatus_Rejected;
                    case "MD":
                        return ResourceFile.TransactionHistModule.DealStatus_Matched;
                    case "XS":
                        return ResourceFile.TransactionHistModule.DealStatus_Pending;
                    case "XB":
                        return ResourceFile.TransactionHistModule.DealStatus_Pending;
                    case "U":
                        return ResourceFile.TransactionHistModule.DealStatus_Other;
                    case "x":
                        return ResourceFile.TransactionHistModule.DealStatus_CancelDeal;
                    case "m":
                        return ResourceFile.TransactionHistModule.DealStatus_SemiMatched;
                    default:
                        return string.Empty;
                }


            }
        }

        [Description("Kênh đặt lệnh")]
        public string Channel { get; set; }

        public string ChannelTypeName
        {
            get
            {
                switch (Channel)
                {
                    case "W":
                        return ResourceFile.TransactionHistModule.ChanelType_W;
                    case "B":
                        return ResourceFile.TransactionHistModule.ChanelType_B;
                    case "S":
                        return ResourceFile.TransactionHistModule.ChanelType_S;
                    case "I":
                        return ResourceFile.TransactionHistModule.ChanelType_I;
                    case "N":
                        return ResourceFile.TransactionHistModule.ChanelType_N;
                    case "M":
                        return ResourceFile.TransactionHistModule.ChanelType_M;
                    case "T":
                        return ResourceFile.TransactionHistModule.ChanelType_T;
                    case "K":
                        return ResourceFile.TransactionHistModule.ChanelType_K;
                    case "H":
                        return ResourceFile.TransactionHistModule.ChanelType_H;
                    default:
                        return ResourceFile.TransactionHistModule.ChanelType_B;
                }

            }
        }

    }

    public class SP_SEL_FO_ORDERHISTORY_Query
    {
        public long TotalRow { get; set; }
        public long IND { get; set; }
        public string AccountNo { get; set; }
        public string SecSymbol { get; set; }
        public int OrderNo { get; set; }
        public string Side { get; set; }
        public decimal Price { get; set; }
        public decimal volume { get; set; }
        public string Condition { get; set; }
        public string ConditionPrice { get; set; }
        public string OrderType { get; set; }
        public string EnterID { get; set; }
        public string OrderDate { get; set; }
        public string OrderTime { get; set; }
        public string OrderStatus { get; set; }
        public decimal MatchVolume { get; set; }
        public string CancelID { get; set; }
        public string CancelTime { get; set; }
        public int CancelVolume { get; set; }
        public string ServiceType { get; set; }
        public int OrderSeqNo { get; set; }
        public decimal MatchValue { get; set; }
        public decimal MatchPrice { get; set; }
        public string TrusteeID { get; set; }
        public decimal PubVolume { get; set; }
        public decimal Comm { get; set; }
        public decimal Tax { get; set; }
        public string Channel { get; set; }
        public string EffectedDate { get; set; }
        public string CustomerId { get; set; }
        public decimal? StopPrice { get; set; }
        public decimal? PublishVol { get; set; }
        public decimal MatchValue_Net { get; set; }
        public decimal Tariff { get; set; }
        public decimal Total_Volume { get; set; }
        public decimal Total_MatchValue { get; set; }
        public decimal Total_Fee { get; set; }
        public decimal Total_Tax { get; set; }
        public decimal Total_MatchValue_Net { get; set; }
        public decimal Total_MatchVolume { get; set; }
        public string Market { get; set; }

    }
}
