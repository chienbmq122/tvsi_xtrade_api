﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Models.TransactionHistory
{
    public class StockTransactionHistRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }
        [Description("Đến ngày")]
        public string ToDate { get; set; }
    }

    public class StockTransactionHistResponse
    {
        [Description("Mã CK")]
        public string Symbol { get; set; }
        public decimal Price { get; set; }

        [Description("Số đàu kỳ-Chứng khoán khả dụng (StockType=2)")]
        public decimal FirstSellableStocks { get; set; }

        [Description("Số đàu kỳ-Chứng khoán cầm cố (StockType=47)")]
        public decimal FirstPledgeStocks { get; set; }

        [Description("Số đàu kỳ-Chứng khoán Hạn chế chuyển nhượng(StockType=49)")]
        public decimal FirstRestrictedStocks { get; set; }

        [Description("Số đàu kỳ-Chứng khoán Đăng ký lưu ký(StockType=03)")]
        public decimal FirstRegisterStocks { get; set; }

        [Description("Số đàu kỳ-Chứng khoán Hưởng quyền(StockType=12)")]
        public decimal FirstRightStock { get; set; }

        [Description("Danh sách giao dịch")]
        public List<StockTransData> StockTransList { get; set; }


        [Description("Số cuối kỳ-Chứng khoán khả dụng (StockType=2)")]
        public decimal LastSellableStocks { get; set; }

        [Description("Số cuối kỳ-Chứng khoán cầm cố (StockType=47)")]
        public decimal LastPledgeStocks { get; set; }

        [Description("Số cuối kỳ-Chứng khoán Hạn chế chuyển nhượng(StockType=49)")]
        public decimal LastRestrictedStocks { get; set; }

        [Description("Số cuối kỳ-Chứng khoán Đăng ký lưu ký(StockType=03)")]
        public decimal LastRegisterStocks { get; set; }

        [Description("Số cuối kỳ-Chứng khoán Hưởng quyền(StockType=12)")]
        public decimal LastRightStock { get; set; }

    }

    public class StockTransData
    {
        [Description("Mã chứng khoán")]
        public string Symbol { get; set; }
        
        [Description("Giá")]
        public decimal Price { get; set; }

        [Description("Chứng khoán khả dụng (StockType=2)")]
        public decimal SellableStocks { get; set; }

        [Description("Chứng khoán cầm cố (StockType=47)")]
        public decimal PledgeStocks { get; set; }

        [Description("Chứng khoán Hạn chế chuyển nhượng(StockType=49)")]
        public decimal RestrictedStocks { get; set; }

        [Description("Chứng khoán Đăng ký lưu ký(StockType=03)")]
        public decimal RegisterStocks { get; set; }

        [Description("Chứng khoán Hưởng quyền(StockType=12)")]
        public decimal RightStock { get; set; }

        [Description("Số dư")]
        public decimal Balance { get; set; }

        [JsonIgnore]
        public string TransType_01 { get; set; }
        [JsonIgnore]
        public string TransType_02 { get; set; }
        [JsonIgnore]
        public string TransDate { get; set; }
        public string DueDate { get; set; }
        [JsonIgnore]
        public string RefType { get; set; }
        [JsonIgnore]
        public string CustType { get; set; }
        [JsonIgnore]
        public string StockType { get; set; }
        [JsonIgnore]
        public string StockType2 { get; set; }
        [JsonIgnore]
        public string Remark { get; set; }

        [Description("Ghi chú Loại GD")]
        [JsonProperty("Remark")]
        public string Content
        {
            get
            {
                return TransHistUtil.GetContent(TransDate, Remark, Price + "", RefType, CustType, TransType_02,
                    StockType, StockType2);
            }
        }

        [Description("Ngày")]
        [JsonProperty("TradeDate")]
        public string TradeDateStr
        {
            get
            {
                if (RefType == "SE")
                    return TransHistUtil.ConvertDateByLang(TransDate);

                return TransHistUtil.ConvertDateByLang(DueDate);
            }
        }
    }

    public class SP_SEL_BALANCE_TRANSACTION_Query
    {
        public DateTime? Asofdate { get; set; }
        public string StockType { get; set; }
        public decimal Quantity { get; set; }
        public string StockSymbol { get; set; }
    }

    public class SP_SEL_STOCK_TRANSACTION_Query
    {
        public string AccountNo { get; set; }   
        public string AccountNo2 { get; set; }
        public string StockType { get; set; }   
        public string RefType { get; set; }
        public string STOCKTYPE2 { get; set; }
        public int CustType { get; set; }
        public string Symbol { get; set; }
        public string Symbol2 { get; set; }
        public string Trans_Type1 { get; set; }
        public string Trans_Type2 { get; set; }
        public DateTime TransDate { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DueDate { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public string Remark { get; set; }
    }

    
}
