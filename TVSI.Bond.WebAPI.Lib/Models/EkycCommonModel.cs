﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using TVSI.Bond.WebAPI.Lib.Models.Ekyc;
using TVSI.Bond.WebAPI.Lib.Utility;

namespace TVSI.Bond.WebAPI.Lib.Models
{
    public class EkycCommonModel : EkycSaveUserInfoRequest
    {
        public long dangkyid { get; set; }
        public string BankName_01 { get; set; }
        public string SubBranchName_01 { get; set; }     
        public string BankName_02 { get; set; }
        public string SubBranchName_02 { get; set; }
    }
    public class EkycMorong 
    {
            
        [Description("Có đăng ký tài khoản giao dịch ký quỹ? 0:Không đăng ký, 1: Có đăng ký")]
        public int MarginAccount { get; set; }
        [Description("Phương thức nhận hợp đòng 1:Nhận bản mềm, 2:Nhận bản cứng")]
        public string ReceiveContractMethod { get; set; }
        [Description("Thông tin nhận chuyển phát nhanh,Trường hợp nhận bản cứng thì phải gửi thông tin này")]
        public ReceiveContractData ReceiveContractData { get; set; }
        
        [Description("Đường dẫn chưa các video + ảnh upload EKYC")]
        public FolderPath FolderPath { get; set; }
    }    
    public class EkycMorong_v2
    {
            
        [Description("Có đăng ký tài khoản giao dịch ký quỹ? 0:Không đăng ký, 1: Có đăng ký")]
        public int MarginAccount { get; set; }
        [Description("Đường dẫn chưa các video + ảnh upload EKYC")]
        public FolderPath FolderPath { get; set; }
    }
    

    public class EkycSuccessModel : SuccessModel
    {
        public EkycSuccessModel()
        {
            RetCode = EkycRetCodeConst.SUCCESS_CODE;
            RetMsg = EkycRetMsgConst.EKRetMsg_EK000;
        }
        [Description("Messeger trả về text đăng ký thành công")]
        public string RetMsg { get; set; }
    }

    public class EkycSuccessModel<T> : SuccessModel
    {
        public EkycSuccessModel()
        {
            RetCode = EkycRetCodeConst.SUCCESS_CODE;
        }
        [Description("Mã kết quả thực hiện")]
        public string RetCode { get; set; }
        
        [Description("Thông báo trả về")]
        public string RetMsg { get; set; }

        [Description("Dữ liệu trả về")]
        public T RetData { get; set; }
    }

    public class EkycErrorModel : ErrorModel
    {
        public EkycErrorModel()
        {
        }     
        public EkycErrorModel(string retCode, IEnumerable<string> retMsgList)
        {
            RetCode = retCode;
            _retMsgList = retMsgList;
        }
        public string RetCode { get; set; }
        public IEnumerable<string> _retMsgList;
        
        public static IEnumerable<string> GetDefaultMsgList(IEnumerable<string> tolist)
        {
            return tolist;
        }
    }
    public class EkycErrorListModel
    {
        public string RetCode { get; set; }
        public List<string> RetMsgList { get; set; }
    }
    public class EkycErrorListModel<T>
    {
        public EkycErrorListModel()
        {
            RetCode = EkycRetCodeConst.EK900_VALIDATION_DATA;
        }

        [Description("Mã kết quả thực hiện")]
        public string RetCode { get; set; }

        [Description("Dữ liệu trả về")]
        public IEnumerable<T> RetErrorList { get; set; } 
    }

    public class EkycRegisterErrors
    {
        public string RetErrorCode { get; set; }
        public string RetErrorMsg { get; set; }
    }

    public class EkycCompareNotSameResult
    {
        public string issueField { get; set; }
        public string issueMessage { get; set; }
    }

    public class EkycCheckMobileCust
    {
        public string issueMessage { get; set; }
    }

    public class EkycClientError
    {
        public EkycClientError()
        {
        }

        public EkycClientError(string retCode)
        {
            RetCode = retCode;
        }

        public EkycClientError(string retCode, string retMsg)
        {
            RetCode = retCode;
            _retMsg = retMsg;
        }
        public string RetCode { get; set; }
        [JsonIgnore]
        public string ParamName { get; set; }


        private string _retMsg;
        public string RetMsg
        {
            get
            {
                if (!string.IsNullOrEmpty(_retMsg))
                    return _retMsg;

                return _retMsg;
                
            }
            set { _retMsg = value; }
        }
    }
    
}