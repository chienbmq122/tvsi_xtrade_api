﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using log4net;
using System.Web;
using System.Xml;
using log4net.Repository.Hierarchy;
using TVSI.Bond.WebAPI.Lib.Models.Ekyc;

namespace TVSI.Bond.WebAPI.Lib.Utility
{
    public static class EkycUtils
    {
        private readonly static ILog _log = LogManager.GetLogger(typeof(EkycUtils));


        public static string EncriptMd5(string content)
        {
           
            
            byte[] textBytes = System.Text.Encoding.Default.GetBytes(content);
            try
            {
                System.Security.Cryptography.MD5CryptoServiceProvider cryptHandler;
                cryptHandler = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                string ret = "";
                foreach (byte a in hash)
                {
                    ret += a.ToString("x2");
                }
                return ret;
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }
        
        public static string Reverse( string s )
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse( charArray );
            return new string( charArray );
        }
        public static string ReturnBrandID(string SaleID)
        {
            string brand_id = "";
            string EMSStrConfig = ConfigurationManager.ConnectionStrings["EmsDB_Connection"].ToString();
            SqlConnection SqlConn = new SqlConnection(EMSStrConfig);
            SqlDataReader drd = null;
            SqlCommand comm =
                new SqlCommand("select ma_chi_nhanh from TVSI_DANH_MUC_MA_QUAN_LY where ma_quan_ly = @SaleID", SqlConn);
            comm.CommandType = CommandType.Text;
            comm.Parameters.AddWithValue("@SaleID", SaleID);

            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                {
                    brand_id = drd["ma_chi_nhanh"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }
            

            return brand_id;
        }

        public static IList<AddressBranch> ReadFileXMLADDCT()
        {
            var path = HttpContext.Current.Server.MapPath("~/Templates/AddressBranch/AddressBranch.xml");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/data/records/record");
            IList<AddressBranch> addressBranchModel = new List<AddressBranch>();

            foreach (XmlNode node in nodeList)
            {
                var addressBranch = new AddressBranch();
                addressBranch.mcn = node.SelectSingleNode("MCN").InnerText;
                addressBranch.Ten_CN = node.SelectSingleNode("Ten_CN").InnerText;
                addressBranch.Dia_Chi = node.SelectSingleNode("Dia_Chi").InnerText;
                addressBranchModel.Add(addressBranch);
            }
            return addressBranchModel;
        }
        public static string ReturnBrandName(string SaleID)
        {
            string brand_Name = "";
            string EMSStrConfig = ConfigurationManager.ConnectionStrings["EmsDB_Connection"].ToString();
            SqlConnection SqlConn = new SqlConnection(EMSStrConfig);
            SqlDataReader drd = null;
            SqlCommand comm =
                new SqlCommand("select ten_bo_phan from TVSI_DANH_MUC_MA_QUAN_LY where ma_quan_ly = @SaleID", SqlConn);
            comm.CommandType = CommandType.Text;
            comm.Parameters.AddWithValue("@SaleID", SaleID);

            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                {
                    brand_Name = drd["ten_bo_phan"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return brand_Name;
        }

        public static bool ExpiraDateCardId(string Date)
        {
            var dateNow = DateTime.Now.ToString("dd/MM/yyyy");
            var fifteenYear = DateTime.Now.AddYears(15).ToString("dd/MM/yyyy");
            var dateNowCV =  DateTime.ParseExact(dateNow, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var dateFormat = DateTime.ParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var fifteenYearcv = DateTime.ParseExact(fifteenYear, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var total = dateNowCV - dateFormat;
            var test = DateTime.Now.AddDays( - total.Days);
            
            return true;
        }
        
        // ham check tren 18 tuoi
        public static bool IsLegalAge18(string oldDate) 
        {

            var bday = Convert.ToDateTime(oldDate).ToString("dd/MM/yyyy HH:mm:ss");
            DateTime pbday = DateTime.ParseExact(bday, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var datenow = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            DateTime pdDateTime = DateTime.ParseExact(bday, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var ts = pdDateTime - pbday;
            var year = DateTime.MinValue.Add(ts).Year - 1;
            return year >= 18;
        }
        
        //Ham check > 18 tuoi
        public static bool IsAge18(string bornDate)
        {
            try
            {
                var strtime = DateTime.ParseExact(bornDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime today = DateTime.Today;
                int age = today.Year - strtime.Year;
                if (strtime > today.AddYears(-age)) 
                    age--;
                return age >= 18;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        // Ham check format datetime dd/mm/yyyy
        public static bool IsValidDateFormat(string dateFormat)
        {
            try {
                string s = DateTime.Now.ToString(dateFormat, CultureInfo.InvariantCulture);
                DateTime.ParseExact(s, "dd/MM/yyyy",null);
                return true;
            } catch(Exception ex) {
                return false;
            }
        }
        // Ham check cmnd con hieu luc > 15 nam
        public static bool IsValidCardId15Year(string dateCardIdClient)
        {
            var dateclient = DateTime.ParseExact(dateCardIdClient, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var ddatenow = DateTime.Now.ToString("dd/MM/yyyy");
            var datenow = DateTime.ParseExact(ddatenow, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var clientfifteenyear = dateclient.AddYears(15);
            var nDatenow = datenow - dateclient;
            var totaldate = clientfifteenyear - dateclient;
            return totaldate > nDatenow;
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool IsvalidBank1(string bankAccount, string bankNo, string branchNo, string bankAccountName,
            string cityNo)
        {
            return !string.IsNullOrEmpty(bankAccount)  && !string.IsNullOrEmpty(bankNo) && !string.IsNullOrEmpty(branchNo) && !string.IsNullOrEmpty(bankAccountName) &&
                   !string.IsNullOrEmpty(cityNo);

        }


        public static List<string> ImageNameSR
        {
            get
            {
                return new List<string>()
                {
                    "jpg",
                    "jpeg",
                    "png"
                };
            }
        }

        static string[] mediaExtensions =
        {
            ".PNG", ".JPG", ".JPEG", ".BMP", ".GIF", //etc
            ".WAV", ".MID", ".MIDI", ".WMA", ".MP3", ".OGG", ".RMA", //etc
            ".AVI", ".MP4", ".DIVX", ".WMV", //etc
        };

        public static bool IsMediaFile(string path)
        {
            return -1 != Array.IndexOf(mediaExtensions, Path.GetExtension(path).ToUpperInvariant());
        }

        public static void CreateFolder()
        {
            var listFolder = new List<string>()
            {
                "~/UploadEkyc/UploadImageFace",
                "~/UploadEkyc/UploadImageOCR",
                "~/UploadEkyc/UploadVideoST",
                "~/UploadEkyc/UploadVideoCFST",
            };
            foreach (var lists in listFolder)
            {
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(lists)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(lists));
                }
            }
        }
        public class RegisterConst
        {
            public const string Direct = "1";
            public const string ITradeHome = "2";
            public const string Phone = "1";
            public const string Internet = "2";
            public const string SMS = "1";
            public const string Email = "2";
            public const string RequisteredLetter = "1";
            public const string Monthly = "1";
            public const string Quarterly = "2";
            public const string Bank = "1";
            public const string Express  = "3";
            public const string MeReceiver  = "1";
            public const string OtherReceiver  = "1";
        }
        /*public static bool IsRegistBankTransferService(EkycSaveUserInfo model)
        {
            if (!string.IsNullOrEmpty(model.AccNameBank))
            {
                return true;
            }
            return false;
        }*/
        public static string RandomNumber(int length = 6)
        {
            //Initiate objects & vars  
            byte[] seed = Guid.NewGuid().ToByteArray();
            Random random = new Random(BitConverter.ToInt32(seed, 0));
            int randNumber = 0;
            //Loop ‘length’ times to generate a random number or character
            String randomNumber = "";
            for (int i = 0; i < length; i++)
            {
                randNumber = random.Next(48, 58);
                randomNumber = randomNumber + (char)randNumber;
                //append random char or digit to randomNumber string

            }
            return randomNumber;
        }
        public static bool CheckEkycOtp(string code, string otp)
        {

            bool bOutPut = false;
            string CRMStrConfig = ConfigurationManager.ConnectionStrings["CRM"].ToString();
            SqlConnection SqlConn = new SqlConnection(CRMStrConfig);
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("select ConfirmCode from TVSI_DANG_KY_MO_TAI_KHOAN where ConfirmCode = @ConfirmCode and ConfirmSms = @ConfirmOtp and ConfirmStatus = 0", SqlConn);
            comm.CommandType = CommandType.Text;
            comm.Parameters.AddWithValue("@ConfirmCode", code);
            comm.Parameters.AddWithValue("@ConfirmOtp", otp);
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                    bOutPut = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return bOutPut;
        }
        
        public static bool ConfirmEkyc(string code)
        {
            string CRMStrConfig = ConfigurationManager.ConnectionStrings["CRM"].ToString();
            SqlConnection SqlConn = new SqlConnection(CRMStrConfig);
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("TVSI_sCONFIRM_EKYC_API_MOBILE", SqlConn);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@ConfirmCode", code);
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                {
                    if(int.Parse(drd["ReturnCode"].ToString()) == 1)
                        return true;
                    else
                        return false;
                }
                    
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return false;
        }
        public static bool ExistsCMNDinCRM(string strCMND, string AStr)
        {
            SqlConnection SqlConn = new SqlConnection(AStr);
            bool bOutPut = false;
            SqlDataReader drd = null;

            SqlCommand comm =
                new SqlCommand(
                    "select * from TVSI_DANG_KY_MO_TAI_KHOAN where so_cmnd=@so_cmnd and trang_thai_tk <> 999", SqlConn);
            comm.Parameters.Add(new SqlParameter("@so_cmnd", strCMND));
            comm.CommandType = CommandType.Text;
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                    bOutPut = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return bOutPut;
        }
        
        public static bool ExistsCMND(string strCMND, string AStr)
        {
            SqlConnection SqlConn = new SqlConnection(AStr);
            bool bOutPut = false;
            SqlDataReader drd = null;
            SqlParameter para_cmnd = new SqlParameter("@so_cmnd", strCMND);
            SqlCommand comm = new SqlCommand("select so_cmnd_passport from TVSI_TAI_KHOAN_KHACH_HANG where trang_thai = 1 and so_cmnd_passport ='" + strCMND + "'", SqlConn);
            comm.Parameters.Add(para_cmnd);
            comm.CommandType = CommandType.Text;
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                    bOutPut = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return bOutPut;
        }
        
        public static List<TvsiBranch> BranchListTvsi()
        {
            string[] IdBranchTransaction = ConfigurationManager.AppSettings["IdBranchTransaction"].Split(',');
            var addBranch = new List<TvsiBranch>();
            for (int i = 0; i < IdBranchTransaction.Length; i++)
            {
                if (IdBranchTransaction[i] == IdBranchTransaction[0])
                {
                    addBranch.Add(new TvsiBranch() {BranchName ="Hà Nội",BranchNo = IdBranchTransaction[i]});
                }
                else if (IdBranchTransaction[i] == IdBranchTransaction[1])
                {
                    addBranch.Add(new TvsiBranch() {BranchName ="Hồ Chí Minh",BranchNo = IdBranchTransaction[i]});
                }
            }
            return addBranch;
        }
        
        public static string GetNumberSaleID(string strSales_ID, string EMSStr)
        {
            var getSaleStr = string.Empty;
            using (SqlConnection sqlConnection = new SqlConnection(EMSStr))
            using (SqlCommand comm = new SqlCommand("TVSI_sGET_ID_SALE", sqlConnection))
            {
                SqlDataReader drd;
                comm.Parameters.Add("@id_he_thong", strSales_ID);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Connection.Open();
                comm.ExecuteNonQuery();
                drd = comm.ExecuteReader();
                while (drd.Read())
                {
                    getSaleStr = drd["id_he_thong"].ToString();
                }

                comm.Connection.Close();
                comm.Connection.Dispose();
            }
            return getSaleStr;
        }

        /*public static bool SendEmailToCS(EkycSaveUserInfo ekycmodel)
        {
            if (string.IsNullOrEmpty(ekycmodel.SaleID))
            {
                try
                {
                    var para_Content = ReturnNoiDungMail(ekycmodel);
                    var para_Subject = "TVSI: Đăng ký mở tài khoản trực tuyến";
                    var strEmail = ConfigurationManager.AppSettings["SendEmailEkyc"].ToString();
                    var arrListStr = strEmail.Split(',');
                    var IdBranchTransaction = ConfigurationManager.AppSettings["IdBranchTransaction"].Split(',');
                    if (ekycmodel.TvsiBranch == IdBranchTransaction[0] ||
                        ekycmodel.TvsiBranch == IdBranchTransaction[2])
                    {
                        TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(para_Subject,
                            para_Content, arrListStr[0], 0);
                        TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(para_Subject,
                            para_Content, arrListStr[1], 0);
                        TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(para_Subject,
                            para_Content, arrListStr[2], 0);
                        TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(para_Subject,
                            para_Content, arrListStr[3], 0);
                    }
                    else
                    {
                        TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(para_Subject,
                            para_Content, arrListStr[4], 0);
                    }
                    
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                    
                }
            }
            return false;
        }*/
        public static int SendEmailOpenEKYC(string user, string so_dien_thoai,  string email,  string ConfirmCode, string  ConfirmSms, string url)
        {
            
            string smsMessage = ConfigurationManager.AppSettings["SMSMessage"].ToString();
            var subject = "Chứng Khoán Tân Việt(TVSI): Xác thực đăng ký mở tài khoản của Ông(Bà): " + user;

            var templateFile = "";
            templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_EKYCTemplateConfirm.html");
            var content = "";

            using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (var readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
            }
            if (string.IsNullOrEmpty(content))
            {
                throw new Exception("Khong load duoc noi dung email template");
            }
            var contentData = new EmailData
            {
                FullName = user,
                url = url,
                ConfirmCode = ConfirmCode,
                ConfirmSms = ConfirmSms
            };
            content = ReplaceEmailContent(content, contentData);
            DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
            /*
            DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_SMS_INSERT(smsMessage + ConfirmSms + "Vui long khong cung cap ma OTP cho ai vi bat ki ly do nao. Tran Trong!", so_dien_thoai, 0);
            */
            return 1;
            
            
        }

        public static int SendEmailReceiveContract(string user,string address, string email,string cardid,int margin)
        {
            try
            {
                var URLCRM = ConfigurationManager.AppSettings["CRM_URL"].ToString();
                var subject = "Chứng Khoán Tân Việt(TVSI): Xác thực đăng ký mở tài khoản của Ông(Bà): " + user;
                var templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_EKYCTemplateContract.html");
                var content = "";

                using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    using (var readFileStr = new StreamReader(readFile))
                    {
                        content = readFileStr.ReadToEnd();
                    }
                }
                if (string.IsNullOrEmpty(content))
                {
                    throw new Exception("Khong load duoc noi dung email template");
                }
                var contentData = new EmailData
                {
                    FullName = user,
                    AddressReceContract = address,
                    type = margin,
                    url = URLCRM,
                    CustId = cardid
                };
                content = ReplaceEmailConfirmContent(content, contentData);
                DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
                return 1;
            }
            catch (Exception e)
            {
                _log.Error(typeof(EkycUtils), e);
                _log.Info(typeof(EkycUtils), e);
                return -1;
            }

        }


        public static int SendEmailContractEKYC(UserCRM model)
        {
            try
            {
                var urlCrm = ConfigurationManager.AppSettings["CRM_URL"].ToString();
                string smsMessage = ConfigurationManager.AppSettings["SMSMessage"].ToString();
                var subject = "CHÚC MỪNG QUÝ KHÁCH MỞ TÀI KHOẢN GIAO DỊCH CHỨNG KHOÁN THÀNH CÔNG TẠI TVSI";

                var templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_EKYCTemplateContract.html");

                var content = "";

                using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    using (var readFileStr = new StreamReader(readFile))
                    {
                        content = readFileStr.ReadToEnd();
                    }
                }
                if (string.IsNullOrEmpty(content))
                {
                    throw new Exception("Khong load duoc noi dung email template");
                }
                var contentData = new EmailData
                {
                    FullName = model.FullName,
                    DateOfBirth = model.DateOfBirth,
                    CardID = model.CardID,
                    IssueDate = model.IssueDate,
                    CardIssue = model.CardIssue,
                    TypeAccount = model.TypeAccount,
                    url = urlCrm
                };
                content = ReplaceEmailContact(content, contentData);
                TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, model.Email, 0);
                return 1;
            }
            catch (Exception ex)
            {
                throw new Exception();
               
            }

        }        
        
        public static int SendEmailContactMargin(string cardID, string custcode, string email,string fullname)
        {
            var urlCrm = ConfigurationManager.AppSettings["CRM_URL"];
                var subject = "CHÚC MỪNG QUÝ KHÁCH MỞ TÀI KHOẢN GIAO DỊCH KÝ QUỸ THÀNH CÔNG TẠI TVSI";

                var templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_EKYCTemplateContractMargin.html");

                var content = "";

                using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    using (var readFileStr = new StreamReader(readFile))
                    {
                        content = readFileStr.ReadToEnd();
                    }
                }
                if (string.IsNullOrEmpty(content))
                {
                    _log.Error($"SendEmailContactMargin Loi khong lay duoc noi dung mail");

                }
                var contentData = new EmailData
                {
                    FullName =  fullname,
                    CardID = cardID,
                    CustCode = custcode,
                    url = urlCrm,
                    
                };
                content = ReplaceEmailContractMargin(content, contentData);
                DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
                return 1;
        }  
        
        
        /*public static List<string> CutTextCardIssueCompare(string cardIssue)
        {
            var listCardIssue = new List<string>();
            var checknoicap = false;
            var listnoicap = new List<string>();
            listnoicap.Add("trật tự");
            listnoicap.Add("xã hội"); ;
            listnoicap.Add("trật tự xã hội");
            listnoicap.Add("cục trưởng cục cảnh sát quản lý hành chính về trật tự xã hội");

            var listnoicap1 = new List<string>();
            listnoicap1.Add("ĐKQL");
            listnoicap1.Add("DLQG");
            listnoicap1.Add("về dân cư");
            listnoicap1.Add("cục trưởng cục cảnh sát đkql cư trú và dlqg về dân cư");
            if (!string.IsNullOrEmpty(cardIssue))
            {
                for (int i = 0; i < listnoicap.Count; i++)
                {
                    if (cardIssue.Trim().ToLower().IndexOf(listnoicap[i].Trim().ToLower(),StringComparison.OrdinalIgnoreCase) >=0)
                    {
                        listCardIssue.Add("CCS QLHC về TTXH");
                        listCardIssue.Add("Cục CCS QLHC về TTXH");
                        listCardIssue.Add("Cục CSQLHC về TTXH");
                        checknoicap = true;
                        break;
                    }
                }
                if (checknoicap == false)
                {
                    for (int i = 0; i < listnoicap1.Count; i++)
                    {
                        if (cardIssue.Trim().ToLower().IndexOf(listnoicap1[i].Trim().ToLower(),StringComparison.OrdinalIgnoreCase)>=0)
                        {
                            listCardIssue.Add("CCS ĐKQL Cư trú và DLQG về DC");
                            listCardIssue.Add("Cục CS ĐKQL Cư trú và DLQG về DC");
                            break;
                        }
                    }
                }
            }

            return listCardIssue;
        }*/
        public static string ReturnNoiDungMail(EkycSaveUserInfoRequest model)
        {
            string strReturn = string.Empty;
            strReturn += "<table width=\"760px\">";
            strReturn += "<tr>";
            strReturn +=
                "<td style=\"width:680px; height:60px; background-color:#ff0000;border: 0px solid #ff0000;border-top-left-radius: 50px; border-bottom-right-radius:50px; \">";
            strReturn +=
                "<p style=\"color:white; font-size:18px; font-weight:bold; padding-left:40px; font-family:Tahoma;\">";
            strReturn += "THÔNG BÁO <br />";
            strReturn += "ĐĂNG KÝ THÔNG TIN THÀNH CÔNG";
            strReturn += "</p>";
            strReturn += "</td>";
            strReturn += "<td style=\"padding-left:20px;\">";
            strReturn +=
                "<img src=\"http://www.tvsi.com.vn/images/saoke/logo-tvsi-saokecuoithang.jpg\" height=\"60\" />";
            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn +=
                "<td colspan=\"2\" style=\"text-align:justify; width:700px; padding-left:30px; padding-right:30px; padding-top:20px; padding-bottom:20px; font-family:Tahoma; font-size:10pt;\">";
            strReturn += "<b> Kính gửi: Mr/Ms " + model.CustomerName + "</b><br />";
            strReturn +=
                "Quý khách đã đăng ký mở tài khoản giao dịch chứng khoán tại Công ty Cổ phần Chứng khoán Tân Việt (TVSI) thành công với các thông tin cụ thể như sau:<br />";
            strReturn += "<p style=\"padding-left:30px; font-weight:bold;\">";
            strReturn += "Họ tên: " + model.CustomerName+ "<br />";
            strReturn += "Ngày sinh: " + model.Birthday + " <br />";
            strReturn += "Số CMND: " + model.CardID + "<br />";
            strReturn += "Ngày cấp: " + model.IssueDate + "<br />";
            strReturn += "Số điện thoại: " + model.Mobile + "<br />";
            strReturn += "Email: " + model.Email + "<br />";
            strReturn += "Địa chỉ: " + model.Address_02 + "<br />";
            strReturn += "</p>";
            strReturn += "Cảm ơn Quý khách đã quan tâm và sử dụng sản phẩm – dịch vụ của TVSI<br />";
            strReturn += "<b>Chúng tôi sẽ liên hệ trong vòng 01 ngày làm việc để hoàn tất việc mở tài khoản.</b><br />";
            strReturn +=
                "Để được hỗ trợ về mở tài khoản, vui lòng liên hệ tổng đài chăm sóc khách hàng 1900 1885 của TVSI<br /><br />";
            strReturn += "Trân trọng cảm ơn.<br />";
            strReturn += "<b>Chứng khoán Tân Việt</b><br />";
            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\">";
            strReturn += "<img src=\"http://www.tvsi.com.vn/images/dktk/footer_dangky_tai_khoan_email_vn.jpg\" />";
            strReturn += "</td>";
            strReturn += "</tr>";

            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\" style=\"height:10px;\">&nbsp;</td>";
            strReturn += "</tr>";

            strReturn += "<tr>";
            strReturn +=
                "<td style=\"width:680px; height:60px; background-color:#ff0000;border: 0px solid #ff0000;border-top-left-radius: 50px; border-bottom-right-radius:50px; \">";
            strReturn +=
                "<p style=\"color:white; font-size:20px; font-weight:bold; padding-left:40px; font-family:Tahoma;\">";
            strReturn += "SUCCESFUL REGISTRATION";

            strReturn += "</p>";
            strReturn += "</td>";
            strReturn += "<td style=\"padding-left:20px;\">";
            strReturn +=
                "<img src=\"http://www.tvsi.com.vn/images/saoke/logo-tvsi-saokecuoithang.jpg\" height=\"60\" />";
            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn +=
                "<td colspan=\"2\" style=\"text-align:justify; width:700px; padding-left:30px; padding-right:30px; padding-top:20px; padding-bottom:20px;font-family:Tahoma; font-size:10pt;\">";
            strReturn += "<b>Dear Mr/Ms " + model.CustomerName + "</b><br />";
            strReturn +=
                "You have finished the registration open account at Tan Viet Securities Incorporation (TVSI):<br />";
            strReturn += "<p style=\"padding-left:30px; font-weight:bold;\">";

            strReturn += "Name: " + model.CustomerName + "<br />";
            strReturn += "Date of birth: " + model.Birthday + "<br />";
            strReturn += "ID card: " + model.CardID + "<br />";
            strReturn += "Date Issue: " + model.IssueDate + "<br />";
            strReturn += "Mobile phone: " + model.Mobile + "<br />";
            strReturn += "Email: " + model.Email + "<br />";
            strReturn += "Address: " + model.Address_02 + "<br />";
            strReturn += "</p>";
            strReturn += "TVSI customer service representative will contact you within 24 hours working day.<br />";
            strReturn += "Please kindly contact to TVSI Contact Center 1900 1885 if you have any questions.<br />";
            strReturn += "Thank you & Best Regard<br />";
            strReturn += "TVSI<br />";

            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\">";
            strReturn += "<img src=\"http://www.tvsi.com.vn/images/dktk/footer_dangky_tai_khoan_email_vn.jpg\" />";
            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "</table>";
            return strReturn.ToString();
        }

        
        private static string ReplaceEmailContent(string content, EmailData data)
        {
            
            content = content.Replace("{FullName}", data.FullName);
            content = content.Replace("{url}", data.url);
            content = content.Replace("{confirmCode}", data.ConfirmCode);
            content = content.Replace("{confirmSms}", data.ConfirmSms);
            return content;
        }
        private static string ReplaceEmailConfirmContent(string content, EmailData data)
        {
            content = content.Replace("{FullName}", data.FullName);
            content = content.Replace("{BranchAddress}", data.AddressReceContract);
            content = content.Replace("{url}", data.url);
            content = content.Replace("{custid}", data.CustId);
            /*if (data.type == 0)
            {
                content = content.Replace("{hide}", "display:none");

            }*/
              
            
            return content;
        }
        
        private static string ReplaceEmailContact(string content, EmailData data)
        {
            
            content = content.Replace("{Full_Name}", data.FullName);
            content = content.Replace("{Date_of_birth}", data.DateOfBirth);
            content = content.Replace("{Card_ID}", data.CardID);
            content = content.Replace("{Issue_date}", data.IssueDate);
            content = content.Replace("{Issuer}", data.CardIssue);
            content = content.Replace("{Text_Margin}", string.IsNullOrEmpty(data.TypeAccount) ? "Tài khoản giao dịch thông thường" : "Tài khoản giao dịch thông thường, Tài khoản giao dịch ký quỹ " );
            content = content.Replace("{Url}", data.url);
            content = content.Replace("{CustId}", Base64Encode(data.CardID));
            return content;
        }

        private static string ReplaceEmailContractMargin(string content, EmailData data)
        {
            content = content.Replace("{CustId}", Base64Encode(data.CardID));
            content = content.Replace("{custcode}", data.CustCode);
            content = content.Replace("{FullName}", data.FullName);
            content = content.Replace("{custcodemargin}", data.CustCode + 6);
            content = content.Replace("{Url}", data.url);
            return content;
        }

        public static string CutTextCardIssue(string cardIssue)
        {
            var checknoicap = false;
            var listnoicap = new List<string>();
            listnoicap.Add("trật tự");
            listnoicap.Add("xã hội"); ;
            listnoicap.Add("trật tự xã hội");
            listnoicap.Add("cục trưởng cục cảnh sát quản lý hành chính về trật tự xã hội");

            var listnoicap1 = new List<string>();
            listnoicap1.Add("ĐKQL");
            listnoicap1.Add("DLQG");
            listnoicap1.Add("về dân cư");
            listnoicap1.Add("cục trưởng cục cảnh sát đkql cư trú và dlqg về dân cư");
            if (!string.IsNullOrEmpty(cardIssue))
            {
                for (int i = 0; i < listnoicap.Count; i++)
                {
                    if (cardIssue.ToLower().Contains(listnoicap[i]))
                    {
                        cardIssue = "CCS QLHC về TTXH";
                        checknoicap = true;
                        break;
                    }
                }
                if (checknoicap == false)
                {
                    for (int i = 0; i < listnoicap1.Count; i++)
                    {
                        if (cardIssue.ToLower().Contains(listnoicap1[i]))
                        {
                            cardIssue = "CCS ĐKQL Cư trú và DLQG về DC";
                            break;
                        }
                    }
                }
            }

            return cardIssue;
        }
        
        public static List<string> CutTextCardIssueCompare(string cardIssue)
        {
            var listCardIssue = new List<string>();
            var checknoicap = false;
            var listnoicap = new List<string>();
            listnoicap.Add("trật tự");
            listnoicap.Add("xã hội");
            listnoicap.Add("trật tự xã hội");
            listnoicap.Add("cục trưởng cục cảnh sát quản lý hành chính về trật tự xã hội");

            var listnoicap1 = new List<string>();
            listnoicap1.Add("ĐKQL");
            listnoicap1.Add("DLQG");
            listnoicap1.Add("về dân cư");
            listnoicap1.Add("cục trưởng cục cảnh sát đkql cư trú và dlqg về dân cư");
            if (!string.IsNullOrEmpty(cardIssue))
            {
                for (int i = 0; i < listnoicap.Count; i++)
                {
                    if (cardIssue.Trim().ToLower().IndexOf(listnoicap[i].Trim().ToLower(),StringComparison.OrdinalIgnoreCase) >=0)
                    {
                        listCardIssue.Add("CCS QLHC về TTXH");
                        listCardIssue.Add("Cục CS QLHC về TTXH");
                        listCardIssue.Add("Cục CSQLHC về TTXH");
                        listCardIssue.Add("Cục QLHC về TTXH");
                        checknoicap = true;
                        break;
                    }
                }
                if (checknoicap == false)
                {
                    for (int i = 0; i < listnoicap1.Count; i++)
                    {
                        if (cardIssue.Trim().ToLower().IndexOf(listnoicap1[i].Trim().ToLower(),StringComparison.OrdinalIgnoreCase)>=0)
                        {
                            listCardIssue.Add("CCS ĐKQL Cư trú và DLQG về DC");
                            listCardIssue.Add("Cục CS ĐKQL Cư trú và DLQG về DC");
                            break;
                        }
                    }
                }
            }

            return listCardIssue;
        }
        
        public static string ToTitleCase(this string title)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title.ToLower()); 
        }
        
        /// <summary>
        /// This method is used to encode base64
        /// </summary>
        /// <param name="plainText">param encode</param>
        public static string Base64Encode(string plainText) {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        
        
                
        /// <summary>
        /// This method is used to Decode base64
        /// </summary>
        /// <param name="base64EncodedData">param decode </param>
        public static string Base64Decode(string base64EncodedData) {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        
        /// <summary>
        /// format phonenumber
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static string formatPhoneNumber(string phone)
        {
            string sdt = phone;
            string[] listChar = {".",","," ","+","-"};
            foreach(string ch in listChar)
            {
                if (sdt.Contains(ch))
                {
                    sdt = sdt.Replace(ch, "");
                }
            }
            return sdt;
        }
        /// <summary>
        /// lay trang thai VNPAY tra ve
        /// </summary>
        /// <param name="strStatus"></param>
        /// <returns></returns>
        public static string GetStatus(string strStatus)
        {
            string result = "";
            switch (strStatus)
            {
                case "00":
                    result = "Thành công";
                    break;
                case "01":
                    result = "Sai số điện thoại";
                    break;
                case "02":
                    result = "Độ dài tin nhắn không hợp lệ";
                    break;
                case "04":
                    result = "Sai thông tin xác thực";
                    break;
                case "05":
                    result = "Mất kết nối đến nhà cung cấp";
                    break;
                case "06":
                    result = "Địa chỉ IP không được phép truy cập";
                    break;
                case "08":
                    result = "Timeout";
                    break;
                case "10":
                    result = "Tin nhắn bị trùng lặp";
                    break;
                case "11":
                    result = "Sai loại tin nhắn";
                    break;
                case "12":
                    result = "Không hỗ trợ tin Unicode";
                    break;
                case "80":
                    result = "Không tìm thấy đối tác";
                    break;
                case "81":
                    result = "Đối tác chưa được hỗ trợ";
                    break;
                case "82":
                    result = "Không tìm thấy nhà cung cấp";
                    break;
                case "83":
                    result = "Nhà cung cấp chưa được hỗ trợ";
                    break;
                case "84":
                    result = "Chưa định tuyến dịch vụ";
                    break;
                case "85":
                    result = "Sai sender";
                    break;
                case "86":
                    result = "Sai từ khóa";
                    break;
                case "87":
                    result = "Sai mẫu tin nhắn";
                    break;
                case "88":
                    result = "Thuê bao chưa được mã hóa";
                    break;
                case "89":
                    result = "Thuê bao mạng khác Viettel chưa được mã hóa";
                    break;
                case "90":
                    result = "Tin nhắn trùng lặp";
                    break;
                case "97":
                    result = "Sai dữ liệu đầu vào";
                    break;
                case "99":
                    result = "Lỗi ngoại lệ";
                    break;
                default:
                    result = "Lỗi Không xác định";
                    break;
            }

            return result;
        }
        
    }
    public static class UtilsBranchID
    {
        public const string DVGDHS = "12";
        public const string DVGDHCM = "02";
        public const string DVKH = "54";
        public const string DVDTHS = "53-002";
        public const string DVDTHCM = "53-001";
    }
    
    public class EmailData
    {
        public string FullName { get; set; }
        public string email { get; set; }
        public string url { get; set; }
        public string ConfirmCode { get; set; }
        public string ConfirmSms { get; set; }
        public string dr { get; set; }
        public string TypeAcc { get; set; }
        public string CustCode { get; set; }
        public string CustId { get; set; }
        public  string DateOfBirth { get; set; }
        public  string CardID { get; set; }
        public  string IssueDate { get; set; }
        public  string CardIssue { get; set; }
        public  string TypeAccount { get; set; }
        public int type { get; set; }
        public string AddressReceContract { get; set; }
    }
    


}