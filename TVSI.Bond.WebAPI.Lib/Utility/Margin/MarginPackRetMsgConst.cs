﻿namespace TVSI.Bond.WebAPI.Lib.Utility.Margin
{
    public class MarginPackRetMsgConst
    {
        public const string MG_1000_SAVE_FAIL = "Lưu không thành công"; // Lưu ko thành công
        public const string MG_100_EMPTY_DATA = "Không tìm thấy dữ liệu"; // Lưu ko thành công
    }
}