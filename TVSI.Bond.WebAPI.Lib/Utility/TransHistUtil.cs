﻿using System;
using System.Globalization;

namespace TVSI.Bond.WebAPI.Lib.Utility
{
    public class TransHistUtil
    {
        public static string GetContent(string transDate, string remark, string price, string refType, string custType, 
            string transType2, string stockType, string stockType2)
        {
            var stockTransNo = GetStockTransNo(refType, custType, transType2, stockType, stockType2);
            string str1 = string.Empty;
            string str2 = string.IsNullOrEmpty(remark) || (stockTransNo == STOCK_TRANS_NO.Opening)   
                || (stockTransNo == STOCK_TRANS_NO.Closing) ? string.Empty : " (" + remark + ")";
            switch (stockTransNo)
            {
                case STOCK_TRANS_NO.Closing:
                    str1 = ResourceFile.TransactionHistModule.TO_History_SaokeCK_ClosingBalance + str2;
                    break;
                case STOCK_TRANS_NO.Opening:
                    str1 = ResourceFile.TransactionHistModule.TO_History_SaokeCK_OpeningBalance + str2;
                    break;
                case STOCK_TRANS_NO.DE:
                    str1 = ResourceFile.TransactionHistModule.Deposit + str2;
                    break;
                case STOCK_TRANS_NO.WD:
                    str1 = ResourceFile.TransactionHistModule.WithDraw + str2;
                    break;
                case STOCK_TRANS_NO.TI:
                    str1 = ResourceFile.TransactionHistModule.TransferFromOtherBroker + str2;
                    break;
                case STOCK_TRANS_NO.TO:
                    str1 = ResourceFile.TransactionHistModule.TransferToOtherBroker + str2;
                    break;
                case STOCK_TRANS_NO.TCTransferIn:
                    str1 = ResourceFile.TransactionHistModule.TransferFromOtherBroker + str2;
                    break;
                case STOCK_TRANS_NO.TCSellOddLotShares:
                    str1 = ResourceFile.TransactionHistModule.SellOddLotShares + str2;
                    break;
                case STOCK_TRANS_NO.GCRegisterSharesToSellable:
                    str1 = ResourceFile.TransactionHistModule.RegisterSharesToSellable + str2;
                    break;
                case STOCK_TRANS_NO.GCPledgeToSellable:
                    str1 = ResourceFile.TransactionHistModule.PledgeToSellable + str2;
                    break;
                case STOCK_TRANS_NO.GCRestrictedSharesToSellable:
                    str1 = ResourceFile.TransactionHistModule.LimitedTransferToSellable + str2;
                    break;
                case STOCK_TRANS_NO.GCRegisterShares:
                    str1 = ResourceFile.TransactionHistModule.Deposit + str2;
                    break;
                case STOCK_TRANS_NO.GCPledgeShares:
                    str1 = ResourceFile.TransactionHistModule.PledgeShares + str2;
                    break;
                case STOCK_TRANS_NO.GCRestrictedShares:
                    str1 = ResourceFile.TransactionHistModule.LimitedTransfer + str2;
                    break;
                case STOCK_TRANS_NO.XR:
                    str1 = getDoubleFromString(price) > 0.0 ? 
                        ResourceFile.TransactionHistModule.RightBuyStock + str2 : 
                        ResourceFile.TransactionHistModule.RightStockDividend_Bonus + str2;
                    break;
                case STOCK_TRANS_NO.RX:
                    str1 = getDoubleFromString(price) > 0.0 ? 
                        ResourceFile.TransactionHistModule.ReceiveRightStock + str2 : ResourceFile.TransactionHistModule.RightStockDividend_Bonus + str2;
                    break;
                case STOCK_TRANS_NO.BU:
                    str1 = ResourceFile.TransactionHistModule.BuyDate + " " + ConvertDateByLang(transDate) 
                        + " " + ResourceFile.TransactionHistModule.TO_History_StockTrans_Price 
                        + " " + FormatNumber(price, "0") + str2;
                    break;
                case STOCK_TRANS_NO.SE:
                    str1 = ResourceFile.TransactionHistModule.SellDate2 + " " + 
                        ConvertDateByLang(transDate) + " " + ResourceFile.TransactionHistModule.TO_History_StockTrans_Price 
                        + " " + FormatNumber(price, "0") + str2;
                    break;
                case STOCK_TRANS_NO.EXRightStocks:
                    str1 = ResourceFile.TransactionHistModule.RightStocks + str2;
                    break;
                case STOCK_TRANS_NO.EXReceiveRightStocks:
                    str1 = getDoubleFromString(price) > 0.0 ? 
                        ResourceFile.TransactionHistModule.ReceiveRightStocks + str2 : 
                        ResourceFile.TransactionHistModule.RightStockDividend_Bonus + str2;
                    break;
                case STOCK_TRANS_NO.XE:
                    str1 = ResourceFile.TransactionHistModule.RightStocks2 + str2;
                    break;
                case STOCK_TRANS_NO.XNNewShares:
                    str1 = ResourceFile.TransactionHistModule.NewShares + str2;
                    break;
                case STOCK_TRANS_NO.XNRegisterSharesToSellable:
                    str1 = ResourceFile.TransactionHistModule.RegisterSharesToSellable + str2;
                    break;
                case STOCK_TRANS_NO.GCSellableToRegisterShares:
                    str1 = ResourceFile.TransactionHistModule.SellableToRegisterShares + str2;
                    break;
                case STOCK_TRANS_NO.GCSellablePledge:
                    str1 = ResourceFile.TransactionHistModule.SellableToPledge + str2;
                    break;
                case STOCK_TRANS_NO.GCSellableToRestrictedShares:
                    str1 = ResourceFile.TransactionHistModule.SellableToLimitedTransfer + str2;
                    break;
                case STOCK_TRANS_NO.TCTransferInDown:
                    str1 = ResourceFile.TransactionHistModule.TransferToOtherBroker + str2;
                    break;
                case STOCK_TRANS_NO.GCRegisterSharesToRestrictedShares:
                    str1 = ResourceFile.TransactionHistModule.RegisterSharesToLimitedTransfer + str2;
                    break;
                case STOCK_TRANS_NO.GCRestrictedSharesToRegisterShares:
                    str1 = ResourceFile.TransactionHistModule.LimitedTransferToRegisterShares + str2;
                    break;
            }
            return str1;
        }

        public static string FormatNumber(string sNum, string defaultNum)
        {
            double result;
            if (!double.TryParse(sNum, NumberStyles.Float, (IFormatProvider)(new CultureInfo("en-US")), out result))
                return defaultNum;
            return result.ToString("##,###0.##", (IFormatProvider)(new CultureInfo("en-US")));
        }

        public static string ConvertDateByLang(string date)
        {
            if (date.Trim() == "")
                return "";
            
            return date.Substring(6, 2) + "/" + date.Substring(4, 2) + "/" + date.Substring(0, 4);
            
        }

        public static double getDoubleFromString(string InitValue)
        {
            InitValue = InitValue.Replace(",", "");
            double result = 0.0;
            if (double.TryParse(InitValue, NumberStyles.Float, (IFormatProvider)(new CultureInfo("en-US")), out result))
                return result;
            return 0.0;
        }

        public static STOCK_TRANS_NO GetStockTransNo(string RefType, string CustType, string TransType2,
            string StockType, string StockType2)
        {
            REF_STOCK_TYPE refStockType = (REF_STOCK_TYPE)Enum.Parse(typeof(REF_STOCK_TYPE), RefType, true);
            STOCK_TRANS_NO stockTransNo = STOCK_TRANS_NO.Opening;
            switch (refStockType)
            {
                case REF_STOCK_TYPE.Opening:
                    stockTransNo = STOCK_TRANS_NO.Opening;
                    break;
                case REF_STOCK_TYPE.Closing:
                    stockTransNo = STOCK_TRANS_NO.Closing;
                    break;
                case REF_STOCK_TYPE.DE:
                    stockTransNo = STOCK_TRANS_NO.DE;
                    break;
                case REF_STOCK_TYPE.WD:
                    stockTransNo = STOCK_TRANS_NO.WD;
                    break;
                case REF_STOCK_TYPE.TI:
                    stockTransNo = STOCK_TRANS_NO.TI;
                    break;
                case REF_STOCK_TYPE.TO:
                    stockTransNo = STOCK_TRANS_NO.TO;
                    break;
                case REF_STOCK_TYPE.TC:
                    stockTransNo = CustType != "1" ? STOCK_TRANS_NO.TCSellOddLotShares 
                        : (TransType2 == "1" ? STOCK_TRANS_NO.TCTransferIn : STOCK_TRANS_NO.TCTransferInDown);
                    break;
                case REF_STOCK_TYPE.GC:
                    if (StockType == "02" && StockType2 == "47")
                    {
                        stockTransNo = TransType2 == "1" ? STOCK_TRANS_NO.GCPledgeToSellable : STOCK_TRANS_NO.GCSellablePledge;
                        break;
                    }
                    if (StockType == "02" && StockType2 == "49")
                    {
                        stockTransNo = TransType2 == "1" ? STOCK_TRANS_NO.GCRestrictedSharesToSellable : STOCK_TRANS_NO.GCSellableToRestrictedShares;
                        break;
                    }
                    if (StockType == "02" && StockType2 == "03")
                    {
                        stockTransNo = TransType2 == "1" ? STOCK_TRANS_NO.GCRegisterSharesToSellable : STOCK_TRANS_NO.GCSellableToRegisterShares;
                        break;
                    }
                    if ((StockType == "02" || StockType == "08" || (StockType == "47" || StockType == "49")) && (StockType2 == "C2" || StockType2 == "T2" || (StockType2 == "W2" || StockType2 == "G2")))
                    {
                        stockTransNo = STOCK_TRANS_NO.OtherToRestrictedShares;
                        break;
                    }
                    if (StockType == "02")
                    {
                        stockTransNo = TransType2 == "1" ? STOCK_TRANS_NO.GCPledgeToSellable : STOCK_TRANS_NO.GCSellablePledge;
                        break;
                    }
                    if (StockType == "03" && StockType2 == "49")
                    {
                        stockTransNo = TransType2 == "1" ? STOCK_TRANS_NO.GCRestrictedSharesToRegisterShares : STOCK_TRANS_NO.GCRegisterSharesToRestrictedShares;
                        break;
                    }
                    if (StockType == "03")
                    {
                        stockTransNo = TransType2 == "1" ? STOCK_TRANS_NO.GCRegisterShares : STOCK_TRANS_NO.GCSellableToRegisterShares;
                        break;
                    }
                    if (StockType == "47")
                    {
                        stockTransNo = STOCK_TRANS_NO.GCPledgeShares;
                        break;
                    }
                    if (StockType == "49")
                    {
                        stockTransNo = STOCK_TRANS_NO.GCRestrictedShares;
                    }
                    break;
                case REF_STOCK_TYPE.XR:
                    stockTransNo = STOCK_TRANS_NO.XR;
                    break;
                case REF_STOCK_TYPE.RX:
                    stockTransNo = STOCK_TRANS_NO.RX;
                    break;
                case REF_STOCK_TYPE.BU:
                    stockTransNo = STOCK_TRANS_NO.BU;
                    break;
                case REF_STOCK_TYPE.SE:
                    stockTransNo = STOCK_TRANS_NO.SE;
                    break;
                case REF_STOCK_TYPE.DL:
                    stockTransNo = STOCK_TRANS_NO.DL1;
                    break;
                case REF_STOCK_TYPE.EX:
                    if (StockType == "02")
                    {
                        stockTransNo = STOCK_TRANS_NO.EXReceiveRightStocks;
                        break;
                    }
                    if (StockType == "12")
                    {
                        stockTransNo = STOCK_TRANS_NO.EXRightStocks;
                    }
                    break;
                case REF_STOCK_TYPE.NC:
                    stockTransNo = STOCK_TRANS_NO.NC1;
                    break;
                case REF_STOCK_TYPE.NT:
                    stockTransNo = STOCK_TRANS_NO.NT1;
                    break;
                case REF_STOCK_TYPE.TL:
                    stockTransNo = STOCK_TRANS_NO.TL1;
                    break;
                case REF_STOCK_TYPE.XE:
                    stockTransNo = STOCK_TRANS_NO.XE;
                    break;
                case REF_STOCK_TYPE.XN:
                    if (StockType == "02")
                    {
                        stockTransNo = STOCK_TRANS_NO.XNRegisterSharesToSellable;
                        break;
                    }
                    if (StockType == "08")
                    {
                        stockTransNo = STOCK_TRANS_NO.XNNewShares;
                    }
                    break;
                case REF_STOCK_TYPE.BD:
                    stockTransNo = STOCK_TRANS_NO.BD;
                    break;
                case REF_STOCK_TYPE.SD:
                    stockTransNo = STOCK_TRANS_NO.SD;
                    break;
            }
            return stockTransNo;
        }

        public enum STOCK_TRANS_NO
        {
            Closing = -1,
            Opening = 0,
            DE = 1,
            WD = 2,
            TI = 3,
            TO = 4,
            TCTransferIn = 5,
            TCSellOddLotShares = 6,
            GCRegisterSharesToSellable = 7,
            GCPledgeToSellable = 8,
            GCRestrictedSharesToSellable = 9,
            GCRegisterShares = 10, // 0x0000000A
            GCPledgeShares = 11, // 0x0000000B
            GCRestrictedShares = 12, // 0x0000000C
            XR = 13, // 0x0000000D
            RX = 14, // 0x0000000E
            BU = 15, // 0x0000000F
            SE = 16, // 0x00000010
            DL1 = 17, // 0x00000011
            DL2 = 18, // 0x00000012
            DL3 = 19, // 0x00000013
            DL4 = 20, // 0x00000014
            EXRightStocks = 21, // 0x00000015
            EXReceiveRightStocks = 22, // 0x00000016
            NC1 = 23, // 0x00000017
            NC2 = 24, // 0x00000018
            NC3 = 25, // 0x00000019
            NT1 = 26, // 0x0000001A
            NT2 = 27, // 0x0000001B
            TL1 = 28, // 0x0000001C
            TL2 = 29, // 0x0000001D
            XE = 30, // 0x0000001E
            XNNewShares = 31, // 0x0000001F
            XNRegisterSharesToSellable = 32, // 0x00000020
            GCSellablePledge = 33, // 0x00000021
            GCSellableToRegisterShares = 34, // 0x00000022
            GCSellableToRestrictedShares = 35, // 0x00000023
            TCTransferInDown = 36, // 0x00000024
            BD = 37, // 0x00000025
            SD = 38, // 0x00000026
            GCRegisterSharesToRestrictedShares = 39, // 0x00000027
            GCRestrictedSharesToRegisterShares = 40, // 0x00000028
            OtherToRestrictedShares = 41, // 0x00000029
        }

        public enum REF_STOCK_TYPE
        {
            Opening,
            Closing,
            DE,
            WD,
            TI,
            TO,
            TC,
            GC,
            XR,
            RX,
            BU,
            SE,
            DL,
            EX,
            NC,
            NT,
            TL,
            XE,
            XN,
            BD,
            SD,
        }


    }
}
