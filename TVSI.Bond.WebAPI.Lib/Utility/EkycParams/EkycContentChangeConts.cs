﻿namespace TVSI.Bond.WebAPI.Lib.Utility.EkycParams
{
    public class EkycContentChangeConts
    {
        public const string EK_HUYBANK = "{DeleteBank}";
        public const string EK_DANGKYBANK = "{RegisterBank}";

        public const string EK_DANGKYDICHVUGDTT = "{RegisterService}";

        public const string EK_HUYDANGKYTKDOIUNG = "{DeleteInternalAccount}";
        public const string EK_DANGKYCHUYENTIEN = "{BankTransfer}";
        
    }
}