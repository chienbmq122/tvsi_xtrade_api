﻿namespace TVSI.Bond.WebAPI.Lib.Utility.EkycParams
{
    public class EkycStatusConts
    {
        public const string EK_REG_SERVICE_STATUS = "1"; // TRẠNG THÁI ĐĂNG KÝ DỊCH VỤ GDTT
        public const string EK_INFO_CUSTOMER = "2"; // TRẠNG THÁI THAY ĐỔI THÔNG TIN CÁ NHÂN
        public const string EK_REG_MONEY_TRANSFER = "3"; // TRẠNG THÁI ĐĂNG KÝ CHUYỂN TIỀN
    }
}