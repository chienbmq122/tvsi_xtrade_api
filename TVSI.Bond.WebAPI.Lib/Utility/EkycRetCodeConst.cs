﻿using System;
using System.ComponentModel;

namespace TVSI.Bond.WebAPI.Lib.Utility
{
    public class EkycRetCodeConst : RetCodeConst
    {
        public const string EK902_TYPE_FAIL_MEDIA = "EK_902";           // không đúng định dạng File
        public const string EK903_DATA_EMPTYORNULL = "EK_903";         // Dữ liệu null hoặc rỗng 
        public const string EK1000_DATA_SAVEFAIL = "EK_1000";            // Lưu không thành công
        public const string EK904_CARDID_EXISTED = "EK_904";             // THÔNG TIN CMND/CCCD đã tồn tại
        public const string EK905_ACCOUNT_NOREGISTER = "EK_905";             // TK CHUA DANG KY
        public const string EK999_MISSING_DATA = "EK_999";             // Thiếu dữ liệu đầu vào
        public const string EK906_SALEID_NOT_EXISTED = "EK_906";             // Sale id không tồn tại
        public const string EK900_VALIDATION_DATA = "EK_900";             // validate data
        public const string EK_998_ERRORS_LIST = "EK_998"; // 
        public const string EK_908_BIRTHDAY_FORMAT_ERROR = "EK_908"; // định dạng ngày sinh k đúng.
        public const string EK_907_UNDER_AGE_18 = "EK_907"; // Duoi 18 tuoi
        public const string EK_909_ISSUEDATE_FORMAT_ERROR = "EK_909"; // khong dung dinh dang ngay cap cmnd/cccd
        public const string EK_911_EXPIRED_CARDID = "EK_911"; // CMND/CCCD HET HAN 15 NAM
        public const string EK_912_IS_LENGTH_MOBILE = "EK_912"; // so dien thoai k dung
        public const string EK_913_EMAIL_FORMAT_ERROR = "EK_913"; // email k dung dinh dang
        public const string EK_914_ADDRESS_LENGTH14 = "EK_914"; // dia chi phai tren 14 ky tu
        public const string EK_930_ADDRESS_LENGTH32 = "EK_LENG_932"; // dia chi phai tren 14 ky tu
        public const string EK_915_BANK_ONE_ISVALID_REQUIRED= "EK_915"; // dien full thong tin ngan hang
        public const string EK_SALEID_TRUE = "EK_001"; // SaleID tồn tại
        public const string EK_916_UPDATE_VIDEO = "EK_916"; // Trạng thái video update
        public const string EK_917_UPDATE_VIDEO = "EK_917"; // Trạng thái video update
        public const string EK_918_INFONOTSAME = "EK_918"; // THÔNG TIN KHÔNG KHỚP
        public const string EK_919_ACCNOTFOUND = "EK_919"; // Không tìm thấy số tài khoản
        public const string EK_999_CONFIRM_EMAIL = "EK_CF_999"; // Trạng thái video update
        public const string EK_998_CHANGEINFOCUST = "EK_CF_998"; // Chuyển đổi trạng thái ko thành công
        public const string EK_999_CHANGEINFOPENDING = "EK_1001"; // trạng thái TĐTT đang chờ
        public const string EK_333_ACCOUNTMARGINEEXISTED = "EK_333"; // TK MARGIN DA DUOC DANG KY HOAC HUY
        public const string EK_555_CHECKADDPROFILE = "EK_555";// TK ko có nợ hồ sơ
        public const string EK_001_DATAEXISTED = "EK_001";// Dữ liệu tồn tai
        public const string EK_966_DATAEXISTED = "EK_966";// Dữ liệu tồn tai ekyc



    }

    public class EkycRetMsgConst
    {
        public const string EKRetMsg_EK000 = "Đăng ký tài khoản thành công."; // Đăng ký tài khoản  ekyc thành công
        public const string EKRetMsg_EK902 = "Không đúng định dạng File.";           // không đúng định dạng File
        public const string EKRetMsg_EK903 = "Mã nhân viên không tồn tại.";         // Dữ liệu null hoặc rỗng 
        public const string EKRetMsg_EK1000 = "Lưu không thành công.";            // Lưu không thành công
        public const string EKRetMsg_EK904= "CMT/CCCD đã tồn tại.";            // Lưu không thành công
        public const string EKRetMsg_EK905= "Tài khoản chưa xác thực hoặc chưa đăng ký.";            // TK CHUA DANG KY
        public const string EKRetMsg_EK999= "Quý khách vui lòng hoàn thiện đủ các thông tin bắt buộc";            // đầu vào Model thiếu trường required
        public const string EKRetMsg_EK906= "Mã nhân viên không tồn tại";            // saleID không tồn tại
        public const string EKRetMsg_EK907= "Giấy tờ quý khách không đạt điều kiện để mở tài khoản chứng khoán";            // Khong du 18+
        public const string EKRetMsg_EK908= "Định dạng ngày sinh không đúng.  Vui lòng nhập đúng định dạng Ngày/ Tháng/ Năm";          
        public const string EKRetMsg_EK909= "Định dạng ngày cấp  không đúng.  Vui lòng nhập đúng định dạng Ngày/ Tháng/ Năm";          
        public const string EKRetMsg_EK910= "Ngày cấp không hợp lệ. Quý khách vui lòng nhập lại.";          
        public const string EKRetMsg_EK911= "Giấy tờ quý khách không đạt điều kiện để mở tài khoản chứng khoán.";          
        public const string EKRetMsg_EK912= "Số điện thoại không đúng, Quý khách vui lòng nhập đúng số điện thoại.";          
        public const string EKRetMsg_EK913= "Email không hợp lệ. Quý khách vui lòng nhập địa chỉ email đúng.";          
        public const string EKRetMsg_EK914= "Quý khách vui lòng nhập đủ 15 ký tự cho Địa chỉ liên hệ.";          
        public const string EKRetMsg_EK915= "Quý khách vui lòng hoàn thiện đủ các thông tin Tài  khoản Ngân hàng 1.";          
        public const string EKRetMsg_EK917= "video chưa dược cập nhật.";          
        public const string EKRetMsg_EK917_ACCNUM= "Số tài khoản không đúng.";          
        public const string EKRetMsg_EK917_SACEFAIL= "Có lỗi xảy ra.";          
        public const string EKRetMsg_EK919_ACCNOTFOUND= "Không tìm thấy số tài khoản.";          
        public const string EKRetMsg_EK_CONTENT_LENGTH= "Chuỗi chưa đủ ký tự";          
        public const string EKRetMsg_EK_CONFIRM_999= "Đăng ký không thành công";          
        public const string EKRetMsg_EK_SUCCESS = "Thành công";          
        public const string EKRetMsg_EK_CHANGEINFOCUST_998= "Thay đổi thông tin không thành công.";
        public const string EKRetMsg_EK_INFOSAME= "Thông tin xác thực khớp.";
        public const string EKRetMsg_EK_NOTSAME= "Thông tin xác thực không khớp.";
        public const string EKRetMsg_EK_MARGIN= "Tài khoản Margin đã được đăng ký";
        public const string EKRetMsg_EK_CHANGEPENDING_999 = "Thông tin được lựa chọn thay đổi đã có yêu cầu thay đổi đang trong quá trình xử lý. Quý khách có thế theo dõi tiến trình xử lý tại mục Quản lý thay đổi thông tin.";
        public const string EKRetMsg_EK_SMS = "Tài khoản không có nợ hồ sơ";
        public const string EKRetMsg_EK_PHONE01 = "Số điện thoại {Phone} đã được đăng ký SMS trước đó, vui lòng chọn thay đổi sđt 01!";
        public const string EKRetMsg_EK_PHONE01_SMS = "Đã có số điện thoại đăng ký SMS trước đó nên không thể thêm mới !";
        public const string EKRetMsg_EK_OpenInfo_Form = "Quý khách đã đăng ký mở tài khoản thành công. Vui lòng liên hệ Nhân viên quản lý tài khoản hoặc 19001885 để kích hoạt giao dịch.";
        public const string EKRetMsg_EK_OpenInfo_AccountExst = "Quý khách đã mở tài khoản số {0}.Vui lòng kích hoạt lại tài khoản để giao dịch ngay!";
        public const string EKRetMsg_EK_OpenInfo_CheckEmail = "Quý khách đã đăng ký mở tài khoản trực tuyến thành công. Vui lòng truy cập email để kích hoạt tài khoản và giao dịch ngay!";
    }

    public class EkycCompareInfoCust
    {
        public const string CLIENT_REQUEST = "Client_Request";
        public const string TVSI_RESPONSE = "TVSI_Response";
        public const string FULLNAME = "FullName";
        public const string CARID = "CardID";
        public const string SEX = "Sex";
        public const string BIRTHDAY = "BirthDay";
        public const string ISSUEDATE = "IssueDate";
        public const string CARDISSUE = "CardIssuer";
    }

    public class EkycChangeCustInfoConst
    {
        public const string NAME_HOTEN = "HoTen";
        public const string NAME_CMND = "CMND";
        public const string NAME_DIA_CHI = "DiaChi";
        public const string NAME_NGUOI_DAI_DIEN = "NgDaiDien";
        public const string NAME_NGUOI_UY_QUYEN = "NgUyQuyen";
        public const string NAME_SO_DIEN_THOAI_01 = "SDT01";
        public const string NAME_SO_DIEN_THOAI_02 = "SDT02";
        public const string NAME_EMAIL = "Email";
        public const string NAME_CHUKY = "ChuKy";
        public const string NAME_FILE_CHUKY = "FileChuKy";
        public const string NAME_THAY_DOI_KHAC = "OtherChange";

        public const string NAME_BANK_TRANSFER = "BankTransfer";
        public const string NAME_ACCOUNT_TRANSFER = "AccountTransfer";

        public const string NAME_GDTT = "GDTT";
        public const string NAME_RESET_PASS = "ResetPass";
        public const string NAME_RESET_PIN = "ResetPin";
        public const string NAME_UTTD = "UTTD";
        public const string NAME_SMS = "SMS";
        public const string NAME_SAO_KE = "SaoKe";
        public const string NAME_DICH_VU_KHAC = "OtherService";

        public const string REGIST_SYMBOL = "w:char=\"00FE\"";
        public const string DEFAULT_SYMBOL = "w:char=\"F0A8\"";

        public const string REPORT_HOTEN = "HoTen";
        public const string REPORT_DKSH = "DKSH";
        public const string REPORT_NGAYCAP = "NgayCap";
        public const string REPORT_LOAIHINH = "LoaiHinh";
        public const string REPORT_DIACHI = "DiaChi";
    }

    public class EkycCommondConst
    {
        public const string CREATE_USER = "EKYC-MOBILE";
        public const string BRANHID_NO = "01";
        public const string IMAGE_BASE64 = "data:image/jpg;base64,";
        public const string VIDEO_BASE64 = "data:video/mp4;base64,";
        public const string CODE044C = "044C";

    }
    public enum EkycChangeCustInfoStatus
    {
        CHO_XU_LY = 0,
        DANG_XU_LY = 1,
        DA_XU_LY = 2,
        TU_CHOI = 9,
        DA_XOA = 99,
    }
    public enum EkycChangeCustInfoStatusVsd
    {
        CHO_XU_LY = 2,
        DANG_XU_LY = 3,
        HOAN_THANH = 10,
        TU_CHOI = 999
    }
    
    public enum AdditionContractConst
    {
        Normal = 0,     // HĐ thường
        Margin = 1,     // HĐ Margin
        Derivative = 2  // HĐ phái sinh
    }
    public enum DocumentTypeConst
    {
        OPEN_DOCUMENT = 1,
        ADDITION_DOCUMENT = 2
    }
    public enum CustTypeConst
    {
        INDIVIDUAL_DOMESTIC_INVEST = 1,     // CN trong nuoc
        ORGANIZATION_DOMESTIC_INVEST = 2,   // TC trong nuoc
        INDIVIDUAL_FOREIGN_INVEST = 3,       // CN nuoc ngoai
        ORGANIZATION_FOREIGN_INVEST = 4     // TC nuoc ngoai
    }
    public class OpenAccountStatus
    {
        public const string CHO_XU_LY_000 = "000";
        public const string DANG_XU_LY_100 = "100";
        public const string BU_MAN_XU_LY_001 = "001";
        public const string FS_XU_LY_002 = "002";
        public const string BU_MAN_TU_CHOI_099 = "099";
        public const string DA_KICK_HOAT_200 = "200";
        public const string HOAN_THANH_300 = "300";
        public const string NVSS_TU_CHOI_199 = "199";
        public const string KSVSS_TU_CHOI_299 = "299";
        public const string FS_TU_CHOI_399 = "399";
        public const string HUY_999 = "999";
        public const string KICH_HOAT_102 = "102";
    }
    public class DocumentStatusConst
    {
        public const string TAO_MOI_000 = "000";
        public const string KO_HO_SO_102 = "102";
        public const string CHO_XU_LU_100 = "100";
        public const string HOAN_THANH_200 = "200";
        public const string TU_CHOI_199 = "199";
        public const string NO_HO_SO_101 = "101";
    }
    public enum ServiceStatusConst
    {
        [Description("Không đăng ký")]
        KO_DANG_KY = -1,
        [Description("Chờ xử lý")]
        CHO_XU_LY = 0,
        [Description("Chờ HO xác nhận")]
        CHO_HO_XAC_NHAN = 1,
        [Description("Chờ KH xác nhận")]
        CHO_KH_XAC_NHAN = 2,
        [Description("Từ chối")]
        BUMAN_TU_CHOI = 99,
        [Description("Từ chối")]
        KSV_TU_CHOI = 98
    }
    
    
}