﻿using Humanizer;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;

namespace TVSI.Bond.WebAPI.Lib.Utility
{
    public class CommonConst
    {
        public const string SOURCE_TYPE_M = "M";  // Mobile App
        public const string SOURCE_TYPE_W = "W";  // Website

        public const string LANGUAGE_VI = "VI";   // Việt Nam
        public const string LANGUAGE_EN = "EN";   // English

        public const string SIDE_BUY = "B";
        public const string SIDE_SELL = "S";
        public const string SIDE_ROLL = "R";

        public enum RedeemType
        {
            Buy = 1,
            Sell = 2,
            Roll = 3
        }

        public enum BondProductType
        {
            Bond = 1,
            MM = 2
        }

        public enum ProductType
        {
            FIX = 1,
            FLEX = 2,
            OUTCUT = 3,
            OUTRIGHT = 4,
            TVSI = 5,
            MM = 6
        }

        public static string ToWords(object obj, string lang)
        {
            try
            {
                var data = obj.ToString().Replace(",", "");
                var value = (long)Math.Round(Convert.ToDouble(data));
                if (string.IsNullOrWhiteSpace(lang) || lang.Transform(To.UpperCase) == "VI")
                    return value.ToWords(new CultureInfo("vi")).Transform(To.SentenceCase);
                else
                    return value.ToWords(new CultureInfo(lang)).Transform(To.SentenceCase);
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string ConvertWordToEN(object value)
        {
            var chuoiTiengVietCoDau = value?.ToString() ?? "";
            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string strFormD = chuoiTiengVietCoDau.Normalize(System.Text.NormalizationForm.FormD);
            return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static string FormatDateTimeEN(string value)
        {
            try
            {
                CultureInfo provider = CultureInfo.CurrentCulture;
                return DateTime.ParseExact(value, "dd/MM/yyyy", provider).ToString("MMMM dd, yyyy");
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string FormatDateTimeEN(DateTime? value)
        {
            if (value.HasValue)
                return value.Value.ToString("MMMM dd, yyyy");
            else
                return "";
        }
    }

    public enum AssetTypeEnum
    {
        [Description("Tài sản Cổ phiếu")]
        StockAsset = 1,
        [Description("Tài sản Trái phiếu")]
        BondAsset = 2,
        [Description("Tài sản MM")]
        MmAsset = 3,
        [Description("HĐ ứng trước")]
        AdvanceAsset =4
    }


    public enum RedeemType
    {
        Buy = 1,
        Sell = 2,
        Roll = 3
    }
    
    public class CommonConstExtend
    {
        public const string FAIL_DATA_EMPTY = "E100";           // Không tìm thấy dữ liệu
        public const string FAIL_SAVE_FAILED = "E111";           // Lưu không thành công
        public const string FAIL_VALIDATE_FAILED = "E222";           // Thiếu dữ liệu đầu vào
        public const string FAIL_VALIDATE_ACCOUNT_EXISTED = "E333";           // Dữ liệu tồn tại
        public const string ERROR_OCCURRED_E888 = "E888"; // Cõ lỗi xảy ra
        
    }
 
}
