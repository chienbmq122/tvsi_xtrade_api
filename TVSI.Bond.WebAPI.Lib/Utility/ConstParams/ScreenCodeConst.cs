﻿namespace TVSI.Bond.WebAPI.Lib.Utility

{
    public class ScrCodeConst
    {
        public const string SCREEN_MSMLO = "MSMLO";     // Màn hình đăng nhập
        public const string SCREEN_MSMCA = "MSMCA";     // Màn hình thông tin tài khoản > Thông tin đăng nhập > Thay đổi MK
        public const string SCREEN_MSMCI = "MSMCI";     // Màn hình thông tin tài khoản > Thông tin đăng nhập > Thay đổi mã Pin
        public const string SCREEN_MSMNO = "MSMNO";     // All màn hình > Icon Thông báo > màn hinh thông báo

        public const string SCREEN_MCMCB = "MCMCB";     // Màn hình SPTC > Tab Tài sản
        public const string SCREEN_MBMPR = "MBMPR";     // Màn hình SPTC > Tab Trái phiếu > Tab Sản phẩm
        public const string SCREEN_MBMOR = "MBMOR";     // Màn hình SPTC > Tab Trái phiếu > Tab Bảng giá Outright
        public const string SCREEN_MBMBI = "MBMBI";     // Màn hình SPTC > Tab Trái phiếu > Tab Bảng giá Outright > Thông tin trái phiếu
        public const string SCREEN_MBMIV = "MBMIV";     // Màn hình SPTC > Tab Trái phiếu > Tab Đầu tư > Màn hình mua
        public const string SCREEN_MOMCL = "MOMCL";     // Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Danh sách HĐ
        public const string SCREEN_MOMOB = "MOMOB";     // Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Sổ lệnh
        public const string SCREEN_MOMOH = "MOMOH";     // Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Lịch sử giao dịch
        public const string SCREEN_MOMOC = "MOMOC";     // Màn hình SPTC > Tab Trái phiếu > Tab TTGD > Xác nhận lệnh đặt
        public const string SCREEN_MCTCI = "MCTCI";     // Màn hình GD tiền > Chuyển tiền
        public const string SCREEN_MCTBT = "MCTBT";     // Màn hình GD tiền > Chuyển tiền > Chuyển tiền ngân hàng
        public const string SCREEN_MCTIT = "MCTIT";     // Màn hình GD tiền > Chuyển tiền > Chuyển tiền TK Chứng khoán
        public const string SCREEN_MCTTO = "MCTTO";     // Màn hình GD tiền > Chuyển tiền > Trạng thái chuyển tiền
        public const string SCREEN_MCTCH = "MCTCH";     // Màn hình GD tiền > Chuyển tiền > Lịch sử chuyển tiền
        public const string SCREEN_MCTMT = "MCTMT";     // Màn hình GD tiền > Chuyển tiền > Sao kê tiền
        public const string SCREEN_MCMCI = "MCMCI";     // Màn hình Thông tin tài khoản 
    }
}
