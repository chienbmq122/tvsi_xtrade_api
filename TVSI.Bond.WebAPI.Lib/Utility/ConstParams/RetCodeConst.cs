﻿namespace TVSI.Bond.WebAPI.Lib.Utility
{
    public class RetCodeConst
    {
        // Common
        public const string SUCCESS_CODE = "000";           // Thanh cong
        public const string ERR999_SYSTEM_ERROR = "E999";    // Loi he thong
        public const string ERR998_NOT_IMPLEMENT = "E998";   // API chua cai dat
        public const string ERR900_ISNULLOREMPTY = "E900";   // Gia tri nhap la Null hoac Empty
        public const string ERR901_DATETIME_INVALID = "E901";   // Gia tri ngày tháng không hợp lệ

        // SM_LO_SystemLogin
        public const string MSMLO_E001_PASSWORD_INCORRECT = "MSMLO_001";        // User va Mat khau khong dung
        public const string MSMLO_E002_CUSTOMER_NOT_ACTIVE = "MSMLO_002";       // Tai khoan chua duoc kich hoat
        public const string MSMLO_E003_NOT_PERMISSION = "MSMLO_003";       

        // SM_CA_ChangePassword
        public const string MSMCA_E001_OLDPASSWORD_INCORRECT = "MSMCA_001";     // Mat khau cu khong dung

        // SM_CA_ForgotPassword
        public const string MSMCA_E001_FORGOTPASSWORD_INCORRECT = "MSMFP_001";     // Ma khach hang hoac CCCD khong dung
        public const string MSMMP_001_ACCOUNT_NOT_FOUND = "MSMMP_001";     // Ma khach hang hoac CCCD khong dung

        // SM_FP_ForgotPin
        public const string MSMCA_E001_RESETPIN_INCORRECT = "MSMRP_001";     // Reset ma PIN

        // SM_CC_ConfirmCode
        public const string MSMCA_E001_CONFIRMCODE_INCORRECT = "MSMCC_001";     // Mã kích hoạt

        // SM_FP_ForgotPin
        public const string MSVA_E001_VERIFYACCOUNT_INCORRECT = "MSVA_001";     // Kích hoạt tài khoản

        // SM_CI_ChangePin
        public const string MSMCI_E001_OLDPIN_INCORRECT = "MSMCI_001";          // Ma Pin cu khong dung

        // SM_ME_SendSms
        public const string MSMME_E001_MOBILE_PHONE_NOT_EXIST = "MSMME_001";    // Số điện thoại không tồn tại

        // CM_CB_CustCashBalance
        public const string MCMCB_E001_CUSTBALANCE_NOT_FOUND = "MCMCB_001";     // Không tìm thấy thông tin số dư tài khoản

        // CM_CI_CustInfo
        public const string MCTCI_E001_CUSTINFO_NOT_FOUND = "MCTCI_001";     // Không tìm thấy thông tin tài khoản

        // CT_BT_CashTransRequestBankTransfer
        public const string MCTBT_E001_BANKINFO_NOT_FOUND = "MCTBT_001";    // Khong tim thay thong tin ngan hang
        public const string MCTBT_E002_TRANSFER_FAILED = "MCTBT_002";    // Loi gui tien ngan hang

        // CT_IT_CashTransRequestInternalTransfer
        public const string MCTIT_E001_INTERNALACCOUNT_NOT_FOUND = "MCTIT_001"; // Không tìm thấy thông tin chuyển khoản nội bộ
        public const string MCTIT_E002_TRANSFER_FAILED = "MCTIT_002"; // Loi khi chuyen khoan noi bo

        // OM_OB_OrderCancel
        public const string MOMOB_E001_NOT_FOUND = "MOMOB_E001"; // Không tìm thấy thông tin HĐ cần hủy
        public const string MOMOB_E002_STATUS_HAS_CHANGED = "MOMOB_E002"; // Trạng thái HĐ đã thay đổi

        // OM_OB_RefuseOrder
        public const string MOMOB_E003_CAN_NOT_REFUSE = "MOMOB_E003"; // Lệnh bán đúng hạn => Không thể từ chối yêu cầu xác nhận
    }
}
