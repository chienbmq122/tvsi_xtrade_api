﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using log4net;
using Serilog;
using TVSI.Bond.WebAPI.Lib.Models.Ekyc;
using TVSI.DAL.SystemNotificationDB;
namespace TVSI.Bond.WebAPI.Lib.Utility
{
    public class MailHelper
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MailHelper));

        public static void SendForgetPasswordMail(Dictionary<string, string> accountInfo, string email)
        {
            const string subject = "iTrade Home - Thông báo yêu cầu cấp lại mật khẩu thành công - Forgot Password";
            var templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_ForgotPassTemplate.html");
            var content = "";

            using (FileStream readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
            }
            //log.Info("SEND MAIL FORGOT PASSWORD:" + content);
            //Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + "mail-check")
            //    .Information("SEND MAIL FORGOT PASSWORD:" + content);
            foreach (KeyValuePair<string, string> item in accountInfo)
            {
                content = content.Replace(item.Key, item.Value);
            }
            
            ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
            //log.Info("SEND MAIL FORGOT PASSWORD:COMPLETED");
            //Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "/" + "mail-check")
            //    .Information("SEND MAIL FORGOT PASSWORD:COMPLETED");
        }


        public static void SendForgetPINMail(Dictionary<string, string> accountInfo, string email)
        {
            const string subject = "iTrade Home - Thông báo yêu cầu cấp lại PIN thành công - Forgot Pin code";
            var templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_ForgotPINTemplate.html");
            var content = "";

            using (FileStream readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
            }

            foreach (KeyValuePair<string, string> item in accountInfo)
            {
                content = content.Replace(item.Key, item.Value);
            }

            ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
        }



        public static void SendVerifyAccountMail(Dictionary<string, string> accountInfo, string email)
        {
            const string subject = "iTrade Home - Thông báo Kích hoạt tài khoản thành công – Active account";
            var templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_VerifyAccountTemplate.html");
            var content = "";

            using (FileStream readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
            }

            foreach (KeyValuePair<string, string> item in accountInfo)
            {
                content = content.Replace(item.Key, item.Value);
            }

            ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
        }



        public static void SendNewPassMail(Dictionary<string, string> accountInfo, string email)
        {
            const string subject = "iTrade Home - Cấp lại mật khẩu - New Password";
            var templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_NewPassTemplate.html");
            var content = "";

            using (FileStream readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
            }

            foreach (KeyValuePair<string, string> item in accountInfo)
            {
                content = content.Replace(item.Key, item.Value);
            }

            ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
        }


        public static void SendNewPINMail(Dictionary<string, string> accountInfo, string email)
        {
            const string subject = "iTrade Home - Cấp lại mật khẩu - New Password";
            var templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_NewPinTemplate.html");
            var content = "";

            using (FileStream readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
            }

            foreach (KeyValuePair<string, string> item in accountInfo)
            {
                content = content.Replace(item.Key, item.Value);
            }

            ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
        }    
        public static void SendMailToSale(Dictionary<string, string> saleInfo, string email)
        {
            const string subject = "TVSI: Đăng ký mở tài khoản trực tuyến";
            var templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_SendSale_EKYC.html");
            var content = "";

            using (FileStream readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
            }

            foreach (KeyValuePair<string, string> item in saleInfo)
            {
                content = content.Replace(item.Key, item.Value);
            }

            ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
        } 
        public static void SendOTPEmail(Dictionary<string, string> otp, string email)
        {
            const string subject = "TVSI: THÔNG BÁO MÃ XÁC THỰC (AUTHENTICATION CODE NOTIFICATION)";
            var templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_SendOTPTemplate.html");
            var content = "";

            using (FileStream readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
            }

            foreach (KeyValuePair<string, string> item in otp)
            {
                content = content.Replace(item.Key, item.Value);
            }

            ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
        }

        public static void SendEmailToSaleEkyc(EkycSaveUserInfoRequest_v2 model,string email,string customDepartment = "",string saleNameCustom = "")
        {
            const string subject = "TVSI:Đăng ký mở tài khoản eKYC trực tuyến";
            var templateFile = HttpContext.Current.Server.MapPath("~/Templates/Email/Mail_SendSale_EKYC.html");
            var content = "";

            using (FileStream readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
            }

            if (!string.IsNullOrEmpty(content))
            {

                var saleName = "";
                var department = "";
                var packName = "";
                if (model.ServicePackage == 1)
                {
                    saleName = "Phòng Dịch Vụ Tư Vấn Giao Dịch";
                    department = "DVGD";
                    packName = "Dịch vụ Tư vấn Giao Dịch";

                }

                if (model.ServicePackage == 2)
                {
                    saleName = "Phòng Dịch Vụ Đầu Tư";
                    department = "DVĐT";
                    packName = "Dịch vụ Đầu Tư";
                }

                if (model.ServicePackage == 3)
                {
                    saleName = "TVSI-CĐGD";
                    department = "CĐGD";
                    packName = "Dịch vụ chủ động giao dịch hoàn toàn";
                }

                if (model.ServicePackage == 0)
                {
                    department = customDepartment;
                }

                var items = new Dictionary<string, string>()
                {
                    { "{Channel_Open}", "eKYC Web" },
                    { "{Department}", department },
                    { "{SaleName}", !string.IsNullOrEmpty(saleNameCustom) ? saleNameCustom : saleName },
                    { "{FullName}", model.CustomerName },
                    { "{BirthDay}", model.Birthday },
                    { "{CardID}", model.CardID },
                    { "{IssueDate}", model.IssueDate },
                    { "{Phone}", model.Mobile },
                    { "{Emai}", model.Email },
                    { "{Address}", model.Address_02 },
                    { "{Package}", packName },
                    { "{Note}", model.Note },
                    { "{CreateDate}", DateTime.Now.ToString("dd/MM/yyyy") },
                };
                foreach (KeyValuePair<string, string> item in items)
                {
                    content = content.Replace(item.Key, item.Value);
                }
                Log.Information($"SendEmailToSaleEkyc SendEmailToSaleEkyc send email {content} email gui di {email}");
                ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
            }
            else
            {
                Log.ForContext("SendEmailToSaleEkyc",$"Loi khong load dc template SendEmailToSaleEkyc");
            }

        }
        
        
        
    }
}
