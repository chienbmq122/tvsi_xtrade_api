﻿namespace TVSI.Bond.WebAPI.Lib.Utility.OddLotParams
{
    public class OddLotRetCodeConst
    {
        public const string OL_902_MISSING_DATA = "OL_902"; // Thiếu dữ liệu đầu vào
        public const string OL_903_NOT_FOUND = "OL_903"; // Không tìm thấy thông tin
    }
}