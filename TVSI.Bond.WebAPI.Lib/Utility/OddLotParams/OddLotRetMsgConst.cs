﻿namespace TVSI.Bond.WebAPI.Lib.Utility.OddLotParams
{
    public class OddLotRetMsgConst
    {
        public const string
            OL_902_MISSING_DATA =
                "Thông tin bắt buộc bị thiếu"; // Input Model thiếu trường dữ liệu required
        public const string OL_903_NOT_FOUND = "Không tìm thấy thông tin"; // Không tìm thấy thông tin
    }
}