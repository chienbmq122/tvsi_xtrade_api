﻿namespace TVSI.Bond.WebAPI.Lib.Utility.OddLotParams
{
    public class OddLotOrderStatusConst
    {
        public const int CHO_XU_LY = 0;
        public const int DANG_XU_LY = 10;
        public const int DANG_CHO_VSD = 20;
        public const int DA_XU_LY = 30;
        public const int TU_CHOI = 40;
        public const int HUY_BO = 50;
    }
}