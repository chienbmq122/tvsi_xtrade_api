﻿using System;
using System.Globalization;

namespace TVSI.Bond.WebAPI.Lib.Utility
{
    public class DateTimeUtils
    {
        public static bool IsValidDate(string input, string format = "dd/MM/yyyy")
        {
            DateTime dt;

            if (DateTime.TryParseExact(input, format,
                            System.Globalization.CultureInfo.InvariantCulture,
                            DateTimeStyles.None, out dt))
            {
                //your condition fail code goes here
                return true;
            }

            return false;

        }
    }
}
